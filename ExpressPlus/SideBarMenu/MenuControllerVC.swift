//
//  MenuControllerVC.swift
//  SidebarMenu
//
//  Created by Apple  on 16/10/19.
//  Copyright © 2019 AppCoda. All rights reserved.
//

import UIKit
import Alamofire

class MenuControllerVC: UIViewController {

    let cellReuseIdentifier = "menuControllerVCTableCell"
    
    var bgImage: UIImageView?
    
    var roleIs:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "NAVIGATION"
            lblNavigationTitle.textColor = .white
        }
    }
    
    // driver
    var arrMenuItemList = ["Dashboard", // done
                           "Edit Profile", // done
                           "Job history", // done
                           "Orders",
                           "Food Order Earning",
                           // "Service Earning",
                           // "To Get Earning",
                           // "Special Order Earning",
                           "Reviews",
                           "Cashout",
                           "Bag Purchase",
                           "Card",
                           "W-9 Form",
                           "Change Password",
                           "Help",
                           "Logout"
    ]
    
    
    var arrMenuItemListImage = ["home",
                            "edit",
                            "trip",
                            "note",
                            "dollar",
                            "dollar",
                            "dollar",
                            "dollar",
                            "star_white",
                            "cashout",
                            "bag",
                            "cash_card",
                            "w9-form",
                            "lock",
                            "help",
                            "logout"
    ]
    
    
    // customer menu
    var arrMenuItemListCustomer = ["Dashboard",
                                      "Edit Profile",
                                      "Order History",
                                      /*"Food",
                                      "Services",
                                      "To get",
                                      "Special Order",*/
                                      "Change password",
                                      "Help",
                                      "Logout" ]
       
    
    
    
       var arrMenuItemListImageCustomer = ["home",
                                               "edit",
                                               "trip",
                                               /*"food-1",
                                               "service_menu",
                                               "tp_get",
                                               "dish",*/
                                               "lock",
                                               "help",
                                               "logout" ]
        
    
    
    
    // restaurant menu
    var arrMenuItemListRestaurant = ["Dashboard",
                                   "Edit Profile",
                                   "Manage Menu",
                                   "Orders",
                                   "Order History",
                                   "Total Earnings",
                                   "Reviews",
                                   "Cashouts",
                                   "Change Password",
                                   "Help",
                                   "Logout",
    ]
    
    
    
    var arrMenuItemListImageRestaurant = ["home",
                                            "edit",
                                            "dish",
                                            "note",
                                            "trip",
                                            "dollar",
                                            "star_white",
                                            "cashout",
                                            "lock",
                                            "help",
                                            "logout"
                                            ]
    
    
    
    
    @IBOutlet var menuButton:UIButton!
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
            // tbleView.tableFooterView = UIView.init()
            tbleView.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            // tbleView.separatorInset = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
            // tbleView.separatorColor = .white
        }
    }
    @IBOutlet weak var lblMainTitle:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideBarMenuClick()
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
            
            /*
             ["userId": 99, "ssnImage": , "socialId": , "image": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1587560500Corona.png, "zipCode": , "device": , "dob": , "email": restaurent@gmail.com, "AutoInsurance": , "RoutingNo": vstvush6sg6, "AccountHolderName": jjsjs, "accountType": Saving, "country": India, "deviceToken": , "logitude": 78.1694957, "middleName": , "fullName": restaurent, "AccountNo": 8505858545884555, "longitude": 78.1694957, "foodTag": Veg, "wallet": 0, "role": Restaurant, "contactNumber": 9494645544, "firebaseId": , "address": Gwalior, Madhya Pradesh 474008, India, "gender": , "drivlingImage": , "socialType": , "state": , "BankName": djdgsgs, "lastName": , "latitude": 26.2313245, "companyBackground": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1587560500Corona.png]
             (lldb)
             */
            // roleIs
            
            self.lblMainTitle.text = (person["fullName"] as! String)
            self.lblAddress.text = (person["address"] as! String)
            
            
          
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @objc func sideBarMenuClick() {
        if revealViewController() != nil {
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
}

extension MenuControllerVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        if (person["role"] as! String) == "Driver" {
            return arrMenuItemList.count
        } else if (person["role"] as! String) == "Member" {
            return arrMenuItemListCustomer.count
        } else if (person["role"] as! String) == "Restaurant" {
            return arrMenuItemListRestaurant.count
            }
        }
        
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MenuControllerVCTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MenuControllerVCTableCell
        
        cell.backgroundColor = .clear
      
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        if (person["role"] as! String) == "Driver" {
            cell.lblName.text = arrMenuItemList[indexPath.row]
            cell.imgProfile.image = UIImage(named: arrMenuItemListImage[indexPath.row])
            cell.imgProfile.backgroundColor = .clear
        } else if (person["role"] as! String) == "Member" {
            cell.lblName.text = arrMenuItemListCustomer[indexPath.row]
            cell.imgProfile.image = UIImage(named: arrMenuItemListImageCustomer[indexPath.row])
            cell.imgProfile.backgroundColor = .clear
        } else if (person["role"] as! String) == "Restaurant" {
            cell.lblName.text = arrMenuItemListRestaurant[indexPath.row]
            cell.imgProfile.image = UIImage(named: arrMenuItemListImageRestaurant[indexPath.row])
            cell.imgProfile.backgroundColor = .clear
            }
        }
        
        cell.lblName.textColor = .white
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        if (person["role"] as! String) == "Driver" {
            if indexPath.row == 0 { // dashboard
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "PDDashboardId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 1 { // REditProfile
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "REditProfileId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 2 { // job history
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "PDJobRequestId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 3 { // orders
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "PDOnlyOrdersId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 4 { // food order earning

                let myString = "foodOrderEarningCheck"
                UserDefaults.standard.set(myString, forKey: "keyFoodOrderEarnings")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "RTotalEarningsId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } /*else if indexPath.row == 5 { // food order earning

                let myString = "searviceEarningCheck"
                UserDefaults.standard.set(myString, forKey: "keyFoodOrderEarnings")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "RTotalEarningsId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 6 { // to get earning

                let myString = "toGetEarningCheck"
                UserDefaults.standard.set(myString, forKey: "keyFoodOrderEarnings")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "RTotalEarningsId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 7 { // to get earning

                let myString = "specialOrderEarningCheck"
                UserDefaults.standard.set(myString, forKey: "keyFoodOrderEarnings")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "RTotalEarningsId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } */else if indexPath.row == 5 { // orders
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "RReviewId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 6 { // cashout
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "RCashoutId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 7 { // bag purchase
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "PDBagPurchaseId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 8 { // bag purchase
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "PDCardMakeId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 9 { // cashout
                
                if let url = URL(string: K9_FORM_URL) {
                    UIApplication.shared.open(url)
                }
                
                /*
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "PDW9FormId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
                */
                
            } else if indexPath.row == 10 { // change password
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 11 { // help
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "HelpId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 12 { // logout
                let alertController = UIAlertController(title: "Logout!", message: "Are you sure you want to logout ?", preferredStyle: .actionSheet)
                
                let okAction = UIAlertAction(title: "Yes, Logout", style: .default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                    
                    self.logoutWB()
                    
                    
                }
                alertController.addAction(okAction)
                
                let dismissAction = UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                    NSLog("OK Pressed")
                    
                }
                alertController.addAction(dismissAction)
                self.present(alertController, animated: true, completion: nil)
            }
        } else if (person["role"] as! String) == "Member" {
            if indexPath.row == 0 { // dashboard
                
                let myString = "backOrMenu"
                UserDefaults.standard.set(myString, forKey: "keySetToBackOrMenu")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "UPFoodId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
                
            } else if indexPath.row == 1 { // edit
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "REditProfileId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 2 { // dashboard UPFood
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "PDJobRequestId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } /*else if indexPath.row == 3 { // dashboard
                
                let myString = "backOrMenu"
                UserDefaults.standard.set(myString, forKey: "keySetToBackOrMenu")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "UPFoodId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 4 { // to get
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "ServicesId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 5 { // to get
                
                let myString = "backOrMenu"
                UserDefaults.standard.set(myString, forKey: "keySetToBackOrMenu")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "UPToGetId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 6 { // help
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "AnotherOptionScrenId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            }*/ else if indexPath.row == 3 { // change password
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 4 { // help
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "HelpId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 5 { // logout
                let alertController = UIAlertController(title: "Logout!", message: "Are you sure you want to logout ?", preferredStyle: .actionSheet)
                
                let okAction = UIAlertAction(title: "Logout", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                    
                    self.logoutWB()
                }
                alertController.addAction(okAction)
                
                let dismissAction = UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                    
                }
                alertController.addAction(dismissAction)
                self.present(alertController, animated: true, completion: nil)
            }
        } else if (person["role"] as! String) == "Restaurant" {
            if indexPath.row == 0 { // dashboard
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "RHomeId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 1 { // edit profile
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "REditProfileId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 2 { // manage menu
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "RManageMenuId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 3 { // manage menu
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "OrderListId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 4 { // order history
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "ROrderHistoryId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 5 { // total earnings
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "RTotalEarningsId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 6 { // review
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "RReviewId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 7 { // cashout
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "RCashoutId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 8 { // change password
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 9 { // help
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                self.view.window?.rootViewController = sw
                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "HelpId")
                let navigationController = UINavigationController(rootViewController: destinationController!)
                sw.setFront(navigationController, animated: true)
            } else if indexPath.row == 10 { // logout
                let alertController = UIAlertController(title: "Logout!", message: "Are you sure you want to logout ?", preferredStyle: .actionSheet)
                
                let okAction = UIAlertAction(title: "Yes, Logout", style: .default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                    
                    self.logoutWB()
                    
                    
                }
                alertController.addAction(okAction)
                
                let dismissAction = UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                    NSLog("OK Pressed")
                    
                }
                alertController.addAction(dismissAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        }
    }
    
    @objc func logoutWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "logging out...")
    
        let urlString = BASE_URL_EXPRESS_PLUS
  
        var parameters:Dictionary<AnyHashable, Any>!
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
           
          
            parameters = [
                "action"             : "logout",
                "userId"             : person["userId"] as Any, // done
                              
            ]
         }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue("", forKey: "keyLoginFullData")
                                defaults.setValue(nil, forKey: "keyLoginFullData")

                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                                self.view.window?.rootViewController = sw
                                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeId")
                                let navigationController = UINavigationController(rootViewController: destinationController!)
                                sw.setFront(navigationController, animated: true)
                                
                                  // let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WelcomeId") as? Welcome
                                  // self.navigationController?.pushViewController(push!, animated: true)
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension MenuControllerVC: UITableViewDelegate {
    
}
