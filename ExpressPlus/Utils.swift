//
//  Utils.swift
//  KREASE
//
//  Created by Apple  on 16/10/19.
//  Copyright © 2019 Apple . All rights reserved.
//

import UIKit
import CoreLocation

// 25,32,143


// MARK:- LIVE BASE URL -
let BASE_URL_EXPRESS_PLUS = "https://www.expressplusnow.com/appbackend/services/index"

// test
// let ORDER_FOOD_GENERATE_TOKEN = "https://www.expressplusnow.com/appbackend/webroot/strip_master/splitpayment_test.php"

// live
let ORDER_FOOD_GENERATE_TOKEN = "https://www.expressplusnow.com/appbackend/webroot/strip_master/splitpayment.php"





/*
// MARK:- TEST -
let CARD_PROCESS = "https://www.expressplusnow.com/appbackend/webroot/strip_master/cardprocess_test.php"
let CHARGE_PROCESS = "https://www.expressplusnow.com/appbackend/webroot/strip_master/charge_test.php/"
//let STRIPE_PROCESS = "pk_test_g81fdr3TVQTVTVTvyleDtLkM00pnWCXte4"
let STRIPE_PROCESS = "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"
*/







// MARK:- LIVE -
 let CARD_PROCESS = "https://www.expressplusnow.com/appbackend/webroot/strip_master/cardprocess.php"
 let CHARGE_PROCESS = "https://www.expressplusnow.com/appbackend/webroot/strip_master/charge.php"
 let STRIPE_PROCESS = "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"








let K9_FORM_URL = "https://www.expressplusnow.com/upload-w9-form/"
// 0 ,//// 179 , 138

let NAVIGATION_BACKGROUND_COLOR = UIColor.init(red: 43.0/255.0, green: 30.0/255.0, blue: 75.0/255.0, alpha: 1)
// 0 , 155 , 123
let DASHBOARD_BACKGROUND_COLOR = UIColor.init(red: 0.0/255.0, green: 155.0/255.0, blue: 123.0/255.0, alpha: 1)



let NAVIGATION_BUSINESS_BACKGROUND_COLOR = UIColor.init(red: 25.0/255.0, green: 32.0/255.0, blue: 143.0/255.0, alpha: 1)

let NAVIGATION_TITLE_FONT_12 = UIFont(name: "Avenir Next", size: 12)
let NAVIGATION_TITLE_FONT_14 = UIFont(name: "Avenir", size: 14)
let NAVIGATION_TITLE_FONT_16 = UIFont(name: "Avenir", size: 16)

let BUTTON_BACKGROUND_COLOR = UIColor.init(red: 242.0/255.0, green: 208.0/255.0, blue: 11.0/255.0, alpha: 1)

let BUTTON_BACKGROUND_COLOR_GREEN = UIColor.init(red: 53.0/255.0, green: 211.0/255.0, blue: 100.0/255.0, alpha: 1)
let BUTTON_BACKGROUND_COLOR_BLUE = UIColor.init(red: 25.0/255.0, green: 32.0/255.0, blue: 143.0/255.0, alpha: 1)
let BUTTON_BACKGROUND_COLOR_YELLOW = UIColor.init(red: 242.0/255.0, green: 208.0/255.0, blue: 11.0/255.0, alpha: 1)

let PLACEHOLDER_EMAIL       = "Email address"
let PLACEHOLDER_PASSWORD    = "Password"
let PLACEHOLDER_NAME        = "Name"
let PLACEHOLDER_PHONE       = "Phone"
let PLACEHOLDER_ADDRESS     = "Address"

// SERVER ISSUE
let SERVER_ISSUE_TITLE          = "Server Issue."
let SERVER_ISSUE_MESSAGE_ONE    = "Server Not Responding."
let SERVER_ISSUE_MESSAGE_TWO    = "Please contact to Server Admin."

// validations
let strNameValidation:String    = "Name should not be empty."
let strPhoneValidation:String   = "Phone should not be empty."

class Utils: NSObject {
    
}

extension CLLocation {
    
    func fetchCityAndCountry(completion: @escaping (_ city: String?,_ country: String?, _ zipcode:  String?, _ localAddress:  String?, _ locality:  String?, _ subLocality:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.locality,$0?.first?.country, $0?.first?.postalCode,$0?.first?.subAdministrativeArea,$0?.first?.locality,$0?.first?.subLocality, $1) }
    }
    
    func countryCode(completion: @escaping (_ countryCodeIs:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.isoCountryCode, $1) }
    }
    
    func fullAddressFull(completion: @escaping (_ city: String?,_ country: String?, _ zipcode:  String?, _ localAddress:  String?, _ locality:  String?, _ subLocality:  String?,_ countryCodeIs:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.locality,$0?.first?.country, $0?.first?.postalCode,$0?.first?.administrativeArea,$0?.first?.locality,$0?.first?.subLocality,$0?.first?.isoCountryCode, $1) }
    }
    
}

/*
 // for root back
 for controller in self.navigationController!.viewControllers as Array {
     if controller.isKind(of: Login.self) {
         self.navigationController!.popToViewController(controller, animated: true)
         break
     }
 }
 */
