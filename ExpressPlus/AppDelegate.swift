//
//  AppDelegate.swift
//  ExpressPlus
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

// com.evs.expressPlusDis

// pdpaymentinformation ( remove comment from payment wb )

// bigmac67890@gmail.com

// live bundle : com.evs.expressPlusDis

import UIKit
import Stripe
import Firebase

import RNNotificationView

import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window:UIWindow?
    
    /// swRevealContainer
    var swContainer : SWRevealViewController?
    
    /// main navigaion controller
    var navigationController : UINavigationController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // MARK:- FIREBASE CONFIGURATION -
        FirebaseApp.configure()
        
        // MARK:- STRIPE LIVE KEY -
        Stripe.setDefaultPublishableKey(STRIPE_PROCESS)
        
        
        // MARK:- STRIPE TEST KEY -
        // Stripe.setDefaultPublishableKey("pk_test_g81fdr3TVQTVTVTvyleDtLkM00pnWCXte4")
        
        GMSServices.provideAPIKey("AIzaSyDEYP9nenPV-zJjlCSbPuyf9eYSOfBdKlk")
        
        // MARK:- REGISTER NOTIFICAITON -
        self.registerForRemoteNotifications(application)
        
        self.firebaseTokenIs()
        
        return true
    }

    @objc func registerForRemoteNotifications(_ application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            
          // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
            
        } else {
            
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
            
        }

        application.registerForRemoteNotifications()

    }
    
    // fXPduDkbWSc:APA91bE696oTApspPElP4YKUqI6CfyADbT7fqLWRfFQ2-PiYUt1mS_ymlHkLaWQ7Q7_hlLYAygCyIK77705PCJvgtadttH9zzz0UtLqlVI4Piq_IrQKcqCsC2fdPW2JHUkIbd9UdXFp1
    
    @objc func firebaseTokenIs() {
        InstanceID.instanceID().instanceID { (result, error) in
          if let error = error {
            print("Error fetching remote instance ID: \(error)")
          } else if let result = result {
            print("Remote instance ID token: \(result.token)")
            
            let defaults = UserDefaults.standard
            defaults.set("\(result.token)", forKey: "deviceFirebaseToken")
            // self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
          }
        }
    }
   
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error = ",error.localizedDescription)
    }
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
          // Called when a new scene session is being created.
          // Use this method to select a configuration to create the new scene with.
          return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
      }

      func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
          // Called when the user discards a scene session.
          // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
          // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
      }

      func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification

        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)

        // Print message ID.
        // if let messageID = userInfo[gcmMessageIDKey] {
          // print("Message ID: \(messageID)")
        // }

        // Print full message.
        print(userInfo)
      }

      func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                       fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification

        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)

        // Print message ID.
        // if let messageID = userInfo[gcmMessageIDKey] {
          // print("Message ID: \(messageID)")
       //  }

        // Print full message.
         print(userInfo)

        completionHandler(UIBackgroundFetchResult.newData)
      }
   
    
    // MARK:- WHEN APP IS OPEN -
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //print("User Info = ",notification.request.content.userInfo)
        completionHandler([.alert, .badge, .sound])
        
        
        print("User Info dishu = ",notification.request.content.userInfo)
        let dict = notification.request.content.userInfo
        
        //         print(dict["aps"] as! String as Any)
        
        /*
         aps =     {
         alert =         {
         body = "New booking has been confirm.";
         title = "New booking has been confirm.";
         };
         sound = Default;
         };
         driverContect = 2525368521;
         driverID = 354;
         driverImage = "https://www.expressplusnow.com/appbackend/img/uploads/users/1600510943images(6).jpeg";
         driverName = purnimad;
         fooOrderId = 374;
         foodOrderGroupId = 373;
         "gcm.message_id" = 1602244584111011;
         "google.c.a.e" = 1;
         "google.c.sender.id" = 27735899901;
         message = "New booking has been confirm.";
         totalAmount = 56;
         type = foodAccept;
         */
        
        // type = ServiceAccept;
        
        
        // when user pay service data
        // type :  ServiceRequestPayment
        
        
        
        /*
         User Info dishu =  [AnyHashable("type"): ToGetRequestAccept, AnyHashable("google.c.a.e"): 1, AnyHashable("gcm.message_id"): 1603280859562894, AnyHashable("google.c.sender.id"): 27735899901, AnyHashable("message"): Request accepted, AnyHashable("togetRequestId"): 240, AnyHashable("aps"): {
         alert =     {
         body = "Request accepted";
         title = "Request accepted";
         };
         sound = Default;
         }]
         Optional(ToGetRequestAccept)
         Optional({
         aps =     {
         alert =         {
         body = "Request accepted";
         title = "Request accepted";
         };
         sound = Default;
         };
         "gcm.message_id" = 1603280859562894;
         "google.c.a.e" = 1;
         "google.c.sender.id" = 27735899901;
         message = "Request accepted";
         togetRequestId = 240;
         type = ToGetRequestAccept;
         })
         
         */
        print(dict["type"] as Any)
        
        if dict["type"] == nil {
            
        } else {
            
            // FoodPayment : when user payment successfully
            
            if dict["type"] as! String == "orderComplete" {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let destinationController = storyboard.instantiateViewController(withIdentifier:"PDJobRequestId") as? PDJobRequest
                destinationController?.dictOfNotificationPopup = notification.request.content.userInfo as NSDictionary
                let frontNavigationController = UINavigationController(rootViewController: destinationController!)
                
                let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC
                
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.rearViewController = rearViewController
                mainRevealController.frontViewController = frontNavigationController
                
                DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                }
                
                // window?.rootViewController = mainRevealController
                window?.makeKeyAndVisible()
                
            } else if dict["type"] as! String == "foodAccept"  {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                print(notification.request.content.userInfo as NSDictionary)
                
                let destinationController = storyboard.instantiateViewController(withIdentifier:"UPSuccessAndPayNowId") as? UPSuccessAndPayNow
                destinationController?.dictOfNotificationPopup = notification.request.content.userInfo as NSDictionary
                let frontNavigationController = UINavigationController(rootViewController: destinationController!)
                
                let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC
                
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.rearViewController = rearViewController
                mainRevealController.frontViewController = frontNavigationController
                
                DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                }
                
                // window?.rootViewController = mainRevealController
                window?.makeKeyAndVisible()
                
            } else if dict["type"] as! String == "FoodRequest" {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let destinationController = storyboard.instantiateViewController(withIdentifier:"AllPopupsId") as? AllPopups
                destinationController?.dictOfNotificationPopup = notification.request.content.userInfo as NSDictionary
                let frontNavigationController = UINavigationController(rootViewController: destinationController!)
                
                let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC
                
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.rearViewController = rearViewController
                mainRevealController.frontViewController = frontNavigationController
                
                DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                }
                
                // window?.rootViewController = mainRevealController
                window?.makeKeyAndVisible()
                
            } else {
                
            }
            
            
            
            /*if dict["type"] as! String == "ServiceRequest" {
                
                // working
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let destinationController = storyboard.instantiateViewController(withIdentifier:"NotificationPopupForServiceRequestId") as? NotificationPopupForServiceRequest
                destinationController?.dictOfNotificationPopup = notification.request.content.userInfo as NSDictionary
                let frontNavigationController = UINavigationController(rootViewController: destinationController!)
                
                let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC
                
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.rearViewController = rearViewController
                mainRevealController.frontViewController = frontNavigationController
                
                DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                }
                
                // window?.rootViewController = mainRevealController
                window?.makeKeyAndVisible()
                
            } else if dict["type"] as! String == "ToGetRequestAccept" {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                print(notification.request.content.userInfo as NSDictionary)
                
                let destinationController = storyboard.instantiateViewController(withIdentifier:"UPSuccessAndPayNowId") as? UPSuccessAndPayNow
                destinationController?.dictOfNotificationPopup = notification.request.content.userInfo as NSDictionary
                let frontNavigationController = UINavigationController(rootViewController: destinationController!)
                
                let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC
                
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.rearViewController = rearViewController
                mainRevealController.frontViewController = frontNavigationController
                
                DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                }
                
                // window?.rootViewController = mainRevealController
                window?.makeKeyAndVisible()
                
            } else if dict["type"] as! String == "specialRequest" {
                
                
                // working
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let destinationController = storyboard.instantiateViewController(withIdentifier:"NotificationForSpecialOrderId") as? NotificationForSpecialOrder
                destinationController?.dictOfNotificationPopup = notification.request.content.userInfo as NSDictionary
                let frontNavigationController = UINavigationController(rootViewController: destinationController!)
                
                let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC
                
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.rearViewController = rearViewController
                mainRevealController.frontViewController = frontNavigationController
                
                DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                }
                
                // window?.rootViewController = mainRevealController
                window?.makeKeyAndVisible()
                
                
            } else if dict["type"] as! String == "foodAccept"  {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                print(notification.request.content.userInfo as NSDictionary)
                
                let destinationController = storyboard.instantiateViewController(withIdentifier:"UPSuccessAndPayNowId") as? UPSuccessAndPayNow
                destinationController?.dictOfNotificationPopup = notification.request.content.userInfo as NSDictionary
                let frontNavigationController = UINavigationController(rootViewController: destinationController!)
                
                let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC
                
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.rearViewController = rearViewController
                mainRevealController.frontViewController = frontNavigationController
                
                DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                }
                
                // window?.rootViewController = mainRevealController
                window?.makeKeyAndVisible()
                
            } else if dict["type"] as! String == "ServiceAccept"  {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                print(notification.request.content.userInfo as NSDictionary)
                
                let destinationController = storyboard.instantiateViewController(withIdentifier:"UPSuccessAndPayNowId") as? UPSuccessAndPayNow
                destinationController?.dictOfNotificationPopup = notification.request.content.userInfo as NSDictionary
                let frontNavigationController = UINavigationController(rootViewController: destinationController!)
                
                let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC
                
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.rearViewController = rearViewController
                mainRevealController.frontViewController = frontNavigationController
                
                DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                }
                
                // window?.rootViewController = mainRevealController
                window?.makeKeyAndVisible()
                
            } else if dict["type"] as! String == "ServiceRequestPayment" {
                /*
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 
                 print(notification.request.content.userInfo as NSDictionary)
                 
                 let destinationController = storyboard.instantiateViewController(withIdentifier:"UPDashboardId") as? UPSuccessAndPayNow
                 destinationController?.dictOfNotificationPopup = notification.request.content.userInfo as NSDictionary
                 let frontNavigationController = UINavigationController(rootViewController: destinationController!)
                 
                 let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC
                 
                 let mainRevealController = SWRevealViewController()
                 
                 mainRevealController.rearViewController = rearViewController
                 mainRevealController.frontViewController = frontNavigationController
                 
                 DispatchQueue.main.async {
                 UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                 }
                 
                 // window?.rootViewController = mainRevealController
                 window?.makeKeyAndVisible()
                 */
                
            } else if dict["type"] as! String == "specialAccept" {
                /*
                 [AnyHashable("userId"): 367, AnyHashable("driverPhone"): 5689562352, AnyHashable("whatYouWant"): Dhdjd, AnyHashable("userName"): user, AnyHashable("google.c.sender.id"): 27735899901, AnyHashable("specialrequestId"): 63, AnyHashable("TIP"): 125, AnyHashable("message"): New special request, AnyHashable("longitude"): 77.06066351177401, AnyHashable("userImage"): /home/expressplusnow/public_html/appbackend/webroot/img/uploads/users/1603183526expressPlusEditProfilePhoto.jpg, AnyHashable("driverName"): ddriver, AnyHashable("deliveryFee"): 6, AnyHashable("google.c.a.e"): 1, AnyHashable("price"): 125, AnyHashable("status"): 1, AnyHashable("driverImage"): /home/expressplusnow/public_html/appbackend/webroot/img/uploads/users/1603183585123269927912595.png, AnyHashable("bookingDate"): 2020-10-22, AnyHashable("address"): Sector 10 Dwarka South West Delhi, AnyHashable("salesTax"): 0, AnyHashable("gcm.message_id"): 1603349220745109, AnyHashable("type"): specialAccept, AnyHashable("aps"): {
                 alert = "New special request";
                 */
                
                
                
                //
                
            } else if dict["type"] as! String == "orderComplete" {
                //
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let destinationController = storyboard.instantiateViewController(withIdentifier:"PDJobRequestId") as? PDJobRequest
                destinationController?.dictOfNotificationPopup = notification.request.content.userInfo as NSDictionary
                let frontNavigationController = UINavigationController(rootViewController: destinationController!)
                
                let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC
                
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.rearViewController = rearViewController
                mainRevealController.frontViewController = frontNavigationController
                
                DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                }
                
                // window?.rootViewController = mainRevealController
                window?.makeKeyAndVisible()
                
            } else {
                
                // c12@mailinator.com
                
                // working
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let destinationController = storyboard.instantiateViewController(withIdentifier:"AllPopupsId") as? AllPopups
                destinationController?.dictOfNotificationPopup = notification.request.content.userInfo as NSDictionary
                let frontNavigationController = UINavigationController(rootViewController: destinationController!)
                
                let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC
                
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.rearViewController = rearViewController
                mainRevealController.frontViewController = frontNavigationController
                
                DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                }
                
                // window?.rootViewController = mainRevealController
                window?.makeKeyAndVisible()
                
                
            }*/
            
        }
        
        
    }
    
    // MARK:- WHEN APP IS IN BACKGROUND - ( after click popup ) -
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("User Info = ",response.notification.request.content.userInfo)
        
        // var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            if person["role"] as! String == "Member" {
                
                
                let dict = response.notification.request.content.userInfo
                print(dict["type"] as Any)
                
                if dict["type"] == nil {
                    
                } else if dict["type"] as! String == "specialAccept" {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)

                    print(response.notification.request.content.userInfo as NSDictionary)
                    
                    let destinationController = storyboard.instantiateViewController(withIdentifier:"PDJobRequestId") as? PDJobRequest
                    destinationController?.dictOfNotificationPopup = response.notification.request.content.userInfo as NSDictionary
                    let frontNavigationController = UINavigationController(rootViewController: destinationController!)

                    let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC

                    let mainRevealController = SWRevealViewController()

                    mainRevealController.rearViewController = rearViewController
                    mainRevealController.frontViewController = frontNavigationController
                
                    DispatchQueue.main.async {
                        UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                    }
                
                // window?.rootViewController = mainRevealController
                    window?.makeKeyAndVisible()
                    
                }
                
            }
        
    }
        
        
        
       /*
        print("User Info = ",response.notification.request.content.userInfo)
        let dict = response.notification.request.content.userInfo
        
        // print(dict["aps"] as! String as Any)
        
        
        // print(dict["type"] as Any)
        
        if dict["type"] == nil {
            
        } else {
            
            if dict["type"] as! String == "ServiceRequest" {
                
                // working
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)

                    let destinationController = storyboard.instantiateViewController(withIdentifier:"NotificationPopupForServiceRequestId") as? NotificationPopupForServiceRequest
                destinationController?.dictOfNotificationPopup = response.notification.request.content.userInfo as NSDictionary
                let frontNavigationController = UINavigationController(rootViewController: destinationController!)

                let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC

                let mainRevealController = SWRevealViewController()

                mainRevealController.rearViewController = rearViewController
                mainRevealController.frontViewController = frontNavigationController
                
                DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                }
                
                // window?.rootViewController = mainRevealController
                window?.makeKeyAndVisible()
                
            } else if dict["type"] as! String == "specialRequest" {
                    
                    
                    // working
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)

                    let destinationController = storyboard.instantiateViewController(withIdentifier:"NotificationForSpecialOrderId") as? NotificationForSpecialOrder
                destinationController?.dictOfNotificationPopup = response.notification.request.content.userInfo as NSDictionary
                    let frontNavigationController = UINavigationController(rootViewController: destinationController!)

                    let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC

                    let mainRevealController = SWRevealViewController()

                    mainRevealController.rearViewController = rearViewController
                    mainRevealController.frontViewController = frontNavigationController
                    
                    DispatchQueue.main.async {
                        UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                    }
                    
                    // window?.rootViewController = mainRevealController
                    window?.makeKeyAndVisible()
                    
                
                   } else if dict["type"] as! String == "foodAccept"  {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)

                    print(response.notification.request.content.userInfo as NSDictionary)
                    
                    let destinationController = storyboard.instantiateViewController(withIdentifier:"UPSuccessAndPayNowId") as? UPSuccessAndPayNow
                    destinationController?.dictOfNotificationPopup = response.notification.request.content.userInfo as NSDictionary
                    let frontNavigationController = UINavigationController(rootViewController: destinationController!)

                    let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC

                    let mainRevealController = SWRevealViewController()

                    mainRevealController.rearViewController = rearViewController
                    mainRevealController.frontViewController = frontNavigationController
                
                    DispatchQueue.main.async {
                        UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                    }
                
                // window?.rootViewController = mainRevealController
                    window?.makeKeyAndVisible()
                    
                   } else if dict["type"] as! String == "ServiceAccept"  {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)

                    print(response.notification.request.content.userInfo as NSDictionary)
                    
                    let destinationController = storyboard.instantiateViewController(withIdentifier:"UPSuccessAndPayNowId") as? UPSuccessAndPayNow
                    destinationController?.dictOfNotificationPopup = response.notification.request.content.userInfo as NSDictionary
                    let frontNavigationController = UINavigationController(rootViewController: destinationController!)

                    let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC

                    let mainRevealController = SWRevealViewController()

                    mainRevealController.rearViewController = rearViewController
                    mainRevealController.frontViewController = frontNavigationController
                
                    DispatchQueue.main.async {
                        UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                    }
                
                // window?.rootViewController = mainRevealController
                    window?.makeKeyAndVisible()
                    
                   } else if dict["type"] as! String == "ServiceRequestPayment" {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)

                    print(response.notification.request.content.userInfo as NSDictionary)
                    
                    let destinationController = storyboard.instantiateViewController(withIdentifier:"UPDashboardId") as? UPSuccessAndPayNow
                    destinationController?.dictOfNotificationPopup = response.notification.request.content.userInfo as NSDictionary
                    let frontNavigationController = UINavigationController(rootViewController: destinationController!)

                    let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC

                    let mainRevealController = SWRevealViewController()

                    mainRevealController.rearViewController = rearViewController
                    mainRevealController.frontViewController = frontNavigationController
                
                    DispatchQueue.main.async {
                        UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                    }
                
                // window?.rootViewController = mainRevealController
                    window?.makeKeyAndVisible()
                    
                   }  else {
                
                // working
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)

                    let destinationController = storyboard.instantiateViewController(withIdentifier:"AllPopupsId") as? AllPopups
                    destinationController?.dictOfNotificationPopup = response.notification.request.content.userInfo as NSDictionary
                    let frontNavigationController = UINavigationController(rootViewController: destinationController!)

                    let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC
                    
                    let mainRevealController = SWRevealViewController()

                    mainRevealController.rearViewController = rearViewController
                    mainRevealController.frontViewController = frontNavigationController
                
                    DispatchQueue.main.async {
                        UIApplication.shared.keyWindow?.rootViewController = mainRevealController
                    }
                
                // window?.rootViewController = mainRevealController
                    window?.makeKeyAndVisible()
                
                   }
            
        }
        */
        
        
        
        completionHandler()
        
    }

}

/*
 func latLong(lat: Double,long: Double)  {

     let geoCoder = CLGeocoder()
     let location = CLLocation(latitude: lat , longitude: long)
     geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in

         print("Response GeoLocation : \(placemarks)")
         var placeMark: CLPlacemark!
         placeMark = placemarks?[0]

         // Country
         if let country = placeMark.addressDictionary!["Country"] as? String {
             print("Country :- \(country)")
             // City
             if let city = placeMark.addressDictionary!["City"] as? String {
                 print("City :- \(city)")
                 // State
                 if let state = placeMark.addressDictionary!["State"] as? String{
                     print("State :- \(state)")
                     // Street
                     if let street = placeMark.addressDictionary!["Street"] as? String{
                         print("Street :- \(street)")
                         let str = street
                         let streetNumber = str.components(
                             separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                         print("streetNumber :- \(streetNumber)" as Any)

                         // ZIP
                         if let zip = placeMark.addressDictionary!["ZIP"] as? String{
                             print("ZIP :- \(zip)")
                             // Location name
                             if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                                 print("Location Name :- \(locationName)")
                                 // Street address
                                 if let thoroughfare = placeMark?.addressDictionary!["Thoroughfare"] as? NSString {
                                 print("Thoroughfare :- \(thoroughfare)")

                                 }
                             }
                         }
                     }
                 }
             }
         }
     })
 }
 */
