//
//  RAfterRegistration.swift
//  ExpressPlus
//
//  Created by Apple on 08/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

// MARK:- LOCATION -
import CoreLocation

import Alamofire

class RAfterRegistration: UIViewController, CLLocationManagerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate {

    let regularFont = UIFont.systemFont(ofSize: 16)
    let boldFont = UIFont.boldSystemFont(ofSize: 16)
    
    var dictGetRegistrationData:NSDictionary!
    
    let locationManager = CLLocationManager()
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    // MARK:- ADDRESS STRING -
    var strSaveAddress:String!
    
    var imageStrOne:String!
    var imageStrTwo:String!
    
    var strPassImgString1:String!
    var imageStr1:String!
    var imgData1:Data!
    
    var strPassImgString2:String!
    var imageStr2:String!
    var imgData2:Data!
    
    @IBOutlet weak var navigationBar:UIView! {
           didSet {
               navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
           }
       }
       
       @IBOutlet weak var lblNavigationTitle:UILabel! {
           didSet {
               lblNavigationTitle.text = "COMPLETE PROFILE"
           }
       }
       
       @IBOutlet weak var btnBack:UIButton! {
           didSet {
               btnBack.setTitle("|||", for: .normal)
           }
       }
    @IBOutlet weak var uploadRestaurantImage:UIView! {
        didSet {
            uploadRestaurantImage.backgroundColor = .white
            uploadRestaurantImage.layer.cornerRadius = 6
            uploadRestaurantImage.clipsToBounds = true
            uploadRestaurantImage.addDashedBorder()
        }
    }
    @IBOutlet weak var uploadRestaurantLogo:UIView! {
        didSet {
            uploadRestaurantLogo.backgroundColor = .white
            uploadRestaurantLogo.layer.cornerRadius = 6
            uploadRestaurantLogo.clipsToBounds = true
            uploadRestaurantLogo.addDashedBorder()
        }
    }
    
    @IBOutlet weak var imgOne:UIImageView!
    @IBOutlet weak var imgTwo:UIImageView!
    
    @IBOutlet weak var imgOneShow:UIImageView! {
        didSet {
            imgOneShow.isHidden = true
        }
    }
    @IBOutlet weak var imgTwoShow:UIImageView! {
        didSet {
            imgTwoShow.isHidden = true
        }
    }
    @IBOutlet weak var btnSaveAndContinue:UIButton! {
        didSet {
            btnSaveAndContinue.layer.cornerRadius = 6
            btnSaveAndContinue.clipsToBounds = true
            btnSaveAndContinue.setTitle("Save & Continue", for: .normal)
            btnSaveAndContinue.setTitleColor(.white, for: .normal)
            btnSaveAndContinue.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var txtAddress:UITextField! {
        didSet {
            txtAddress.layer.cornerRadius = 6
            txtAddress.clipsToBounds = true
            txtAddress.backgroundColor = .white
        }
    }
    @IBOutlet weak var txtType:UITextField! {
        didSet {
            txtType.layer.cornerRadius = 6
            txtType.clipsToBounds = true
            txtType.backgroundColor = .white
        }
    }
    @IBOutlet weak var lblCountryName:UILabel! {
        didSet {
            lblCountryName.text = ""
        }
    }
    @IBOutlet weak var btnRestaurantType:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        
        btnRestaurantType.addTarget(self, action: #selector(openGenderPickerAction), for: .touchUpInside)
        
        self.btnSaveAndContinue.addTarget(self, action: #selector(saveAndContinueClickMethod), for: .touchUpInside)
        
        
        // print(dictGetRegistrationData as Any)
        
        /*
        var imageStrOne:String!
        var imageStrTwo:String!
        */
        
        self.txtAddress.delegate = self
        
        self.imageStrOne = "0"
        self.imageStrTwo = "0"
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(RAfterRegistration.cellTappedMethod1(_:)))

        imgOne.isUserInteractionEnabled = true
        // imgProfile.tag = indexPath.row
        imgOne.addGestureRecognizer(tapGestureRecognizer1)
        
        
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(RAfterRegistration.cellTappedMethod2(_:)))

        imgTwo.isUserInteractionEnabled = true
        // imgProfile.tag = indexPath.row
        imgTwo.addGestureRecognizer(tapGestureRecognizer2)
        
        
        
        
        self.iAmHereForLocationPermission()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @objc func saveAndContinueClickMethod() {
        if imgOneShow.isHidden == true {
            let alert = UIAlertController(title: String("Error!"), message: String("Please select your Restaurant Cover image."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
        } else if imgTwoShow.isHidden == true {
            let alert = UIAlertController(title: String("Error!"), message: String("Please select your Restaurant Logo."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            // push
            // PDPaymentInformation
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDPaymentInformationId") as? PDPaymentInformation
            push!.fromRestaurantPage = "afterRegistrationOfRestaurant"
            push!.getImgDataOne = imgData1
            push!.getImgDataTwo = imgData2
            
            /*
            var getAddress:String!
            var getLat:String!
            var getLong:String!
            var getCountry:String!
            */
            
            /*
             self.strSaveCountryName     = country
             self.strSaveStateName       = city
             self.strSaveZipcodeName     = zipcode
             
             self.strSaveLocalAddress     = localAddress
             self.strSaveLocality         = locality
             self.strSaveLocalAddressMini = localAddressMini
             
             let doubleLat = locValue.latitude
             let doubleStringLat = String(doubleLat)
             
             let doubleLong = locValue.latitude
             let doubleStringLong = String(doubleLong)
             
             self.strSaveLatitude = String(doubleStringLat)
             self.strSaveLongitude = String(doubleStringLong)
             */
            
            // let addressIs:String = String(self.strSaveLocality)+" "+String(self.self.strSaveLocalAddress)
            
            push!.getCountry    = self.strSaveCountryName
            push!.getLat        = String(self.strSaveLatitude)
            push!.getLong       = String(self.strSaveLongitude)
            push!.getAddress    = String(self.strSaveLocality)+" "+String(self.self.strSaveLocalAddress)
            push!.getFoodTag = self.txtType.text
            
            
            self.navigationController?.pushViewController(push!, animated: true)
        }
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
                // self.iAmHereForLocationPermission()
                
            @unknown default:
                break
            }
        }
    }
    @objc func openGenderPickerAction(_ sender: UIButton) {
        
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RAddMenuTableCell
        
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "Please Select Food Type",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : sender.backgroundColor,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "Search Food Type",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : sender.backgroundColor,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        let arrGender = [ "Select Restaurant Type","Veg","Non-Veg","none"]
        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if let selectedValue = selectedValues.first{
                                            if selectedValue == arrGender.last!{
                                                // self.btnGenderPicker.setTitle("I'm a human", for: .normal)
                                                // cell.txtFoodType.text = ""
                                                self.txtType.text = "\(selectedValue)"
                                                self.txtType.textColor = .black
                                                
                                                
                                            } else {
                                                // self.btnGenderPicker.setTitle("I'm \(selectedValue)", for: .normal)
                                                // cell.txtFoodType.text = "\(selectedValue)"
                                                self.txtType.text = "\(selectedValue)"
                                                self.txtType.textColor = .black
                                                
                                                
                                            }
                                        } else {
                                            // self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                                            // cell.txtFoodType.text = "Please select type"
                                            self.txtType.text = "Please select type"
                                        }
        },
                                       onCancel: {
                                        print("Cancelled")
        }
        )
        
        picker.show(withAnimation: .FromBottom)
    }
    // MARK:- GET CUSTOMER LOCATION
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            print("locality ==> "+locality as Any) // sector 10 dwarka
            
            // print(self.strSaveCountryName as Any) // india
            // print(self.strSaveStateName as Any) // new delhi
            // print(self.strSaveZipcodeName as Any) // 110075
            
            let addressIs:String = String(self.strSaveLocality)+" "+String(self.self.strSaveLocalAddress)
            self.txtAddress.text = String(addressIs)
            
            self.lblCountryName.text = self.strSaveCountryName
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            
            
            // self.findMyStateTaxWB()
        }
    }
    
    
    @objc func cellTappedMethod1(_ sender:AnyObject){
         print("you tap image number: \(sender.view.tag)")
        
        let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera1()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery1()
        }))

        alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    @objc func openCamera1() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        self.imageStrTwo = "0"
        self.imageStrOne = "1"
        
    }
    
    @objc func openGallery1() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        self.imageStrTwo = "0"
        self.imageStrOne = "1"
    }
    
    @objc func cellTappedMethod2(_ sender:AnyObject){
         print("you tap image number: \(sender.view.tag)")
        
        let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera2()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery2()
        }))

        alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    @objc func openCamera2() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        self.imageStrOne = "0"
        self.imageStrTwo = "1"
    }
    
    @objc func openGallery2() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        self.imageStrOne = "0"
        self.imageStrTwo = "1"
    }
    
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RAddMenuTableCell
        
        // cell.imgPhoto.isHidden = false
        
        if imageStrOne == "1" {
            imgOneShow.isHidden = false
            let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            imgOneShow.image = image_data // show image on profile
            let imageData:Data = image_data!.pngData()!
            imageStr1 = imageData.base64EncodedString()
            self.dismiss(animated: true, completion: nil)
            imgData1 = image_data!.jpegData(compressionQuality: 0.2)!
            //print(type(of: imgData)) // data
            
            // self.strPassImgString1 = "1"
        } else if imageStrTwo == "1" {
            imgTwoShow.isHidden = false
            let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            imgTwoShow.image = image_data // show image on profile
            let imageData:Data = image_data!.pngData()!
            imageStr2 = imageData.base64EncodedString()
            self.dismiss(animated: true, completion: nil)
            imgData2 = image_data!.jpegData(compressionQuality: 0.2)!
            //print(type(of: imgData)) // data
            
            // self.strPassImgString1 = "1"
        }
        
        
        // self.uploadWithImage()
    }
    
}
