//
//  RManageMenuCollectionCell.swift
//  ExpressPlus
//
//  Created by Apple on 29/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class RManageMenuCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imgFoodProfile:UIImageView! {
        didSet {
            imgFoodProfile.backgroundColor = .clear
        }
    }
    @IBOutlet weak var imgVegNonveg:UIImageView!  {
           didSet {
               imgVegNonveg.backgroundColor = .clear
           }
       }
    
    @IBOutlet weak var lblFoodName:UILabel!
    @IBOutlet weak var lblOldPrice:UILabel!
    @IBOutlet weak var lblRealPrice:UILabel!
}
