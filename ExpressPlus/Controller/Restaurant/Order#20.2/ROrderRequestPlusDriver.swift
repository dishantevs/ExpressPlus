//
//  ROrderRequestPlusDriver.swift
//  ExpressPlus
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ROrderRequestPlusDriver: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    var dictOrderDetails:NSDictionary!
    
    var locationManager:CLLocationManager!
    // var mapView:MKMapView!
    
    @IBOutlet weak var mapView:MKMapView!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "ORDER LIST"
        }
    }
    
    @IBOutlet weak var btnInProgress:UIButton! {
        didSet {
            btnInProgress.backgroundColor = UIColor.init(red: 217.0/255.0, green: 91.0/255.0, blue: 49.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var btnDeliverd:UIButton! {
        didSet {
            btnDeliverd.backgroundColor = UIColor.init(red: 217.0/255.0, green: 91.0/255.0, blue: 49.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var btnRequestPlusDriver:UIButton! {
        didSet {
            btnRequestPlusDriver.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnRequestPlusDriver.setTitleColor(.white, for: .normal)
            btnRequestPlusDriver.setTitle("Request plus driver to Pickup", for: .normal)
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblTotalAmount:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblWorkAndLandmark:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnRequestPlusDriver.addTarget(self, action: #selector(pushToEarningPage), for: .touchUpInside)
        btnInProgress.addTarget(self, action: #selector(backClickMehod), for: .touchUpInside)
        
        self.btnBack.addTarget(self, action: #selector(backClickMehod), for: .touchUpInside)
        // print(dictOrderDetails as Any)
        /*
         Optional({
             EstTime = "15 Minutes";
             Mile = "4 Mile";
             TIP = "0.75";
             address = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
             assignDriver = 113;
             cardNo = 4242424242424242;
             city = Ghaziabad;
             couponCode = "";
             created = "2020-05-19 17:40:00";
             deliveryFee = "";
             deliveryLat = "28.6634703";
             deliveryLong = "77.3240304";
             discount = 0;
             driverAVG = 0;
             driverContactNumber = 2356898523;
             driverId = 113;
             driverImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1589802893IMG-20200518-WA0003.jpg";
             driverName = "driver evs";
             foodDetails =     (
                         {
                     id = 43;
                     name = gagsgs;
                     price = "15.00";
                     quantity = 1;
                     resturentId = 99;
                 }
             );
             foodDetailsNew =     (
                         {
                     categoryId = "";
                     description = "";
                     foodName = "";
                     foodTag = "";
                     foodType = "";
                     "image_1" = "";
                     "image_2" = "";
                     "image_3" = "";
                     "image_4" = "";
                     "image_5" = "";
                     menuId = "";
                     price = "";
                     quantity = 1;
                     resturentId = "";
                     specialPrice = "";
                 }
             );
             foodId = "";
             foodorderId = 70;
             landmark = hotel;
             name = purnima;
             noContact = "";
             phone = 5623852369;
             resturentAddress = "Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India";
             resturentId = 99;
             resturentLatitude = "28.5870717";
             resturentLongitude = "77.0605306";
             resturentName = "restaurent 2d";
             specialNote = "test y";
             state = "Uttar Pradesh";
             status = 4;
             storecity = "";
             totalAmount = "15.75";
             userAVG = 0;
             userId = 114;
             userName = kiwi;
             whatYouWant = "";
             workPlace = Home;
             zipcode = 201011;
         })
         (lldb)
         */
        
        self.lblName.text               = (dictOrderDetails["name"] as! String)
        self.lblTotalAmount.text        = "$ "+(dictOrderDetails["totalAmount"] as! String)
        self.lblAddress.text            = (dictOrderDetails["address"] as! String)
        self.lblWorkAndLandmark.text    = (dictOrderDetails["workPlace"] as! String)+" "+(dictOrderDetails["landmark"] as! String)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create and Add MapView to our main view
        createMapView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        determineCurrentLocation()
    }
    func createMapView() {
        // mapView = MKMapView()
        
        // let leftMargin:CGFloat = 10
        // let topMargin:CGFloat = 60
        // let mapWidth:CGFloat = view.frame.size.width-20
        // let mapHeight:CGFloat = 300
        
        // mapView.frame = CGRectMake(leftMargin, topMargin, mapWidth, mapHeight)
        // mapView.frame = CGRect(x: leftMargin, y: topMargin, width: mapWidth, height: mapHeight)
        
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        // Or, if needed, we can position map in the center of the view
        mapView.center = view.center
        
        view.addSubview(mapView)
    }
    func determineCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            //locationManager.startUpdatingHeading()
            locationManager.startUpdatingLocation()
        }
    }
    // private func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        //manager.stopUpdatingLocation()
        
        let myDouble1 = Double(dictOrderDetails["deliveryLat"] as! String)
        let myDouble2 = Double(dictOrderDetails["deliveryLong"] as! String)
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: myDouble1!, longitudeDelta: myDouble2!))
        
        mapView.setRegion(region, animated: true)
        
        // Drop a pin at user's Current Location
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
        myAnnotation.title = (dictOrderDetails["name"] as! String)
        mapView.addAnnotation(myAnnotation)
    }
    
    private func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error \(error)")
    }
    
    @objc func backClickMehod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func pushToEarningPage() {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TrackYourOrderId") as? TrackYourOrder
        
         let x : Int = dictOrderDetails["foodorderId"] as! Int
         let myString = String(x)
         
        settingsVCId!.strFoodOrderId = String(myString)
        
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
}
