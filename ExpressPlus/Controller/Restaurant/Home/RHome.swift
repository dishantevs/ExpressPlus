//
//  RHome.swift
//  ExpressPlus
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class RHome: UIViewController {

    let cellReuseIdentifier = "rHomeCollectionCell"
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
    
    var page : Int! = 1
    var loadMore : Int! = 1;
    
    @IBOutlet weak var navigationBar:UIView! {
          didSet {
              navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
          }
      }
      
      @IBOutlet weak var lblNavigationTitle:UILabel! {
          didSet {
              lblNavigationTitle.text = "HOME"
          }
      }
      
      @IBOutlet weak var btnBack:UIButton! {
          didSet {
              btnBack.setTitle("|||", for: .normal)
          }
      }
      
    @IBOutlet weak var clView:UICollectionView! {
        didSet {
            // clView.delegate = self
            // clView.dataSource = self
        }
    }
      
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var lblRestaurantName:UILabel!
    
    // MARK:- THIS IS PLUS BUTTON -
    @IBOutlet weak var btnCart:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnCart.addTarget(self, action: #selector(cartClickMethod), for: .touchUpInside)
        
        self.sideBarMenuClick()
        
        // Access Shared Defaults Object
        let userDefaults = UserDefaults.standard
        let myString = userDefaults.string(forKey: "forMenuAfterReg")
        // userDefaults.set("menuReg", forKey: "forMenuAfterReg")
        if myString == "menuReg" {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RAddMenuId") as? RAddMenu
            self.navigationController?.pushViewController(push!, animated: false)
            
        } else {
            // oops
        }
        
        
        
        
        /*
        let defaults = UserDefaults.standard
        defaults.setValue("", forKey: "keyLoginFullData")
        defaults.setValue(nil, forKey: "keyLoginFullData")
        */
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
            
            self.lblRestaurantName.text = (person["fullName"] as! String)
            self.imgProfile.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "avatar"))
            
             self.menuListWB()
        } else {
            self.restaurantLoginWB()
        }
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    @objc func cartClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RAddMenuId") as? RAddMenu
        // push.getRestaurantDetails = 
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    
    
    // MARK:- RESTAURANT LOGIN -
    @objc func restaurantLoginWB() {
           
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
                  
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        /*
         [action] => login
         [email] => purnimac@gmail.com
         [password] => 123456
         */
        
           var parameters:Dictionary<AnyHashable, Any>!
    
                      parameters = [
                          "action"             : "login",
                          "email"              : String("restaurent@gmail.com"),
                          "password"           : String("123456")
                          
                ]
                 
                      print("parameters-------\(String(describing: parameters))")
                      
                      Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                              response in
                  
                              switch(response.result) {
                              case .success(_):
                                 if let data = response.result.value {

                                  let JSON = data as! NSDictionary
                                   // print(JSON as Any)
                                  
                                    /*
                                     AccountHolderName = jjsjs;
                                     AccountNo = 8505858545884555;
                                     AutoInsurance = "";
                                     BankName = djdgsgs;
                                     RoutingNo = vstvush6sg6;
                                     accountType = Saving;
                                     address = "Gwalior, Madhya Pradesh 474008, India";
                                     companyBackground = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1587560500Corona.png";
                                     contactNumber = 9494645544;
                                     country = India;
                                     device = "";
                                     deviceToken = "";
                                     dob = "";
                                     drivlingImage = "";
                                     email = "restaurent@gmail.com";
                                     firebaseId = "";
                                     foodTag = Veg;
                                     fullName = restaurent;
                                     gender = "";
                                     image = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1587560500Corona.png";
                                     lastName = "";
                                     latitude = "26.2313245";
                                     logitude = "78.1694957";
                                     longitude = "78.1694957";
                                     middleName = "";
                                     role = Restaurant;
                                     socialId = "";
                                     socialType = "";
                                     ssnImage = "";
                                     state = "";
                                     userId = 99;
                                     wallet = 0;
                                     zipCode = "";
                                     */
                                    
                                  var strSuccess : String!
                                  strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                                  if strSuccess == String("success") {
                                   print("yes")
                                    
                                    // ERProgressHud.sharedInstance.hide()
                                    
                                    
                                    var dict: Dictionary<AnyHashable, Any>
                                    dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                    self.lblRestaurantName.text = (dict["fullName"] as! String)
                                    self.imgProfile.sd_setImage(with: URL(string: (dict["image"] as! String)), placeholderImage: UIImage(named: "avatar"))
                                    
                                    let defaults = UserDefaults.standard
                                    defaults.setValue(dict, forKey: "keyLoginFullData")
                                    
                                    self.menuListWB()
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                        }
        }
    }
    
    // MARK:- MENU LIST WEBSERVICE -
    @objc func menuListWB() {
        self.arrListOfAllMyOrders.removeAllObjects()
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
                  
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        /*
         [action] => menulist
         [restaurantId] => 99
         [userId] => 99
         [categoryId] =>
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
    // print(person as Any)
        
        let strMyId:String!
        let x : Int = person["userId"] as! Int
        strMyId = String(x)
        
            parameters = [
                "action"           : "menulist",
                "restaurantId"     : String(strMyId),
                "userId"           : String(strMyId),
                "categoryId"       : String("")
            ]
        }
        print("parameters-------\(String(describing: parameters))")
                      
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
                  
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary;
                    print(JSON as Any)
                                  
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                    if strSuccess == String("success") {
                        print("yes")
                                    
                        ERProgressHud.sharedInstance.hide()
                                    
                        var ar : NSArray!
                        ar = (JSON["data"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                    
                        self.clView.delegate = self
                        self.clView.dataSource = self
                        self.clView.reloadData()
                        self.loadMore = 1;
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                };

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                                  
                ERProgressHud.sharedInstance.hide()
                                  
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                                  
                alertController.addAction(okAction)
                                  
                self.present(alertController, animated: true, completion: nil)
                                  
                break
            }
        }
    }
}

//MARK:- COLLECTION VIEW
extension RHome: UICollectionViewDelegate {
    //Write Delegate Code Here
    
}

extension RHome: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "rHomeCollectionCell", for: indexPath as IndexPath) as! RHomeCollectionCell
           cell.backgroundColor = UIColor.white
           cell.layer.borderWidth = 0.5
           cell.layer.borderColor = UIColor.black.cgColor
           cell.layer.borderWidth = 0.70
           
           cell.contentView.layer.cornerRadius = 2.0
           cell.contentView.layer.borderWidth = 1.0
           cell.contentView.layer.borderColor = UIColor.clear.cgColor
           cell.contentView.layer.masksToBounds = true

           cell.layer.shadowColor = UIColor.darkGray.cgColor
           cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
           cell.layer.shadowRadius = 2.0
           cell.layer.shadowOpacity = 0.5
           cell.layer.masksToBounds = false
           cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
           
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        
        /*
         categoryId = 67;
         categoryName = Dinner;
         created = "2020-04-22 18:32:00";
         description = vahahs;
         foodName = gagsgs;
         foodTag = "";
         "image_1" = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/foods/1587560546Corona.png";
         "image_2" = "";
         "image_3" = "";
         "image_4" = "";
         "image_5" = "";
         menuId = 43;
         price = 28;
         quantity = 0;
         resturentId = 99;
         resturentName = restaurent;
         specialPrice = 15;
         */
        
        cell.lblFoodName.text = (item!["foodName"] as! String)

        if item!["foodTag"] as! String == "Veg" {
            cell.imgVegNonveg.image = UIImage(named: "veg")
        } else if item!["foodTag"] as! String == "" {
            cell.imgVegNonveg.image = UIImage(named: "veg")
        } else {
            cell.imgVegNonveg.image = UIImage(named: "nonVeg")
        }
        
        let x : Int = item!["price"] as! Int
        let myString = String(x)
        cell.lblOldPrice.text = "$ "+String(myString)
        
        let x2 : Int = item!["specialPrice"] as! Int
        let myString2 = String(x2)
        cell.lblRealPrice.text = "$ "+String(myString2)
        
      
        cell.imgFoodProfile.sd_setImage(with: URL(string: (item!["image_1"] as! String)), placeholderImage: UIImage(named: "avatar"))
        
              
        return cell
    }
    
    //Write DataSource Code Here
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension RHome: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var sizes: CGSize
           
           let result = UIScreen.main.bounds.size
               
           // NSLog("%f",result.height)
        
           if result.height == 480 {
               //Load 3.5 inch xib
               sizes = CGSize(width: 170.0, height: 190.0)
           }
           else if result.height == 568 {
               //Load 4 inch xib
               sizes = CGSize(width: 100.0, height: 80.0)
           }
           else if result.height == 667.000000 {
               //Load 4.7 inch xib , 8
               sizes = CGSize(width: 160.0, height: 190.0) // done
           }
           else if result.height == 736.000000 {
               // iphone 6s Plus and 7 Plus
               sizes = CGSize(width: 180.0, height: 190.0) // done
           }
           else if result.height == 812.000000 {
               // iphone X , 11 pro
               sizes = CGSize(width: 180.0, height: 190.0) // done
           }
           else if result.height == 896.000000 {
               // iphone Xr ,11, 11 pro max
               sizes = CGSize(width: 180.0, height: 190.0) // done
           }
           else {
               sizes = CGSize(width: 114.0, height: 114.0)
           }
        
           return sizes
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
    }
}
