//
//  RTotalEarnings.swift
//  ExpressPlus
//
//  Created by Apple on 10/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class RTotalEarnings: UIViewController {

    let cellReuseIdentifier = "rTotalEarningsTableCell"
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
    
    var arrGetCategoryName:NSMutableArray! = []
    var arrGetCategoryId:NSMutableArray! = []
    
    var page : Int! = 1
    var loadMore : Int! = 1
    
    @IBOutlet weak var myEarning:UILabel!
    @IBOutlet weak var serviceCompleted:UILabel!
    
    @IBOutlet weak var navigationBar:UIView! {
           didSet {
               navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
           }
       }
       @IBOutlet weak var lblNavigationTitle:UILabel! {
           didSet {
               lblNavigationTitle.text = "EARNING"
           }
       }
       
    @IBOutlet weak var btnCashout:UIButton! {
        didSet {
            btnCashout.setTitle("CASHOUT", for: .normal)
            btnCashout.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnCashout.setTitleColor(.white, for: .normal)
            btnCashout.layer.cornerRadius = 20
            btnCashout.clipsToBounds = true
        }
    }
    
       @IBOutlet weak var btnBack:UIButton! {
           didSet {
               btnBack.setTitle("|||", for: .normal)
           }
       }
       @IBOutlet weak var tbleView: UITableView! {
           didSet {
               // self.tbleView.delegate = self
               // self.tbleView.dataSource = self
               self.tbleView.backgroundColor = .clear
               // self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
               self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
               // self.tbleView.separatorStyle = UITableViewCell.SeparatorStyle.none
           }
       }
    
    @IBOutlet weak var segmentContolls:UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        btnBack.setImage(UIImage(named: "sMenu"), for: .normal)
        //btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        sideBarMenuClick()
        btnCashout.addTarget(self, action: #selector(cashoutClickMethod), for: .touchUpInside)
        
        segmentContolls.addTarget(self, action: #selector(segmControlClickMethod), for: .valueChanged)
        
        if let myLoadedString = UserDefaults.standard.string(forKey: "keyFoodOrderEarnings") {
            // print(myLoadedString) // "foodOrderEarningCheck"
            
            // toGetEarningCheck
            
            /*
             [action] => specialearning
             [userId] => 292
             [usertype] => Driver
             [dateBy] => Today
             */
            if myLoadedString == "foodOrderEarningCheck" {
                self.totalEarningClickMethod(strActionName: "foodorderlist", strDateBy: "Today", strStatus: "4")
            } else if myLoadedString == "searviceEarningCheck" {
                self.totalEarningClickMethod(strActionName: "bookinglist", strDateBy: "Today", strStatus: "2")
            } else if myLoadedString == "toGetEarningCheck" {
                self.totalEarningClickMethod(strActionName: "gogetlist", strDateBy: "Today", strStatus: "2")
            } else if myLoadedString == "specialOrderEarningCheck" {
                self.earningForSpecialOrder(strActionName: "specialrequestlist", strDateBy: "Today", strStatus: "2")
            }
            
        }
        
        
    }
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    @objc func cashoutClickMethod() {
        let userDefaults = UserDefaults.standard
        userDefaults.set("back", forKey: "backOrMenu")
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RCashoutId") as? RCashout
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    @objc func segmControlClickMethod() {
        
        if let myLoadedString = UserDefaults.standard.string(forKey: "keyFoodOrderEarnings") {
            
        if myLoadedString == "foodOrderEarningCheck" {
            
            /*
             [action] => foodorderlist
             [userId] => 123
             [userType] => Driver
             [status] => 4
             [dateBy] => Today
             [pageNo] => 1
             */
            
            if segmentContolls.selectedSegmentIndex == 0 {
                self.totalEarningClickMethod(strActionName: "foodorderlist", strDateBy: "Today", strStatus: "4")
            } else if segmentContolls.selectedSegmentIndex == 1 {
                self.totalEarningClickMethod(strActionName: "foodorderlist", strDateBy: "Weekly", strStatus: "4")
            }
            
        } else if myLoadedString == "searviceEarningCheck" {
            
            /*
             [action] => bookinglist
             [userId] => 123
             [userType] => Driver
             [status] => 2
             [dateBy] => Weekly
             [pageNo] => 1
             */
            
            if segmentContolls.selectedSegmentIndex == 0 {
                self.totalEarningClickMethod(strActionName: "bookinglist", strDateBy: "Today", strStatus: "2")
            } else if segmentContolls.selectedSegmentIndex == 1 {
                self.totalEarningClickMethod(strActionName: "bookinglist", strDateBy: "Weekly", strStatus: "2")
            }
            
        } else if myLoadedString == "toGetEarningCheck" {
            
            /*
             [action] => gogetlist
             [userId] => 123
             [userType] => Driver
             [status] => 2
             [dateBy] => Weekly
             [pageNo] => 1
             */
            
            if segmentContolls.selectedSegmentIndex == 0 {
                self.totalEarningClickMethod(strActionName: "gogetlist", strDateBy: "Today", strStatus: "2")
            } else if segmentContolls.selectedSegmentIndex == 1 {
                self.totalEarningClickMethod(strActionName: "gogetlist", strDateBy: "Weekly", strStatus: "2")
            }
            
            } else if myLoadedString == "specialOrderEarningCheck" {
            
            
            
            
            
            
            if segmentContolls.selectedSegmentIndex == 0 {
                self.earningForSpecialOrder(strActionName: "specialrequestlist", strDateBy: "Today", strStatus: "2")
            } else if segmentContolls.selectedSegmentIndex == 1 {
                self.earningForSpecialOrder(strActionName: "specialrequestlist", strDateBy: "Weekly", strStatus: "2")
            }
            
            
            
            }
        
        }
        
        
    }
    
    
    
    @objc func earningForSpecialOrder(strActionName:String,strDateBy:String,strStatus:String) {
        
        self.arrListOfAllMyOrders.removeAllObjects()
            
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
            let urlString = BASE_URL_EXPRESS_PLUS
         
            var parameters:Dictionary<AnyHashable, Any>!
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                
                /*
                 [action] => bookinglist
                 [userId] => 123
                 [userType] => Driver
                 [status] => 2 is service and 4 is food earning
                 [dateBy] => Weekly
                 [pageNo] => 1
                 */
                
                 let x : Int = person["userId"] as! Int
                 let myString = String(x)
                
                       parameters = [
                           "action"     : String(strActionName),
                           "userId"     : String(myString),
                           "userType"   : person["role"] as! String,
                           "status"     : String(strStatus),
                           "dateBy"     : String(strDateBy),
                           // "pageNo"     : String("1")
                ]
            }
            print("parameters-------\(String(describing: parameters))")
                       
            Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                {
                    response in
                   
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value {

                            let JSON = data as! NSDictionary
                            print(JSON as Any)
                                    
                                   /*
                                     AVGRating = "";
                                     TIP = 10;
                                     address = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
                                     bookingDate = "21st May";
                                     bookingId = 227;
                                     contactNumber = 5623000852;
                                     created = "2020-05-20 18:46:00";
                                     email = "meen@mailinator.com";
                                     fullName = kiwi;
                                     groupId = 43;
                                     image = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1589889715IMG-20200519-WA0003.jpg";
                                     serviceAmount = 100;
                                     serviceName = "Yard Work";
                                     servicePrice = 100;
                                     slote = "22:0-21:16";
                                     status = 2;
                                     totalAmount = 110;
                                     userAddress = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
                                     userId = 114;
                                     zipCode = "";
                                     */
                                    
                                   var strSuccess : String!
                                   strSuccess = JSON["status"]as Any as? String
                                   
                                     // var strSuccessAlert : String!
                                     // strSuccessAlert = JSON["msg"]as Any as? String
                                   
                                   if strSuccess == String("success") {
                                    print("yes")
                                     
                                    var ar : NSArray!
                                    ar = (JSON["data"] as! Array<Any>) as NSArray
                                    self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                    
                                    
                                    if let myLoadedString = UserDefaults.standard.string(forKey: "keyFoodOrderEarnings") {
                                        if myLoadedString == "foodOrderEarningCheck" {
                                            self.getEarning(strDateBy2: strDateBy, getEarning: "getearningfood")
                                        } else if myLoadedString == "searviceEarningCheck" {
                                            self.getEarning(strDateBy2: strDateBy, getEarning: "getearning")
                                        } else if myLoadedString == "toGetEarningCheck" {
                                            self.getEarning(strDateBy2: strDateBy, getEarning: "togetearning")
                                        } else if myLoadedString == "specialOrderEarningCheck" {
                                        
                                            self.getEarning(strDateBy2: strDateBy, getEarning: "specialearning")
                                            
                                        }
                                    }
                                    
                                    
                                      ERProgressHud.sharedInstance.hide()
                                    
                                    // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDJobRequestId") as? PDJobRequest
                                    // settingsVCId!.dictGetDetails = dict as NSDictionary
                                    // self.navigationController?.pushViewController(settingsVCId!, animated: true)
                                    // PDJobRequest
                                    
                                   }
                                   else {
                                    print("no")
                                     ERProgressHud.sharedInstance.hide()
                                   }
                               }

                               case .failure(_):
                                   print("Error message:\(String(describing: response.result.error))")
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   
                                   let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                   
                                   let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                    UIAlertAction in
                                    NSLog("OK Pressed")
                                   }
                                   
                                   alertController.addAction(okAction)
                                   
                                   self.present(alertController, animated: true, completion: nil)
                                   
                                   break
                            }
            }
        
    }
    
    
    
    @objc func totalEarningClickMethod(strActionName:String,strDateBy:String,strStatus:String) {
        self.arrListOfAllMyOrders.removeAllObjects()
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
        let urlString = BASE_URL_EXPRESS_PLUS
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            /*
             [action] => bookinglist
             [userId] => 123
             [userType] => Driver
             [status] => 2 is service and 4 is food earning
             [dateBy] => Weekly
             [pageNo] => 1
             */
            
             let x : Int = person["userId"] as! Int
             let myString = String(x)
            
                   parameters = [
                       "action"     : String(strActionName),
                       "userId"     : String(myString),
                       "userType"   : person["role"] as! String,
                       "status"     : String(strStatus),
                       "dateBy"     : String(strDateBy),
                       "pageNo"     : String("1")
            ]
        }
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
            {
                response in
               
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value {

                        let JSON = data as! NSDictionary
                        print(JSON as Any)
                                
                               /*
                                 AVGRating = "";
                                 TIP = 10;
                                 address = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
                                 bookingDate = "21st May";
                                 bookingId = 227;
                                 contactNumber = 5623000852;
                                 created = "2020-05-20 18:46:00";
                                 email = "meen@mailinator.com";
                                 fullName = kiwi;
                                 groupId = 43;
                                 image = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1589889715IMG-20200519-WA0003.jpg";
                                 serviceAmount = 100;
                                 serviceName = "Yard Work";
                                 servicePrice = 100;
                                 slote = "22:0-21:16";
                                 status = 2;
                                 totalAmount = 110;
                                 userAddress = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
                                 userId = 114;
                                 zipCode = "";
                                 */
                                
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                
                                
                                if let myLoadedString = UserDefaults.standard.string(forKey: "keyFoodOrderEarnings") {
                                    if myLoadedString == "foodOrderEarningCheck" {
                                        self.getEarning(strDateBy2: strDateBy, getEarning: "getearningfood")
                                    } else if myLoadedString == "searviceEarningCheck" {
                                        self.getEarning(strDateBy2: strDateBy, getEarning: "getearning")
                                    } else if myLoadedString == "toGetEarningCheck" {
                                        self.getEarning(strDateBy2: strDateBy, getEarning: "togetearning")
                                    } else if myLoadedString == "specialOrderEarningCheck" {
                                    
                                        self.getEarning(strDateBy2: strDateBy, getEarning: "specialearning")
                                        
                                    }
                                }
                                
                                
                                  ERProgressHud.sharedInstance.hide()
                                
                                // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDJobRequestId") as? PDJobRequest
                                // settingsVCId!.dictGetDetails = dict as NSDictionary
                                // self.navigationController?.pushViewController(settingsVCId!, animated: true)
                                // PDJobRequest
                                
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                NSLog("OK Pressed")
                               }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                        }
        }
    }
    
    @objc func getEarning(strDateBy2:String,getEarning:String) {
    
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
           
            let urlString = BASE_URL_EXPRESS_PLUS
            
               var parameters:Dictionary<AnyHashable, Any>!
               if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                   
                let x : Int = person["userId"] as! Int
                let myString = String(x)
                
                parameters = [
                    "action"     : String(getEarning),
                    "userId"     : String(myString),
                    "usertype"   : person["role"] as! String,
                    "dateBy"     : String(strDateBy2)
                   ]
               }
        print("parameters-------\(String(describing: parameters))")
                          
                          Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                              {
                                  response in
                      
                                  switch(response.result) {
                                  case .success(_):
                                     if let data = response.result.value {

                                      let JSON = data as! NSDictionary
                                        print(JSON as Any)
                                     
                                      var strSuccess : String!
                                      strSuccess = JSON["status"]as Any as? String
                                      
                                        /*
                                         status = success;
                                         totalAmount = 115;
                                         totalCount = 1;
                                         */
                                        
                                        /*
                                        @IBOutlet weak var myEarning:UILabel!
                                        @IBOutlet weak var serviceCompleted:UILabel!
                                        */
                                        // var strSuccessAlert : String!
                                        // strSuccessAlert = JSON["msg"]as Any as? String
                                      
                                      if strSuccess == String("success") {
                                       print("yes")
                                        
                                        ERProgressHud.sharedInstance.hide()
                                       
                                        // (item!["fullName"] as! String)
                                        
                                        if JSON["totalAmount"] == nil {
                                            print("i am nil")
                                        } else {
                                            print("i am nil no")
                                        }
                                        
                                        // let x : Int = (JSON["totalAmount"] as! Int)
                                        // let myString = String(x)
                                        // self.myEarning.text = "$ "+myString
                                        
                                        
                                        if JSON["totalAmount"] is String {
                                            
                                            print("Yes, it's a String")
                                            self.myEarning.text = "$ "+(JSON["totalAmount"] as! String)
                                            
                                        } else if JSON["v"] is Int {
                                            
                                            print("It is Integer")
                                            let x2 : Int = (JSON["totalAmount"] as! Int)
                                            let myString2 = String(x2)
                                            self.myEarning.text = "$ "+myString2
                                            
                                        } else {
                                          //some other check
                                            print("i am ")
                                            
                                            let temp:NSNumber = JSON["totalAmount"] as! NSNumber
                                            let tempString = temp.stringValue
                                            self.myEarning.text = "$ "+tempString
                                            
                                        }
                                        
                                        
                                        
                                        
                                        
                                        
                                        if JSON["totalCount"] is String {
                                          print("Yes, it's a String")
                                            self.serviceCompleted.text = (JSON["totalCount"] as! String)
                                        } else if JSON["totalCount"] is Int {
                                          print("It is Integer")
                                            let x2 : Int = (JSON["totalCount"] as! Int)
                                            let myString2 = String(x2)
                                            self.serviceCompleted.text = myString2
                                        } else {
                                          //some other check
                                        }
                                        
                                        
                                        
                                        
                                        self.tbleView.delegate = self
                                        self.tbleView.dataSource = self
                                        
                                        self.tbleView.reloadData()
                                        
                                       // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDJobRequestId") as? PDJobRequest
                                       // settingsVCId!.dictGetDetails = dict as NSDictionary
                                       // self.navigationController?.pushViewController(settingsVCId!, animated: true)
                                       // PDJobRequest
                                       
                                      }
                                      else {
                                       print("no")
                                        ERProgressHud.sharedInstance.hide()
                                      }
                                  }

                                  case .failure(_):
                                      print("Error message:\(String(describing: response.result.error))")
                                      
                                      ERProgressHud.sharedInstance.hide()
                                      
                                      let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                      
                                      let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                              UIAlertAction in
                                              NSLog("OK Pressed")
                                          }
                                      
                                      alertController.addAction(okAction)
                                      
                                      self.present(alertController, animated: true, completion: nil)
                                      
                                      break
                                   }
                              }
    }
}


//MARK:- TABLE VIEW
extension RTotalEarnings: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RTotalEarningsTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! RTotalEarningsTableCell
        
        /*
                                               AVGRating = "";
                                               TIP = 10;
                                               address = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
                                               bookingDate = "21st May";
                                               bookingId = 227;
                                               contactNumber = 5623000852;
                                               created = "2020-05-20 18:46:00";
                                               email = "meen@mailinator.com";
                                               fullName = kiwi;
                                               groupId = 43;
                                               image = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1589889715IMG-20200519-WA0003.jpg";
                                               serviceAmount = 100;
                                               serviceName = "Yard Work";
                                               servicePrice = 100;
                                               slote = "22:0-21:16";
                                               status = 2;
                                               totalAmount = 110;
                                               userAddress = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
                                               userId = 114;
                                               zipCode = "";
                                               */
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        print(item as Any)
        
        if let myLoadedString = UserDefaults.standard.string(forKey: "keyFoodOrderEarnings") {
            // print(myLoadedString) // "foodOrderEarningCheck"
            
            // toGetEarningCheck
            
            if myLoadedString == "foodOrderEarningCheck" {
                cell.lblName.text       = (item!["userName"] as! String)+"- $"+(item!["totalAmount"] as! String)
                // cell.lblPrice.text      = (item!["servicePrice"] as! String)
                
                var ar : NSArray!
                ar = (item!["foodDetails"] as! Array<Any>) as NSArray
                // self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                
                cell.lblService.text    = " "+String(ar.count)+" items "+" "
                cell.lblAddress.text    = (item!["resturentAddress"] as! String)
                
                cell.img.sd_setImage(with: URL(string: (item!["userImage"] as! String)), placeholderImage: UIImage(named: "profile"))
                
            } else if myLoadedString == "searviceEarningCheck" {
                
                cell.lblName.text       = (item!["fullName"] as! String)+"- $"+(item!["totalAmount"] as! String)
                // cell.lblPrice.text      = (item!["servicePrice"] as! String)
                cell.lblService.text    = " "+(item!["serviceName"] as! String)+" "
                cell.lblAddress.text    = (item!["address"] as! String)
                
                cell.img.sd_setImage(with: URL(string: (item!["image"] as! String)), placeholderImage: UIImage(named: "profile"))
                
            } else if myLoadedString == "toGetEarningCheck" {
                
                /*
                 Optional(["created": 2020-06-10 18:17:00, "longitude": 77.0605605, "salesTax": 0, "TIP": 23, "userImage": , "wahtUwant": w, "notes": jvj , "noContact": , "RequestDate": , "StoreCity": r, "PaymentDate": , "togetrequestId": 65, "RequestStatus": 0, "status": 1, "userName": customerios2020, "userId": 170, "deliveryFee": 6, "requestMoney": 0, "address": Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India, "EPrice": 162, "latidude": ])
                 (lldb)
                 */
                
                cell.lblName.text       = (item!["userName"] as! String) // +" $"+(item!["totalAmount"] as! String)
                // cell.lblPrice.text      = (item!["servicePrice"] as! String)
                cell.lblService.text    = " "+(item!["wahtUwant"] as! String)+" "
                cell.lblAddress.text    = (item!["address"] as! String)
                
                cell.img.sd_setImage(with: URL(string: (item!["userImage"] as! String)), placeholderImage: UIImage(named: "profile"))
                
            } else if myLoadedString == "specialOrderEarningCheck" {
                
                cell.lblName.text       = (item!["userName"] as! String) // +" $"+(item!["totalAmount"] as! String)
                // cell.lblPrice.text      = (item!["servicePrice"] as! String)
                cell.lblService.text    = " "+(item!["whatYouWant"] as! String)+" "
                cell.lblAddress.text    = (item!["address"] as! String)
                
                cell.img.sd_setImage(with: URL(string: (item!["userImage"] as! String)), placeholderImage: UIImage(named: "profile"))
                
            }
        }
        
        cell.backgroundColor = .white
       
        return cell
    }
    
    @objc func backDirectlyClickMethod() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
}

extension RTotalEarnings: UITableViewDelegate {
    
}


