//
//  RTotalEarningsTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 10/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class RTotalEarningsTableCell: UITableViewCell {

    @IBOutlet weak var img:UIImageView! {
        didSet {
            img.layer.cornerRadius = 30
            img.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblService:UILabel! {
        didSet {
            lblService.layer.cornerRadius = 6
            lblService.clipsToBounds = true
        }
    }
    @IBOutlet weak var lblAddress:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
