//
//  RReview.swift
//  ExpressPlus
//
//  Created by Apple on 29/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class RReview: UIViewController {

    let cellReuseIdentifier = "rReviewTableCell"
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
    
    var page : Int! = 1
    var loadMore : Int! = 1;
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "REVIEWS"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            // tbleView.delegate = self
            // tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.sideBarMenuClick()
        
        self.reviewListWB()
    }
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    // MARK:- REVIEW WEBSERVICE -
    @objc func reviewListWB() {
        self.arrListOfAllMyOrders.removeAllObjects()
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
        
        /*
         [action] => reviewlist
         [userId] => 99
         [pageNo] => 2
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
    // print(person as Any)
        
        let strMyId:String!
        let x : Int = person["userId"] as! Int
        strMyId = String(x)
        
                      parameters = [
                          "action"  : "reviewlist",
                          "pageNo"  : String("1"),
                          "userId"  : String(strMyId)
                ]
        }
                      print("parameters-------\(String(describing: parameters))")
                      
                      Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                              response in
                  
                              switch(response.result) {
                              case .success(_):
                                 if let data = response.result.value {

                                  let JSON = data as! NSDictionary
                                    print(JSON as Any)
                                  
                                  var strSuccess : String!
                                  strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                                  if strSuccess == String("success") {
                                   print("yes")
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    
                                    var ar : NSArray!
                                    ar = (JSON["response"] as! Array<Any>) as NSArray
                                    self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                    
                                    self.tbleView.delegate = self
                                    self.tbleView.dataSource = self
                                    self.tbleView.reloadData()
                                    self.loadMore = 1;
                                    
                                    
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                        }
        }
    }
}

//MARK:- TABLE VIEW
extension RReview: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RReviewTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! RReviewTableCell
        
        cell.backgroundColor = .clear
       
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        // print(item as Any)
       
        cell.lblName.text       = (item!["userName"] as! String).uppercased()
        cell.lblMessage.text    = (item!["message"] as! String)
        
        let x1 : Int = item!["star"] as! Int
        let myString1 = String(x1)
        
        if myString1 == "0" {
            cell.btnStarOne.image = UIImage(named: "star_white")
            cell.btnStarTwo.image = UIImage(named: "star_white")
            cell.btnStarThree.image = UIImage(named: "star_white")
            cell.btnStarFour.image = UIImage(named: "star_white")
            cell.btnStarFive.image = UIImage(named: "star_white")
        } else if myString1 == "1" {
             cell.btnStarOne.image = UIImage(named: "star_yellow")
             cell.btnStarTwo.image = UIImage(named: "star_white")
             cell.btnStarThree.image = UIImage(named: "star_white")
             cell.btnStarFour.image = UIImage(named: "star_white")
             cell.btnStarFive.image = UIImage(named: "star_white")
        } else if myString1 == "2" {
             cell.btnStarOne.image = UIImage(named: "star_yellow")
             cell.btnStarTwo.image = UIImage(named: "star_yellow")
             cell.btnStarThree.image = UIImage(named: "star_white")
             cell.btnStarFour.image = UIImage(named: "star_white")
             cell.btnStarFive.image = UIImage(named: "star_white")
        } else if myString1 == "3" {
             cell.btnStarOne.image = UIImage(named: "star_yellow")
             cell.btnStarTwo.image = UIImage(named: "star_yellow")
             cell.btnStarThree.image = UIImage(named: "star_yellow")
             cell.btnStarFour.image = UIImage(named: "star_white")
             cell.btnStarFive.image = UIImage(named: "star_white")
        } else if myString1 == "4" {
             cell.btnStarOne.image = UIImage(named: "star_yellow")
             cell.btnStarTwo.image = UIImage(named: "star_yellow")
             cell.btnStarThree.image = UIImage(named: "star_yellow")
             cell.btnStarFour.image = UIImage(named: "star_yellow")
             cell.btnStarFive.image = UIImage(named: "star_white")
        } else if myString1 == "5" {
             cell.btnStarOne.image = UIImage(named: "star_yellow")
             cell.btnStarTwo.image = UIImage(named: "star_yellow")
             cell.btnStarThree.image = UIImage(named: "star_yellow")
             cell.btnStarFour.image = UIImage(named: "star_yellow")
             cell.btnStarFive.image = UIImage(named: "star_yellow")
        }
        
        cell.imgProfile.sd_setImage(with: URL(string: (item!["profile_picture"] as! String)), placeholderImage: UIImage(named: "profile"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

extension RReview: UITableViewDelegate {
    
}

