//
//  RReviewTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 29/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class RReviewTableCell: UITableViewCell {

    @IBOutlet weak var imgProfile:UIImageView!
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblMessage:UILabel!
    
    @IBOutlet weak var btnStarOne:UIImageView!
    @IBOutlet weak var btnStarTwo:UIImageView!
    @IBOutlet weak var btnStarThree:UIImageView!
    @IBOutlet weak var btnStarFour:UIImageView!
    @IBOutlet weak var btnStarFive:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
