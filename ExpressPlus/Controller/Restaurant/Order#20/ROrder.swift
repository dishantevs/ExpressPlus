//
//  ROrder.swift
//  ExpressPlus
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class ROrder: UIViewController {

    let cellReuseIdentifier = "rOrderTableCell"
    
    var dictGetFoodDetails:NSDictionary!
    
    var dict: Dictionary<AnyHashable, Any> = [:]
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
    
    var page : Int! = 1
    var loadMore : Int! = 1;
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblCardNumber:UILabel! {
        didSet {
            lblCardNumber.text = ""
        }
    }
    @IBOutlet weak var lblTipAmount:UILabel! {
        didSet {
            lblTipAmount.text = ""
        }
    }
    @IBOutlet weak var lblTotalAmount:UILabel! {
        didSet {
            lblTotalAmount.text = ""
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "ORDER LIST"
        }
    }
    
    @IBOutlet weak var btnInProgress:UIButton! {
        didSet {
            btnInProgress.backgroundColor = UIColor.init(red: 217.0/255.0, green: 91.0/255.0, blue: 49.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var btnDeliverd:UIButton! {
        didSet {
            btnDeliverd.backgroundColor = UIColor.init(red: 217.0/255.0, green: 91.0/255.0, blue: 49.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
        }
    }
    
    @IBOutlet weak var viewBottomOne:UIView! {
        didSet {
            viewBottomOne.backgroundColor = .white
            viewBottomOne.layer.cornerRadius = 8
            viewBottomOne.clipsToBounds = true
        }
    }
    @IBOutlet weak var viewBottomTwo:UIView! {
        didSet {
            viewBottomTwo.backgroundColor = .white
            viewBottomTwo.layer.cornerRadius = 8
            viewBottomTwo.clipsToBounds = true
        }
    }
    @IBOutlet weak var viewBottomThree:UIView! {
        didSet {
            viewBottomThree.backgroundColor = .white
            viewBottomThree.layer.cornerRadius = 8
            viewBottomThree.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        self.btnBack.addTarget(self, action: #selector(backClickMehod), for: .touchUpInside)
        self.btnDeliverd.addTarget(self, action: #selector(pushToDeliveredScreen), for: .touchUpInside)
        
        // print(dictGetFoodDetails as Any)
        self.lblNavigationTitle.text = "Order # "
        self.foodDetailsWB()
        
    }
    
    @objc func pushToDeliveredScreen() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ROrderRequestPlusDriverId") as? ROrderRequestPlusDriver
        settingsVCId!.dictOrderDetails = dict as NSDictionary
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    @objc func backClickMehod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- FOOD DETAILS -
    @objc func foodDetailsWB() {
         self.arrListOfAllMyOrders.removeAllObjects()
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
                  
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        /*
         [action] => foodorderdetails
         [userId] => 99
         [foodorderId] => 70
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
    // print(person as Any)
        
        let strMyId:String!
        let x : Int = person["userId"] as! Int
        strMyId = String(x)
        
            let strMyIdy:String!
            let xy : Int = dictGetFoodDetails["foodorderId"] as! Int
            strMyIdy = String(xy)
            
                      parameters = [
                          "action"      : "foodorderdetails",
                          "foodorderId" : String(strMyIdy),
                          "userId"      : String(strMyId)
                          
                ]
        }
                      print("parameters-------\(String(describing: parameters))")
                      
                      Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                              response in
                  
                              switch(response.result) {
                              case .success(_):
                                 if let data = response.result.value {

                                  let JSON = data as! NSDictionary
                                    // print(JSON as Any)
                                  /*
                                     EstTime = "15 Minutes";
                                            Mile = "4 Mile";
                                            TIP = "0.75";
                                            address = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
                                            assignDriver = 113;
                                            cardNo = 4242424242424242;
                                            city = Ghaziabad;
                                            couponCode = "";
                                            created = "2020-05-19 17:40:00";
                                            deliveryFee = "";
                                            deliveryLat = "28.6634703";
                                            deliveryLong = "77.3240304";
                                            discount = "";
                                            driverAVG = 0;
                                            driverContactNumber = 2356898523;
                                            driverId = 113;
                                            driverImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1589802893IMG-20200518-WA0003.jpg";
                                            driverName = "driver evs";
                                            foodDetails =         (
                                                            {
                                                    id = 43;
                                                    name = gagsgs;
                                                    price = "15.00";
                                                    quantity = 1;
                                                    resturentId = 99;
                                                }
                                            );
                                            foodDetailsNew =         (
                                                            {
                                                    categoryId = "";
                                                    description = "";
                                                    foodName = "";
                                                    foodTag = "";
                                                    foodType = "";
                                                    "image_1" = "";
                                                    "image_2" = "";
                                                    "image_3" = "";
                                                    "image_4" = "";
                                                    "image_5" = "";
                                                    menuId = "";
                                                    price = "";
                                                    quantity = 1;
                                                    resturentId = "";
                                                    specialPrice = "";
                                                }
                                            );
                                            foodId = "";
                                            foodorderId = 70;
                                            landmark = hotel;
                                            name = purnima;
                                            noContact = "";
                                            phone = 5623852369;
                                            resturentAddress = "Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India";
                                            resturentId = 99;
                                            resturentLatitude = "28.5870717";
                                            resturentLongitude = "77.0605306";
                                            resturentName = "restaurent 2d";
                                            specialNote = "test y";
                                            state = "Uttar Pradesh";
                                            status = 4;
                                            storecity = "";
                                            totalAmount = "15.75";
                                            userAVG = 0;
                                            userId = 114;
                                            userName = kiwi;
                                            whatYouWant = "";
                                            workPlace = Home;
                                            zipcode = 201011;
                                        };
                                     */
                                  var strSuccess : String!
                                  strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                                  if strSuccess == String("success") {
                                   print("yes")
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    
                                    // self.txt
                                    /*
                                    var ar : NSArray!
                                    ar = (JSON["data"] as! Array<Any>) as NSArray
                                    self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                    */
                                    
                                    
                                    self.dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                    
                                    self.lblCardNumber.text     = "$ "+(self.dict["cardNo"] as! String)
                                    self.lblTipAmount.text      = "$ "+(self.dict["TIP"] as! String)
                                    self.lblTotalAmount.text    = "$ "+(self.dict["totalAmount"] as! String)
                                   
                                    var ar : NSArray!
                                    ar = (self.dict["foodDetails"] as! Array<Any>) as NSArray
                                    self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                    
                                    // foodorderId
                                    let x : Int = self.dict["foodorderId"] as! Int
                                    let myString = String(x)
                                    self.lblNavigationTitle.text    = "Order # "+myString
                                    
                                    
                                    self.tbleView.delegate = self
                                    self.tbleView.dataSource = self
                                    self.tbleView.reloadData()
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                        }
        }
    }
}


//MARK:- TABLE VIEW
extension ROrder: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ROrderTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! ROrderTableCell
        
        cell.backgroundColor = .white
        
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        // print(item as Any)
        cell.lblName.text = (item!["name"] as! String)
        cell.lblPrice.text = "$ "+(item!["price"] as! String)
        cell.lblQuantity.text = (item!["quantity"] as! String)
        
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}

extension ROrder: UITableViewDelegate {
    
}

