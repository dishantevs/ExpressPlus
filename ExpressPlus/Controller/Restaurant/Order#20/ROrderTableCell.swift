//
//  ROrderTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ROrderTableCell: UITableViewCell {

    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    
    @IBOutlet weak var imgFoodProfile:UIImageView! {
        didSet {
            imgFoodProfile.backgroundColor = .clear
            // imgFoodProfile.layer.cornerRadius = 40
            // imgFoodProfile.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblQuantity:UILabel! {
        didSet {
            lblQuantity.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            lblQuantity.layer.cornerRadius = 4
            lblQuantity.clipsToBounds = true
            lblQuantity.textColor = .white
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
