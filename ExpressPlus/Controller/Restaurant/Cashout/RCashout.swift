//
//  RCashout.swift
//  ExpressPlus
//
//  Created by Apple on 01/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class RCashout: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var dragbaseView:UIView! {
        didSet {
            dragbaseView.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var imgSelectedBankName:UIImageView! {
        didSet {
             
        }
    }
    
    @IBOutlet weak var navigationBar:UIView! {
           didSet {
               navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
           }
       }
    
    @IBOutlet weak var lblBankName:UILabel!
    @IBOutlet weak var lblAccountNumberShowOnScreen:UILabel!
    
       @IBOutlet weak var lblNavigationTitle:UILabel! {
           didSet {
               lblNavigationTitle.text = "CASHOUT"
               lblNavigationTitle.textColor = .white
           }
       }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            //btnBack.setTitleColor(BUTTON_BACKGROUND_COLOR_BLUE, for: .normal)
            // btnBack.addTarget(self, action: #selector(backClick), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var lblCurrentWalletBalance:UILabel! {
        didSet {
            lblCurrentWalletBalance.text = "      Current Wallet Balance          "
            lblCurrentWalletBalance.textColor = .white
            lblCurrentWalletBalance.backgroundColor = UIColor.init(red: 100.0/255.0, green: 206.0/255.0, blue: 225.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var lblTotalAmountInWallet:UILabel! {
        didSet {
            lblTotalAmountInWallet.textAlignment = .center
            // lblTotalAmountInWallet.text = "      Current Wallet Balance          $600     "
            lblTotalAmountInWallet.textColor = .white
            lblTotalAmountInWallet.backgroundColor = UIColor.init(red: 100.0/255.0, green: 206.0/255.0, blue: 225.0/255.0, alpha: 1)
        }
    }
     
    
    
    @IBOutlet weak var extendedWithDrawableBalance:UILabel! {
        didSet {
            extendedWithDrawableBalance.textColor = .black
            extendedWithDrawableBalance.textColor = .black
            // extendedWithDrawableBalance.text = "Withdrawable amount is $650"
        }
    }
    
    @IBOutlet weak var txtEnterAmount:UITextField! {
        didSet {
            txtEnterAmount.layer.cornerRadius = 4
            txtEnterAmount.clipsToBounds = true
            txtEnterAmount.placeholder = "Enter Amount"
            txtEnterAmount.keyboardAppearance = .dark
            txtEnterAmount.keyboardType = .decimalPad
            txtEnterAmount.delegate = self
        }
    }
    @IBOutlet weak var txtTotalAccountList:UITextField! {
        didSet {
            txtTotalAccountList.layer.cornerRadius = 4
            txtTotalAccountList.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnOpenBankList:UIButton! {
        didSet {
            
        }
    }
        
    @IBOutlet weak var btnSubmitRequest:UIButton! {
        didSet {
            btnSubmitRequest.setTitleColor(.white, for: .normal)
            btnSubmitRequest.layer.cornerRadius = 4
            btnSubmitRequest.clipsToBounds = true
            btnSubmitRequest.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnSubmitRequest.setTitle("Submit Request", for: .normal)
            
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white;
        
        let userDefaults = UserDefaults.standard
        let myString = userDefaults.string(forKey: "backOrMenu")
        if myString == "back" {
            btnBack.setImage(UIImage(named:"back"), for: .normal)
            
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
            
            // let userDefaults = UserDefaults.standard
            userDefaults.set("", forKey: "backOrMenu")
            userDefaults.set(nil, forKey: "backOrMenu")
            
        } else {
            btnBack.setImage(UIImage(named:"sMenu"), for: .normal)
            sideBarMenuClick()
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(RCashout.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        setPaddingWithImage(image: UIImage(named:"dollarCashout")!, textField: txtEnterAmount)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            self.lblAccountNumberShowOnScreen.text = (person["AccountNo"] as! String)
        }
        // btnOpenBankList
        btnOpenBankList.addTarget(self, action: #selector(openBankListClickMethod), for: .touchUpInside)
        
        btnSubmitRequest.addTarget(self, action: #selector(setUpValidation), for: .touchUpInside)
        
        self.getWalletBalanceWB()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @objc func openBankListClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDPaymentInformationId") as? PDPaymentInformation
        push!.fromRestaurantPage = "yesRestaurant"
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func sideBarMenuClick() {
        
        self.view.endEditing(true)
        
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    // MARK:- WEBSERVICE ( WALLET )
    @objc func getWalletBalanceWB() {
       ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Fetching your Balance...")
        
        let urlString = BASE_URL_EXPRESS_PLUS
        var parameters:Dictionary<AnyHashable, Any>!
           
        /*
         [action] => getwallet
         [userId] => 99
         */
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
            
            self.lblBankName.text = (person["BankName"] as! String)
            
            let x : Int = (person["userId"] as! Int)
            let myString = String(x)
                   parameters = [
                       "action" : "getwallet",
                       "userId"   : String(myString)
            ]
        }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                   // print(JSON)
                                /*
                                 status = success;
                                 wallet = 0;
                                 */
                               
                                var strSuccess : String!
                                strSuccess = JSON["status"]as Any as? String
                               
                               // var strSuccessAlert : String!
                               // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == "success" {
                                ERProgressHud.sharedInstance.hide()
                                
                                // print(JSON["specialPrice"] as! Int as Any)
                                
                                if JSON["wallet"] is String {
                                    
                                    print("Yes, it's a String")
                                    self.lblTotalAmountInWallet.text = "Current Wallet Balance : $ "+(JSON["wallet"] as! String)
                                    
                                } else if JSON["wallet"] is Int {
                                    
                                    print("It is Integer")
                                    let x : Int = JSON["wallet"] as! Int
                                    let myString = String(x)
                                    self.lblTotalAmountInWallet.text = "Current Wallet Balance : $ "+String(myString)
                                    
                                } else if JSON["wallet"] is NSNumber {
                                  print("It is nsnumber")
                                  //some other check
                                    
                                    let temp:NSNumber = JSON["wallet"] as! NSNumber
                                    let tempString = temp.stringValue
                                    self.lblTotalAmountInWallet.text = "Current Wallet Balance : $ "+String(tempString)
                                    
                                }
                                
                                
                                
                               }
                               else {
                                 ERProgressHud.sharedInstance.hide()
                               }
                               
                           }

                           case .failure(_):
                            print("Error message:\(String(describing: response.result.error))")
                            ERProgressHud.sharedInstance.hide()
                            let alertController = UIAlertController(title: nil, message: SERVER_ISSUE_MESSAGE_ONE+"\n"+SERVER_ISSUE_MESSAGE_TWO, preferredStyle: .actionSheet)
                               
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                NSLog("OK Pressed")
                            }
                               
                            alertController.addAction(okAction)
                               
                            self.present(alertController, animated: true, completion: nil)
                               
                            break
                        }
        }
    }
    
    func setPaddingWithImage(image: UIImage, textField: UITextField){
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 50))
        imageView.frame = CGRect(x: 13.0, y: 13.0, width: 24.0, height: 24.0)
        //For Setting extra padding other than Icon.
        let seperatorView = UIView(frame: CGRect(x: 50, y: 0, width: 2, height: 50))
        seperatorView.backgroundColor = UIColor(red: 80/255, green: 89/255, blue: 94/255, alpha: 1)
        view.addSubview(seperatorView)
        textField.leftViewMode = .always
        view.addSubview(imageView)
        view.backgroundColor = .clear
        textField.leftViewMode = UITextField.ViewMode.always
        textField.leftView = view
    }
    
    
    @objc func setUpValidation() {
        if String(txtEnterAmount.text!) == "" {
            let alert = UIAlertController(title: String("Alert"), message: String("Please enter some Amount"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
                // self.backClickMthod()
                
                self.txtEnterAmount.text = ""
                
            }))
            self.present(alert, animated: true, completion: nil)
        } else if String(txtEnterAmount.text!) == "0" {
            let alert = UIAlertController(title: String("Alert"), message: String("Amount should be greate than zero."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
                // self.backClickMthod()
                
                self.txtEnterAmount.text = ""
                
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            self.cashoutMoneyFromDriverScreen()
        }
    }
    @objc func cashoutMoneyFromDriverScreen() {
     
      ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => cashoutrequest
      [userId] => 123
      [requestAmount] => 1
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
                   parameters = [
                       "action"              : "cashoutrequest",
                        "userId"              : person["userId"] as Any,
                        "requestAmount"      : String(txtEnterAmount.text!),
                       // "status"              : String(""),
                       // "pageNo"              : String("0")
             ]
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                 print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                // ERProgressHud.sharedInstance.hide()
                                 
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                ERProgressHud.sharedInstance.hide()
                                let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                    // self.dismiss(animated: true, completion: nil)
                                    // self.backClickMthod()
                                    
                                    self.txtEnterAmount.text = ""
                                    
                                    self.getWalletBalanceWB()
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                        }
        }
    }
}
