//
//  RAddMenuTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 28/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class RAddMenuTableCell: UITableViewCell {

    @IBOutlet weak var txtDishName:UITextField!
    @IBOutlet weak var txtCategory:UITextField!
    @IBOutlet weak var txtFoodType:UITextField!
    @IBOutlet weak var txtPrice:UITextField!
    @IBOutlet weak var txtSpecialPrice:UITextField!
    
    @IBOutlet weak var txtDescription:UITextView! {
        didSet {
            txtDescription.layer.cornerRadius = 6
            txtDescription.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnUploadDishImage:UIButton!
    @IBOutlet weak var btnSaveAndContinue:UIButton! {
        didSet {
            btnSaveAndContinue.layer.cornerRadius = 6
            btnSaveAndContinue.clipsToBounds = true
            btnSaveAndContinue.setTitle("Save & Continue", for: .normal)
            btnSaveAndContinue.setTitleColor(.white, for: .normal)
            btnSaveAndContinue.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    @IBOutlet weak var btnAddMenuItem:UIButton!
    @IBOutlet weak var btnSkip:UIButton!
    
    @IBOutlet weak var imgPhoto:UIImageView! {
        didSet {
            imgPhoto.isHidden = true
        }
    }
    
    @IBOutlet weak var btnCategory:UIButton!
    @IBOutlet weak var btnFoodType:UIButton!
    
    @IBOutlet weak var viewUploadImage:UIView! {
        didSet {
            viewUploadImage.backgroundColor = .white
            viewUploadImage.layer.cornerRadius = 6
            viewUploadImage.clipsToBounds = true
            viewUploadImage.addDashedBorder()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension UIView {
  func addDashedBorder() {
    let color = UIColor.black.cgColor

    let shapeLayer:CAShapeLayer = CAShapeLayer()
    let frameSize = self.frame.size
    let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

    shapeLayer.bounds = shapeRect
    shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.strokeColor = color
    shapeLayer.lineWidth = 2
    shapeLayer.lineJoin = CAShapeLayerLineJoin.round
    shapeLayer.lineDashPattern = [6,3]
    shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath

    self.layer.addSublayer(shapeLayer)
    }
}
