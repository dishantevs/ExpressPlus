//
//  RAddMenu.swift
//  ExpressPlus
//
//  Created by Apple on 28/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire
class RAddMenu: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    let cellReuseIdentifier = "rAddMenuTableCell"
    
    let regularFont = UIFont.systemFont(ofSize: 16)
    let boldFont = UIFont.boldSystemFont(ofSize: 16)
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
    
    var arrGetCategoryName:NSMutableArray! = []
    var arrGetCategoryId:NSMutableArray! = []
    
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var saveCategoryIdForWebserviceParameter:String!
    
    var strPassImgString:String!
    var imageStr:String!
    var imgData:Data!
    
    var getRestaurantDetails:NSDictionary!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "ADD ITEM"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            // self.tbleView.separatorStyle = UITableViewCell.SeparatorStyle.none
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        let userDefaults = UserDefaults.standard

        // Write/Set Value
        userDefaults.set("", forKey: "forMenuAfterReg")
        userDefaults.set(nil, forKey: "forMenuAfterReg")
        
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        
        self.strPassImgString = "0"
        
        self.categoryListWB()
    }
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK:- RESTAURANT LOGIN -
    @objc func categoryListWB() {
           
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
                  
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        /*
         [action] => login
         [email] => purnimac@gmail.com
         [password] => 123456
         */
        
           var parameters:Dictionary<AnyHashable, Any>!
    
                      parameters = [
                          "action"             : "category"
                          
                ]
                 
                      print("parameters-------\(String(describing: parameters))")
                      
                      Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                              response in
                  
                              switch(response.result) {
                              case .success(_):
                                 if let data = response.result.value {

                                  let JSON = data as! NSDictionary
                                    print(JSON as Any)
                                  
                                    
                                    
                                  var strSuccess : String!
                                  strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                                  if strSuccess == String("success") {
                                   print("yes")
                                    
                                     ERProgressHud.sharedInstance.hide()
                                    
                                    
                                    var ar : NSArray!
                                    ar = (JSON["data"] as! Array<Any>) as NSArray
                                    self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                  
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                        }
        }
    }
    @objc func imageTapped() {
        // let tappedImage = tapGestureRecognizer.view as! UIImageView

        let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery()
        }))

        alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @objc func openCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func openGallery() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! RAddMenuTableCell
        
        cell.imgPhoto.isHidden = false
        
        let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        cell.imgPhoto.image = image_data // show image on profile
        let imageData:Data = image_data!.pngData()!
        imageStr = imageData.base64EncodedString()
        self.dismiss(animated: true, completion: nil)
        imgData = image_data!.jpegData(compressionQuality: 0.2)!
        //print(type(of: imgData)) // data
        
        self.strPassImgString = "1"
        
        // self.uploadWithImage()
    }
    
    @objc func withOrWithoutImage1() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! RAddMenuTableCell
        
          if cell.txtDishName.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Dish Name should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtCategory.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Category should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtFoodType.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Food Type should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtSpecialPrice.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Selected Price should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtDescription.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Description should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            if self.strPassImgString == "1" {
                self.uploadWithImageWB()
            } else {
                self.withoutImageUpload()
            }
        }
    }
    
    @objc func withOrWithoutImage() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! RAddMenuTableCell
        
          if cell.txtDishName.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Dish Name should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtCategory.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Category should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtFoodType.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Food Type should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtSpecialPrice.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Selected Price should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtDescription.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Description should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            if self.strPassImgString == "1" {
                self.uploadWithImageWB()
            } else {
                self.withoutImageUpload()
            }
        }
    }
    
    // MARK:- RESTAURANT LOGIN -
    @objc func withoutImageUpload() {
          
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! RAddMenuTableCell
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Uploading...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
        
        /*
         [action] => addmenu
         [resturentId] => 99
         [categoryId] => 69
         [foodName] => test2
         [foodType] => Veg
         [price] => 1245
         [specialPrice] => 1200
         [description] => bdhdkddb
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
            
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
                      parameters = [
                          "action"      : "addmenu",
                          "resturentId" : String(myString),
                          "categoryId"  : String(self.saveCategoryIdForWebserviceParameter),
                          "foodName"    : String(cell.txtDishName.text!),
                          "foodType"    : String(cell.txtFoodType.text!),
                          "price"       : String(cell.txtPrice.text!),
                          "specialPrice": String(cell.txtSpecialPrice.text!),
                          "description" : String(cell.txtDescription.text!)
                          
                ]
        }
                      print("parameters-------\(String(describing: parameters))")
                      
                      Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                              response in
                  
                              switch(response.result) {
                              case .success(_):
                                 if let data = response.result.value {

                                  let JSON = data as! NSDictionary
                                    print(JSON as Any)
                                  
                                  var strSuccess : String!
                                  strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                                  if strSuccess == String("success") {
                                   print("yes")
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    self.navigationController?.popViewController(animated: true)
                                    
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                        }
        }
    }
    
    
    
    // MARK:- EDIT BUSINESS DETAILS WITH IMAGE
    @objc func uploadWithImageWB() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! RAddMenuTableCell
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Uploading...")
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
          
            // var parameters:Dictionary<AnyHashable, Any>!
                 let parameters = [
                     "action"      : "addmenu",
                     "resturentId" : String(myString),
                     "categoryId"  : String(self.saveCategoryIdForWebserviceParameter),
                     "foodName"    : String(cell.txtDishName.text!),
                     "foodType"    : String(cell.txtFoodType.text!),
                     "price"       : String(cell.txtPrice.text!),
                     "specialPrice": String(cell.txtSpecialPrice.text!),
                     "description" : String(cell.txtDescription.text!)
                 ]
                
                    print(parameters as Any)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.imgData, withName: "image_1",fileName: "expressPlusUploadFoodPhoto.jpg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        
                        // let paramsData:Data = NSKeyedArchiver.archivedData(withRootObject: value)
                        
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                },
                to:BASE_URL_EXPRESS_PLUS)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):

                        upload.uploadProgress(closure: { (progress) in
                            //print("Upload Progress: \(progress.fractionCompleted)")
                            
                            /*
                            let alertController = UIAlertController(title: "Uploading image", message: "Please wait......", preferredStyle: .alert)

                            let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)

                            progressDownload.setProgress(Float((progress.fractionCompleted)/1.0), animated: true)
                            progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)

                            alertController.view.addSubview(progressDownload)
                            self.present(alertController, animated: true, completion: nil)
                            */
                        })

                        upload.responseJSON { response in
                            //print(response.result.value as Any)
                            if let data = response.result.value {
                                let JSON = data as! NSDictionary
                                print(JSON)
                                ERProgressHud.sharedInstance.hide()
                                self.dismiss(animated: true, completion: nil)
                                
                                self.strPassImgString = "0"
                                self.navigationController?.popViewController(animated: true)
                            }
                            else {
                                ERProgressHud.sharedInstance.hide()
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        ERProgressHud.sharedInstance.hide()
                        self.dismiss(animated: true, completion: nil)
                    }}}
        
    }
}

//MARK:- TABLE VIEW
extension RAddMenu: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RAddMenuTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! RAddMenuTableCell
        
        cell.backgroundColor = .white
       
        // cell.lblTitle.text = titleArray[indexPath.row]
        // cell.imgVieww.image = UIImage(named: imageArray[indexPath.row])
        
        cell.btnFoodType.addTarget(self, action: #selector(openGenderPickerAction), for: .touchUpInside)
        cell.btnCategory.addTarget(self, action: #selector(openCategoryPickerAction), for: .touchUpInside)
        
        cell.btnUploadDishImage.addTarget(self, action: #selector(imageTapped), for: .touchUpInside)
        
        cell.btnSaveAndContinue.addTarget(self, action: #selector(withOrWithoutImage), for: .touchUpInside)
        cell.btnAddMenuItem.addTarget(self, action: #selector(withOrWithoutImage1), for: .touchUpInside)
        
        cell.btnSkip.addTarget(self, action: #selector(backDirectlyClickMethod), for: .touchUpInside)
        
        return cell
    }
    @objc func backDirectlyClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        /*
        if indexPath.row == 0 {
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPFoodId") as? UPFood
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
        } else if indexPath.row == 1 {
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ServicesId") as? Services
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
        } else if indexPath.row == 2 {
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPToGetId") as? UPToGet
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
        }
        */
        
    }
    
    @objc func openCategoryPickerAction(_ sender: UIButton) {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! RAddMenuTableCell
        
        // print(self.arrListOfAllMyOrders as Any)
        
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "Please Select Category",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : sender.backgroundColor,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "Search Food Category",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : sender.backgroundColor,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        for check in 0...self.arrListOfAllMyOrders.count-1 {
            // print(check as Any)
            
            let item = arrListOfAllMyOrders[check] as? [String:Any]
            // print(item as Any)
            
            let strName:String!
            let strId:String!
            strName = (item!["name"] as! String)
            
            
            let x : Int = item!["id"] as! Int
            let myString = String(x)
            strId = strName+String(myString)
            
            
            self.arrGetCategoryName.add(strName as Any)
            
            self.arrGetCategoryId.add(strId as Any)
            
        }
        /*
         SubCat =             (
             );
             id = 66;
             image = "";
             name = Lunch;
         },
                 {
             SubCat =             (
             );
             id = 67;
             image = "";
             name = Dinner;
         },
                 {
             SubCat =             (
             );
             id = 68;
             image = "";
             name = "Quick Bites";
         },
                 {
             SubCat =             (
             );
             id = 69;
             image = "";
             name = Desert;
         },
                 {
             SubCat =             (
             );
             id = 70;
             image = "";
             name = Breakfast;
         */
        
        
        let array: [String] = self.arrGetCategoryName.copy() as! [String]
        
        let arrGender = array
        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
           onCompletion: { (selectedIndexes, selectedValues) in
            if let selectedValue = selectedValues.first{
                if selectedValue == arrGender.last!{
                    // cell.txtCategory.text = ""
                    
                    cell.txtCategory.text = "\(selectedValue)"
                      
                      let index = self.arrGetCategoryName.index(of: "\(selectedValue)");
                    
                      let item2 = self.arrListOfAllMyOrders[index] as? [String:Any]
                      let x : Int = item2!["id"] as! Int
                      let myString = String(x)
                      self.saveCategoryIdForWebserviceParameter = String(myString)
                      // print(self.saveCategoryIdForWebserviceParameter as Any)
                    
                    
                } else{
                    cell.txtCategory.text = "\(selectedValue)"
                    
                    let index = self.arrGetCategoryName.index(of: "\(selectedValue)");
                  
                    let item2 = self.arrListOfAllMyOrders[index] as? [String:Any]
                    let x : Int = item2!["id"] as! Int
                    let myString = String(x)
                    self.saveCategoryIdForWebserviceParameter = String(myString)
                    // print(self.saveCategoryIdForWebserviceParameter as Any)
                    
                }
            } else {
                // self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                cell.txtCategory.text = "Please select type"
            }
        },
           onCancel: {
            print("Cancelled")
        }
        )
        
        picker.show(withAnimation: .FromBottom)
    }
    
    @objc func openGenderPickerAction(_ sender: UIButton) {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! RAddMenuTableCell
        
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "Please Select Food Type",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : sender.backgroundColor,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "Search Food Type",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : sender.backgroundColor,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        let arrGender = ["Select Food Type","Veg", "Non-Veg"]
        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if let selectedValue = selectedValues.first{
                                            if selectedValue == arrGender.last!{
                                                // self.btnGenderPicker.setTitle("I'm a human", for: .normal)
                                                cell.txtFoodType.text = "\(selectedValue)"
                                            } else {
                                                // self.btnGenderPicker.setTitle("I'm \(selectedValue)", for: .normal)
                                                cell.txtFoodType.text = "\(selectedValue)"
                                            }
                                        } else {
                                            // self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                                            cell.txtFoodType.text = "Please select type"
                                        }
        },
                                       onCancel: {
                                        print("Cancelled")
        }
        )
        
        picker.show(withAnimation: .FromBottom)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1200
    }
    
}

extension RAddMenu: UITableViewDelegate {
    
}


