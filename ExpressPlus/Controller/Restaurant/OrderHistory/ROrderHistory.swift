//
//  ROrderHistory.swift
//  ExpressPlus
//
//  Created by Apple on 29/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class ROrderHistory: UIViewController {

    let cellReuseIdentifier = "rOrderHistoryTableCell"
    
    // MARK:- ARRAY -
        var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
        
        var page : Int! = 1
        var loadMore : Int! = 1;
        
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            }
        }
        
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "JOB HISTORY"
            }
        }
        
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.setTitle("|||", for: .normal)
            }
        }
        @IBOutlet weak var tbleView:UITableView! {
            didSet {
                // tbleView.delegate = self
                // tbleView.dataSource = self
                
                self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
                self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
                
            }
        }
        override func viewDidLoad() {
            super.viewDidLoad()
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            
            
            
            self.sideBarMenuClick()
            
            self.inprogressWB1()
        }

        @objc func sideBarMenuClick() {
            self.view.endEditing(true)
            if revealViewController() != nil {
                btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                revealViewController().rearViewRevealWidth = 300
                view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            }
        }
        // MARK:- MENU LIST WEBSERVICE -
        @objc func inprogressWB1() {
            self.arrListOfAllMyOrders.removeAllObjects()
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
           
            let urlString = BASE_URL_EXPRESS_PLUS
                      
            // let indexPath = IndexPath.init(row: 0, section: 0)
            // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
            
            /*
             [action] => foodorderlist
             [userId] => 99
             [userType] => Restaurant
             [status] => 4
             */
            
            var parameters:Dictionary<AnyHashable, Any>!
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        // print(person as Any)
            
            let strMyId:String!
            let x : Int = person["userId"] as! Int
            strMyId = String(x)
            
                          parameters = [
                              "action"      : "foodorderlist",
                              "userType"    : String("Restaurant"),
                              "userId"      : String(strMyId),
                              "status"      : String("1")
                              
                    ]
            }
                          print("parameters-------\(String(describing: parameters))")
                          
                          Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                                  response in
                      
                                  switch(response.result) {
                                  case .success(_):
                                     if let data = response.result.value {

                                      let JSON = data as! NSDictionary
                                        print(JSON as Any)
                                      
                                      var strSuccess : String!
                                      strSuccess = JSON["status"]as Any as? String
                                      
                                        // var strSuccessAlert : String!
                                        // strSuccessAlert = JSON["msg"]as Any as? String
                                      
                                      if strSuccess == String("success") {
                                       print("yes")
                                        
                                        ERProgressHud.sharedInstance.hide()
                                        
                                        var ar : NSArray!
                                        ar = (JSON["data"] as! Array<Any>) as NSArray
                                        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                        
                                        self.tbleView.delegate = self
                                        self.tbleView.dataSource = self
                                        self.tbleView.reloadData()
                                        self.loadMore = 1;
                                        
                                        
                                      }
                                      else {
                                       print("no")
                                        ERProgressHud.sharedInstance.hide()
                                       
                                      }
                                  }

                                  case .failure(_):
                                      print("Error message:\(String(describing: response.result.error))")
                                      
                                      ERProgressHud.sharedInstance.hide()
                                      
                                      let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                      
                                      let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                              UIAlertAction in
                                              NSLog("OK Pressed")
                                          }
                                      
                                      alertController.addAction(okAction)
                                      
                                      self.present(alertController, animated: true, completion: nil)
                                      
                                      break
                            }
            }
        }
        // MARK:- MENU LIST WEBSERVICE -
        @objc func inprogressWB4() {
            self.arrListOfAllMyOrders.removeAllObjects()
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
           
            let urlString = BASE_URL_EXPRESS_PLUS
                      
            // let indexPath = IndexPath.init(row: 0, section: 0)
            // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
            
            /*
             [action] => foodorderlist
             [userId] => 99
             [userType] => Restaurant
             [status] => 4
             */
            
               var parameters:Dictionary<AnyHashable, Any>!
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        // print(person as Any)
            
            let strMyId:String!
            let x : Int = person["userId"] as! Int
            strMyId = String(x)
            
                          parameters = [
                              "action"      : "foodorderlist",
                              "userType"    : String("Restaurant"),
                              "userId"      : String(strMyId),
                              "status"      : String("4")
                              
                    ]
            }
                          print("parameters-------\(String(describing: parameters))")
                          
                          Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                                  response in
                      
                                  switch(response.result) {
                                  case .success(_):
                                     if let data = response.result.value {

                                      let JSON = data as! NSDictionary
                                        print(JSON as Any)
                                      
                                      var strSuccess : String!
                                      strSuccess = JSON["status"]as Any as? String
                                      
                                        // var strSuccessAlert : String!
                                        // strSuccessAlert = JSON["msg"]as Any as? String
                                      
                                      if strSuccess == String("success") {
                                       print("yes")
                                        
                                        ERProgressHud.sharedInstance.hide()
                                        
                                        var ar : NSArray!
                                        ar = (JSON["data"] as! Array<Any>) as NSArray
                                        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                        
                                        self.tbleView.delegate = self
                                        self.tbleView.dataSource = self
                                        self.tbleView.reloadData()
                                        self.loadMore = 1;
                                        
                                        
                                      }
                                      else {
                                       print("no")
                                        ERProgressHud.sharedInstance.hide()
                                       
                                      }
                                  }

                                  case .failure(_):
                                      print("Error message:\(String(describing: response.result.error))")
                                      
                                      ERProgressHud.sharedInstance.hide()
                                      
                                      let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                      
                                      let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                              UIAlertAction in
                                              NSLog("OK Pressed")
                                          }
                                      
                                      alertController.addAction(okAction)
                                      
                                      self.present(alertController, animated: true, completion: nil)
                                      
                                      break
                            }
            }
        }
    }


    //MARK:- TABLE VIEW
    extension ROrderHistory: UITableViewDataSource {
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrListOfAllMyOrders.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell:ROrderHistoryTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! ROrderHistoryTableCell
            
            cell.backgroundColor = .white
           
            let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
            // print(item as Any)
           
            /*
             @IBOutlet weak var lblName:UILabel!
             @IBOutlet weak var lblPrice:UILabel!
             */
            
            cell.lblName.text = (item!["userName"] as! String)
            
            cell.lblPrice.text = "$ "+(item!["totalAmount"] as! String)
            
            let fullName: String = (item!["created"] as! String)
            let fullNameArr = fullName.components(separatedBy: " ")
            // let name    = fullNameArr[0]
            let surname = fullNameArr[1]
            
            // print(name as Any)
            // print(surname as Any)
            
            var lastOne:String = String(surname)
            lastOne.removeLast()
            
            var lastTwo:String = String(lastOne)
            lastTwo.removeLast()
            
            var lastThree:String = String(lastTwo)
            lastThree.removeLast()
            
            cell.lblTime.text = " "+String(lastThree)+" "
            
            // let trimmedString = (item!["created"] as! String).trimmingCharacters(in: .whitespaces)
            // print(trimmedString as Any)
            
            cell.imgFoodProfile.sd_setImage(with: URL(string: (item!["userImage"] as! String)), placeholderImage: UIImage(named: "user"))
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            tableView.deselectRow(at: indexPath, animated: true)
            
            let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
           let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ROrderId") as? ROrder
            settingsVCId!.dictGetFoodDetails = item as NSDictionary?
           self.navigationController?.pushViewController(settingsVCId!, animated: true)
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 100
        }
        
    }

    extension ROrderHistory: UITableViewDelegate {
        
    }

