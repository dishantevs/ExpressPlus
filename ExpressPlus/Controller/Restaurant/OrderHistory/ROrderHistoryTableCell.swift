//
//  ROrderHistoryTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 29/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ROrderHistoryTableCell: UITableViewCell {

    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    
    @IBOutlet weak var imgFoodProfile:UIImageView! {
        didSet {
            imgFoodProfile.backgroundColor = .clear
            imgFoodProfile.layer.cornerRadius = 40
            imgFoodProfile.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblTime:UILabel! {
        didSet {
            lblTime.backgroundColor = .systemGreen
            lblTime.layer.cornerRadius = 6
            lblTime.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
