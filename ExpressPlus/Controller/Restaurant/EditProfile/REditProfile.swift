//
//  REditProfile.swift
//  ExpressPlus
//
//  Created by Apple on 28/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

// MARK:- LOCATION -
import CoreLocation

class REditProfile: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, CLLocationManagerDelegate {

    var strPassImgString:String!
    var imageStr:String!
    var imgData:Data!
    
    let locationManager = CLLocationManager()
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    var strEditDriverPortal:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "EDIT PROFILE"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var imgProfile:UIImageView! {
        didSet {
            imgProfile.layer.cornerRadius = 60
            imgProfile.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var txtRestaurantName:UITextField! {
        didSet {
            txtRestaurantName.layer.cornerRadius = 6
            txtRestaurantName.clipsToBounds = true
            txtRestaurantName.setLeftPaddingPoints(40)
            txtRestaurantName.layer.borderColor = UIColor.clear.cgColor
            txtRestaurantName.layer.borderWidth = 0.8
        }
    }
    @IBOutlet weak var txtEmail:UITextField! {
           didSet {
            txtEmail.layer.cornerRadius = 6
            txtEmail.clipsToBounds = true
            txtEmail.setLeftPaddingPoints(44)
            txtEmail.layer.borderColor = UIColor.clear.cgColor
            txtEmail.layer.borderWidth = 0.8
            txtEmail.isUserInteractionEnabled = false
           }
       }
    @IBOutlet weak var txtPhone:UITextField! {
           didSet {
            txtPhone.layer.cornerRadius = 6
            txtPhone.clipsToBounds = true;
            txtPhone.setLeftPaddingPoints(40)
            txtPhone.layer.borderColor = UIColor.clear.cgColor
            txtPhone.layer.borderWidth = 0.8
           }
       }
    @IBOutlet weak var txtAddress:UITextField! {
           didSet {
            txtAddress.layer.cornerRadius = 6
            txtAddress.clipsToBounds = true
            txtAddress.setLeftPaddingPoints(40)
            txtAddress.layer.borderColor = UIColor.clear.cgColor
            txtAddress.layer.borderWidth = 0.8
           }
       }
    
    @IBOutlet weak var btnEditYourBusinessProfile:UIButton!
    
    @IBOutlet weak var btnEditYourProfile:UIButton! {
        didSet {
            btnEditYourProfile.layer.cornerRadius = 6
            btnEditYourProfile.clipsToBounds = true
            btnEditYourProfile.setTitle("Edit Your Profile", for: .normal)
            btnEditYourProfile.setTitleColor(.white, for: .normal)
            btnEditYourProfile.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.strPassImgString = "0"
        
        self.strSaveLatitude = "0"
        self.strSaveLongitude = "0"
        
        self.txtRestaurantName.delegate = self
        self.txtPhone.delegate = self
        self.txtAddress.delegate = self
        
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        btnEditYourProfile.addTarget(self, action: #selector(editRestaurantProfileWB), for: .touchUpInside)
        
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
          // print(person as Any)
            /*
             ["zipCode": , "AutoInsurance": , "dob": , "companyBackground": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1587560500Corona.png, "BankName": djdgsgs, "userId": 99, "socialId": , "gender": , "RoutingNo": vstvush6sg6, "AccountNo": 8505858545884555, "ssnImage": , "contactNumber": 9494645544, "firebaseId": , "image": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1587560500Corona.png, "drivlingImage": , "AccountHolderName": jjsjs, "fullName": restaurent, "address": Gwalior, Madhya Pradesh 474008, India, "state": , "middleName": , "wallet": 0, "role": Restaurant, "country": India, "email": restaurent@gmail.com, "accountType": Saving, "socialType": , "logitude": 78.1694957, "longitude": 78.1694957, "lastName": , "latitude": 26.2313245, "device": , "deviceToken": , "foodTag": Veg]
             */
            
            if (person["role"] as! String) == "Driver" {
                self.btnEditYourBusinessProfile.isHidden = false
                btnEditYourBusinessProfile.addTarget(self, action: #selector(editYourBusinessProfileClickMethod), for: .touchUpInside)
            } else {
                self.btnEditYourBusinessProfile.isHidden = true
            }
            
            self.txtRestaurantName.text = (person["fullName"] as! String)
            self.txtEmail.text = (person["email"] as! String)
            self.txtPhone.text = (person["contactNumber"] as! String)
            self.txtAddress.text = (person["address"] as! String)
            
            imgProfile.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "user"))
            
            
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(REditProfile.cellTappedMethod(_:)))

            imgProfile.isUserInteractionEnabled = true
            // imgProfile.tag = indexPath.row
            imgProfile.addGestureRecognizer(tapGestureRecognizer)
            
            /*if (person["address"] as! String) == "" {
                self.iAmHereForLocationPermission()
            }*/
            
            self.iAmHereForLocationPermission()
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    // MARK:- GET CUSTOMER LOCATION
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            // print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            // print("locality ==> "+locality as Any) // sector 10 dwarka
            
            // print(self.strSaveCountryName as Any) // india
            // print(self.strSaveStateName as Any) // new delhi
            // print(self.strSaveZipcodeName as Any) // 110075
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            
            
            let addressIs:String = String(self.strSaveLocality)+" "+String(self.self.strSaveLocalAddress)
            self.txtAddress.text = String(addressIs)
        }
    }
    
    @objc func editYourBusinessProfileClickMethod() {
       let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDCompleteProfileId") as? PDCompleteProfile
         push!.areYouFromDriverEdit = "yesFromDriverEdit"
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func cellTappedMethod(_ sender:AnyObject){
         print("you tap image number: \(sender.view.tag)")
        
        let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery()
        }))

        alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    @objc func openCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func openGallery() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RAddMenuTableCell
        
        imgProfile.isHidden = false
        
        let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        imgProfile.image = image_data // show image on profile
        let imageData:Data = image_data!.pngData()!
        imageStr = imageData.base64EncodedString()
        self.dismiss(animated: true, completion: nil)
        imgData = image_data!.jpegData(compressionQuality: 0.2)!
        //print(type(of: imgData)) // data
        
        self.strPassImgString = "1"
        
        // self.uploadWithImage()
    }
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    @objc func editRestaurantProfileWB() {
            if self.strPassImgString == "1" {
                self.editWithImageProfileWB()
            } else {
                self.editWithoutImageProfileWB()
            }
    }
    
    // MARK:- RESTAURANT LOGIN -
    @objc func editWithoutImageProfileWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS
      
        /*
         [action] => editprofile
         [userId] => 99
         [fullName] => restaurent2
         [contactNumber] => 9494645544
         [address] => Gwalior, Madhya Pradesh 474008, India
         [device] => Android
         [latitude] => 26.2313245
         [longitude] => 78.1694957
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
            parameters = [
                "action"          : "editprofile",
                "userId"          : String(myString),
                "fullName"        : String(txtRestaurantName.text!),
                "contactNumber"   : String(txtPhone.text!),
                "address"         : String(txtAddress.text!),
                "device"          : String("iOS"),
                "latitude"        : String(self.strSaveLatitude),////String(self.strSaveLatitude),
                "longitude"       : String(self.strSaveLongitude),//"77.0500",//String(self.strSaveLongitude)
            ]
        }
                      print("parameters-------\(String(describing: parameters))")
                      
                      Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                              response in
                  
                              switch(response.result) {
                              case .success(_):
                                 if let data = response.result.value {

                                    let JSON = data as! NSDictionary
                                    print(JSON as Any)
                                  
                                    var strSuccess : String!
                                    strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                                    if strSuccess == String("success") {
                                        print("yes")
                                    
                                     ERProgressHud.sharedInstance.hide()
                                    
                                    
                                    var dict: Dictionary<AnyHashable, Any>
                                    dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                    
                                    let defaults = UserDefaults.standard
                                    defaults.setValue(dict, forKey: "keyLoginFullData")
                                    
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                        }
        }
    }
    
    
    
    // MARK:- EDIT BUSINESS DETAILS WITH IMAGE -
    @objc func editWithImageProfileWB() {
        self.view.endEditing(true)
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Updating...")
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
          
            // var parameters:Dictionary<AnyHashable, Any>!
                 let parameters = [
                     "action"          : "editprofile",
                     "userId"          : String(myString),
                     "fullName"        : String(txtRestaurantName.text!),
                     "contactNumber"   : String(txtPhone.text!),
                     "address"         : String(txtAddress.text!),
                     "device"          : String("iOS")
                 ]
                
            print(parameters as Any)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.imgData, withName: "image",fileName: "expressPlusEditProfilePhoto.jpg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        
                        // let paramsData:Data = NSKeyedArchiver.archivedData(withRootObject: value)
                        
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                },
                to:BASE_URL_EXPRESS_PLUS)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):

                        upload.uploadProgress(closure: { (progress) in
                            //print("Upload Progress: \(progress.fractionCompleted)")
                            
                            /*
                            let alertController = UIAlertController(title: "Uploading image", message: "Please wait......", preferredStyle: .alert)

                            let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)

                            progressDownload.setProgress(Float((progress.fractionCompleted)/1.0), animated: true)
                            progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)

                            alertController.view.addSubview(progressDownload)
                            self.present(alertController, animated: true, completion: nil)
                            */
                        })

                        upload.responseJSON { response in
                            //print(response.result.value as Any)
                            if let data = response.result.value {
                                let JSON = data as! NSDictionary
                                print(JSON)
                                ERProgressHud.sharedInstance.hide()
                                self.dismiss(animated: true, completion: nil)
                                
                                self.strPassImgString = "0"
                                
                                var dict: Dictionary<AnyHashable, Any>
                                dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue(dict, forKey: "keyLoginFullData")
                                
                                // self.navigationController?.popViewController(animated: true)
                            }
                            else {
                                ERProgressHud.sharedInstance.hide()
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        ERProgressHud.sharedInstance.hide()
                        self.dismiss(animated: true, completion: nil)
                    }}}
        
    }
    
    
    
    
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
