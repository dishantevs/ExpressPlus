//
//  REditBusinessDetails.swift
//  ExpressPlus
//
//  Created by Apple on 28/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class REditBusinessDetails: UIViewController {

    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "EDIT BUSINESS PROFILE"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var viewBgOne:UIView! {
        didSet {
            viewBgOne.addDashedBorder()
        }
    }
    @IBOutlet weak var viewBgTwo:UIView! {
        didSet {
            viewBgTwo.addDashedBorder()
        }
    }
    @IBOutlet weak var viewBgThree:UIView! {
        didSet {
            viewBgThree.addDashedBorder()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
}
