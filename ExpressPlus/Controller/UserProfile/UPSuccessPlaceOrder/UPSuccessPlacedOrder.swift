//
//  UPSuccessPlacedOrder.swift
//  ExpressPlus
//
//  Created by Apple on 21/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UPSuccessPlacedOrder: UIViewController {

    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "SUCCESS"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnPlaceOrder:UIButton! {
        didSet {
            btnPlaceOrder.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnPlaceOrder.setTitleColor(.white, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnPlaceOrder.addTarget(self, action: #selector(checkUncheckClickMethod), for: .touchUpInside)
    }
    
    @objc func checkUncheckClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPFoodId")
        self.navigationController?.pushViewController(push, animated: true)
    }
    
}
