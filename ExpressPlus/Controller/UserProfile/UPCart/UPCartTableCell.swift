//
//  UPCartTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 07/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import ValueStepper

class UPCartTableCell: UITableViewCell {

    
    
    @IBOutlet weak var imgProfilee:UIImageView! {
        didSet {
            imgProfilee.layer.cornerRadius = 10
            imgProfilee.clipsToBounds = true
            imgProfilee.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var stepper1: ValueStepper! {
        didSet {
            stepper1.enableManualEditing = true
            // stepper1.isHidden = true
            stepper1.disabledIconButtonColor = .systemGreen
            stepper1.tintColor = .systemGreen
        }
    }
    
    @IBOutlet weak var imgVegNonveg:UIImageView!  {
           didSet {
               imgVegNonveg.backgroundColor = .clear
           }
       }
    
    @IBOutlet weak var lblFoodName:UILabel!
    @IBOutlet weak var lblOldPrice:UILabel!
    @IBOutlet weak var lblRealPrice:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
