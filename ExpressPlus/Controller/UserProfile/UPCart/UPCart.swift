//
//  UPCart.swift
//  ExpressPlus
//
//  Created by Apple on 07/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import ValueStepper

import Alamofire
import SDWebImage
    
class UPCart: UIViewController {

    let cellReuseIdentifier = "uPCartTableCell"
    
    // MARK:- ARRAY - 
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
       
    var page : Int! = 1
    var loadMore : Int! = 1
       
    var strContactOrNot:String!
    
    var addInitialMutable:NSMutableArray = []
    
    
    var strSumTotalIs:String!
    var sumTotalInt:Int!
    
    
    var intAddValueOne:Int!
    var intAddValueTwo:Int!
    var intAddValueThree:Int!
    
    
    var intAddValueOne1:NSNumber!
    var intAddValueTwo1:NSNumber!
    var intAddValueThree1:NSNumber!
    
    
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblTotalItemInCart:UILabel! {
        didSet {
            lblTotalItemInCart.textColor = .systemRed
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "CART"
        }
    }
    
    @IBOutlet weak var btnCheckUncheck:UIButton!
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var tbleView:UITableView!
    
    @IBOutlet weak var txtView:UITextView! {
        didSet {
            txtView.text = ""
            txtView.layer.cornerRadius = 4
            txtView.clipsToBounds = true
            txtView.layer.borderColor = UIColor.lightGray.cgColor
            txtView.layer.borderWidth = 0.08
            txtView.backgroundColor = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var btnPlaceOrder:UIButton! {
        didSet {
            btnPlaceOrder.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnPlaceOrder.setTitleColor(.white, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.intAddValueOne = 0
        self.intAddValueTwo = 0
        self.intAddValueThree = 0
        
        self.btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        
        self.btnCheckUncheck.tag = 0
        self.strContactOrNot = "0"
        self.btnCheckUncheck.addTarget(self, action: #selector(checkUncheckClickMethod), for: .touchUpInside)
        self.btnPlaceOrder.addTarget(self, action: #selector(placeOrderClickMethod), for: .touchUpInside)
        
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false

        view.addGestureRecognizer(tap)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.cartListInitial()
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    @objc func checkUncheckClickMethod() {
        if btnCheckUncheck.tag == 0 {
            self.btnCheckUncheck.setImage(UIImage(named: "checkCustomerContact"), for: .normal)
            self.strContactOrNot = "1"
            self.btnCheckUncheck.tag = 1
        } else if btnCheckUncheck.tag == 1 {
            self.btnCheckUncheck.setImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
            self.strContactOrNot = "0"
            self.btnCheckUncheck.tag = 0
        }
    }
    
    @objc func placeOrderClickMethod() {
        // strContactOrNot
        // self.addInitialMutable
        // txtView.text
        
        /*
         var strSaveContactOnDelivery:String!
         var strSaveSpecialNotes:String!
         var strSaveMutable:NSMutableArray = []
         */
        
        /*let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPTipForDeliveryGuyId") as? UPTipForDeliveryGuy
        push!.strSaveContactOnDelivery = strContactOrNot
        push!.strSaveSpecialNotes = String(txtView.text)
        push!.staticSubTotalPrice = String(self.strSumTotalIs)
        push!.mArrSaveMutable = self.addInitialMutable
        self.navigationController?.pushViewController(push!, animated: true)*/
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPTipForDeliveryGuyTwoId") as? UPTipForDeliveryGuyTwo
        push!.strSaveContactOnDelivery = strContactOrNot
        push!.strSaveSpecialNotes = String(txtView.text)
        push!.staticSubTotalPrice = String(self.strSumTotalIs)
         push!.mArrSaveMutable = self.addInitialMutable
        push!.mArrSaveMutableForFoodImage = self.arrListOfAllMyOrders
        
        self.navigationController?.pushViewController(push!, animated: true)
        
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }

    // MARK:- WEBSERVICE ( CART LIST ) -
    @objc func cartListInitial() {
        self.addInitialMutable.removeAllObjects()
        self.arrListOfAllMyOrders.removeAllObjects()
     
     ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
               
     // let indexPath = IndexPath.init(row: 0, section: 0)
     // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
     // print(dictGetRestaurantFoodListing as Any)
     
     /*
      [action] => getcarts
      [userId] => 80
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
                   parameters = [
                       "action"              : "getcarts",
                       "userId"              : person["userId"] as Any
            ]
        }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                   { [self]
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 // var dict: Dictionary<AnyHashable, Any>
                                 // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                
                                
                                if self.arrListOfAllMyOrders.count == 0 {
                                    
                                    self.navigationController?.popViewController(animated: true)
                                    
                                } else {
                                    
                                
                                    /*
                                 let x : Int = JSON["TotalCartItem"] as! Int
                                 let myString = String(x)
                                 
                                
                                 if myString == "0" {
                                     self.lblTotalItemInCart.isHidden = true
                                 } else if myString == "" {
                                     self.lblTotalItemInCart.isHidden = true
                                 } else if myString == "1" {
                                     self.lblTotalItemInCart.isHidden = false
                                     self.lblTotalItemInCart.text = String(myString)+" Item"
                                 } else {
                                    self.lblTotalItemInCart.isHidden = false
                                    self.lblTotalItemInCart.text = String(myString)+" Items"
                                }
                                    
                                    */
                                
                                    if JSON["TotalCartItem"] is String {
                                      print("Yes, it's a String")
                                        
                                        self.lblTotalItemInCart.text = JSON["TotalCartItem"] as! String+" Item"
                                        
                                    } else if JSON["TotalCartItem"] is Int {
                                      print("It is Integer")
                                        let x : Int = JSON["TotalCartItem"] as! Int
                                        let myString = String(x)
                                        
                                        if myString == "0" {
                                             self.lblTotalItemInCart.isHidden = true
                                         } else if myString == "" {
                                             self.lblTotalItemInCart.isHidden = true
                                         } else if myString == "1" {
                                             self.lblTotalItemInCart.isHidden = false
                                             self.lblTotalItemInCart.text = String(myString)+" Item"
                                         } else {
                                            self.lblTotalItemInCart.isHidden = false
                                            self.lblTotalItemInCart.text = String(myString)+" Items"
                                        }
                                        
                                        
                                    } else if JSON["TotalCartItem"] is NSNumber {
                                      print("It is nsnimber")
                                      //some other check
                                        
                                    }
                                    
                                    for specialPriceIndex in 0..<ar.count {
                                        let item = ar[specialPriceIndex] as? [String:Any]
                                        
                                        if item!["specialPrice"] is String {
                                        
                                        print("Yes, it's a String")
                                        // cell.lblRealPrice.text = "$ "+(item!["specialPrice"] as! String)

                                    } else if item!["specialPrice"] is Int {

                                        print("It is Integer")
                                        // let x : Int = item!["specialPrice"] as! Int
                                        // let myString = String(x)
                                        // cell.lblRealPrice.text = "$ "+String(myString)
                                            // print(String(myString))
                                        
                                            print(item!["specialPrice"] as! Int)
                                            print(item!["quantity"] as! Int)
                                            
                                            self.intAddValueOne = ((item!["specialPrice"] as! Int)+self.intAddValueTwo)*(item!["quantity"] as! Int)
                                            
                                            self.intAddValueTwo = self.intAddValueOne
                                            
                                            print(self.intAddValueTwo as Any)
                                            
                                            let x : Int = self.intAddValueTwo!
                                            let myString = String(x)
                                            
                                            self.strSumTotalIs = String(myString)
                                           
                                    } else if item!["specialPrice"] is NSNumber {

                                      print("It is nsnimber")
                                      //some other check

                                            if item!["quantity"] is NSNumber {
                                                
                                                print("yes")
                                            }
                                           
                                            let num1 = item!["specialPrice"] as! Double
                                            let num2 = item!["quantity"] as! Int

                                            let a = Double(num2)
                                            let b = num1
                                            let c = a * b
                                           
                                            let d = String(format: "%.2f", c)
                                            
                                            self.strSumTotalIs = "$ "+d
                                            
                                        
                                        
                                    }
                                        
                                    }
                                    
                                    
                                    
                                 self.tbleView.delegate = self
                                 self.tbleView.dataSource = self
                                 self.tbleView.reloadData()
                                 self.loadMore = 1;
                                }
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }

    // MARK:- WEBSERVICE ( ADD TO CART ) -
    @objc func addToCartWB(strFoodId:String,strQuantity:String) {
        self.addInitialMutable.removeAllObjects()
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
        let urlString = BASE_URL_EXPRESS_PLUS
     
     /*
      [action] => addcart
      [userId] => 80
      [foodId] => 48
      [quantity] => 1
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
                   parameters = [
                       "action"              : "addcart",
                       "foodId"              : String(strFoodId),
                       "userId"              : person["userId"] as Any,
                       "quantity"            : String(strQuantity)
             ]
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 // ERProgressHud.sharedInstance.hide()
                                 
                                 // var dict: Dictionary<AnyHashable, Any>
                                 // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                
                                 let x : Int = JSON["totalCartItem"] as! Int
                                 let myString = String(x)
                                 
                                 if myString == "0" {
                                     self.lblTotalItemInCart.isHidden = true
                                 } else if myString == "" {
                                     self.lblTotalItemInCart.isHidden = true
                                 } else if myString == "1" {
                                      self.lblTotalItemInCart.isHidden = false
                                      self.lblTotalItemInCart.text = String(myString)+" Item"
                                  } else {
                                     self.lblTotalItemInCart.isHidden = false
                                     self.lblTotalItemInCart.text = String(myString)+" Items"
                                 }
                                
                                
                                
                                
                                
                                self.cartCallAfterAddSomeItem()
                                
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
       }
    
    
    // MARK:- WEBSERVICE ( CART LIST ) -
    @objc func cartCallAfterAddSomeItem() {
        self.addInitialMutable.removeAllObjects()
     self.arrListOfAllMyOrders.removeAllObjects()
     
        self.intAddValueOne = 0
        self.intAddValueTwo = 0
        self.intAddValueThree = 0
        
     ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
               
     // let indexPath = IndexPath.init(row: 0, section: 0)
     // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
     // print(dictGetRestaurantFoodListing as Any)
     
     
     /*
      [action] => getcarts
      [userId] => 80
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
            parameters = [
                "action"              : "getcarts",
                "userId"              : person["userId"] as Any
                       
            ]
        }
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
        {
            response in
               
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 var ar : NSArray!
                                 ar = (JSON["data"] as! Array<Any>) as NSArray
                                 self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                
                                // print(self.arrListOfAllMyOrders.count)
                                
                                if self.arrListOfAllMyOrders.count == 0 {
                                    self.navigationController?.popViewController(animated: true)
                                } else {
                                    
                                 let x : Int = JSON["TotalCartItem"] as! Int
                                 let myString = String(x)
                                 
                                
                                 if myString == "0" {
                                     self.lblTotalItemInCart.isHidden = true
                                 } else if myString == "" {
                                     self.lblTotalItemInCart.isHidden = true
                                 } else if myString == "1" {
                                      self.lblTotalItemInCart.isHidden = false
                                      self.lblTotalItemInCart.text = String(myString)+" Item"
                                  } else {
                                     self.lblTotalItemInCart.isHidden = false
                                     self.lblTotalItemInCart.text = String(myString)+" Items"
                                 }
                                for specialPriceIndex in 0..<ar.count {
                                    let item = ar[specialPriceIndex] as? [String:Any]
                                    
                                    if item!["specialPrice"] is String {
                                    
                                    print("Yes, it's a String")
                                    // cell.lblRealPrice.text = "$ "+(item!["specialPrice"] as! String)

                                } else if item!["specialPrice"] is Int {

                                    print("It is Integer")
                                    // let x : Int = item!["specialPrice"] as! Int
                                    // let myString = String(x)
                                    // cell.lblRealPrice.text = "$ "+String(myString)
                                        // print(String(myString))
                                    
                                    print(item!["specialPrice"] as! Int)
                                        
                                    self.intAddValueOne = ((item!["specialPrice"] as! Int)+self.intAddValueTwo)*(item!["quantity"] as! Int)
                                        
                                    self.intAddValueTwo = self.intAddValueOne
                                        
                                    print(self.intAddValueTwo as Any)
                                    print(self.intAddValueTwo as Any)
                                        
                                    let x : Int = self.intAddValueTwo!
                                    let myString = String(x)
                                        
                                    self.strSumTotalIs = String(myString)
                                       
                                } else if item!["specialPrice"] is NSNumber {

                                    print("It is nsnimber")
                                  //some other check

                                    if item!["quantity"] is NSNumber {
                                            
                                        print("yes")
                                    }
                                       
                                    let num1 = item!["specialPrice"] as! Double
                                    let num2 = item!["quantity"] as! Int

                                    let a = Double(num2)
                                    let b = num1
                                    let c = a * b
                                       
                                    let d = String(format: "%.2f", c)
                                        
                                    self.strSumTotalIs = "$ "+d
                                    
                                    
                                }
                                    
                                }
                                 
                                 self.tbleView.delegate = self
                                 self.tbleView.dataSource = self
                                 self.tbleView.reloadData()
                                 self.loadMore = 1
                                    
                                }
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    
    
    
    // MARK:- WEBSERVICE ( CLEAR CART TO ZERO ) -
    @objc func clearThatFoodItemCartToZero(strFoodId:String,strQuantity:String) {
        self.addInitialMutable.removeAllObjects()
     self.arrListOfAllMyOrders.removeAllObjects()
     
      ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => deletecarts
      [userId] => 80
      [foodId] => 49
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
            parameters = [
                "action"              : "deletecarts",
                "foodId"              : String(strFoodId),
                "userId"              : person["userId"] as Any
                       
            ]
        }
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
               
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                               
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                    if strSuccess == String("success") {
                        print("yes")
                                 
                                 // ERProgressHud.sharedInstance.hide()
                                 
                                 // var dict: Dictionary<AnyHashable, Any>
                                 // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                        let x : Int = JSON["totalCartItem"] as! Int
                        let myString = String(x)
                                 
                        if myString == "0" {
                            self.lblTotalItemInCart.isHidden = true
                        } else if myString == "" {
                            self.lblTotalItemInCart.isHidden = true
                        } else if myString == "1" {
                            self.lblTotalItemInCart.isHidden = false
                            self.lblTotalItemInCart.text = String(myString)+" Item"
                        } else {
                            self.lblTotalItemInCart.isHidden = false
                            self.lblTotalItemInCart.text = String(myString)+" Items"
                        }
                                 
                        self.cartCallAfterAddSomeItem()
                                 
                                
                                 /*
                                 var ar : NSArray!
                                 ar = (JSON["data"] as! Array<Any>) as NSArray
                                 self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                 
                                 self.clView.delegate = self
                                 self.clView.dataSource = self
                                 self.clView.reloadData()
                                 self.loadMore = 1;
                                 */
                                 
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                                
                                
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                               
                ERProgressHud.sharedInstance.hide()
                               
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                               
                alertController.addAction(okAction)
                               
                self.present(alertController, animated: true, completion: nil)
                               
                break
            }
        }
    
    }
    
    
    
}


//MARK:- TABLE VIEW -
extension UPCart: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UPCartTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UPCartTableCell
        
        cell.backgroundColor = .white
        
       /*
         description = solid;
         foodId = 48;
         foodImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/foods/1588662121IMG20191105144302.jpg";
         foodName = dosa;
         foodTag = "";
         price = "70.00";
         quantity = 2;
         resturentId = 104;
         specialPrice = "65.00";
         userId = 80;
         */
        
        // cell.lblTitle.text = titleArray[indexPath.row]
        
       let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        
        cell.lblFoodName.text = (item!["foodName"] as! String)

        if item!["foodTag"] as! String == "Veg" {
            cell.imgVegNonveg.image = UIImage(named: "veg")
        } else if item!["foodTag"] as! String == "" {
            cell.imgVegNonveg.image = UIImage(named: "veg")
        } else {
            cell.imgVegNonveg.image = UIImage(named: "nonVeg")
        }
        
        // cell.lblRealPrice.text = "PRICE : "+(item!["specialPrice"] as! String)
        
        
        
        
        if item!["specialPrice"] is String {
            
            print("Yes, it's a String")
            cell.lblRealPrice.text = "$ "+(item!["specialPrice"] as! String)
            
        } else if item!["specialPrice"] is Int {
            
            print("It is Integer")
            // let x : Int = item!["specialPrice"] as! Int
            // let myString = String(x)
            
            let num1 = item!["specialPrice"] as! Double
            let num2 = item!["quantity"] as! Int

            let a = Double(num2)
            let b = num1
            let c = a * b
           
           let d = String(format: "%.2f", c)
            
           self.strSumTotalIs = d
           
           cell.lblRealPrice.text = "$ "+String(self.strSumTotalIs)
            // cell.lblRealPrice.text = "$ "+String(myString)
            
        } else if item!["specialPrice"] is NSNumber {
            
          print("It is nsnimber")
          //some other check
            
            if item!["quantity"] is NSNumber {
                 
                 print("yes")
            }
            
             let num1 = item!["specialPrice"] as! Double
             let num2 = item!["quantity"] as! Int

             let a = Double(num2)
             let b = num1
             let c = a * b
            
            let d = String(format: "%.2f", c)
             
            self.strSumTotalIs = d
            
            cell.lblRealPrice.text = "$ "+String(self.strSumTotalIs)
            
        }
        
        
        
        
        
        // food id
        let x4 : Int = item!["foodId"] as! Int
        let myString4 = String(x4)
        
        // array quantity
        let x5 : Int = item!["quantity"] as! Int
        let myString5 = String(x5)
        
        // array res id
        let x6 : Int = item!["resturentId"] as! Int
        let myString6 = String(x6)
        
        // specialPrice
        // let xSpecialPrice : Int = item!["specialPrice"] as! Int
        // let myStringSpecialPrice = String(xSpecialPrice)
        
        
        let myStringSpecialPrice:String!
        
        
        if item!["specialPrice"] is String {
                            
            print("Yes, it's a String")
            myStringSpecialPrice = (item!["specialPrice"] as! String)
            
        } else if item!["specialPrice"] is Int {
            
            print("It is Integer")
            let x2 : Int = (item!["specialPrice"] as! Int)
            let myString2 = String(x2)
            myStringSpecialPrice = String(myString2)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = item!["specialPrice"] as! NSNumber
            let tempString = temp.stringValue
            myStringSpecialPrice = String(tempString)
            
        }
        
        
        
        
        
        let myDictionary: [String:String] = [
            "id":String(myString4),
            "name":(item!["foodName"] as! String),
            "price":String(myStringSpecialPrice),
            "quantity":String(myString5),
            "resturentId":String(myString6),
        ]
        
        var res = [[String: String]]()
        res.append(myDictionary)
        
        self.addInitialMutable.addObjects(from: res)
        
        
        
        // quantity
        let x3 : Int = item!["quantity"] as! Int
        let myString3 = String(x3)
        if myString3 == "0" {
            cell.stepper1.isHidden = true
        } else {
            cell.stepper1.isHidden = false
            
            let morePrecisePI = Double(myString3)
            cell.stepper1.value = morePrecisePI!
        }
        
        cell.stepper1.tag = indexPath.row
        cell.stepper1.addTarget(self, action: #selector(valueChanged1), for: .valueChanged)
        
        
        
        // cell.imgProfilee.sd_setImage(with: URL(string: (item!["foodImage"] as! String)), placeholderImage: UIImage(named: "avatar"))
        
        cell.imgProfilee.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        
        if let url = (item!["foodImage"] as! String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            
         // cell.imgFoodProfile.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "food1"), options: [.continueInBackground, .progressiveDownload])
             cell.imgProfilee.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "food1"))
            
        }
        
        return cell
    }
    
    @objc func valueChanged1(_ sender: ValueStepper) {
        let step:ValueStepper = sender
       
        let item = arrListOfAllMyOrders[step.tag] as? [String:Any]
        // print(item as Any)
        
        let someValue: Float = Float(step.value) // remove deciaml value from double or float
        let double = someValue.cleanValue
        let doubleString = String(double) // convert double or float into string
        
        // convert menu id to string
        let x : Int = item!["foodId"] as! Int
        let myString = String(x)
        
        if doubleString == "0" {
             self.clearThatFoodItemCartToZero(strFoodId: String(myString), strQuantity: doubleString)
        } else {
             self.addToCartWB(strFoodId: String(myString), strQuantity: doubleString)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
}

extension UPCart: UITableViewDelegate {
    
}

