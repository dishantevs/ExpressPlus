//
//  UPTipForDeliveryGuyTwo.swift
//  ExpressPlus
//
//  Created by Apple on 06/01/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class UPTipForDeliveryGuyTwo: UIViewController, UITextFieldDelegate {

    let SELECT_COLOR = UIColor.init(red: 116.0/255.0, green: 233.0/255.0, blue: 139.0/255.0, alpha: 1)
    let UN_SELECT_COLOR = UIColor.init(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1)
    
    let cellReuseIdentifier = "uPTipForDeliveryGuyTwoTableCell"
    
    // MARK:- GET ALL VALUE FROM CART -
    var strSaveContactOnDelivery:String!
    var strSaveSpecialNotes:String!
    var mArrSaveMutable:NSMutableArray = []
    var mArrSaveMutableForFoodImage:NSMutableArray = []
    
    var strTotalPrice:String!
    
    var staticSubTotalPrice:String!
    var myTipAmount:String!
    
    var strSpecialPrice2:String!
    
    var arrGetCarditems:NSMutableArray = []
    
    // send total amount fetch and send to the server
    var modifiedTotalAmounToPaySendToServer:String!
    
    var saveTransactionFee:String!
    
    var finalTotalPriceFetch:String!
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblTotalItemInCart:UILabel! {
        didSet {
            lblTotalItemInCart.textColor = .systemRed
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "CART"
        }
    }
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
            tbleView.backgroundColor = .clear
            tbleView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = UIColor.init(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1)
        
        self.saveTransactionFee = "0"
        
        self.serverValueFetch()
    }

    @objc func serverValueFetch() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPTipForDeliveryGuyTwoTableCell
        
        self.btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        cell.btnPlaceOrder.addTarget(self, action: #selector(placeOrder), for: .touchUpInside)
        
        self.tbleView.separatorColor = .clear
        
        self.myTipAmount = "0"
        // cell.lblSelectedTipAmount.text = "$0" ( temp comment )
        
        cell.txtApply.delegate = self
        cell.txtother.delegate = self
        
        print(mArrSaveMutableForFoodImage as Any)
        print(mArrSaveMutableForFoodImage.count as Any)
        
        if String(mArrSaveMutable.count) == "1" {
            cell.lblTotalItemInCart.text = " "+String(mArrSaveMutable.count)+" item"
        } else {
            cell.lblTotalItemInCart.text = " "+String(mArrSaveMutable.count)+" items"
        }
        
        cell.txtother.isUserInteractionEnabled = false
        cell.btnFive.backgroundColor    = UN_SELECT_COLOR
        cell.btnTen.backgroundColor     = UN_SELECT_COLOR
        cell.btnFifteen.backgroundColor = UN_SELECT_COLOR
        cell.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        cell.btnOther.backgroundColor   = UN_SELECT_COLOR
        
        cell.btnFive.addTarget(self, action: #selector(addFiveClick), for: .touchUpInside)
        cell.btnTen.addTarget(self, action: #selector(addTenClick), for: .touchUpInside)
        cell.btnFifteen.addTarget(self, action: #selector(addFifteenClick), for: .touchUpInside)
        cell.btnTwenty.addTarget(self, action: #selector(addTwentyClick), for: .touchUpInside)
        cell.btnOther.addTarget(self, action: #selector(addOtherClick), for: .touchUpInside)
        
        cell.btnDoneOther.isHidden = true
        
         
        
         
        /*var sum2:Double!
        sum2 = 0
        
        for n in 0...mArrSaveMutable.count-1 {
            
            let item = mArrSaveMutable[n] as? [String:Any]
            
            // multiply quantity with price
            let multiplyQuantityWithPrice = Double(item!["price"] as! String)!*Double(item!["quantity"] as! String)!
            
            sum2 += Double(multiplyQuantityWithPrice)
            
        }
        
        let addTotalPriceWithDeliveryCharge = Double(sum2)+Double("6")!
        // print(addTotalPriceWithDeliveryCharge as Any)
        
        strTotalPrice = String(addTotalPriceWithDeliveryCharge)
        print(strTotalPrice as Any)*/
        
        self.calculateFullTotalPriceWithoutTip()
        
        /*// convert string into int ( add 6 on total price )
        let editedText2 = staticSubTotalPrice.replacingOccurrences(of: ",", with: "")
        print(editedText2 as Any)
        
        let string = editedText2
        if string.contains(".") {
            print("exists")
            
            // let num1 = item!["specialPrice"] as! Double
             // let num2 = item!["quantity"] as! Int
            let myString = editedText2
            let myFloat = (myString as NSString).doubleValue
            
            
            let a = Double(6)
            let b = myFloat
            let c = a + b
            
            let d = String(format: "%.2f", c)
            // print(d)
            // total amount show when user didn't select tip amount
            cell.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+d
            
            self.modifiedTotalAmounToPaySendToServer = d
            
            self.strTotalPrice = d
            cell.lblSubTotal.text = "$ "+editedText2
            // self.lblSubTotal.text = "$ "+d
            print(self.strTotalPrice as Any)
            
            self.lblTransactionFee(strTotalAmount: self.strTotalPrice)
            
        } else {
            print("no")
            
            let myString1 = editedText2
            let myInt1 = Int(myString1)
            print(myInt1 as Any)
            
            // MARK:- FOOD PRICE WIHOUT TIP -
            // food price without tip is 'myInt1'
            let foodPriceWithoutTipInInt : Int = myInt1 ?? 00
            let strPriceWihoutTip = String(foodPriceWithoutTipInInt)
            cell.lblSubTotal.text = "$ "+strPriceWihoutTip
           
            
            let addDeliveryToTotalAmount = myInt1!+6
            
            let convertToalAmountWithDeliveryChargeToString : Int = addDeliveryToTotalAmount
            let resultOf = String(convertToalAmountWithDeliveryChargeToString)
            
            let cv: String = String(format: "%.2f", resultOf)
            
            // total amount show when user didn't select tip amount
            cell.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+cv
            
            self.modifiedTotalAmounToPaySendToServer = resultOf
            
            self.strTotalPrice = resultOf
            
            // print(self.strTotalPrice as Any)
            
            // this method calls for transaction fee
            self.lblTransactionFee(strTotalAmount: self.strTotalPrice)
            
        }*/
    }
    
    
    
    // MARK:- CALCULATE FULL PRICE WITHOUT TIP -
    @objc func calculateFullTotalPriceWithoutTip() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPTipForDeliveryGuyTwoTableCell
        
        var sum2:Double!
        sum2 = 0
        
        for n in 0...mArrSaveMutable.count-1 {
            
            let item = mArrSaveMutable[n] as? [String:Any]
            
            // multiply quantity with price
            let multiplyQuantityWithPrice = Double(item!["price"] as! String)!*Double(item!["quantity"] as! String)!
            
            sum2 += Double(multiplyQuantityWithPrice)
            
        }
        
        // print(sum2 as Any)
        cell.lblSubTotal.text = "$ "+String(sum2)
        
        
        
        self.addDeliveryFeeWithTotalPrice(strTotalPriceWithoutDeliveryFee: sum2, strDeliveryFee: "6")
    }
    
    // MARK:- DELIVERY FEE ADD AFTER GETTING TOTAL PRICE -
    @objc func addDeliveryFeeWithTotalPrice(strTotalPriceWithoutDeliveryFee:Double,strDeliveryFee:String) {
        
        let addTotalPriceWithDeliveryCharge = Double(strTotalPriceWithoutDeliveryFee)+Double(strDeliveryFee)!
        // print(addTotalPriceWithDeliveryCharge as Any)
        
        strTotalPrice = String(addTotalPriceWithDeliveryCharge)
        print(strTotalPrice as Any)
        
        self.finalTotalPriceFetch = self.strTotalPrice
        
        self.lblTransactionFee(strTotalAmount: strTotalPrice)
    }
    
   
    
    // MARK:- TRANSACTION FEE -
    @objc func lblTransactionFee(strTotalAmount:String) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPTipForDeliveryGuyTwoTableCell
        
        /*
         total price ka 5.8 percent calculate karna hai jo amount hoga usme +.60 cent add karna hai phir wo total amount m add karna hai
         */
        
        print(strTotalAmount as Any)
        
        let convertTotalPriceToDouble = Double(strTotalAmount)!
        
        // calculate total price
        // formula = totalPrice *5.8%
        
        // 0.058
        // why 0.058 bcause it get value from 5.8%
        // 5.8% = 0.058
        let multiplyTractactionFeeWithTotalAmount = convertTotalPriceToDouble * 0.058
        print(multiplyTractactionFeeWithTotalAmount as Any)
        
        // convert multiple double values into two digits
        print(String(format:"%.02f", multiplyTractactionFeeWithTotalAmount))
        
        let getValueAfterMultiplyByFivePointEightPercentage = String(format:"%.02f", multiplyTractactionFeeWithTotalAmount)
        print(getValueAfterMultiplyByFivePointEightPercentage as Any)
        
        // now add .60 with result
        let cents = 0.60
        
        let addCentsWithMultiplicationResult = Double(getValueAfterMultiplyByFivePointEightPercentage)!+cents
        print(addCentsWithMultiplicationResult as Any)
        
        // round figure of result
        let roundFigureOfTotalCalulation = String(format:"%.02f", addCentsWithMultiplicationResult)
        print(roundFigureOfTotalCalulation as Any)
        
        // final price for Transaction Fee
        let transactionFeeIs = roundFigureOfTotalCalulation
        // print(transactionFeeIs as Any)
        
        cell.lblTransactionFee.text = "$ "+String(transactionFeeIs)
        // print(self.strTotalPrice as Any)
        // print(self.strTotalPrice as Any)
        // add transaction fee with total amount
        let totalAmountAfterGetTransactionFee = Double(strTotalAmount)!+Double(transactionFeeIs)!
        // print(totalAmountAfterGetTransactionFee as Any)
        
        let cv: String = String(format: "%.2f", totalAmountAfterGetTransactionFee)
        cell.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+String(cv)
        
        self.strTotalPrice = String(cv)
        print(self.strTotalPrice as Any)
        self.saveTransactionFee = String(transactionFeeIs)
        
        self.modifiedTotalAmounToPaySendToServer = String(self.strTotalPrice)
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func addFiveClick() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPTipForDeliveryGuyTwoTableCell
        
        cell.txtother.isUserInteractionEnabled = false
        cell.btnFive.backgroundColor    = SELECT_COLOR
        cell.btnTen.backgroundColor     = UN_SELECT_COLOR
        cell.btnFifteen.backgroundColor = UN_SELECT_COLOR
        cell.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        cell.btnOther.backgroundColor   = UN_SELECT_COLOR
        
        cell.btnDoneOther.isHidden = true
        
        self.calculateFinalTotalAfterTip(strTipAmount: "5")
    }
    
    @objc func addTenClick() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPTipForDeliveryGuyTwoTableCell
        
        cell.txtother.isUserInteractionEnabled  = false
        cell.btnFive.backgroundColor    = UN_SELECT_COLOR
        cell.btnTen.backgroundColor     = SELECT_COLOR
        cell.btnFifteen.backgroundColor = UN_SELECT_COLOR
        cell.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        cell.btnOther.backgroundColor   = UN_SELECT_COLOR
        
        cell.btnDoneOther.isHidden = true
        
        self.calculateFinalTotalAfterTip(strTipAmount: "10")
    }
    
    @objc func addFifteenClick() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPTipForDeliveryGuyTwoTableCell
        
        cell.txtother.isUserInteractionEnabled = false
        cell.btnFive.backgroundColor    = UN_SELECT_COLOR
        cell.btnTen.backgroundColor     = UN_SELECT_COLOR
        cell.btnFifteen.backgroundColor = SELECT_COLOR
        cell.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        cell.btnOther.backgroundColor   = UN_SELECT_COLOR
        
        cell.btnDoneOther.isHidden = true
        
        self.calculateFinalTotalAfterTip(strTipAmount: "15")
    }
    
    @objc func addTwentyClick() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPTipForDeliveryGuyTwoTableCell
        
        cell.txtother.isUserInteractionEnabled = false
        cell.btnFive.backgroundColor    = UN_SELECT_COLOR
        cell.btnTen.backgroundColor     = UN_SELECT_COLOR
        cell.btnFifteen.backgroundColor = UN_SELECT_COLOR
        cell.btnTwenty.backgroundColor  = SELECT_COLOR
        cell.btnOther.backgroundColor   = UN_SELECT_COLOR
        
        cell.btnDoneOther.isHidden = true
        
        self.calculateFinalTotalAfterTip(strTipAmount: "20")
    }
    
    @objc func addOtherClick() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPTipForDeliveryGuyTwoTableCell
        
        cell.txtother.isUserInteractionEnabled = true
        cell.btnFive.backgroundColor    = UN_SELECT_COLOR
        cell.btnTen.backgroundColor     = UN_SELECT_COLOR
        cell.btnFifteen.backgroundColor = UN_SELECT_COLOR
        cell.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        cell.btnOther.backgroundColor   = SELECT_COLOR
        
        cell.btnDoneOther.isHidden = false
        cell.btnDoneOther.addTarget(self, action: #selector(doneOther2), for: .touchUpInside)
        
        cell.lblSelectedTipAmount.text = "$ 0"
        
        // self.myTipAmount = String(cell.lblSelectedTipAmount.text!)
        
        self.lblTransactionFee(strTotalAmount: self.strTotalPrice)
        
        /*
        var strSpecialPrice:String!
        var strQuantity:String!
        
        var sum2 = 0
        
        for n in 0...mArrSaveMutable.count-1 {
            let item = mArrSaveMutable[n] as? [String:Any]
            
            strSpecialPrice = (item!["price"] as! String)
            strQuantity = (item!["quantity"] as! String)
           
            let a = (strSpecialPrice as NSString).floatValue
            let b = (strQuantity as NSString).floatValue
            let sum = a * b
            
            sum2 += Int(sum)
        }
        let x : Int = sum2
        strTotalPrice = String(x)
        self.lblSubtotal.text = "$ "+String(strTotalPrice)
        
        */
    }
    
    
    
    @objc func calculateFinalTotalAfterTip(strTipAmount:String) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPTipForDeliveryGuyTwoTableCell
        
        
        let tipAmountSelected = Double(strTipAmount)!
        print(tipAmountSelected as Any)
        
        cell.txtother.text = ""
        
        self.myTipAmount = "\(tipAmountSelected)"
        
        let addTipAmountWithTotalAmountAfterEverything = Double(tipAmountSelected)+Double(self.finalTotalPriceFetch)!
        print(addTipAmountWithTotalAmountAfterEverything as Any)
        
        let cv: String = String(format: "%.2f", addTipAmountWithTotalAmountAfterEverything)
        print(cv as Any)
        
        self.strTotalPrice = String(cv)
        print(self.strTotalPrice as Any)
        
        self.modifiedTotalAmounToPaySendToServer = String(self.strTotalPrice)
        
        cell.lblSelectedTipAmount.text = "$ "+String(tipAmountSelected)
        
        self.lblTransactionFee(strTotalAmount: String(self.strTotalPrice))
        
        
        
        
        /*let editedText = self.strTotalPrice.replacingOccurrences(of: "$ ", with: "")
        
        let valueone = strTipAmount
        let myInt1 = Int(valueone)
        
        let valueTwo = editedText
        
        print(valueTwo as Any)
        let string = valueTwo
        if string.contains(".") {
            
            let myString = string
            let myFloat = (myString as NSString).doubleValue
            
            let a = Double(strTipAmount)
            let b = myFloat
            let c = a! + b
            
            let cv: String = String(format: "%.2f", c)
            
            cell.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+String(cv)
            
            cell.lblSelectedTipAmount.text = "$"+strTipAmount
            
            self.modifiedTotalAmounToPaySendToServer = String(c)
            
            self.myTipAmount = strTipAmount
            
            // this method calls for transaction fee
             self.lblTransactionFee(strTotalAmount: String(c))
        } else {
            
            let myInt2 = Int(valueTwo)
            let x2 : Int = Int(myInt1!+myInt2!)
            // self.lblSubtotal.text = "$ "+String(x2)
            
            let cv: String = String(format: "%.2f", x2)
            
            cell.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+String(cv)
            
            cell.lblSelectedTipAmount.text = "$"+strTipAmount
            
            self.modifiedTotalAmounToPaySendToServer = String(x2)
            
            self.myTipAmount = strTipAmount
            
            // this method calls for transaction fee
             self.lblTransactionFee(strTotalAmount: String(x2))
        }*/
        
        
        
    }
    
    @objc func doneOther2() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPTipForDeliveryGuyTwoTableCell
        
        
        let tipAmountSelected = Double(cell.txtother.text!)!
        print(tipAmountSelected as Any)
        
        // cell.txtother.text = ""
        self.myTipAmount = "\(tipAmountSelected)"
        
        let addTipAmountWithTotalAmountAfterEverything = Double(cell.txtother.text!)!+Double(self.finalTotalPriceFetch)!
        print(addTipAmountWithTotalAmountAfterEverything as Any)
        
        let cv: String = String(format: "%.2f", addTipAmountWithTotalAmountAfterEverything)
        print(cv as Any)
        
        self.strTotalPrice = String(cv)
        print(self.strTotalPrice as Any)
        
        self.modifiedTotalAmounToPaySendToServer = String(self.strTotalPrice)
        
        cell.lblSelectedTipAmount.text = "$ "+String(cell.txtother.text!)
        
        self.lblTransactionFee(strTotalAmount: String(self.strTotalPrice))
        
        
        
        
        
        // self.txtother.text = ""
        
        /*let editedText = self.strTotalPrice.replacingOccurrences(of: "$ ", with: "")
        
        let valueone = cell.txtother.text!
        let myInt1 = Int(valueone)
        
        let valueTwo = editedText
        
        print(valueTwo as Any)
        let string = valueTwo
        if string.contains(".") {
            
            let myString = string
            let myFloat = (myString as NSString).doubleValue
            
            let a = Double(cell.txtother.text!)
            let b = myFloat
            let c = a! + b
            
            print(c as Any)
            
            // let doubleStr = Double(String(format: "%.2f", c))
             // print(doubleStr as Any)
            // print(doubleStr)
            // print(type(of: doubleStr))
            // let b: String = String(format: "%f", doubleStr as! CVarArg)
            
            // print("/////////////////////")
            
            let cv: String = String(format: "%.2f", c)
            // print(cv as Any)
            // print(type(of: cv))
            
            cell.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+String(cv)
            
            cell.lblSelectedTipAmount.text = "$"+cell.txtother.text!
            
            self.modifiedTotalAmounToPaySendToServer = String(c)
            
            self.myTipAmount = cell.txtother.text!
            
            
            // this method calls for transaction fee
             self.lblTransactionFee(strTotalAmount: String(cv))
            
        } else {
            
            // price is INT and tip is DECIMAL
            let string = cell.txtother.text!
            if string.contains(".") {
                
                print("decimal tip and price int")
                
                print(strTotalPrice as Any)
                
                let editedText = self.strTotalPrice.replacingOccurrences(of: "$ ", with: "")
                
                // add subtotal with tip
                let myString = editedText
                let myFloat = (myString as NSString).doubleValue
                print(cell.lblSubTotal.text as Any)
                print(editedText as Any)
                print(myFloat as Any)
                
                let a = Double(cell.txtother.text!)
                let b = myFloat
                let c = a! + b
                
                print(c as Any)
                
                // let doubleStr = Double(String(format: "%.2f", c))
                 // print(doubleStr as Any)
                // print(doubleStr)
                // print(type(of: doubleStr))
                // let b: String = String(format: "%f", doubleStr as! CVarArg)
                
                // print("/////////////////////")
                
                let cv: String = String(format: "%.2f", c)
                print(cv as Any)
                
                // print(type(of: cv))
                
                cell.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+String(cv)
                
                cell.lblSelectedTipAmount.text = "$"+cell.txtother.text!
                
                self.modifiedTotalAmounToPaySendToServer = String(c)
                
                self.myTipAmount = cell.txtother.text!
                
                // this method calls for transaction fee
                 self.lblTransactionFee(strTotalAmount: String(cv))
                
            } else {
                
                print(strTotalPrice as Any)
                // price is INT and tip is INT
                let myInt2 = Int(valueTwo)
                let x2 : Int = Int(myInt1!+myInt2!)
                // self.lblSubtotal.text = "$ "+String(x2)
                
                let cv: String = String(format: "%.2f", x2)
                
                cell.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+String(cv)
                
                cell.lblSelectedTipAmount.text = "$"+cell.txtother.text!
                
                self.modifiedTotalAmounToPaySendToServer = String(x2)
                
                self.myTipAmount = cell.txtother.text!
                
                // this method calls for transaction fee
                 self.lblTransactionFee(strTotalAmount: String(x2))
            }
            
        }*/
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @objc func placeOrder() {
         // print(mArrSaveMutable as Any)
        print(modifiedTotalAmounToPaySendToServer as Any)
        print(self.myTipAmount as Any)
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPAddressId") as? UPAddress
        push!.strSaveContactOnDelivery2     = strSaveContactOnDelivery
        push!.strSaveSpecialNotes2          = strSaveSpecialNotes
        push!.mArrSaveMutable2              = mArrSaveMutable
        push!.getTotalAmountOfFood          = modifiedTotalAmounToPaySendToServer // strTotalPrice
        push!.tipAmountIs                   = self.myTipAmount
        push!.transactionFeeIs = self.saveTransactionFee
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
}

//MARK:- TABLE VIEW -
extension UPTipForDeliveryGuyTwo: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 // mArrSaveMutable.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if let cell = cell as? UPTipForDeliveryGuyTwoTableCell {

            cell.clView.dataSource = self
            cell.clView.delegate = self
            // cell.clView.tag = indexPath.section
            cell.clView.reloadData()

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UPTipForDeliveryGuyTwoTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UPTipForDeliveryGuyTwoTableCell
        
        cell.backgroundColor = .clear
        
       /*
         description = solid;
         foodId = 48;
         foodImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/foods/1588662121IMG20191105144302.jpg";
         foodName = dosa;
         foodTag = "";
         price = "70.00";
         quantity = 2;
         resturentId = 104;
         specialPrice = "65.00";
         userId = 80;
         */
        
        // cell.lblTitle.text = titleArray[indexPath.row]
        
        /*let item = mArrSaveMutable[indexPath.row] as? [String:Any]
        cell.lblFoodTitle.text = (item!["name"] as! String)
        cell.lblQuantity.text = (item!["quantity"] as! String)
        
        // let cv: String = String(format: "%.2f", (item!["price"] as! String))
        // print(cv as Any)
        
        cell.lblPrice.text = "$ "+(item!["price"] as! String)
        */
        
        return cell
    }
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1200
    }
    
}

extension UPTipForDeliveryGuyTwo: UITableViewDelegate {
    
}

//MARK:- COLLECTION VIEW -
extension UPTipForDeliveryGuyTwo: UICollectionViewDelegate {
    //Write Delegate Code Here
    
}

extension UPTipForDeliveryGuyTwo: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mArrSaveMutableForFoodImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "uPTipForDeliveryGuyCollectionCell", for: indexPath as IndexPath) as! UPTipForDeliveryGuyCollectionCell
        
        /*
           cell.backgroundColor = UIColor.white
           cell.layer.borderWidth = 0.5
           cell.layer.borderColor = UIColor.black.cgColor
           cell.layer.borderWidth = 0.70
           
           cell.contentView.layer.cornerRadius = 10
           cell.contentView.layer.borderWidth = 1.0
           cell.contentView.layer.borderColor = UIColor.clear.cgColor
           cell.contentView.layer.masksToBounds = true

           cell.layer.shadowColor = UIColor.darkGray.cgColor
           cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
           cell.layer.shadowRadius = 2.0
           cell.layer.shadowOpacity = 0.5
           cell.layer.masksToBounds = false
           cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
           */
        
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        
        /*
         "description": , "resturentId": 437, "quantity": 2, "foodName": Food , "foodImage": https://www.expressplusnow.com/appbackend/img/uploads/foods/1606834448_premiumforrestaurants_0.jpg, "foodTag": Veg, "specialPrice": 10, "userId": 127, "foodId": 115, "price": 20
         */
        
        let item = mArrSaveMutableForFoodImage[indexPath.row] as? [String:Any]
        // print(item as Any)
        cell.imgProfile.sd_setImage(with: URL(string: (item!["foodImage"] as! String)), placeholderImage: UIImage(named: "food1"))
        
        
        
        
        
        
        
        if item!["specialPrice"] is String {
                            
            print("Yes, it's a String")
            cell.lblPrice.text = "Price : $"+(item!["specialPrice"] as! String)
            // cell.lblPrice.text = "Price : $"+(item!["specialPrice"] as! String)
            
            
            
        } else if item!["specialPrice"] is Int {
            
            print("It is Integer")
            let x2 : Int = (item!["specialPrice"] as! Int)
            let myString2 = String(x2)
            // cell.lblPrice.text = "Price : $"+String(myString2)
            // cell.lblPrice.text = "Price : $"+String(format: "%.2f", myString2)

            let doubleess = Double(myString2)
            // print(String(format: "%.2f", doubleess!))
            
            cell.lblPrice.text = "Price : $"+String(format: "%.2f", doubleess!)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = item!["specialPrice"] as! NSNumber
            let tempString = temp.stringValue
            // cell.lblPrice.text = "Price : $"+String(tempString)
            // cell.lblPrice.text = "Price : $"+String(format: "%.2f", tempString)
            
            print(tempString as Any)
            
            let doubleess = Double(tempString)
            print(String(format: "%.2f", doubleess!))
            
            cell.lblPrice.text = "Price : $"+String(format: "%.2f", doubleess!)
            
        }
        
        
        
        
        
        
        
        
        
        
        
        // quantity
        if item!["quantity"] is String {
                            
            print("Yes, it's a String")
            cell.lblQuantity.text = "Quantity : "+(item!["quantity"] as! String)
            
        } else if item!["quantity"] is Int {
            
            print("It is Integer")
            let x2 : Int = (item!["quantity"] as! Int)
            let myString2 = String(x2)
            cell.lblQuantity.text = "Quantity : "+String(myString2)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = item!["quantity"] as! NSNumber
            let tempString = temp.stringValue
            cell.lblQuantity.text = "Quantity : "+String(tempString)
            
        }
        // String(format: "%.2f", totalAmount)
        
        return cell
    }
    
    //Write DataSource Code Here
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
  
}


extension UPTipForDeliveryGuyTwo: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        var sizes: CGSize
        
        let result = UIScreen.main.bounds.size
            
//        NSLog("%f",result.height)
     
        if result.height == 480
        {
            //Load 3.5 inch xib
            sizes = CGSize(width: 160, height: 180)
        }
        else if result.height == 568
        {
            //Load 4 inch xib
            sizes = CGSize(width: 160, height: 180)
        }
        else if result.height == 667.000000
        {
            //Load 4.7 inch xib , 8
            sizes = CGSize(width: 160, height: 180) // done
        }
        else if result.height == 736.000000
        {
            // iphone 6s Plus and 7 Plus
            sizes = CGSize(width: 160, height: 180) // done
        }
        else if result.height == 812.000000
        {
            // iphone X , 11 pro
            sizes = CGSize(width: 160, height: 180)
        }
        else if result.height == 896.000000
        {
            // iphone Xr ,11, 11 pro max
            sizes = CGSize(width: 160, height: 1880) // done
        }
        else
        {
            sizes = CGSize(width: 160, height: 180)
        }
     
        return sizes
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
       
        return 10
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
         
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
    
    }
    
}
