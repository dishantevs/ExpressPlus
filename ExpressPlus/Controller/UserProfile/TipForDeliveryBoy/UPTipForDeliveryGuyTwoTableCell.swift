//
//  UPTipForDeliveryGuyTwoTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 06/01/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class UPTipForDeliveryGuyTwoTableCell: UITableViewCell {

    let SELECT_COLOR = UIColor.init(red: 116.0/255.0, green: 233.0/255.0, blue: 139.0/255.0, alpha: 1)
    let UN_SELECT_COLOR = UIColor.init(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1)
    
    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.layer.cornerRadius = 16
            viewBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblSelectedTipAmount:UILabel!
    
    @IBOutlet weak var btnOne:UIButton! {
        didSet {
            btnOne.layer.cornerRadius = 8
            btnOne.clipsToBounds = true
            btnOne.setTitle("  Item Name", for: .normal)
            btnOne.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var btnTwo:UIButton! {
        didSet {
            btnTwo.layer.cornerRadius = 8
            btnTwo.clipsToBounds = true
            btnTwo.setTitle("Quantity", for: .normal)
            btnTwo.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var btnThree:UIButton! {
        didSet {
            btnThree.layer.cornerRadius = 8
            btnThree.clipsToBounds = true
            btnThree.setTitle("Price", for: .normal)
            btnThree.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblTotalItemInCart:UILabel! {
        didSet {
            lblTotalItemInCart.textColor = .systemRed
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "CART"
        }
    }
    
    /*
    @IBOutlet weak var lblSubtotal:UILabel! {
        didSet {
            lblSubtotal.text = "$ 250"
        }
    }
    */
    
    @IBOutlet weak var btnDoneOther:UIButton! {
        didSet {
            btnDoneOther.isHidden = true
            btnDoneOther.layer.cornerRadius = 12
            btnDoneOther.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnApply:UIButton! {
        didSet {
            btnApply.layer.cornerRadius = 20
            btnApply.setTitle("APPLY", for: .normal)
            btnApply.clipsToBounds = true
            // 216 91 49
            btnApply.backgroundColor = UIColor.init(red: 216.0/255.0, green: 91.0/255.0, blue: 49.0/255.0, alpha: 1)
            btnApply.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var btnPlaceOrder:UIButton! {
        didSet {
            btnPlaceOrder.layer.cornerRadius = 20
            btnPlaceOrder.clipsToBounds = true
            btnPlaceOrder.backgroundColor = .systemOrange // NAVIGATION_BACKGROUND_COLOR
            btnPlaceOrder.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnFive:UIButton! {
        didSet {
            btnFive.layer.cornerRadius = 20
            btnFive.clipsToBounds = true
            btnFive.layer.borderColor = UIColor.black.cgColor
            btnFive.layer.borderWidth = 0.8
            btnFive.setTitle("$5", for: .normal)
        }
    }
    @IBOutlet weak var btnTen:UIButton! {
        didSet {
            btnTen.layer.cornerRadius = 20
            btnTen.clipsToBounds = true
            btnTen.layer.borderColor = UIColor.black.cgColor
            btnTen.layer.borderWidth = 0.8
            btnTen.setTitle("$10", for: .normal)
        }
    }
    @IBOutlet weak var btnFifteen:UIButton! {
        didSet {
            btnFifteen.layer.cornerRadius = 20
            btnFifteen.clipsToBounds = true
            btnFifteen.layer.borderColor = UIColor.black.cgColor
            btnFifteen.layer.borderWidth = 0.8
            btnFifteen.setTitle("$15", for: .normal)
        }
    }
    @IBOutlet weak var btnTwenty:UIButton! {
        didSet {
            btnTwenty.layer.cornerRadius = 20
            btnTwenty.clipsToBounds = true
            btnTwenty.layer.borderColor = UIColor.black.cgColor
            btnTwenty.layer.borderWidth = 0.8
            btnTwenty.setTitle("$20", for: .normal)
        }
    }
    @IBOutlet weak var btnOther:UIButton! {
        didSet {
            btnOther.layer.cornerRadius = 20
            btnOther.clipsToBounds = true
            btnOther.layer.borderColor = UIColor.black.cgColor
            btnOther.layer.borderWidth = 0.8
            btnOther.setTitle("Other", for: .normal)
        }
    }
    
    @IBOutlet weak var txtApply:UITextField! {
        didSet {
            txtApply.layer.cornerRadius = 6
            txtApply.clipsToBounds = true
            txtApply.layer.borderColor = UIColor.lightGray.cgColor
            txtApply.layer.borderWidth = 0.8
            txtApply.backgroundColor = UN_SELECT_COLOR
            txtApply.placeholder = "coupon code"
            txtApply.setLeftPaddingPoints(20)
            txtApply.keyboardType = .decimalPad
        }
    }
    
    @IBOutlet weak var txtother:UITextField! {
        didSet {
            txtother.layer.cornerRadius = 6
            txtother.clipsToBounds = true
            txtother.layer.borderColor = UIColor.lightGray.cgColor
            txtother.layer.borderWidth = 0.8
            txtother.backgroundColor = UN_SELECT_COLOR
            txtother.setLeftPaddingPoints(20)
            txtother.placeholder = "Enter Tip Amount"
        }
    }
    
    
    
    @IBOutlet weak var lblTotalAmountToPay:UILabel!
    
    @IBOutlet weak var lblSubTotal:UILabel!
    
    @IBOutlet weak var viewBGClview:UIView! {
        didSet {
            viewBGClview.layer.cornerRadius = 8
            viewBGClview.clipsToBounds = true
            viewBGClview.backgroundColor = NAVIGATION_BACKGROUND_COLOR // UIColor.init(red: 217.0/255.0, green: 217.0/255.0, blue: 217.0/255.0, alpha: 1)
            viewBGClview.layer.borderWidth = 0.8
            viewBGClview.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    @IBOutlet weak var clView:UICollectionView! {
        didSet {
            clView.layer.cornerRadius = 8
            clView.clipsToBounds = true
            clView.backgroundColor = UIColor.init(red: 217.0/255.0, green: 217.0/255.0, blue: 217.0/255.0, alpha: 1)
            clView.layer.borderWidth = 0.8
            clView.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    @IBOutlet weak var viewBGOne:UIView! {
        didSet {
            viewBGOne.layer.cornerRadius = 8
            viewBGOne.clipsToBounds = true
            viewBGOne.backgroundColor = UIColor.init(red: 217.0/255.0, green: 217.0/255.0, blue: 217.0/255.0, alpha: 1)
            viewBGOne.layer.borderWidth = 0.8
            viewBGOne.layer.borderColor = UIColor.black.cgColor
        }
    }
    @IBOutlet weak var viewBGTwo:UIView! {
        didSet {
            viewBGTwo.layer.cornerRadius = 8
            viewBGTwo.clipsToBounds = true
            viewBGTwo.backgroundColor = UIColor.init(red: 217.0/255.0, green: 217.0/255.0, blue: 217.0/255.0, alpha: 1)
            viewBGTwo.layer.borderWidth = 0.8
            viewBGTwo.layer.borderColor = UIColor.black.cgColor
        }
    }
    @IBOutlet weak var viewBGThree:UIView! {
        didSet {
            viewBGThree.layer.cornerRadius = 8
            viewBGThree.clipsToBounds = true
            viewBGThree.backgroundColor = UIColor.init(red: 217.0/255.0, green: 217.0/255.0, blue: 217.0/255.0, alpha: 1)
            viewBGThree.layer.borderWidth = 0.8
            viewBGThree.layer.borderColor = UIColor.black.cgColor
        }
    }
    @IBOutlet weak var viewBGFour:UIView! {
        didSet {
            viewBGFour.layer.cornerRadius = 8
            viewBGFour.clipsToBounds = true
            viewBGFour.backgroundColor = UIColor.init(red: 217.0/255.0, green: 217.0/255.0, blue: 217.0/255.0, alpha: 1)
            viewBGFour.layer.borderWidth = 0.8
            viewBGFour.layer.borderColor = UIColor.black.cgColor
        }
    }
    @IBOutlet weak var viewBGFive:UIView! {
        didSet {
            viewBGFive.layer.cornerRadius = 8
            viewBGFive.clipsToBounds = true
            viewBGFive.backgroundColor = .white // NAVIGATION_BACKGROUND_COLOR
            viewBGFive.layer.borderWidth = 0.8
            viewBGFive.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    @IBOutlet weak var viewBgPayment:UIView! {
        didSet {
            viewBgPayment.backgroundColor = NAVIGATION_BACKGROUND_COLOR // UIColor.init(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1)
            viewBgPayment.layer.cornerRadius = 8
            viewBgPayment.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblTransactionFee:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
