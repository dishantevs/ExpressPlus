//
//  UPTipForDeliveryGuyCollectionCell.swift
//  ExpressPlus
//
//  Created by Apple on 06/01/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class UPTipForDeliveryGuyCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProfile:UIImageView!
    
    @IBOutlet weak var viewBg:UIView! {
        didSet {
            viewBg.layer.cornerRadius = 4
            viewBg.clipsToBounds = true
            viewBg.backgroundColor = UIColor.init(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var txtCoupon:UITextField! {
        didSet {
            txtCoupon.layer.cornerRadius = 6
            txtCoupon.clipsToBounds = true
            txtCoupon.layer.borderColor = UIColor.lightGray.cgColor
            txtCoupon.layer.borderWidth = 0.8
        }
    }
    
    @IBOutlet weak var lblFoodTitle:UILabel!
    @IBOutlet weak var lblQuantity:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    
}
