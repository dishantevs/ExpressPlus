//
//  UPTipForDeliveryGuy.swift
//  ExpressPlus
//
//  Created by Apple on 20/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UPTipForDeliveryGuy: UIViewController, UITextFieldDelegate {
// 116 233 139
    let SELECT_COLOR = UIColor.init(red: 116.0/255.0, green: 233.0/255.0, blue: 139.0/255.0, alpha: 1)
    let UN_SELECT_COLOR = UIColor.init(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1)
    
    // MARK:- GET ALL VALUE FROM CART -
    var strSaveContactOnDelivery:String!
    var strSaveSpecialNotes:String!
    var mArrSaveMutable:NSMutableArray = []
    
    var strTotalPrice:String!
    
    var staticSubTotalPrice:String!
    var myTipAmount:String!
    
    var strSpecialPrice2:String!
    
    var arrGetCarditems:NSMutableArray = []
    
    // send total amount fetch and send to the server
    var modifiedTotalAmounToPaySendToServer:String!
    
    let cellReuseIdentifier = "uPTipForDeliveryGuyTableCell"
    
    
    
    
    
    
    
    
    @IBOutlet weak var lblSelectedTipAmount:UILabel!
    
    @IBOutlet weak var btnOne:UIButton! {
        didSet {
            btnOne.layer.cornerRadius = 8
            btnOne.clipsToBounds = true
            btnOne.setTitle("  Item Name", for: .normal)
            btnOne.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var btnTwo:UIButton! {
        didSet {
            btnTwo.layer.cornerRadius = 8
            btnTwo.clipsToBounds = true
            btnTwo.setTitle("Quantity", for: .normal)
            btnTwo.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var btnThree:UIButton! {
        didSet {
            btnThree.layer.cornerRadius = 8
            btnThree.clipsToBounds = true
            btnThree.setTitle("Price", for: .normal)
            btnThree.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblTotalItemInCart:UILabel! {
        didSet {
            lblTotalItemInCart.textColor = .systemRed
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "CART"
        }
    }
    
    /*
    @IBOutlet weak var lblSubtotal:UILabel! {
        didSet {
            lblSubtotal.text = "$ 250"
        }
    }
    */
    
    @IBOutlet weak var btnDoneOther:UIButton! {
        didSet {
            btnDoneOther.isHidden = true
            btnDoneOther.layer.cornerRadius = 12
            btnDoneOther.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnApply:UIButton! {
        didSet {
            btnApply.layer.cornerRadius = 20
            btnApply.setTitle("APPLY", for: .normal)
            btnApply.clipsToBounds = true
            // 216 91 49
            btnApply.backgroundColor = UIColor.init(red: 216.0/255.0, green: 91.0/255.0, blue: 49.0/255.0, alpha: 1)
            btnApply.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var btnPlaceOrder:UIButton! {
        didSet {
            btnPlaceOrder.layer.cornerRadius = 20
            btnPlaceOrder.clipsToBounds = true
            btnPlaceOrder.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnPlaceOrder.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnFive:UIButton! {
        didSet {
            btnFive.layer.cornerRadius = 20
            btnFive.clipsToBounds = true
            btnFive.layer.borderColor = UIColor.black.cgColor
            btnFive.layer.borderWidth = 0.8
            btnFive.setTitle("$5", for: .normal)
        }
    }
    @IBOutlet weak var btnTen:UIButton! {
        didSet {
            btnTen.layer.cornerRadius = 20
            btnTen.clipsToBounds = true
            btnTen.layer.borderColor = UIColor.black.cgColor
            btnTen.layer.borderWidth = 0.8
            btnTen.setTitle("$10", for: .normal)
        }
    }
    @IBOutlet weak var btnFifteen:UIButton! {
        didSet {
            btnFifteen.layer.cornerRadius = 20
            btnFifteen.clipsToBounds = true
            btnFifteen.layer.borderColor = UIColor.black.cgColor
            btnFifteen.layer.borderWidth = 0.8
            btnFifteen.setTitle("$15", for: .normal)
        }
    }
    @IBOutlet weak var btnTwenty:UIButton! {
        didSet {
            btnTwenty.layer.cornerRadius = 20
            btnTwenty.clipsToBounds = true
            btnTwenty.layer.borderColor = UIColor.black.cgColor
            btnTwenty.layer.borderWidth = 0.8
            btnTwenty.setTitle("$20", for: .normal)
        }
    }
    @IBOutlet weak var btnOther:UIButton! {
        didSet {
            btnOther.layer.cornerRadius = 20
            btnOther.clipsToBounds = true
            btnOther.layer.borderColor = UIColor.black.cgColor
            btnOther.layer.borderWidth = 0.8
            btnOther.setTitle("Other", for: .normal)
        }
    }
    
    @IBOutlet weak var txtApply:UITextField! {
        didSet {
            txtApply.layer.cornerRadius = 6
            txtApply.clipsToBounds = true
            txtApply.layer.borderColor = UIColor.lightGray.cgColor
            txtApply.layer.borderWidth = 0.8
            txtApply.backgroundColor = UN_SELECT_COLOR
            txtApply.placeholder = "coupon code"
            txtApply.setLeftPaddingPoints(20)
            txtApply.keyboardType = .decimalPad
        }
    }
    
    @IBOutlet weak var txtother:UITextField! {
        didSet {
            txtother.layer.cornerRadius = 6
            txtother.clipsToBounds = true
            txtother.layer.borderColor = UIColor.lightGray.cgColor
            txtother.layer.borderWidth = 0.8
            txtother.backgroundColor = UN_SELECT_COLOR
            txtother.setLeftPaddingPoints(20)
            txtother.placeholder = "Enter Tip Amount"
        }
    }
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
            tbleView.reloadData()
        }
    }
    
    @IBOutlet weak var lblTotalAmountToPay:UILabel!
    
    @IBOutlet weak var lblSubTotal:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // print(staticSubTotalPrice as Any)
        // print(strTotalPrice as Any)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        self.btnPlaceOrder.addTarget(self, action: #selector(placeOrder), for: .touchUpInside)
        
        self.tbleView.separatorColor = .clear
        
        self.myTipAmount = "0"
        self.lblSelectedTipAmount.text = "$0"
        
        self.txtApply.delegate = self
        self.txtother.delegate = self
        
        // print(mArrSaveMutable as Any)
        
        if String(mArrSaveMutable.count) == "1" {
            self.lblTotalItemInCart.text = " "+String(mArrSaveMutable.count)+" item"
        } else {
            self.lblTotalItemInCart.text = " "+String(mArrSaveMutable.count)+" items"
        }
        
        self.txtother.isUserInteractionEnabled = false
        self.btnFive.backgroundColor    = UN_SELECT_COLOR
        self.btnTen.backgroundColor     = UN_SELECT_COLOR
        self.btnFifteen.backgroundColor = UN_SELECT_COLOR
        self.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        self.btnOther.backgroundColor   = UN_SELECT_COLOR
        
        self.btnFive.addTarget(self, action: #selector(addFiveClick), for: .touchUpInside)
        self.btnTen.addTarget(self, action: #selector(addTenClick), for: .touchUpInside)
        self.btnFifteen.addTarget(self, action: #selector(addFifteenClick), for: .touchUpInside)
        self.btnTwenty.addTarget(self, action: #selector(addTwentyClick), for: .touchUpInside)
        self.btnOther.addTarget(self, action: #selector(addOtherClick), for: .touchUpInside)
        
        self.btnDoneOther.isHidden = true
        
         var strSpecialPrice:String!
        
         var strQuantity:String!
         // print(mArrSaveMutable as Any)
        
        
        var sum2 = 0
        print(mArrSaveMutable as Any)
        
        for n in 0...mArrSaveMutable.count-1 {
            let item = mArrSaveMutable[n] as? [String:Any]
            
            strSpecialPrice = (item!["price"] as! String)
            let editedText = strSpecialPrice.replacingOccurrences(of: ",", with: "")
            strSpecialPrice2 = String(editedText)
            
            strQuantity = (item!["quantity"] as! String)
           
            let a = (strSpecialPrice2 as NSString).floatValue
            let b = (strQuantity as NSString).floatValue
            let sum = a * b
            
            sum2 += Int(sum)
        }
        
        // add 6 for delivery charge
        let x : Int = sum2+6
        strTotalPrice = String(x)
        print(strTotalPrice as Any)
        
        // convert string into int ( add 6 on total price )
        let editedText2 = staticSubTotalPrice.replacingOccurrences(of: ",", with: "")
        print(editedText2 as Any)
        
        let string = editedText2
        if string.contains(".") {
            print("exists")
            
            // let num1 = item!["specialPrice"] as! Double
             // let num2 = item!["quantity"] as! Int
            let myString = editedText2
            let myFloat = (myString as NSString).doubleValue
            
            
            let a = Double(6)
            let b = myFloat
            let c = a + b
            
            let d = String(format: "%.2f", c)
            // print(d)
            // total amount show when user didn't select tip amount
            self.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+d
            
            self.modifiedTotalAmounToPaySendToServer = d
            
            self.strTotalPrice = d
            self.lblSubTotal.text = "$ "+editedText2
            // self.lblSubTotal.text = "$ "+d
            
        } else {
            print("no")
            
            let myString1 = editedText2
            let myInt1 = Int(myString1)
            print(myInt1 as Any)
            
            // MARK:- FOOD PRICE WIHOUT TIP -
            // food price without tip is 'myInt1'
            let foodPriceWithoutTipInInt : Int = myInt1 ?? 00
            let strPriceWihoutTip = String(foodPriceWithoutTipInInt)
            self.lblSubTotal.text = "$ "+strPriceWihoutTip
           
            
            let addDeliveryToTotalAmount = myInt1!+6
            
            let convertToalAmountWithDeliveryChargeToString : Int = addDeliveryToTotalAmount
            let resultOf = String(convertToalAmountWithDeliveryChargeToString)
            
            // total amount show when user didn't select tip amount
            self.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+resultOf
            
            self.modifiedTotalAmounToPaySendToServer = resultOf
            
            self.strTotalPrice = resultOf
        }
        
        
        // self.btnApply.addTarget(self, action: #selector(applyClickMethod), for: .touchUpInside)
        
    }
    
    @objc func applyClickMethod() {
        let salexTax = self.txtApply.text!
        
        let totalAmountIs = "46"
        
        print(totalAmountIs as Any)
        
        if salexTax.contains(".") {
            print("decimal")
            
            let myString = salexTax
            let myFloat = (myString as NSString).doubleValue
            print(myFloat as Any)
            
            print((myFloat/100))
            
            // print()
            
            // total amount
            let myString2 = totalAmountIs
            let myFloat2 = (myString2 as NSString).doubleValue
            print(myFloat2 as Any)
            
            print((myFloat*myFloat2)/100)
            
            
            let a = (myFloat*myFloat2)/100
            let b = Double(totalAmountIs)
            let c = a + b!
            
            print(c as Any)
            
            
            let alert = UIAlertController(title: String("result"), message: String(c), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                        
                        
            }))
            self.present(alert, animated: true, completion: nil)
            
            
        } else {
            print("non decimal")
            
            let salexTax = self.txtApply.text!
            
            let totalAmountIs = "46"
            
            let myString = salexTax
            let myFloat = (myString as NSString).doubleValue
            print(myFloat as Any)
            
            print((myFloat/100))
            
            // print()
            
            // total amount
            let myString2 = totalAmountIs
            let myFloat2 = (myString2 as NSString).doubleValue
            print(myFloat2 as Any)
            
            print((myFloat*myFloat2)/100)
            
            
            let a = (myFloat*myFloat2)/100
            let b = Double(totalAmountIs)
            let c = a + b!
            
            print(c as Any)
            
            
            let alert = UIAlertController(title: String("result"), message: String(c), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                        
                        
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func addFiveClick() {
        self.txtother.isUserInteractionEnabled = false
        self.btnFive.backgroundColor    = SELECT_COLOR
        self.btnTen.backgroundColor     = UN_SELECT_COLOR
        self.btnFifteen.backgroundColor = UN_SELECT_COLOR
        self.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        self.btnOther.backgroundColor   = UN_SELECT_COLOR
        
        self.btnDoneOther.isHidden = true
        
        self.calculateFinalTotalAfterTip(strTipAmount: "5")
    }
    
    @objc func addTenClick() {
        self.txtother.isUserInteractionEnabled  = false
        self.btnFive.backgroundColor    = UN_SELECT_COLOR
        self.btnTen.backgroundColor     = SELECT_COLOR
        self.btnFifteen.backgroundColor = UN_SELECT_COLOR
        self.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        self.btnOther.backgroundColor   = UN_SELECT_COLOR
        
        self.btnDoneOther.isHidden = true
        
        self.calculateFinalTotalAfterTip(strTipAmount: "10")
    }
    
    @objc func addFifteenClick() {
        self.txtother.isUserInteractionEnabled = false
        self.btnFive.backgroundColor    = UN_SELECT_COLOR
        self.btnTen.backgroundColor     = UN_SELECT_COLOR
        self.btnFifteen.backgroundColor = SELECT_COLOR
        self.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        self.btnOther.backgroundColor   = UN_SELECT_COLOR
        
        self.btnDoneOther.isHidden = true
        
        self.calculateFinalTotalAfterTip(strTipAmount: "15")
    }
    
    @objc func addTwentyClick() {
        self.txtother.isUserInteractionEnabled = false
        self.btnFive.backgroundColor    = UN_SELECT_COLOR
        self.btnTen.backgroundColor     = UN_SELECT_COLOR
        self.btnFifteen.backgroundColor = UN_SELECT_COLOR
        self.btnTwenty.backgroundColor  = SELECT_COLOR
        self.btnOther.backgroundColor   = UN_SELECT_COLOR
        
        self.btnDoneOther.isHidden = true
        
        self.calculateFinalTotalAfterTip(strTipAmount: "20")
    }
    
    @objc func addOtherClick() {
        self.txtother.isUserInteractionEnabled = true
        self.btnFive.backgroundColor    = UN_SELECT_COLOR
        self.btnTen.backgroundColor     = UN_SELECT_COLOR
        self.btnFifteen.backgroundColor = UN_SELECT_COLOR
        self.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        self.btnOther.backgroundColor   = SELECT_COLOR
        
        self.btnDoneOther.isHidden = false
        self.btnDoneOther.addTarget(self, action: #selector(doneOther2), for: .touchUpInside)
        
        self.lblSelectedTipAmount.text = "$ 0"
        /*
        var strSpecialPrice:String!
        var strQuantity:String!
        
        var sum2 = 0
        
        for n in 0...mArrSaveMutable.count-1 {
            let item = mArrSaveMutable[n] as? [String:Any]
            
            strSpecialPrice = (item!["price"] as! String)
            strQuantity = (item!["quantity"] as! String)
           
            let a = (strSpecialPrice as NSString).floatValue
            let b = (strQuantity as NSString).floatValue
            let sum = a * b
            
            sum2 += Int(sum)
        }
        let x : Int = sum2
        strTotalPrice = String(x)
        self.lblSubtotal.text = "$ "+String(strTotalPrice)
        
        */
    }
    
    
    
    @objc func calculateFinalTotalAfterTip(strTipAmount:String) {
        // print(strTipAmount as Any)
        
        self.txtother.text = ""
        
        let editedText = self.strTotalPrice.replacingOccurrences(of: "$ ", with: "")
        
        let valueone = strTipAmount
        let myInt1 = Int(valueone)
        
        let valueTwo = editedText
        
        print(valueTwo as Any)
        let string = valueTwo
        if string.contains(".") {
            
            let myString = string
            let myFloat = (myString as NSString).doubleValue
            
            let a = Double(strTipAmount)
            let b = myFloat
            let c = a! + b
            
            self.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+String(c)
            
            self.lblSelectedTipAmount.text = "$"+strTipAmount
            
            self.modifiedTotalAmounToPaySendToServer = String(c)
            
            self.myTipAmount = strTipAmount
            
        } else {
            
            let myInt2 = Int(valueTwo)
            let x2 : Int = Int(myInt1!+myInt2!)
            // self.lblSubtotal.text = "$ "+String(x2)
            self.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+String(x2)
            
            self.lblSelectedTipAmount.text = "$"+strTipAmount
            
            self.modifiedTotalAmounToPaySendToServer = String(x2)
            
            self.myTipAmount = strTipAmount
            
        }
        
        
        
    }
    
    @objc func doneOther2() {
        
        /*
        let editedText = self.strTotalPrice.replacingOccurrences(of: "$ ", with: "")
        
        let valueone = self.txtother.text!
        let myInt1 = Int(valueone)
        
        let valueTwo = editedText
        let myInt2 = Int(valueTwo)
        
        let x2 : Int = Int(myInt1!+myInt2!)
        // self.lblSubtotal.text = "$ "+String(x2)
        self.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+String(x2)
        
        self.lblSelectedTipAmount.text = "$ "+self.txtother.text!
        
        self.modifiedTotalAmounToPaySendToServer = String(x2)
        
        self.myTipAmount = self.txtother.text!
        */
        
        
        // self.txtother.text = ""
        
        let editedText = self.strTotalPrice.replacingOccurrences(of: "$ ", with: "")
        
        let valueone = self.txtother.text!
        let myInt1 = Int(valueone)
        
        let valueTwo = editedText
        
        print(valueTwo as Any)
        let string = valueTwo
        if string.contains(".") {
            
            let myString = string
            let myFloat = (myString as NSString).doubleValue
            
            let a = Double(self.txtother.text!)
            let b = myFloat
            let c = a! + b
            
            print(c as Any)
            
            // let doubleStr = Double(String(format: "%.2f", c))
             // print(doubleStr as Any)
            // print(doubleStr)
            // print(type(of: doubleStr))
            // let b: String = String(format: "%f", doubleStr as! CVarArg)
            
            // print("/////////////////////")
            
            let cv: String = String(format: "%.2f", c)
            // print(cv as Any)
            // print(type(of: cv))
            
            self.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+String(cv)
            
            self.lblSelectedTipAmount.text = "$"+self.txtother.text!
            
            self.modifiedTotalAmounToPaySendToServer = String(c)
            
            self.myTipAmount = self.txtother.text!
            
        } else {
            
            // price is INT and tip is DECIMAL
            let string = self.txtother.text!
            if string.contains(".") {
                
                print("decimal tip and price int")
                
                print(strTotalPrice as Any)
                
                let editedText = self.strTotalPrice.replacingOccurrences(of: "$ ", with: "")
                
                // add subtotal with tip
                let myString = editedText
                let myFloat = (myString as NSString).doubleValue
                print(self.lblSubTotal.text as Any)
                print(editedText as Any)
                print(myFloat as Any)
                
                let a = Double(self.txtother.text!)
                let b = myFloat
                let c = a! + b
                
                print(c as Any)
                
                // let doubleStr = Double(String(format: "%.2f", c))
                 // print(doubleStr as Any)
                // print(doubleStr)
                // print(type(of: doubleStr))
                // let b: String = String(format: "%f", doubleStr as! CVarArg)
                
                // print("/////////////////////")
                
                let cv: String = String(format: "%.2f", c)
                print(cv as Any)
                
                // print(type(of: cv))
                
                self.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+String(cv)
                
                self.lblSelectedTipAmount.text = "$"+self.txtother.text!
                
                self.modifiedTotalAmounToPaySendToServer = String(c)
                
                self.myTipAmount = self.txtother.text!
                
                
            } else {
                
                print(strTotalPrice as Any)
                // price is INT and tip is INT
                let myInt2 = Int(valueTwo)
                let x2 : Int = Int(myInt1!+myInt2!)
                // self.lblSubtotal.text = "$ "+String(x2)
                self.lblTotalAmountToPay.text = "Total Amount to Pay : $ "+String(x2)
                
                self.lblSelectedTipAmount.text = "$"+self.txtother.text!
                
                self.modifiedTotalAmounToPaySendToServer = String(x2)
                
                self.myTipAmount = self.txtother.text!
                
            }
            
            
            
            
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @objc func placeOrder() {
         // print(mArrSaveMutable as Any)
        print(modifiedTotalAmounToPaySendToServer as Any)
        print(self.myTipAmount as Any)
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPAddressId") as? UPAddress
        push!.strSaveContactOnDelivery2     = strSaveContactOnDelivery
        push!.strSaveSpecialNotes2          = strSaveSpecialNotes
        push!.mArrSaveMutable2              = mArrSaveMutable
        push!.getTotalAmountOfFood          = modifiedTotalAmounToPaySendToServer // strTotalPrice
        push!.tipAmountIs                   = self.myTipAmount
        self.navigationController?.pushViewController(push!, animated: true)
    }
}


//MARK:- TABLE VIEW -
extension UPTipForDeliveryGuy: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mArrSaveMutable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UPTipForDeliveryGuyTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UPTipForDeliveryGuyTableCell
        
        cell.backgroundColor = .white
        
       /*
         description = solid;
         foodId = 48;
         foodImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/foods/1588662121IMG20191105144302.jpg";
         foodName = dosa;
         foodTag = "";
         price = "70.00";
         quantity = 2;
         resturentId = 104;
         specialPrice = "65.00";
         userId = 80;
         */
        
        // cell.lblTitle.text = titleArray[indexPath.row]
        
        let item = mArrSaveMutable[indexPath.row] as? [String:Any]
        cell.lblFoodTitle.text = (item!["name"] as! String)
        cell.lblQuantity.text = (item!["quantity"] as! String)
        
        // let cv: String = String(format: "%.2f", (item!["price"] as! String))
        // print(cv as Any)
        
        cell.lblPrice.text = "$ "+(item!["price"] as! String)
        
        return cell
    }
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}

extension UPTipForDeliveryGuy: UITableViewDelegate {
    
}

