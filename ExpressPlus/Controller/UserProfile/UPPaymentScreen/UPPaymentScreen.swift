//
//  UPPaymentScreen.swift
//  ExpressPlus
//
//  Created by Apple on 04/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit


import Alamofire

import Stripe

class UPPaymentScreen: UIViewController, UITextFieldDelegate {

    let cellReuseIdentifier = "uPPaymentScreenTableCell"
    
    var getTextOneFromTaxScreen2:String!
    var getTexttWOFromTaxScreen2:String!
    var getestimatePrice2:String!
    var getTip2:String!
    var getDeliveryFee2:String!
    var getNotes2:String!
    var getContactOnDelivery2:String!
    var getTotalPayment2:String!
    
    var latitude2:String!
    var longitude2:String!
    var address2:String!
    var state2:String!
    var city2:String!
    var zipcode2:String!
    var landmark2:String!
    var workplace2:String!
    var stateTax2:String!
    
    var namee2:String!
    var phonee2:String!
    
    // month and year
    var expMonthIs:String!
    var expYearIs:String!
    
    // string
    var strWhatDoYouWant2:String!
    var strTotalPrice1:String!
    var strFromWhere:String!
    
    // this strings is for service section customer
    /*
     [TIP] => 10.0
     [groupId] => 61
     [serviceAmount] => 500
     [serviceType] => 8
     [totalAmount] => 510.0
     */
    var seviceTipAmount:String!
    var seviceGroupId:String!
    var serviceAmount:String!
    var seviceType:String!
    var serviceTotalAmount:String!
 
    
    var specialOfferScreenImplemented:String!
    
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "PAYMENT"
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
        }
    }
    @IBOutlet weak var lblCardNumberHeading:UILabel!
    @IBOutlet weak var lblEXPDate:UILabel!
    
    @IBOutlet weak var lblPayableAmount:UILabel!
    
    @IBOutlet weak var btnmakePayment:UIButton! {
        didSet {
            btnmakePayment.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnmakePayment.setTitle("MAKE PAYMENT", for: .normal)
            btnmakePayment.setTitleColor(.white, for: .normal)
        }
    }
    @IBOutlet weak var viewCard:UIView! {
        didSet {
            viewCard.backgroundColor = UIColor.init(red: 34.0/255.0, green: 72.0/255.0, blue: 104.0/255.0, alpha: 1)
            viewCard.layer.cornerRadius = 6
            viewCard.clipsToBounds = true
        }
    }
    // 34 72 104
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false

        view.addGestureRecognizer(tap)
        
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        btnmakePayment.addTarget(self, action: #selector(firstCheckValidation), for: .touchUpInside)
        
        /*
         var getTextOneFromTaxScreen2:String!
         var getTexttWOFromTaxScreen2:String!
         var getestimatePrice2:String!
         var getTip2:String!
         var getDeliveryFee2:String!
         var getNotes2:String!
         var getContactOnDelivery2:String!
         var getTotalPayment2:String!
         
         var latitude2:String!
         var longitude2:String!
         var address2:String!
         var state2:String!
         var city2:String!
         var zipcode2:String!
         var landmark2:String!
         var workplace2:String!
         var stateTax2:String!
         */
        
        /*
        print(getTextOneFromTaxScreen2 as Any)
        print(getestimatePrice2 as Any)
        print(getTip2 as Any)
        print(getDeliveryFee2 as Any)
        print(getTexttWOFromTaxScreen2 as Any)
        print(getNotes2 as Any)
        print(getContactOnDelivery2 as Any)
        print(getTotalPayment2 as Any)
        print(latitude2 as Any)
        print(longitude2 as Any)
        print(address2 as Any)
        print(state2 as Any)
        print(city2 as Any)
        print(zipcode2 as Any)
        print(landmark2 as Any)
        print(workplace2 as Any)
        print(stateTax2 as Any)
 */
        
        /*
         [action] => addfooorder
         [deliveryLat] => 28.5871275
         [deliveryLong] => 77.0605771
         [userId] => 138
         [address] => Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India
         [state] => Delhi
         [city] => Delhi
         [zipcode] => 110075
         [name] => hehd
         [phone] => 9565884859
         [landmark] => vdvs
         [workPlace] => Home
         [whatYouWant] => white sauce pasta
         [amount] => 61.0
         [TIP] => 5.0
         [deliveryFee] => 6
         [totalAmount] => 72.0
         [cardNo] => 12/25
         [transactionId] => tok_1GriptBnk7ygV50q72lKlXKM
         [salesTax] => 0
         */
        
        
        self.lblPayableAmount.text = "PAYABLE AMOUNT : $ "+String(getTotalPayment2)
        
        
        
        
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func firstCheckValidation() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPPaymentScreenTableCell
        
        if cell.txtCardNumber.text == "" {
            let alert = UIAlertController(title: "Card Number", message: "Card number should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtExpDate.text == "" {
            let alert = UIAlertController(title: "Exp Month", message: "Expiry Month should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtCVV.text == "" {
            let alert = UIAlertController(title: "Security Code", message: "Security Code should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            self.fetchStripeToken()
        }
    }
    @objc func fetchStripeToken() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPPaymentScreenTableCell
        
        let fullNameArr = cell.txtExpDate.text!.components(separatedBy: "/")

        // print(fullNameArr as Any)
        let name    = fullNameArr[0]
        let surname = fullNameArr[1]
        
        // print(name as Any)
        // print(surname as Any)
        
        let cardParams = STPCardParams()
        
        cardParams.number       = String(cell.txtCardNumber.text!)
        cardParams.expMonth     = UInt(name)!
        cardParams.expYear      = UInt(surname)!
        cardParams.cvc          = String(cell.txtCVV.text!)
        
        STPAPIClient.shared.createToken(withCard: cardParams) { token, error in
            guard let token = token else {
                // Handle the error
                // print(error as Any)
                // print(error?.localizedDescription as Any)
                ERProgressHud.sharedInstance.hide()
                
                let alert = UIAlertController(title: "Error", message: String(error!.localizedDescription), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
                
                return
            }
            let tokenID = token.tokenId
            print(tokenID)
            
            // push!.strFromWhere              = "spcialOffer"
            
            if self.strFromWhere == "spcialOffer" {
                //
                
                if self.specialOfferScreenImplemented == "yesIAmSpecial" {
                    self.addSpecialOfferWB(strTokenIdIs: tokenID)
                }
                
                
            } else if self.strFromWhere == "serviceCustomerNew" {
                 self.serviceCustomerPaymentWB(strTokenIdIs: tokenID)
                // self.finalPaymentIsAfterStripe(strTokenIdIs: tokenID)
            } else {
                self.fullAndFinalBW(strTokenIdIs: tokenID)
            }
             
        }
        
        
    }
    
    
    // MARK:- FINAL PAYMENT -
    @objc func finalPaymentIsAfterStripe(strTokenIdIs:String) {
        
        print(strTokenIdIs as Any)
        
        /*
        action:chargeramount
        userId:25
        amount:100
        tokenID:tok_1GxrrzIudZlr53ucmrz4pyyq
        */
        
        let urlString = ORDER_FOOD_GENERATE_TOKEN
        
        // serviceAmount
        
        let strTotalAmount = Double(serviceAmount)!*100
            
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // let str:String = person["role"] as! String
                   parameters = [
                    "action"    : "chargeramount",
                    "userId"    : person["userId"] as Any,
                    "amount"    : strTotalAmount,//String("500"),
                    "tokenID"   : String(strTokenIdIs)
            ]
        }
        
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
               
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                               
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                    if strSuccess == String("success") {
                        print("yes")
                                 
                                // ERProgressHud.sharedInstance.hide()
                                
                                
                        // self.callAfterFullAnFinalPayment(transactionIdIs: JSON["transactionID"] as! String)
                                
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                                
                                
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                               
                ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    
    
    
    
    // service
    @objc func serviceCustomerPaymentWB(strTokenIdIs:String!) {
          // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
          let urlString = BASE_URL_EXPRESS_PLUS
    
          var parameters:Dictionary<AnyHashable, Any>!
           if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         
            /*
             [TIP] => 10.0
             [action] => updatepayment
             [groupId] => 61
             [serviceAmount] => 500
             [serviceType] => 8
             [totalAmount] => 510.0
             [transactionId] => tok_1Gs2pKBnk7ygV50qJ3Z9kYDZ
             [userId] => 138
             */
            
            /*
             var seviceTipAmount:String!
             var seviceGroupId:String!
             var serviceAmount:String!
             var seviceType:String!
             var serviceTotalAmount:String!
             */
            
            parameters = [
                "action"           : "updatepayment",
                "userId"           : person["userId"] as Any,
                "groupId"          : String(seviceGroupId),
                "serviceAmount"    : String(serviceAmount),
                "serviceType"      : String(seviceType),
                "totalAmount"      : String(self.getTotalPayment2),
                // "transactionId"    : String(strTokenIdIs),
                "transactionId"    : String(""),
                "TIP"              : String(seviceTipAmount),
                         
            ]
           }
        
        print("parameters-------\(String(describing: parameters))")
                     
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                             response in
                 
                             switch(response.result) {
                             case .success(_):
                                if let data = response.result.value {

                                 let JSON = data as! NSDictionary
                                  print(JSON as Any)
                                 
                                 var strSuccess : String!
                                 strSuccess = JSON["status"]as Any as? String
                                 
                                   // var strSuccessAlert : String!
                                   // strSuccessAlert = JSON["msg"]as Any as? String
                                 
                                 if strSuccess == String("success") {
                                  print("yes")
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   
                                   // var dict: Dictionary<AnyHashable, Any>
                                  //  dict = JSON["msg"] as! Dictionary<AnyHashable, Any>
                                  
                                  // var strSuccess : String!
                                  // strSuccess = JSON["msg"]as Any as? String
                                    if self.strFromWhere == "serviceCustomerNew" {
                                        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CustomerServiceSuccessId") as? CustomerServiceSuccess
                                        push!.iamTotalPrice = self.getTotalPayment2
                                        self.navigationController?.pushViewController(push!, animated: true)
                                    } else {
                                        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
                                        self.navigationController?.pushViewController(push!, animated: true)
                                    }
                                    
                                   
                                 }
                                 else {
                                  print("no")
                                   ERProgressHud.sharedInstance.hide()
                                 }
                             }

                             case .failure(_):
                                 print("Error message:\(String(describing: response.result.error))")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                 
                                 let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                         UIAlertAction in
                                         NSLog("OK Pressed")
                                     }
                                 
                                 alertController.addAction(okAction)
                                 
                                 self.present(alertController, animated: true, completion: nil)
                                 
                                 break
                              }
                         }
          }
    
    
    @objc func addSpecialOfferWB(strTokenIdIs:String!) {
          // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
        let urlString = BASE_URL_EXPRESS_PLUS
    
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
              /*
         [action] => addfooorder
         [deliveryLat] => 28.5871275
         [deliveryLong] => 77.0605771
         [userId] => 138
         [address] => Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India
         [state] => Delhi
         [city] => Delhi
         [zipcode] => 110075
         [name] => hehd
         [phone] => 9565884859
         [landmark] => vdvs
         [workPlace] => Home
         [whatYouWant] => white sauce pasta
         [amount] => 61.0
         [TIP] => 5.0
         [deliveryFee] => 6
         [totalAmount] => 72.0
         [cardNo] => 12/25
         [transactionId] => tok_1GriptBnk7ygV50q72lKlXKM
         [salesTax] => 0
         */
            
            /*
             print(getTextOneFromTaxScreen2 as Any)
             print(getestimatePrice2 as Any)
             print(getTip2 as Any)
             print(getDeliveryFee2 as Any)
             print(getTexttWOFromTaxScreen2 as Any)
             print(getNotes2 as Any)
             print(getContactOnDelivery2 as Any)
             print(getTotalPayment2 as Any)
             print(latitude2 as Any)
             print(longitude2 as Any)
             print(address2 as Any)
             print(state2 as Any)
             print(city2 as Any)
             print(zipcode2 as Any)
             print(landmark2 as Any)
             print(workplace2 as Any)
             print(stateTax2 as Any)
             */
            
            /*
             [action] => addrequest
             [latitude] => 28.5871357
             [longitude] => 77.0606099
             [userId] => 267
             
             [address] => Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India
             [state] => Delhi
             [city] => Delhi
             [zipcode] => 110075
             [name] => bfbdb
             [phone] => 9598959565
             [landmark] => v
             [workPlace] => Home
             [whatYouWant] => h
             [price] => 2016.0
             [TIP] => 10.0
             [deliveryFee] => 6
             [totalAmount] => 2032.0
             [cardNo] => 12/25
             [transactionId] => tok_1HSitqIudZlr53ucP3y5WNJL
             [salesTax] => 0
             */
            parameters = [
                "action"       : "addrequest",
                "userId"       : person["userId"] as Any,
                "latitude"     : String(latitude2), //
                "longitude"    : String(longitude2), //
                "address"      : String(address2), //
                "state"        : String(state2), //
                "city"         : String(city2), //
                "zipcode"      : String(zipcode2), //
                "name"         : String(self.namee2), //
                "phone"        : String(self.phonee2), //
                "landmark"     : String(landmark2),//
                "workPlace"    : String(workplace2), //
                "whatYouWant"  : String(strWhatDoYouWant2), //
                "price"        : String(getestimatePrice2), // price
                "TIP"          : String(getTip2), //
                "deliveryFee"  : String(getDeliveryFee2), //
                "totalAmount"  : String(getTotalPayment2), // total amount
                "transactionId": String(strTokenIdIs), //
                "salesTax"     : String(stateTax2),//
                "cardNo"        : String("12/25")
                        
            ]
        }
        print("parameters-------\(String(describing: parameters))")
                     
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
                 
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                 
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                                 
                                   // var strSuccessAlert : String!
                                   // strSuccessAlert = JSON["msg"]as Any as? String
                                 
                    if strSuccess == String("success") {
                        print("yes")
                                   
                        ERProgressHud.sharedInstance.hide()
                                   
                                   // var dict: Dictionary<AnyHashable, Any>
                                  //  dict = JSON["msg"] as! Dictionary<AnyHashable, Any>
                                  
                                  // var strSuccess : String!
                                  // strSuccess = JSON["msg"]as Any as? String
                               
                        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
                        self.navigationController?.pushViewController(push!, animated: true)
                                   
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                }

                             case .failure(_):
                                 print("Error message:\(String(describing: response.result.error))")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                 
                                 let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                         UIAlertAction in
                                         NSLog("OK Pressed")
                                     }
                                 
                                 alertController.addAction(okAction)
                                 
                                 self.present(alertController, animated: true, completion: nil)
                                 
                                 break
                            }
        }
    }
    
    @objc func fullAndFinalBW(strTokenIdIs:String!) {
          // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
          let urlString = BASE_URL_EXPRESS_PLUS
    
          var parameters:Dictionary<AnyHashable, Any>!
           if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
              /*
         [action] => togetrequest
         [latitude] => 28.5870514
         [longitude] => 77.0605172
         [userId] => 80
         [address] => Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India
         [state] => Delhi
         [city] => Delhi
         [zipcode] => 110075
         [name] => ok
         [phone] => 9595959550
         [landmark] => vdvd
         [workPlace] => Home
         [notes] => test notes
         [EPrice] => 51.0
         [TIP] => 22.0
         [deliveryFee] => 6
         [wahtUwant] => test
         [StoreCity] => test
         [noContac] => 0
         [transactionId] => tok_1GqDEKBnk7ygV50q4kWNeWnV
         [salesTax] => 0
         */
            /*
             print(getTextOneFromTaxScreen2 as Any)
             print(getestimatePrice2 as Any)
             print(getTip2 as Any)
             print(getDeliveryFee2 as Any)
             print(getTexttWOFromTaxScreen2 as Any)
             print(getNotes2 as Any)
             print(getContactOnDelivery2 as Any)
             print(getTotalPayment2 as Any)
             print(latitude2 as Any)
             print(longitude2 as Any)
             print(address2 as Any)
             print(state2 as Any)
             print(city2 as Any)
             print(zipcode2 as Any)
             print(landmark2 as Any)
             print(workplace2 as Any)
             print(stateTax2 as Any)
             */
            
            
            // E-Price = tip + delivery
            
            // eprice
            let myString1 = String(getestimatePrice2)
            let myInt1 = Int(myString1)
            
            // tip
            let myString2 = String(getTip2)
            let myInt2 = Int(myString2)
            
            // delivery
            let myString3 = String(getDeliveryFee2)
            let myInt3 = Int(myString3)
            
            let sumOfAllValue = myInt1!+myInt2!+myInt3!
             
            let x : Int = sumOfAllValue
            let andTheTotalPriceIs = String(x)
            
                     parameters = [
                         "action"       : "togetrequest",
                         "userId"       : person["userId"] as Any,
                         "latitude"     : String(latitude2),
                         "longitude"    : String(longitude2),
                         "address"      : String(address2),
                         "state"        : String(state2),
                         "city"         : String(city2),
                         "zipcode"      : String(zipcode2),
                         "name"         : String(self.namee2),
                         "phone"        : String(self.phonee2),
                         "landmark"     : String(landmark2),
                         "workPlace"    : String(workplace2),
                         "notes"        : String(getNotes2),
                         "EPrice"       : String(andTheTotalPriceIs),
                         "TIP"          : String(getTip2),
                         "deliveryFee"  : String(getDeliveryFee2),
                         "wahtUwant"    : String(getTextOneFromTaxScreen2),
                         "StoreCity"    : String(getTexttWOFromTaxScreen2),
                         "noContac"     : String(getContactOnDelivery2),
                         "transactionId": String(strTokenIdIs),
                         "salesTax"     : String(stateTax2),
                     ]
           }
                     print("parameters-------\(String(describing: parameters))")
                     
                     Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                         {
                             response in
                 
                             switch(response.result) {
                             case .success(_):
                                if let data = response.result.value {

                                 let JSON = data as! NSDictionary
                                  print(JSON as Any)
                                 
                                 var strSuccess : String!
                                 strSuccess = JSON["status"]as Any as? String
                                 
                                   // var strSuccessAlert : String!
                                   // strSuccessAlert = JSON["msg"]as Any as? String
                                 
                                 if strSuccess == String("success") {
                                  print("yes")
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   
                                   // var dict: Dictionary<AnyHashable, Any>
                                  //  dict = JSON["msg"] as! Dictionary<AnyHashable, Any>
                                  
                                  // var strSuccess : String!
                                  // strSuccess = JSON["msg"]as Any as? String
                               
                                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
                                    self.navigationController?.pushViewController(push!, animated: true)
                                   
                                 }
                                 else {
                                  print("no")
                                   ERProgressHud.sharedInstance.hide()
                                 }
                             }

                             case .failure(_):
                                 print("Error message:\(String(describing: response.result.error))")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                 
                                 let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                         UIAlertAction in
                                         NSLog("OK Pressed")
                                     }
                                 
                                 alertController.addAction(okAction)
                                 
                                 self.present(alertController, animated: true, completion: nil)
                                 
                                 break
                              }
                         }
          }
}


//MARK:- TABLE VIEW -
extension UPPaymentScreen: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UPPaymentScreenTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UPPaymentScreenTableCell
        
        cell.backgroundColor = .white
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.txtCardNumber.delegate = self
        cell.txtExpDate.delegate = self
        cell.txtCVV.delegate = self
        
        cell.txtCardNumber.addTarget(self, action: #selector(UPPaymentScreen.textFieldDidChange(_:)), for: .editingChanged)
        cell.txtExpDate.addTarget(self, action: #selector(UPPaymentScreen.textFieldDidChange2(_:)), for: .editingChanged)
        
        return cell
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPPaymentScreenTableCell
            
         self.lblCardNumberHeading.text! = cell.txtCardNumber.text!
        
    }
    @objc func textFieldDidChange2(_ textField: UITextField) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPPaymentScreenTableCell
            
         self.lblEXPDate.text! = cell.txtExpDate.text!
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPPaymentScreenTableCell
        
        if textField == cell.txtCardNumber {
            print(self.lblCardNumberHeading.text!.count+1 as Any)
            
            if self.lblCardNumberHeading.text!.count+1 == 2 {
                print("check card")
            } else {
                print("do not check card")
            }
            
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 16
        }
        
        if textField == cell.txtExpDate {
            if string == "" {
                return true
            }

            
            let currentText = textField.text! as NSString
            let updatedText = currentText.replacingCharacters(in: range, with: string)

            textField.text = updatedText
            let numberOfCharacters = updatedText.count
            
            if numberOfCharacters == 2 {
                textField.text?.append("/")
            }
            self.lblEXPDate.text! = cell.txtExpDate.text!
        }
        
       if textField == cell.txtCVV {
           
           guard let textFieldText = textField.text,
               let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                   return false
           }
           let substringToReplace = textFieldText[rangeOfTextToReplace]
           let count = textFieldText.count - substringToReplace.count + string.count
        
        
        
        /*if self.strWhatCardIam == "amex" {
            return count <= 4
        } else {
            return count <= 3
        }*/
        
        return count <= 3
        
        
        
       }
        
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
    
}

extension UPPaymentScreen: UITableViewDelegate {
    
}

