//
//  CustomerServiceSuccess.swift
//  ExpressPlus
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CustomerServiceSuccess: UIViewController {

    var iamTotalPrice:String!
    
    @IBOutlet weak var lblTotalPrice:UILabel!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var btnPlaceOrder:UIButton! {
        didSet {
            btnPlaceOrder.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnPlaceOrder.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "SERVICE CONFIRMED"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTotalPrice.text = "Total Charge $"+String(iamTotalPrice)
        
        btnPlaceOrder.addTarget(self, action: #selector(checkUncheckClickMethod), for: .touchUpInside)
    }
    
    @objc func checkUncheckClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPDashboardId") as? UPDashboard
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
}
