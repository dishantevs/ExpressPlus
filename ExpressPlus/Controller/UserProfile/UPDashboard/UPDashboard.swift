//
//  Dashboard.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class UPDashboard: UIViewController {

    let cellReuseIdentifier = "dashboardTableCell"
    
    var titleArray = ["Food","To get confirmed","To Get"]
    var imageArray = ["food1","service1","toGet1"]
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "DASHBOARD"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
        }
    }
    
    @IBOutlet weak var lblWelcomeUserTitle:UILabel!
    
     override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
           
         self.sideBarMenuClick()
        
        /*
        let defaults = UserDefaults.standard
        defaults.setValue("", forKey: "keyLoginFullData")
        defaults.setValue(nil, forKey: "keyLoginFullData")
        */
        
        
        // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
            
        self.lblWelcomeUserTitle.text = " '50% of all net profits feed and support starving children' "
        // }
        
         // self.funcd()
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPFoodId") as? UPFood
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    
    @objc func funcd() {
        
        let time1 = "11:00"
        let time2 = "11:50"

        let formatter = DateFormatter()
        // formatter.dateFormat = "hh:mma"
        formatter.dateFormat = "hh:mm"
        
        let date1 = formatter.date(from: time1)!
        let date2 = formatter.date(from: time2)!

        let elapsedTime = date2.timeIntervalSince(date1)

        // convert from seconds to hours, rounding down to the nearest hour
        let hours = floor(elapsedTime / 60 / 60)

        // we have to subtract the number of seconds in hours from minutes to get
        // the remaining minutes, rounding down to the nearest minute (in case you
        // want to get seconds down the road)
        let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)

        // print("\(Int(hours)) hr and \(Int(minutes)) min")
        print("\(Int(hours)).\(Int(minutes))")
        print(type(of: "\(Int(hours)).\(Int(minutes))"))
        
        let str : NSString = "\(Int(hours)).\(Int(minutes))" as NSString
        let xa : Float = str.floatValue
        let xa1 : Double = str.doubleValue
        
        print(xa as Any)
        print(xa1 as Any)
        
//      print(type(of: xa))
        
        print(type(of: xa1))
        
        print(35.0*xa1)
    }
    
    @IBAction func dp(_ sender:UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        // textfieldjobdate.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func sideBarMenuClick() {
        
        self.view.endEditing(true)
           if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
    }
    
    @objc func firstLogin() {
           
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
                  
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        /*
         [action] => login
         [email] => purnimac@gmail.com
         [password] => 123456
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        
        parameters = [
            "action"             : "login",
            "email"              : String("purnimac@mailinator.com"),
            "password"           : String("123456")
        ]
                 
        print("parameters-------\(String(describing: parameters))")
                      
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
                  
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                  
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                    if strSuccess == String("success") {
                        print("yes")
                                    
                        ERProgressHud.sharedInstance.hide()
                                    
                        var dict: Dictionary<AnyHashable, Any>
                        dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                   
                        let defaults = UserDefaults.standard
                        defaults.setValue(dict, forKey: "keyLoginFullData")
                                    
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                                  
                ERProgressHud.sharedInstance.hide()
                                  
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                                  
                alertController.addAction(okAction)
                                  
                self.present(alertController, animated: true, completion: nil)
                                  
                break
            }
        }
    }
}

//MARK:- TABLE VIEW -
extension UPDashboard: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DashboardTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! DashboardTableCell
        
        cell.backgroundColor = .white
       
        cell.lblTitle.text = titleArray[indexPath.row]
        cell.imgVieww.image = UIImage(named: imageArray[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 0 {
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPFoodId") as? UPFood
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
        } else if indexPath.row == 1 {
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ServicesId") as? Services
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
        } else if indexPath.row == 2 {
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPToGetId") as? UPToGet
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}

extension UPDashboard: UITableViewDelegate {
    
}

