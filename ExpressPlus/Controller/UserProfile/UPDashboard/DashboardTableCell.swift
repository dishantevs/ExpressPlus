//
//  DashboardTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class DashboardTableCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    
    @IBOutlet weak var imgVieww:UIImageView! {
        didSet {
            imgVieww.layer.cornerRadius = 8
            imgVieww.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
