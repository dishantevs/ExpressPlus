//
//  ServiceBookingDetails.swift
//  ExpressPlus
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ServiceBookingDetails: UIViewController {

    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    @IBOutlet weak var bottomNav:UIView! {
        didSet {
            
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "SERVICE BOOKING DETAILS"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
