//
//  UPToGetDetails.swift
//  ExpressPlus
//
//  Created by Apple on 03/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UPToGetDetails: UIViewController {

    var fromDriver:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "TO GET DETAILS"
        }
    }
    
    var dictGetAllDetailsToGet:NSDictionary!
    
    @IBOutlet weak var lblProductNmae:UILabel!
    @IBOutlet weak var lblWhatStore:UILabel!
    @IBOutlet weak var lblEstimatedPrice:UILabel!
    @IBOutlet weak var lblTipAmount:UILabel!
    @IBOutlet weak var lblDeliveryFee:UILabel!
    @IBOutlet weak var lblSpecialNotes:UILabel!
    @IBOutlet weak var lblStateTax:UILabel!
    
    @IBOutlet weak var lblRequestMoney:UILabel!
    
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lblUserAddress:UILabel!
    
    @IBOutlet weak var btnBack:UIButton!
    @IBOutlet weak var btnTotalPrice:UIButton! {
        didSet {
            btnTotalPrice.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnTotalPrice.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var imgProfile:UIImageView! {
        didSet {
            imgProfile.layer.cornerRadius = 30
            imgProfile.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        
        print(dictGetAllDetailsToGet as Any)
        
        /*
         EPrice = 114;
         PaymentDate = "";
         RequestDate = "";
         RequestStatus = 0;
         StoreCity = tgbk;
         TIP = 25;
         address = "Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India";
         created = "2020-05-27 19:10:00";
         deliveryFee = 6;
         latidude = "";
         longitude = "77.0605227";
         noContact = "";
         notes = tyu;
         requestMoney = "";
         salesTax = 0;
         status = 1;
         togetrequestId = 54;
         userId = 80;
         userImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1591188029expressPlusEditProfilePhoto.jpg";
         userName = purnimacr2;
         wahtUwant = tyty;
         */
        
        imgProfile.sd_setImage(with: URL(string: (dictGetAllDetailsToGet["userImage"] as! String)), placeholderImage: UIImage(named: "user"))
        
        self.lblUserName.text    = (dictGetAllDetailsToGet["userName"] as! String)
        self.lblUserAddress.text    = (dictGetAllDetailsToGet["address"] as! String)
        
        self.lblProductNmae.text    = (dictGetAllDetailsToGet["wahtUwant"] as! String)
        self.lblWhatStore.text      = (dictGetAllDetailsToGet["StoreCity"] as! String)
        self.lblEstimatedPrice.text = "$ "+(dictGetAllDetailsToGet["EPrice"] as! String)
        self.lblTipAmount.text      = "$ "+(dictGetAllDetailsToGet["TIP"] as! String)
        let x : Int = (dictGetAllDetailsToGet["deliveryFee"] as! Int)
        let myString = String(x)
        self.lblDeliveryFee.text    = "$ "+String(myString)
        self.lblSpecialNotes.text   = (dictGetAllDetailsToGet["notes"] as! String)
        // self.lblStateTax.text       = "$ "+(dictGetAllDetailsToGet["salesTax"] as! String)
        
        if dictGetAllDetailsToGet["salesTax"] is String {
                          
            print("Yes, it's a String")
          
            self.lblStateTax.text = "$ "+(dictGetAllDetailsToGet["salesTax"] as! String)

        } else if dictGetAllDetailsToGet["salesTax"] is Int {
          
            print("It is Integer")
          
            let x2 : Int = (dictGetAllDetailsToGet["salesTax"] as! Int)
            let myString2 = String(x2)
            self.lblStateTax.text = "$ "+myString2
          
        } else {
        //some other check
          print("i am ")
          
            let temp:NSNumber = dictGetAllDetailsToGet["salesTax"] as! NSNumber
            let tempString = temp.stringValue
            self.lblStateTax.text = "$ "+tempString
          
        }
        
        /*
        let num1 = 4;
        let num2 = 6;
        let num3 = 10;
        
        var ans = Int();
        ans = num1+num2+num3;

        print("num1 + num2 = ",ans);
        */
        
        let myString1 = (dictGetAllDetailsToGet["EPrice"] as! String)
        let myInt1 = Int(myString1)
        
        let myString2 = (dictGetAllDetailsToGet["TIP"] as! String)
        let myInt2 = Int(myString2)
        
        let addPrice = myInt1
        let addTip = myInt2
        let addDeliveryFee = (dictGetAllDetailsToGet["deliveryFee"] as! Int)
        
        // add request money also
        // lblRequestMoney
        
        // let addRequestMoney = (dictGetAllDetailsToGet["requestMoney"] as! Int)
        
        
        
        var ans2 = Int();
        ans2 = addPrice!+addTip!+addDeliveryFee// +addRequestMoney;
        
        let x2 : Int = ans2
        let myString3 = String(x2)
        
        if fromDriver == "iAmFromDriverScreen" {
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                if person["role"] as! String == "Member" {
                    self.btnTotalPrice.setTitle("$ "+String(myString3), for: .normal)
                } else {
                    self.btnTotalPrice.setTitle("Delivered "+"$ "+String(myString3), for: .normal)
                }
            }
            self.btnTotalPrice.backgroundColor = .systemOrange
        } else {
            self.btnTotalPrice.setTitle("$ "+String(myString3), for: .normal)
        }
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
}
