//
//  ToGetConfirmed.swift
//  ExpressPlus
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ToGetConfirmed: UIViewController {

    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    @IBOutlet weak var viewNavBottom:UIView! {
        didSet {
            viewNavBottom.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            viewNavBottom.layer.cornerRadius = 6
            viewNavBottom.clipsToBounds = true
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "TO GET CONFIRMED"
        }
    }
    
    @IBOutlet weak var bottomOne:UIView! {
        didSet {
            bottomOne.backgroundColor = .white
        }
    }
    @IBOutlet weak var bottomTwo:UIView! {
        didSet {
            bottomTwo.backgroundColor = .white
        }
    }
    
    // 219 , 87 , 39
    // 217 , 48 , 74
    
    @IBOutlet weak var btnCallDriver:UIButton! {
        didSet {
            btnCallDriver.backgroundColor = UIColor.init(red: 219.0/255.0, green: 87.0/255.0, blue: 39.0/255.0, alpha: 1)
            btnCallDriver.layer.cornerRadius = 6
            btnCallDriver.clipsToBounds = true
        }
    }
    @IBOutlet weak var btnCancelRequest:UIButton! {
        didSet {
            btnCancelRequest.backgroundColor = UIColor.init(red: 217.0/255.0, green: 48.0/255.0, blue: 74.0/255.0, alpha: 1)
            btnCancelRequest.layer.cornerRadius = 6
            btnCancelRequest.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    @IBOutlet weak var btnTrackYourOrder:UIButton! {
        didSet {
            btnTrackYourOrder.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnTrackYourOrder.setTitleColor(.white, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnTrackYourOrder.addTarget(self, action: #selector(trackYourOrderClickMethod), for: .touchUpInside)
    }
    
    @objc func trackYourOrderClickMethod() {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPTrackToGetRequestId") as? UPTrackToGetRequest
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }

}
