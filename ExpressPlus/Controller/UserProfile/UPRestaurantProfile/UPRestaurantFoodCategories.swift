//
//  UPRestaurantFoodCategories.swift
//  ExpressPlus
//
//  Created by Apple on 11/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UPRestaurantFoodCategories: UICollectionViewCell {
    
    @IBOutlet weak var lblFoodCategory:UILabel! {
        didSet {
            lblFoodCategory.textColor = .white
        }
    }
    
}
