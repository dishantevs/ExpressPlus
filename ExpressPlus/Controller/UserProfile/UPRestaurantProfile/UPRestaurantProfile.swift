//
//  UPRestaurantProfile.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import ValueStepper

import Alamofire
import SDWebImage

class UPRestaurantProfile: UIViewController, UITextFieldDelegate {

    let cellReuseIdentifier = "uPRestaurantProfileTableCell"
    
    var dictGetRestaurantFoodListing:NSDictionary!
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
    
    var page : Int! = 1
    var loadMore : Int! = 1;
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrdersCategories:NSMutableArray! = [] // Array<Any>!
    
    // var page : Int! = 1
    // var loadMore : Int! = 1;
    
    
    var myPriceChange:String!
    var mySpecialPriceChange:String!
    var arrMyDynamicArray:NSMutableArray! = []
    
    @IBOutlet weak var navigationBar:UIView! {
          didSet {
              navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
          }
      }
      
    @IBOutlet weak var lblTotalItemInCart:UILabel! {
        didSet {
            lblTotalItemInCart.layer.cornerRadius = 10
            lblTotalItemInCart.clipsToBounds = true
            lblTotalItemInCart.backgroundColor = .systemRed
            lblTotalItemInCart.textColor = .white
        }
    }
    
      @IBOutlet weak var lblNavigationTitle:UILabel! {
          didSet {
              lblNavigationTitle.text = "FOOD"
          }
      }
      
      @IBOutlet weak var btnBack:UIButton! {
          didSet {
              btnBack.setTitle("|||", for: .normal)
          }
      }
      
    @IBOutlet weak var clView:UICollectionView! {
        didSet {
            clView.delegate = self
            clView.dataSource = self
        }
    }
      
    @IBOutlet weak var btnCart:UIButton! {
        didSet {
            btnCart.tintColor = .white
        }
    }
    
    @IBOutlet weak var clViewCategories: UICollectionView! {
        didSet {
               //collection
            // clViewCategories!.dataSource = self
            // clViewCategories!.delegate = self
            clViewCategories!.backgroundColor = .white
            clViewCategories.isPagingEnabled = true
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.lblTotalItemInCart.isHidden = true
        
        self.btnCart.addTarget(self, action: #selector(cartClickMethod), for: .touchUpInside)
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
         
        
        // self.sideBarMenuClick()
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.foodListingCallINITIAL()
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func cartClickMethod() {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPCartId") as? UPCart
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    
    @objc func foodListingCallINITIAL() {
          // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.arrListOfAllMyOrders.removeAllObjects()
        self.arrListOfAllMyOrdersCategories.removeAllObjects()
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
                  
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        // print(dictGetRestaurantFoodListing as Any)
        
        /*
         address = "Gwalior, Madhya Pradesh, India";
         contactNumber = 9464645787;
         created = "2020-05-05 11:58:00";
         designation = "";
         device = "";
         deviceToken = "";
         email = "satish93@gmail.com";
         experence = "";
         foodTag = Veg;
         fullName = satish;
         image = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1588663958IMG20191105144302.jpg";
         role = Restaurant;
         userId = 104;
         zipCode = "";
         */
        
        self.lblNavigationTitle.text = (dictGetRestaurantFoodListing["fullName"] as! String)
        
        /*
         [action] => menulist
         [restaurantId] => 109
         [userId] => 80
         [categoryId] => 67
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        
            parameters = [
                "action"              : "menulist",
                "restaurantId"        : dictGetRestaurantFoodListing["userId"] as Any,
                "userId"              : person["userId"] as Any,
                "categoryId"          : ""
                          
            ]
        }
        print("parameters-------\(String(describing: parameters))")
                      
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
        {
            response in
                  
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                  
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                    if strSuccess == String("success") {
                        print("yes")
                                    
                        ERProgressHud.sharedInstance.hide()
                                    
                                    // var dict: Dictionary<AnyHashable, Any>
                                    // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                   
                        let x : Int = JSON["totalCartItem"] as! Int
                        let myString = String(x)
                        
                        if myString == "0" {
                            self.lblTotalItemInCart.isHidden = true
                        } else if myString == "" {
                            self.lblTotalItemInCart.isHidden = true
                        } else {
                            self.lblTotalItemInCart.isHidden = false
                            self.lblTotalItemInCart.text = String(myString)
                        }
                                    
                                    
                                    
                        var ar : NSArray!
                        ar = (JSON["data"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                    
                        var arCategories : NSArray!
                        arCategories = (JSON["categoryList"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyOrdersCategories.addObjects(from: arCategories as! [Any])
                           
                        
                        /*for index1 in 0..<self.arrListOfAllMyOrders.count {
                            
                            let item = self.arrListOfAllMyOrders[index1] as? [String:Any]
                            
                            
                            // price
                            if item!["price"] is String {
                                
                                print("Yes, it's a String")
                                self.myPriceChange = "$ "+(item!["price"] as! String)
                                
                            } else if item!["price"] is Int {
                                
                              print("It is Integer")
                                let x : Int = item!["price"] as! Int
                                let myString = String(x)
                                self.myPriceChange = "$ "+String(myString)
                                
                            } else if item!["price"] is NSNumber {
                              print("It is nsnimber")
                              //some other check
                                
                                let temp:NSNumber = item!["price"] as! NSNumber
                                let tempString = temp.stringValue
                                self.myPriceChange = String(tempString)
                                
                            }
                            
                            // special price
                            if item!["specialPrice"] is String {
                                
                                print("Yes, it's a String")
                                self.mySpecialPriceChange = "$ "+(item!["specialPrice"] as! String)
                                
                           } else if item!["specialPrice"] is Int {
                            
                            print("It is Integer")
                            let x : Int = item!["specialPrice"] as! Int
                            let myString = String(x)
                            self.mySpecialPriceChange = "$ "+String(myString)
                               
                           } else if item!["specialPrice"] is NSNumber {
                            
                            print("It is nsnimber")
                             //some other check
                            let temp:NSNumber = item!["specialPrice"] as! NSNumber
                            let tempString = temp.stringValue
                            self.mySpecialPriceChange = String(tempString)
                            
                           }
                            
                            let x12 : Int = item!["categoryId"] as! Int
                            let myString12 = String(x12)
                            
                            let x1 : Int = item!["resturentId"] as! Int
                            let myString1 = String(x1)
                            
                            let x2 : Int = item!["menuId"] as! Int
                            let myString2 = String(x2)
                            
                            let x3 : Int = item!["quantity"] as! Int
                            let myString3 = String(x3)
                            
                            let myDictionary: [String:String] = [
                                
                                "categoryId"    : String(myString12),
                                "categoryName"  : (item!["categoryName"] as! String),
                                "created"       : (item!["created"] as! String),
                                "description"   : (item!["description"] as! String),
                                "foodName"      : (item!["foodName"] as! String),
                                "foodTag"       : (item!["foodTag"] as! String),
                                "image_1"       : (item!["image_1"] as! String),
                                "image_2"       : (item!["image_2"] as! String),
                                "image_3"       : (item!["image_3"] as! String),
                                "image_4"       : (item!["image_4"] as! String),
                                "image_5"       : (item!["image_5"] as! String),
                                "menuId"        : String(myString2),
                                "price"         : String(self.myPriceChange),
                                "quantity"      : String(myString3),
                                "resturentId"   : String(myString1),
                                "resturentName" : (item!["resturentName"] as! String),
                                "specialPrice"  : String(self.mySpecialPriceChange),
                                
                            ]
                            
                            var res = [[String: String]]()
                            res.append(myDictionary)
                            
                            self.arrMyDynamicArray.addObjects(from: res)
                            
                        }
                        
                        // print(self.arrMyDynamicArray as Any)
                        */
                        
                        
                        
                        
                        self.clView.delegate = self
                        self.clView.dataSource = self
                        self.clView.reloadData()
                        
                        self.clViewCategories.delegate = self
                        self.clViewCategories.dataSource = self
                        self.clViewCategories.reloadData()
                        
                        
                        
                        
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                                   
                                   
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                                  
                ERProgressHud.sharedInstance.hide()
                                  
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                                  
                alertController.addAction(okAction)
                                  
                self.present(alertController, animated: true, completion: nil)
                                  
                break
            }
        }
       
    }
    
    /*
     [action] => menulist
         [restaurantId] => 512
         [userId] => 510
         [categoryId] => 68
     */
    
    // MARK:- WEBSERVICE ( ADD TO CART ) -
    @objc func addToCartWB(strFoodId:String,strQuantity:String) {
         // self.arrListOfAllMyOrders.removeAllObjects()
        
     ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
     
     /*
      [action] => addcart
      [userId] => 80
      [foodId] => 48
      [quantity] => 1
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
            let x1 : Int = person["userId"] as! Int
            let myString1 = String(x1)
            
                   parameters = [
                       "action"              : "addcart",
                       "foodId"              : String(strFoodId),
                       "userId"              : String(myString1),
                       "quantity"            : String(strQuantity)
             ]
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                  var strSuccessAlert : String!
                                  strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 /*let x : Int = JSON["totalCartItem"] as! Int
                                 let myString = String(x)
                                 
                                 if myString == "0" {
                                     self.lblTotalItemInCart.isHidden = true
                                 } else if myString == "" {
                                     self.lblTotalItemInCart.isHidden = true
                                 } else {
                                     self.lblTotalItemInCart.isHidden = false
                                     self.lblTotalItemInCart.text = String(myString)
                                 }
                                
                                self.clView.reloadData()
                                
                                ERProgressHud.sharedInstance.hide()
                                */
                                /*self.arrListOfAllMyOrders.removeAllObjects()
                                self.dummyReLoadData()*/
                                 
                                 self.foodListingCallINITIAL()
                                
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                let alert = UIAlertController(title: String("Replace Cart Items?"), message: String(strSuccessAlert), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Yes, Replace", style: UIAlertAction.Style.default, handler: { action in
                                    
                                    self.clearAllCart()
                                    
                                }))
                                
                                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: { action in
                                    // self.dismiss(animated: true, completion: nil)
                                 
                                    
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
       }
    
    @objc func dummyReLoadData() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
        // self.arrListOfAllMyOrders.removeAllObjects()
        self.arrListOfAllMyOrdersCategories.removeAllObjects()
     
        let urlString = BASE_URL_EXPRESS_PLUS
        
        self.lblNavigationTitle.text = (dictGetRestaurantFoodListing["fullName"] as! String)
      
      /*
       [action] => menulist
       [restaurantId] => 109
       [userId] => 80
       [categoryId] => 67
       */
      
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
      
            parameters = [
                "action"              : "menulist",
                "restaurantId"        : dictGetRestaurantFoodListing["userId"] as Any,
                "userId"              : person["userId"] as Any,
                "categoryId"          : ""
                        
            ]
        }
        print("parameters-------\(String(describing: parameters))")
                    
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
        {
            response in
                
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                                
                                  // var strSuccessAlert : String!
                                  // strSuccessAlert = JSON["msg"]as Any as? String
                                
                    if strSuccess == String("success") {
                        print("yes")
                                  
                        ERProgressHud.sharedInstance.hide()
                                  
                                  // var dict: Dictionary<AnyHashable, Any>
                                  // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                 
                        let x : Int = JSON["totalCartItem"] as! Int
                        let myString = String(x)
                                  
                        if myString == "0" {
                            self.lblTotalItemInCart.isHidden = true
                        } else if myString == "" {
                            self.lblTotalItemInCart.isHidden = true
                        } else {
                            self.lblTotalItemInCart.isHidden = false
                            self.lblTotalItemInCart.text = String(myString)
                        }
                                  
                                  
                                  
                        var ar : NSArray!
                        ar = (JSON["data"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                  
                        /*var arCategories : NSArray!
                        arCategories = (JSON["categoryList"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyOrdersCategories.addObjects(from: arCategories as! [Any])*/
                                  
                                  
                                  
                        self.clView.delegate = self
                        self.clView.dataSource = self
                        self.clView.reloadData()
                        
                        /*self.clViewCategories.delegate = self
                        self.clViewCategories.dataSource = self
                        self.clViewCategories.reloadData()*/
                                  
                                  
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                                
                ERProgressHud.sharedInstance.hide()
                                
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                                
                alertController.addAction(okAction)
                                
                self.present(alertController, animated: true, completion: nil)
                                
                break
            }
        }
     
    }
    
    @objc func clearAllCart() {
        // self.addInitialMutable.removeAllObjects()
     // self.arrListOfAllMyOrders.removeAllObjects()
     
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
        let urlString = BASE_URL_EXPRESS_PLUS
    
        var parameters:Dictionary<AnyHashable, Any>!
     if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
                   parameters = [
                       "action"              : "deleteallcarts",
                       "userId"              : person["userId"] as Any
                       
             ]
     }
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
        {
            response in
               
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                               
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                    if strSuccess == String("success") {
                        print("yes")
                        ERProgressHud.sharedInstance.hide()
                         
                        let x : Int = JSON["totalCartItem"] as! Int
                        let myString = String(x)
                        
                        if myString == "0" {
                            self.lblTotalItemInCart.isHidden = true
                        } else if myString == "" {
                            self.lblTotalItemInCart.isHidden = true
                        } else {
                            self.lblTotalItemInCart.isHidden = false
                            self.lblTotalItemInCart.text = String(myString)
                        }
                        
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                                
                                
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                               
                ERProgressHud.sharedInstance.hide()
                               
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                               
                alertController.addAction(okAction)
                               
                self.present(alertController, animated: true, completion: nil)
                               
                break
            }
        }
    
    }
    
    
    
    // MARK:- WEBSERVICE ( FOOD LISTING CALL AFTER ADD TO CART ) -
    @objc func foodListingCallAfterAddCart(strCategoryId:String) {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...");
        
        self.arrListOfAllMyOrders.removeAllObjects()
     
        let urlString = BASE_URL_EXPRESS_PLUS
               
     // let indexPath = IndexPath.init(row: 0, section: 0)
     // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
     // print(dictGetRestaurantFoodListing as Any)
     
     /*
      [action] => menulist
      [restaurantId] => 109
      [userId] => 80
      [categoryId] => 67
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
     if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
                   parameters = [
                       "action"              : "menulist",
                       "restaurantId"        : dictGetRestaurantFoodListing["userId"] as Any,
                       "userId"              : person["userId"] as Any,
                       "categoryId"          : String(strCategoryId)
                       
             ]
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 // var dict: Dictionary<AnyHashable, Any>
                                 // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                 let x : Int = JSON["totalCartItem"] as! Int
                                 let myString = String(x)
                                 
                                 if myString == "0" {
                                     self.lblTotalItemInCart.isHidden = true
                                 } else if myString == "" {
                                     self.lblTotalItemInCart.isHidden = true
                                 } else {
                                     self.lblTotalItemInCart.isHidden = false
                                     self.lblTotalItemInCart.text = String(myString)
                                 }
                                 
                                 
                                 
                                 var ar : NSArray!
                                 ar = (JSON["data"] as! Array<Any>) as NSArray
                                 self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                 
                                var arCategoru : NSArray!
                                arCategoru = (JSON["categoryList"] as! Array<Any>) as NSArray
                                self.arrListOfAllMyOrdersCategories.addObjects(from: arCategoru as! [Any])
                                
                                 self.clView.delegate = self
                                 self.clView.dataSource = self
                                 
                                self.clView.reloadData()
                                self.loadMore = 1;
                                
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    
    
    // MARK:- WEBSERVICE ( CLEAR CART TO ZERO ) -
    @objc func clearThatFoodItemCartToZero(strFoodId:String,strQuantity:String) {
        
     self.arrListOfAllMyOrders.removeAllObjects()
     
      ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => deletecarts
      [userId] => 80
      [foodId] => 49
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
     if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
                   parameters = [
                       "action"              : "deletecarts",
                       "foodId"              : String(strFoodId),
                       "userId"              : person["userId"] as Any
                       
             ]
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 // ERProgressHud.sharedInstance.hide()
                                 
                                 // var dict: Dictionary<AnyHashable, Any>
                                 // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                 let x : Int = JSON["totalCartItem"] as! Int
                                 let myString = String(x)
                                 
                                 if myString == "0" {
                                     self.lblTotalItemInCart.isHidden = true
                                 } else if myString == "" {
                                     self.lblTotalItemInCart.isHidden = true
                                 } else {
                                     self.lblTotalItemInCart.isHidden = false
                                     self.lblTotalItemInCart.text = String(myString)
                                 }
                                 
                                 self.foodListingCallINITIAL()
                                
                                 /*
                                 var ar : NSArray!
                                 ar = (JSON["data"] as! Array<Any>) as NSArray
                                 self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                 
                                 self.clView.delegate = self
                                 self.clView.dataSource = self
                                 self.clView.reloadData()
                                 self.loadMore = 1;
                                 */
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    
    
    
}

//MARK:- COLLECTION VIEW -
extension UPRestaurantProfile: UICollectionViewDelegate {
    //Write Delegate Code Here
    
}

extension UPRestaurantProfile: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == clView {
            return arrListOfAllMyOrders.count // return arrMyDynamicArray.count // return arrListOfAllMyOrders.count
        } else {
            return arrListOfAllMyOrdersCategories.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == clView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "uPRestaurantProfileCollecionCell", for: indexPath as IndexPath) as! UPRestaurantProfileCollecionCell
        
            cell.layer.cornerRadius = 8
            cell.clipsToBounds = true
        
            cell.backgroundColor = UIColor.white
            cell.layer.borderWidth = 0.5
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.borderWidth = 0.70
           
        /*
         categoryId = 67;
         categoryName = Dinner;
         created = "2020-05-05 13:05:00";
         description = vzhhss;
         foodName = babbSs;
         foodTag = "";
         "image_1" = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/foods/1588664112IMG20200226201604.jpg";
         "image_2" = "";
         "image_3" = "";
         "image_4" = "";
         "image_5" = "";
         menuId = 49;
         price = 25;
         quantity = 0;
         resturentId = 104;
         resturentName = satish;
         specialPrice = 20;
         */
        
            let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
            print(item as Any)
            
            cell.lblFoodName.text = (item!["foodName"] as! String)

            if item!["foodTag"] as! String == "Veg" {
                cell.imgVegNonveg.image = UIImage(named: "veg")
            } else if item!["foodTag"] as! String == "" {
                cell.imgVegNonveg.image = UIImage(named: "veg")
            } else {
                cell.imgVegNonveg.image = UIImage(named: "nonVeg")
            }
        
            // cell.lblOldPrice.text = (item!["price"] as! String)
            
            // cell.lblRealPrice.text = (item!["specialPrice"] as! String)
          
            
            let x3131 : Int = item!["quantity"] as! Int
            let myString3131 = String(x3131)
            
            if String(myString3131) == "0" {
            
                cell.stepper1.isHidden = true
                cell.btnAdd.isHidden = false
            
                cell.btnAdd.tag = indexPath.row
                cell.btnAdd.addTarget(self, action: #selector(addToCartWithQuantityOne), for: .touchUpInside)
            
            } else {
            
                cell.stepper1.isHidden = false
                cell.btnAdd.isHidden = true
            
                let morePrecisePI = Double(myString3131)
                cell.stepper1.value = morePrecisePI!
            
                cell.stepper1.tag = indexPath.row
                cell.stepper1.addTarget(self, action: #selector(valueChanged1), for: .valueChanged)
            
            }
        
            
            
            // print(item!["image_1"] as! String)
            // let trimmed = (item!["image_1"] as! String).filter {!$0.isWhitespace}
            // print(trimmed as Any)
            
            cell.imgFoodProfile.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            
            if let url = (item!["image_1"] as! String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                
             // cell.imgFoodProfile.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "food1"), options: [.continueInBackground, .progressiveDownload])
                 cell.imgFoodProfile.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "food1"))
                
            }
            
            
            
        
            
            cell.layer.cornerRadius = 6
            cell.clipsToBounds = true
            cell.layer.borderWidth = 2
            cell.layer.borderColor = UIColor.darkGray.cgColor
        
            
            if item!["price"] is String {
              print("Yes, it's a String")
                cell.lblOldPrice.text = "$ "+(item!["price"] as! String)
            } else if item!["price"] is Int {
              print("It is Integer")
                let x : Int = item!["price"] as! Int
                let myString = String(x)
                cell.lblOldPrice.text = "$ "+String(myString)
                
            } else if item!["price"] is NSNumber {
              print("It is nsnimber")
              //some other check
                
                let temp:NSNumber = item!["price"] as! NSNumber
                let tempString = temp.stringValue
                cell.lblOldPrice.text = String(tempString)
                
            }
            
            
            if item!["specialPrice"] is String {
             print("Yes, it's a String")
               cell.lblRealPrice.text = "$ "+(item!["specialPrice"] as! String)
           } else if item!["specialPrice"] is Int {
             print("It is Integer")
               let x : Int = item!["specialPrice"] as! Int
               let myString = String(x)
               cell.lblRealPrice.text = "$ "+String(myString)
               
           } else if item!["specialPrice"] is NSNumber {
             print("It is nsnimber")
             //some other check
           }
            
            
            
            
        return cell
            
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "uPRestaurantFoodCategories", for: indexPath as IndexPath) as! UPRestaurantFoodCategories
            
            cell.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            cell.layer.cornerRadius = 4
            cell.clipsToBounds = true

            let item = arrListOfAllMyOrdersCategories[indexPath.row] as? [String:Any]
            cell.lblFoodCategory.text = (item!["name"] as! String)
            
            return cell
        }
    }
    
    @objc func addToCartWithQuantityOne(_ sender:UIButton) {
        
        let btn:UIButton = sender
        let item = self.arrListOfAllMyOrders[btn.tag] as? [String:Any]
        let x : Int = item!["menuId"] as! Int
        let myString = String(x)
        
        
        /*self.arrMyDynamicArray.removeObject(at: btn.tag)
        
        let myDictionary: [String:String] = [
            
            "categoryId"    : (item!["categoryId"] as! String),
            "categoryName"  : (item!["categoryName"] as! String),
            "created"       : (item!["created"] as! String),
            "description"   : (item!["description"] as! String),
            "foodName"      : (item!["foodName"] as! String),
            "foodTag"       : (item!["foodTag"] as! String),
            "image_1"       : (item!["image_1"] as! String),
            "image_2"       : (item!["image_2"] as! String),
            "image_3"       : (item!["image_3"] as! String),
            "image_4"       : (item!["image_4"] as! String),
            "image_5"       : (item!["image_5"] as! String),
            "menuId"        : (item!["menuId"] as! String),
            "price"         : (item!["price"] as! String),
            "quantity"      : String("1"),
            "resturentId"   : (item!["resturentId"] as! String),
            "resturentName" : (item!["resturentName"] as! String),
            "specialPrice"  : (item!["specialPrice"] as! String),
            
        ]
        
        self.arrMyDynamicArray.insert(myDictionary, at: btn.tag)*/
        
        
        self.addToCartWB(strFoodId: String(myString), strQuantity: "1")
        
    }
    
    @objc func valueChanged1(_ sender: ValueStepper) {
        
        let step:ValueStepper = sender
       
        let item = arrListOfAllMyOrders[step.tag] as? [String:Any]
         print(item as Any)
        
        let someValue: Float = Float(step.value) // remove decimal value from double or float
        let double = someValue.cleanValue
        let doubleString = String(double) // convert double or float into string
        
        // convert menu id to string
        let x : Int = item!["menuId"] as! Int
        let myString = String(x)
        
        print(doubleString as Any)
        print(String(myString))
        
        if doubleString == "0" {
            
            self.clearThatFoodItemCartToZero(strFoodId: String(myString), strQuantity: doubleString)
            
        } else {
            
             // self.addToCartWB(strFoodId: String(myString), strQuantity: doubleString)
            
            // print(self.arrMyDynamicArray[])
             
        }
        
        
        
        /*let step:ValueStepper = sender
       
        let item = arrMyDynamicArray[step.tag] as? [String:Any]
         print(item as Any)
        
        let someValue: Float = Float(step.value) // remove decimal value from double or float
        let double = someValue.cleanValue
        let doubleString = String(double) // convert double or float into string
        
        print(doubleString as Any)
        // print(String(item!["menuId"] as! String))
        
        if doubleString == "0" {
            
            self.clearThatFoodItemCartToZero(strFoodId: (item!["menuId"] as! String), strQuantity: doubleString)
            
        } else {
            
             // self.addToCartWB(strFoodId: String(myString), strQuantity: doubleString)
            
            // print(item as Any)
            
            self.arrMyDynamicArray.removeObject(at: step.tag)
            
            print(arrMyDynamicArray as Any)
            
            // let item = arrMyDynamicArray[step.tag] as? [String:Any]
            
            let myDictionary: [String:String] = [
                
                "categoryId"    : (item!["categoryId"] as! String),
                "categoryName"  : (item!["categoryName"] as! String),
                "created"       : (item!["created"] as! String),
                "description"   : (item!["description"] as! String),
                "foodName"      : (item!["foodName"] as! String),
                "foodTag"       : (item!["foodTag"] as! String),
                "image_1"       : (item!["image_1"] as! String),
                "image_2"       : (item!["image_2"] as! String),
                "image_3"       : (item!["image_3"] as! String),
                "image_4"       : (item!["image_4"] as! String),
                "image_5"       : (item!["image_5"] as! String),
                "menuId"        : (item!["menuId"] as! String),
                "price"         : (item!["price"] as! String),
                "quantity"      : String(doubleString),
                "resturentId"   : (item!["resturentId"] as! String),
                "resturentName" : (item!["resturentName"] as! String),
                "specialPrice"  : (item!["specialPrice"] as! String),
                
            ]
            
            /*var res = [[String: String]]()
            res.append(myDictionary)*/
            
            self.arrMyDynamicArray.insert(myDictionary, at: step.tag)
            print(arrMyDynamicArray as Any)
            // self.arrMyDynamicArray.add(self.arrMyDynamicArray as Any)
            
            // self.clViewCategories.reloadData()
            self.addToCartWB(strFoodId: (item!["menuId"] as! String), strQuantity: String(doubleString))
            
            
        }*/
        
        
        
    }
    
    
    
    
    
    
    
    //Write DataSource Code Here
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == clViewCategories {
            
            let item = arrListOfAllMyOrdersCategories[indexPath.row] as? [String:Any]
            print(item as Any)
            
            let x : Int = item!["id"] as! Int
            let myString = String(x)
            
             self.foodListingCallAfterAddCart(strCategoryId: String(myString))
            
        }
        
    }
}

extension UPRestaurantProfile: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // if collectionView == clView {
            
            var sizes: CGSize
               
               let result = UIScreen.main.bounds.size
                   
                NSLog("%f",result.height)
            
               if result.height == 480 {
                   //Load 3.5 inch xib
                   sizes = CGSize(width: 170.0, height: 190.0)
               }
               else if result.height == 568 {
                   //Load 4 inch xib
                   sizes = CGSize(width: 100.0, height: 80.0)
               }
               else if result.height == 667.000000 {
                   //Load 4.7 inch xib , 8
                   sizes = CGSize(width: 180.0, height: 190.0) // done
               }
               else if result.height == 736.000000 {
                   // iphone 6s Plus and 7 Plus
                   sizes = CGSize(width: 180.0, height: 190.0) // done
               }
               else if result.height == 812.000000 {
                   // iphone X , 11 pro
                   sizes = CGSize(width: 160.0, height: 190.0) // done
               }
               else if result.height == 896.000000 {
                   // iphone Xr ,11, 11 pro max
                   sizes = CGSize(width: 180.0, height: 190.0) // done
               }
               else {
                   sizes = CGSize(width: 180.0, height: 190.0)
               }
            
               return sizes
            
        // }
        
        // return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        let result = UIScreen.main.bounds.size
        if result.height == 667 {
            return 5
        } else {
            return 10
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        let result = UIScreen.main.bounds.size
        if result.height == 667 {
            return 5
        } else {
            return 10
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let result = UIScreen.main.bounds.size
               
           // NSLog("%f",result.height)
        
           if result.height == 667 {
            return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
           } else {
            return UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        }
        
    }
}


extension Float {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
