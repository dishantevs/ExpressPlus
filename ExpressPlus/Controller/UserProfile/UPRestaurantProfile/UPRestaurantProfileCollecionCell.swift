//
//  UPRestaurantProfileCollecionCell.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import ValueStepper

class UPRestaurantProfileCollecionCell: UICollectionViewCell {
    
    @IBOutlet weak var stepper1: ValueStepper! {
        didSet {
            stepper1.enableManualEditing = true
            stepper1.isHidden = true
            stepper1.disabledIconButtonColor = .systemGreen
            stepper1.tintColor = .systemGreen
        }
    }
    
    @IBOutlet weak var imgFoodProfile:UIImageView! {
        didSet {
            imgFoodProfile.backgroundColor = .clear
        }
    }
    @IBOutlet weak var imgVegNonveg:UIImageView!  {
           didSet {
               imgVegNonveg.backgroundColor = .clear
           }
       }
    
    @IBOutlet weak var lblFoodName:UILabel!
    @IBOutlet weak var lblOldPrice:UILabel!
    @IBOutlet weak var lblRealPrice:UILabel!
    
    @IBOutlet weak var btnAdd:UIButton! {
        didSet {
            btnAdd.setTitleColor(.white, for: .normal)
            btnAdd.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnAdd.layer.cornerRadius = 6
            btnAdd.clipsToBounds = true
        }
    }
    
}
