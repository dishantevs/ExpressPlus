//
//  ScheduleDateAndTime.swift
//  ExpressPlus
//
//  Created by Apple on 26/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import JKCalendar
import Alamofire
import SwiftyJSON


// MARK:- LOCATION -
import CoreLocation

class ScheduleDateAndTime: UIViewController, CLLocationManagerDelegate {

    let locationManager = CLLocationManager()
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    
    let timePicker = UIDatePicker()
    
    var dictGetFinal:NSDictionary!
    
    var staticSubTotalPrice:String!
    var myTipAmount:String = "0"
    var strTotalPrice:String!
    
    var startDate:String = ""
    var endDate:String! = ""
    
    var otherSaved:String!
    
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    
    @IBOutlet weak var btnTransactionCalculationFeeMethod:UIButton!
    
    @IBOutlet weak var datePicker: UIDatePicker! {
        didSet {
            datePicker.backgroundColor = .black
            datePicker.setValue(UIColor.white, forKeyPath: "textColor")
            datePicker.isHidden = true
            datePicker.datePickerMode = UIDatePicker.Mode.time
            datePicker.minuteInterval = 55
        }
    }
    
    @IBOutlet weak var lblSelectTimeHourly: UILabel! {
        didSet {
            lblSelectTimeHourly.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            lblSelectTimeHourly.textColor = .white
        }
    }
    
    var transactionFeeValueIs:String!
    
    @IBOutlet weak var dateLabel2: UILabel!
    @IBOutlet weak var btnDoneTimePicker:UIButton!
    
    @IBOutlet weak var txtAddress:UITextField!
    
    var theTotalValueSendToServerIs:String!
    
    var dateString:String = ""
    
    let SELECT_COLOR = UIColor.init(red: 116.0/255.0, green: 233.0/255.0, blue: 139.0/255.0, alpha: 1)
    let UN_SELECT_COLOR = UIColor.init(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1)
    
    let markColor = UIColor(red: 40/255, green: 178/255, blue: 253/255, alpha: 1)
        var selectDays: [JKDay]?{
            didSet{
                if let days = selectDays,
                    days.count > 0{
                    let formatter = DateFormatter()
                    formatter.dateStyle = .medium
                    formatter.timeStyle = .none
                    
                    let firstDay = days.first!
                    let firstDate = JKCalendar.calendar.date(from: DateComponents(
                        
                    year: firstDay.year,
                    month: firstDay.month,
                    day: firstDay.day))!
                    
                        let lastDay = days.last!
                    _ = JKCalendar.calendar.date(from: DateComponents(year: lastDay.year,
                                                                                     month: lastDay.month,
                                                                                     day: lastDay.day))!
                    // dateLabel.text = formatter.string(from: firstDate)
                    dateString = formatter.string(from: firstDate)
                    
                    // self.getTimeSlot()
                    //}
                }
            }
        }
    
    @IBOutlet weak var calendar: JKCalendar! {
        didSet {
            // calendar.delegate = self
            // calendar.dataSource = self
        }
    }
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblTotalPayable:UILabel!
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "SCHEDULE DATE AND TIME"
        }
    }
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnStartDate:UIButton! {
        didSet {
            btnStartDate.layer.cornerRadius = 6
            btnStartDate.clipsToBounds = true
            btnStartDate.setTitle("Start Time", for: .normal)
        }
    }
    
    @IBOutlet weak var btnEndDate:UIButton! {
        didSet {
            btnEndDate.layer.cornerRadius = 6
            btnEndDate.clipsToBounds = true
            btnEndDate.setTitle("End Time", for: .normal)
        }
    }
    
    @IBOutlet weak var btnFive:UIButton! {
        didSet {
            btnFive.layer.borderColor = UIColor.black.cgColor
            btnFive.layer.borderWidth = 0.8
            btnFive.setTitle("$5", for: .normal)
        }
    }
    @IBOutlet weak var btnTen:UIButton! {
        didSet {
            btnTen.layer.borderColor = UIColor.black.cgColor
            btnTen.layer.borderWidth = 0.8
            btnTen.setTitle("$10", for: .normal)
        }
    }
    @IBOutlet weak var btnFifteen:UIButton! {
        didSet {
            btnFifteen.layer.borderColor = UIColor.black.cgColor
            btnFifteen.layer.borderWidth = 0.8
            btnFifteen.setTitle("$15", for: .normal)
        }
    }
    @IBOutlet weak var btnTwenty:UIButton! {
        didSet {
            btnTwenty.layer.borderColor = UIColor.black.cgColor
            btnTwenty.layer.borderWidth = 0.8
            btnTwenty.setTitle("$20", for: .normal)
        }
    }
    @IBOutlet weak var btnOther:UIButton! {
        didSet {
            btnOther.layer.borderColor = UIColor.black.cgColor
            btnOther.layer.borderWidth = 0.8
            btnOther.setTitle(" Other ", for: .normal)
        }
    }
    
    @IBOutlet weak var btnPaymentDone:UIButton! {
        didSet {
            btnPaymentDone.backgroundColor = .systemGreen
            btnPaymentDone.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var txtVieww:UITextView! {
        didSet {
            txtVieww.layer.cornerRadius = 6
            txtVieww.clipsToBounds = true
            txtVieww.layer.borderWidth = 0.8
            txtVieww.layer.borderColor = UIColor.systemGray2.cgColor
            txtVieww.text = ""
        }
    }
    
    @IBOutlet weak var btnUpdateLocation:UIButton!
    
    var yesUserMyCurrentLocation:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.btnDoneTimePicker.isHidden = true
        
        self.yesUserMyCurrentLocation = "0"
        
        // calendar.delegate = self
        // calendar.dataSource = self
        
        // calendar.textColor = UIColor(white: 0.25, alpha: 1)
        // calendar.backgroundColor = UIColor.white
        
        // calendar.isNearbyMonthNameDisplayed = false
        // calendar.isScrollEnabled = false
        
        // DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            // your code here
            
        self.calendar.delegate = self
        self.calendar.dataSource = self
            
        self.calendar.textColor = UIColor(white: 0.25, alpha: 1)
        self.calendar.backgroundColor = UIColor.white
            
        self.calendar.isNearbyMonthNameDisplayed = false
        self.calendar.isScrollEnabled = false
            
        // }
        
        self.btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        self.btnDoneTimePicker.addTarget(self, action: #selector(doneTimeOne), for: .touchUpInside)
        
        // self.txtother.isUserInteractionEnabled = false
        self.btnFive.backgroundColor    = UN_SELECT_COLOR
        self.btnTen.backgroundColor     = UN_SELECT_COLOR
        self.btnFifteen.backgroundColor = UN_SELECT_COLOR
        self.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        self.btnOther.backgroundColor   = UN_SELECT_COLOR
        
        self.btnFive.addTarget(self, action: #selector(addFiveClick), for: .touchUpInside)
        self.btnTen.addTarget(self, action: #selector(addTenClick), for: .touchUpInside)
        self.btnFifteen.addTarget(self, action: #selector(addFifteenClick), for: .touchUpInside)
        self.btnTwenty.addTarget(self, action: #selector(addTwentyClick), for: .touchUpInside)
        self.btnOther.addTarget(self, action: #selector(addOtherClick), for: .touchUpInside)
        
        self.btnUpdateLocation.addTarget(self, action: #selector(updateCurrentLocation), for: .touchUpInside)
        
        self.btnStartDate.addTarget(self, action: #selector(startDateClickMethod), for: .touchUpInside)
        self.btnEndDate.addTarget(self, action: #selector(endDateClickMethod), for: .touchUpInside)
        
        // MARK:- VIEW UP WHEN CLICK ON TEXT FIELD -
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // self.btnOther.addTarget(self, action: #selector(addOtherClick), for: .touchUpInside)
        
        // print(dictGetFinal as Any)
        
        self.btnPaymentDone.addTarget(self, action: #selector(paymentDoneClickMethod), for: .touchUpInside)
        
        let x : Int = dictGetFinal["price"] as! Int
        let myString = String(x)
        self.lblSelectTimeHourly.text = "Select Time Hourly Fee - $"+String(myString)
        self.lblTotalPayable.text = "Total Payable Amount : $"+String(myString)
        self.strTotalPrice = String(myString)
        
        // print(self.strTotalPrice as Any)
        
        self.staticSubTotalPrice = String(myString)
        self.iAmHereForLocationPermission()
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(ScheduleDateAndTime.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        self.btnTransactionCalculationFeeMethod.addTarget(self, action: #selector(transactionFeeC), for: .touchUpInside)
        
        self.lblTransactionFee(strTotalAmount: String(myString))
    }
    
    @objc func transactionFeeC() {
        
        let alert = UIAlertController(title: String("Trasanction Fee"), message: "2.9% + 0.30 cents", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
            
         
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    // MARK:- KEYBOARD WILL SHOW -
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    // MARK:- KEYBOARD WILL HIDE -
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        
        // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
                ERProgressHud.sharedInstance.hide()
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    
    // MARK:- GET CUSTOMER LOCATION -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            // print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            // print("locality ==> "+locality as Any) // sector 10 dwarka
            
            // print(self.strSaveCountryName as Any) // india
            // print(self.strSaveStateName as Any) // new delhi
            // print(self.strSaveZipcodeName as Any) // 110075
            
            let addressIs:String = String(self.strSaveLocality)+" "+String(self.self.strSaveLocalAddress)
            
            // self.txtAddress.text = localAddressMini
            
            if self.yesUserMyCurrentLocation == "1" {
                
                self.txtAddress.text    = addressIs
                
            } else {
                
                if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                    // print(person as Any)
                    
                    self.strSaveLatitude = (person["latitude"] as! String) //String(doubleStringLat)
                    self.strSaveLongitude = (person["longitude"] as! String) // String(doubleStringLong)
                    
                    self.txtAddress.text = (person["address"] as! String)
                }
                
            }
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            ERProgressHud.sharedInstance.hide()
            
            
            
        }
    }
    
    @objc func updateCurrentLocation() {
        
        let addressIs = self.strSaveLocality+", "+self.strSaveLocalAddress+", "+self.strSaveLocalAddressMini
        
        
        let alert = UIAlertController(title: String("Current Location"), message: String(addressIs), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Use Current Location", style: .default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
             
            self.yesUserMyCurrentLocation = "1"
            self.txtAddress.text    = addressIs
            
            self.iAmHereForLocationPermission()
                
        }))
        
        alert.addAction(UIAlertAction(title: "Use Existing address", style: UIAlertAction.Style.default, handler: { action in
            // self.dismiss(animated: true, completion: nil)
         
            self.yesUserMyCurrentLocation = "0"
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                // print(person as Any)
                /*
                 ["AutoInsurance": , "latitude": 28.5871234, "foodTag": , "address": Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India, "firebaseId": , "logitude": 77.0605133, "companyBackground": , "contactNumber": 9798969785, "cardholderId": , "AccountHolderName": , "dob": , "ssnImage": , "state": , "middleName": , "accountType": , "lastName": , "gender": , "socialType": , "image": , "RoutingNo": , "country": , "zipCode": , "email": iosu@gmail.com, "date_5": , "branchName": , "fullName": iosu, "role": Member, "wallet": 0, "userId": 397, "device": Android, "socialId": , "BankName": , "deviceToken": du9nH2Bys9A:APA91bH-niveAFURAIX2KMux4ovB3fTevmHTJSiBqtwH9vanbswk75DHn0hYycTXL51L23ei4OfP-mxNIYIn9mTJdJuwEQwM5JICnJeudJLML5iKgHOwfoCps5vKCGiFRdUmX4aYkDy6, "longitude": 77.0605133, "AccountNo": , "drivlingImage": ]
                 */
                
                //  let doubleLat = (person["latitude"] as! Double)
                // let doubleStringLat = String(doubleLat)
                
                // let doubleLong = (person["longitude"] as! Double)
                // let doubleStringLong = String(doubleLong)
                
                self.strSaveLatitude = (person["latitude"] as! String) //String(doubleStringLat)
                self.strSaveLongitude = (person["longitude"] as! String) // String(doubleStringLong)
                
                self.txtAddress.text = (person["address"] as! String)
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
            // self.dismiss(animated: true, completion: nil)
         
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func startDateClickMethod() {
        self.datePicker.isHidden = false
        self.btnDoneTimePicker.isHidden = false
        self.btnDoneTimePicker.tag = 0
    }
    
    @objc func endDateClickMethod() {
        
        if self.btnStartDate.titleLabel?.text == nil {
            
            let alert = UIAlertController(title: String("Error!"), message: String("Please select start time."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if self.btnStartDate.titleLabel?.text == "Start Time" {
            
            let alert = UIAlertController(title: String("Error!"), message: String("Please select start time."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if self.btnStartDate.titleLabel?.text == "" {
            
            let alert = UIAlertController(title: String("Error!"), message: String("Please select start time."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            self.datePicker.isHidden = false
            self.btnDoneTimePicker.isHidden = false
            self.btnDoneTimePicker.tag = 1
            
             
        }
        
    }
    
    @objc func calculateEndTimeShouldBeGreater() {
    
        let s1 = self.btnStartDate.titleLabel?.text
        let s2 = self.btnEndDate.titleLabel?.text
        
        // print(self.btnStartDate.titleLabel?.text)
        // print(self.btnEndDate.titleLabel?.text)
        
        let f = DateFormatter()
        f.dateFormat = "hh:mma"

        // f.date(from: s1)! //"Jan 1, 2000 at 10:31 PM"
        // f.date(from: s2)! //"Jan 1, 2000 at 2:31 PM"
        // f.date(from: s1)! > f.date(from: s2)!  // true
        
        print(f.date(from: s1!)!)
        print(f.date(from: s2!)!)
        print(f.date(from: s1!)! > f.date(from: s2!)!)
    }
    
    @objc func doneTimeOne() {
        
        // self.calculateFinalTotalAfterTip(strTipAmount: "0")
        
        if self.btnDoneTimePicker.tag == 0 {
            let dateFormatter = DateFormatter()
            // dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.short

            let strDate = dateFormatter.string(from: datePicker.date)
            
            self.btnStartDate.setTitle(strDate, for: .normal)
            
            self.startDate = strDate
        } else if self.btnDoneTimePicker.tag == 1 {
            let dateFormatter = DateFormatter()
            // dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.short

            let strDate = dateFormatter.string(from: datePicker.date)
            
            self.btnEndDate.setTitle(strDate, for: .normal)
            
            self.endDate = strDate
            
            if self.btnStartDate.titleLabel?.text == nil {
                
            } else if self.btnStartDate.titleLabel?.text == "Start Time" {
                
            } else if self.btnStartDate.titleLabel?.text == "" {
                
            } else {
                
                
                
                
                
                let s1 = self.btnStartDate.titleLabel?.text
                let s2 = strDate
                
                 
                let f = DateFormatter()
                f.dateFormat = "hh:mma"

                let date11 = f.date(from: s1!)!
                let date22 = f.date(from: s2)!
                
                print(date11 as Any)
                print(date22 as Any)
                
                // f.date(from: s1)! //"Jan 1, 2000 at 10:31 PM"
                // f.date(from: s2)! //"Jan 1, 2000 at 2:31 PM"
                // f.date(from: s1)! > f.date(from: s2)!  // true
                
                // print(f.date(from: s1!)!)
                // print(f.date(from: s2)!)
                // print(f.date(from: s1!)! > f.date(from: s2)!)
                
                if date11 < date22 {
                    print("i am big")
                    
                    // MARK:- TIME DIFFERENCE BETWEEN TIME -
                     let time1 = String((self.btnStartDate.titleLabel?.text)!)
                     let time2 = String(strDate)

                     let formatter = DateFormatter()
                     formatter.dateFormat = "hh:mma"

                     let date1 = formatter.date(from: time1)!
                     let date2 = formatter.date(from: time2)!

                     let elapsedTime = date2.timeIntervalSince(date1)

                     // convert from seconds to hours, rounding down to the nearest hour
                     let hours = floor(elapsedTime / 60 / 60)

                     // we have to subtract the number of seconds in hours from minutes to get
                     // the remaining minutes, rounding down to the nearest minute (in case you
                     // want to get seconds down the road)
                     let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)

                     // print("\(Int(hours)) hr and \(Int(minutes)) min")
                     print("\(Int(hours)) hr \(Int(minutes)) min")
                     print(type(of: "\(Int(hours)).\(Int(minutes))"))
                    
                    
                    if ("\(Int(hours)) hr \(Int(minutes)) min") == "1 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "2 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "3 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "4 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "5 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "6 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "7 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "8 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "9 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "10 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "11 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "12 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "13 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "14 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "15 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "16 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "17 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "18 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "19 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "20 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "21 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "22 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "23 hr 0 min" ||
                        ("\(Int(hours)) hr \(Int(minutes)) min") == "24 hr 0 min"
                    
                    
                    {
                        print("yes yes yes yes yes")
                        
                        let str : NSString = "\(Int(hours)).\(Int(minutes))" as NSString
                        let xa : Float = str.floatValue
                        let xa1 : Double = str.doubleValue
                         
                        print(xa as Any)
                        print(xa1 as Any)
                         
                         
                        let one = self.dictGetFinal["price"] as! Int
                        let two = xa1
                         
                          // print(one as Any)
                          // print(two as Any)
                         
                        let three = Double(one) * two
                        
                         
                         // add tip amount on total price
                        let addTipAmount = self.myTipAmount
                        let myString = addTipAmount
                        let myFloat = (myString as NSString).doubleValue
                         
                        let finalPricepaymentIs = three + myFloat
                         // print(finalPricepaymentIs as Any)
                         
                         
                         
                         // convert double into string
                        let c: String = String(format: "%.1f", finalPricepaymentIs)

                        print("c: \(c)") // c: 1.5
                        self.lblTotalPayable.text = "Total Payable Amount: $ "+"\(c)"
                        
                        self.lblTransactionFee(strTotalAmount: "\(c)")
                        
                    } else {
                        // you can only select time on hourly basis
                        
                        let alert = UIAlertController(title: String("Alert!"), message: String("You can only select time on hourly basis."), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            self.btnEndDate.setTitle("End Time", for: .normal)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                } else {
                    print("i am small")
                    
                    let alert = UIAlertController(title: String("Error!"), message: String("End Time should be greater then Start Time."), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                        self.btnEndDate.setTitle("End Time", for: .normal)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
                
                
                
                
                
                
                
                
                
                           
                // self.calculateEndTimeShouldBeGreater()
                
                
                
            }
           
            
            
        }
        
        self.btnDoneTimePicker.isHidden = true
        self.datePicker.isHidden = true
    }
    
    @objc func onClickDoneButton() {
        self.view.endEditing(true)
    }
    
    func openTimePicker()  {
        timePicker.datePickerMode = UIDatePicker.Mode.time
        timePicker.frame = CGRect(x: 0.0, y: (self.view.frame.height/2 + 60), width: self.view.frame.width, height: 150.0)
        timePicker.backgroundColor = UIColor.white
        self.view.addSubview(timePicker)
        timePicker.addTarget(self, action: #selector(ScheduleDateAndTime.startTimeDiveChanged), for: UIControl.Event.valueChanged)
    }

    @objc func startTimeDiveChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        print(formatter.string(from: sender.date))
        // youtTextFieldName.text = formatter.string(from: sender.date)
        timePicker.removeFromSuperview() // if you want to remove time picker
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func addFiveClick() {
        
        if self.btnStartDate.titleLabel?.text == nil {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnStartDate.titleLabel?.text == "Start Time" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnStartDate.titleLabel?.text == "" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnEndDate.titleLabel?.text == nil {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnEndDate.titleLabel?.text == "End Time" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnEndDate.titleLabel?.text == "" {
            self.pleaseSelectTimeFirstAlertPopup()
            
        } else {
            
            // self.txtother.isUserInteractionEnabled = false
            self.btnFive.backgroundColor    = SELECT_COLOR
            self.btnTen.backgroundColor     = UN_SELECT_COLOR
            self.btnFifteen.backgroundColor = UN_SELECT_COLOR
            self.btnTwenty.backgroundColor  = UN_SELECT_COLOR
            self.btnOther.backgroundColor   = UN_SELECT_COLOR
            self.btnOther.setTitle(String("Other"), for: .normal)
            // self.btnDoneOther.isHidden = true
            
             self.calculateFinalTotalAfterTip(strTipAmount: "5")
            
        }
        
    }
    
    @objc func addTenClick() {
        
        if self.btnStartDate.titleLabel?.text == nil {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnStartDate.titleLabel?.text == "Start Time" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnStartDate.titleLabel?.text == "" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnEndDate.titleLabel?.text == nil {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnEndDate.titleLabel?.text == "End Time" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnEndDate.titleLabel?.text == "" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else {
            
        // self.txtother.isUserInteractionEnabled  = false
        self.btnFive.backgroundColor    = UN_SELECT_COLOR
        self.btnTen.backgroundColor     = SELECT_COLOR
        self.btnFifteen.backgroundColor = UN_SELECT_COLOR
        self.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        self.btnOther.backgroundColor   = UN_SELECT_COLOR
        self.btnOther.setTitle(String("Other"), for: .normal)
        // self.btnDoneOther.isHidden = true
        
         self.calculateFinalTotalAfterTip(strTipAmount: "10")
            
        }
    }
    @objc func addFifteenClick() {
        
        if self.btnStartDate.titleLabel?.text == nil {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnStartDate.titleLabel?.text == "Start Time" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnStartDate.titleLabel?.text == "" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnEndDate.titleLabel?.text == nil {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnEndDate.titleLabel?.text == "End Time" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnEndDate.titleLabel?.text == "" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else {
            
        // self.txtother.isUserInteractionEnabled = false
        self.btnFive.backgroundColor    = UN_SELECT_COLOR
        self.btnTen.backgroundColor     = UN_SELECT_COLOR
        self.btnFifteen.backgroundColor = SELECT_COLOR
        self.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        self.btnOther.backgroundColor   = UN_SELECT_COLOR
        self.btnOther.setTitle(String("Other"), for: .normal)
        // self.btnDoneOther.isHidden = true
        
         self.calculateFinalTotalAfterTip(strTipAmount: "15")
            
        }
    }
    
    @objc func addTwentyClick() {
        
        if self.btnStartDate.titleLabel?.text == nil {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnStartDate.titleLabel?.text == "Start Time" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnStartDate.titleLabel?.text == "" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnEndDate.titleLabel?.text == nil {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnEndDate.titleLabel?.text == "End Time" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else if self.btnEndDate.titleLabel?.text == "" {
            self.pleaseSelectTimeFirstAlertPopup()
        } else {
            
        // self.txtother.isUserInteractionEnabled = false
        self.btnFive.backgroundColor    = UN_SELECT_COLOR
        self.btnTen.backgroundColor     = UN_SELECT_COLOR
        self.btnFifteen.backgroundColor = UN_SELECT_COLOR
        self.btnTwenty.backgroundColor  = SELECT_COLOR
        self.btnOther.backgroundColor   = UN_SELECT_COLOR
        self.btnOther.setTitle(String("Other"), for: .normal)
        // self.btnDoneOther.isHidden = true
        
         self.calculateFinalTotalAfterTip(strTipAmount: "20")
            
        }
    }
    
    @objc func calculateFinalTotalAfterTip(strTipAmount:String) {
        
        
        
        self.myTipAmount = strTipAmount
        let a = (staticSubTotalPrice as NSString).floatValue
        let b = (strTipAmount as NSString).floatValue
        let sum = a + b
        let x : Int = Int(sum)
        strTotalPrice = String(x)
        self.lblTotalPayable.text = "Total Payable Amount: $ "+String(strTotalPrice)
        
        print(strTotalPrice as Any)
        
        self.calculatingTotalAmountWhenClickOnTip(strTipAmountIsSet: strTipAmount)
        
    }
    
    
    
    @objc func pleaseSelectTimeFirstAlertPopup() {
        
        let alert = UIAlertController(title: String("Error!"), message: String("Please select Time - Slot first."), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
             
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func calculatingTotalAmountWhenClickOnTip(strTipAmountIsSet:String) {
    
        let time1 = String((self.btnStartDate.titleLabel?.text)!)
        let time2 = String((self.btnEndDate.titleLabel?.text)!)

         let formatter = DateFormatter()
         formatter.dateFormat = "hh:mma"

         let date1 = formatter.date(from: time1)!
         let date2 = formatter.date(from: time2)!

         let elapsedTime = date2.timeIntervalSince(date1)

         // convert from seconds to hours, rounding down to the nearest hour
         let hours = floor(elapsedTime / 60 / 60)

         // we have to subtract the number of seconds in hours from minutes to get
         // the remaining minutes, rounding down to the nearest minute (in case you
         // want to get seconds down the road)
         let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)

         // print("\(Int(hours)) hr and \(Int(minutes)) min")
         print("\(Int(hours)) hr \(Int(minutes)) min")
         print(type(of: "\(Int(hours)).\(Int(minutes))"))
         
         let str : NSString = "\(Int(hours)).\(Int(minutes))" as NSString
         let xa : Float = str.floatValue
        let xa1 : Double = str.doubleValue
         
        print(xa as Any)
        print(xa1 as Any)
        
         
        let one = self.dictGetFinal["price"] as! Int
        let two = xa1
         
          // print(one as Any)
          // print(two as Any)
         
        let three = Double(one) * two
        
         
         // add tip amount on total price
        let addTipAmount = self.myTipAmount
        let myString = addTipAmount
        let myFloat = (myString as NSString).doubleValue
         
        let finalPricepaymentIs = three + myFloat
        // print(finalPricepaymentIs as Any)
         
         // let myString1 = strTipAmountIsSet
         // let myInt1 = Double(myString1)
         
        // let iAmTheFinalpriceAfterSelectingTip = finalPricepaymentIs + myInt1!
        // print(iAmTheFinalpriceAfterSelectingTip as Any)
        
         // convert double into string
        let c: String = String(format: "%.1f", finalPricepaymentIs)

        print("c: \(c)") // c: 1.5
        
        self.theTotalValueSendToServerIs = "\(c)"
        
        self.lblTotalPayable.text = "Total Payable Amount: $ "+"\(c)"
        
         self.lblTransactionFee(strTotalAmount: "\(c)")
    }
    
    // MARK:- TRANSACTION FEE -
    @objc func lblTransactionFee(strTotalAmount:String) {
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! UPTipForDeliveryGuyTwoTableCell
        
        
        
        print(strTotalAmount as Any)
        
        let convertTotalPriceToDouble = Double(strTotalAmount)!
        
        //* calculate total price
        //* formula = totalPrice *5.8%
        
        //* 0.058
        //* why 0.058 bcause it get value from 5.8%
        //* 2.9% = 0.058
        let multiplyTractactionFeeWithTotalAmount = convertTotalPriceToDouble * 0.029
        // print(multiplyTractactionFeeWithTotalAmount as Any)
        
        //* convert multiple double values into two digits
        // print(String(format:"%.02f", multiplyTractactionFeeWithTotalAmount))
        
        let getValueAfterMultiplyByFivePointEightPercentage = String(format:"%.02f", multiplyTractactionFeeWithTotalAmount)
        // print(getValueAfterMultiplyByFivePointEightPercentage as Any)
        
        //* now add .30 with result
        let cents = 0.30
        
        let addCentsWithMultiplicationResult = Double(getValueAfterMultiplyByFivePointEightPercentage)!+cents
        // print(addCentsWithMultiplicationResult as Any)
        
        // round figure of result
        let roundFigureOfTotalCalulation = String(format:"%.02f", addCentsWithMultiplicationResult)
        // print(roundFigureOfTotalCalulation as Any)
        
        //* final price for Transaction Fee
        let transactionFeeIs = roundFigureOfTotalCalulation
        
        // cell.lblTransactionFee.text = "$ "+String(transactionFeeIs)
        
        self.transactionFeeValueIs = roundFigureOfTotalCalulation
        
        //* add transaction fee with total amount
        let totalAmountAfterGetTransactionFee = Double(strTotalAmount)!+Double(transactionFeeIs)!
        self.lblTotalPayable.text = "Transaction Fee : $ "+roundFigureOfTotalCalulation+"\nTotal Amount to Pay : $ "+String(totalAmountAfterGetTransactionFee)
    }
    
    @objc func addOtherClick() {
        self.btnFive.backgroundColor    = UN_SELECT_COLOR
        self.btnTen.backgroundColor     = UN_SELECT_COLOR
        self.btnFifteen.backgroundColor = UN_SELECT_COLOR
        self.btnTwenty.backgroundColor  = UN_SELECT_COLOR
        self.btnOther.backgroundColor   = SELECT_COLOR
        
        let alertController = UIAlertController(title: "Tip Amount", message: "Please enter tip amount", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "tip"
        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            // let secondTextField = alertController.textFields![1] as UITextField
            
            /*
            // Get the String from UserDefaults
            if let myString = defaults.string(forKey: "savedString") {
                print("defaults savedString: \(myString)")
                
            }
            */
            
            self.btnOther.setTitle("$"+String(firstTextField.text!), for: .normal)
            
            
            self.myTipAmount = String(firstTextField.text!)
            let a = (self.staticSubTotalPrice as NSString).floatValue
            let b = (firstTextField.text! as NSString).floatValue
            let sum = a + b
            let x : Int = Int(sum)
            self.strTotalPrice = String(x)
            self.lblTotalPayable.text = "Total Payable Amount: $ "+String(self.strTotalPrice)
            
            self.calculatingTotalAmountWhenClickOnTip(strTipAmountIsSet: firstTextField.text!)
            
            
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        

        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func paymentDoneClickMethod() {
        if String(dateString) == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Date should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if String(startDate) == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Start Time should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if String(endDate) == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("End Time should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        }
        /*
        else if String(txtVieww.text!) == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Special Notes should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        }*/
        else {
            
            
            if btnStartDate.titleLabel?.text == "Start Time"{
                
                let alert = UIAlertController(title: String("Error"), message: String("Please select Start Time."), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                     
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            } else if btnEndDate.titleLabel?.text == "End Time" {
                
                let alert = UIAlertController(title: String("Error"), message: String("Please select End Time."), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                     
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            } else {
                
                self.calculateBeforeWebserviceHit()
                
            }
            
            
            
            // self.finalConfirmAndPayment()
        }
    }
    
    
    @objc func calculateBeforeWebserviceHit() {
        
        // MARK:- TIME DIFFERENCE BETWEEN TIME -
        let time1 = String((self.btnStartDate.titleLabel?.text)!)
        let time2 = String((self.btnEndDate.titleLabel?.text)!)

        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mma"

        let date1 = formatter.date(from: time1)!
        let date2 = formatter.date(from: time2)!

        let elapsedTime = date2.timeIntervalSince(date1)

        // convert from seconds to hours, rounding down to the nearest hour
        let hours = floor(elapsedTime / 60 / 60)

        // we have to subtract the number of seconds in hours from minutes to get
        // the remaining minutes, rounding down to the nearest minute (in case you
        // want to get seconds down the road)
        let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)

        // print("\(Int(hours)) hr and \(Int(minutes)) min")
        print("\(Int(hours)) hr \(Int(minutes)) min")
        print(type(of: "\(Int(hours)).\(Int(minutes))"))
        
        let str : NSString = "\(Int(hours)).\(Int(minutes))" as NSString
        let xa : Float = str.floatValue
        let xa1 : Double = str.doubleValue
        
        print(xa as Any)
        print(xa1 as Any)
        
        
        let one = self.dictGetFinal["price"] as! Int
        let two = xa1
        
         // print(one as Any)
         // print(two as Any)
        
        let three = Double(one) * two
       
        
        // add tip amount on total price
        let addTipAmount = self.myTipAmount
        let myString = addTipAmount
        let myFloat = (myString as NSString).doubleValue
        
        let finalPricepaymentIs = three + myFloat
         print(finalPricepaymentIs as Any)
        
        
        
        // convert double into string
        let c: String = String(format: "%.1f", finalPricepaymentIs)

        print("c: \(c)") // c: 1.5
                                            
        
        
        // self.showPopUpBeforePayment(strPaymentFetchOnPopUp: "\(c)", totalTime: "\(Int(hours)) hr \(Int(minutes)) min")
        
        // self.lblTotalPayable.text = "Total Payable Amount: $ "+"\(c)"
        
        self.lblTransactionFee2(strTotalAmount: "\(c)", strTime: "\(Int(hours)) hr \(Int(minutes)) min")
    }
    
    
    // MARK:- TRANSACTION FEE -
    @objc func lblTransactionFee2(strTotalAmount:String,strTime:String) {
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! UPTipForDeliveryGuyTwoTableCell
        
        print(strTotalAmount as Any)
        
        let convertTotalPriceToDouble = Double(strTotalAmount)!
        
        //* calculate total price
        //* formula = totalPrice *5.8%
        
        //* 0.058
        //* why 0.058 bcause it get value from 5.8%
        //* 2.9% = 0.058
        let multiplyTractactionFeeWithTotalAmount = convertTotalPriceToDouble * 0.029
        // print(multiplyTractactionFeeWithTotalAmount as Any)
        
        //* convert multiple double values into two digits
        // print(String(format:"%.02f", multiplyTractactionFeeWithTotalAmount))
        
        let getValueAfterMultiplyByFivePointEightPercentage = String(format:"%.02f", multiplyTractactionFeeWithTotalAmount)
        // print(getValueAfterMultiplyByFivePointEightPercentage as Any)
        
        //* now add .30 with result
        let cents = 0.30
        
        let addCentsWithMultiplicationResult = Double(getValueAfterMultiplyByFivePointEightPercentage)!+cents
        // print(addCentsWithMultiplicationResult as Any)
        
        // round figure of result
        let roundFigureOfTotalCalulation = String(format:"%.02f", addCentsWithMultiplicationResult)
        // print(roundFigureOfTotalCalulation as Any)
        
        //* final price for Transaction Fee
        let transactionFeeIs = roundFigureOfTotalCalulation
        
        // cell.lblTransactionFee.text = "$ "+String(transactionFeeIs)
        
        self.transactionFeeValueIs = roundFigureOfTotalCalulation
        
        //* add transaction fee with total amount
        let totalAmountAfterGetTransactionFee = Double(strTotalAmount)!+Double(transactionFeeIs)!
         self.lblTotalPayable.text = "Transaction Fee : $ "+roundFigureOfTotalCalulation+"\nTotal Amount to Pay : $ "+String(totalAmountAfterGetTransactionFee)
        
        
        self.showPopUpBeforePayment(strPaymentFetchOnPopUp: String(totalAmountAfterGetTransactionFee), totalTime: strTime, strTransactionFee: roundFigureOfTotalCalulation)
        
    }
    
    
    
    
    @objc func showPopUpBeforePayment(strPaymentFetchOnPopUp:String,totalTime:String,strTransactionFee:String) {
        
        let alert = UIAlertController(title: String("Total Payment"), message: String("Details\n Total Service Time :"+totalTime+"\nTransaction Fee : $ "+strTransactionFee+"\nTotal Amount :"+strPaymentFetchOnPopUp), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Pay : $"+strPaymentFetchOnPopUp, style: UIAlertAction.Style.default, handler: { action in
             
            self.findMyStateTaxWB(strFinalAmountpayIs: strPaymentFetchOnPopUp)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.destructive, handler: { action in
             
            
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    // MARK:- WEBSERVICE ( FIND STATE ) -
    @objc func findMyStateTaxWB(strFinalAmountpayIs:String) {
          ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
          let urlString = BASE_URL_EXPRESS_PLUS
    
          var parameters:Dictionary<AnyHashable, Any>!
          // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
              
                     parameters = [
                         "action"       : "statefind",
                         "stateName"    : String(self.strSaveStateName)
              ]
          // }
                     print("parameters-------\(String(describing: parameters))")
                     
                     Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                         {
                             response in
                 
                             switch(response.result) {
                             case .success(_):
                                if let data = response.result.value {

                                 let JSON = data as! NSDictionary
                                  print(JSON as Any)
                                 
                                 var strSuccess : String!
                                 strSuccess = JSON["status"]as Any as? String
                                
                                 if strSuccess == String("success") {
                                  print("yes")
                                  
                                    
                                    if JSON["price"] is String {
                                                      
                                        print("Yes, it's a String")
                                      
                                        self.finalConfirmAndPayment(strSalesTaxIs: (JSON["price"] as! String), strFinalAmountpayIss: strFinalAmountpayIs)
                                        
                                    } else if JSON["price"] is Int {
                                      
                                        print("It is Integer")
                                      
                                        let x2 : Int = (JSON["price"] as! Int)
                                        let myString2 = String(x2)
                                        self.finalConfirmAndPayment(strSalesTaxIs: myString2, strFinalAmountpayIss: strFinalAmountpayIs)
                                      
                                    } else {
                                    //some other check
                                      print("i am ")
                                      
                                        let temp:NSNumber = JSON["price"] as! NSNumber
                                        let tempString = temp.stringValue
                                        self.finalConfirmAndPayment(strSalesTaxIs: tempString, strFinalAmountpayIss: strFinalAmountpayIs)
                                      
                                    }
                                    
                                    
                                    
                                 }
                                 else {
                                  print("no")
                                   ERProgressHud.sharedInstance.hide()
                                    
                                    
                                 }
                             }

                             case .failure(_):
                                 print("Error message:\(String(describing: response.result.error))")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 break
                              }
                         }
          }
    
    
    
    
    
    
    @objc func finalConfirmAndPayment(strSalesTaxIs:String,strFinalAmountpayIss:String) {
        
        
        
        self.view.endEditing(true)
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
        let urlString = BASE_URL_EXPRESS_PLUS
    
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
        /*
             [TIP] => 20.0
             [action] => addbooking
             [address] => Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India
             [bookingDate] => 11-06-2020
             [latitude] => 28.5870461
             [longitude] => 77.0605256
             [otherService] =>
             [salesTax] => 0
             [serceAmount] => 80
             [serviceId] => 7
             [slote] => 17:17-17:17
             [specialNote] => test sn
             [totalAmount] => 100.0
             [userId] => 80
             */
            
            let slots:String = String(startDate)+"-"+String(endDate)
            let x : Int = dictGetFinal["id"] as! Int
            let myString = String(x)
            
            let defaults = UserDefaults.standard
            if let myString = defaults.string(forKey: "keyServiceOther") {
                print("defaults savedString: \(myString)")
                self.otherSaved = String(myString)
            } else {
                self.otherSaved = String(myString)
            }
            
            let addressIs:String = String(self.strSaveLocality)+" "+String(self.self.strSaveLocalAddress)
            
            
            
            if strSalesTaxIs == "0" {
                parameters = [
                             "action"       : "addbooking",
                             "userId"       : person["userId"] as Any,
                             "TIP"          : String(self.myTipAmount),
                            "address"      : String(self.txtAddress.text!),
                             "bookingDate"  : String(dateString),
                             "latitude"     : String(self.strSaveLatitude),
                             "longitude"    : String(self.strSaveLongitude),
                             "otherService" : String(self.otherSaved),
                             "salesTax"     : String("0"),
                             "serviceAmount"  : String(staticSubTotalPrice),
                             "serviceId"    : String(myString),
                             "slote"        : String(slots),
                             "specialNote"  : String(txtVieww.text!),
                    "transactionFee":String(self.transactionFeeValueIs),
                    "totalAmount"  : String(self.theTotalValueSendToServerIs)
                          
                  ]
            } else {
                
                
                let salexTax = strSalesTaxIs
                
                let totalAmountIs = self.theTotalValueSendToServerIs
                
                print(totalAmountIs as Any)
                
                if salexTax.contains(".") {
                    print("decimal")
                    
                    let myString = salexTax
                    let myFloat = (myString as NSString).doubleValue
                    print(myFloat as Any)
                    
                    print((myFloat/100))
                    
                    // print()
                    
                    // total amount
                    let myString2 = totalAmountIs
                    let myFloat2 = (myString2! as NSString).doubleValue
                    print(myFloat2 as Any)
                    
                    print((myFloat*myFloat2)/100)
                    
                    
                    let a = (myFloat*myFloat2)/100
                    let b = Double(totalAmountIs!)
                    let c = a + b!
                    
                    print(c as Any)
                    
                    
                    parameters = [
                        "action"       : "addbooking",
                        "userId"       : person["userId"] as Any,
                        "TIP"          : String(self.myTipAmount),
                        "address"      : String(self.txtAddress.text!),
                        "bookingDate"  : String(dateString),
                        "latitude"     : String(self.strSaveLatitude),
                        "longitude"    : String(self.strSaveLongitude),
                        "otherService" : String(self.otherSaved),
                        "salesTax"     : String(salexTax),
                        "serviceAmount"  : String(staticSubTotalPrice),
                        "serviceId"    : String(myString),
                                 "slote"        : String(slots),
                        "transactionFee":String(self.transactionFeeValueIs),
                        "specialNote"  : String(txtVieww.text!),
                        "totalAmount"  : String(c)
                              
                      ]
                    
                    
                    
                } else {
                    print("no decimal")
                    
                    
                    let salexTax = strSalesTaxIs
                    
                    let totalAmountIs = self.theTotalValueSendToServerIs
                    
                    let myString = salexTax
                    let myFloat = (myString as NSString).doubleValue
                    print(myFloat as Any)
                    
                    print((myFloat/100))
                    
                    // print()
                    
                    // total amount
                    let myString2 = totalAmountIs
                    let myFloat2 = (myString2! as NSString).doubleValue
                    print(myFloat2 as Any)
                    
                    print((myFloat*myFloat2)/100)
                    
                    
                    let a = (myFloat*myFloat2)/100
                    let b = Double(totalAmountIs!)
                    let c = a + b!
                    
                    print(c as Any)
                    
                    
                    
                    parameters = [
                                 "action"       : "addbooking",
                                 "userId"       : person["userId"] as Any,
                                 "TIP"          : String(self.myTipAmount),
                                "address"      : String(self.txtAddress.text!),
                                 "bookingDate"  : String(dateString),
                                 "latitude"     : String(self.strSaveLatitude),
                                 "longitude"    : String(self.strSaveLongitude),
                                 "otherService" : String(self.otherSaved),
                                 "salesTax"     : String(salexTax),
                                 "serviceAmount"  : String(staticSubTotalPrice),
                                 "serviceId"    : String(myString),
                                 "slote"        : String(slots),
                                 "specialNote"  : String(txtVieww.text!),
                        "transactionFee":String(self.transactionFeeValueIs),
                                 "totalAmount"  : String(c)
                              
                      ]
                    
                }
            }
           }
        
        print("parameters-------\(String(describing: parameters))")
                     
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                         {
            response in
                 
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                 
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                                 
                    var strSuccessAlert : String!
                    strSuccessAlert = JSON["msg"]as Any as? String
                                 
                    if strSuccess == String("success") {
                        print("yes")
                                   
                        ERProgressHud.sharedInstance.hide()
                                   
                                   // var dict: Dictionary<AnyHashable, Any>
                                  //  dict = JSON["msg"] as! Dictionary<AnyHashable, Any>
                                  
                                  // var strSuccess : String!
                                  // strSuccess = JSON["msg"]as Any as? String
                           
                                    
                                    
                                    
                                    
                        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessAfterOrderFoodId") as? UPSuccessAfterOrderFood
                        self.navigationController?.pushViewController(push!, animated: true)
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    /*
                                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPPaymentScreenId") as? UPPaymentScreen
                                    
                                    /*
                                     var seviceTipAmount:String!
                                     var seviceGroupId:String!
                                     var serviceAmount:String!
                                     var seviceType:String!
                                     var serviceTotalAmount:String!
                                     */
                                    
                                     
                                    let x : Int = JSON["bookingGroupId"] as! Int
                                    let myString = String(x)
                                    
                                    // service id
                                    let x2 : Int = self.dictGetFinal["id"] as! Int
                                    let myString2 = String(x2)
                                    
                                    push!.seviceTipAmount = String(self.myTipAmount)
                                    push!.seviceGroupId = String(myString)
                                    push!.seviceType = String(myString2)
                                    push!.serviceAmount = String(self.staticSubTotalPrice)
                                    push!.getTotalPayment2 = String(strFinalAmountpayIs) // String(self.strTotalPrice)
                                    
                                    push!.self.strFromWhere = "serviceCustomerNew"
                                    
                                    self.navigationController?.pushViewController(push!, animated: true)
                                   */
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                                    
                        let alert = UIAlertController(title: String(strSuccess), message: String(strSuccessAlert), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                         
                        }))
                        self.present(alert, animated: true, completion: nil)
                                    
                    }
                }

                case .failure(_):
                    print("Error message:\(String(describing: response.result.error))")
                                 
                    ERProgressHud.sharedInstance.hide()
                                 
                    let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                 
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                         UIAlertAction in
                        NSLog("OK Pressed")
                    }
                                 
                    alertController.addAction(okAction)
                                 
                    self.present(alertController, animated: true, completion: nil)
                                 
                    break
            }
        }
    }
    
}

extension ScheduleDateAndTime: JKCalendarDelegate{
    
    func calendar(_ calendar: JKCalendar, didTouch day: JKDay) {
        selectDays = [day]
        calendar.reloadData()
    }
    
    func calendar(_ calendar: JKCalendar, didPan days: [JKDay]) {
        selectDays = days
        calendar.reloadData()
    }
}

extension ScheduleDateAndTime: JKCalendarDataSource {
    
    func calendar(_ calendar: JKCalendar, continuousMarksWith month: JKMonth) -> [JKCalendarContinuousMark]?{
        if let days = selectDays,
            let start = days.first,
            let end = days.last{
            return [JKCalendarContinuousMark(type: .circle, start: start, end: end, color: markColor)]
        }else{
            return nil
        }
    }
    
}

extension Date {
    var time: Time {
        return Time(self)
    }
}
