//
//  UPMapFromUserSide.swift
//  ExpressPlus
//
//  Created by Apple on 13/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import MapKit

import Firebase

class UPMapFromUserSide: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    /*
     open map
     Optional({
         EstTime = "15 Minutes";
         Mile = "4 Mile";
         PaymentStatus = 2;
         TIP = 10;
         address = "Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India";
         assignDriver = 412;
         cardNo = 4242424242424242;
         city = Delhi;
         couponCode = "";
         created = "2020-11-13 18:56:00";
         deliveryFee = "";
         deliveryLat = "28.587083";
         deliveryLong = "77.0605289";
         discount = 0;
         driverAVG = 0;
         driverContactNumber = 986532659855;
         driverId = 412;
         driverImage = "https://www.expressplusnow.com/appbackend/img/uploads/users/1605252045expressPlusEditProfilePhoto.jpg";
         driverName = qwed;
         foodDetails =     (
                     {
                 id = 103;
                 name = "Food ";
                 price = 20;
                 quantity = 1;
                 resturentId = 363;
             },
                     {
                 id = 104;
                 name = Pizza;
                 price = 50;
                 quantity = 1;
                 resturentId = 363;
             }
         );
         foodDetailsNew =     (
                     {
                 categoryId = 66;
                 description = "";
                 foodName = "Food ";
                 foodTag = Veg;
                 foodType = "";
                 "image_1" = "https://www.expressplusnow.com/appbackend/img/uploads/foods/1603090833_pexels-photo-262978.jpeg";
                 "image_2" = "";
                 "image_3" = "";
                 "image_4" = "";
                 "image_5" = "";
                 menuId = 103;
                 price = 30;
                 quantity = 1;
                 resturentId = 363;
                 specialPrice = 20;
             },
                     {
                 categoryId = 68;
                 description = "";
                 foodName = Pizza;
                 foodTag = Veg;
                 foodType = "";
                 "image_1" = "https://www.expressplusnow.com/appbackend/img/uploads/foods/1603090872_premiumforrestaurants_0.jpg";
                 "image_2" = "";
                 "image_3" = "";
                 "image_4" = "";
                 "image_5" = "";
                 menuId = 104;
                 price = 100;
                 quantity = 1;
                 resturentId = 363;
                 specialPrice = 50;
             }
         );
         foodId = "";
         foodOrderGroupId = 465;
         foodorderId = 479;
         imageUpload = "https://www.expressplusnow.com/appbackend/img/uploads/users/1605274015expressPlusUploadReceipt.jpg";
         landmark = "";
         name = v;
         noContact = "";
         phone = 9658352369;
         resturentAddress = "Anand vihar";
         resturentId = 363;
         resturentLatitude = "28.6457668";
         resturentLongitude = "77.3147845";
         resturentName = Restaurant;
         salesTax = 0;
         specialNote = "";
         state = Delhi;
         status = 2;
         storecity = "";
         totalAmount = 86;
         userAVG = 0;
         userId = 411;
         userName = qwe;
         whatYouWant = "";
         workPlace = Home;
         zipcode = 110075;
     })

     */
    
    var dictGetFullFormOfData:NSDictionary!
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    // MARK:- DRIVER -
    var driverLatitute:String!
    var driverLongitude:String!
    
    // MARK:- RESTAURANT -
    var restaurantLatitude:String!
    var restaurantLongitude:String!
    
    
    // MARK:- CUSTOMER -
    var customerLatitude:String!
    var customerLongitude:String!
    
    var chatMessages:NSMutableArray! = []
    
    @IBOutlet weak var mapView:MKMapView!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "Path"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnPhone:UIButton!
    @IBOutlet weak var btnNameAndDetails:UIButton!
    
    @IBOutlet weak var lblMessage:UILabel! {
        didSet {
            lblMessage.layer.cornerRadius = 4
            lblMessage.clipsToBounds = true
            lblMessage.layer.borderWidth = 0.8
            lblMessage.layer.borderColor = UIColor.lightGray.cgColor
            lblMessage.text = "Rider is going to pick up your order."
        }
    }
    
    @IBOutlet weak var imgReceiptImage:UIImageView! {
        didSet {
            imgReceiptImage.layer.cornerRadius = 12
            imgReceiptImage.clipsToBounds = true
            imgReceiptImage.backgroundColor = .brown
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(dictGetFullFormOfData as Any)
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.imgReceiptImage.sd_setImage(with: URL(string: (self.dictGetFullFormOfData["imageUpload"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        self.btnNameAndDetails.setTitle("Rider Name : "+(self.dictGetFullFormOfData["driverName"] as! String), for: .normal)
        self.btnPhone.setTitle((self.dictGetFullFormOfData["driverContactNumber"] as! String), for: .normal)
        
        // MARK:- 1 ( MAP ) -
        self.locManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            self.locManager.delegate = self
            self.locManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locManager.startUpdatingLocation()
            print("UPDATE UPDATE")
        }
       
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways) {
            print("")
        }
        
        
    }

    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- 2 ( MAP ) -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
                
        if self.dictGetFullFormOfData["status"] as! String == "1" {
            
        } else if self.dictGetFullFormOfData["status"] as! String == "2" {
            // towards customer
            
            let x2 : Int = dictGetFullFormOfData["foodOrderGroupId"] as! Int
            let driverId = String(x2)
            
            let x3 : Int = dictGetFullFormOfData["driverId"] as! Int
            let driverId3 = String(x3)
            
            let subChildId = driverId+"@"+driverId3
            print(subChildId as Any)
            
            let ref = Database.database().reference()
            ref.child("liveTracking").child(subChildId).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    print("true rooms exist")
                   
                    for child in snapshot.children {
                        let snap = child as! DataSnapshot
                        let placeDict = snap.value as! [String: Any]
                        // print(placeDict as Any)
                        self.chatMessages.add(placeDict)
                    }
                    
                    /*
                     ["driverId": "412", "longs": "37.617633", "adress": "Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India", "lat": "55.755786", "bookingId": "465"]
                     */
                    
                    let item = self.chatMessages.lastObject as? [String:Any]
                    
                    // print(self.chatMessages.count)
                    
                    // print(self.chatMessages.lastObject as Any)
                    
                    //print("Long \(manager.location!.coordinate.longitude)")
                    //print("Lati \(manager.location!.coordinate.latitude)")
                    
                    /*
                     "lat"            : "\(manager.location!.coordinate.latitude)",
                     "longs"          : "\(manager.location!.coordinate.longitude)"] as [String : Any]
                     */
                    
                    // user
                    let restaurantLatitudeDouble    = Double("\(manager.location!.coordinate.latitude)")
                    let restaurantLongitudeDouble   = Double("\(manager.location!.coordinate.longitude)")
                    
                    // driver
                    let driverLatitudeDouble        = Double(item!["lat"] as! String) //Double(self.dictGetFullFormOfData["deliveryLat"] as! String)
                    let driverLongitudeDouble       = Double(item!["longs"] as! String) //Double(self.dictGetFullFormOfData["deliveryLong"] as! String)
                    
                    let coordinate₀ = CLLocation(latitude: restaurantLatitudeDouble!, longitude: restaurantLongitudeDouble!)
                        let coordinate₁ = CLLocation(latitude: driverLatitudeDouble!, longitude: driverLongitudeDouble!)
                    
                /************************************** RESTAURANT LATITUTDE AND LONGITUDE  ********************************/
                                    // first location
                    let sourceLocation = CLLocationCoordinate2D(latitude: restaurantLatitudeDouble!, longitude: restaurantLongitudeDouble!)
                /********************************************************************************************************************/
                                                        

                /************************************* DRIVER LATITUTDE AND LINGITUDE ******************************************/
                                    // second location
                                    let destinationLocation = CLLocationCoordinate2D(latitude: driverLatitudeDouble!, longitude: driverLongitudeDouble!)
                /********************************************************************************************************************/
                      
                            //print(sourceLocation)
                            //print(destinationLocation)
                             
                                    let sourcePin = customPin(pinTitle: "You", pinSubTitle: "", location: sourceLocation)
                                    let destinationPin = customPin(pinTitle: "Driver", pinSubTitle: "", location: destinationLocation)

                /***************** REMOVE PREVIUOS ANNOTATION TO GENERATE NEW ANNOTATION *******************************************/
                                self.mapView.removeAnnotations(self.mapView.annotations)
                /********************************************************************************************************************/
                            
                    self.mapView.addAnnotation(sourcePin)
                    self.mapView.addAnnotation(destinationPin)
                                   
                    let sourcePlaceMark = MKPlacemark(coordinate: sourceLocation)
                    let destinationPlaceMark = MKPlacemark(coordinate: destinationLocation)
                                                               
                    let directionRequest = MKDirections.Request()
                    directionRequest.source = MKMapItem(placemark: sourcePlaceMark)
                    directionRequest.destination = MKMapItem(placemark: destinationPlaceMark)
                    directionRequest.transportType = .automobile
                                           
                    let directions = MKDirections(request: directionRequest)
                    directions.calculate { [self] (response, error) in
                        guard let directionResonse = response else {
                            if let error = error {
                                print("we have error getting directions==\(error.localizedDescription)")
                            }
                            return
                        }
                                               
                /***************** REMOVE PREVIUOS POLYLINE TO GENERATE NEW POLYLINE *******************************/
                        let overlays = self.mapView.overlays
                        self.mapView.removeOverlays(overlays)
                /************************************************************************************/
                
                                            
                /***************** GET DISTANCE BETWEEN TWO CORDINATES *******************************/
                                         
                                            let distanceInMeters = coordinate₀.distance(from: coordinate₁)
                                            // print(distanceInMeters as Any)
                                            
                                            // remove decimal
                                            let distanceFloat: Double = (distanceInMeters as Any as! Double)
                                            // print(distanceFloat as Any)
                                            // self.lblDistance.text = (String(format: "Distance : %.0f Miles away", distanceFloat/1609.344))
                                            self.lblMessage.text = "Rider is "+(String(format: " : %.0f Miles away", distanceFloat/1609.344))+" from you."
                                            
                                            // print(distanceFloat/1609.344)
                                            // print(String(format: "Distance : %.0f Miles away", distanceFloat/1609.344))
                                            
                                            
                                            let s:String = String(format: "%.0f",distanceFloat/1609.344)
                                            // print(s as Any)
                                            
                                            let myString1 = s
                                            let myInt1 = Int(myString1)
                                            
                                            if myInt1! > 5 {
                                                
                                                
                                                
                                                /*
                                                if self.dictOne["imageUpload"] as! String == "" {
                                                    
                                                    self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                                    self.btnMarkAsDelivery.setTitle("Upload Receipt", for: .normal)
                                                    self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                    self.btnMarkAsDelivery.addTarget(self, action: #selector(self.uploadReceiptClickMethod), for: .touchUpInside)
                                                    
                                                } else {
                                                    
                                                    
                                                    if self.dictOne["status"] is String {
                                                                        
                                                        print("Yes, it's a String")
                                                        // self.changesBookingIdis = (dictGetSpecialOrderData["bookingId"] as! String);
                                                        // myString12 = (dictGetSpecialOrderData["status"] as! String)
                                                        
                                                        if self.dictOne["status"] as! String == "1" {
                                                            // 1
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                            self.btnMarkAsDelivery.backgroundColor = .systemYellow
                                                            self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                                                            
                                                            self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                                                            
                                                        } else  if self.dictOne["status"] as! String == "2" {
                                                            // 2 : on the way
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                            self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                                            self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                                            
                                                            self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB), for: .touchUpInside)
                                                            
                                                        } else if self.dictOne["status"] as! String == "4" {
                                                            // 4
                                                            self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                                            self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = false
                                                        }
                                                        
                                                    } else if self.dictOne["status"] is Int {
                                                        
                                                        print("It is Integer")
                                                        let x2 : Int = (self.dictOne["status"] as! Int)
                                                        let myString2 = String(x2)
                                                        // self.dictGetSpecialOrderData = String(myString2)
                                                        // myString12 = String(myString2)
                                                        
                                                        if myString2 == "1" {
                                                            // 1
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                            self.btnMarkAsDelivery.backgroundColor = .systemYellow
                                                            self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                                                            
                                                            self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                                                            
                                                        } else if myString2 == "2" {
                                                            // 2 : on the way
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                            self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                                            self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                                            
                                                            self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB), for: .touchUpInside)
                                                            
                                                        } else if myString2 == "4" {
                                                            // 4
                                                            self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                                            self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = false
                                                        }
                                                        
                                                    } else {
                                                        
                                                        print("i am ")
                                                        let temp:NSNumber = self.dictOne["status"] as! NSNumber
                                                        let tempString = temp.stringValue
                                                        // self.changesBookingIdis = String(tempString)
                                                        // myString12 = String(tempString)
                                                        
                                                        
                                                        if tempString == "1" {
                                                            // 1
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                            self.btnMarkAsDelivery.backgroundColor = .systemYellow
                                                            self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                                                            
                                                            self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                                                            
                                                        } else  if tempString == "2" {
                                                            // 2 : on the way
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                            self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                                            self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                                            
                                                            self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB), for: .touchUpInside)
                                                            
                                                        } else if tempString == "4" {
                                                            // 4
                                                            self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                                            self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = false
                                                        }
                                                        
                                                    }
                                                    
                                                    
                                                    
                                                    // self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                                    // self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                    
                                                    self.imgUploadRecipt.sd_setImage(with: URL(string: (dictOne["imageUpload"] as! String)), placeholderImage: UIImage(named: "logo"))
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                }
                                                */
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                /*
                                                if self.dictOne["imageUpload"] as! String == "" {
                                                    
                                                    self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                                    self.btnMarkAsDelivery.setTitle("Upload Receipt", for: .normal)
                                                    self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                    self.btnMarkAsDelivery.addTarget(self, action: #selector(self.uploadReceiptClickMethod), for: .touchUpInside)
                                                    
                                                } else {
                                                    
                                                    self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                                    self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                                    self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                    // self.btnMarkAsDelivery.addTarget(self, action: #selector(self.uploadReceiptClickMethod), for: .touchUpInside)
                                                    
                                                }
                                                */
                                                
                                                
                                            } else {
                                                print("outside")
                                            }
                                              
                                            
                                            
                                            
                                            
                                            
                /************************************************************************************/
                                            
                /***************** GENERATE NEW POLYLINE *******************************/
                                            let route = directionResonse.routes[0]
                                            self.mapView.addOverlay(route.polyline, level: .aboveRoads)
                                            let rect = route.polyline.boundingMapRect
                                            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
                /************************************************************************************/
                                       
                                           }
                    self.mapView.delegate = self
                    print("update location after 5 sec")
                    
                    
                } else {
                    print("false room doesn't exist")
                    // self.tbleView.delegate = self
                    // self.tbleView.dataSource = self
                    // self.viewTypeBG.isHidden = false // send message view
                }
            })
            
            
            
            
            
            
            
            
            
            
            
            
            
        }
        
        
        
        
                        // self.locManager.stopUpdatingLocation()
                
                // speed = distance / time
    }
    
    //MARK:- MapKit delegates -
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        return renderer
    }
    
}
