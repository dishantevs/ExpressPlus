//
//  ROrderRequestPlusDriver.swift
//  ExpressPlus
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

import Alamofire
import SDWebImage

class UPResDeliveryDetails: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate , UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    var dictOrderDetails:NSDictionary!
    
    var locationManager:CLLocationManager!
    // var mapView:MKMapView!
    
    
    
    @IBOutlet weak var mapView:MKMapView!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "ORDER LIST"
        }
    }
    
    
    
    
    
    @IBOutlet weak var btnInProgress:UIButton! {
        didSet {
            btnInProgress.backgroundColor = UIColor.init(red: 217.0/255.0, green: 91.0/255.0, blue: 49.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var btnDeliverd:UIButton! {
        didSet {
            btnDeliverd.backgroundColor = UIColor.init(red: 217.0/255.0, green: 91.0/255.0, blue: 49.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var btnRequestPlusDriver:UIButton! {
        didSet {
            btnRequestPlusDriver.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnRequestPlusDriver.setTitleColor(.white, for: .normal)
            btnRequestPlusDriver.setTitle("DRIVER IS NOT START", for: .normal)
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblTotalAmount:UILabel! {
        didSet {
            lblTotalAmount.text = ""
        }
    }
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblWorkAndLandmark:UILabel!
    @IBOutlet weak var lblSpecialNotes:UILabel!
    
    var strPassImgString:String!
    var imageStr:String!
    var imgData:Data!
    
    @IBOutlet weak var imageSlip:UIImageView!
    @IBOutlet weak var imageDelivered:UIImageView!
    
    @IBOutlet weak var btnGoogleMap:UIButton!
    
    @IBOutlet weak var btnGoogleMapForCustomer:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // btnRequestPlusDriver.addTarget(self, action: #selector(pushToEarningPage), for: .touchUpInside)
        self.btnInProgress.addTarget(self, action: #selector(backClickMehod), for: .touchUpInside)
        
        self.btnBack.addTarget(self, action: #selector(backClickMehod), for: .touchUpInside)
        
        /*self.strSaveLatitude = "0"
        self.strSaveLongitude = "0"*/
        
        print(dictOrderDetails as Any)
        
        /*
         let d = String(format: "%.2f", c)
         PaymentStatus = 1;
         EstTime = "15 Minutes";
             Mile = "4 Mile";
             TIP = 0;
             address = "Sector 10 Dwarka South West Delhi";
             assignDriver = "";
             cardNo = 4242424242424242;
             city = "New Delhi";
             couponCode = "";
             created = "2020-09-14 18:25:00";
             deliveryFee = "";
             deliveryLat = "28.587248707236366";
             deliveryLong = "28.587248707236366";
             discount = 0;
             driverAVG = 0;
             driverContactNumber = "";
             driverId = "";
             driverImage = "";
             driverName = "";
             foodDetails =     (
                         {
                     id = 94;
                     name = "Shahi Paneer";
                     price = 270;
                     quantity = 1;
                     resturentId = 247;
                 },
                         {
                     id = 95;
                     name = "Fried Okra";
                     price = 140;
                     quantity = 1;
                     resturentId = 247;
                 }
             );
             foodDetailsNew =     (
                         {
                     categoryId = 67;
                     description = "Nice sahi paneer";
                     foodName = "Shahi Paneer";
                     foodTag = "";
                     foodType = "";
                     "image_1" = "https://www.expressplusnow.com/appbackend/img/uploads/foods/1599897474expressPlusUploadFoodPhoto.jpg";
                     "image_2" = "";
                     "image_3" = "";
                     "image_4" = "";
                     "image_5" = "";
                     menuId = 94;
                     price = 300;
                     quantity = 1;
                     resturentId = 247;
                     specialPrice = 270;
                 },
                         {
                     categoryId = 66;
                     description = "Nice okra ";
                     foodName = "Fried Okra";
                     foodTag = "";
                     foodType = "";
                     "image_1" = "https://www.expressplusnow.com/appbackend/img/uploads/foods/1599897512expressPlusUploadFoodPhoto.jpg";
                     "image_2" = "";
                     "image_3" = "";
                     "image_4" = "";
                     "image_5" = "";
                     menuId = 95;
                     price = 150;
                     quantity = 1;
                     resturentId = 247;

                     specialPrice = 140;
                 }
             );
             foodId = "";
             foodorderId = 225;
             landmark = ya;
             name = hi;
             noContact = "";
             phone = 8989898989;
             resturentAddress = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
             resturentId = 247;
             resturentLatitude = "33.985805";
             resturentLongitude = "77.3240623";
             resturentName = restaurant;
             salesTax = 0;
             specialNote = "";
             state = "South West Delhi";
             status = 10;
             storecity = "";
             totalAmount = 416;
             userAVG = 0;
             userId = 267;
             userName = "I am customer";
             whatYouWant = "";
             workPlace = Home;
             zipcode = 110075;
         })
         (lldb)
         
         // status = 10;
         // PaymentStatus = 1;
       
        
        // DRIVER
        // !2 but status is 1 = payment not done
        //
        */
        
        // image one
        
        
        // self.loginUserFoodData()
         
        
    }
    
    @objc func loginUserFoodData() {
    
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             print(person as Any)
            /*
             ["fullName": ios2020, "AccountHolderName": , "state": , "email": ios2020@gmail.com, "socialType": , "middleName": , "longitude": , "companyBackground": , "BankName": , "lastName": , "latitude": , "AccountNo": , "socialId": , "accountType": , "gender": , "ssnImage": , "role": Member, "country": , "address": Wadala West Mumbai, "deviceToken": , "firebaseId": , "contactNumber": 8287632340, "drivlingImage": , "device": iOS, "dob": , "logitude": , "AutoInsurance": , "userId": 127, "wallet": 0, "image": , "zipCode": , "RoutingNo": , "foodTag": ]
             */
            
            if person["role"] as! String == "Member" {
        
               self.customerDataShow()
               
            } else {
               
                let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(cellTappedMethod2))

                self.imageSlip.isUserInteractionEnabled = true
                // imgProfile.tag = indexPath.row
                self.imageSlip.addGestureRecognizer(tapGestureRecognizer2)
                          
               self.driverDataShow()
            }
           
        }
        
        // self.iAmHereForLocationPermission()
    }
    
    @objc func customerDataShow() {
        ERProgressHud.sharedInstance.hide()
        // 10 driver not start
        self.lblName.text               = (dictOrderDetails["userName"] as! String)
        
        self.btnGoogleMap.isHidden = true
        self.btnGoogleMapForCustomer.isHidden = true
        
        let addTransactionFeeWithTotalAmount = Double((dictOrderDetails["transactionFee"] as! String))!+Double((dictOrderDetails["totalAmount"] as! String))!
        
        let d = String(format: "%.2f", addTransactionFeeWithTotalAmount)
        self.lblTotalAmount.text        = "$ "+String(d)
        // self.lblTotalAmount.text        = "$ "+(dictOrderDetails["totalAmount"] as! String)
        self.lblAddress.text            = (dictOrderDetails["address"] as! String)
        self.lblWorkAndLandmark.text    = "Restaurant Name" //(dictOrderDetails["workPlace"] as! String)+" "+(dictOrderDetails["landmark"] as! String)
        
        self.lblSpecialNotes.text = (dictOrderDetails["resturentName"] as! String)
        
        
        let x2 : String = dictOrderDetails["PaymentStatus"] as! String
        let paymentStatusIs = String(x2)
        
         let x : String = dictOrderDetails["status"] as! String
         let myString = String(x)
        
        if myString == "10" {
            
            self.btnRequestPlusDriver.setTitle("DRIVER IS NOT START", for: .normal)
            self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
            
        } else if myString == "4" {
            
            self.btnRequestPlusDriver.setTitle("DELIVERED", for: .normal)
            self.btnRequestPlusDriver.backgroundColor = .systemGreen
            
        } else if myString == "1" && paymentStatusIs == "2" {
            
            if (dictOrderDetails["imageUpload"] as! String) == "" {
                
                self.btnRequestPlusDriver.setTitle("WAITING FOR SLIP!", for: .normal)
                self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                self.btnRequestPlusDriver.isUserInteractionEnabled = false
                
            } else {
                
                self.btnRequestPlusDriver.setTitle("SLIP UPLOADED", for: .normal)
                self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                self.btnRequestPlusDriver.isUserInteractionEnabled = false
                
            }
            
            
        } else if myString == "2" && paymentStatusIs == "2" {
            
            if (dictOrderDetails["imageUpload"] as! String) == "" {
                
                self.btnRequestPlusDriver.setTitle("SLIP UPLOADED", for: .normal)
                self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                
            } else {
                
                self.btnRequestPlusDriver.setTitle("ON THE WAY!", for: .normal)
                self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                
            }
            
        } else if myString == "1" && paymentStatusIs == "1" {
            
            self.btnRequestPlusDriver.isHidden = true
            
        } else {
            
            self.btnRequestPlusDriver.setTitle("ON THE WAY", for: .normal)
            self.btnRequestPlusDriver.addTarget(self, action: #selector(onTheWayShowPath), for: .touchUpInside)
            self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        }
        
        
        imageSlip.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        
        if let url = (dictOrderDetails["imageUpload"] as! String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            
            imageSlip.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "avatar"))
        }
        
        
        self.imageDelivered.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        
        if let url = (dictOrderDetails["DeliveryImage"] as! String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            
            imageDelivered.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "avatar"))
        }
        
        
        self.btnRequestPlusDriver.addTarget(self, action: #selector(requestAndUpdateButton), for: .touchUpInside)
        
    }
    
    @objc func driverDataShow() {
        ERProgressHud.sharedInstance.hide()
        /*
         [action] => foodrequestaccept
             [userId] => 465
             [foodrequestId] => 721
             [foodorderId] => 611
             [status] => 2
         */
        
        self.lblName.text               = (dictOrderDetails["userName"] as! String)
        
        let addTransactionFeeWithTotalAmount = Double((dictOrderDetails["transactionFee"] as! String))!+Double((dictOrderDetails["totalAmount"] as! String))!
        
        let d = String(format: "%.2f", addTransactionFeeWithTotalAmount)
        self.lblTotalAmount.text        = "$ "+String(d)
        // self.lblTotalAmount.text        = "$ "+(dictOrderDetails["totalAmount"] as! String)
        self.lblAddress.text            = (dictOrderDetails["address"] as! String)
        self.lblWorkAndLandmark.text    = "Restaurant Name" //(dictOrderDetails["workPlace"] as! String)+" "+(dictOrderDetails["landmark"] as! String)
        
        self.lblSpecialNotes.text = (dictOrderDetails["resturentName"] as! String)
        
        
         let x : String = dictOrderDetails["status"] as! String
         let statusIs = String(x)
        
        let x2 : String = dictOrderDetails["PaymentStatus"] as! String
        let paymentStatusIs = String(x2)
        
        // PaymentStatus = 1; status = 1;
        
        if statusIs == "1" && paymentStatusIs == "1" {
            
            self.btnRequestPlusDriver.setTitle("WAITING FOR PAYMENT", for: .normal)
            self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
            
        } else if statusIs == "1" && paymentStatusIs == "2" {
            
            if (dictOrderDetails["imageUpload"] as! String) == "" {
                
                self.btnRequestPlusDriver.setTitle("UPLOAD SLIP", for: .normal)
                self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                
            } else {
                
                self.btnRequestPlusDriver.setTitle("CONFIRM AND PICKUP", for: .normal)
                self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                
            }
            
        } else if statusIs == "2" && paymentStatusIs == "2" {
            
            if (dictOrderDetails["imageUpload"] as! String) == "" {
                
                self.btnRequestPlusDriver.setTitle("UPLOAD SLIP", for: .normal)
                self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                
            } else {
                
                self.btnRequestPlusDriver.setTitle("TRACK YOUR ORDER", for: .normal)
                self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                
            }
            
        } else if statusIs == "10" {
            
            self.btnRequestPlusDriver.setTitle("ACCEPT", for: .normal)
            self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
            
        } else if statusIs == "4" && paymentStatusIs == "2" {
            
            self.btnRequestPlusDriver.setTitle("COMPLETED", for: .normal)
            self.btnRequestPlusDriver.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
            self.btnRequestPlusDriver.isUserInteractionEnabled = false
            
            self.imageSlip.isUserInteractionEnabled = false
        }
        
        imageSlip.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        
        if let url = (dictOrderDetails["imageUpload"] as! String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            
            imageSlip.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "avatar"))
        }
        
        self.imageDelivered.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        
        if let url = (dictOrderDetails["DeliveryImage"] as! String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            
            imageDelivered.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "avatar"))
        }
        
        self.btnRequestPlusDriver.addTarget(self, action: #selector(requestAndUpdateButton), for: .touchUpInside)
        self.btnGoogleMap.addTarget(self, action: #selector(openGoogleMaps), for: .touchUpInside)
        
        self.btnGoogleMapForCustomer.addTarget(self, action: #selector(openGoogleMapsForCustomerLocation), for: .touchUpInside)
        
    }
    
    @objc func openGoogleMaps() {
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
            
            
            let alert = UIAlertController(title: String("Express Plus"), message: String("Please Google to the closest restaurant to you based on this order."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                
                
                if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                    UIApplication.shared.openURL(NSURL(string:
                                                        "comgooglemaps://?saddr=&daddr=\(person["latitude"] as! String),\(person["longitude"] as! String)&directionsmode=driving")! as URL)
                    
                } else {
                    
                    NSLog("Can't use comgooglemaps://")
                    
                    let alert = UIAlertController(title: String("Error"), message: String("Either Google Maps is not installed in your Device or your Device does not support Google Maps"), preferredStyle: UIAlertController.Style.alert)
                    
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                        
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @objc func openGoogleMapsForCustomerLocation() {
        // print(self.dictOrderDetails as Any)
        
        if (self.dictOrderDetails["deliveryLat"] as! String) == "" {
            
            let alert = UIAlertController(title: String("Error"), message: String("Location not supported"), preferredStyle: UIAlertController.Style.alert)
            
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            let alert = UIAlertController(title: String("Express Plus"), message: String("Please google the customer location to deliver the order."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                
                
                if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                    UIApplication.shared.openURL(NSURL(string:
                                                        "comgooglemaps://?saddr=&daddr=\(self.dictOrderDetails["deliveryLat"] as! String),\(self.dictOrderDetails["deliveryLong"] as! String)&directionsmode=driving")! as URL)
                    
                } else {
                    
                    NSLog("Can't use comgooglemaps://")
                    
                    let alert = UIAlertController(title: String("Error"), message: String("Either Google Maps is not installed in your Device or your Device does not support Google Maps"), preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @objc func pushToAcceptOrDeclinePage() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDPopUpDeliveryId") as? PDPopUpDelivery
        
        // settingsVCId!.dictGetFoodDeliveryDetailsForAccept = dictGetDeliveryDetail
        push!.dictGetFoodDeliveryDetailsForAcceptNew = dictOrderDetails
        
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func onTheWayShowPath() {
        print("open map")
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPMapFromUserSideId") as? UPMapFromUserSide
        
        settingsVCId!.dictGetFullFormOfData = self.dictOrderDetails
        
//         let x : Int = dictOrderDetails["foodorderId"] as! Int
//         let myString = String(x)
//         
//        settingsVCId!.strFoodOrderId = String(myString)
        
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateFoodDetails()
        
        
        // Create and Add MapView to our main view
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        determineCurrentLocation()
    }
    
    func createMapView() {
        // mapView = MKMapView()
        
        // let leftMargin:CGFloat = 10
        // let topMargin:CGFloat = 60
        // let mapWidth:CGFloat = view.frame.size.width-20
        // let mapHeight:CGFloat = 300
        
        // mapView.frame = CGRectMake(leftMargin, topMargin, mapWidth, mapHeight)
        // mapView.frame = CGRect(x: leftMargin, y: topMargin, width: mapWidth, height: mapHeight)
        
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        // Or, if needed, we can position map in the center of the view
        mapView.center = view.center
        
        view.addSubview(mapView)
    }
    func determineCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            //locationManager.startUpdatingHeading()
            locationManager.startUpdatingLocation()
        }
    }
    // private func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        //manager.stopUpdatingLocation()
        
        let myDouble1 = Double(dictOrderDetails["deliveryLat"] as! String)
        let myDouble2 = Double(dictOrderDetails["deliveryLong"] as! String)
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: myDouble1!, longitudeDelta: myDouble2!))
        
        mapView.setRegion(region, animated: true)
        
        // Drop a pin at user's Current Location
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
        myAnnotation.title = (dictOrderDetails["name"] as! String)
        mapView.addAnnotation(myAnnotation)
        
        
        
        
        
    }
    
    private func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error \(error)")
    }
    
    @objc func backClickMehod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func pushToEarningPage() {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TrackYourOrderId") as? TrackYourOrder
        
         let x : Int = dictOrderDetails["foodorderId"] as! Int
         let myString = String(x)
         
        settingsVCId!.strFoodOrderId = String(myString)
        
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    
    // MARK:- CONFIRM AND PICKUP ( FROM DRIVER END ) -
    @objc func requestAndUpdateButton() {
        
        if self.btnRequestPlusDriver.titleLabel?.text == "UPLOAD SLIP" {
            
            // self.DRIVERuploadSlipAndConfirmPickup()
            
            self.cellTappedMethod2()
             
        } else if self.btnRequestPlusDriver.titleLabel?.text == "CONFIRM AND PICKUP" {
            
            self.confirmAndPickupFromDriverSide()
            
        } else if self.btnRequestPlusDriver.titleLabel?.text == "TRACK YOUR ORDER" || self.btnRequestPlusDriver.titleLabel?.text == "ON THE WAY!" {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDInProgressId") as? PDInProgress
            
            push!.dictOne = self.dictOrderDetails
            // settingsVCId!.dictTwo = self.dict2Try
            
            self.navigationController?.pushViewController(push!, animated: true)
            
        } else if self.btnRequestPlusDriver.titleLabel?.text == "ACCEPT" {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDPopUpDeliveryId") as? PDPopUpDelivery
            // settingsVCId!.dictGetFoodDeliveryDetailsForAccept = dictGetDeliveryDetail
            push!.dictGetFoodDeliveryDetailsForAccept = dictOrderDetails
            self.navigationController?.pushViewController(push!, animated: true)
            
        }
        
    }
    
    @objc func cellTappedMethod2() {
        
        let alert = UIAlertController(title: "Upload Slip", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera1()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery1()
        }))

        alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    @objc func openCamera1() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    @objc func openGallery1() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
        imageSlip.isHidden = false
        let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        imageSlip.image = image_data // show image on profile
        let imageData:Data = image_data!.pngData()!
        imageStr = imageData.base64EncodedString()
        self.dismiss(animated: true, completion: nil)
        imgData = image_data!.jpegData(compressionQuality: 0.2)!
               //print(type(of: imgData)) // data
               
               // self.strPassImgString1 = "1"
           
        DispatchQueue.main.async {
            
            self.uploadSlipImage()
            
        }
        
    }
    
    @objc func uploadSlipImage() {
        
    /*
         "action"        : "updatefoodorder",
         "driverId"      : String(driverId),
         "foodorderId"   : String(foodOrderId),
         "status"        : String("4")
         */
        
        self.view.endEditing(true)
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Uploading...")
        
            let xfd : Int = dictOrderDetails["foodorderId"] as! Int
            let foodOrderId = String(xfd)
            
            // let drid : Int = dictOrderDetails["driverId"] as! Int
            // let driverId = String(drid)
            
            
            // var parameters:Dictionary<AnyHashable, Any>!
                 let parameters = [
                    "action"        : "uploadimage",
                    // "driverId"      : String(driverId),
                    "foodorderId"   : String(foodOrderId),
                    // "status"        : String("4")
                 ]
                
            print(parameters as Any)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.imgData, withName: "imageUpload",fileName: "uploadSlipViaDriver.jpg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        
                        // let paramsData:Data = NSKeyedArchiver.archivedData(withRootObject: value)
                        
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                },
                to:BASE_URL_EXPRESS_PLUS)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):

                        upload.uploadProgress(closure: { (progress) in
                            //print("Upload Progress: \(progress.fractionCompleted)")
                            
                            /*
                            let alertController = UIAlertController(title: "Uploading image", message: "Please wait......", preferredStyle: .alert)

                            let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)

                            progressDownload.setProgress(Float((progress.fractionCompleted)/1.0), animated: true)
                            progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)

                            alertController.view.addSubview(progressDownload)
                            self.present(alertController, animated: true, completion: nil)
                            */
                        })

                        upload.responseJSON { response in
                            //print(response.result.value as Any)
                            if let data = response.result.value {
                                let JSON = data as! NSDictionary
                                print(JSON)
                                
                                /*ERProgressHud.sharedInstance.hide()
                                self.dismiss(animated: true, completion: nil)*/
                                
                                self.updateFoodDetails()
                                
                                
                            }
                            else {
                                ERProgressHud.sharedInstance.hide()
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        ERProgressHud.sharedInstance.hide()
                        self.dismiss(animated: true, completion: nil)
                    }}
        
    
    
    }
   
    @objc func updateFoodDetails() {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
        let urlString = BASE_URL_EXPRESS_PLUS
       
        var parameters:Dictionary<AnyHashable, Any>!
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
       
            let strMyId:String!
            let x : Int = person["userId"] as! Int
            strMyId = String(x)
       
            let strMyIdy:String!
            let xy : Int = dictOrderDetails["foodorderId"] as! Int
            strMyIdy = String(xy)
           
            parameters = [
                "action"      : "foodorderdetails",
                "foodorderId" : String(strMyIdy),
                "userId"      : String(strMyId)
                         
            ]
        }
        
        print("parameters-------\(String(describing: parameters))")
                     
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
           response in
                 
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                 
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                                 
                                  
                    if strSuccess == String("success") {
                        print("yes")
                                 
                        self.dictOrderDetails = JSON["data"] as! Dictionary<AnyHashable, Any> as NSDictionary
                       
                        self.loginUserFoodData()
                       
                   }
                   else {
                       
                       print("no")
                       ERProgressHud.sharedInstance.hide()
                                  
                   }
               }

           case .failure(_):
               print("Error message:\(String(describing: response.result.error))")
                                 
               ERProgressHud.sharedInstance.hide()
                                 
               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                 
               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                   UIAlertAction in
                   NSLog("OK Pressed")
               }
                                 
               alertController.addAction(okAction)
                                 
               self.present(alertController, animated: true, completion: nil)
                                 
               break
           }
       }
   }
    
    
    
    
    
    
    
    
    
    
    
    // MARK:- CONFIRM AND PICKUP -
    @objc func confirmAndPickupFromDriverSide() {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
        let urlString = BASE_URL_EXPRESS_PLUS
      
        let x : Int = dictOrderDetails["foodorderId"] as! Int
        let myString = String(x)
      
        var parameters:Dictionary<AnyHashable, Any>!
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
              
            parameters = [
                "action"        : "updatefoodorder",
                "driverId"      : person["userId"] as Any,
                "foodorderId"   : String(myString),
                "status"        : String("2")
            ]
        }
        
        print("parameters-------\(String(describing: parameters))")
                     
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
        {
            response in
                 
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                 
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                                 
                    if strSuccess == String("success") {
                        print("yes")
                        
                        self.updateFoodDetails()
                        
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                                  
                                  
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                                 
                ERProgressHud.sharedInstance.hide()
                                 
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                 
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                                 
                alertController.addAction(okAction)
                                 
                self.present(alertController, animated: true, completion: nil)
                                 
                break
            }
        }
      
    }
      
}
