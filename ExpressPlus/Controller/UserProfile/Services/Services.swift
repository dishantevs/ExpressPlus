//
//  Services.swift
//  ExpressPlus
//
//  Created by Apple on 11/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class Services: UIViewController {

    let cellReuseIdentifier = "serviceTableCell"
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
    
    var arr = ["YARD WORK","HOUSE CLEANING","BABY SITTING","PET WALKING"]
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    @IBOutlet weak var viewNavBottom:UIView! {
        didSet {
            viewNavBottom.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            viewNavBottom.layer.cornerRadius = 6
            viewNavBottom.clipsToBounds = true
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "SERVICE"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            // tbleView.delegate = self
            // tbleView.dataSource = self
        }
    }
    @IBOutlet weak var btnSaveContinue:UIButton! {
        didSet {
            btnSaveContinue.setTitleColor(.white, for: .normal)
            btnSaveContinue.setTitle("Continue", for: .normal)
            btnSaveContinue.setTitleColor(.white, for: .normal)
            btnSaveContinue.layer.cornerRadius = 6
            btnSaveContinue.clipsToBounds = true
            btnSaveContinue.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        btnBack.setImage(UIImage(named: "sMenu"), for: .normal)
        // btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        self.sideBarMenuClick()
        btnSaveContinue.addTarget(self, action: #selector(saveAndContinueClickMethod), for: .touchUpInside)
        
        self.getAllServiceListWB()
    }
    @objc func saveAndContinueClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPDisclouserId") as? UPDisclouser
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    // [action] => servicelist

    @objc func getAllServiceListWB() {
        // self.arrListOfAllMyOrders.removeAllObjects()
     // self.strCheckOption = "3"
        
     ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
               
     // let indexPath = IndexPath.init(row: 0, section: 0)
     // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
     // print(dictGetRestaurantFoodListing as Any)
     
     /*
      [action] => servicelist
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
                   parameters = [
                       "action"              : "servicelist"
            ]
        // }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 // var dict: Dictionary<AnyHashable, Any>
                                 // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                  
                                 self.tbleView.delegate = self
                                 self.tbleView.dataSource = self
                                 self.tbleView.reloadData()
                                 // self.loadMore = 1;
                                
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
        }
}

//MARK:- TABLE VIEW -
extension Services: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ServiceTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! ServiceTableCell
        
        cell.backgroundColor = .white
       
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        
        cell.lblTitle.text = (item!["name"] as! String)
       
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        
        cell.btnClickUnclick.isHidden = true
        cell.btnClickUnclick.tag = indexPath.row
        cell.btnClickUnclick.addTarget(self, action: #selector(clickUnclickMenuClick), for: .touchUpInside)
        
        return cell
    }
    @objc func clickUnclickMenuClick(_ sender:UIButton) {
        
        let btnSelectedOrNot:UIButton = sender
        if btnSelectedOrNot.isSelected == true {
            btnSelectedOrNot.setImage(UIImage(named: "checkCustomerContact"), for: .normal)
            btnSelectedOrNot.isSelected = false
        } else if btnSelectedOrNot.isSelected == false {
            btnSelectedOrNot.setImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
            btnSelectedOrNot.isSelected = true
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        
        if item!["name"] as! String == "Other Service" {
            
            let alertController = UIAlertController(title: "Other Service", message: "", preferredStyle: UIAlertController.Style.alert)
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "other"
            }
            let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
                let firstTextField = alertController.textFields![0] as UITextField
                // let secondTextField = alertController.textFields![1] as UITextField
                
                // Create UserDefaults
                let defaults = UserDefaults.standard

                // Save String value to UserDefaults
                // Using defaults.set(value: Any?, forKey: String)
                defaults.set(String(firstTextField.text!), forKey: "keyServiceOther")

                
                
                
                // Get the String from UserDefaults
                if let myString = defaults.string(forKey: "savedString") {
                    print("defaults savedString: \(myString)")
                    
                }
                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPDisclouserId") as? UPDisclouser
                settingsVCId!.dictGetService = item as NSDictionary?
                self.navigationController?.pushViewController(settingsVCId!, animated: true)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
                (action : UIAlertAction!) -> Void in
                
            })
            

            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)

            self.present(alertController, animated: true, completion: nil)
            
        } else {
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPDisclouserId") as? UPDisclouser
            settingsVCId!.dictGetService = item as NSDictionary?
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}

extension Services: UITableViewDelegate {
    
}

