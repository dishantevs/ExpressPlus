//
//  ServiceTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 11/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ServiceTableCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    
    @IBOutlet weak var btnClickUnclick:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
