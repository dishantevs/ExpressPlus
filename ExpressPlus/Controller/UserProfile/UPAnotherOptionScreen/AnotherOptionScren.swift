//
//  AnotherOptionScren.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class AnotherOptionScren: UIViewController, UITextFieldDelegate {

    let cellReuseIdentifier = "uPAnotherOptionScreenTableCell"
    
    var strSaveTotalAmount:String!
    
    var myInt1:Int!
    var myInt2:Int!
    var myInt3:Int!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "FOODS"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
        }
    }
    
    @IBOutlet weak var btnSubmitRequest:UIButton! {
        didSet {
            btnSubmitRequest.setTitleColor(.white, for: .normal)
            btnSubmitRequest.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    
    var transactionFeeValueIs:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.strSaveTotalAmount = "0"
        
        self.btnSubmitRequest.addTarget(self, action: #selector(submitRequestClickMethod), for: .touchUpInside)
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(AnotherOptionScren.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        /*let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPAnotherOptionScreenTableCell
        cell.lblTotalAmount.text = "Transaction Fee : $ "+roundFigureOfTotalCalulation+"\nTotal Amount to Pay : $ "+String(totalAmountAfterGetTransactionFee)*/
        
        self.sideBarMenuClick()
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    // MARK:- ANOTHER OPTION SCREEN PUSH -
    @objc func submitRequestClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPAnotherOptionScreenTableCell
        
        
        if cell.txtViewWhatDoYou.text == "" {
             let alert = UIAlertController(title: String("Alert!"), message: String("What do you want should not be Empty"), preferredStyle: UIAlertController.Style.alert)
             alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
             }))
             self.present(alert, animated: true, completion: nil)
        } else if cell.txtPrice.text == "" {
             let alert = UIAlertController(title: String("Alert!"), message: String("Price should not be Empty"), preferredStyle: UIAlertController.Style.alert)
             alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
             }))
             self.present(alert, animated: true, completion: nil)
        } else if cell.txtTip.text == "" {
             let alert = UIAlertController(title: String("Alert!"), message: String("Tip should not be Empty"), preferredStyle: UIAlertController.Style.alert)
             alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
             }))
             self.present(alert, animated: true, completion: nil)
        } else {
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSpecialOrderAddressFourId") as? UPSpecialOrderAddressFour
            push!.strWhatDoYouWant  = String(cell.txtViewWhatDoYou.text!)
            push!.strTotalAmount    = String(self.strSaveTotalAmount)
            push!.getTip = String(cell.txtTip.text!)
            push!.getestimatePrice = String(cell.txtPrice.text!)
            push!.strTransactionValueIs = self.transactionFeeValueIs
            self.navigationController?.pushViewController(push!, animated: true)
        }
        
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}

//MARK:- TABLE VIEW -
extension AnotherOptionScren: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UPAnotherOptionScreenTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UPAnotherOptionScreenTableCell
        
        cell.backgroundColor = .white
       
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.txtPrice.delegate = self
        cell.txtTip.delegate = self
        
        cell.txtPrice.addTarget(self, action: #selector(AnotherOptionScren.textFieldDidChange(_:)), for: .editingChanged)
        cell.txtTip.addTarget(self, action: #selector(AnotherOptionScren.textFieldDidChange(_:)), for: .editingChanged)
        
        // cell.lblTitle.text = titleArray[indexPath.row]
       
        return cell
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPAnotherOptionScreenTableCell
        
        let myString1 = String(cell.txtPrice.text!)
        // let myInt1 = Int(myString1)
        if myString1 == "" {
            myInt1 = Int(0)
        } else {
            myInt1 = Int(myString1)
        }
        
        let myString2 = String("6")
        // let myInt2 = Int(myString2)
        myInt2 = Int(myString2)
        
        let myString3 = String(cell.txtTip.text!)
        // let myInt3 = Int(myString3)
        if myString3 == "" {
            myInt3 = Int(0)
        } else {
            myInt3 = Int(myString3)
        }
        
        var ans1 = Int();
        ans1 = myInt1!+myInt2!+myInt3!
        
        let x2 : Int = ans1
        let myStringFinal = String(x2)
        self.strSaveTotalAmount = String(myStringFinal)
        
        cell.lblTotalAmount.text = "Total Amount : $"+String(myStringFinal)
        // cell.btnTotalAmount.setTitle("Total Amount : $"+String(myStringFinal), for: .normal)//text = "Total Amount : $ "+String(myStringFinal)
     
        self.lblTransactionFee(strTotalAmount: String(myStringFinal))
    }
    
    // MARK:- TRANSACTION FEE -
    @objc func lblTransactionFee(strTotalAmount:String) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPAnotherOptionScreenTableCell
        
        print(strTotalAmount as Any)
        
        let convertTotalPriceToDouble = Double(strTotalAmount)!
        
        //* calculate total price
        //* formula = totalPrice *5.8%
        
        //* 0.058
        //* why 0.058 bcause it get value from 5.8%
        //* 5.8% = 0.058
        let multiplyTractactionFeeWithTotalAmount = convertTotalPriceToDouble * 0.058
        // print(multiplyTractactionFeeWithTotalAmount as Any)
        
        //* convert multiple double values into two digits
        // print(String(format:"%.02f", multiplyTractactionFeeWithTotalAmount))
        
        let getValueAfterMultiplyByFivePointEightPercentage = String(format:"%.02f", multiplyTractactionFeeWithTotalAmount)
        // print(getValueAfterMultiplyByFivePointEightPercentage as Any)
        
        //* now add .60 with result
        let cents = 0.60
        
        let addCentsWithMultiplicationResult = Double(getValueAfterMultiplyByFivePointEightPercentage)!+cents
        // print(addCentsWithMultiplicationResult as Any)
        
        // round figure of result
        let roundFigureOfTotalCalulation = String(format:"%.02f", addCentsWithMultiplicationResult)
        // print(roundFigureOfTotalCalulation as Any)
        
        //* final price for Transaction Fee
        let transactionFeeIs = roundFigureOfTotalCalulation
        
        // cell.lblTransactionFee.text = "$ "+String(transactionFeeIs)
        
        self.transactionFeeValueIs = roundFigureOfTotalCalulation
        
        //* add transaction fee with total amount
        let totalAmountAfterGetTransactionFee = Double(strTotalAmount)!+Double(transactionFeeIs)!
         cell.lblTotalAmount.text = "Transaction Fee : $ "+roundFigureOfTotalCalulation+"\nTotal Amount to Pay : $ "+String(totalAmountAfterGetTransactionFee)
        //  cell.btnTotalAmount.setTitle("Transaction Fee : $ "+roundFigureOfTotalCalulation+"\nTotal Amount to Pay : $ "+String(totalAmountAfterGetTransactionFee), for: .normal)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPRestaurantProfileId") as? UPRestaurantProfile
        // self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 660
    }
    
}

extension AnotherOptionScren: UITableViewDelegate {
    
}

