//
//  UPSpecialOrderAddressFour.swift
//  ExpressPlus
//
//  Created by Apple on 08/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
// MARK:- LOCATION -
import CoreLocation

import Alamofire

class UPSpecialOrderAddressFour: UIViewController, CLLocationManagerDelegate {

    var getestimatePrice:String!
    var getTip:String!
    var getDeliveryFee:String!
    var getNotes:String!
     var getTotalPayment:String!
    
    // strings for special offer
    var strWhatDoYouWant:String!
    var strTotalAmount:String!
    
    let cellReuseIdentifier = "uPSpecialOrderAddressFourTableCell"
    let locationManager = CLLocationManager()
       
       // MARK:- SAVE LOCATION STRING -
       var strSaveLatitude:String!
       var strSaveLongitude:String!
       var strSaveCountryName:String!
       var strSaveLocalAddress:String!
       var strSaveLocality:String!
       var strSaveLocalAddressMini:String!
       var strSaveStateName:String!
       var strSaveZipcodeName:String!
       
       // MARK:- ADDRESS STRING -
       var strSaveAddress:String!
       var strSaveWork:String!
       
       // MARK:- GET ALL VALUE FROM CART -
       var strSaveContactOnDelivery2:String!
       var strSaveSpecialNotes2:String!
       var mArrSaveMutable2:NSMutableArray = []
       var getTotalAmountOfFood:String!
       var tipAmountIs:String!
       
       var strHW:String!
       
    var taxPriceIs:String!
    
    var strTransactionValueIs:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "ADDRESS"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            
        }
    }
    
    var yesUserMyCurrentLocation:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
                
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
                
                // print(strSaveContactOnDelivery as Any)
                // print(strSaveSpecialNotes as Any)
                // print(mArrSaveMutable as Any)
        self.yesUserMyCurrentLocation = "0"
        
        self.strSaveAddress = "0"
        self.strSaveWork = "0"
        self.taxPriceIs = "0"
            
                // let indexPath = IndexPath.init(row: 0, section: 0)
                // let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressTableCell
        //         cell.btnSaveContinue.addTarget(self, action: #selector(saveAndContinueClick), for: .touchUpInside)
                // cell.txtAddress.text = ""
                
        self.iAmHereForLocationPermission()
    }
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    // MARK:- GET CUSTOMER LOCATION
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            // print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            // print("locality ==> "+locality as Any) // sector 10 dwarka
            
            // print(self.strSaveCountryName as Any) // india
            // print(self.strSaveStateName as Any) // new delhi
            // print(self.strSaveZipcodeName as Any) // 110075
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            self.findMyStateTaxWB()
        }
    }
    
    // MARK:- WEBSERVICE ( FIND STATE ) -
    @objc func findMyStateTaxWB() {
          ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
          let urlString = BASE_URL_EXPRESS_PLUS
    
          var parameters:Dictionary<AnyHashable, Any>!
          // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
              
                     parameters = [
                         "action"       : "statefind",
                         "stateName"    : String(self.strSaveStateName)
              ]
          // }
                     print("parameters-------\(String(describing: parameters))")
                     
                     Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                         {
                             response in
                 
                             switch(response.result) {
                             case .success(_):
                                if let data = response.result.value {

                                 let JSON = data as! NSDictionary
                                  print(JSON as Any)
                                 
                                 var strSuccess : String!
                                 strSuccess = JSON["status"]as Any as? String
                                 
                                    /*
                                     name = Delhi;
                                     price = 0;
                                     status = success;
                                     */
                                    
                                    
                                    
                                   // var strSuccessAlert : String!
                                   // strSuccessAlert = JSON["msg"]as Any as? String
                                 
                                 if strSuccess == String("success") {
                                  print("yes")
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   
                                   // var dict: Dictionary<AnyHashable, Any>
                                  //  dict = JSON["msg"] as! Dictionary<AnyHashable, Any>
                                  
                                  // var strSuccess : String!
                                  // strSuccess = JSON["msg"]as Any as? String
                                
                                    
                                    if JSON["price"] is String {
                                                      
                                        print("Yes, it's a String")
                                      
                                        self.taxPriceIs  = (JSON["price"] as! String)
                                        
                                        // self.finalConfirmAndPayment(strSalesTaxIs: (JSON["price"] as! String), strFinalAmountpayIss: strFinalAmountpayIs)
                                        
                                    } else if JSON["price"] is Int {
                                      
                                        print("It is Integer")
                                      
                                        let x2 : Int = (JSON["price"] as! Int)
                                        let myString2 = String(x2)
                                        self.taxPriceIs = myString2
                                        
                                        // self.finalConfirmAndPayment(strSalesTaxIs: myString2, strFinalAmountpayIss: strFinalAmountpayIs)
                                      
                                    } else {
                                    //some other check
                                      print("i am ")
                                      
                                        let temp:NSNumber = JSON["price"] as! NSNumber
                                        let tempString = temp.stringValue
                                        self.taxPriceIs = tempString
                                        
                                        // self.finalConfirmAndPayment(strSalesTaxIs: tempString, strFinalAmountpayIss: strFinalAmountpayIs)
                                      
                                    }
                                    
                                    self.tbleView.delegate = self
                                    self.tbleView.dataSource = self
                                    self.tbleView.reloadData()
                                   
                                 }
                                 else {
                                  print("no")
                                   ERProgressHud.sharedInstance.hide()
                                    
                                    self.tbleView.delegate = self
                                    self.tbleView.dataSource = self
                                    self.tbleView.reloadData()
                                 }
                             }

                             case .failure(_):
                                 print("Error message:\(String(describing: response.result.error))")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 self.tbleView.delegate = self
                                 self.tbleView.dataSource = self
                                 self.tbleView.reloadData()
                                 
                                 /*
                                 let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                 
                                 let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                         UIAlertAction in
                                         NSLog("OK Pressed")
                                     }
                                 
                                 alertController.addAction(okAction)
                                 
                                 self.present(alertController, animated: true, completion: nil)
                                 */
                                 break
                            }
        }
    }
}

//MARK:- TABLE VIEW -
extension UPSpecialOrderAddressFour: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UPSpecialOrderAddressFourTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UPSpecialOrderAddressFourTableCell
        
        cell.backgroundColor    = .white
              
//        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
//            if person["address"] as! String == "" {
//                print(person as Any)
//
//                cell.txtAddress.text    = (person["address"] as! String)
//
//            } else {
//
//
//            }
//
//        } else {
            // cell.txtAddress.text    = String(self.strSaveLocality)+" "+String(self.strSaveStateName)
        // }
        
        
        if self.yesUserMyCurrentLocation == "1" {
            
            cell.txtAddress.text    = self.strSaveAddress
            
        } else {
            
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                cell.txtAddress.text    = (person["address"] as! String) //String(self.strSaveLocality)+" "+String(self.strSaveStateName)
            }
            
        }
        
        
        cell.txtState.text      = String(self.strSaveLocalAddress)
        cell.txtCity.text       = String(self.strSaveLocalAddressMini)
        cell.txtZipCode.text    = String(self.strSaveZipcodeName)
        
        cell.btnHome.tag = 0
        cell.btnWork.tag = 0
        
        cell.btnHome.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
        cell.btnWork.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
        
        cell.btnHome.addTarget(self, action: #selector(homeClickMethod), for: .touchUpInside)
        cell.btnWork.addTarget(self, action: #selector(workClickMethod), for: .touchUpInside)
        cell.btnSaveContinue.addTarget(self, action: #selector(saveAndContinuClickMethod), for: .touchUpInside)
        
        cell.btnCurrentlocation.addTarget(self, action: #selector(updateCurrentLocationOrNotPopup), for: .touchUpInside)
        
        return cell
    }
    
    @objc func updateCurrentLocationOrNotPopup() {
        
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressTableCell
        
        // print(self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
        /*
        print(cell.txtState.text as Any)
        print(cell.txtCity.text as Any)
        print(cell.txtZipCode.text as Any)
        print(self.strSaveLocality as Any)
        print(self.strSaveLocalAddress as Any)
        print(self.strSaveLocalAddressMini as Any)
        */
        //String(self.strSaveLocality)+" "+String(self.strSaveStateName)
            
        
        // self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini
        
        let addressIs = self.strSaveLocality+", "+self.strSaveLocalAddress+", "+self.strSaveLocalAddressMini
        
        
        let alert = UIAlertController(title: String("Current Location"), message: String(addressIs), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Use Current Location", style: .default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
             
            self.yesUserMyCurrentLocation = "1"
            self.strSaveAddress = addressIs
            self.tbleView.reloadData()
                
        }))
        
        alert.addAction(UIAlertAction(title: "Use Existing address", style: UIAlertAction.Style.default, handler: { action in
            // self.dismiss(animated: true, completion: nil)
         
            self.yesUserMyCurrentLocation = "0"
            self.strSaveAddress = addressIs
            self.tbleView.reloadData()
            
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
            // self.dismiss(animated: true, completion: nil)
         
            
        }))
        self.present(alert, animated: true, completion: nil)
            
            
        
        
    }
    
    @objc func fetchCurrentLocationClickMethod() {
        
        self.iAmHereForLocationPermission()
    }
    
    @objc func saveAndContinuClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPSpecialOrderAddressFourTableCell
        
        if cell.txtName.text == "" {
            let alert = UIAlertController(title: "Error", message: String("Name should not be empty"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtphone.text == "" {
            let alert = UIAlertController(title: "Error", message: String("Phone should not be empty"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }  else {
        /*
        print(cell.txtName.text as Any)
        print(cell.txtphone.text as Any)
        print(cell.txtLandmark.text as Any)
        print(self.strSaveAddress as Any)
        print(self.strSaveWork as Any)
        
        print(cell.txtState.text as Any)
        print(cell.txtCity.text as Any)
        print(cell.txtZipCode.text as Any)
        
            /*
            var strWhatDoYouWant:String!
            var strTotalAmount:String!
            */
            
            /*
             var latitude2:String!
             var longitude2:String!
             var address2:String!
             var state2:String!
             var city2:String!
             var zipcode2:String!
             var landmark2:String!
             var workplace2:String!
             var stateTax2:String!
             
             var namee2:String!
             var phonee2:String!
             */
            
           let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPPaymentScreenId") as? UPPaymentScreen
            
            push!.getestimatePrice2          = getestimatePrice // done
            push!.getTip2                    = getTip // done
            push!.getDeliveryFee2            = String("6") // done
            push!.strWhatDoYouWant2         = strWhatDoYouWant // done
            push!.getTotalPayment2            = strTotalAmount // done
            push!.strFromWhere              = "spcialOffer"
           
            push!.specialOfferScreenImplemented = "yesIAmSpecial"
            
            push!.stateTax2              = taxPriceIs
            
            push!.latitude2     = String(self.strSaveLatitude)
            push!.longitude2    = String(self.strSaveLongitude)
            push!.address2      = String(cell.txtAddress.text!)
            push!.state2        = String(cell.txtState.text!)
            push!.city2         = String(cell.txtCity.text!)
            push!.zipcode2      = String(cell.txtZipCode.text!)
            push!.landmark2     = String(cell.txtLandmark.text!)
            push!.workplace2    = String(strSaveWork)
            push!.stateTax2     = String(taxPriceIs)
            
            push!.namee2        = String(cell.txtName.text!)
            push!.phonee2       = String(cell.txtphone.text!)
            
            self.navigationController?.pushViewController(push!, animated: true)
            */
            
            self.specialOrderWebservice()
            
        }
    }
    
    @objc func specialOrderWebservice() {
         ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPSpecialOrderAddressFourTableCell
        
      let urlString = BASE_URL_EXPRESS_PLUS
  
      var parameters:Dictionary<AnyHashable, Any>!
      if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            /*
       [action] => addfooorder
       [deliveryLat] => 28.5871275
       [deliveryLong] => 77.0605771
       [userId] => 138
       [address] => Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India
       [state] => Delhi
       [city] => Delhi
       [zipcode] => 110075
       [name] => hehd
       [phone] => 9565884859
       [landmark] => vdvs
       [workPlace] => Home
       [whatYouWant] => white sauce pasta
       [amount] => 61.0
       [TIP] => 5.0
       [deliveryFee] => 6
       [totalAmount] => 72.0
       [cardNo] => 12/25
       [transactionId] => tok_1GriptBnk7ygV50q72lKlXKM
       [salesTax] => 0
       */
          
          /*
           print(getTextOneFromTaxScreen2 as Any)
           print(getestimatePrice2 as Any)
           print(getTip2 as Any)
           print(getDeliveryFee2 as Any)
           print(getTexttWOFromTaxScreen2 as Any)
           print(getNotes2 as Any)
           print(getContactOnDelivery2 as Any)
           print(getTotalPayment2 as Any)
           print(latitude2 as Any)
           print(longitude2 as Any)
           print(address2 as Any)
           print(state2 as Any)
           print(city2 as Any)
           print(zipcode2 as Any)
           print(landmark2 as Any)
           print(workplace2 as Any)
           print(stateTax2 as Any)
           */
          
          /*
           [action] => addrequest
           [latitude] => 28.5871357
           [longitude] => 77.0606099
           [userId] => 267
           
           [address] => Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India
           [state] => Delhi
           [city] => Delhi
           [zipcode] => 110075
           [name] => bfbdb
           [phone] => 9598959565
           [landmark] => v
           [workPlace] => Home
           [whatYouWant] => h
           [price] => 2016.0
           [TIP] => 10.0
           [deliveryFee] => 6
           [totalAmount] => 2032.0
           [cardNo] => 12/25
           [transactionId] => tok_1HSitqIudZlr53ucP3y5WNJL
           [salesTax] => 0
           */
        
        
        
        
        
        
        
        // strTransactionValueIs
        
        if taxPriceIs == "0" {
            
            parameters = [
                "action"       : "addrequest",
                "userId"       : person["userId"] as Any,
                "latitude"     : String(self.strSaveLatitude), //
                "longitude"    : String(self.strSaveLongitude), //
                "address"      : person["address"] as! String, //
                "state"        : String(cell.txtState.text!),
                "city"         : String(cell.txtCity.text!),
                "zipcode"      : String(cell.txtZipCode.text!),
                "name"         : String(cell.txtName.text!),
                "phone"        : String(cell.txtphone.text!),
                "landmark"     : String(""),//
                "workPlace"    : String(strSaveWork),
                "whatYouWant"  : String(strWhatDoYouWant), //
                "price"        : String(getestimatePrice), // price
                "TIP"          : String(getTip), //
                "deliveryFee"  : String("6"), //
                "totalAmount"  : String(strTotalAmount), // total amount
                "transactionId": String(""), //
                "salesTax"     : String(taxPriceIs),
                "transactionFee":String(strTransactionValueIs),
                "cardNo"        : String("12/25")
                        
            ]
            
        } else {
            
            
            let salexTax = taxPriceIs
            
            let totalAmountIs = strTotalAmount
            
            print(totalAmountIs as Any)
            
            if salexTax!.contains(".") {
                print("decimal")
                
                let myString = salexTax
                let myFloat = (myString! as NSString).doubleValue
                print(myFloat as Any)
                
                print((myFloat/100))
                
                // print()
                
                // total amount
                let myString2 = totalAmountIs
                let myFloat2 = (myString2! as NSString).doubleValue
                print(myFloat2 as Any)
                
                print((myFloat*myFloat2)/100)
                
                
                let a = (myFloat*myFloat2)/100
                let b = Double(totalAmountIs!)
                let c = a + b!
                
                print(c as Any)
                
                parameters = [
                    "action"       : "addrequest",
                    "userId"       : person["userId"] as Any,
                    "latitude"     : String(self.strSaveLatitude), //
                    "longitude"    : String(self.strSaveLongitude), //
                    "address"      : person["address"] as! String, //
                    "state"        : String(cell.txtState.text!),
                    "city"         : String(cell.txtCity.text!),
                    "zipcode"      : String(cell.txtZipCode.text!),
                    "name"         : String(cell.txtName.text!),
                    "phone"        : String(cell.txtphone.text!),
                    "landmark"     : String(""),//
                    "workPlace"    : String(strSaveWork),
                    "whatYouWant"  : String(strWhatDoYouWant), //
                    "price"        : String(getestimatePrice), // price
                    "TIP"          : String(getTip), //
                    "deliveryFee"  : String("6"), //
                    "totalAmount"  : String(c), // total amount
                    "transactionId": String(""), //
                    "salesTax"     : String(taxPriceIs),
                    "transactionFee":String(strTransactionValueIs),
                    "cardNo"        : String("12/25")
                            
                ]
                
            } else {
                print("no decimal")
                
                let salexTax = taxPriceIs
                
                let totalAmountIs = strTotalAmount
                
                let myString = salexTax
                let myFloat = (myString! as NSString).doubleValue
                print(myFloat as Any)
                
                print((myFloat/100))
                
                // print()
                
                // total amount
                let myString2 = totalAmountIs
                let myFloat2 = (myString2! as NSString).doubleValue
                print(myFloat2 as Any)
                
                print((myFloat*myFloat2)/100)
                
                
                let a = (myFloat*myFloat2)/100
                let b = Double(totalAmountIs!)
                let c = a + b!
                
                print(c as Any)
                
                
                parameters = [
                    "action"       : "addrequest",
                    "userId"       : person["userId"] as Any,
                    "latitude"     : String(self.strSaveLatitude), //
                    "longitude"    : String(self.strSaveLongitude), //
                    "address"      : person["address"] as! String, //
                    "state"        : String(cell.txtState.text!),
                    "city"         : String(cell.txtCity.text!),
                    "zipcode"      : String(cell.txtZipCode.text!),
                    "name"         : String(cell.txtName.text!),
                    "phone"        : String(cell.txtphone.text!),
                    "landmark"     : String(""),//
                    "workPlace"    : String(strSaveWork),
                    "whatYouWant"  : String(strWhatDoYouWant), //
                    "price"        : String(getestimatePrice), // price
                    "TIP"          : String(getTip), //
                    "deliveryFee"  : String("6"), //
                    "totalAmount"  : String(c), // total amount
                    "transactionId": String(""), //
                    "salesTax"     : String(taxPriceIs),
                    "transactionFee":String(strTransactionValueIs),
                    "cardNo"        : String("12/25")
                            
                ]
                
                
            }
        }
        
        
        
          
      }
      
        
      print("parameters-------\(String(describing: parameters))")
                   
      Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
          {
              response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                let alert = UIAlertController(title: String("Success"), message: String("We will notify you when any driver assigned to you."), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                     
                                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPDashboardId") as? UPDashboard
                                    self.navigationController?.pushViewController(push!, animated: true)
                                    
                                }))
                                
                                 
                                
                                self.present(alert, animated: true, completion: nil)
                                
                                
                                 // var dict: Dictionary<AnyHashable, Any>
                                //  dict = JSON["msg"] as! Dictionary<AnyHashable, Any>
                                
                                // var strSuccess : String!
                                // strSuccess = JSON["msg"]as Any as? String
//
//                                  let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessAfterOrderFoodId") as? UPSuccessAfterOrderFood
//                                  self.navigationController?.pushViewController(push!, animated: true)
//
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                          }
      }
  }
    
    @objc func homeClickMethod(_ sender:UIButton) {
        let btnH:UIButton = sender
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPSpecialOrderAddressFourTableCell
        cell.btnWork.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
        cell.btnWork.tag = 0
        self.strSaveWork = "0"
        
        if btnH.tag == 0 {
            btnH.setBackgroundImage(UIImage(named: "checkCustomerContact"), for: .normal)
            self.strSaveAddress = "1"
            
            btnH.tag = 1
        } else if btnH.tag == 1 {
            btnH.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
            self.strSaveAddress = "0"
            
            btnH.tag = 0
        }
    }
    
    @objc func workClickMethod(_ sender:UIButton) {
        let btnW:UIButton = sender
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPSpecialOrderAddressFourTableCell
        cell.btnHome.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
        cell.btnHome.tag = 0
        self.strSaveAddress = "0"
        
        if btnW.tag == 0 {
            btnW.setBackgroundImage(UIImage(named: "checkCustomerContact"), for: .normal)
            self.strSaveWork = "1"
            
            btnW.tag = 1
        } else if btnW.tag == 1 {
            btnW.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
            self.strSaveWork = "0"
            
            btnW.tag = 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TrackYourOrderId") as? TrackYourOrder
//        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 700
    }
    
}

extension UPSpecialOrderAddressFour: UITableViewDelegate {
    
}


