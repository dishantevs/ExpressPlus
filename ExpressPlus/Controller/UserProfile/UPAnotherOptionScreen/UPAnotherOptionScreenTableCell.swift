//
//  UPAnotherOptionScreenTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UPAnotherOptionScreenTableCell: UITableViewCell {

    @IBOutlet weak var txtViewWhatDoYou:UITextView! {
        didSet {
            txtViewWhatDoYou.layer.cornerRadius = 6
            txtViewWhatDoYou.clipsToBounds = true
            txtViewWhatDoYou.layer.borderColor = UIColor.lightGray.cgColor
            txtViewWhatDoYou.layer.borderWidth = 0.8
        }
    }
    
    @IBOutlet weak var txtPrice:UITextField! {
        didSet {
            txtPrice.layer.cornerRadius = 6
            txtPrice.clipsToBounds = true
            txtPrice.layer.borderColor = UIColor.lightGray.cgColor
            txtPrice.layer.borderWidth = 0.8
            txtPrice.setLeftPaddingPoints(20)
            // txtPrice.text = "0"
        }
    }
    
    @IBOutlet weak var txtTip:UITextField! {
        didSet {
            txtTip.layer.cornerRadius = 6
            txtTip.clipsToBounds = true
            txtTip.layer.borderColor = UIColor.lightGray.cgColor
            txtTip.layer.borderWidth = 0.8
            txtTip.setLeftPaddingPoints(20)
            // txtTip.text = "0"
        }
    }
    
    @IBOutlet weak var txtDeliveryFee:UITextField! {
        didSet {
            txtDeliveryFee.layer.cornerRadius = 6
            txtDeliveryFee.clipsToBounds = true
            txtDeliveryFee.layer.borderColor = UIColor.lightGray.cgColor
            txtDeliveryFee.layer.borderWidth = 0.8
            txtDeliveryFee.text = "6"
            txtDeliveryFee.setLeftPaddingPoints(20)
        }
    }
    
    @IBOutlet weak var btnTotalAmount:UIButton! {
        didSet {
            btnTotalAmount.setTitleColor(.white, for: .normal)
            btnTotalAmount.layer.cornerRadius = 6
            btnTotalAmount.clipsToBounds = true
            btnTotalAmount.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
            btnTotalAmount.setTitle("Total Amount : $6", for: .normal)
        }
    }
    
    @IBOutlet weak var lblTotalAmount:UILabel! {
        didSet {
            lblTotalAmount.textColor = .white
            lblTotalAmount.text = "Total Amount : $ 6"// (.white, for: .normal)
            lblTotalAmount.layer.cornerRadius = 6
            lblTotalAmount.clipsToBounds = true
            lblTotalAmount.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
         }
    }
    
    
    
    // UPToGetEstimateTaxAndPrice 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
