//
//  TrackYourOrder.swift
//  ExpressPlus
//
//  Created by Apple on 11/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import MapKit

class customPin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(pinTitle:String, pinSubTitle:String, location:CLLocationCoordinate2D) {
        self.title = pinTitle
        self.subtitle = pinSubTitle
        self.coordinate = location
    }
}

class TrackYourOrder: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate {

    let cellReuseIdentifier = "uPAddressTableCell"
    
    var strFoodOrderId:String!
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    let annotation = MKPointAnnotation()
    let annotation2 = MKPointAnnotation()
    
    // MARK:- DRIVER -
    var driverLatitute:String!
    var driverLongitude:String!
    
    // MARK:- RESTAURANT -
    var restaurantLatitude:String!
    var restaurantLongitude:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "TRACK YOUR ORDER"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    @IBOutlet weak var btnWaitingForOrderSetup:UIButton! {
        didSet {
            btnWaitingForOrderSetup.setTitleColor(.white, for: .normal)
            btnWaitingForOrderSetup.setTitle("WAITING FOR ORDER PICKUP", for: .normal)
            btnWaitingForOrderSetup.setTitleColor(.white, for: .normal)
            btnWaitingForOrderSetup.layer.cornerRadius = 6
            btnWaitingForOrderSetup.clipsToBounds = true
            btnWaitingForOrderSetup.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var mapView:MKMapView!
    
    @IBOutlet weak var lblDeliveryIn:UILabel! {
        didSet {
            
        }
    }
    @IBOutlet weak var lblOrderId:UILabel! {
        didSet {
            lblOrderId.text = ""
        }
    }
    @IBOutlet weak var lblAddress:UILabel! {
        didSet {
            lblAddress.text = ""
        }
    }
    @IBOutlet weak var lblDistance:UILabel! {
        didSet {
            lblDistance.text = ""
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        btnWaitingForOrderSetup.addTarget(self, action: #selector(serviceClickMethod), for: .touchUpInside)
        
        
        self.trackYourOrderWithFullDetails()
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func serviceClickMethod() {
        // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ServicesId") as? Services
        // self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    // MARK:- MENU LIST WEBSERVICE -
    @objc func trackYourOrderWithFullDetails() {
        // self.arrListOfAllMyOrders.removeAllObjects()
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
                  
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        /*
         [action] => foodorderdetails
         [userId] => 99
         [foodorderId] => 51
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
        
        let strMyId:String!
        let x : Int = person["userId"] as! Int
        strMyId = String(x)
        
            parameters = [
                "action"      : "foodorderdetails",
                "foodorderId" : String(strFoodOrderId),
                "userId"      : String(strMyId)
            ]
        }
        print("parameters-------\(String(describing: parameters))")
                      
                      Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                        response in
                  
                              switch(response.result) {
                              case .success(_):
                                 if let data = response.result.value {

                                  let JSON = data as! NSDictionary
                                    // print(JSON as Any)
                                  /*
                                     {
                                         data =     {
                                             EstTime = "15 Minutes";
                                             Mile = "4 Mile";
                                             TIP = "0.75";
                                             address = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
                                             assignDriver = 113;
                                             cardNo = 4242424242424242;
                                             city = Ghaziabad;
                                             couponCode = "";
                                             created = "2020-05-19 17:40:00";
                                             deliveryFee = "";
                                             deliveryLat = "28.6634703";
                                             deliveryLong = "77.3240304";
                                             discount = 0;
                                             driverAVG = 0;
                                             driverContactNumber = 2356898523;
                                             driverId = 113;
                                             driverImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1589802893IMG-20200518-WA0003.jpg";
                                             driverName = "driver evs";
                                             foodDetails =         (
                                                             {
                                                     id = 43;
                                                     name = gagsgs;
                                                     price = "15.00";
                                                     quantity = 1;
                                                     resturentId = 99;
                                                 }
                                             );
                                             foodDetailsNew =         (
                                                             {
                                                     categoryId = "";
                                                     description = "";
                                                     foodName = "";
                                                     foodTag = "";
                                                     foodType = "";
                                                     "image_1" = "";
                                                     "image_2" = "";
                                                     "image_3" = "";
                                                     "image_4" = "";
                                                     "image_5" = "";
                                                     menuId = "";
                                                     price = "";
                                                     quantity = 1;
                                                     resturentId = "";
                                                     specialPrice = "";
                                                 }
                                             );
                                             foodId = "";
                                             foodorderId = 70;
                                             landmark = hotel;
                                             name = purnima;
                                             noContact = "";
                                             phone = 5623852369;
                                             resturentAddress = "Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India";
                                             resturentId = 99;
                                             resturentLatitude = "28.5870717";
                                             resturentLongitude = "77.0605306";
                                             resturentName = "restaurent 2d";
                                             specialNote = "test y";
                                             state = "Uttar Pradesh";
                                             status = 4;
                                             storecity = "";
                                             totalAmount = "15.75";
                                             userAVG = 0;
                                             userId = 114;
                                             userName = kiwi;
                                             whatYouWant = "";
                                             workPlace = Home;
                                             zipcode = 201011;
                                         };
                                         status = success;
                                     }
                                     (lldb)
                                     */
                                  var strSuccess : String!
                                  strSuccess = JSON["status"]as Any as? String
                                  
                                  if strSuccess == String("success") {
                                   print("yes")
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    
                                    var dict: Dictionary<AnyHashable, Any>
                                    dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                    self.lblDeliveryIn.text = (dict["EstTime"] as! String)
                                    
                                    
                                    let x : Int = dict["foodorderId"] as! Int
                                    let myString = String(x)
                                    self.lblOrderId.text    = "Order # "+myString
                                    
                                    self.lblAddress.text = (dict["address"] as! String)
                                    
                                    
                                    // restaurant
                                    self.restaurantLatitude     = (dict["deliveryLat"] as! String)
                                    self.restaurantLongitude    = (dict["deliveryLong"] as! String)
                                      
                                    // driver
                                    self.driverLatitute     = (dict["resturentLatitude"] as! String)
                                    self.driverLongitude    = (dict["resturentLongitude"] as! String)
                                    
                                    
                                    
                                    self.locManager.requestWhenInUseAuthorization()
                                    if CLLocationManager.locationServicesEnabled() {
                                        self.locManager.delegate = self
                                        self.locManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                                        self.locManager.startUpdatingLocation()
                                        print("UPDATE UPDATE")
                                    }
                                    
                                    if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
                                        CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways) {
                                        print("")
                                    }
                                    
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                        }
        }
    }
    
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
                
                            //print("**********************")
                            //print("Long \(manager.location!.coordinate.longitude)")
                            //print("Lati \(manager.location!.coordinate.latitude)")
                            //print("Alt \(manager.location!.altitude)")
                            //print("Speed \(manager.location!.speed)")
                            //print("Accu \(manager.location!.horizontalAccuracy)")
                            //print("**********************")
                
                //print(Double((vendorLatitute as NSString).doubleValue))
                //print(Double((vendorLongitute as NSString).doubleValue))
                
        /*
         // restaurant
         self.restaurantLatitude     = (dict["deliveryLat"] as! String)
         self.restaurantLongitude    = (dict["deliveryLong"] as! String)
           
         // driver
         self.driverLatitute     = (dict["resturentLatitude"] as! String)
         self.driverLongitude    = (dict["resturentLongitude"] as! String)
         */
        
        let restaurantLatitudeDouble    = Double(self.restaurantLatitude)
        let restaurantLongitudeDouble   = Double(self.restaurantLongitude)
        let driverLatitudeDouble        = Double(self.driverLatitute)
        let driverLongitudeDouble       = Double(self.driverLongitude)
        
        let coordinate₀ = CLLocation(latitude: restaurantLatitudeDouble!, longitude: restaurantLongitudeDouble!)
        let coordinate₁ = CLLocation(latitude: driverLatitudeDouble!, longitude: driverLongitudeDouble!)
        
    /************************************** RESTAURANT LATITUTDE AND LINGITUDE  ********************************/
                        // first location
        let sourceLocation = CLLocationCoordinate2D(latitude: restaurantLatitudeDouble!, longitude: restaurantLongitudeDouble!)
    /********************************************************************************************************************/
                                            

    /************************************* DRIVER LATITUTDE AND LINGITUDE ******************************************/
                        // second location
                        let destinationLocation = CLLocationCoordinate2D(latitude: driverLatitudeDouble!, longitude: driverLongitudeDouble!)
    /********************************************************************************************************************/
          
                //print(sourceLocation)
                //print(destinationLocation)
                 
        let sourcePin = customPin(pinTitle: "You", pinSubTitle: "", location: sourceLocation)
        let destinationPin = customPin(pinTitle: "Driver", pinSubTitle: "", location: destinationLocation)

    /***************** REMOVE PREVIUOS ANNOTATION TO GENERATE NEW ANNOTATION *******************************************/
                    self.mapView.removeAnnotations(self.mapView.annotations)
    /********************************************************************************************************************/
                
                        self.mapView.addAnnotation(sourcePin)
                        self.mapView.addAnnotation(destinationPin)
                       
                       let sourcePlaceMark = MKPlacemark(coordinate: sourceLocation)
                       let destinationPlaceMark = MKPlacemark(coordinate: destinationLocation)
                                                   
                       let directionRequest = MKDirections.Request()
                               directionRequest.source = MKMapItem(placemark: sourcePlaceMark)
                               directionRequest.destination = MKMapItem(placemark: destinationPlaceMark)
                               directionRequest.transportType = .automobile
                               
                               let directions = MKDirections(request: directionRequest)
                               directions.calculate { (response, error) in
                                   guard let directionResonse = response else {
                                       if let error = error {
                                           print("we have error getting directions==\(error.localizedDescription)")
                                       }
                                       return
                                   }
                                   
    /***************** REMOVE PREVIUOS POLYLINE TO GENERATE NEW POLYLINE *******************************/
                        let overlays = self.mapView.overlays
                        self.mapView.removeOverlays(overlays)
    /************************************************************************************/
    
                                
    /***************** GET DISTANCE BETWEEN TWO CORDINATES *******************************/
                             
                                let distanceInMeters = coordinate₀.distance(from: coordinate₁)
                                // print(distanceInMeters as Any)
                                
                                // remove decimal
                                let distanceFloat: Double = (distanceInMeters as Any as! Double)
                                self.lblDistance.text = (String(format: "Distance : %.0f Miles away", distanceFloat/1609.344))
                                
    /************************************************************************************/
                                
    /***************** GENERATE NEW POLYLINE *******************************/
                       let route = directionResonse.routes[0]
                       self.mapView.addOverlay(route.polyline, level: .aboveRoads)
                       let rect = route.polyline.boundingMapRect
                       self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
    /************************************************************************************/
                           
                }
                        self.mapView.delegate = self
         
                        self.locManager.stopUpdatingLocation()
                
                // speed = distance / time
             }
    
    
    //MARK:- MapKit delegates -

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        return renderer
    }
    
}
