//
//  Review.swift
//  ExpressPlus
//
//  Created by Apple on 16/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class Review: UIViewController {

    var dictGetDetails:NSDictionary!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "Send Review"
        }
    }
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnStarOne:UIButton!
    @IBOutlet weak var btnStarTwo:UIButton!
    @IBOutlet weak var btnStarThree:UIButton!
    @IBOutlet weak var btnStarFour:UIButton!
    @IBOutlet weak var btnStarFive:UIButton!
    
    @IBOutlet weak var txtView:UITextView! {
        didSet {
            txtView.text = ""
            txtView.backgroundColor = .white
            txtView.layer.cornerRadius = 4
            txtView.clipsToBounds = true
            txtView.layer.borderColor = UIColor.lightGray.cgColor
            txtView.layer.borderWidth = 0.8
        }
    }
    
    @IBOutlet weak var btnSubmitReview:UIButton! {
        didSet {
            btnSubmitReview.setTitle("Submit Review", for: .normal)
            btnSubmitReview.setTitleColor(.white, for: .normal)
            btnSubmitReview.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    var strStarCountCheck:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        btnSubmitReview.addTarget(self, action: #selector(submitReviewClickMethod), for: .touchUpInside)
        
        // by default one
        self.btnStarOne.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.strStarCountCheck = "1"
        
        btnStarOne.addTarget(self, action: #selector(oneStarClickMethod), for: .touchUpInside)
        btnStarTwo.addTarget(self, action: #selector(twoStarClickMethod), for: .touchUpInside)
        btnStarThree.addTarget(self, action: #selector(threeStarClickMethod), for: .touchUpInside)
        btnStarFour.addTarget(self, action: #selector(fourStarClickMethod), for: .touchUpInside)
        btnStarFive.addTarget(self, action: #selector(fiveStarClickMethod), for: .touchUpInside)
        
        print(dictGetDetails as Any)
    }
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @objc func oneStarClickMethod() {
        self.strStarCountCheck = "1"
        self.btnStarOne.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarTwo.setImage(UIImage(named: "star_white"), for: .normal)
        self.btnStarThree.setImage(UIImage(named: "star_white"), for: .normal)
        self.btnStarFour.setImage(UIImage(named: "star_white"), for: .normal)
        self.btnStarFive.setImage(UIImage(named: "star_white"), for: .normal)
    }
    @objc func twoStarClickMethod() {
        self.strStarCountCheck = "2"
        self.btnStarOne.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarTwo.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarThree.setImage(UIImage(named: "star_white"), for: .normal)
        self.btnStarFour.setImage(UIImage(named: "star_white"), for: .normal)
        self.btnStarFive.setImage(UIImage(named: "star_white"), for: .normal)
    }
    @objc func threeStarClickMethod() {
        self.strStarCountCheck = "3"
        self.btnStarOne.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarTwo.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarThree.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarFour.setImage(UIImage(named: "star_white"), for: .normal)
        self.btnStarFive.setImage(UIImage(named: "star_white"), for: .normal)
    }
    @objc func fourStarClickMethod() {
        self.strStarCountCheck = "4"
        self.btnStarOne.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarTwo.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarThree.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarFour.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarFive.setImage(UIImage(named: "star_white"), for: .normal)
    }
    @objc func fiveStarClickMethod() {
        self.strStarCountCheck = "5"
        self.btnStarOne.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarTwo.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarThree.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarFour.setImage(UIImage(named: "star_yellow"), for: .normal)
        self.btnStarFive.setImage(UIImage(named: "star_yellow"), for: .normal)
    }
    
    @objc func submitReviewClickMethod() {
                            
     ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
     /*
             [action] => submitreview
             [reviewTo] => 123
             [reviewFrom] => 138
             [star] => 5.0
             [message] => ok
             */
                   parameters = [
                       "action"              : "submitreview",
                       "reviewTo"              : dictGetDetails["vendorId"] as Any,
                       "reviewFrom"            : person["userId"] as Any,
                       "star"              : String(strStarCountCheck),
                       "message"              : String(txtView.text!)
            ]
        }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                 print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("Success") {
                                print("yes")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                
                                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDJobRequestId") as? PDJobRequest
                                // settingsVCId!.dictGetDetails = dict as NSDictionary
                                self.navigationController?.pushViewController(settingsVCId!, animated: true)
                                // PDJobRequest
                                
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
        }
    
}
