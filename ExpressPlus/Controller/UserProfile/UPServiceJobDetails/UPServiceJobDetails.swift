//
//  PDServiceJobDetails.swift
//  ExpressPlus
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

import MapKit
import CoreLocation

class UPServiceJobDetails: UIViewController,CLLocationManagerDelegate {
    
    let cellReuseIdentifier = "pDServiceJobDetailsTableCell"
    
    var locationManager:CLLocationManager!
    
    var dict: Dictionary<AnyHashable, Any> = [:]
    
    var myDouble1 :Double!
    
    var myDouble2 :Double!
    
    var fromDriverScreen2:String!
    
    var hideReviewString:String!
    
    @IBOutlet weak var lblTransactionFee:UILabel!
    
    var saveTotalPrice:String!
    var saveRegistrationFee:String!
    
    var addTransactionFeeWithTotalAmount:Double!
    
    @IBOutlet weak var btnReview:UIButton! {
        didSet {
            btnReview.layer.cornerRadius = 6
            btnReview.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var mapView:MKMapView!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "SERVICE JOB DETAILS"
        }
    }
    @IBOutlet weak var BelowNavigationBar:UIView! {
        didSet {
            BelowNavigationBar.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var BelowBelowNavigationBar:UIView! {
        didSet {
            BelowBelowNavigationBar.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var imgProfileImage:UIImageView! {
        didSet {
            imgProfileImage.layer.cornerRadius = 35
            imgProfileImage.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var upperMapNavigationBar:UIView! {
        didSet {
            upperMapNavigationBar.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var addressView:UIView! {
        didSet {
            addressView.backgroundColor = .white // NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.separatorStyle = UITableViewCell.SeparatorStyle.none
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnPending:UIButton! {
        didSet {
            
        }
    }
    
    @IBOutlet weak var lblUsername:UILabel! {
        didSet {
            lblUsername.text = ""
        }
    }
    @IBOutlet weak var lblUsernamePhoneNumber:UILabel! {
        didSet {
            lblUsernamePhoneNumber.text = ""
        }
    }
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblCreatedAt:UILabel!
    
    @IBOutlet weak var lblServiceTitle:UILabel!
    @IBOutlet weak var lblServiceAmount:UILabel!
    @IBOutlet weak var lblTotalPrice:UILabel!
    @IBOutlet weak var lblTipAmount:UILabel!
    
    @IBOutlet weak var lblSalesTax:UILabel!
    
    var dictGetAllServiceDetails:NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        print(dictGetAllServiceDetails as Any)
        
        //   self.btnReview.isHidden = true
        
        
        
        /*
         AVGRating = "";
         TIP = 15;
         address = "Sector 10 Dwarka South West Delhi";
         bookingDate = "30th Sep";
         bookingId = 359;
         contactNumber = "";
         created = "2020-09-14 17:59:00";
         email = "";
         fullName = "";
         groupId = 171;
         image = "";
         serviceAmount = 35;
         serviceName = "Baby Sitting";
         servicePrice = 35;
         slote = "5:59 PM-6:58 PM";
         status = 0;
         totalAmount = "35.6";
         userAddress = "";
         userId = "";
         zipCode = "";
         */
        
        /*
         @IBOutlet weak var lblServiceTitle:UILabel!
         @IBOutlet weak var lblTotalPrice:UILabel!
         @IBOutlet weak var lblTipAmount:UILabel!
         */
        
        /*
         
         */
        
        
        // 1 done
         // 2 pending
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            if person["role"] as! String == "Driver" {
                self.btnReview.isHidden = true

                
                self.lblAddress.textColor           = .black
                self.lblAddress.text                = (dictGetAllServiceDetails["address"] as! String)
                self.lblCreatedAt.text              = (dictGetAllServiceDetails["created"] as! String)
                 
                self.lblServiceTitle.text       = (dictGetAllServiceDetails["serviceName"] as! String)
                self.lblServiceAmount.text      = "$ "+(dictGetAllServiceDetails["serviceAmount"] as! String)
                self.lblTipAmount.text          = "$ "+(dictGetAllServiceDetails["TIP"] as! String)
                
                self.lblTotalPrice.text = "$ "+(dictGetAllServiceDetails["totalAmount"] as! String)
                self.lblTotalPrice.textColor = .white
                 
                if fromDriverScreen2 == "yesFromDriverPortal" {
                    btnPending.backgroundColor = NAVIGATION_BACKGROUND_COLOR
                     
                    let x : Int = dictGetAllServiceDetails["status"] as! Int
                    let myString = String(x)
                     
                    if String(myString) == "1" {
                         
                        btnPending.setTitle("Done", for: .normal)
                        self.btnPending.isUserInteractionEnabled = true
                        btnPending.addTarget(self, action: #selector(jobDoneCompleteClickMethod), for: .touchUpInside)
                        self.btnReview.isHidden = true
                         
                    } else if String(myString) == "2" {
                         
                        btnPending.setTitle("COMPLETED", for: .normal)
                        self.btnPending.isUserInteractionEnabled = false
                        self.btnReview.isHidden = true
                        self.btnReview.addTarget(self, action: #selector(reviewButtonClick), for: .touchUpInside)
                         
                    } else {
                        
                        btnPending.setTitle("PENDING", for: .normal)
                        self.btnPending.isUserInteractionEnabled = false
                        self.btnReview.isHidden = true
                        self.btnReview.addTarget(self, action: #selector(reviewButtonClick), for: .touchUpInside)
                        
                    }
                     
                    btnPending.setTitleColor(.white, for: .normal)
                     
                } else {
                     
                    btnPending.backgroundColor = .systemGreen
                    btnPending.setTitleColor(.white, for: .normal)
                     
                }
                
                
                
            } else if person["role"] as! String == "Member" {
                self.btnReview.isHidden = true
                
                
                self.lblAddress.textColor           = .black
                self.lblAddress.text                = (dictGetAllServiceDetails["address"] as! String)
                self.lblCreatedAt.text              = (dictGetAllServiceDetails["created"] as! String)
                 
                self.lblServiceTitle.text       = (dictGetAllServiceDetails["serviceName"] as! String)
                self.lblServiceAmount.text      = "$ "+(dictGetAllServiceDetails["serviceAmount"] as! String)
                self.lblTipAmount.text          = "$ "+(dictGetAllServiceDetails["TIP"] as! String)
                
                
                let aStr = String(format: "%.2f", (dictGetAllServiceDetails["totalAmount"] as! String))
                
                self.lblTotalPrice.text = "$ "+aStr
                self.lblTotalPrice.textColor = .white
                
                print(dictGetAllServiceDetails as Any)
                
                let x : Int = dictGetAllServiceDetails["status"] as! Int
                let myString = String(x)
                
                if myString == "0" {
                    print("yes")
                    
                    self.btnReview.isHidden = true
                    self.btnPending.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                    self.btnPending.setTitle("PENDING", for: .normal)
                    self.btnPending.setTitleColor(.white, for: .normal)
                    
                } else if myString == "2" {
                    print("no")
                    
                    self.btnReview.isHidden = false
                    self.btnPending.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                    self.btnPending.setTitle("COMPLETED", for: .normal)
                    self.btnPending.setTitleColor(.white, for: .normal)
                    self.btnReview.addTarget(self, action: #selector(reviewButtonClick), for: .touchUpInside)
                    
                } else {
                    
                    self.btnReview.isHidden = true
                    self.btnPending.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                    self.btnPending.setTitle("Done", for: .normal)
                    
                }
                
                
                
                
                
                
 
                
                
                
                
                
                
                /*
                if fromDriverScreen2 == "yesFromDriverPortal" {
                    btnPending.backgroundColor = NAVIGATION_BACKGROUND_COLOR
                     
                    let x : Int = dictGetAllServiceDetails["status"] as! Int
                    let myString = String(x)
                     
                    if String(myString) == "1" {
                         
                        btnPending.setTitle("Done", for: .normal)
                        self.btnPending.isUserInteractionEnabled = true
                        btnPending.addTarget(self, action: #selector(jobDoneCompleteClickMethod), for: .touchUpInside)
                        self.btnReview.isHidden = true
                         
                    } else {
                         
                        btnPending.setTitle("SERVICE COMPLETED", for: .normal)
                        self.btnPending.isUserInteractionEnabled = false
                        self.btnReview.isHidden = false
                        self.btnReview.addTarget(self, action: #selector(reviewButtonClick), for: .touchUpInside)
                         
                    }
                     
                    btnPending.setTitleColor(.white, for: .normal)
                     
                } else {
                     
                    btnPending.backgroundColor = .systemGreen
                    btnPending.setTitleColor(.white, for: .normal)
                     
                }
                */
                
                
                
            } else {
                self.btnReview.isHidden = true
            }
            
        } else {
            
        }
        
        
        
        
        
        self.foodOrderListWB()
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func reviewButtonClick() {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReviewId") as? Review
        settingsVCId!.dictGetDetails = dict as NSDictionary
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    
    // MARK:- WEBSERVICE ( SERVICE JOB REQUEST ) -
    @objc func foodOrderListWB() {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
        let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => bookingdetail
      [bookingId] => 233
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
                   parameters = [
                       "action"         : "bookingdetail",
                       "bookingId"      : dictGetAllServiceDetails["bookingId"] as Any
             ]
     // }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                  print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                ERProgressHud.sharedInstance.hide()
                                
                                
                                self.dict = JSON["historyList"] as! Dictionary<AnyHashable, Any>
                                
                                self.lblUsername.text               = (self.dict["userName"] as! String)
                                self.lblUsernamePhoneNumber.text    = "Phone : "+(self.dict["userPhone"] as! String)
                                self.lblSalesTax.text = "$ "+((self.dict["salesTax"] as Any) as! String)
                                
                                self.imgProfileImage.sd_setImage(with: URL(string: (self.dict["vendorImage"] as! String)), placeholderImage: UIImage(named: "user"))
                                
                                
                                
                                /*
                                 self.lblServiceAmount.text      = "$120 "+(dictGetAllServiceDetails["serviceAmount"] as! String)
                                 self.lblTipAmount.text          = "$ "+(dictGetAllServiceDetails["TIP"] as! String)
                                 */
                                
                           // total amount - salex tax - tip
                             
                                /*
                                let myString1 = (self.dictGetAllServiceDetails["totalAmount"] as! String)
                                let myInt1 = Int(myString1)
                                
                                
                                print((self.dict["salesTax"] as Any))
                                
                                let myString2 = ((self.dict["salesTax"] as Any) as! String)
                                let myInt2 = Int(myString2)
                                
                                
                                
                                let myString3 = (self.dictGetAllServiceDetails["TIP"] as! String)
                                let myInt3 = Int(myString3)
                                */
                                
                                // print(myInt1!-myInt2!-myInt3!)
                                
                                // if
                                
                                
                                // if ((self.dict["salesTax"] as Any) as! String).contains(".") {
                                    
                                    // print("decimal")
                                    /*
                                    // 1
                                    let myString = (self.dictGetAllServiceDetails["totalAmount"] as! String)
                                    let myFloat = (myString as NSString).doubleValue
                                    print(myFloat as Any)
                                    
                                    // 2
                                    let myString2 = (self.dictGetAllServiceDetails["TIP"] as! String)
                                    let myFloat2 = (myString2 as NSString).doubleValue
                                    print(myFloat2 as Any)
                                    
                                    // 3
                                    let myString3 = ((self.dict["salesTax"] as Any) as! String)
                                    let myFloat3 = (myString3 as NSString).doubleValue
                                    print(myFloat3 as Any)
                                    
                                    print(myFloat+myFloat2+myFloat3)
                                    let c = myFloat-myFloat2-myFloat3
                                    
                                    print(c as Any)
                                    self.lblTotalPrice.text      = "$ "+String(format: "%.2f", c)
                                    // String(format: "%.2f", currentRatio)
                                    */
                                // } else {
                                    // print("no decimal")
                                    
                                
                                if self.dictGetAllServiceDetails["totalAmount"] is String {
                                                  
                                    print("Yes, it's a String")
                                  
                                    self.lblTotalPrice.text = "$ "+(self.dictGetAllServiceDetails["totalAmount"] as! String)
                                    self.saveTotalPrice = (self.dictGetAllServiceDetails["totalAmount"] as! String)
                                    
                                } else if self.dictGetAllServiceDetails["totalAmount"] is Int {
                                  
                                    print("It is Integer")
                                  
                                    let x2 : Int = (self.dictGetAllServiceDetails["totalAmount"] as! Int)
                                    let myString2 = String(x2)
                                    self.lblTotalPrice.text = "$ "+myString2
                                    self.saveTotalPrice = myString2
                                    
                                } else {
                                //some other check
                                  print("i am ")
                                  
                                    let temp:NSNumber = self.dictGetAllServiceDetails["totalAmount"] as! NSNumber
                                    let tempString = temp.stringValue
                                    self.lblTotalPrice.text = "$ "+tempString
                                    self.saveTotalPrice = tempString
                                    
                                }
                                
                                if self.dict["transactionFee"] is String {
                                                  
                                    print("Yes, it's a String")
                                  
                                    self.lblTransactionFee.text = "$ "+(self.dict["transactionFee"] as! String)
                                    self.saveRegistrationFee = (self.dict["transactionFee"] as! String)
                                    
                                } else if self.dict["transactionFee"] is Int {
                                  
                                    print("It is Integer")
                                  
                                    let x2 : Int = (self.dict["transactionFee"] as! Int)
                                    let myString2 = String(x2)
                                    self.lblTransactionFee.text = "$ "+myString2
                                    self.saveRegistrationFee = myString2
                                    
                                } else {
                                //some other check
                                  print("i am ")
                                  
                                    let temp:NSNumber = self.dict["transactionFee"] as! NSNumber
                                    let tempString = temp.stringValue
                                    self.lblTransactionFee.text = "$ "+tempString
                                    self.saveRegistrationFee = tempString
                                    
                                }
                                
                                
                                // saveTotalPrice
                                // saveRegistrationFee
                                
                                let addTotalAmountWithTransactionFee = Double(self.saveTotalPrice)!+Double(self.saveRegistrationFee)!
                                 
                                // let aStr = String(format: "%.2f", String(addTotalAmountWithTransactionFee))
                                self.lblTotalPrice.text = "$ "+String(addTotalAmountWithTransactionFee)
                                
                                
                                
                                /*
                                    let x41 : Int = (self.dictGetAllServiceDetails["totalAmount"]) as! Int//myInt1!-myInt2!-myInt3!
                                    let myString41 = String(x41)
                                    self.lblTotalPrice.text      = "$ "+myString41
                                    */
                                    
                                // }
                                
                                
                                
                                
                                
                                
                                
                                
           
                                // member
    /* ////////////////////////////////////////////////////////////////////////////////////////////////  */
                   
                if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                if person["role"] as! String == "Member" {
                                
                    // lblTransactionFee
                    // transactionFee
                    
                    
                    
                    
                    
                    
                // let x : Int = self.dict["status"] as! Int
                let myString = self.dict["status"] as! String//String(x)
      
                                
                   // pending status
                   if self.dict["paymentStatus"] is String {
                                                               
                       print("Yes, it's a String")
                       
                       // self.btnPaymentAccess.setTitle((self.dict["paymentStatus"] as! String), for: .normal)
                       
                       if (self.dict["paymentStatus"] as! String) == "0" {
                           
                        self.btnPending.isHidden = false
                        self.btnPending.setTitle("Driver not Accept yet", for: .normal)
                        self.btnReview.isHidden = true
                        
                       } else if (self.dict["paymentStatus"] as! String) == "1" {
                           
                        self.btnPending.isHidden = false
                        self.btnPending.setTitle("Confirm & Pay", for: .normal)
                        self.btnPending.backgroundColor = .systemGreen
                            self.btnPending.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                        self.btnReview.isHidden = true
                        
                       } else if (self.dict["paymentStatus"] as! String) == "2" {
                           
                        self.btnPending.isHidden = false
                        self.btnPending.setTitle("Payment Done", for: .normal)
                        self.btnPending.setTitleColor(.white, for: .normal)
                        self.btnPending.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                        
                        if String(myString) == "2" {
                            
                            self.btnReview.isHidden = false
                            self.btnPending.setTitle("COMPLETED", for: .normal)
                            
                        } else {
                            
                            self.btnReview.isHidden = true
                            
                        }
                        
                        
                        
                        
                       }
                       
                       
                       // 2 = complete
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                   } else if self.dict["paymentStatus"] is Int {
                       
                       print("It is Integer")
                       let x2 : Int = (self.dict["paymentStatus"] as! Int)
                       let myString2 = String(x2)
                       // self.btnPaymentAccess.setTitle((myString2), for: .normal)
                       
                       
                       
                       if myString2 == "0" {
                           
                           self.btnPending.isHidden = false
                           self.btnPending.setTitle("Driver not Accept yet", for: .normal)
                           
                       } else if myString2 == "1" {
                           
                        self.btnPending.isHidden = false
                        self.btnPending.setTitle("Confirm & Pay", for: .normal)
                        self.btnPending.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                        self.btnPending.backgroundColor = .systemGreen
                        
                        self.btnReview.isHidden = true
                        
                       } else if myString2 == "2" {
                           
                        self.btnPending.isHidden = false
                        self.btnPending.setTitle("Payment Done", for: .normal)
                        self.btnPending.setTitleColor(.white, for: .normal)
                        self.btnPending.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                        
                        
                        if String(myString) == "2" {
                            
                            self.btnReview.isHidden = false
                            self.btnPending.setTitle("COMPLETED", for: .normal)
                            
                        } else {
                            
                            self.btnReview.isHidden = true
                            
                        }
                        
                        
                        
                        
                       }
                       
                   } else {
                       
                       print("i am ")
                       let temp:NSNumber = self.dict["paymentStatus"] as! NSNumber
                       let tempString = temp.stringValue
                       // self.btnPaymentAccess.setTitle((tempString), for: .normal)
                       
                       
                       if tempString == "0" {
                           
                           self.btnPending.isHidden = false
                           self.btnPending.setTitle("Driver not Accept yet", for: .normal)
                           
                       } else if tempString == "1" {
                           
                           self.btnPending.isHidden = false
                           self.btnPending.setTitle("Confirm & Pay", for: .normal)
                            self.btnPending.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                        self.btnPending.backgroundColor = .systemGreen
                        
                       } else if tempString == "2" {
                           
                        self.btnPending.isHidden = false
                        self.btnPending.setTitle("Payment Done", for: .normal)
                        self.btnPending.setTitleColor(.white, for: .normal)
                        self.btnPending.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                        
                        if String(myString) == "2" {
                            
                            self.btnReview.isHidden = false
                            self.btnPending.setTitle("COMPLETED", for: .normal)
                            
                        } else {
                            
                            self.btnReview.isHidden = true
                            
                        }
                        
                        
                        
                       }
                       
                       
                   }
                   
                   
                                } // member ends here
                }
                   
                   
   /* ////////////////////////////////////////////////////////////////////////////////////////////////  */
                   
                                               
                  
                                
                                
                                
                                // driver
    /* ////////////////////////////////////////////////////////////////////////////////////////////////  */
                   
                if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                if person["role"] as! String == "Driver" {
                                    
                // let x : Int = self.dict["status"] as! Int
                // let myString = String(x)
      
                    let myString = self.dict["status"] as! String
                                
                   // pending status
                   if self.dict["paymentStatus"] is String {
                                                               
                       print("Yes, it's a String")
                       
                       // self.btnPaymentAccess.setTitle((self.dict["paymentStatus"] as! String), for: .normal)
                       
                       if (self.dict["paymentStatus"] as! String) == "0" {
                           
                        self.btnPending.isHidden = false
                        self.btnPending.setTitle("Service not Accepted", for: .normal)
                        self.btnReview.isHidden = true
                        
                       } else if (self.dict["paymentStatus"] as! String) == "1" {
                           
                        self.btnPending.isHidden = false
                        self.btnPending.setTitle("Pending Payment", for: .normal)
                        self.btnPending.backgroundColor = .systemGreen
                            // self.btnPending.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                        self.btnReview.isHidden = true
                        self.btnPending.isUserInteractionEnabled = false
                        
                       } else if (self.dict["paymentStatus"] as! String) == "2" {
                           
                        self.btnPending.isHidden = false
                        self.btnPending.setTitle("COMPLETED", for: .normal)
                        self.btnPending.setTitleColor(.white, for: .normal)
                        self.btnPending.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                        
                        
                        if String(myString) == "2" {
                            
                            self.btnReview.isHidden = false
                            self.btnPending.setTitle("Service Completed", for: .normal)
                            
                        } else {
                            
                            self.btnReview.isHidden = true
                            self.btnPending.setTitle("Mark as Delivered", for: .normal)
                            self.btnPending.backgroundColor = .systemOrange
                        }
                        
                        
                        
                        
                       }
                       
                       
                       // 2 = complete
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                   } else if self.dict["paymentStatus"] is Int {
                       
                       print("It is Integer1")
                       let x2 : Int = (self.dict["paymentStatus"] as! Int)
                       let myString2 = String(x2)
                       // self.btnPaymentAccess.setTitle((myString2), for: .normal)
                       
                       
                       
                       if myString2 == "0" {
                           
                           self.btnPending.isHidden = false
                           self.btnPending.setTitle("Service not Accepted", for: .normal)
                           
                       } else if myString2 == "1" {
                           
                        self.btnPending.isHidden = false
                        self.btnPending.setTitle("Pending Payment", for: .normal)
                        self.btnPending.isUserInteractionEnabled = false
                        // self.btnPending.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                        self.btnPending.backgroundColor = .systemGreen
                        
                        self.btnReview.isHidden = true
                        
                       } else if myString2 == "2" {
                           
                        self.btnPending.isHidden = false
                        self.btnPending.setTitle("COMPLETED", for: .normal)
                        self.btnPending.setTitleColor(.white, for: .normal)
                        self.btnPending.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                        
                    
                        if String(myString) == "2" {
                            
                            self.btnReview.isHidden = false
                            self.btnPending.setTitle("Service Completed", for: .normal)
                            
                        } else {
                            
                            self.btnReview.isHidden = true
                            self.btnPending.setTitle("Mark as Delivered", for: .normal)
                            self.btnPending.backgroundColor = .systemOrange
                        }
                        
                        
                        
                        
                       }
                       
                   } else {
                       
                       print("i am ")
                       let temp:NSNumber = self.dict["paymentStatus"] as! NSNumber
                       let tempString = temp.stringValue
                       // self.btnPaymentAccess.setTitle((tempString), for: .normal)
                       
                       
                       if tempString == "0" {
                           
                           self.btnPending.isHidden = false
                           self.btnPending.setTitle("Service not Accepted", for: .normal)
                           
                       } else if tempString == "1" {
                           
                           self.btnPending.isHidden = false
                           self.btnPending.setTitle("Pending Payment", for: .normal)
                            // self.btnPending.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                        self.btnPending.backgroundColor = .systemGreen
                        self.btnPending.isUserInteractionEnabled = false
                       } else if tempString == "2" {
                           
                        self.btnPending.isHidden = false
                        self.btnPending.setTitle("COMPLETED", for: .normal)
                        self.btnPending.setTitleColor(.white, for: .normal)
                        self.btnPending.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                        
                        
                        if String(myString) == "2" {
                            
                            self.btnReview.isHidden = false
                            self.btnPending.setTitle("Service Completed", for: .normal)
                            
                        } else {
                            
                            self.btnReview.isHidden = true
                            self.btnPending.setTitle("Mark as Delivered", for: .normal)
                            self.btnPending.backgroundColor = .systemOrange
                        }
                        
                        
                        
                       }
                       
                       
                   }
                   
                   
                                } // driver ends here
                }
                   
                   
   /* ////////////////////////////////////////////////////////////////////////////////////////////////  */
                   
                                               
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                 self.determineCurrentLocation()
                                
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    
    @objc func confirmAndPay() {
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        if person["role"] as! String == "Member" {
            
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPPayemtnScreenForFoodSectionId") as? UPPayemtnScreenForFoodSection
        
       /*
             var paymentFoodOrService:String!
             var servicePaymentAmount:String!
             var servicePaymentTotalAmount:String!
             var servicePaymentType:String!
             var servicePaymentTip:String!
             var servicePaymentBookingId:String!
             */
            
            
            
            // strGetAdminAmount
            if self.dict["AdminAmount"] is String {
                                
                print("Yes, it's a String")
                // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
                // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
                push!.strGetAdminAmount = (self.dict["AdminAmount"] as! String)
                
            } else if self.dict["AdminAmount"] is Int {
                
                print("It is Integer")
                let x2 : Int = (self.dict["AdminAmount"] as! Int)
                let myString2 = String(x2)
                // self.changesBookingIdis = String(myString2)
                // push!.getTotalPriceOfFood = String(myString2)
                push!.strGetAdminAmount = String(myString2)
                
            } else {
                
                print("i am ")
                let temp:NSNumber = self.dict["AdminAmount"] as! NSNumber
                let tempString = temp.stringValue
                // self.changesBookingIdis = String(tempString)
                // push!.getTotalPriceOfFood = String(tempString)
                push!.strGetAdminAmount = String(tempString)
                
            }
            
            
            
            
            
       // TIP
       if self.dict["TIP"] is String {
                           
           print("Yes, it's a String")
           // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
           // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
           push!.servicePaymentTip = (self.dict["TIP"] as! String)
           
       } else if self.dict["TIP"] is Int {
           
           print("It is Integer")
           let x2 : Int = (self.dict["TIP"] as! Int)
           let myString2 = String(x2)
           // self.changesBookingIdis = String(myString2)
           // push!.getTotalPriceOfFood = String(myString2)
           push!.servicePaymentTip = String(myString2)
           
       } else {
           
           print("i am ")
           let temp:NSNumber = self.dict["TIP"] as! NSNumber
           let tempString = temp.stringValue
           // self.changesBookingIdis = String(tempString)
           // push!.getTotalPriceOfFood = String(tempString)
           push!.servicePaymentTip = String(tempString)
           
       }
       
       // service id
       if self.dict["serviceId"] is String {
                           
           print("Yes, it's a String")
           // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
           // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
           push!.servicePaymentType = (self.dict["serviceId"] as! String)
           
       } else if self.dict["serviceId"] is Int {
           
           print("It is Integer")
           let x2 : Int = (self.dict["serviceId"] as! Int)
           let myString2 = String(x2)
           // self.changesBookingIdis = String(myString2)
           // push!.getTotalPriceOfFood = String(myString2)
           push!.servicePaymentType = String(myString2)
           
       } else {
           
           print("i am ")
           let temp:NSNumber = self.dict["serviceId"] as! NSNumber
           let tempString = temp.stringValue
           // self.changesBookingIdis = String(tempString)
           // push!.getTotalPriceOfFood = String(tempString)
           push!.servicePaymentType = String(tempString)
           
       }
       
       
       // service amount
       if self.dict["serviceAmount"] is String {
                           
           print("Yes, it's a String")
           // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
           // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
           push!.servicePaymentAmount = (self.dict["serviceAmount"] as! String)
           
       } else if self.dict["serviceAmount"] is Int {
           
           print("It is Integer")
           let x2 : Int = (self.dict["serviceAmount"] as! Int)
           let myString2 = String(x2)
           // self.changesBookingIdis = String(myString2)
           // push!.getTotalPriceOfFood = String(myString2)
           push!.servicePaymentAmount = String(myString2)
           
       } else {
           
           print("i am ")
           let temp:NSNumber = self.dict["serviceAmount"] as! NSNumber
           let tempString = temp.stringValue
           // self.changesBookingIdis = String(tempString)
           // push!.getTotalPriceOfFood = String(tempString)
           push!.servicePaymentAmount = String(tempString)
           
       }
       
       
       
       
       
       // group id
       if self.dict["groupId"] is String {
                           
           print("Yes, it's a String")
           // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
           // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
           push!.servicePaymentBookingId = (self.dict["groupId"] as! String)
           
       } else if self.dict["groupId"] is Int {
           
           print("It is Integer")
           let x2 : Int = (self.dict["groupId"] as! Int)
           let myString2 = String(x2)
           // self.changesBookingIdis = String(myString2)
           // push!.getTotalPriceOfFood = String(myString2)
           push!.servicePaymentBookingId = String(myString2)
           
       } else {
           
           print("i am ")
           let temp:NSNumber = self.dict["groupId"] as! NSNumber
           let tempString = temp.stringValue
           // self.changesBookingIdis = String(tempString)
           // push!.getTotalPriceOfFood = String(tempString)
           push!.servicePaymentBookingId = String(tempString)
           
       }
       
       
       
       
       
       
       
       
       /*
        if self.dict["bookingId"] is String {
                            
            print("Yes, it's a String")
            // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
            // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
            push!.foodOrderId = (self.dict["bookingId"] as! String)
            
        } else if self.dict["bookingId"] is Int {
            
            print("It is Integer")
            let x2 : Int = (self.dict["bookingId"] as! Int)
            let myString2 = String(x2)
            // self.changesBookingIdis = String(myString2)
            // push!.getTotalPriceOfFood = String(myString2)
            push!.foodOrderId = String(myString2)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = self.dict["bookingId"] as! NSNumber
            let tempString = temp.stringValue
            // self.changesBookingIdis = String(tempString)
            // push!.getTotalPriceOfFood = String(tempString)
            push!.foodOrderId = String(tempString)
            
        }*/
        
        if self.dict["totalAmount"] is String {
                            
            print("Yes, it's a String")
            // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
            
            
            self.addTransactionFeeWithTotalAmount = Double((self.dict["transactionFee"] as! String))!+Double((self.dict["totalAmount"] as! String))!
            
            push!.servicePaymentTotalAmount = String(addTransactionFeeWithTotalAmount)
            
        } else if self.dict["bookingId"] is Int {
            
            print("It is Integer")
            let x2 : Int = (self.dict["totalAmount"] as! Int)
            let myString2 = String(x2)
            // self.changesBookingIdis = String(myString2)
            // push!.servicePaymentTotalAmount = String(myString2)
            
            self.addTransactionFeeWithTotalAmount = Double((self.dict["transactionFee"] as! String))!+Double(myString2)!
            push!.servicePaymentTotalAmount = String(addTransactionFeeWithTotalAmount)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = self.dict["totalAmount"] as! NSNumber
            let tempString = temp.stringValue
            // self.changesBookingIdis = String(tempString)
            // push!.servicePaymentTotalAmount = String(tempString)
            
            self.addTransactionFeeWithTotalAmount = Double((self.dict["transactionFee"] as! String))!+Double(tempString)!
            push!.servicePaymentTotalAmount = String(addTransactionFeeWithTotalAmount)
            
        }
            
        
            push!.paymentFoodOrService = "iAmFromService"
            push!.enableBackOrNot = "yes"
            push!.strAccountNumberIs = (self.dict["driverStripeAccount"] as! String)
            
       /*
        var servicePaymentAmount:String!
        var servicePaymentTotalAmount:String!
        var servicePaymentType:String!
        var servicePaymentTip:String!
        var servicePaymentBookingId:String!
        */
       
       // push!.servicePaymentAmount          = ""
       // push!.servicePaymentTotalAmount     = ""
       // push!.servicePaymentType            = ""
       // push!.servicePaymentTip             = ""
       // push!.servicePaymentBookingId       = ""
       
       
       
       
       // serviceAmount
       // bookingGroupId
       
       
       
       
       self.navigationController?.pushViewController(push!, animated: true)
        
        
    }}

    }
    
    @objc func jobDoneCompleteClickMethod() {
       
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        let urlString = BASE_URL_EXPRESS_PLUS
        
         /*
          [action] => changebookingstatus
          [bookingId] => 253
          [driverId] => 123
          [status] => 2
          */
        
        /*
         [action] => changebookingstatus
         [bookingId] => 321
         [driverId] => 279
         [status] => 2
         */
         
            var parameters:Dictionary<AnyHashable, Any>!
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         
                       parameters = [
                           "action"     : "changebookingstatus",
                           "driverId"   : person["userId"] as Any,
                           "bookingId"  : dictGetAllServiceDetails["bookingId"] as Any,
                           "status"     : "2"
                 ]
          }
                       print("parameters-------\(String(describing: parameters))")
                       
                       Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                           {
                               response in
                   
                               switch(response.result) {
                               case .success(_):
                                  if let data = response.result.value {

                                   let JSON = data as! NSDictionary
                                      print(JSON as Any)
                                   
                                   var strSuccess : String!
                                   strSuccess = JSON["status"]as Any as? String
                                   
                                     // var strSuccessAlert : String!
                                     // strSuccessAlert = JSON["msg"]as Any as? String
                                   
                                   if strSuccess == String("success") {
                                    print("yes")
                                     
                                    ERProgressHud.sharedInstance.hide()
                                    
                                   var strSuccess2 : String!
                                    strSuccess2 = JSON["msg"]as Any as? String
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                        // self.dismiss(animated: true, completion: nil)
                                        self.navigationController?.popViewController(animated: true)
                                        
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                   }
                                   else {
                                    print("no")
                                     ERProgressHud.sharedInstance.hide()
                                    
                                    
                                   }
                               }

                               case .failure(_):
                                   print("Error message:\(String(describing: response.result.error))")
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   
                                   let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                   
                                   let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                           UIAlertAction in
                                           NSLog("OK Pressed")
                                       }
                                   
                                   alertController.addAction(okAction)
                                   
                                   self.present(alertController, animated: true, completion: nil)
                                   
                                   break
                                }
                           }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create and Add MapView to our main view
        // createMapView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    func createMapView() {
        // mapView = MKMapView()
        
        // let leftMargin:CGFloat = 10
        // let topMargin:CGFloat = 60
        // let mapWidth:CGFloat = view.frame.size.width-20
        // let mapHeight:CGFloat = 300
        
        // mapView.frame = CGRectMake(leftMargin, topMargin, mapWidth, mapHeight)
        // mapView.frame = CGRect(x: leftMargin, y: topMargin, width: mapWidth, height: mapHeight)
        
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        // mapView.center = view.center
        // view.addSubview(mapView)
        
        
        
        
    }
    func determineCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            //locationManager.startUpdatingHeading()
            locationManager.startUpdatingLocation()
        }
    }
    // private func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.dict["latidude"] as! String == "" {
            myDouble1 = Double("0.0")
        } else {
            myDouble1 = Double(self.dict["latidude"] as! String)
        }
        
        if self.dict["longitude"] as! String == "" {
            myDouble2 = Double("0.0")
        } else {
            myDouble2 = Double(self.dict["longitude"] as! String)
        }
        
        let london = MKPointAnnotation()
        london.title = (self.dict["vendorAddress"] as! String)
        london.coordinate = CLLocationCoordinate2D(latitude: myDouble1!, longitude: myDouble2!)
        mapView.addAnnotation(london)
        
        /*
        let region = MKCoordinateRegion(center: view.annotation!.coordinate, span: mapView.region.span)
            mapView.setRegion(region, animated: true)
        mapView.setRegion(region, animated: true)
        */
                   
        /*
        let userLocation:CLLocation = locations[0] as CLLocation
        
        if self.dict["latidude"] as! String == "" {
            myDouble1 = Double("0.0")
        } else {
            myDouble1 = Double(self.dict["latidude"] as! String)
        }
        
        if self.dict["longitude"] as! String == "" {
            myDouble2 = Double("0.0")
        } else {
            myDouble2 = Double(self.dict["longitude"] as! String)
        }
    
        print(myDouble1 as Any)
        print(myDouble2 as Any)
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: myDouble1!, longitudeDelta: myDouble2!))
        
        mapView.setRegion(region, animated: true)
        
        // Drop a pin at user's Current Location
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(myDouble1!, myDouble2!)
        // myAnnotation.coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
        myAnnotation.title = (self.dict["userName"] as! String)
        mapView.addAnnotation(myAnnotation)
        
        
        locationManager.stopUpdatingLocation()
        */
    }
    
    private func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error \(error)")
    }
    
}


extension UPServiceJobDetails: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PDServiceJobDetailsTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! PDServiceJobDetailsTableCell
        
        cell.backgroundColor = .clear
        
        return cell
    }

func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView .deselectRow(at: indexPath, animated: true)
    
    if indexPath.row == 1 {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDInProgressId") as? PDInProgress
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    } else {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDServiceScreenId") as? PDServiceScreen
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
}
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
}


extension UPServiceJobDetails: UITableViewDelegate {
    
}
