//
//  UPTrackToGetRequest.swift
//  ExpressPlus
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UPTrackToGetRequest: UIViewController {

    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "TRACK TO GET REQUEST"
        }
    }
    @IBOutlet weak var btnTrackYourOrder:UIButton! {
        didSet {
            btnTrackYourOrder.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnTrackYourOrder.setTitleColor(.white, for: .normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}
