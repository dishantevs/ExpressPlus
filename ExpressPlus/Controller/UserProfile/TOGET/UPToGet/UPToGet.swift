//
//  UPToGet.swift
//  ExpressPlus
//
//  Created by Apple on 27/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UPToGet: UIViewController {

    
    
    @IBOutlet weak var txtViewOne:UITextView! {
        didSet {
            txtViewOne.layer.cornerRadius = 6
            txtViewOne.clipsToBounds = true
            txtViewOne.backgroundColor = .white
            txtViewOne.text = ""
        }
    }
    
    @IBOutlet weak var txtViewTwo:UITextView! {
        didSet {
            txtViewTwo.layer.cornerRadius = 6
            txtViewTwo.clipsToBounds = true
            txtViewTwo.backgroundColor = .white
            txtViewTwo.text = ""
        }
    }
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnNext:UIButton! {
        didSet {
            btnNext.setTitleColor(.white, for: .normal)
            btnNext.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // self.btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        self.btnNext.addTarget(self, action: #selector(nextClickMethod), for: .touchUpInside)
        
        if let myLoadedString = UserDefaults.standard.string(forKey: "keySetToBackOrMenu") {
            print(myLoadedString)
            
            if myLoadedString == "backOrMenu" {
                // menu
                self.btnBack.setImage(UIImage(named: "sMenu"), for: .normal)
                self.sideBarMenuClick()
            } else {
                // back
                self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
            }
        } else {
            // back
            self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.txtViewOne.text = ""
        self.txtViewTwo.text = ""
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func sideBarMenuClick() {
        
        UserDefaults.standard.set("", forKey: "keySetToBackOrMenu")
        UserDefaults.standard.set(nil, forKey: "keySetToBackOrMenu")
        
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    @objc func nextClickMethod() {
        
        if txtViewOne.text == "" {
            let alert = UIAlertController(title: "Error", message: String("What would you like us to get not Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else if txtViewOne.text == "" {
            let alert = UIAlertController(title: "Error", message: String("What store should not be Empty"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPDisclouserTwoId") as? UPDisclouserTwo
            push!.textOne = String(txtViewOne.text!)
            push!.textTwo = String(txtViewTwo.text!)
            self.navigationController?.pushViewController(push!, animated: true)
        }
    }
    
}
