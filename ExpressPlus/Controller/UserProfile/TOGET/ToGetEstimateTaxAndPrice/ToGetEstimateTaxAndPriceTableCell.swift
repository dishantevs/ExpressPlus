//
//  ToGetEstimateTaxAndPriceTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 27/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ToGetEstimateTaxAndPriceTableCell: UITableViewCell {

    @IBOutlet weak var txtEnterPrice:UITextField! {
        didSet {
            txtEnterPrice.layer.cornerRadius = 6
            txtEnterPrice.clipsToBounds = true
            txtEnterPrice.setLeftPaddingPoints(20)
            // txtEnterPrice.text = "0"
            // txtEnterPrice.backgroundColor = UIColor.init(red: 199.0/2255.0, green: 199.0/255.0, blue: 199.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var txtTipAmount:UITextField! {
        didSet {
            txtTipAmount.layer.cornerRadius = 6
            txtTipAmount.clipsToBounds = true
            txtTipAmount.setLeftPaddingPoints(20)
            // txtTipAmount.text = "0"
            // txtTipAmount.backgroundColor = UIColor.init(red: 199.0/2255.0, green: 199.0/255.0, blue: 199.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var txtDeliveryFees:UITextField! {
        didSet {
           txtDeliveryFees.layer.cornerRadius = 6
            txtDeliveryFees.clipsToBounds = true
            txtDeliveryFees.text = "6"
            txtDeliveryFees.isUserInteractionEnabled = false
            txtDeliveryFees.setLeftPaddingPoints(20)
            // txtDeliveryFees.backgroundColor = UIColor.init(red: 199.0/2255.0, green: 199.0/255.0, blue: 199.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var txtViewNotes:UITextView! {
        didSet {
           txtViewNotes.layer.cornerRadius = 6
            txtViewNotes.clipsToBounds = true
            // txtViewNotes.backgroundColor = UIColor.init(red: 199.0/2255.0, green: 199.0/255.0, blue: 199.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var btnCheckUncheck:UIButton!
    
    @IBOutlet weak var lblTotalAmount:UILabel! {
        didSet {
            lblTotalAmount.layer.cornerRadius = 6
            lblTotalAmount.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
