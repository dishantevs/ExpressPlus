//
//  UPToGetEstimateTaxAndPrice.swift
//  ExpressPlus
//
//  Created by Apple on 27/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UPToGetEstimateTaxAndPrice: UIViewController, UITextFieldDelegate {

    let cellReuseIdentifier = "toGetEstimateTaxAndPriceTableCell"
    
    var getTextOneFromDisclouser:String!
    var getTextTwoFromDisclouser:String!
    
    var strContactOrNot:String!
    
    var strSaveTotalAmount:String!
    
    var myInt1:Int!
    var myInt2:Int!
    var myInt3:Int!
    
    var toGetSaveTranFeeIs:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "TO GET"
        }
    }
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    // @IBOutlet weak var btnSubmitRequest:UIButton!
    
    @IBOutlet weak var btnSubmitRequest:UIButton! {
        didSet {
            btnSubmitRequest.setTitleColor(.white, for: .normal)
            btnSubmitRequest.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        self.btnSubmitRequest.addTarget(self, action: #selector(validationIs), for: .touchUpInside)
        
        self.strContactOrNot = "3"
        self.strSaveTotalAmount = "0"
        // print(getTextOneFromDisclouser as Any)
        // print(getTextTwoFromDisclouser as Any)
        
        // self.btnCheckUncheck.addTarget(self, action: #selector(checkUncheckClickMethod), for: .touchUpInside)
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false

        view.addGestureRecognizer(tap)
    }
    
    
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func validationIs() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! ToGetEstimateTaxAndPriceTableCell
        
        if String(cell.txtEnterPrice.text!) == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Price should not be Empty or Zero."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if String(cell.txtTipAmount.text!) == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Tip should not be Empty or Zero."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if String(cell.txtViewNotes.text!) == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Notes should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            self.submitRequestClickMethod()
        }
    }
    @objc func submitRequestClickMethod() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! ToGetEstimateTaxAndPriceTableCell
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPAddressThreeId") as? UPAddressThree
        push!.getTextOneFromTaxScreen   = getTextOneFromDisclouser
        push!.getTexttWOFromTaxScreen   = getTextTwoFromDisclouser
        push!.getestimatePrice          = String(cell.txtEnterPrice.text!)
        push!.getTip                    = String(cell.txtTipAmount.text!)
        push!.getDeliveryFee            = String(cell.txtDeliveryFees.text!)
        push!.getNotes                  = String(cell.txtViewNotes.text!)
        push!.getContactOnDelivery      = String(self.strContactOrNot)
        push!.getTotalPayment           = String(self.strSaveTotalAmount)
        push!.transactionFeeValueIs = toGetSaveTranFeeIs
        // print(cell.txtEnterPrice.text! as Any)
        /*
         var namee:String!
         var phonee:String!
         var getTextOneFromTaxScreen:String!
         var getTexttWOFromTaxScreen:String!
         var getestimatePrice:String!
         var getTip:String!
         var getDeliveryFee:String!
         var getNotes:String!
         var getContactOnDelivery:String!
         */
        
        self.navigationController?.pushViewController(push!, animated: true)
    }
}


//MARK:- TABLE VIEW -
extension UPToGetEstimateTaxAndPrice: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ToGetEstimateTaxAndPriceTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! ToGetEstimateTaxAndPriceTableCell
        
        cell.backgroundColor = .white
       
        // cell.lblTitle.text = titleArray[indexPath.row]
       
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.txtEnterPrice.delegate = self
        cell.txtTipAmount.delegate = self
        
        cell.txtEnterPrice.addTarget(self, action: #selector(UPToGetEstimateTaxAndPrice.textFieldDidChange(_:)), for: .editingChanged)
        cell.txtTipAmount.addTarget(self, action: #selector(UPToGetEstimateTaxAndPrice.textFieldDidChange(_:)), for: .editingChanged)
        
        cell.btnCheckUncheck.tag = 0//indexPath.row
        cell.btnCheckUncheck.addTarget(self, action: #selector(checkUncheckClickMethod), for: .touchUpInside)
        
        return cell
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! ToGetEstimateTaxAndPriceTableCell
        
        let myString1 = String(cell.txtEnterPrice.text!)
        if myString1 == "" {
            myInt1 = Int(0)
        } else {
            myInt1 = Int(myString1)
        }
        
        
        let myString2 = String("6")
         myInt2 = Int(myString2)
        
        let myString3 = String(cell.txtTipAmount.text!)
        if myString3 == "" {
            myInt3 = Int(0)
        } else {
            myInt3 = Int(myString3)
        }
        // myInt3 = Int(myString3)
        
        var ans1 = Int();
        ans1 = myInt1!+myInt2!+myInt3!
        
        let x2 : Int = ans1
        let myStringFinal = String(x2)
        self.strSaveTotalAmount = String(myStringFinal)
        
        cell.lblTotalAmount.text = "Total Amount : $ "+String(myStringFinal)
        
        self.lblTransactionFee(strTotalAmount: String(myStringFinal))
    }
    
    // MARK:- TRANSACTION FEE -
    @objc func lblTransactionFee(strTotalAmount:String) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! ToGetEstimateTaxAndPriceTableCell
        
        
        print(strTotalAmount as Any)
        
        let convertTotalPriceToDouble = Double(strTotalAmount)!
        
        //* calculate total price
        //* formula = totalPrice *5.8%
        
        //* 0.058
        //* why 0.058 bcause it get value from 5.8%
        //* 2.9% = 0.058
        let multiplyTractactionFeeWithTotalAmount = convertTotalPriceToDouble * 0.058
        // print(multiplyTractactionFeeWithTotalAmount as Any)
        
        //* convert multiple double values into two digits
        // print(String(format:"%.02f", multiplyTractactionFeeWithTotalAmount))
        
        let getValueAfterMultiplyByFivePointEightPercentage = String(format:"%.02f", multiplyTractactionFeeWithTotalAmount)
        // print(getValueAfterMultiplyByFivePointEightPercentage as Any)
        
        //* now add .30 with result
        let cents = 0.60
        
        let addCentsWithMultiplicationResult = Double(getValueAfterMultiplyByFivePointEightPercentage)!+cents
        // print(addCentsWithMultiplicationResult as Any)
        
        // round figure of result
        let roundFigureOfTotalCalulation = String(format:"%.02f", addCentsWithMultiplicationResult)
        // print(roundFigureOfTotalCalulation as Any)
        
        //* final price for Transaction Fee
        let transactionFeeIs = roundFigureOfTotalCalulation
        
        // cell.lblTransactionFee.text = "$ "+String(transactionFeeIs)
        
        self.toGetSaveTranFeeIs = roundFigureOfTotalCalulation
        
        //* add transaction fee with total amount
        let totalAmountAfterGetTransactionFee = Double(strTotalAmount)!+Double(transactionFeeIs)!
        cell.lblTotalAmount.text = "Transaction Fee : $ "+roundFigureOfTotalCalulation+"\nTotal Amount : $ "+String(totalAmountAfterGetTransactionFee)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    
    
    @objc func checkUncheckClickMethod(_ sender:UIButton) {
        let brn:UIButton = sender
        
        if brn.tag == 0 {
            brn.setImage(UIImage(named: "checkCustomerContact"), for: .normal)
            self.strContactOrNot = "1"
            brn.tag = 1
        } else if brn.tag == 1 {
            brn.setImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
            self.strContactOrNot = "0"
            brn.tag = 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPRestaurantProfileId") as? UPRestaurantProfile
        // self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1200
    }
    
}

extension UPToGetEstimateTaxAndPrice: UITableViewDelegate {
    
}
