//
//  UPAddressThree.swift
//  ExpressPlus
//
//  Created by Apple on 04/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
// MARK:- LOCATION -
import CoreLocation

import Alamofire

class UPAddressThree: UIViewController, CLLocationManagerDelegate {

    var getTextOneFromTaxScreen:String!
    var getTexttWOFromTaxScreen:String!
    var getestimatePrice:String!
    var getTip:String!
    var getDeliveryFee:String!
    var getNotes:String!
    var getContactOnDelivery:String!
    var getTotalPayment:String!
    
     
    
    let cellReuseIdentifier = "uPAddressThreeTableCell"
       
       let locationManager = CLLocationManager()
       
       // MARK:- SAVE LOCATION STRING -
       var strSaveLatitude:String!
       var strSaveLongitude:String!
       var strSaveCountryName:String!
       var strSaveLocalAddress:String!
       var strSaveLocality:String!
       var strSaveLocalAddressMini:String!
       var strSaveStateName:String!
       var strSaveZipcodeName:String!
       
       // MARK:- ADDRESS STRING -
       var strSaveAddress:String!
       var strSaveWork:String!
       
       // MARK:- GET ALL VALUE FROM CART -
       var strSaveContactOnDelivery2:String!
       var strSaveSpecialNotes2:String!
       var mArrSaveMutable2:NSMutableArray = []
       var getTotalAmountOfFood:String!
       var tipAmountIs:String!
       
       var strHW:String!
       
    var taxPriceIs:String!
    
    var transactionFeeValueIs:String!
    
       @IBOutlet weak var navigationBar:UIView! {
           didSet {
               navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
           }
       }
       
       @IBOutlet weak var lblNavigationTitle:UILabel! {
           didSet {
               lblNavigationTitle.text = "ADDRESS"
           }
       }
       
       @IBOutlet weak var btnBack:UIButton! {
           didSet {
               btnBack.setTitle("|||", for: .normal)
           }
       }
       
       @IBOutlet weak var tbleView:UITableView! {
           didSet {
               
           }
       }
       
    var yesUserMyCurrentLocation:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
            
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
            
//             print(strSaveContactOnDelivery as Any)
//             print(strSaveSpecialNotes as Any)
//             print(mArrSaveMutable as Any)
         
        print(getestimatePrice as Any)
        
        self.yesUserMyCurrentLocation = "0"
        
        self.strSaveAddress = "0"
        self.strSaveWork = "0"
        self.taxPriceIs = "0"
        
            // let indexPath = IndexPath.init(row: 0, section: 0)
            // let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressTableCell
    //         cell.btnSaveContinue.addTarget(self, action: #selector(saveAndContinueClick), for: .touchUpInside)
            // cell.txtAddress.text = ""
            
            self.iAmHereForLocationPermission()
        }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    // MARK:- GET CUSTOMER LOCATION
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            // print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            // print("locality ==> "+locality as Any) // sector 10 dwarka
            
            // print(self.strSaveCountryName as Any) // india
             print("state is =====>"+self.strSaveStateName as Any) // new delhi
            // print(self.strSaveZipcodeName as Any) // 110075
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            
            
            self.findMyStateTaxWB()
        }
    }
    
    // MARK:- WEBSERVICE ( FIND STATE ) -
    @objc func findMyStateTaxWB() {
          ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
          let urlString = BASE_URL_EXPRESS_PLUS
    
          var parameters:Dictionary<AnyHashable, Any>!
          // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
              
                     parameters = [
                         "action"       : "statefind",
                         "stateName"    : String(self.strSaveStateName)
              ]
          // }
                     print("parameters-------\(String(describing: parameters))")
                     
                     Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                         {
                             response in
                 
                             switch(response.result) {
                             case .success(_):
                                if let data = response.result.value {

                                 let JSON = data as! NSDictionary
                                  print(JSON as Any)
                                 
                                 var strSuccess : String!
                                 strSuccess = JSON["status"]as Any as? String
                                 
                                    /*
                                     name = Delhi;
                                     price = 0;
                                     status = success;
                                     */
                                    
                                    
                                    
                                   // var strSuccessAlert : String!
                                   // strSuccessAlert = JSON["msg"]as Any as? String
                                 
                                 if strSuccess == String("success") {
                                  print("yes")
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   
                                   // var dict: Dictionary<AnyHashable, Any>
                                  //  dict = JSON["msg"] as! Dictionary<AnyHashable, Any>
                                  
                                  // var strSuccess : String!
                                  // strSuccess = JSON["msg"]as Any as? String
                                
                                    
                                    // self.taxPriceIs = (JSON["price"] as! String)
                                    
                                    
                                    
                                    
                                    
                                    
                                    if JSON["price"] is String {
                                                      
                                        print("Yes, it's a String")
                                      
                                        self.taxPriceIs  = (JSON["price"] as! String)
                                        
                                        // self.finalConfirmAndPayment(strSalesTaxIs: (JSON["price"] as! String), strFinalAmountpayIss: strFinalAmountpayIs)
                                        
                                    } else if JSON["price"] is Int {
                                      
                                        print("It is Integer")
                                      
                                        let x2 : Int = (JSON["price"] as! Int)
                                        let myString2 = String(x2)
                                        self.taxPriceIs = myString2
                                        
                                        // self.finalConfirmAndPayment(strSalesTaxIs: myString2, strFinalAmountpayIss: strFinalAmountpayIs)
                                      
                                    } else {
                                    //some other check
                                      print("i am ")
                                      
                                        let temp:NSNumber = JSON["price"] as! NSNumber
                                        let tempString = temp.stringValue
                                        self.taxPriceIs = tempString
                                        
                                        // self.finalConfirmAndPayment(strSalesTaxIs: tempString, strFinalAmountpayIss: strFinalAmountpayIs)
                                      
                                    }
                                    
                                    
                                    
                                    
                                    
                                    self.tbleView.delegate = self
                                    self.tbleView.dataSource = self
                                    self.tbleView.reloadData()
                                   
                                 }
                                 else {
                                  print("no")
                                   ERProgressHud.sharedInstance.hide()
                                    
                                    self.tbleView.delegate = self
                                    self.tbleView.dataSource = self
                                    self.tbleView.reloadData()
                                 }
                             }

                             case .failure(_):
                                 print("Error message:\(String(describing: response.result.error))")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 self.tbleView.delegate = self
                                 self.tbleView.dataSource = self
                                 self.tbleView.reloadData()
                                 
                                 /*
                                 let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                 
                                 let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                         UIAlertAction in
                                         NSLog("OK Pressed")
                                     }
                                 
                                 alertController.addAction(okAction)
                                 
                                 self.present(alertController, animated: true, completion: nil)
                                 */
                                 break
                              }
                         }
          }
    
    
}

//MARK:- TABLE VIEW -
extension UPAddressThree: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UPAddressThreeTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UPAddressThreeTableCell
        
        cell.backgroundColor    = .white
           
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        // cell.txtAddress.text    = String(self.strSaveLocality)+" "+String(self.strSaveStateName)
        
        if self.yesUserMyCurrentLocation == "1" {
            
            cell.txtAddress.text    = self.strSaveAddress
            
        } else {
            
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                cell.txtAddress.text    = (person["address"] as! String) //String(self.strSaveLocality)+" "+String(self.strSaveStateName)
            }
            
        }
        
        cell.txtState.text      = String(self.strSaveLocalAddress)
        cell.txtCity.text       = String(self.strSaveLocalAddressMini)
        cell.txtZipCode.text    = String(self.strSaveZipcodeName)
        
        cell.btnHome.tag = 0
        cell.btnWork.tag = 0
        
        cell.btnHome.setBackgroundImage(UIImage(named: "checkCustomerContact"), for: .normal)
        cell.btnWork.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
        
        cell.btnHome.addTarget(self, action: #selector(homeClickMethod), for: .touchUpInside)
        cell.btnWork.addTarget(self, action: #selector(workClickMethod), for: .touchUpInside)
        cell.btnSaveContinue.addTarget(self, action: #selector(saveAndContinuClickMethod), for: .touchUpInside)
        
        cell.btnUpdateLocationPopup.addTarget(self, action: #selector(updateCurrentLocationOrNotPopup), for: .touchUpInside)
        
        return cell
    }
    
    @objc func updateCurrentLocationOrNotPopup() {
        
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressTableCell
        
        // print(self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
        /*
        print(cell.txtState.text as Any)
        print(cell.txtCity.text as Any)
        print(cell.txtZipCode.text as Any)
        print(self.strSaveLocality as Any)
        print(self.strSaveLocalAddress as Any)
        print(self.strSaveLocalAddressMini as Any)
        */
        //String(self.strSaveLocality)+" "+String(self.strSaveStateName)
            
        
        // self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini
        
        let addressIs = self.strSaveLocality+", "+self.strSaveLocalAddress+", "+self.strSaveLocalAddressMini
        
        
        let alert = UIAlertController(title: String("Current Location"), message: String(addressIs), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Use Current Location", style: .default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
             
            self.yesUserMyCurrentLocation = "1"
            self.strSaveAddress = addressIs
            // self.tbleView.reloadData()
            self.iAmHereForLocationPermission()
        }))
        
        alert.addAction(UIAlertAction(title: "Use Existing address", style: UIAlertAction.Style.default, handler: { action in
            // self.dismiss(animated: true, completion: nil)
         
            self.yesUserMyCurrentLocation = "0"
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                
                self.strSaveLatitude = (person["latitude"] as! String) //String(doubleStringLat)
                self.strSaveLongitude = (person["longitude"] as! String) // String(doubleStringLong)
                self.strSaveAddress = (person["address"] as! String)
            }
            self.tbleView.reloadData()
            
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
            // self.dismiss(animated: true, completion: nil)
         
            
        }))
        self.present(alert, animated: true, completion: nil)
            
            
        
        
    }
    
    @objc func saveAndContinuClickMethod() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressThreeTableCell
        
        if cell.txtName.text == "" {
            let alert = UIAlertController(title: "Error", message: String("Name should not be empty"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtphone.text == "" {
            let alert = UIAlertController(title: "Error", message: String("Phone should not be empty"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }  else {
        
            print(cell.txtName.text as Any)
            print(cell.txtphone.text as Any)
        // print(cell.txtLandmark.text as Any)
            print(self.strSaveAddress as Any)
            print(self.strSaveWork as Any)
        
            print(cell.txtState.text as Any)
            print(cell.txtCity.text as Any)
            print(cell.txtZipCode.text as Any)
        
            self.fullAndFinalBW()
            
            
        }
    }
    
    @objc func fullAndFinalBW() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressThreeTableCell
        
          let urlString = BASE_URL_EXPRESS_PLUS
    
          var parameters:Dictionary<AnyHashable, Any>!
           if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
              
            
            // eprice
            let myString1 = String(getestimatePrice)
            let myInt1 = Int(myString1)
            
            // tip
            let myString2 = String(getTip)
            let myInt2 = Int(myString2)
            
            // delivery
            let myString3 = String(getDeliveryFee)
            let myInt3 = Int(myString3)
            let sumOfAllValue = myInt1!+myInt2!+myInt3!
            
            let x : Int = sumOfAllValue
            let andTheTotalPriceIs = String(x)
            
            
            
            
            
            
            // taxPriceIs
            //
            
            if taxPriceIs == "0" {
                
                parameters = [
                    "action"       : "togetrequest",
                    "userId"       : person["userId"] as Any,
                    "latitude"     : String(self.strSaveLatitude),// "28.5823",// String(self.strSaveLatitude),
                    "longitude"    : String(self.strSaveLongitude),// "77.0500",// String(self.strSaveLongitude),
                    "address"      : String(cell.txtAddress.text!),
                    "state"        : String(cell.txtState.text!),
                    "city"         : String(cell.txtCity.text!),
                    "zipcode"      : String(cell.txtZipCode.text!),
                    "name"         : String(cell.txtName.text!),
                    "phone"        : String(cell.txtphone.text!),
                    "landmark"     : String(""),
                    "workPlace"    : String(strSaveWork),
                    "notes"        : String(getNotes),
                    "EPrice"       : String(andTheTotalPriceIs),
                    "TIP"          : String(getTip),
                    "deliveryFee"  : String(getDeliveryFee),
                    "wahtUwant"    : String(getTextOneFromTaxScreen),
                    "StoreCity"    : String(getTexttWOFromTaxScreen),
                    "noContac"     : String(getContactOnDelivery),
                    "transactionId": String(""),
                    "transactionFee":String(transactionFeeValueIs),
                    "salesTax"     : String(taxPriceIs),
                ]
                
            } else {
                
                
                
                
                let salexTax = taxPriceIs
                
                let totalAmountIs = andTheTotalPriceIs
                
                print(totalAmountIs as Any)
                
                if salexTax!.contains(".") {
                    print("decimal")
                    
                    let myString = salexTax
                    let myFloat = (myString! as NSString).doubleValue
                    print(myFloat as Any)
                    
                    print((myFloat/100))
                    
                    // print()
                    
                    // total amount
                    let myString2 = totalAmountIs
                    let myFloat2 = (myString2 as NSString).doubleValue
                    print(myFloat2 as Any)
                    
                    print((myFloat*myFloat2)/100)
                    
                    
                    let a = (myFloat*myFloat2)/100
                    let b = Double(totalAmountIs)
                    let c = a + b!
                    
                    print(c as Any)
                    
                    
                    
                    
                    parameters = [
                        "action"       : "togetrequest",
                        "userId"       : person["userId"] as Any,
                        "latitude"     : String(self.strSaveLatitude),
                        "longitude"    : String(self.strSaveLongitude),
                        "address"      : String(cell.txtAddress.text!),
                        "state"        : String(cell.txtState.text!),
                        "city"         : String(cell.txtCity.text!),
                        "zipcode"      : String(cell.txtZipCode.text!),
                        "name"         : String(cell.txtName.text!),
                        "phone"        : String(cell.txtphone.text!),
                        "landmark"     : String(""),
                        "workPlace"    : String(strSaveWork),
                        "notes"        : String(getNotes),
                        "EPrice"       : String(c),
                        "TIP"          : String(getTip),
                        "deliveryFee"  : String(getDeliveryFee),
                        "wahtUwant"    : String(getTextOneFromTaxScreen),
                        "StoreCity"    : String(getTexttWOFromTaxScreen),
                        "noContac"     : String(getContactOnDelivery),
                        "transactionId": String(""),
                        "transactionFee":String(transactionFeeValueIs),
                        "salesTax"     : String(taxPriceIs),
                    ]
                    
                    
                    
                } else {
                    print("no decimal")
                    
                    
                    
                    let salexTax = taxPriceIs
                    
                    let totalAmountIs = andTheTotalPriceIs
                    
                    let myString = salexTax
                    let myFloat = (myString! as NSString).doubleValue
                    print(myFloat as Any)
                    
                    print((myFloat/100))
                    
                    // print()
                    
                    // total amount
                    let myString2 = totalAmountIs
                    let myFloat2 = (myString2 as NSString).doubleValue
                    print(myFloat2 as Any)
                    
                    print((myFloat*myFloat2)/100)
                    
                    
                    let a = (myFloat*myFloat2)/100
                    let b = Double(totalAmountIs)
                    let c = a + b!
                    
                    print(c as Any)
                    
                    
                    
                    parameters = [
                        "action"       : "togetrequest",
                        "userId"       : person["userId"] as Any,
                        "latitude"     : String(self.strSaveLatitude),
                        "longitude"    : String(self.strSaveLongitude),
                        "address"      : String(cell.txtAddress.text!),
                        "state"        : String(cell.txtState.text!),
                        "city"         : String(cell.txtCity.text!),
                        "zipcode"      : String(cell.txtZipCode.text!),
                        "name"         : String(cell.txtName.text!),
                        "phone"        : String(cell.txtphone.text!),
                        "landmark"     : String(""),
                        "workPlace"    : String(strSaveWork),
                        "notes"        : String(getNotes),
                        "EPrice"       : String(c),
                        "TIP"          : String(getTip),
                        "deliveryFee"  : String(getDeliveryFee),
                        "wahtUwant"    : String(getTextOneFromTaxScreen),
                        "StoreCity"    : String(getTexttWOFromTaxScreen),
                        "noContac"     : String(getContactOnDelivery),
                        "transactionId": String(""),
                        "transactionFee":String(transactionFeeValueIs),
                        "salesTax"     : String(taxPriceIs),
                    ]
                    
                    
                    
                }
                    
                    
                    
            }
            
            
           }
        print("parameters-------\(String(describing: parameters))")
                     
                     Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                         {
                             response in
                 
                             switch(response.result) {
                             case .success(_):
                                if let data = response.result.value {

                                 let JSON = data as! NSDictionary
                                  print(JSON as Any)
                                 
                                 var strSuccess : String!
                                 strSuccess = JSON["status"]as Any as? String
                                 
                                   // var strSuccessAlert : String!
                                   // strSuccessAlert = JSON["msg"]as Any as? String
                                 
                                 if strSuccess == String("success") {
                                  print("yes")
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   
                                   // var dict: Dictionary<AnyHashable, Any>
                                  //  dict = JSON["msg"] as! Dictionary<AnyHashable, Any>
                                  
                                  // var strSuccess : String!
                                  // strSuccess = JSON["msg"]as Any as? String
                               
                                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessAfterOrderFoodId") as? UPSuccessAfterOrderFood
                                    self.navigationController?.pushViewController(push!, animated: true)
                                    
                                    
                                    /*
                                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
                                    self.navigationController?.pushViewController(push!, animated: true)
                                   */
                                    
                                    
                                    
                                    
                                    
                                    
                                 }
                                 else {
                                  print("no")
                                   ERProgressHud.sharedInstance.hide()
                                 }
                             }

                             case .failure(_):
                                 print("Error message:\(String(describing: response.result.error))")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                 
                                 let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                         UIAlertAction in
                                         NSLog("OK Pressed")
                                     }
                                 
                                 alertController.addAction(okAction)
                                 
                                 self.present(alertController, animated: true, completion: nil)
                                 
                                 break
                              }
                         }
          }
    
    @objc func homeClickMethod(_ sender:UIButton) {
        
        let btnH:UIButton = sender
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressThreeTableCell
        cell.btnWork.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
        cell.btnWork.tag = 0
        self.strSaveWork = "0"
        
        if btnH.tag == 0 {
            btnH.setBackgroundImage(UIImage(named: "checkCustomerContact"), for: .normal)
            self.strSaveAddress = "1"
            
            btnH.tag = 1
        } else if btnH.tag == 1 {
            btnH.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
            self.strSaveAddress = "0"
            
            btnH.tag = 0
        }
    }
    
    @objc func workClickMethod(_ sender:UIButton) {
        let btnW:UIButton = sender
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressThreeTableCell
        cell.btnHome.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
        cell.btnHome.tag = 0
        self.strSaveAddress = "0"
        
        if btnW.tag == 0 {
            btnW.setBackgroundImage(UIImage(named: "checkCustomerContact"), for: .normal)
            self.strSaveWork = "1"
            
            btnW.tag = 1
        } else if btnW.tag == 1 {
            btnW.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
            self.strSaveWork = "0"
            
            btnW.tag = 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TrackYourOrderId") as? TrackYourOrder
//        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 700
    }
    
}

extension UPAddressThree: UITableViewDelegate {
    
}

