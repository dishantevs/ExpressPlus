//
//  UPDisclouser.swift
//  ExpressPlus
//
//  Created by Apple on 11/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class UPDisclouserTwo: UIViewController {

    var textOne:String!
    var textTwo:String!
    
    @IBOutlet weak var navigationBar:UIView! {
          didSet {
              navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
          }
      }
      @IBOutlet weak var viewNavBottom:UIView! {
          didSet {
              viewNavBottom.backgroundColor = NAVIGATION_BACKGROUND_COLOR
              viewNavBottom.layer.cornerRadius = 6
              viewNavBottom.clipsToBounds = true
          }
      }
      @IBOutlet weak var lblNavigationTitle:UILabel! {
          didSet {
              lblNavigationTitle.text = "DISCLAIMER"
          }
      }
      
      @IBOutlet weak var btnBack:UIButton! {
          didSet {
              btnBack.setTitle("|||", for: .normal)
          }
      }
      
    @IBOutlet weak var btnSaveContinue:UIButton! {
        didSet {
            btnSaveContinue.setTitleColor(.white, for: .normal)
            btnSaveContinue.setTitle("Accept & Continue", for: .normal)
            btnSaveContinue.setTitleColor(.white, for: .normal)
            btnSaveContinue.layer.cornerRadius = 6
            btnSaveContinue.clipsToBounds = true
            btnSaveContinue.backgroundColor = .systemGreen
        }
    }
    
    @IBOutlet weak var txtView:UITextView! {
        didSet {
            txtView.isEditable = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        btnSaveContinue.addTarget(self, action: #selector(disclosureBarMenuClick), for: .touchUpInside)
        
        self.disclouser()
    }
    
    @objc func disclosureBarMenuClick() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPToGetEstimateTaxAndPriceId") as? UPToGetEstimateTaxAndPrice
        push!.getTextOneFromDisclouser = textOne
        push!.getTextTwoFromDisclouser = textTwo
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func disclouser() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
        let urlString = BASE_URL_EXPRESS_PLUS
  
        var parameters:Dictionary<AnyHashable, Any>!
        // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
                   parameters = [
                       "action"              : "getdisclosures"
            ]
        // }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 // var dict: Dictionary<AnyHashable, Any>
                                //  dict = JSON["msg"] as! Dictionary<AnyHashable, Any>
                                
                                var strSuccess : String!
                                strSuccess = JSON["msg"]as Any as? String
                                
                                let aux = "<span style=\"font-family: Avenier Next; font-size: 18\">\(strSuccess ?? "0")</span>"
                                
                                let attrStr = try! NSAttributedString(
                                    data: aux.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                                    options:[NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
                                self.txtView.attributedText = attrStr
                                
                                // self.txtView.text = strSuccess
                                 // var dict: Dictionary<AnyHashable, Any>
                                 // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                
                                /*
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                  
                                 self.tbleView.delegate = self
                                 self.tbleView.dataSource = self
                                 self.tbleView.reloadData()
                                */
                                 // self.loadMore = 1;
                                
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
        }

}

