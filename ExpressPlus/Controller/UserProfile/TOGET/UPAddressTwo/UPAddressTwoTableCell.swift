//
//  UPAddressTwoTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 27/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UPAddressTwoTableCell: UITableViewCell {

    @IBOutlet weak var txtAddress:UITextField! {
        didSet {
            txtAddress.layer.cornerRadius = 6
            txtAddress.clipsToBounds = true
            txtAddress.layer.borderColor = UIColor.lightGray.cgColor
            txtAddress.layer.borderWidth = 0.8
        }
    }
    @IBOutlet weak var txtState:UITextField! {
        didSet {
            txtState.layer.cornerRadius = 6
            txtState.clipsToBounds = true
            txtState.layer.borderColor = UIColor.lightGray.cgColor
            txtState.layer.borderWidth = 0.8
        }
    }
    @IBOutlet weak var txtCity:UITextField! {
        didSet {
            txtCity.layer.cornerRadius = 6
            txtCity.clipsToBounds = true
            txtCity.layer.borderColor = UIColor.lightGray.cgColor
            txtCity.layer.borderWidth = 0.8
        }
    }
    @IBOutlet weak var txtZipCode:UITextField! {
        didSet {
            txtZipCode.layer.cornerRadius = 6
            txtZipCode.clipsToBounds = true
            txtZipCode.layer.borderColor = UIColor.lightGray.cgColor
            txtZipCode.layer.borderWidth = 0.8
        }
    }
    @IBOutlet weak var txtName:UITextField! {
        didSet {
            txtName.layer.cornerRadius = 6
            txtName.clipsToBounds = true
            txtName.layer.borderColor = UIColor.lightGray.cgColor
            txtName.layer.borderWidth = 0.8
        }
    }
    @IBOutlet weak var txtphone:UITextField! {
        didSet {
            txtphone.layer.cornerRadius = 6
            txtphone.clipsToBounds = true
            txtphone.layer.borderColor = UIColor.lightGray.cgColor
            txtphone.layer.borderWidth = 0.8
        }
    }
    @IBOutlet weak var txtLandmark:UITextField! {
        didSet {
            txtLandmark.layer.cornerRadius = 6
            txtLandmark.clipsToBounds = true
            txtLandmark.layer.borderColor = UIColor.lightGray.cgColor
            txtLandmark.layer.borderWidth = 0.8
        }
    }
    @IBOutlet weak var btnSaveContinue:UIButton! {
        didSet {
            btnSaveContinue.setTitleColor(.white, for: .normal)
            btnSaveContinue.setTitle("Save & Continue", for: .normal)
            btnSaveContinue.setTitleColor(.white, for: .normal)
            btnSaveContinue.layer.cornerRadius = 6
            btnSaveContinue.clipsToBounds = true
            btnSaveContinue.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var btnHome:UIButton! {
        didSet {
            btnHome.backgroundColor = .clear
        }
    }
    @IBOutlet weak var btnWork:UIButton! {
        didSet {
            btnWork.backgroundColor = .clear
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
