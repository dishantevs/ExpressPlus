//
//  UPAddressTwo.swift
//  ExpressPlus
//
//  Created by Apple on 27/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

// MARK:- LOCATION -
import CoreLocation

import Alamofire
import SwiftyJSON

class UPAddressTwo: UIViewController, CLLocationManagerDelegate {

    
    
    
    let cellReuseIdentifier = "uPAddressTwoTableCell"
    
    let locationManager = CLLocationManager()
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    // MARK:- ADDRESS STRING -
    var strSaveAddress:String!
    var strSaveWork:String!
    
    // MARK:- GET ALL VALUE FROM CART -
    var strSaveContactOnDelivery2:String!
    var strSaveSpecialNotes2:String!
    var mArrSaveMutable2:NSMutableArray = []
    var getTotalAmountOfFood:String!
    var tipAmountIs:String!
    
    var strHW:String!
    
    var strTransactionFee:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "ADDRESS"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        
        // print(strSaveContactOnDelivery as Any)
        // print(strSaveSpecialNotes as Any)
        // print(mArrSaveMutable as Any)
     
        self.strSaveAddress = "0"
        self.strSaveWork = "0"
        
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressTableCell
//         cell.btnSaveContinue.addTarget(self, action: #selector(saveAndContinueClick), for: .touchUpInside)
        // cell.txtAddress.text = ""
        
        self.iAmHereForLocationPermission()
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func saveAndContinueClick() {
        /*
         let indexPath = IndexPath.init(row: 0, section: 0)
         let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressTableCell
         */
        
        
        
        
    }
    
    // MARK:- GET CUSTOMER LOCATION
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            // print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            // print("locality ==> "+locality as Any) // sector 10 dwarka
            
            // print(self.strSaveCountryName as Any) // india
            // print(self.strSaveStateName as Any) // new delhi
            // print(self.strSaveZipcodeName as Any) // 110075
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.reloadData()
        }
    }
    
    
    
    
    
    
    
    
    
    @objc func sendFoodDetailsToRestaurant() {
        
        /*
         [action] => addfooorder
         [deliveryLat] => 28.5871286
         [deliveryLong] => 77.0605454
         [userId] => 80
         [foodDetails] => [{"id":"49","name":"babbSs","price":"20.00","quantity":"4","resturentId":"104"},{"id":"48","name":"dosa","price":"65.00","quantity":"2","resturentId":"104"}]
         [discount] => 0.0
         [couponCode] =>
         [totalAmount] => 231.0
         [specialNote] => i am sprctial note
         [TIP] => 21.0
         [address] => Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India
         [state] => Delhi
         [city] => Delhi
         [zipcode] => 110075
         [name] =>
         [phone] => 6252525252
         [landmark] => fccg
         [workPlace] => Home
         [noContact] => 1
         */
        
        
        
        let paramsArray = self.mArrSaveMutable2
        let paramsJSON = JSON(paramsArray)
        let paramsString = paramsJSON.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!
        
        
        
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        let urlString = BASE_URL_EXPRESS_PLUS
                   
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressTwoTableCell
         // print(dictGetRestaurantFoodListing as Any)
         
         /*
          [action] => getcarts
          [userId] => 80
          */
         
        /*
         self.strSaveAddress = "0"
         self.strSaveWork = "0"
         */
        
        
        if self.strSaveAddress == "1" {
            self.strHW = "Home"
        } else if self.strSaveWork == "1" {
            self.strHW = "Work"
        } else {
            print("none")
            self.strHW = "0"
        }
            var parameters:Dictionary<AnyHashable, Any>!
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         
                let addressIs:String = String(self.strSaveLocality)+" "+String(self.self.strSaveLocalAddress)
                
                       parameters = [
                           "action"             : "addfooorder",
                           "userId"             : person["userId"] as Any,
                           "deliveryLat"        : String(self.strSaveLatitude),
                           "deliveryLong"       : String(self.strSaveLongitude),
                           "foodDetails"        : paramsString,
                           "discount"           : String("0"),
                           "couponCode"         : String(""),
                           "totalAmount"        : String(getTotalAmountOfFood),
                           "specialNote"        : String(strSaveSpecialNotes2),
                           "TIP"                : String(tipAmountIs),
                           "address"            : String(addressIs),
                           "state"              : String(cell.txtState.text!),
                           "city"               : String(cell.txtCity.text!),
                           "zipcode"            : String(cell.txtZipCode.text!),
                           "name"               : String(cell.txtName.text!),
                           "phone"              : String(cell.txtphone.text!),
                           "landmark"           : String(cell.txtLandmark.text!),
                           "workPlace"          : String(strHW),
                           "noContact"          : String(strSaveContactOnDelivery2)
                ]
            }
                       print("parameters-------\(String(describing: parameters))")
                       
                       Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                           {
                               response in
                   
                               switch(response.result) {
                               case .success(_):
                                  if let data = response.result.value {

                                   let JSON = data as! NSDictionary
                                    print(JSON as Any)
                                   
                                   var strSuccess : String!
                                   strSuccess = JSON["status"]as Any as? String
                                   
                                     // var strSuccessAlert : String!
                                     // strSuccessAlert = JSON["msg"]as Any as? String
                                   
                                   if strSuccess == String("success") {
                                    print("yes")
                                     
                                     // ERProgressHud.sharedInstance.hide()
                                    
                                    self.deleteAllCartWB()
                                   }
                                   else {
                                    print("no")
                                     ERProgressHud.sharedInstance.hide()
                                   }
                               }

                               case .failure(_):
                                   print("Error message:\(String(describing: response.result.error))")
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   
                                   let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                   
                                   let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                           UIAlertAction in
                                           NSLog("OK Pressed")
                                       }
                                   
                                   alertController.addAction(okAction)
                                   
                                   self.present(alertController, animated: true, completion: nil)
                                   
                                   break
                                }
                           }
    }
    
    // MARK:- DELETE ALL CART
    @objc func deleteAllCartWB() {
        // self.addInitialMutable.removeAllObjects()
     // self.arrListOfAllMyOrders.removeAllObjects()
     
      // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => deletecarts
      [userId] => 80
      [foodId] => 49
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
     if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
                   parameters = [
                       "action"              : "deleteallcarts",
                       "userId"              : person["userId"] as Any
                       
             ]
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                ERProgressHud.sharedInstance.hide()
                                
                                // push to success page
                                 let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
                                 self.navigationController?.pushViewController(push!, animated: true)
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
}

//MARK:- TABLE VIEW -
extension UPAddressTwo: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UPAddressTwoTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UPAddressTwoTableCell
        
        cell.backgroundColor    = .white
              
        cell.txtAddress.text    = String(self.strSaveLocality)+" "+String(self.strSaveStateName)
        
        cell.txtState.text      = String(self.strSaveLocalAddress)
        cell.txtCity.text       = String(self.strSaveLocalAddressMini)
        cell.txtZipCode.text    = String(self.strSaveZipcodeName)
        
        cell.btnHome.tag = 0
        cell.btnWork.tag = 0
        
        cell.btnHome.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
        cell.btnWork.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
        
        cell.btnHome.addTarget(self, action: #selector(homeClickMethod), for: .touchUpInside)
        cell.btnWork.addTarget(self, action: #selector(workClickMethod), for: .touchUpInside)
        cell.btnSaveContinue.addTarget(self, action: #selector(saveAndContinuClickMethod), for: .touchUpInside)
        
        return cell
    }
    
    @objc func saveAndContinuClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressTwoTableCell
        
        print(cell.txtName.text as Any)
        print(cell.txtphone.text as Any)
        print(cell.txtLandmark.text as Any)
        print(self.strSaveAddress as Any)
        print(self.strSaveWork as Any)
        
        print(cell.txtState.text as Any)
        print(cell.txtCity.text as Any)
        print(cell.txtZipCode.text as Any)
        
        // self.sendFoodDetailsToRestaurant()
        
        // UPSuccessPlacedOrder
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    @objc func homeClickMethod(_ sender:UIButton) {
        let btnH:UIButton = sender
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressTwoTableCell
        cell.btnWork.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
        cell.btnWork.tag = 0
        self.strSaveWork = "0"
        
        if btnH.tag == 0 {
            btnH.setBackgroundImage(UIImage(named: "checkCustomerContact"), for: .normal)
            self.strSaveAddress = "1"
            
            btnH.tag = 1
        } else if btnH.tag == 1 {
            btnH.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
            self.strSaveAddress = "0"
            
            btnH.tag = 0
        }
    }
    
    @objc func workClickMethod(_ sender:UIButton) {
        let btnW:UIButton = sender
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPAddressTwoTableCell
        cell.btnHome.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
        cell.btnHome.tag = 0
        self.strSaveAddress = "0"
        
        if btnW.tag == 0 {
            btnW.setBackgroundImage(UIImage(named: "checkCustomerContact"), for: .normal)
            self.strSaveWork = "1"
            
            btnW.tag = 1
        } else if btnW.tag == 1 {
            btnW.setBackgroundImage(UIImage(named: "unCheckCustomerContact"), for: .normal)
            self.strSaveWork = "0"
            
            btnW.tag = 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TrackYourOrderId") as? TrackYourOrder
//        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 700
    }
    
}

extension UPAddressTwo: UITableViewDelegate {
    
}

