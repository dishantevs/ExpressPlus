//
//  UPPaymentCard.swift
//  ExpressPlus
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Stripe
// import CreditCardForm

class UPPaymentCard: UIViewController ,STPPaymentCardTextFieldDelegate {

    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    @IBOutlet weak var viewNavBottom:UIView! {
        didSet {
            viewNavBottom.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            viewNavBottom.layer.cornerRadius = 6
            viewNavBottom.clipsToBounds = true
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "PAYMENT"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
