//
//  UPOrderHistory.swift
//  ExpressPlus
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class UPOrderHistory: UIViewController {

    let cellReuseIdentifier = "uPOrderHistoryTableCell"
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
       
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var strCheckOption:String!
    
    @IBOutlet weak var lblDownOne:UILabel!
    @IBOutlet weak var lblDownTwo:UILabel!
    @IBOutlet weak var lblDownThree:UILabel!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "ORDER HISTORY"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnService:UIButton! {
        didSet {
            btnService.setTitle("Services", for: .normal)
            btnService.setTitleColor(.white, for: .normal)
            btnService.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var btnFoodDelivery:UIButton! {
        didSet {
            btnFoodDelivery.setTitle("Food", for: .normal)
            btnFoodDelivery.setTitleColor(.white, for: .normal)
            btnFoodDelivery.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var btnToGet:UIButton! {
        didSet {
            btnToGet.setTitle("To Get", for: .normal)
            btnToGet.setTitleColor(.white, for: .normal)
            btnToGet.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            // self.tbleView.separatorStyle = UITableViewCell.SeparatorStyle.none
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        
        self.btnService.addTarget(self, action: #selector(serviceInitialsClick), for: .touchUpInside)
        self.btnFoodDelivery.addTarget(self, action: #selector(foodDeliveryInitialsClick), for: .touchUpInside)
        self.btnToGet.addTarget(self, action: #selector(toGetInitialsClick), for: .touchUpInside)
        
        self.lblDownOne.isHidden = false
        self.lblDownTwo.isHidden = true
        self.lblDownThree.isHidden = true
        
        self.sideBarMenuClick()
        
        self.orderHistoryWB()
    }
    
    override func viewDidAppear(_ animated: Bool) {
       // addTopAndBottomBorders()
    }
    
    @objc func serviceInitialsClick() {
        self.strCheckOption = "1"
    
        self.orderHistoryWB()
    }
    
    @objc func foodDeliveryInitialsClick() {
        self.strCheckOption = "2"
        
        let thickness: CGFloat = 2.0
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x:0, y: self.btnService.frame.size.height - thickness, width: self.btnService.frame.size.width, height:thickness)
        bottomBorder.backgroundColor = UIColor.clear.cgColor
        btnService.layer.addSublayer(bottomBorder)
        
        let thickness2: CGFloat = 2.0
        let bottomBorder2 = CALayer()
        bottomBorder.frame = CGRect(x:0, y: self.btnFoodDelivery.frame.size.height - thickness2, width: self.btnFoodDelivery.frame.size.width, height:thickness2)
        bottomBorder2.backgroundColor = UIColor.yellow.cgColor
        btnFoodDelivery.layer.addSublayer(bottomBorder2)
        
        let thickness3: CGFloat = 2.0
        let bottomBorder3 = CALayer()
        bottomBorder.frame = CGRect(x:0, y: self.btnToGet.frame.size.height - thickness3, width: self.btnToGet.frame.size.width, height:thickness3)
        bottomBorder3.backgroundColor = UIColor.clear.cgColor
        btnToGet.layer.addSublayer(bottomBorder3)
        
         self.foodDeliveryWB()
    }
    
    @objc func toGetInitialsClick() {
        self.strCheckOption = "3"
        
        let thickness: CGFloat = 2.0
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x:0, y: self.btnService.frame.size.height - thickness, width: self.btnService.frame.size.width, height:thickness)
        bottomBorder.backgroundColor = UIColor.clear.cgColor
        btnService.layer.addSublayer(bottomBorder)
        
        let thickness2: CGFloat = 2.0
        let bottomBorder2 = CALayer()
        bottomBorder.frame = CGRect(x:0, y: self.btnFoodDelivery.frame.size.height - thickness2, width: self.btnFoodDelivery.frame.size.width, height:thickness2)
        bottomBorder2.backgroundColor = UIColor.clear.cgColor
        btnFoodDelivery.layer.addSublayer(bottomBorder2)
        
        let thickness3: CGFloat = 2.0
        let bottomBorder3 = CALayer()
        bottomBorder.frame = CGRect(x:0, y: self.btnToGet.frame.size.height - thickness3, width: self.btnToGet.frame.size.width, height:thickness3)
        bottomBorder3.backgroundColor = UIColor.yellow.cgColor
        btnToGet.layer.addSublayer(bottomBorder3)
        
         self.toGetWB()
    }
    
    func addTopAndBottomBorders() {
       let thickness: CGFloat = 2.0
       let bottomBorder = CALayer()
       bottomBorder.frame = CGRect(x:0, y: self.btnService.frame.size.height - thickness, width: self.btnService.frame.size.width, height:thickness)
       bottomBorder.backgroundColor = UIColor.yellow.cgColor
       btnService.layer.addSublayer(bottomBorder)
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    // MARK:- WEBSERVICES ( ORDER HISTORY )
    @objc func orderHistoryWB() {
        
        self.lblDownOne.isHidden = false
        self.lblDownTwo.isHidden = true
        self.lblDownThree.isHidden = true
        
        self.arrListOfAllMyOrders.removeAllObjects()
        
     self.strCheckOption = "1"
                              
     ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
               
     // let indexPath = IndexPath.init(row: 0, section: 0)
     // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
     // print(dictGetRestaurantFoodListing as Any)
     
     /*
      [action] => getcarts
      [userId] => 80
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     /*
             [action] => foodorderlist
             [userId] => 80
             [userType] => Member
             [status] =>
             [pageNo] => 1
             */
            
             let x : Int = person["userId"] as! Int
             let myString = String(x)
            
            parameters = [
                "action"              : "bookinglist",
                "userId"              : String(myString),
                "userType"            : String("Member"),
                "status"              : String(""),
                "pageNo"              : String("1")
            ]
        }
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
            {
                response in
               
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value {

                        let JSON = data as! NSDictionary
                                // print(JSON as Any)
                               
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                        if strSuccess == String("success") {
                            print("yes")
                                 
                            ERProgressHud.sharedInstance.hide()
                                 
                                 // var dict: Dictionary<AnyHashable, Any>
                                 // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                  
                            self.tbleView.delegate = self
                            self.tbleView.dataSource = self
                            self.tbleView.reloadData()
                            self.loadMore = 1   
                                
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
        }
    
    // MARK:- WEBSERVICE ( FOOD DELIVERY ) -
    @objc func foodDeliveryWB() {
        
        self.lblDownOne.isHidden = true
        self.lblDownTwo.isHidden = false
        self.lblDownThree.isHidden = true
        
        self.arrListOfAllMyOrders.removeAllObjects()
        
        self.strCheckOption = "2"
        
     ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
               
     // let indexPath = IndexPath.init(row: 0, section: 0)
     // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
     // print(dictGetRestaurantFoodListing as Any)
     
     /*
      [action] => bookinglist
      [pageNo] => 1
      [status] =>
      [userId] => 80
      [userType] => Member
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     /*
             [action] => foodorderlist
             [userId] => 80
             [userType] => Member
             [status] =>
             [pageNo] => 1
             */
                   parameters = [
                       "action"              : "foodorderlist",
                       "userId"              : person["userId"] as Any,
                       "userType"            : String("Member"),
                       "status"              : String(""),
                       "pageNo"              : String("1")
            ]
        }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                  // print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 // var dict: Dictionary<AnyHashable, Any>
                                 // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                  
                                 self.tbleView.delegate = self
                                 self.tbleView.dataSource = self
                                 self.tbleView.reloadData()
                                 self.loadMore = 1;
                                
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
        }
    
    @objc func toGetWB() {
        
        self.lblDownOne.isHidden = true
        self.lblDownTwo.isHidden = true
        self.lblDownThree.isHidden = false
        
        self.arrListOfAllMyOrders.removeAllObjects()
     self.strCheckOption = "3"
     ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
               
     // let indexPath = IndexPath.init(row: 0, section: 0)
     // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
     // print(dictGetRestaurantFoodListing as Any)
     
     /*
      [action] => getcarts
      [userId] => 80
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     /*
             [action] => foodorderlist
             [userId] => 80
             [userType] => Member
             [status] =>
             [pageNo] => 1
             */
                   parameters = [
                       "action"              : "gogetlist",
                       "userId"              : person["userId"] as Any,
                       "userType"            : String("Member"),
                       "status"              : String(""),
                       "pageNo"              : String("1")
            ]
        }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                 print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                 
                                 // var dict: Dictionary<AnyHashable, Any>
                                 // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                  
                                 self.tbleView.delegate = self
                                 self.tbleView.dataSource = self
                                 self.tbleView.reloadData()
                                 self.loadMore = 1;
                                
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
        }
}

extension UPOrderHistory: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UPOrderHistoryTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UPOrderHistoryTableCell
        
        /* // services
         AVGRating = "";
         TIP = 40;
         address = "99, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
         bookingDate = "16th May";
         bookingId = 185;
         contactNumber = 987654321;
         created = "2020-05-15 19:31:00";
         email = "raj@gmail.com";
         fullName = user;
         groupId = 26;
         image = "";
         serviceAmount = 400;
         serviceName = "Music Social";
         servicePrice = 400;
         slote = "21:0-17:0";
         status = 3;
         totalAmount = 440;
         userAddress = "sector 10";
         userId = 57;
         zipCode = 201301;
         */
        
        /* // food delivery
         TIP = 20;
         cardNo = "";
         couponCode = "";
         created = "2020-05-21 13:30:00";
         discount = "";
         driverName = "";
         foodDetails =             (
                             {
                 id = 48;
                 name = dosa;
                 price = "65.00";
                 quantity = 1;
                 resturentId = 104;
             },
                             {
                 id = 49;
                 name = babbSs;
                 price = "20.00";
                 quantity = 1;
                 resturentId = 104;
             }
         );
         foodId = "";
         foodorderId = 87;
         noContact = "";
         resturentAddress = "Gwalior, Madhya Pradesh, India";
         resturentId = 104;
         resturentImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1588663958IMG20191105144302.jpg";
         resturentName = satish;
         specialNote = "";
         status = 1;
         totalAmount = 105;
         userAddress = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
         userId = 80;
         userImage = "";
         userName = purnimacr;
         whatYouWant = "";
         workPlace = Home;
         */
        
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        
         // print(item as Any)
        /*
         Optional(["status": 1, "wahtUwant": tytyt, "created": 2020-05-21 17:20:00, "address": Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India, "latidude": , "longitude": 77.0605496, "userImage": , "noContact": , "userName": purnimacr, "notes": hshshs, "EPrice": 1128, "TIP": 11, "togetrequestId": 49, "deliveryFee": 6, "userId": 80, "StoreCity": ghh])
         (lldb)
         */
        
        if strCheckOption == "1" {
            cell.lblName.text = (item!["fullName"] as! String)
            cell.lblPrice.text = " - $"+(item!["totalAmount"] as! String)
            cell.lblDate.text = (item!["created"] as! String)
            cell.lblService.text = (item!["serviceName"] as! String)
            
            cell.imgProfileImage.sd_setImage(with: URL(string: (item!["image"] as! String)), placeholderImage: UIImage(named: "user"))
        } else if strCheckOption == "2" {
            cell.lblName.text = (item!["userName"] as! String)
            cell.lblPrice.text = " - $"+(item!["totalAmount"] as! String)
            cell.lblDate.text = (item!["created"] as! String)
            cell.lblService.text = ""
            
            cell.imgProfileImage.sd_setImage(with: URL(string: (item!["resturentImage"] as! String)), placeholderImage: UIImage(named: "user"))
        } else if strCheckOption == "3" {
            cell.lblName.text = (item!["userName"] as! String)
            cell.lblPrice.text = " - $"+(item!["EPrice"] as! String)
            cell.lblDate.text = (item!["created"] as! String)
            cell.lblService.text = ""
            
            cell.imgProfileImage.sd_setImage(with: URL(string: (item!["userImage"] as! String)), placeholderImage: UIImage(named: "user"))
        }
        
        
        
        cell.backgroundColor = .clear
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        
        return cell
    }

func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView .deselectRow(at: indexPath, animated: true)
    // dictGetAllServiceDetails
    
    let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
    if strCheckOption == "1" {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPServiceJobDetailsId") as? UPServiceJobDetails
        settingsVCId!.dictGetAllServiceDetails = item as NSDictionary?
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    } else if strCheckOption == "2" {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDRestaurantDetailsOrdersId") as? PDRestaurantDetailsOrders
        settingsVCId!.dictGetFoodDetails = item as NSDictionary?
        settingsVCId!.strDeliveryDetailsFromUserProfile = "yes"
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    } else if strCheckOption == "3" {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPToGetDetailsId") as? UPToGetDetails
        settingsVCId!.dictGetAllDetailsToGet = item as NSDictionary?
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    
}
    
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
    
}
    
}


extension UPOrderHistory: UITableViewDelegate {
    
}
