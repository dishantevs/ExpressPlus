//
//  UPSuccessAfterOrderFood.swift
//  ExpressPlus
//
//  Created by Apple on 09/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UPSuccessAfterOrderFood: UIViewController {

    @IBOutlet weak var imgloadingIndicator:UIImageView!
    
    @IBOutlet weak var btnHomePage:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnHomePage.addTarget(self, action: #selector(homepageClickMethod), for: .touchUpInside)
    }
    
    @objc func homepageClickMethod() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPFoodId")
        self.navigationController?.pushViewController(push, animated: true)
        
    }
}
