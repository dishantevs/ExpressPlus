//
//  UPSuccessAndPayNow.swift
//  ExpressPlus
//
//  Created by Apple on 09/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UPSuccessAndPayNow: UIViewController {

    /*
     aps =     {
         alert =         {
             body = "New booking has been confirm.";
             title = "New booking has been confirm.";
         };
         sound = Default;
     };
     driverContect = 2525368521;
     driverID = 354;
     driverImage = "https://www.expressplusnow.com/appbackend/img/uploads/users/1600510943images(6).jpeg";
     driverName = purnimad;
     fooOrderId = 374;
     foodOrderGroupId = 373;
     "gcm.message_id" = 1602244584111011;
     "google.c.a.e" = 1;
     "google.c.sender.id" = 27735899901;
     message = "New booking has been confirm.";
     totalAmount = 56;
     type = foodAccept;
     */
    
    var dictOfNotificationPopup:NSDictionary!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.layer.cornerRadius = 8
            viewBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnAgreeAndPay:UIButton! {
        didSet {
            btnAgreeAndPay.setTitle("Pay Now", for: .normal)
        }
    }
    @IBOutlet weak var lblDriverName:UILabel! {
        didSet {
            lblDriverName.textAlignment = .left
        }
    }
    
    @IBOutlet weak var lblDriverPhone:UILabel! {
        didSet {
            lblDriverPhone.textAlignment = .left
        }
    }
    
    @IBOutlet weak var imgProfilePicture:UIImageView! {
        didSet {
            imgProfilePicture.layer.cornerRadius = 35
            imgProfilePicture.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblServiceAndSlotName:UILabel!
    
    @IBOutlet weak var serviceView:UIView! {
        didSet {
            serviceView.layer.cornerRadius = 8
            serviceView.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        /*
         aps =     {
             alert =         {
                 body = " has been accepted your booking successfully.";
                 title = " has been accepted your booking successfully.";
             };
             sound = Default;
         };
         bookingId = 446;
         "gcm.message_id" = 1603196972337903;
         "google.c.a.e" = 1;
         "google.c.sender.id" = 27735899901;
         message = " has been accepted your booking successfully.";
         type = ServiceAccept;
         */
        
        if (dictOfNotificationPopup["type"] as! String) == "ServiceAccept" {
            
           /*
             User Info dishu =  [AnyHashable("bookingId"): 462, AnyHashable("serviceId"): 2, AnyHashable("type"): ServiceAccept, AnyHashable("aps"): {
                 alert =     {
                     body = " has been accepted your booking successfully.";
                     title = " has been accepted your booking successfully.";
                 };
                 sound = Default;
             }, AnyHashable("address"): Sector 10 Dwarka South West Delhi, AnyHashable("google.c.sender.id"): 27735899901, AnyHashable("gcm.message_id"): 1603265812394571, AnyHashable("slote"): 1:06 PM-4:06 PM, AnyHashable("bookingDate"): 2020-10-31, AnyHashable("vendorContactNumber"): 5689562352, AnyHashable("longitude"): 77.06065325957097, AnyHashable("userName"): user, AnyHashable("message"):  has been accepted your booking successfully., AnyHashable("serviceName"): House Cleaning , AnyHashable("contactNumber"): 9696986532, AnyHashable("vendorName"): ddriver, AnyHashable("totalAmount"): 26, AnyHashable("TIP"): 20, AnyHashable("google.c.a.e"): 1, AnyHashable("serviceAmount"): 2]
             {
                 TIP = 20;
                 address = "Sector 10 Dwarka South West Delhi";
                 aps =     {
                     alert =         {
                         body = " has been accepted your booking successfully.";
                         title = " has been accepted your booking successfully.";
                     };
                     sound = Default;
                 };
                 bookingDate = "2020-10-31";
                 bookingId = 462;
                 contactNumber = 9696986532;
                 "gcm.message_id" = 1603265812394571;
                 "google.c.a.e" = 1;
                 "google.c.sender.id" = 27735899901;
                 longitude = "77.06065325957097";
                 message = " has been accepted your booking successfully.";
                 serviceAmount = 2;
                 serviceId = 2;
                 serviceName = "House Cleaning ";
                 slote = "1:06 PM-4:06 PM";
                 totalAmount = 26;
                 type = ServiceAccept;
                 userName = user;
                 vendorContactNumber = 5689562352;
                 vendorName = ddriver;
             }


             */
            
            
            
            // when driver accept user's to get request
            /*
             User Info dishu =  [AnyHashable("aps"): {
                 alert =     {
                     body = "Request accepted";
                     title = "Request accepted";
                 };
                 sound = Default;
             }, AnyHashable("togetRequestId"): 245, AnyHashable("google.c.sender.id"): 27735899901, AnyHashable("vendorContactNumber"): 5689562352, AnyHashable("vendorImage"): https://www.expressplusnow.com/appbackend/img/uploads/users/1603183585123269927912595.png, AnyHashable("message"): Request accepted, AnyHashable("type"): ToGetRequestAccept, AnyHashable("gcm.message_id"): 1603283105837850, AnyHashable("vendorName"): ddriver, AnyHashable("google.c.a.e"): 1]
             Optional(ToGetRequestAccept)
             {
                 aps =     {
                     alert =         {
                         body = "Request accepted";
                         title = "Request accepted";
                     };
                     sound = Default;
                 };
                 "gcm.message_id" = 1603283105837850;
                 "google.c.a.e" = 1;
                 "google.c.sender.id" = 27735899901;
                 message = "Request accepted";
                 togetRequestId = 245;
                 type = ToGetRequestAccept;
                 vendorContactNumber = 5689562352;
                 vendorImage = "https://www.expressplusnow.com/appbackend/img/uploads/users/1603183585123269927912595.png";
                 vendorName = ddriver;
             }
             */
            
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            
            self.serviceView.isHidden = false
            
            self.lblServiceAndSlotName.text = "Service Name : "+(dictOfNotificationPopup["serviceName"] as! String)+"\n"+"Service Slot Time :"+(dictOfNotificationPopup["slote"] as! String)
            
            self.lblDriverName.text = (dictOfNotificationPopup["vendorName"] as! String)
            self.lblDriverPhone.text = (dictOfNotificationPopup["vendorContactNumber"] as! String)
            self.btnAgreeAndPay.addTarget(self, action: #selector(paymentNowAfterAccept), for: .touchUpInside)
            
        } else if (dictOfNotificationPopup["type"] as! String) == "ToGetRequestAccept" {
          
            self.serviceView.isHidden = false
            self.lblServiceAndSlotName.text = "To Get Request"
            self.lblServiceAndSlotName.textAlignment = .center
            self.lblDriverName.text = (dictOfNotificationPopup["vendorName"] as! String)
            self.lblDriverPhone.text = (dictOfNotificationPopup["vendorContactNumber"] as! String)
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            
            self.btnAgreeAndPay.addTarget(self, action: #selector(paymentNowAfterAccept), for: .touchUpInside)
            
            self.imgProfilePicture.sd_setImage(with: URL(string: (dictOfNotificationPopup["vendorImage"] as! String)), placeholderImage: UIImage(named: "food1"))
            
        } else {
            
            self.serviceView.isHidden = true
            self.lblDriverName.text = (dictOfNotificationPopup["driverName"] as! String)
            self.lblDriverPhone.text = (dictOfNotificationPopup["driverContect"] as! String)
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            
            self.btnAgreeAndPay.addTarget(self, action: #selector(paymentNowAfterAccept), for: .touchUpInside)
            
            self.imgProfilePicture.sd_setImage(with: URL(string: (dictOfNotificationPopup["driverImage"] as! String)), placeholderImage: UIImage(named: "food1"))
            
        }
        
    }
    
    @objc func paymentNowAfterAccept() {
        
        if (dictOfNotificationPopup["type"] as! String) == "ServiceAccept" {
            
            /*
             "action"           : "updatepayment",
             "userId"           : person["userId"] as Any,
             "groupId"          : String(seviceGroupId),
             "serviceAmount"    : String(serviceAmount),
             "serviceType"      : String(seviceType),
             "totalAmount"      : String(self.getTotalPayment2),
             "transactionId"    : String(strTokenIdIs),
             "TIP"              : String(seviceTipAmount),
             */
            // UPPaymentScreen
            
            /*
             bookingGroupId = 254;
             msg = "Booking completed succssfully";
             status = success;
             */
            
            
             let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPPayemtnScreenForFoodSectionId") as? UPPayemtnScreenForFoodSection
             
            
            // TIP
            if dictOfNotificationPopup["TIP"] is String {
                                
                print("Yes, it's a String")
                // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
                // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
                push!.servicePaymentTip = (dictOfNotificationPopup["TIP"] as! String)
                
            } else if dictOfNotificationPopup["TIP"] is Int {
                
                print("It is Integer")
                let x2 : Int = (dictOfNotificationPopup["TIP"] as! Int)
                let myString2 = String(x2)
                // self.changesBookingIdis = String(myString2)
                // push!.getTotalPriceOfFood = String(myString2)
                push!.servicePaymentTip = String(myString2)
                
            } else {
                
                print("i am ")
                let temp:NSNumber = dictOfNotificationPopup["TIP"] as! NSNumber
                let tempString = temp.stringValue
                // self.changesBookingIdis = String(tempString)
                // push!.getTotalPriceOfFood = String(tempString)
                push!.servicePaymentTip = String(tempString)
                
            }
            
            // service id
            if dictOfNotificationPopup["serviceId"] is String {
                                
                print("Yes, it's a String")
                // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
                // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
                push!.servicePaymentType = (dictOfNotificationPopup["serviceId"] as! String)
                
            } else if dictOfNotificationPopup["serviceId"] is Int {
                
                print("It is Integer")
                let x2 : Int = (dictOfNotificationPopup["serviceId"] as! Int)
                let myString2 = String(x2)
                // self.changesBookingIdis = String(myString2)
                // push!.getTotalPriceOfFood = String(myString2)
                push!.servicePaymentType = String(myString2)
                
            } else {
                
                print("i am ")
                let temp:NSNumber = dictOfNotificationPopup["serviceId"] as! NSNumber
                let tempString = temp.stringValue
                // self.changesBookingIdis = String(tempString)
                // push!.getTotalPriceOfFood = String(tempString)
                push!.servicePaymentType = String(tempString)
                
            }
            
            
            // service amount
            if dictOfNotificationPopup["serviceAmount"] is String {
                                
                print("Yes, it's a String")
                // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
                // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
                push!.servicePaymentAmount = (dictOfNotificationPopup["serviceAmount"] as! String)
                
            } else if dictOfNotificationPopup["serviceAmount"] is Int {
                
                print("It is Integer")
                let x2 : Int = (dictOfNotificationPopup["serviceAmount"] as! Int)
                let myString2 = String(x2)
                // self.changesBookingIdis = String(myString2)
                // push!.getTotalPriceOfFood = String(myString2)
                push!.servicePaymentAmount = String(myString2)
                
            } else {
                
                print("i am ")
                let temp:NSNumber = dictOfNotificationPopup["serviceAmount"] as! NSNumber
                let tempString = temp.stringValue
                // self.changesBookingIdis = String(tempString)
                // push!.getTotalPriceOfFood = String(tempString)
                push!.servicePaymentAmount = String(tempString)
                
            }
            
            
            
            
            
            // group id
            if dictOfNotificationPopup["groupId"] is String {
                                
                print("Yes, it's a String")
                // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
                // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
                push!.servicePaymentBookingId = (dictOfNotificationPopup["groupId"] as! String)
                
            } else if dictOfNotificationPopup["groupId"] is Int {
                
                print("It is Integer")
                let x2 : Int = (dictOfNotificationPopup["groupId"] as! Int)
                let myString2 = String(x2)
                // self.changesBookingIdis = String(myString2)
                // push!.getTotalPriceOfFood = String(myString2)
                push!.servicePaymentBookingId = String(myString2)
                
            } else {
                
                print("i am ")
                let temp:NSNumber = dictOfNotificationPopup["groupId"] as! NSNumber
                let tempString = temp.stringValue
                // self.changesBookingIdis = String(tempString)
                // push!.getTotalPriceOfFood = String(tempString)
                push!.servicePaymentBookingId = String(tempString)
                
            }
            
            
            
            
            
            
            
            
            
             if dictOfNotificationPopup["bookingId"] is String {
                                 
                 print("Yes, it's a String")
                 // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
                 // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
                 push!.foodOrderId = (dictOfNotificationPopup["bookingId"] as! String)
                 
             } else if dictOfNotificationPopup["bookingId"] is Int {
                 
                 print("It is Integer")
                 let x2 : Int = (dictOfNotificationPopup["bookingId"] as! Int)
                 let myString2 = String(x2)
                 // self.changesBookingIdis = String(myString2)
                 // push!.getTotalPriceOfFood = String(myString2)
                 push!.foodOrderId = String(myString2)
                 
             } else {
                 
                 print("i am ")
                 let temp:NSNumber = dictOfNotificationPopup["bookingId"] as! NSNumber
                 let tempString = temp.stringValue
                 // self.changesBookingIdis = String(tempString)
                 // push!.getTotalPriceOfFood = String(tempString)
                 push!.foodOrderId = String(tempString)
                 
             }
             
             if dictOfNotificationPopup["totalAmount"] is String {
                                 
                 print("Yes, it's a String")
                 // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
                 push!.servicePaymentTotalAmount = (dictOfNotificationPopup["totalAmount"] as! String)
                 
             } else if dictOfNotificationPopup["bookingId"] is Int {
                 
                 print("It is Integer")
                 let x2 : Int = (dictOfNotificationPopup["totalAmount"] as! Int)
                 let myString2 = String(x2)
                 // self.changesBookingIdis = String(myString2)
                 push!.servicePaymentTotalAmount = String(myString2)
             } else {
                 
                 print("i am ")
                 let temp:NSNumber = dictOfNotificationPopup["totalAmount"] as! NSNumber
                 let tempString = temp.stringValue
                 // self.changesBookingIdis = String(tempString)
                 push!.servicePaymentTotalAmount = String(tempString)
             }
             
            push!.paymentFoodOrService = "iAmFromService"
            push!.enableBackOrNot = "yes"
            
            /*
             var servicePaymentAmount:String!
             var servicePaymentTotalAmount:String!
             var servicePaymentType:String!
             var servicePaymentTip:String!
             var servicePaymentBookingId:String!
             */
            
            // push!.servicePaymentAmount          = ""
            // push!.servicePaymentTotalAmount     = ""
            // push!.servicePaymentType            = ""
            // push!.servicePaymentTip             = ""
            // push!.servicePaymentBookingId       = ""
            
            
            
            
            // serviceAmount
            // bookingGroupId
            
            
            
            
            self.navigationController?.pushViewController(push!, animated: true)
             
             
            
        } else if (dictOfNotificationPopup["type"] as! String) == "ToGetRequestAccept" {
            
             let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPPayemtnScreenForFoodSectionId") as? UPPayemtnScreenForFoodSection
             
            
            // TO GET REQUEST ID
            if dictOfNotificationPopup["togetRequestId"] is String {
                                
                print("Yes, it's a String")
                push!.toGetRequestId = (dictOfNotificationPopup["togetRequestId"] as! String)
                
            } else if dictOfNotificationPopup["togetRequestId"] is Int {
                
                print("It is Integer")
                let x2 : Int = (dictOfNotificationPopup["togetRequestId"] as! Int)
                let myString2 = String(x2)
                push!.toGetRequestId = String(myString2)
                
            } else {
                
                print("i am ")
                let temp:NSNumber = dictOfNotificationPopup["togetRequestId"] as! NSNumber
                let tempString = temp.stringValue
                push!.toGetRequestId = String(tempString)
                
            }
            
            
            
            
            // TOTAL AMOUNT
            if dictOfNotificationPopup["EPrice"] is String {
                                
                print("Yes, it's a String")
                push!.toGetTotalAmount = (dictOfNotificationPopup["EPrice"] as! String)
                
            } else if dictOfNotificationPopup["EPrice"] is Int {
                
                print("It is Integer")
                let x2 : Int = (dictOfNotificationPopup["EPrice"] as! Int)
                let myString2 = String(x2)
                push!.toGetTotalAmount = String(myString2)
                
            } else {
                
                print("i am ")
                let temp:NSNumber = dictOfNotificationPopup["EPrice"] as! NSNumber
                let tempString = temp.stringValue
                push!.toGetTotalAmount = String(tempString)
                
            }
            
            
            // TOTAL AMOUNT
            if self.dictOfNotificationPopup["TIP"] is String {
                                
                print("Yes, it's a String")
                push!.toGetTipAmount = (self.dictOfNotificationPopup["TIP"] as! String)
                
            } else if self.dictOfNotificationPopup["TIP"] is Int {
                
                print("It is Integer")
                let x2 : Int = (self.dictOfNotificationPopup["TIP"] as! Int)
                let myString2 = String(x2)
                push!.toGetTipAmount = String(myString2)
                
            } else {
                
                print("i am ")
                let temp:NSNumber = self.dictOfNotificationPopup["TIP"] as! NSNumber
                let tempString = temp.stringValue
                push!.toGetTipAmount = String(tempString)
                
            }
            
            
            push!.paymentFoodOrService = "iAmFromToGet"
           
            self.navigationController?.pushViewController(push!, animated: true)
             
             
            
        } else {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPPayemtnScreenForFoodSectionId") as? UPPayemtnScreenForFoodSection
            
            if dictOfNotificationPopup["foodOrderGroupId"] is String {
                                
                print("Yes, it's a String")
                // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
                push!.foodOrderId = (dictOfNotificationPopup["foodOrderGroupId"] as! String)
                
            } else if dictOfNotificationPopup["foodOrderGroupId"] is Int {
                
                print("It is Integer")
                let x2 : Int = (dictOfNotificationPopup["foodOrderGroupId"] as! Int)
                let myString2 = String(x2)
                // self.changesBookingIdis = String(myString2)
                push!.foodOrderId = String(myString2)
                
            } else {
                
                print("i am ")
                let temp:NSNumber = dictOfNotificationPopup["foodOrderGroupId"] as! NSNumber
                let tempString = temp.stringValue
                // self.changesBookingIdis = String(tempString)
                push!.foodOrderId = String(tempString)
                
            }
            
            if dictOfNotificationPopup["totalAmount"] is String {
                                
                print("Yes, it's a String")
                // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
                
                
                let addTotalAmountAndTransactionFee = Double(dictOfNotificationPopup["totalAmount"] as! String)!+Double(dictOfNotificationPopup["transactionFee"] as! String)!
                
                push!.getTotalPriceOfFood = String(addTotalAmountAndTransactionFee)
                
            } else if dictOfNotificationPopup["bookingId"] is Int {
                
                print("It is Integer")
                let x2 : Int = (dictOfNotificationPopup["totalAmount"] as! Int)
                let myString2 = String(x2)
                // self.changesBookingIdis = String(myString2)
                
                
                let addTotalAmountAndTransactionFee = Double(myString2)!+Double(dictOfNotificationPopup["transactionFee"] as! String)!
                
                
                push!.getTotalPriceOfFood = String(addTotalAmountAndTransactionFee)
            } else {
                
                print("i am ")
                let temp:NSNumber = dictOfNotificationPopup["totalAmount"] as! NSNumber
                let tempString = temp.stringValue
                // self.changesBookingIdis = String(tempString)
                
                let addTotalAmountAndTransactionFee = Double(tempString)!+Double(dictOfNotificationPopup["transactionFee"] as! String)!
                
                push!.getTotalPriceOfFood = String(addTotalAmountAndTransactionFee)
            }
            
            
            
            self.navigationController?.pushViewController(push!, animated: true)
            
        }
        
    }

}
