//
//  UPPayemtnScreenForFoodSection.swift
//  ExpressPlus
//
//  Created by Apple on 05/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

import Stripe
class UPPayemtnScreenForFoodSection: UIViewController, UITextFieldDelegate {

    let cellReuseIdentifier = "uPPayemtnScreenForFoodSectionTableCell"
    
    var foodOrderId:String!
    
    var getTotalPriceOfFood:String!
    
    
    /*
     "groupId"          : String(seviceGroupId),
     "serviceAmount"    : String(serviceAmount),
     "serviceType"      : String(seviceType),
     "totalAmount"      : String(self.getTotalPayment2),
     "transactionId"    : String(strTokenIdIs),
     "TIP"              : String(seviceTipAmount)
     */
    
    
    
    
    
    var paymentFoodOrService:String!
    var servicePaymentAmount:String!
    var servicePaymentTotalAmount:String!
    var servicePaymentType:String!
    var servicePaymentTip:String!
    var servicePaymentBookingId:String!
    
    
    
    
    var enableBackOrNot:String!
    
    
    // to get details
    var toGetRequestId:String!
    var toGetTotalAmount:String!
    var requestMoney:String!
    var toGetTipAmount:String!
    
    // special order
    var specialFoodSpecialId:String!
    var specialFoodTotalAmount:String!
    var specialFoodTip:String!
    
    
    
    var strGetAdminAmount:String!
    var strAccountNumberIs:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "PAYMENT"
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
        }
    }
    @IBOutlet weak var lblCardNumberHeading:UILabel!
    @IBOutlet weak var lblEXPDate:UILabel!
    
    @IBOutlet weak var lblPayableAmount:UILabel!
    
    @IBOutlet weak var btnmakePayment:UIButton! {
        didSet {
            btnmakePayment.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnmakePayment.setTitle("MAKE PAYMENT", for: .normal)
            btnmakePayment.setTitleColor(.white, for: .normal)
        }
    }
    @IBOutlet weak var viewCard:UIView! {
        didSet {
            viewCard.backgroundColor = UIColor.init(red: 34.0/255.0, green: 72.0/255.0, blue: 104.0/255.0, alpha: 1)
            viewCard.layer.cornerRadius = 6
            viewCard.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if enableBackOrNot == "yes" {
            
            self.btnBack.isHidden = false
            btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
            
        } else {
            
            self.btnBack.isHidden = true
            
            
        }
        
        
        // print(self.strGetAdminAmount as Any)
        // print(self.strAccountNumberIs as Any)
        
        if paymentFoodOrService == "iAmFromService" {
            
            self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(servicePaymentTotalAmount)
            
        } else if paymentFoodOrService == "iAmFromToGet" {
            
            self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(toGetTotalAmount)
            
        } else if paymentFoodOrService == "iAmFromSpecialOrder" {
            
            self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(specialFoodTotalAmount)
            
        } else if paymentFoodOrService == "iAmFromPayRequestMoney" {
            
            self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(requestMoney)
            
        }  else {
            
             
            // self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(getTotalPriceOfFood)
            // print(getTotalPriceOfFood as Any)
            // strGetAdminAmount
            
            let doublee = Double(getTotalPriceOfFood)!
            
            let aStr = String(format: "%.2f", doublee)
 
            self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(aStr)
        }
        
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        self.btnmakePayment.addTarget(self, action: #selector(firstCheckValidation), for: .touchUpInside)
        
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func firstCheckValidation() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPPayemtnScreenForFoodSectionTableCell
        
        if cell.txtCardNumber.text == "" {
            let alert = UIAlertController(title: "Card Number", message: "Card number should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtExpDate.text == "" {
            let alert = UIAlertController(title: "Exp Month", message: "Expiry Month should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtCVV.text == "" {
            let alert = UIAlertController(title: "Security Code", message: "Security Code should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            self.fetchStripeToken()
        }
        
    }
    
    @objc func fetchStripeToken() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPPayemtnScreenForFoodSectionTableCell
        
        let fullNameArr = cell.txtExpDate.text!.components(separatedBy: "/")

        // print(fullNameArr as Any)
        let name    = fullNameArr[0]
        let surname = fullNameArr[1]
        
        // print(name as Any)
        // print(surname as Any)
        
        let cardParams = STPCardParams()
        
        cardParams.number       = String(cell.txtCardNumber.text!)
        cardParams.expMonth     = UInt(name)!
        cardParams.expYear      = UInt(surname)!
        cardParams.cvc          = String(cell.txtCVV.text!)
        
        STPAPIClient.shared.createToken(withCard: cardParams) { token, error in
            guard let token = token else {
                // Handle the error
                // print(error as Any)
                // print(error?.localizedDescription as Any)
                ERProgressHud.sharedInstance.hide()
                
                let alert = UIAlertController(title: "Error", message: String(error!.localizedDescription), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
                
                return
            }
            let tokenID = token.tokenId
            // print(tokenID)
            
            // self.fullAndFinalBW(strTokenIdIs: tokenID, strCardNumber: String(cell.txtCardNumber.text!))
            
            self.splitPaymentWBhitBeforeUpdatePaymentForFood(strStripeTokenId: tokenID)
            
        }
        
        // self.fullAndFinalBW(strTokenIdIs: "", strCardNumber: String(cell.txtCardNumber.text!))
        
    }
    
    /*
     ORDER_FOOD_GENERATE_TOKEN
     */
    
    // another webservice hit beofre update payment
    @objc func splitPaymentWBhitBeforeUpdatePaymentForFood(strStripeTokenId:String) {
        
        let urlString = ORDER_FOOD_GENERATE_TOKEN
        var parameters:Dictionary<AnyHashable, Any>!
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
            
                // self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(getTotalPriceOfFood)
                
                // print(getTotalPriceOfFood as Any)
            
                let strTotalAmount = Double(getTotalPriceOfFood)!*100
                // print(strTotalAmount as Any)
                
                let driverAmountIs = Double(getTotalPriceOfFood)!-Double(self.strGetAdminAmount)!
                // print(driverAmountIs as Any)
                
                let str = (String(format:"%.02f", driverAmountIs))
                // print(str as Any)
                
                let d = Double(str)!*100
                // print(d as Any)
                
                let s = (String(format:"%.02f", d))
                // print(s as Any)
                
                let myInt3 = (s as NSString).integerValue
                // print(myInt3 as Any)
                
                let x : Int = person["userId"] as! Int
                let myString = String(x)
                
                
                parameters = [
                    "action"        : "chargeramount",
                    "userId"        : String(myString),
                    "amount"        : strTotalAmount,//String("500"),
                    "tokenID"       : String(strStripeTokenId),
                    "DriverAmount"  : String(myInt3),
                    "AccountNo"     : String(strAccountNumberIs)
                ]
                
        }
        
        print("parameters-------\(String(describing: parameters))")
        
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {
                    
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                    
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                    
                    // var strSuccessAlert : String!
                    // strSuccessAlert = JSON["msg"]as Any as? String
                    
                    if strSuccess == String("success") {
                        print("yes")
                        
                        // ERProgressHud.sharedInstance.hide()
                        
                        /*
                         amount = 3519;
                         captured = 1;
                         card = "card_1JeaqoIudZlr53ucKpBrUU1n";
                         currency = usd;
                         msg = "Payment successfully.";
                         status = success;
                         tokeId = "ch_3Jear6IudZlr53uc2tQ282ep";
                         transactionID = "txn_3Jear6IudZlr53uc2pypZ8tW";
                         */
                        // let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
                        // self.navigationController?.pushViewController(push!, animated: true)
                        
                         var strSuccessAlert2 : String!
                         strSuccessAlert2 = JSON["transactionID"]as Any as? String
                        
                        var strSuccessAlert3 : String!
                        strSuccessAlert3 = JSON["card"]as Any as? String
                        
                        self.paymentWhenDriverAcceptUserRestaurantFood(strTransactionId: strSuccessAlert2, strCardNumber: strSuccessAlert3)
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                        
                        
                    }
                }
                
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                
                ERProgressHud.sharedInstance.hide()
                
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                break
            }
        }
        
    }
    
    @objc func paymentWhenDriverAcceptUserRestaurantFood(strTransactionId:String,strCardNumber:String) {
        // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
      let urlString = BASE_URL_EXPRESS_PLUS
      
      var parameters:Dictionary<AnyHashable, Any>!
      if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
          
          let x : Int = person["userId"] as! Int
          let myString = String(x)
          
              parameters = [
                  "action"           : "updatepaymentfood",
                  "userId"           : String(myString),
                  "foodOrderId"      : String(foodOrderId), // food order group id
                  "transactionId"    : String(strTransactionId),
                  "cardNo"           : String(strCardNumber)
              ]
              
      }
      
      print("parameters-------\(String(describing: parameters))")
                   
      Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
          response in
               
          switch(response.result) {
          case .success(_):
              if let data = response.result.value {

                  let JSON = data as! NSDictionary
                  print(JSON as Any)
                               
                  var strSuccess : String!
                  strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                  if strSuccess == String("success") {
                      print("yes")
                                 
                       ERProgressHud.sharedInstance.hide()
                      
                      let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
                      self.navigationController?.pushViewController(push!, animated: true)
                                 
                                 // var dict: Dictionary<AnyHashable, Any>
                                //  dict = JSON["msg"] as! Dictionary<AnyHashable, Any>
                                
                                // var strSuccess : String!
                                // strSuccess = JSON["msg"]as Any as? String
                             
                                  
                      /*let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
                      self.navigationController?.pushViewController(push!, animated: true)*/
                                  
                                  
//                                    if self.paymentFoodOrService == "iAmFromSpecialOrder" {
//
//                                    } else {
//
//                                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
//                                    self.navigationController?.pushViewController(push!, animated: true)
//
//                                 }
                        
                      
                      // self.convertTokenToTransactionID(strStripeTokenId: strTokenIdIs)
                      
                      
                                 
                  }
                  else {
                      print("no")
                      ERProgressHud.sharedInstance.hide()
                  }
              }

          case .failure(_):
              print("Error message:\(String(describing: response.result.error))")
                               
              ERProgressHud.sharedInstance.hide()
                               
              let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
              let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                  NSLog("OK Pressed")
              }
                               
              alertController.addAction(okAction)
                               
              self.present(alertController, animated: true, completion: nil)
                               
              break
          }
      }
  }
    
    
    @objc func fullAndFinalBW(strTokenIdIs:String!,strCardNumber:String) {
          // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
        print(strTokenIdIs as Any)
        print(strCardNumber as Any)
        
        let urlString = BASE_URL_EXPRESS_PLUS
        
        /*
         [action] => updatepaymentfood
         [userId] => 80
         [foodOrderId] => 108
         [transactionId] => tok_1GqZIABnk7ygV50qQRQQ10ZS
         [cardNo] => 4242424242424242
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
            if paymentFoodOrService == "iAmFromService" {
                
                parameters = [
                    "action"           : "updatepayment",
                    "userId"           : String(myString),
                    "groupId"          : String(servicePaymentBookingId),
                    "serviceAmount"    : String(servicePaymentAmount),
                    "serviceType"      : String(servicePaymentType),
                    "totalAmount"      : String(servicePaymentTotalAmount),
                    "transactionId"    : String(strTokenIdIs),
                    "TIP"              : String(servicePaymentTip)
                ]
                
            } else if paymentFoodOrService == "iAmFromToGet" {
                // toGetTipAmount
                parameters = [
                    "action"            : "togetpaymentupdate",
                    "userId"            : String(myString),
                    "togetrequestsId"   : String(toGetRequestId),
                    "totalAmount"       : String(toGetTotalAmount),
                    "transactionId"     : String(strTokenIdIs),
                    "TIP"               : String(toGetTipAmount),
                ]
                
            } else if paymentFoodOrService == "iAmFromSpecialOrder" {
            
                parameters = [
                    "action"            : "specialpaymentupdate",
                    "userId"            : String(myString),
                    "specialId"         : String(specialFoodSpecialId),
                    "totalAmount"       : String(specialFoodTotalAmount),
                    "transactionId"     : String(strTokenIdIs),
                    "TIP"               : String(specialFoodTip)
                ]
                
            } else if paymentFoodOrService == "iAmFromPayRequestMoney" {
                
                parameters = [
                    "action"            : "moremoneypaymentupdate",
                    "userId"            : String(myString),
                    "togetRequestId"    : String(toGetRequestId),
                    "transactionID"     : String(strTokenIdIs)
                ]
                
            } else {
                
                parameters = [
                    "action"           : "updatepaymentfood",
                    "userId"           : String(myString),
                    "foodOrderId"      : String(foodOrderId), // food order group id
                    "transactionId"    : String(strTokenIdIs),
                    "cardNo"           : String(strCardNumber)
                ]
                
            }
            
        }
        
        print("parameters-------\(String(describing: parameters))")
                     
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
                 
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                 
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                                 
                                   // var strSuccessAlert : String!
                                   // strSuccessAlert = JSON["msg"]as Any as? String
                                 
                    if strSuccess == String("success") {
                        print("yes")
                                   
                         ERProgressHud.sharedInstance.hide()
                        
                        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
                        self.navigationController?.pushViewController(push!, animated: true)
                                   
                                   // var dict: Dictionary<AnyHashable, Any>
                                  //  dict = JSON["msg"] as! Dictionary<AnyHashable, Any>
                                  
                                  // var strSuccess : String!
                                  // strSuccess = JSON["msg"]as Any as? String
                               
                                    
                        /*let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
                        self.navigationController?.pushViewController(push!, animated: true)*/
                                    
                                    
//                                    if self.paymentFoodOrService == "iAmFromSpecialOrder" {
//
//                                    } else {
//
//                                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
//                                    self.navigationController?.pushViewController(push!, animated: true)
//
//                                 }
                          
                        
                        // self.convertTokenToTransactionID(strStripeTokenId: strTokenIdIs)
                        
                        
                                   
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                                 
                ERProgressHud.sharedInstance.hide()
                                 
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                 
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                         UIAlertAction in
                    NSLog("OK Pressed")
                }
                                 
                alertController.addAction(okAction)
                                 
                self.present(alertController, animated: true, completion: nil)
                                 
                break
            }
        }
    }
    
    
    /*
     @objc func convertTokenToTransactionID(strStripeTokenId:String) {
         
         // print(strStripeTokenId as Any)
         
         /*
         action:chargeramount
         userId:25
         amount:100
         tokenID:tok_1GxrrzIudZlr53ucmrz4pyyq
         */
         
         
         
         // serviceAmount
         
         
         
         /*if paymentFoodOrService == "iAmFromService" {
             
             // String(servicePaymentTotalAmount)
             let strTotalAmount = Double(servicePaymentTotalAmount)!*100
             
             if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                 // let str:String = person["role"] as! String
                        parameters = [
                         "action"    : "chargeramount",
                         "userId"    : person["userId"] as Any,
                         "amount"    : strTotalAmount,//String("500"),
                         "tokenID"   : String(strStripeTokenId)
                 ]
             }
             
         } else if paymentFoodOrService == "iAmFromToGet" {
             // toGetTipAmount
             
             
             // String(toGetTotalAmount)
             let strTotalAmount = Double(toGetTotalAmount)!*100
             
             if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                 // let str:String = person["role"] as! String
                        parameters = [
                         "action"    : "chargeramount",
                         "userId"    : person["userId"] as Any,
                         "amount"    : strTotalAmount,//String("500"),
                         "tokenID"   : String(strStripeTokenId)
                 ]
             }
             
             
         } else if paymentFoodOrService == "iAmFromSpecialOrder" {
         
             // String(specialFoodTotalAmount)
             let strTotalAmount = Double(specialFoodTotalAmount)!*100
             
             if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                 // let str:String = person["role"] as! String
                        parameters = [
                         "action"    : "chargeramount",
                         "userId"    : person["userId"] as Any,
                         "amount"    : strTotalAmount,//String("500"),
                         "tokenID"   : String(strStripeTokenId)
                 ]
             }
         }*/
         
         let urlString = ORDER_FOOD_GENERATE_TOKEN
         var parameters:Dictionary<AnyHashable, Any>!
         
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         
         if paymentFoodOrService == "iAmFromService" {
             
             // self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(servicePaymentTotalAmount)
             
             print(servicePaymentTotalAmount as Any)
             let strTotalAmount = Double(servicePaymentTotalAmount)!*100
             print(strTotalAmount as Any)
             
             let driverAmountIs = Double(servicePaymentTotalAmount)!-Double(self.strGetAdminAmount)!
             print(driverAmountIs as Any)
             
             let str = (String(format:"%.02f", driverAmountIs))
             print(str as Any)
             
             let d = Double(str)!*100
             print(d as Any)
             
             let s = (String(format:"%.02f", d))
             print(s as Any)
             
             
              let myInt3 = (s as NSString).integerValue
              print(myInt3 as Any)
             
             
             
             let x : Int = person["userId"] as! Int
             let myString = String(x)
             
             
             parameters = [
                 "action"        : "chargeramount",
                 "userId"        : String(myString),
                 "amount"        : strTotalAmount,//String("500"),
                 "tokenID"       : String(strStripeTokenId),
                 "DriverAmount"  : String(myInt3),
                 "AccountNo"     : String(strAccountNumberIs)
             ]
             
         } else if paymentFoodOrService == "iAmFromToGet" {
             
             // self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(toGetTotalAmount)
             
             print(toGetTotalAmount as Any)
             let strTotalAmount = Double(toGetTotalAmount)!*100
             print(strTotalAmount as Any)
             
             let driverAmountIs = Double(toGetTotalAmount)!-Double(self.strGetAdminAmount)!
             print(driverAmountIs as Any)
             
             let str = (String(format:"%.02f", driverAmountIs))
             print(str as Any)
             
             let d = Double(str)!*100
             print(d as Any)
             
             let s = (String(format:"%.02f", d))
             print(s as Any)
             
             
              let myInt3 = (s as NSString).integerValue
              print(myInt3 as Any)
             
             
             
             let x : Int = person["userId"] as! Int
             let myString = String(x)
             
             
             parameters = [
                 "action"        : "chargeramount",
                 "userId"        : String(myString),
                 "amount"        : strTotalAmount,//String("500"),
                 "tokenID"       : String(strStripeTokenId),
                 "DriverAmount"  : String(myInt3),
                 "AccountNo"     : String(strAccountNumberIs)
             ]
             
         } else if paymentFoodOrService == "iAmFromSpecialOrder" {
             
             // self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(specialFoodTotalAmount)
             
             print(specialFoodTotalAmount as Any)
             let strTotalAmount = Double(specialFoodTotalAmount)!*100
             print(strTotalAmount as Any)
             
             let driverAmountIs = Double(specialFoodTotalAmount)!-Double(self.strGetAdminAmount)!
             print(driverAmountIs as Any)
             
             let str = (String(format:"%.02f", driverAmountIs))
             print(str as Any)
             
             let d = Double(str)!*100
             print(d as Any)
             
             let s = (String(format:"%.02f", d))
             print(s as Any)
             
             
              let myInt3 = (s as NSString).integerValue
              print(myInt3 as Any)
             
             
             
             let x : Int = person["userId"] as! Int
             let myString = String(x)
             
             
             parameters = [
                 "action"        : "chargeramount",
                 "userId"        : String(myString),
                 "amount"        : strTotalAmount,//String("500"),
                 "tokenID"       : String(strStripeTokenId),
                 "DriverAmount"  : String(myInt3),
                 "AccountNo"     : String(strAccountNumberIs)
             ]
             
         } else if paymentFoodOrService == "iAmFromPayRequestMoney" {
             
             // self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(requestMoney)
             
             print(requestMoney as Any)
             let strTotalAmount = Double(requestMoney)!*100
             print(strTotalAmount as Any)
             
             let driverAmountIs = Double(requestMoney)!-Double(self.strGetAdminAmount)!
             print(driverAmountIs as Any)
             
             let str = (String(format:"%.02f", driverAmountIs))
             print(str as Any)
             
             let d = Double(str)!*100
             print(d as Any)
             
             let s = (String(format:"%.02f", d))
             print(s as Any)
             
             
              let myInt3 = (s as NSString).integerValue
              print(myInt3 as Any)
             
             
             
             let x : Int = person["userId"] as! Int
             let myString = String(x)
             
             
             parameters = [
                 "action"        : "chargeramount",
                 "userId"        : String(myString),
                 "amount"        : strTotalAmount,//String("500"),
                 "tokenID"       : String(strStripeTokenId),
                 "DriverAmount"  : String(myInt3),
                 "AccountNo"     : String(strAccountNumberIs)
             ]
             
             
         }  else {
             
             // self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(getTotalPriceOfFood)
             
             print(getTotalPriceOfFood as Any)
             let strTotalAmount = Double(getTotalPriceOfFood)!*100
             print(strTotalAmount as Any)
             
             let driverAmountIs = Double(getTotalPriceOfFood)!-Double(self.strGetAdminAmount)!
             print(driverAmountIs as Any)
             
             let str = (String(format:"%.02f", driverAmountIs))
             print(str as Any)
             
             let d = Double(str)!*100
             print(d as Any)
             
             let s = (String(format:"%.02f", d))
             print(s as Any)
             
             
              let myInt3 = (s as NSString).integerValue
              print(myInt3 as Any)
             
             
             
             let x : Int = person["userId"] as! Int
             let myString = String(x)
             
             
             parameters = [
                 "action"        : "chargeramount",
                 "userId"        : String(myString),
                 "amount"        : strTotalAmount,//String("500"),
                 "tokenID"       : String(strStripeTokenId),
                 "DriverAmount"  : String(myInt3),
                 "AccountNo"     : String(strAccountNumberIs)
             ]
             
         }
         
         }
         
         print("parameters-------\(String(describing: parameters))")
                    
         Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
             response in
                
             switch(response.result) {
             case .success(_):
                 if let data = response.result.value {

                     let JSON = data as! NSDictionary
                     print(JSON as Any)
                                
                     var strSuccess : String!
                     strSuccess = JSON["status"]as Any as? String
                                
                                  // var strSuccessAlert : String!
                                  // strSuccessAlert = JSON["msg"]as Any as? String
                                
                     if strSuccess == String("success") {
                         print("yes")
                                  
                         ERProgressHud.sharedInstance.hide()
                                 
                         let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
                         self.navigationController?.pushViewController(push!, animated: true)
                                 
                     }
                     else {
                         print("no")
                         ERProgressHud.sharedInstance.hide()
                                 
                                 
                     }
                 }

             case .failure(_):
                 print("Error message:\(String(describing: response.result.error))")
                                
                 ERProgressHud.sharedInstance.hide()
                                
                                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                
                                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                        UIAlertAction in
                                        NSLog("OK Pressed")
                                    }
                                
                                alertController.addAction(okAction)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                break
                             }
                        }
     
        }
     */
    
    
    
    
    
    
    @objc func convertTokenToTransactionID(strStripeTokenId:String) {
        
        // print(strStripeTokenId as Any)
        
        /*
         action:chargeramount
         userId:25
         amount:100
         tokenID:tok_1GxrrzIudZlr53ucmrz4pyyq
         */
        
        
        
        // serviceAmount
        
        
        
        /*if paymentFoodOrService == "iAmFromService" {
         
         // String(servicePaymentTotalAmount)
         let strTotalAmount = Double(servicePaymentTotalAmount)!*100
         
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         // let str:String = person["role"] as! String
         parameters = [
         "action"    : "chargeramount",
         "userId"    : person["userId"] as Any,
         "amount"    : strTotalAmount,//String("500"),
         "tokenID"   : String(strStripeTokenId)
         ]
         }
         
         } else if paymentFoodOrService == "iAmFromToGet" {
         // toGetTipAmount
         
         
         // String(toGetTotalAmount)
         let strTotalAmount = Double(toGetTotalAmount)!*100
         
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         // let str:String = person["role"] as! String
         parameters = [
         "action"    : "chargeramount",
         "userId"    : person["userId"] as Any,
         "amount"    : strTotalAmount,//String("500"),
         "tokenID"   : String(strStripeTokenId)
         ]
         }
         
         
         } else if paymentFoodOrService == "iAmFromSpecialOrder" {
         
         // String(specialFoodTotalAmount)
         let strTotalAmount = Double(specialFoodTotalAmount)!*100
         
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         // let str:String = person["role"] as! String
         parameters = [
         "action"    : "chargeramount",
         "userId"    : person["userId"] as Any,
         "amount"    : strTotalAmount,//String("500"),
         "tokenID"   : String(strStripeTokenId)
         ]
         }
         }*/
        
        let urlString = ORDER_FOOD_GENERATE_TOKEN
        var parameters:Dictionary<AnyHashable, Any>!
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            if paymentFoodOrService == "iAmFromService" {
                
                // self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(servicePaymentTotalAmount)
                
                print(servicePaymentTotalAmount as Any)
                let strTotalAmount = Double(servicePaymentTotalAmount)!*100
                print(strTotalAmount as Any)
                
                let driverAmountIs = Double(servicePaymentTotalAmount)!-Double(self.strGetAdminAmount)!
                print(driverAmountIs as Any)
                
                let str = (String(format:"%.02f", driverAmountIs))
                print(str as Any)
                
                let d = Double(str)!*100
                print(d as Any)
                
                let s = (String(format:"%.02f", d))
                print(s as Any)
                
                
                let myInt3 = (s as NSString).integerValue
                print(myInt3 as Any)
                
                
                
                let x : Int = person["userId"] as! Int
                let myString = String(x)
                
                
                parameters = [
                    "action"        : "chargeramount",
                    "userId"        : String(myString),
                    "amount"        : strTotalAmount,//String("500"),
                    "tokenID"       : String(strStripeTokenId),
                    "DriverAmount"  : String(myInt3),
                    "AccountNo"     : String(strAccountNumberIs)
                ]
                
            } else if paymentFoodOrService == "iAmFromToGet" {
                
                // self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(toGetTotalAmount)
                
                print(toGetTotalAmount as Any)
                let strTotalAmount = Double(toGetTotalAmount)!*100
                print(strTotalAmount as Any)
                
                let driverAmountIs = Double(toGetTotalAmount)!-Double(self.strGetAdminAmount)!
                print(driverAmountIs as Any)
                
                let str = (String(format:"%.02f", driverAmountIs))
                print(str as Any)
                
                let d = Double(str)!*100
                print(d as Any)
                
                let s = (String(format:"%.02f", d))
                print(s as Any)
                
                
                let myInt3 = (s as NSString).integerValue
                print(myInt3 as Any)
                
                
                
                let x : Int = person["userId"] as! Int
                let myString = String(x)
                
                
                parameters = [
                    "action"        : "chargeramount",
                    "userId"        : String(myString),
                    "amount"        : strTotalAmount,//String("500"),
                    "tokenID"       : String(strStripeTokenId),
                    "DriverAmount"  : String(myInt3),
                    "AccountNo"     : String(strAccountNumberIs)
                ]
                
            } else if paymentFoodOrService == "iAmFromSpecialOrder" {
                
                // self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(specialFoodTotalAmount)
                
                print(specialFoodTotalAmount as Any)
                let strTotalAmount = Double(specialFoodTotalAmount)!*100
                print(strTotalAmount as Any)
                
                let driverAmountIs = Double(specialFoodTotalAmount)!-Double(self.strGetAdminAmount)!
                print(driverAmountIs as Any)
                
                let str = (String(format:"%.02f", driverAmountIs))
                print(str as Any)
                
                let d = Double(str)!*100
                print(d as Any)
                
                let s = (String(format:"%.02f", d))
                print(s as Any)
                
                
                let myInt3 = (s as NSString).integerValue
                print(myInt3 as Any)
                
                
                
                let x : Int = person["userId"] as! Int
                let myString = String(x)
                
                
                parameters = [
                    "action"        : "chargeramount",
                    "userId"        : String(myString),
                    "amount"        : strTotalAmount,//String("500"),
                    "tokenID"       : String(strStripeTokenId),
                    "DriverAmount"  : String(myInt3),
                    "AccountNo"     : String(strAccountNumberIs)
                ]
                
            } else if paymentFoodOrService == "iAmFromPayRequestMoney" {
                
                // self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(requestMoney)
                
                print(requestMoney as Any)
                let strTotalAmount = Double(requestMoney)!*100
                print(strTotalAmount as Any)
                
                let driverAmountIs = Double(requestMoney)!-Double(self.strGetAdminAmount)!
                print(driverAmountIs as Any)
                
                let str = (String(format:"%.02f", driverAmountIs))
                print(str as Any)
                
                let d = Double(str)!*100
                print(d as Any)
                
                let s = (String(format:"%.02f", d))
                print(s as Any)
                
                
                let myInt3 = (s as NSString).integerValue
                print(myInt3 as Any)
                
                
                
                let x : Int = person["userId"] as! Int
                let myString = String(x)
                
                
                parameters = [
                    "action"        : "chargeramount",
                    "userId"        : String(myString),
                    "amount"        : strTotalAmount,//String("500"),
                    "tokenID"       : String(strStripeTokenId),
                    "DriverAmount"  : String(myInt3),
                    "AccountNo"     : String(strAccountNumberIs)
                ]
                
                
            }  else {
                
                // self.lblPayableAmount.text = "PAYABLE AMOUNT: $ "+String(getTotalPriceOfFood)
                
                print(getTotalPriceOfFood as Any)
                let strTotalAmount = Double(getTotalPriceOfFood)!*100
                print(strTotalAmount as Any)
                
                let driverAmountIs = Double(getTotalPriceOfFood)!-Double(self.strGetAdminAmount)!
                print(driverAmountIs as Any)
                
                let str = (String(format:"%.02f", driverAmountIs))
                print(str as Any)
                
                let d = Double(str)!*100
                print(d as Any)
                
                let s = (String(format:"%.02f", d))
                print(s as Any)
                
                
                let myInt3 = (s as NSString).integerValue
                print(myInt3 as Any)
                
                
                
                let x : Int = person["userId"] as! Int
                let myString = String(x)
                
                
                parameters = [
                    "action"        : "chargeramount",
                    "userId"        : String(myString),
                    "amount"        : strTotalAmount,//String("500"),
                    "tokenID"       : String(strStripeTokenId),
                    "DriverAmount"  : String(myInt3),
                    "AccountNo"     : String(strAccountNumberIs)
                ]
                
            }
            
        }
        
        print("parameters-------\(String(describing: parameters))")
        
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {
                    
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                    
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                    
                    // var strSuccessAlert : String!
                    // strSuccessAlert = JSON["msg"]as Any as? String
                    
                    if strSuccess == String("success") {
                        print("yes")
                        
                        ERProgressHud.sharedInstance.hide()
                        
                        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPSuccessPlacedOrderId") as? UPSuccessPlacedOrder
                        self.navigationController?.pushViewController(push!, animated: true)
                        
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                        
                        
                    }
                }
                
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                
                ERProgressHud.sharedInstance.hide()
                
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                break
            }
        }
        
    }
    
    
}


//MARK:- TABLE VIEW -
extension UPPayemtnScreenForFoodSection: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UPPayemtnScreenForFoodSectionTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UPPayemtnScreenForFoodSectionTableCell
        //
        cell.backgroundColor = .white
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.txtCardNumber.delegate = self
        cell.txtExpDate.delegate = self
        cell.txtCVV.delegate = self
        
        cell.txtCardNumber.addTarget(self, action: #selector(UPPaymentScreen.textFieldDidChange(_:)), for: .editingChanged)
        cell.txtExpDate.addTarget(self, action: #selector(UPPaymentScreen.textFieldDidChange2(_:)), for: .editingChanged)
        
        return cell
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPPayemtnScreenForFoodSectionTableCell
            
         self.lblCardNumberHeading.text! = cell.txtCardNumber.text!
        
    }
    @objc func textFieldDidChange2(_ textField: UITextField) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPPayemtnScreenForFoodSectionTableCell
            
         self.lblEXPDate.text! = cell.txtExpDate.text!
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! UPPayemtnScreenForFoodSectionTableCell
        
        if textField == cell.txtCardNumber {
            print(self.lblCardNumberHeading.text!.count+1 as Any)
            
            if self.lblCardNumberHeading.text!.count+1 == 2 {
                print("check card")
            } else {
                print("do not check card")
            }
            
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 16
        }
        
        if textField == cell.txtExpDate {
            if string == "" {
                return true
            }

            
            let currentText = textField.text! as NSString
            let updatedText = currentText.replacingCharacters(in: range, with: string)

            textField.text = updatedText
            let numberOfCharacters = updatedText.count
            
            if numberOfCharacters == 2 {
                textField.text?.append("/")
            }
            self.lblEXPDate.text! = cell.txtExpDate.text!
        }
        
       if textField == cell.txtCVV {
           
           guard let textFieldText = textField.text,
               let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                   return false
           }
           let substringToReplace = textFieldText[rangeOfTextToReplace]
           let count = textFieldText.count - substringToReplace.count + string.count
        
        /*if self.strWhatCardIam == "amex" {
            return count <= 4
        } else {
            return count <= 3
        }*/
        return count <= 3
       }
        
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
    
}

extension UPPayemtnScreenForFoodSection: UITableViewDelegate {
    
}

