//
//  UPFood.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire
import SDWebImage

// MARK:- LOCATION -
import CoreLocation

class UPFood: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {

    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
    
    var page : Int! = 1
    var loadMore : Int! = 1;
    
    let locationManager = CLLocationManager()
    
    let cellReuseIdentifier = "uPFoodTableCell"
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "FOOD"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            
        }
    }
    @IBOutlet weak var btnSearch:UIButton!
    @IBOutlet weak var txtSearch:UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = .white
        
        txtSearch.delegate = self
        
        btnSearch.addTarget(self, action: #selector(searchWB), for: .touchUpInside)
        
        
        self.strSaveLatitude = "0"
        self.strSaveLongitude = "0"
        
        
        /*if let myLoadedString = UserDefaults.standard.string(forKey: "keySetToBackOrMenu") {
            print(myLoadedString)
            
            if myLoadedString == "backOrMenu" {
                // menu
                self.btnBack.setImage(UIImage(named: "sMenu"), for: .normal)
                self.sideBarMenuClick()
            } else {
                // back
                self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
            }
        } else {
            // back
            self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }*/
        
        self.sideBarMenuClick()
        self.restaurantList(pageNumber: 1)
        
        
        

 
        
        self.iAmHereForLocationPermission()
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    // MARK:- GET CUSTOMER LOCATION -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! PDBagPurchaseTableCell
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            //print(self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            print("locality ==> "+locality as Any) // sector 10 dwarka
            
            print(self.strSaveCountryName as Any) // india
            print(self.strSaveStateName as Any) // new delhi
            print(self.strSaveZipcodeName as Any) // 110075
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            
            // self.restaurantList(pageNumber: 1)
            // self.findMyStateTaxWB()
        }
    }
    
    /*@objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }*/
    
    @objc func sideBarMenuClick() {
        
        UserDefaults.standard.set("", forKey: "keySetToBackOrMenu")
        UserDefaults.standard.set(nil, forKey: "keySetToBackOrMenu")
        
        self.view.endEditing(true)
        
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }

    /*func scrollViewDidScroll(_ scrollView: UIScrollView) {
                
        if scrollView == self.tbleView {
            let isReachingEnd = scrollView.contentOffset.y >= 0
                && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
            if(isReachingEnd) {
                if(loadMore == 1) {
                    loadMore = 0
                    page += 1
                    print(page as Any)
                    
                    self.restaurantList(pageNumber: page)
                    
                }
            }
        }
    }*/
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @objc func restaurantList(pageNumber:Int) {
           
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
                  
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        /*
         [action] => userlist
         [TYPE] => Restaurant
         [keyword] =>
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
    // userlist
        parameters = [
            "action"        : "resturentlistnew",
            "TYPE"          : String("Restaurant"),
            "pageNo"        : pageNumber,
            "latitude"      : "",/*String(self.strSaveLatitude)*/
            "longitude"     : ""/*String(self.strSaveLongitude)*/,
                          
        ]
                 
        print("parameters-------\(String(describing: parameters))")
                      
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
                  
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                  
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                    if strSuccess == String("success") {
                        print("yes")
                                    
                        ERProgressHud.sharedInstance.hide()
                                    
                                    // var dict: Dictionary<AnyHashable, Any>
                                    // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                   
                        var ar : NSArray!
                        ar = (JSON["data"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                    
                        self.tbleView.delegate = self
                        self.tbleView.dataSource = self
                        self.tbleView.reloadData()
                        self.loadMore = 1
                                    
                                    
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                                   
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                
                ERProgressHud.sharedInstance.hide()
                                  
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                                  
                alertController.addAction(okAction)
                                  
                self.present(alertController, animated: true, completion: nil)
                                  
                break
            }
        }
       
    }
    
    
    // MARK:- WEBSERVICE ( SEARCH ) -
    @objc func searchWB() {
        self.view.endEditing(true)
        
        self.arrListOfAllMyOrders.removeAllObjects()
     
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
        let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => userlist
      [TYPE] => Restaurant
      [keyword] => jjhhhgggvvv
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
     // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
        parameters = [
            "action"    : "resturentlist",
            "TYPE"      : String("Restaurant"),
            "keyword"   : String(self.txtSearch.text!),
            "latitude"  : String(self.strSaveLatitude),
            "longitude" : String(self.strSaveLongitude),
                       
             ]
     // }
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
               
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                                // print(JSON as Any)
                               
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                    if strSuccess == String("success") {
                        print("yes")
                        ERProgressHud.sharedInstance.hide()
                                
                        var ar : NSArray!
                        ar = (JSON["data"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                 
                        self.tbleView.delegate = self
                        self.tbleView.dataSource = self
                        self.tbleView.reloadData()
                        self.loadMore = 1;
                                 
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                                
                                
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                               
                ERProgressHud.sharedInstance.hide()
                               
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                               
                alertController.addAction(okAction)
                               
                self.present(alertController, animated: true, completion: nil)
                               
                break
            }
        }
    
    }
    
    
}

//MARK:- TABLE VIEW -
extension UPFood: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UPFoodTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UPFoodTableCell
        
        cell.backgroundColor = .white
       
        // cell.lblTitle.text = titleArray[indexPath.row]
       
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        print(item as Any)
        
        /*
         address = "Morar, Gwalior, Madhya Pradesh, India";
         contactNumber = 7997797979;
         created = "2020-05-13 18:16:00";
         designation = "";
         device = "";
         deviceToken = "";
         email = "krishna@gmail.com";
         experence = "";
         foodTag = Veg;
         fullName = krish;
         image = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1589374262IMG_20200429_152659.jpg";
         role = Restaurant;
         userId = 109;
         zipCode = "";
         */
        
        // AVGRating
        
        // print(item!["AVGRating"] as! String)
        
        // star_yellow
        // star_white
        
        if item!["AVGRating"] as! String == "0" {
            
            cell.imgStarOne.image = UIImage(named: "star_yellow")
            cell.imgStarTwo.image = UIImage(named: "star_white")
            cell.imgStarThree.image = UIImage(named: "star_white")
            cell.imgStarFour.image = UIImage(named: "star_white")
            cell.imgStarFive.image = UIImage(named: "star_white")
            
        } else if item!["AVGRating"] as! String == "1" {
            
            cell.imgStarOne.image = UIImage(named: "star_yellow")
            cell.imgStarTwo.image = UIImage(named: "star_white")
            cell.imgStarThree.image = UIImage(named: "star_white")
            cell.imgStarFour.image = UIImage(named: "star_white")
            cell.imgStarFive.image = UIImage(named: "star_white")
            
        } else if item!["AVGRating"] as! String == "2" {
            
            cell.imgStarOne.image = UIImage(named: "star_yellow")
            cell.imgStarTwo.image = UIImage(named: "star_yellow")
            cell.imgStarThree.image = UIImage(named: "star_white")
            cell.imgStarFour.image = UIImage(named: "star_white")
            cell.imgStarFive.image = UIImage(named: "star_white")
            
        } else if item!["AVGRating"] as! String == "3" {
            
            cell.imgStarOne.image = UIImage(named: "star_yellow")
            cell.imgStarTwo.image = UIImage(named: "star_yellow")
            cell.imgStarThree.image = UIImage(named: "star_yellow")
            cell.imgStarFour.image = UIImage(named: "star_white")
            cell.imgStarFive.image = UIImage(named: "star_white")
            
        } else if item!["AVGRating"] as! String == "4" {
            
            cell.imgStarOne.image = UIImage(named: "star_yellow")
            cell.imgStarTwo.image = UIImage(named: "star_yellow")
            cell.imgStarThree.image = UIImage(named: "star_yellow")
            cell.imgStarFour.image = UIImage(named: "star_yellow")
            cell.imgStarFive.image = UIImage(named: "star_white")
            
        } else if item!["AVGRating"] as! String == "4" {
            
            cell.imgStarOne.image = UIImage(named: "star_yellow")
            cell.imgStarTwo.image = UIImage(named: "star_yellow")
            cell.imgStarThree.image = UIImage(named: "star_yellow")
            cell.imgStarFour.image = UIImage(named: "star_yellow")
            cell.imgStarFive.image = UIImage(named: "star_yellow")
            
        }
        
        // print(type(of: item!["AVGRating"]))
        
        // cell.lblOrderId.text = (item!["OrderDate"] as! String)
        cell.lblMenuText.text = " MENU "
        
        if item!["foodTag"] as! String == "Veg" {
            cell.imgVegNonveg.image = UIImage(named: "veg")
        } else if item!["foodTag"] as! String == "" {
            cell.imgVegNonveg.image = UIImage(named: "veg")
        } else {
            cell.imgVegNonveg.image = UIImage(named: "nonVeg")
        }
        
        cell.lblUserName.text = (item!["fullName"] as! String)
        cell.lblEmail.text = (item!["email"] as! String)
        
        cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        if let url = (item!["image"] as! String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            
         // cell.imgFoodProfile.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "food1"), options: [.continueInBackground, .progressiveDownload])
             cell.imgProfile.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "food1"))
            
        }
        // cell.imgProfile.sd_setImage(with: URL(string: (item!["image"] as! String)), placeholderImage: UIImage(named: "food1"))
        
        
        // cell.imgProfile.layer.shadowPath = UIBezierPath(roundedRect: containerView.bounds, cornerRadius: 6).cgPath
        
        cell.backgroundColor = .white
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPRestaurantProfileId") as? UPRestaurantProfile
        settingsVCId!.dictGetRestaurantFoodListing = item as NSDictionary?
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}

extension UPFood: UITableViewDelegate {
    
}

