//
//  UPFoodTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UPFoodTableCell: UITableViewCell {

    @IBOutlet weak var lblEmail:UILabel!
    @IBOutlet weak var lblUserName:UILabel!
    
    @IBOutlet weak var lblMenuText:UILabel! {
        didSet {
            lblMenuText.textColor = .white
            lblMenuText.layer.cornerRadius = 6
            lblMenuText.clipsToBounds = true
            lblMenuText.backgroundColor = .systemGreen
        }
    }
    
    @IBOutlet weak var imgProfile:UIImageView! {
        didSet {
            imgProfile.layer.cornerRadius = 12
            imgProfile.clipsToBounds = true
            imgProfile.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var imgVegNonveg:UIImageView!
    
    // 240 162 58
    @IBOutlet weak var imgStarOne:UIImageView! {
        didSet {
            imgStarOne.layer.cornerRadius = 10
            imgStarOne.clipsToBounds = true
            imgStarOne.image = UIImage(systemName: "star")
            imgStarOne.tintColor = UIColor.init(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var imgStarTwo:UIImageView! {
        didSet {
            imgStarTwo.layer.cornerRadius = 10
            imgStarTwo.clipsToBounds = true
        }
    }
    @IBOutlet weak var imgStarThree:UIImageView! {
        didSet {
            imgStarThree.layer.cornerRadius = 10
            imgStarThree.clipsToBounds = true
        }
    }
    @IBOutlet weak var imgStarFour:UIImageView! {
        didSet {
            imgStarFour.layer.cornerRadius = 10
            imgStarFour.clipsToBounds = true
        }
    }
    @IBOutlet weak var imgStarFive:UIImageView! {
        didSet {
            imgStarFive.layer.cornerRadius = 10
            imgStarFive.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
