//
//  PDInProgress.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SDWebImage

import Firebase

class PDInProgress: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    var dictOne:NSDictionary!
    var dictTwo:NSDictionary!
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    let annotation = MKPointAnnotation()
    let annotation2 = MKPointAnnotation()
    
    // MARK:- DRIVER -
    var driverLatitute:String!
    var driverLongitude:String!
    
    // MARK:- RESTAURANT -
    var restaurantLatitude:String!
    var restaurantLongitude:String!
    
    var strPassImgString1:String!
    var imageStr1:String!
    var imgData1:Data!
    
    @IBOutlet weak var imgUploadRecipt:UIImageView!
    
    @IBOutlet weak var mapView:MKMapView!
    
    var dictData20: Dictionary<AnyHashable, Any> = [:]
    
    
    
    var strPassImgStringDel:String!
    var imageStrDel:String!
    var imgDataDel:Data!
    @IBOutlet weak var imgDelivered:UIImageView!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "IN PROGRESS"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnMarkAsDelivery:UIButton! {
        didSet {
            btnMarkAsDelivery.backgroundColor = .systemGreen
            btnMarkAsDelivery.setTitleColor(.white, for: .normal)
            btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
            btnMarkAsDelivery.layer.cornerRadius = 18
            btnMarkAsDelivery.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var viewBottomUpperView:UIView! {
        didSet {
            viewBottomUpperView.layer.cornerRadius = 18
            viewBottomUpperView.clipsToBounds = true
            viewBottomUpperView.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var lblTotalDistance:UILabel!
    @IBOutlet weak var lblCustomerName:UILabel!
    @IBOutlet weak var lblEstTime:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Create UserDefaults
        let defaults = UserDefaults.standard
        defaults.set("callOnlyOne", forKey: "keyOne")
        
        btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        btnMarkAsDelivery.addTarget(self, action: #selector(markAsDelivered), for: .touchUpInside)
        
        self.btnMarkAsDelivery.isUserInteractionEnabled = false
        self.btnMarkAsDelivery.backgroundColor = .lightGray
        self.btnMarkAsDelivery.setTitle("Towards Restaurant", for: .normal)
        
        print(dictOne as Any)
        print(dictTwo as Any)
        
        /*
         Optional({
         
         AdminAmount = 3;
         DeliveryImage = "";
         EstTime = "15 Minutes";
         Mile = "4 Mile";
         PaymentStatus = 2;
         TIP = 10;
         address = "Ram Prastha, Ghaziabad, Ghaziabad";
         assignDriver = 471;
         cardNo = 4242424242424242;
         city = Ghaziabad;
         couponCode = "";
         created = "2021-07-01 13:21:00";
         deliveryFee = "";
         deliveryLat = "28.663377337093603";
         deliveryLong = "77.32384954196793";
         discount = 0;
         driverAVG = "2.5";
         driverContactNumber = 9313526285;
         driverId = 471;
         driverImage = "https://www.expressplusnow.com/appbackend/img/uploads/users/1624435691images(13).jpeg";
         driverName = driver;
         driverStripeAccount = "acct_1IYnbgEFJmGeJLZY";
         foodDetails =     (
                     {
                 id = 163;
                 name = "Mexi Wrap - Potato";
                 price = 8;
                 quantity = 1;
                 resturentId = 511;
             },
                     {
                 id = 164;
                 name = "Mini Quesadilla - Cheese";
                 price = 2;
                 quantity = 1;
                 resturentId = 511;
             }
         );
         foodDetailsNew =     (
                     {
                 categoryId = 70;
                 description = "Mexi Wrap - Potato";
                 foodName = "Mexi Wrap - Potato";
                 foodTag = Veg;
                 foodType = "";
                 "image_1" = "https://www.expressplusnow.com/appbackend/img/uploads/foods/1623931531_Mexi Wrap - Potato.jpg";
                 "image_2" = "";
                 "image_3" = "";
                 "image_4" = "";
                 "image_5" = "";
                 menuId = 163;
                 price = 11;
                 quantity = 1;
                 resturentId = 511;
                 specialPrice = 8;
             },
                     {
                 categoryId = 67;
                 description = "";
                 foodName = "Mini Quesadilla - Cheese";
                 foodTag = Veg;
                 foodType = "";
                 "image_1" = "https://www.expressplusnow.com/appbackend/img/uploads/foods/1623931572_Mini Quesadilla - Cheese.jpg";
                 "image_2" = "";
                 "image_3" = "";
                 "image_4" = "";
                 "image_5" = "";
                 menuId = 164;
                 price = 6;
                 quantity = 1;
                 resturentId = 511;
                 specialPrice = 2;
             }
         );
         foodId = "";
         foodOrderGroupId = 620;
         foodorderId = 634;
         imageUpload = "https://www.expressplusnow.com/appbackend/img/uploads/users/16251394501625139450012.jpg";
         landmark = "";
         name = "purnima ";
         noContact = "";
         phone = 5623885212;
         resturentAddress = "";
         resturentId = 511;
         resturentLatitude = "";
         resturentLongitude = "";
         resturentName = "Taco bell";
         salesTax = 0;
         specialNote = "";
         state = Ghaziabad;
         status = 2;
         storecity = "";
         totalAmount = 26;
         transactionFee = "2.11";
         userAVG = 0;
         userId = 529;
         userName = ios12;
         whatYouWant = "";
         workPlace = Work;
         zipcode = 201011;
         })
         (lldb)
         */
        
        // print(dictTwo as Any)
        
        /*
         Optional({
             TIP = 10;
             cardNo = 4242424242424242;
             couponCode = "";
             created = "2020-09-12 22:21:00";
             discount = 0;
             driverName = iamdriver;
             foodDetails =     (
                         {
                     id = 85;
                     name = testi;
                     price = 35;
                     quantity = 1;
                     resturentId = 271;
                 }
             );
             foodId = "";
             foodorderId = 196;
             noContact = "";
             resturentAddress = "Gwalior, Madhya Pradesh, India";
             resturentId = 271;
             resturentImage = "https://www.expressplusnow.com/appbackend/img/uploads/users/1599808685Screenshot_2020-08-28-08-42-29-56.png";
             resturentName = 6d3;
             specialNote = "";
             status = 1;
             totalAmount = 45;
             userAddress = "";
             userId = 291;
             userImage = "";
             userName = cust;
             whatYouWant = "";
             workPlace = Work;
         })
         (lldb) 
         */
        
        
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            if (person["role"] as! String) == "Member" {
                
                self.dataParsingForCustomer()

            } else {
                
                self.dataParsingForDriver()
                
            }
            
        }
        
        
        
        
        // MARK:- 1 ( MAP ) -
        self.locManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            self.locManager.delegate = self
            self.locManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locManager.startUpdatingLocation()
            print("UPDATE UPDATE")
        }
       
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways) {
            print("")
        }
        
    }
    
    @objc func dataParsingForCustomer() {
        
        // customer name
        self.lblCustomerName.text = (dictOne["userName"] as! String)
        
        // est time
        self.lblEstTime.text = (dictOne["EstTime"] as! String)
        
        // restaurant
        self.restaurantLatitude     = (dictOne["deliveryLat"] as! String)
        self.restaurantLongitude    = (dictOne["deliveryLong"] as! String)
          
        // address
        self.lblAddress.text = (dictOne["address"] as! String)
        
        // driver
        self.driverLatitute     = (dictOne["resturentLatitude"] as! String)
        self.driverLongitude    = (dictOne["resturentLongitude"] as! String)
        
        
        
        self.btnMarkAsDelivery.setTitle("WAITING FOR ORDER DELIVER", for: .normal)
        self.btnMarkAsDelivery.isUserInteractionEnabled = false
        self.btnMarkAsDelivery.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        
        if self.dictOne["imageUpload"] as! String == "" {
            
        } else {
            
            
            self.imgUploadRecipt.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            
            if let url = (dictOne!["imageUpload"] as! String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                
                self.imgUploadRecipt.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "logo"))
                
            }
        }
    }
    
    @objc func dataParsingForDriver() {
        
        // customer name
        self.lblCustomerName.text = (dictOne["userName"] as! String)
        
        // est time
        self.lblEstTime.text = (dictOne["EstTime"] as! String)
        
        // restaurant
        self.restaurantLatitude     = (dictOne["deliveryLat"] as! String)
        self.restaurantLongitude    = (dictOne["deliveryLong"] as! String)
          
        // address
        self.lblAddress.text = (dictOne["address"] as! String)
        
        // driver
        self.driverLatitute     = (dictOne["resturentLatitude"] as! String)
        self.driverLongitude    = (dictOne["resturentLongitude"] as! String)
        
        if self.dictOne["imageUpload"] as! String == "" {
            
        } else {
            
            
            if self.dictOne["status"] is String {
                                
                print("Yes, it's a String")
                
                if self.dictOne["status"] as! String == "1" {
                    // 1
                    self.btnMarkAsDelivery.isUserInteractionEnabled = true
                    self.btnMarkAsDelivery.backgroundColor = .systemYellow
                    self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                    
                    self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                    
                } else  if self.dictOne["status"] as! String == "2" {
                    // 2 : on the way
                    self.btnMarkAsDelivery.isUserInteractionEnabled = true
                    self.btnMarkAsDelivery.backgroundColor = .systemOrange
                    self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                    
                    self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB), for: .touchUpInside)
                    
                } else if self.dictOne["status"] as! String == "4" {
                    // 4
                    self.btnMarkAsDelivery.backgroundColor = .systemGreen
                    self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                    self.btnMarkAsDelivery.isUserInteractionEnabled = false
                }
                
            } else if self.dictOne["status"] is Int {
                
                print("It is Integer")
                let x2 : Int = (self.dictOne["status"] as! Int)
                let myString2 = String(x2)
                
                
                if myString2 == "1" {
                    // 1
                    self.btnMarkAsDelivery.isUserInteractionEnabled = true
                    self.btnMarkAsDelivery.backgroundColor = .systemYellow
                    self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                    
                    self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                    
                } else if myString2 == "2" {
                    // 2 : on the way
                    
                    self.btnMarkAsDelivery.isUserInteractionEnabled = true
                    
                    self.btnMarkAsDelivery.backgroundColor = .systemOrange
                    self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                    
                    self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB), for: .touchUpInside)
                    
                } else if myString2 == "4" {
                    // 4
                    self.btnMarkAsDelivery.backgroundColor = .systemGreen
                    self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                    self.btnMarkAsDelivery.isUserInteractionEnabled = false
                }
                
            } else {
                
                print("i am ")
                let temp:NSNumber = self.dictOne["status"] as! NSNumber
                let tempString = temp.stringValue
                
                
                if tempString == "1" {
                    // 1
                    self.btnMarkAsDelivery.isUserInteractionEnabled = true
                    self.btnMarkAsDelivery.backgroundColor = .systemYellow
                    self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                    
                    self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                    
                } else  if tempString == "2" {
                    // 2 : on the way
                    self.btnMarkAsDelivery.isUserInteractionEnabled = true
                    self.btnMarkAsDelivery.backgroundColor = .systemOrange
                    self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                    
                    self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB), for: .touchUpInside)
                    
                } else if tempString == "4" {
                    // 4
                    self.btnMarkAsDelivery.backgroundColor = .systemGreen
                    self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                    self.btnMarkAsDelivery.isUserInteractionEnabled = false
                }
                
            }
            
            
            
            // self.btnMarkAsDelivery.backgroundColor = .systemGreen
            // self.btnMarkAsDelivery.isUserInteractionEnabled = true
            
            self.imgUploadRecipt.sd_setImage(with: URL(string: (dictOne["imageUpload"] as! String)), placeholderImage: UIImage(named: "logo"))
            
        }
        
        
    }
    @objc func backClickMethod() {
        
        let defaults = UserDefaults.standard
        defaults.set("", forKey: "keyOne")
        defaults.set(nil, forKey: "keyOne")
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK:- 2 ( MAP ) -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
              
        // set firebase
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            let x2 : Int = person["userId"] as! Int
            let driverId = String(x2)
            
            let x3 : Int = dictOne["foodOrderGroupId"] as! Int
            let driverId3 = String(x3)
            
            let subChildId = driverId3+"@"+driverId
            
            let ref = Database.database().reference().child("liveTracking").child(subChildId).childByAutoId()
            
            let message = ["adress"         : (dictOne["address"] as! String),
                           "bookingId"      : String(driverId3),
                           "driverId"       : String(driverId),
                           "lat"            : "\(manager.location!.coordinate.latitude)",
                           "longs"          : "\(manager.location!.coordinate.longitude)"] as [String : Any]
            
            
            ref.setValue(message)
            
            print(message as Any)
        
                            //print("**********************")
                            //print("Long \(manager.location!.coordinate.longitude)")
                            //print("Lati \(manager.location!.coordinate.latitude)")
                            //print("Alt \(manager.location!.altitude)")
                            //print("Speed \(manager.location!.speed)")
                            //print("Accu \(manager.location!.horizontalAccuracy)")
                            //print("**********************")
                
                //print(Double((vendorLatitute as NSString).doubleValue))
                //print(Double((vendorLongitute as NSString).doubleValue))
                
        /*
         // restaurant
         self.restaurantLatitude     = (dict["deliveryLat"] as! String)
         self.restaurantLongitude    = (dict["deliveryLong"] as! String)
           
         // driver
         self.driverLatitute     = (dict["resturentLatitude"] as! String)
         self.driverLongitude    = (dict["resturentLongitude"] as! String)
         */
        
        let restaurantLatitudeDouble    = Double(self.restaurantLatitude)
        let restaurantLongitudeDouble   = Double(self.restaurantLongitude)
        let driverLatitudeDouble        = Double("\(manager.location!.coordinate.latitude)") //Double(self.driverLatitute)
        let driverLongitudeDouble       = Double("\(manager.location!.coordinate.longitude)") // Double(self.driverLongitude)
        
        let coordinate₀ = CLLocation(latitude: restaurantLatitudeDouble!, longitude: restaurantLongitudeDouble!)
            let coordinate₁ = CLLocation(latitude: driverLatitudeDouble!, longitude: driverLongitudeDouble!)
        
    /************************************** RESTAURANT LATITUTDE AND LONGITUDE  ********************************/
                        // first location
        let sourceLocation = CLLocationCoordinate2D(latitude: restaurantLatitudeDouble!, longitude: restaurantLongitudeDouble!)
    /********************************************************************************************************************/
                                            

    /************************************* DRIVER LATITUTDE AND LINGITUDE ******************************************/
                        // second location
                        let destinationLocation = CLLocationCoordinate2D(latitude: driverLatitudeDouble!, longitude: driverLongitudeDouble!)
    /********************************************************************************************************************/
          
                //print(sourceLocation)
                //print(destinationLocation)
                 
                        let sourcePin = customPin(pinTitle: "You", pinSubTitle: "", location: sourceLocation)
                        let destinationPin = customPin(pinTitle: "Driver", pinSubTitle: "", location: destinationLocation)

    /***************** REMOVE PREVIUOS ANNOTATION TO GENERATE NEW ANNOTATION *******************************************/
                    self.mapView.removeAnnotations(self.mapView.annotations)
    /********************************************************************************************************************/
                
        self.mapView.addAnnotation(sourcePin)
        self.mapView.addAnnotation(destinationPin)
                       
        let sourcePlaceMark = MKPlacemark(coordinate: sourceLocation)
        let destinationPlaceMark = MKPlacemark(coordinate: destinationLocation)
                                                   
        let directionRequest = MKDirections.Request()
        directionRequest.source = MKMapItem(placemark: sourcePlaceMark)
        directionRequest.destination = MKMapItem(placemark: destinationPlaceMark)
        directionRequest.transportType = .automobile
                               
        let directions = MKDirections(request: directionRequest)
        directions.calculate { [self] (response, error) in
            guard let directionResonse = response else {
                if let error = error {
                    print("we have error getting directions==\(error.localizedDescription)")
                }
                return
            }
                                   
    /***************** REMOVE PREVIUOS POLYLINE TO GENERATE NEW POLYLINE *******************************/
            let overlays = self.mapView.overlays
            self.mapView.removeOverlays(overlays)
    /************************************************************************************/
    
                                
    /***************** GET DISTANCE BETWEEN TWO CORDINATES *******************************/
                             
                                let distanceInMeters = coordinate₀.distance(from: coordinate₁)
                                // print(distanceInMeters as Any)
                                
                                // remove decimal
                                let distanceFloat: Double = (distanceInMeters as Any as! Double)
                                // print(distanceFloat as Any)
                                // self.lblDistance.text = (String(format: "Distance : %.0f Miles away", distanceFloat/1609.344))
                                self.lblTotalDistance.text = (String(format: "Distance : %.0f Miles away", distanceFloat/1609.344))
                                
                                // print(distanceFloat/1609.344)
                                // print(String(format: "Distance : %.0f Miles away", distanceFloat/1609.344))
                                
                                
                                let s:String = String(format: "%.0f",distanceFloat/1609.344)
                                // print(s as Any)
                                
                                let myString1 = s
                                let myInt1 = Int(myString1)
                                
                                if myInt1! < 5 {
                                    
                                    
                                    if self.dictOne["imageUpload"] as! String == "" {
                                        
                                        self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                        self.btnMarkAsDelivery.setTitle("Upload Receipt", for: .normal)
                                        self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                        self.btnMarkAsDelivery.addTarget(self, action: #selector(self.uploadReceiptClickMethod), for: .touchUpInside)
                                        
                                    } else {
                                        
                                        
                                        if self.dictOne["status"] is String {
                                                            
                                            print("Yes, it's a String")
                                            // self.changesBookingIdis = (dictGetSpecialOrderData["bookingId"] as! String);
                                            // myString12 = (dictGetSpecialOrderData["status"] as! String)
                                            
                                            if self.dictOne["status"] as! String == "1" {
                                                // 1
                                                self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                self.btnMarkAsDelivery.backgroundColor = .systemYellow
                                                self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                                                
                                                self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                                                
                                            } else  if self.dictOne["status"] as! String == "2" {
                                                // 2 : on the way
                                                self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                                self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                                
                                                self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB), for: .touchUpInside)
                                                
                                            } else if self.dictOne["status"] as! String == "4" {
                                                // 4
                                                self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                                self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                                                self.btnMarkAsDelivery.isUserInteractionEnabled = false
                                            }
                                            
                                        } else if self.dictOne["status"] is Int {
                                            
                                            print("It is Integer")
                                            let x2 : Int = (self.dictOne["status"] as! Int)
                                            let myString2 = String(x2)
                                            // self.dictGetSpecialOrderData = String(myString2)
                                            // myString12 = String(myString2)
                                            
                                            if myString2 == "1" {
                                                // 1
                                                self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                self.btnMarkAsDelivery.backgroundColor = .systemYellow
                                                self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                                                
                                                self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                                                
                                            } else if myString2 == "2" {
                                                // 2 : on the way
                                                self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                                self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                                
                                                self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB), for: .touchUpInside)
                                                
                                            } else if myString2 == "4" {
                                                // 4
                                                self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                                self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                                                self.btnMarkAsDelivery.isUserInteractionEnabled = false
                                            }
                                            
                                        } else {
                                            
                                            print("i am ")
                                            let temp:NSNumber = self.dictOne["status"] as! NSNumber
                                            let tempString = temp.stringValue
                                            // self.changesBookingIdis = String(tempString)
                                            // myString12 = String(tempString)
                                            
                                            
                                            if tempString == "1" {
                                                // 1
                                                self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                self.btnMarkAsDelivery.backgroundColor = .systemYellow
                                                self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                                                
                                                self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                                                
                                            } else  if tempString == "2" {
                                                // 2 : on the way
                                                self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                                self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                                
                                                self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB), for: .touchUpInside)
                                                
                                            } else if tempString == "4" {
                                                // 4
                                                self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                                self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                                                self.btnMarkAsDelivery.isUserInteractionEnabled = false
                                            }
                                            
                                        }
                                        
                                        
                                        
                                        // self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                        // self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                        
                                        self.imgUploadRecipt.sd_setImage(with: URL(string: (self.dictOne["imageUpload"] as! String)), placeholderImage: UIImage(named: "logo"))
                                        
                                        
                                        
                                        
                                        
                                        
                                    }
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    /*
                                    if self.dictOne["imageUpload"] as! String == "" {
                                        
                                        self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                        self.btnMarkAsDelivery.setTitle("Upload Receipt", for: .normal)
                                        self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                        self.btnMarkAsDelivery.addTarget(self, action: #selector(self.uploadReceiptClickMethod), for: .touchUpInside)
                                        
                                    } else {
                                        
                                        self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                        self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                        self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                        // self.btnMarkAsDelivery.addTarget(self, action: #selector(self.uploadReceiptClickMethod), for: .touchUpInside)
                                        
                                    }
                                    */
                                    
                                    
                                } else {
                                    print("outside")
                                }
                                  
                                
                                
                                
                                
                                
    /************************************************************************************/
                                
    /***************** GENERATE NEW POLYLINE *******************************/
                                let route = directionResonse.routes[0]
                                self.mapView.addOverlay(route.polyline, level: .aboveRoads)
                                let rect = route.polyline.boundingMapRect
                                self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
    /************************************************************************************/
                           
                               }
        self.mapView.delegate = self
        print("update location after 5 sec")
        
        
        }
                        // self.locManager.stopUpdatingLocation()
                
                // speed = distance / time
    }
    
    @objc func uploadReceiptClickMethod() {
        if self.btnMarkAsDelivery.titleLabel?.text == "Mark as Delivered" {
            
            self.markAsDeliveredWB()
        } else if self.btnMarkAsDelivery.titleLabel?.text == "On the Way" {
            
            self.markAsDeliveredWB2()
            //
        } else {
           
            let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)

            alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
                print("User click Approve button")
                self.openCamera1()
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
                print("User click Edit button")
                self.openGallery1()
            }))

            alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
                print("User click Delete button")
            }))

            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Dismiss button")
            }))

            self.present(alert, animated: true, completion: {
                print("completion block")
            })
            
        }
        
        /*
         
         btnMarkAsDelivery
         */
        
        
        
        
    }
    
    /*@objc func openCamera1() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        // self.imageStrTwo = "0"
        // self.imageStrOne = "1"
        // self.imageStrThree = "0"
    }
    
    @objc func openGallery1() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        // self.imageStrTwo = "0"
        // self.imageStrOne = "1"
        // self.imageStrThree = "0"
    }
    
    //MARK:- MapKit delegates -
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        return renderer
    }
    
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
         
        imgUploadRecipt.isHidden = false
        let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        imgUploadRecipt.image = image_data // show image on profile
        let imageData:Data = image_data!.pngData()!
        imageStr1 = imageData.base64EncodedString()
        self.dismiss(animated: true, completion: nil)
        imgData1 = image_data!.jpegData(compressionQuality: 0.2)!
            
            
        self.uploadReceipt()
    }*/
    
    
    
    @objc func markAsDeliverdPopup() {
        
        let alert = UIAlertController(title: String("Message"), message: String("Order Delivered ?"), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
        
            self.markAsDeliveredWB()
        
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { action in
        
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func markAsDelivered() {
        self.markAsDeliveredWB()
        
        
    }
    
    // MARK:- MARK AS DELIVERED -
    @objc func markAsDeliveredWB() {
        
        
        if self.btnMarkAsDelivery.titleLabel?.text == "Upload Receipt" {
            
        } else if self.btnMarkAsDelivery.titleLabel?.text == "Mark as Delivered" {
            
            /*if (self.dictOne["DeliveryImage"] as! String) == "" {
                
                
                
            } else {*/
                
            self.uploadImageBeforeDelivered()
                
           //  }
            
            
            
            /*
            // upload slip after delivery
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
                
            let urlString = BASE_URL_EXPRESS_PLUS
            
            var parameters:Dictionary<AnyHashable, Any>!
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                   
                let x2 : Int = person["userId"] as! Int
                let driverId = String(x2)
                    
                let x4 : Int = dictOne["foodorderId"] as! Int
                let foodOrderId = String(x4)
                    
                parameters = [
                    "action"        : "updatefoodorder",
                    "foodorderId"   : String(foodOrderId),
                    "driverId"      : String(driverId),
                    "status"        : String("4")
                ]
            }
            print("parameters-------\(String(describing: parameters))")
                           
            Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                            {
                                response in
                       
                                switch(response.result) {
                                case .success(_):
                                    if let data = response.result.value {

                                        let JSON = data as! NSDictionary
                                        print(JSON as Any)
                                       
                                        var strSuccess : String!
                                        strSuccess = JSON["status"]as Any as? String
                                       
                                        var strSuccessAlert : String!
                                        strSuccessAlert = JSON["msg"]as Any as? String
                                       
                                        if strSuccess == String("success") {
                                            print("yes")
                                            ERProgressHud.sharedInstance.hide()
                                        
                                            let alert = UIAlertController(title: String(strSuccess), message: String(strSuccessAlert), preferredStyle: UIAlertController.Style.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                            
                                                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDJobRequestId") as? PDJobRequest
                                                self.navigationController?.pushViewController(settingsVCId!, animated: true)
                                            
                                            }))
                                            self.present(alert, animated: true, completion: nil)
                                        
                                        }
                                        else if strSuccess == String("Fails") {
                                            var strSuccess2 : String!
                                            strSuccess2 = JSON["msg"]as Any as? String
                                        
                                            ERProgressHud.sharedInstance.hide()
                                            
                                            let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                            
                                            }))
                                            self.present(alert, animated: true, completion: nil)
                                        
                                        }
                                    }

                                case .failure(_):
                                    print("Error message:\(String(describing: response.result.error))")
                                       
                                    ERProgressHud.sharedInstance.hide()
                                       
                                    let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                       
                                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                        UIAlertAction in
                                        NSLog("OK Pressed")
                                    }
                                       
                                    alertController.addAction(okAction)
                                       
                                    self.present(alertController, animated: true, completion: nil)
                                       
                                    break
                                }
            }
            */
            
        } else {
            
        }
        
    }
       
    
    
    
    
    @objc func uploadImageBeforeDelivered() {
        
        let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)
    
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera1()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery1()
        }))

        alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    @objc func openCamera1() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    @objc func openGallery1() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
         
        imgDelivered.isHidden = false
        let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        imgDelivered.image = image_data // show image on profile
        let imageData:Data = image_data!.pngData()!
        imageStrDel = imageData.base64EncodedString()
        self.dismiss(animated: true, completion: nil)
        imgDataDel = image_data!.jpegData(compressionQuality: 0.2)!
            
            
        self.markAsDeliveredDoneAfterUploadImage()
    }
    
    @objc func markAsDeliveredDoneAfterUploadImage() {
       
        self.view.endEditing(true)
            
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Uploading...")
            
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            let x2 : Int = person["userId"] as! Int
            let driverId = String(x2)
            
            let x4 : Int = dictOne["foodorderId"] as! Int
            let foodOrderId = String(x4)
              
            let parameters = [
                "action"        : "updatefoodorder",
                "foodorderId"   : String(foodOrderId),
                "driverId"      : String(driverId),
                "status"        : String("4")
            ]
                    
                print(parameters as Any)
                    
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.imgDataDel, withName: "DeliveryImage",fileName: "uploadSlipViaDriverAfterDelivered.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                            
                            // let paramsData:Data = NSKeyedArchiver.archivedData(withRootObject: value)
                            
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            },
            to:BASE_URL_EXPRESS_PLUS)
            { (result) in
                switch result {
                case .success(let upload, _, _):

                    upload.uploadProgress(closure: { (progress) in
                                //print("Upload Progress: \(progress.fractionCompleted)")
                                
                                /*
                                let alertController = UIAlertController(title: "Uploading image", message: "Please wait......", preferredStyle: .alert)

                                let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)

                                progressDownload.setProgress(Float((progress.fractionCompleted)/1.0), animated: true)
                                progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)

                                alertController.view.addSubview(progressDownload)
                                self.present(alertController, animated: true, completion: nil)
                                */
                    })

                    upload.responseJSON { response in
                                //print(response.result.value as Any)
                        if let data = response.result.value {
                            let JSON = data as! NSDictionary
                            print(JSON)
                                    
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                           
                            var strSuccessAlert : String!
                            strSuccessAlert = JSON["msg"]as Any as? String
                            
                            ERProgressHud.sharedInstance.hide()
                        
                            let alert = UIAlertController(title: String(strSuccess), message: String(strSuccessAlert), preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
                                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDJobRequestId") as? PDJobRequest
                                self.navigationController?.pushViewController(settingsVCId!, animated: true)
                            
                            }))
                            self.present(alert, animated: true, completion: nil)
                                    
                        }
                        else {
                            ERProgressHud.sharedInstance.hide()
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    ERProgressHud.sharedInstance.hide()
                    self.dismiss(animated: true, completion: nil)
                }}
            
        
        }
    }
    
    @objc func markAsDeliveredWB2() {
        
        
        

        let defaults = UserDefaults.standard
        if let myString = defaults.string(forKey: "keyOne") {
            print("defaults savedString: \(myString)") // callOnlyOne
            
            if myString == "callOnlyOne" {
                
                defaults.set("callOnlyOne2", forKey: "keyOne")
                
                
                
                
                
                
                
                
                
                
                
                ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
                    
                let urlString = BASE_URL_EXPRESS_PLUS
                
                var parameters:Dictionary<AnyHashable, Any>!
                if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                       
                    let x2 : Int = person["userId"] as! Int
                    let driverId = String(x2)
                        
                    let x4 : Int = dictOne["foodorderId"] as! Int
                    let foodOrderId = String(x4)
                        
                    parameters = [
                        "action"        : "updatefoodorder",
                        "foodorderId"   : String(foodOrderId),
                        "driverId"      : String(driverId),
                        "status"        : String("2")
                    ]
                }
                print("parameters-------\(String(describing: parameters))")
                               
                Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                                {
                                    response in
                           
                                    switch(response.result) {
                                    case .success(_):
                                        if let data = response.result.value {

                                            let JSON = data as! NSDictionary
                                            print(JSON as Any)
                                           
                                            var strSuccess : String!
                                            strSuccess = JSON["status"]as Any as? String
                                           
                                            // var strSuccessAlert : String!
                                            // strSuccessAlert = JSON["msg"]as Any as? String
                                           
                                            if strSuccess == String("success") {
                                                print("yes")
                                                ERProgressHud.sharedInstance.hide()
                                            
                                                
                                                
                                                let alert = UIAlertController(title: String("Success"), message: String(""), preferredStyle: UIAlertController.Style.alert)
                                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                                      
                                                    let defaults = UserDefaults.standard
                                                    defaults.set("", forKey: "keyOne")
                                                    defaults.set(nil, forKey: "keyOne")
                                                    
                                                    self.navigationController?.popViewController(animated: true)
                                                            
                                                }))
                                                self.present(alert, animated: true, completion: nil)
                                                
                                                
                                                
                                                
                                                
                                                
                                                /*
                                                if self.dictOne["imageUpload"] as! String == "" {
                                                    
                                                    self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                                    self.btnMarkAsDelivery.setTitle("Upload Receipt", for: .normal)
                                                    self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                    //self.btnMarkAsDelivery.addTarget(self, action: #selector(self.uploadReceiptClickMethod), for: .touchUpInside)
                                                    
                                                } else {
                                                    
                                                    
                                                    if self.dictOne["status"] is String {
                                                                        
                                                        print("Yes, it's a String")
                                                        // self.changesBookingIdis = (dictGetSpecialOrderData["bookingId"] as! String);
                                                        // myString12 = (dictGetSpecialOrderData["status"] as! String)
                                                        
                                                        if self.dictOne["status"] as! String == "1" {
                                                            // 1
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                            self.btnMarkAsDelivery.backgroundColor = .systemYellow
                                                            self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                                                            
                                                            //    self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                                                            
                                                        } else  if self.dictOne["status"] as! String == "2" {
                                                            // 2 : on the way
                                                            self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                                            self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                                        } else if self.dictOne["status"] as! String == "4" {
                                                            // 4
                                                            self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                                            self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = false
                                                        }
                                                        
                                                    } else if self.dictOne["status"] is Int {
                                                        
                                                        print("It is Integer")
                                                        let x2 : Int = (self.dictOne["status"] as! Int)
                                                        let myString2 = String(x2)
                                                        // self.dictGetSpecialOrderData = String(myString2)
                                                        // myString12 = String(myString2)
                                                        
                                                        if myString2 == "1" {
                                                            // 1
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                            self.btnMarkAsDelivery.backgroundColor = .systemYellow
                                                            self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                                                            
                                                            // self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                                                            
                                                        } else if myString2 == "2" {
                                                            // 2 : on the way
                                                            self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                                            self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                                        } else if myString2 == "4" {
                                                            // 4
                                                            self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                                            self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = false
                                                        }
                                                        
                                                    } else {
                                                        
                                                        print("i am ")
                                                        let temp:NSNumber = self.dictOne["status"] as! NSNumber
                                                        let tempString = temp.stringValue
                                                        // self.changesBookingIdis = String(tempString)
                                                        // myString12 = String(tempString)
                                                        
                                                        
                                                        if tempString == "1" {
                                                            // 1
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                            self.btnMarkAsDelivery.backgroundColor = .systemYellow
                                                            self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                                                            
                                                            // self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                                                            
                                                        } else  if tempString == "2" {
                                                            // 2 : on the way
                                                            self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                                            self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                                        } else if tempString == "4" {
                                                            // 4
                                                            self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                                            self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                                                            self.btnMarkAsDelivery.isUserInteractionEnabled = false
                                                        }
                                                        
                                                    }
                                                    
                                                    
                                                    
                                                    // self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                                    // self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                                    
                                                    self.imgUploadRecipt.sd_setImage(with: URL(string: (self.dictOne["imageUpload"] as! String)), placeholderImage: UIImage(named: "logo"))
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                }*/
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                            }
                                            else if strSuccess == String("Fails") {
                                                var strSuccess2 : String!
                                                strSuccess2 = JSON["msg"]as Any as? String
                                            
                                                ERProgressHud.sharedInstance.hide()
                                                
                                                let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                                
                                                }))
                                                self.present(alert, animated: true, completion: nil)
                                            
                                            }
                                        }

                                    case .failure(_):
                                        print("Error message:\(String(describing: response.result.error))")
                                           
                                        ERProgressHud.sharedInstance.hide()
                                           
                                        let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                           
                                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                            UIAlertAction in
                                            NSLog("OK Pressed")
                                        }
                                           
                                        alertController.addAction(okAction)
                                           
                                        self.present(alertController, animated: true, completion: nil)
                                           
                                        break
                                    }
                }
            } else {
                
                print("exit")
                
            }
            
            
        }
        
        
        
        
        
        
    
    }
    @objc func uploadReceipt() {
        self.view.endEditing(true)
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Updating...")
        
        
        //if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         // let x : Int = (person["userId"] as! Int)
         // let myString = String(x)
          
            /*
             /*
              action: uploadimage
              foodorderId:
              imageUpload:
              */
             */
            
            let x4 : Int = dictOne["foodorderId"] as! Int
            let foodOrderId = String(x4)
            
            // var parameters:Dictionary<AnyHashable, Any>!
                 let parameters = [
                    "action"              : "uploadimage",
                    // "userId"              : String(myString),
                    "foodorderId"             : String(foodOrderId),
                    
                    
                 ]
             
            print(parameters as Any)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.imgData1, withName: "imageUpload",fileName: "expressPlusUploadReceipt.jpg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        
                        // let paramsData:Data = NSKeyedArchiver.archivedData(withRootObject: value)
                        
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                },
                to:BASE_URL_EXPRESS_PLUS)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):

                        upload.uploadProgress(closure: { (progress) in
                            //print("Upload Progress: \(progress.fractionCompleted)")
                            
                            /*
                            let alertController = UIAlertController(title: "Uploading image", message: "Please wait......", preferredStyle: .alert)

                            let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)

                            progressDownload.setProgress(Float((progress.fractionCompleted)/1.0), animated: true)
                            progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)

                            alertController.view.addSubview(progressDownload)
                            self.present(alertController, animated: true, completion: nil)
                            */
                        })

                        upload.responseJSON { response in
                            //print(response.result.value as Any)
                            if let data = response.result.value {
                                let JSON = data as! NSDictionary
                                print(JSON)
                                ERProgressHud.sharedInstance.hide()
                                 
                                
                                
                                self.dictData20 = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                if self.dictData20["imageUpload"] as! String == "" {
                                    
                                    self.btnMarkAsDelivery.backgroundColor = .systemOrange
                                    self.btnMarkAsDelivery.setTitle("Upload Receipt", for: .normal)
                                    self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                    self.btnMarkAsDelivery.addTarget(self, action: #selector(self.uploadReceiptClickMethod), for: .touchUpInside)
                                    
                                } else {
                                    
                                    
                                    if self.dictOne["status"] is String {
                                                        
                                        print("Yes, it's a String")
                                        
                                        if self.dictOne["status"] as! String == "1" {
                                            // 1
                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                            self.btnMarkAsDelivery.backgroundColor = .systemYellow
                                            self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                                            
                                            self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                                            
                                        } else  if self.dictOne["status"] as! String == "2" {
                                            // 2 : on the way
                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                            self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                            self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                            
                                            
                                            self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB), for: .touchUpInside)
                                            
                                            
                                        } else if self.dictOne["status"] as! String == "4" {
                                            // 4
                                            self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                            self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                                            self.btnMarkAsDelivery.isUserInteractionEnabled = false
                                        }
                                        
                                    } else if self.dictOne["status"] is Int {
                                        
                                        print("It is Integer")
                                        let x2 : Int = (self.dictOne["status"] as! Int)
                                        let myString2 = String(x2)
                                        
                                        
                                        if myString2 == "1" {
                                            // 1
                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                            self.btnMarkAsDelivery.backgroundColor = .systemYellow
                                            self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                                            
                                            self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                                            
                                        } else if myString2 == "2" {
                                            // 2 : on the way
                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                            self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                            self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                        } else if myString2 == "4" {
                                            // 4
                                            self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                            self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                                            self.btnMarkAsDelivery.isUserInteractionEnabled = false
                                        }
                                        
                                    } else {
                                        
                                        print("i am ")
                                        let temp:NSNumber = self.dictOne["status"] as! NSNumber
                                        let tempString = temp.stringValue
                                        
                                        
                                        if tempString == "1" {
                                            // 1
                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                            self.btnMarkAsDelivery.backgroundColor = .systemYellow
                                            self.btnMarkAsDelivery.setTitle("On the Way", for: .normal)
                                            
                                            self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB2), for: .touchUpInside)
                                            
                                        } else  if tempString == "2" {
                                            // 2 : on the way
                                            
                                            self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                            self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                            self.btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
                                            
                                            self.btnMarkAsDelivery.addTarget(self, action: #selector(self.markAsDeliveredWB), for: .touchUpInside)
                                            
                                        } else if tempString == "4" {
                                            // 4
                                            self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                            self.btnMarkAsDelivery.setTitle("Delivered", for: .normal)
                                            self.btnMarkAsDelivery.isUserInteractionEnabled = false
                                        }
                                        
                                    }
                                    
                                    
                                    
                                    // self.btnMarkAsDelivery.backgroundColor = .systemGreen
                                    // self.btnMarkAsDelivery.isUserInteractionEnabled = true
                                    
                                    self.imgUploadRecipt.sd_setImage(with: URL(string: (self.dictOne["imageUpload"] as! String)), placeholderImage: UIImage(named: "logo"))
                                    
                                }
                                
                                self.dictOne = self.dictData20 as NSDictionary
                                
                                self.dismiss(animated: true, completion: nil)
                                
                                
                                
                                // self.secondWB()
                                // self.navigationController?.popViewController(animated: true)
                            }
                            else {
                                ERProgressHud.sharedInstance.hide()
                                // self.secondWB()
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        ERProgressHud.sharedInstance.hide()
                        self.dismiss(animated: true, completion: nil)
                    }}// }
        
    }
}
