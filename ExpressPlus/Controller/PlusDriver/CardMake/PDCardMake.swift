//
//  PDCardMake.swift
//  ExpressPlus
//
//  Created by Apple on 12/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class PDCardMake: UIViewController {

    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE -
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "CARD DETAILS"
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            // btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.layer.cornerRadius = 26
            viewBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblcardNumber:UILabel!
    @IBOutlet weak var lblExpMonth:UILabel!
    @IBOutlet weak var lblCardHolderName:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.sideBarMenuClick()
        
        self.getCardDetails()
        
    }
    
    @objc func sideBarMenuClick() {
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    
    
    @objc func getCardDetails() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        print(person)
                   
            let x : Int = person["userId"] as! Int
            let myString = String(x)
        
        parameters = [
            "action"          : "carddetails",
            "userId"          : String(myString)
        ]
        
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String // match your success status here
                                              
                    var strMessage : String!
                    strMessage = JSON["msg"]as Any as? String
                    
                    if strSuccess == String("success") {
                        ERProgressHud.sharedInstance.hide()
                        
                        var dict: Dictionary<AnyHashable, Any>
                        dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                        
                        
                        /*
                         @IBOutlet weak var lblcardNumber:UILabel!
                         @IBOutlet weak var lblExpMonth:UILabel!
                         
                         */
                        
                        self.lblcardNumber.text = "xxxx xxxx xxxx "+(dict["last4"] as! String)
                        self.lblExpMonth.text = "Exp. "+(dict["exp_month"] as! String)+"/"+(dict["exp_year"] as! String)
                        // lblCardHolderName
                        self.lblCardHolderName.text = (person["fullName"] as! String)
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                        
                        let alert = UIAlertController(title: String(strSuccess), message: String(strMessage), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                             
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
        }
    }
}
