//
//  ROrder.swift
//  ExpressPlus
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class PDRestaurantDetailsOrders: UIViewController {

    let cellReuseIdentifier = "pDRestaurantDetailsOrdersTableCell"
    
    var dictGetFoodDetails:NSDictionary!
    
    var dict: Dictionary<AnyHashable, Any> = [:]
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
    
    var page : Int! = 1
    var loadMore : Int! = 1;
    
    var strDeliveryDetailsFromUserProfile:String!
    
    
    
    var deliveryDetails:String!
    
    var restaurantNameIs:String!
    
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "ORDER LIST"
        }
    }
    
    @IBOutlet weak var lblCardNumber:UILabel! {
        didSet {
            lblCardNumber.text = ""
        }
    }
    @IBOutlet weak var lblTipAmount:UILabel! {
        didSet {
            lblTipAmount.text = ""
        }
    }
    @IBOutlet weak var lblTotalAmount:UILabel! {
        didSet {
            lblTotalAmount.text = ""
        }
    }
    
    @IBOutlet weak var btnPaymentAccess:UIButton! {
        didSet {
            btnPaymentAccess.layer.cornerRadius = 6
            btnPaymentAccess.clipsToBounds = true
            btnPaymentAccess.backgroundColor = .systemGreen
            btnPaymentAccess.isHidden = true
        }
    }
    
    @IBOutlet weak var btnInProgress:UIButton! {
        didSet {
            btnInProgress.backgroundColor = UIColor.init(red: 217.0/255.0, green: 91.0/255.0, blue: 49.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var btnDeliverd:UIButton! {
        didSet {
            btnDeliverd.backgroundColor = UIColor.init(red: 217.0/255.0, green: 91.0/255.0, blue: 49.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
        }
    }
    
    @IBOutlet weak var viewBottomOne:UIView! {
        didSet {
            viewBottomOne.backgroundColor = .white
            viewBottomOne.layer.cornerRadius = 8
            viewBottomOne.clipsToBounds = true
        }
    }
    @IBOutlet weak var viewBottomTwo:UIView! {
        didSet {
            viewBottomTwo.backgroundColor = .white
            viewBottomTwo.layer.cornerRadius = 8
            viewBottomTwo.clipsToBounds = true
        }
    }
    @IBOutlet weak var viewBottomThree:UIView! {
        didSet {
            viewBottomThree.backgroundColor = .white
            viewBottomThree.layer.cornerRadius = 8
            viewBottomThree.clipsToBounds = true
        }
    }
    @IBOutlet weak var lblSalesTax:UILabel! {
        didSet {
            self.lblSalesTax.text = ""
        }
    }
    
    @IBOutlet weak var lblDeliveryCharge:UILabel!
    
    @IBOutlet weak var lblTransactionFee:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        self.btnBack.addTarget(self, action: #selector(backClickMehod), for: .touchUpInside)
        self.btnDeliverd.addTarget(self, action: #selector(pushToDeliveredScreen), for: .touchUpInside)
        
        // print(dictGetFoodDetails as Any)
        self.lblNavigationTitle.text = "Order # "
        self.foodDetailsWB()
        
    }
    
    @objc func pushToDeliveredScreen() {
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        
        if person["role"] as! String == "Member" {
            
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPResDeliveryDetailsId") as? UPResDeliveryDetails
            settingsVCId!.dictOrderDetails = dict as NSDictionary
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
            
        } else {
            
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPResDeliveryDetailsId") as? UPResDeliveryDetails
            settingsVCId!.dictOrderDetails = dict as NSDictionary
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
            
            /*if strDeliveryDetailsFromUserProfile == "yes" {
                     
                if deliveryDetails == "nice" {
                    let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDServiceScreenId") as? PDServiceScreen
                    settingsVCId!.dictGetDeliveryDetail =  dict as NSDictionary
                         //
                    settingsVCId!.dict2Try = dictGetFoodDetails
                    settingsVCId!.driverNiceTwo = deliveryDetails
                    self.navigationController?.pushViewController(settingsVCId!, animated: true)
                } else {
                    let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPResDeliveryDetailsId") as? UPResDeliveryDetails
                    settingsVCId!.dictOrderDetails = dict as NSDictionary
                    self.navigationController?.pushViewController(settingsVCId!, animated: true)
                }
            } else {
                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDServiceScreenId") as? PDServiceScreen
                settingsVCId!.dictGetDeliveryDetail = dict as NSDictionary
                self.navigationController?.pushViewController(settingsVCId!, animated: true)
            }*/
            
            }
     
        }
    }
    
    @objc func backClickMehod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- FOOD DETAILS -
    @objc func foodDetailsWB() {
         self.arrListOfAllMyOrders.removeAllObjects()
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
                  
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        /*
         [action] => foodorderdetails
         [userId] => 99
         [foodorderId] => 70
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
        
        let strMyId:String!
        let x : Int = person["userId"] as! Int
        strMyId = String(x)
        
            let strMyIdy:String!
            let xy : Int = dictGetFoodDetails["foodorderId"] as! Int
            strMyIdy = String(xy)
            
                      parameters = [
                          "action"      : "foodorderdetails",
                          "foodorderId" : String(strMyIdy),
                          "userId"      : String(strMyId)
                          
                ]
        }
        print("parameters-------\(String(describing: parameters))")
                      
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
                  
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                      print(JSON as Any)
                                  /*
                                     EstTime = "15 Minutes";
                                            Mile = "4 Mile";
                     PaymentStatus = 2;
                                            TIP = "0.75";
                                            address = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
                                            assignDriver = 113;
                                            cardNo = 4242424242424242;
                                            city = Ghaziabad;
                                            couponCode = "";
                                            created = "2020-05-19 17:40:00";
                                            deliveryFee = "";
                                            deliveryLat = "28.6634703";
                                            deliveryLong = "77.3240304";
                                            discount = "";
                                            driverAVG = 0;
                                            driverContactNumber = 2356898523;
                                            driverId = 113;
                                            driverImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1589802893IMG-20200518-WA0003.jpg";
                                            driverName = "driver evs";
                                            foodDetails =         (
                                                            {
                                                    id = 43;
                                                    name = gagsgs;
                                                    price = "15.00";
                                                    quantity = 1;
                                                    resturentId = 99;
                                                }
                                            );
                                            foodDetailsNew =         (
                                                            {
                                                    categoryId = "";
                                                    description = "";
                                                    foodName = "";
                                                    foodTag = "";
                                                    foodType = "";
                                                    "image_1" = "";
                                                    "image_2" = "";
                                                    "image_3" = "";
                                                    "image_4" = "";
                                                    "image_5" = "";
                                                    menuId = "";
                                                    price = "";
                                                    quantity = 1;
                                                    resturentId = "";
                                                    specialPrice = "";
                                                }
                                            );
                                            foodId = "";
                                            foodorderId = 70;
                                            landmark = hotel;
                                            name = purnima;
                                            noContact = "";
                                            phone = 5623852369;
                                            resturentAddress = "Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India";
                                            resturentId = 99;
                                            resturentLatitude = "28.5870717";
                                            resturentLongitude = "77.0605306";
                                            resturentName = "restaurent 2d";
                                            specialNote = "test y";
                                            state = "Uttar Pradesh";
                                            status = 4;
                                            storecity = "";
                                            totalAmount = "15.75";
                                            userAVG = 0;
                                            userId = 114;
                                            userName = kiwi;
                                            whatYouWant = "";
                                            workPlace = Home;
                                            zipcode = 201011;
                     transactionFee
                                        };
                                     */
                                    
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                    if strSuccess == String("success") {
                                   print("yes")
                                    
                        ERProgressHud.sharedInstance.hide()
                                    
                                    // self.txt
                                    /*
                                    var ar : NSArray!
                                    ar = (JSON["data"] as! Array<Any>) as NSArray
                                    self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                    */
                                    
                                    
                        self.dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                    
                                    // (self.dict["cardNo"] as! String)
                                    
                                    
                        // let x : Int = self.dict["deliveryFee"] as! Int
                        // let myString = String(x)
                        self.lblDeliveryCharge.text = String("$ 6")
                        
                        let last4 = (self.dict["cardNo"] as! String).suffix(4)
                                    
                        self.lblCardNumber.text     = "XXXX XXXX XXXX "+String(last4)
                        self.lblTipAmount.text      = "$ "+(self.dict["TIP"] as! String)
                        // self.lblTotalAmount.text    = "$ "+(self.dict["totalAmount"] as! String)
                                   
                        var ar : NSArray!
                        ar = (self.dict["foodDetailsNew"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                    
                                    // foodorderId
                        let x121 : Int = self.dict["foodorderId"] as! Int
                        let myString121 = String(x121)
                        self.lblNavigationTitle.text    = "Order # "+myString121
                                    
                        self.lblSalesTax.text = "$ "+(self.dict["salesTax"] as! String)
                                    
                        self.restaurantNameIs = (self.dict["resturentName"] as! String)
                             
                        
                        
                        
                        if self.dict["transactionFee"] is String {
                                          
                            print("Yes, it's a String")
                          
                            self.lblTransactionFee.text = "$ "+(self.dict["transactionFee"] as! String)

                            let totalAmountIs = Double((self.dict["totalAmount"] as! String))!+Double((self.dict["transactionFee"] as! String))!
                            
                            let d = String(format: "%.2f", totalAmountIs)
                            self.lblTotalAmount.text = "$ "+String(d)
                            // self.lblTotalAmount.text = "$ "+(self.dict["totalAmount"] as! String)
                            
                        } else if self.dict["transactionFee"] is Int {
                          
                            print("It is Integer")
                          
                            let x2 : Int = (self.dict["transactionFee"] as! Int)
                            let myString2 = String(x2)
                            self.lblTransactionFee.text = "$ "+myString2
                          
                            let totalAmountIs = Double((self.dict["totalAmount"] as! String))!+Double(myString2)!
                            
                            let d = String(format: "%.2f", totalAmountIs)
                            self.lblTotalAmount.text = "$ "+String(d)
                            // self.lblTotalAmount.text = "$ "+String(d)
                            
                        } else {
                        //some other check
                          print("i am ")
                          
                            let temp:NSNumber = self.dict["transactionFee"] as! NSNumber
                            let tempString = temp.stringValue
                            self.lblTransactionFee.text = "$ "+tempString
                          
                            let totalAmountIs = Double((self.dict["totalAmount"] as! String))!+Double(tempString)!
                            
                            let d = String(format: "%.2f", totalAmountIs)
                            self.lblTotalAmount.text = "$ "+String(d)
                            // self.lblTotalAmount.text = "$ "+String(d)
                        }
                        
                        
                        //
                        
                        
                        
                        
                        
                        
                        
                        //
                        
                        // 2 : done payment
                        // 1 : user not complete payment
                        // 0 : driver not accept
                      
                        
                        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                            
                            if person["role"] as! String == "Member" {
                        
                                
                                
                                
                                
                                
                                
                        
                                
                                if self.dict["PaymentStatus"] is String {
                                            
                                    print("Yes, it's a String")
                            
                            // self.btnPaymentAccess.setTitle((self.dict["PaymentStatus"] as! String), for: .normal)
                            
                                    if (self.dict["PaymentStatus"] as! String) == "0" {
                                
                                        self.btnPaymentAccess.isHidden = false
                                        self.btnPaymentAccess.setTitle("Driver not Accept", for: .normal)
                                
                                    } else if (self.dict["PaymentStatus"] as! String) == "1" {
                                
                                        self.btnPaymentAccess.isHidden = false
                                        self.btnPaymentAccess.setTitle("Confirm & Pay", for: .normal)
                                        self.btnPaymentAccess.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                                
                                    } else if (self.dict["PaymentStatus"] as! String) == "2" {
                                
                                        self.btnPaymentAccess.isHidden = true
                                
                                    }
                            
                            
                            
                            
                                } else if self.dict["PaymentStatus"] is Int {
                            
                                    print("It is Integer")
                                    let x2 : Int = (self.dict["PaymentStatus"] as! Int)
                                    let myString2 = String(x2)
                            // self.btnPaymentAccess.setTitle((myString2), for: .normal)
                            
                            
                            
                                    if myString2 == "0" {
                                
                                        self.btnPaymentAccess.isHidden = false
                                        self.btnPaymentAccess.setTitle("Driver not Accept", for: .normal)
                                
                                    } else if myString2 == "1" {
                                
                                        self.btnPaymentAccess.isHidden = false
                                        self.btnPaymentAccess.setTitle("Confirm & Pay", for: .normal)
                                        self.btnPaymentAccess.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                                
                                    } else if myString2 == "2" {
                                
                                        self.btnPaymentAccess.isHidden = true
                                
                                    }
                            
                            
                            
                            
                            
                        } else {
                            
                            print("i am ")
                            let temp:NSNumber = self.dict["PaymentStatus"] as! NSNumber
                            let tempString = temp.stringValue
                            // self.btnPaymentAccess.setTitle((tempString), for: .normal)
                            
                            
                            if tempString == "0" {
                                
                                self.btnPaymentAccess.isHidden = false
                                self.btnPaymentAccess.setTitle("Driver not Accept", for: .normal)
                                
                            } else if tempString == "1" {
                                
                                self.btnPaymentAccess.isHidden = false
                                self.btnPaymentAccess.setTitle("Confirm & Pay", for: .normal)
                                self.btnPaymentAccess.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                                
                            } else if tempString == "2" {
                                
                                self.btnPaymentAccess.isHidden = true
                                
                            }
                            
                            
                        }
                        
                        
                    }
                            else {
                                // driver
                            }
                        }
                        
                        
                        
                        
                        
                        
                        self.tbleView.delegate = self
                        self.tbleView.dataSource = self
                        self.tbleView.reloadData()
                        
                    }
                    else {
                        
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                                   
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                                  
                ERProgressHud.sharedInstance.hide()
                                  
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                                  
                alertController.addAction(okAction)
                                  
                self.present(alertController, animated: true, completion: nil)
                                  
                break
            }
        }
    }
    
    @objc func confirmAndPay() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPPayemtnScreenForFoodSectionId") as? UPPayemtnScreenForFoodSection
        
        print(self.dict as Any)
        // foodOrderGroupId
        
        
        // strGetAdminAmount
        if self.dict["AdminAmount"] is String {
                            
            print("Yes, it's a String")
            // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
            // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
            push!.strGetAdminAmount = (self.dict["AdminAmount"] as! String)
            
        } else if self.dict["AdminAmount"] is Int {
            
            print("It is Integer")
            let x2 : Int = (self.dict["AdminAmount"] as! Int)
            let myString2 = String(x2)
            // self.changesBookingIdis = String(myString2)
            // push!.getTotalPriceOfFood = String(myString2)
            push!.strGetAdminAmount = String(myString2)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = self.dict["AdminAmount"] as! NSNumber
            let tempString = temp.stringValue
            // self.changesBookingIdis = String(tempString)
            // push!.getTotalPriceOfFood = String(tempString)
            push!.strGetAdminAmount = String(tempString)
            
        }
        
        
        
        
        
        
        
        
        if self.dict["foodOrderGroupId"] is String {
                            
            print("Yes, it's a String")
            // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
            // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
            push!.foodOrderId = (self.dict["foodOrderGroupId"] as! String)
            
        } else if self.dict["foodOrderGroupId"] is Int {
            
            print("It is Integer")
            let x2 : Int = (self.dict["foodOrderGroupId"] as! Int)
            let myString2 = String(x2)
            // self.changesBookingIdis = String(myString2)
            // push!.getTotalPriceOfFood = String(myString2)
            push!.foodOrderId = String(myString2)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = self.dict["foodOrderGroupId"] as! NSNumber
            let tempString = temp.stringValue
            // self.changesBookingIdis = String(tempString)
            // push!.getTotalPriceOfFood = String(tempString)
            push!.foodOrderId = String(tempString)
            
        }
        
        
        
        
        
        
        if self.dict["totalAmount"] is String {
                            
            print("Yes, it's a String")
            // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
            
            
            let addTransactionFeeeesWithTotal = Double((self.dict["transactionFee"] as! String))!+Double((self.dict["totalAmount"] as! String))!
            
            push!.getTotalPriceOfFood = String(addTransactionFeeeesWithTotal)
            
        } else if self.dict["bookingId"] is Int {
            
            print("It is Integer")
            let x2 : Int = (self.dict["totalAmount"] as! Int)
            let myString2 = String(x2)
            // self.changesBookingIdis = String(myString2)
            // push!.getTotalPriceOfFood = String(myString2)
            let addTransactionFeeeesWithTotal = Double((self.dict["transactionFee"] as! String))!+Double(myString2)!
            push!.getTotalPriceOfFood = String(addTransactionFeeeesWithTotal)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = self.dict["totalAmount"] as! NSNumber
            let tempString = temp.stringValue
            // self.changesBookingIdis = String(tempString)
            // push!.getTotalPriceOfFood = String(tempString)
            let addTransactionFeeeesWithTotal = Double((self.dict["transactionFee"] as! String))!+Double(tempString)!
            push!.getTotalPriceOfFood = String(addTransactionFeeeesWithTotal)
            
        }
        
        push!.enableBackOrNot = "yes"
        push!.strAccountNumberIs = (self.dict["driverStripeAccount"] as! String)
        
        self.navigationController?.pushViewController(push!, animated: true)
    }
}


//MARK:- TABLE VIEW -
extension PDRestaurantDetailsOrders: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PDRestaurantDetailsOrdersTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! PDRestaurantDetailsOrdersTableCell
        
        cell.backgroundColor = .white
        
        
        
        
        
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        print(item as Any)
        cell.lblName.text = (item!["foodName"] as! String)
        
        if item!["specialPrice"] is String {
            print("Yes, it's a String")
            cell.lblPrice.text = "Restaurant Name : "+self.restaurantNameIs+"\n"+"Price : $ "+(item!["specialPrice"] as! String)
        } else if item!["specialPrice"] is Int {
            
            print("It is Integer")
            let x : Int = item!["specialPrice"] as! Int
            let myString = String(x)
            cell.lblPrice.text = "Restaurant Name : "+self.restaurantNameIs+"\n"+"Price : $ "+String(myString)
            
        } else if item!["specialPrice"] is NSNumber {
            print("It is nsnimber")
          //some other check
            
            
            // let cv: String = String(format: "%.2f", item!["specialPrice"] as! NSNumber)
             // print(cv as Any)
            
            
            let temp:NSNumber = item!["specialPrice"] as! NSNumber
            let tempString = temp.stringValue
            // print( tempString)
            
            if tempString.contains(".") {
                
                let a = Double(tempString)
                // print(a as Any)
                
                let cv: String = String(format: "%.2f", a!)
              
                cell.lblPrice.text = "Restaurant Name : "+self.restaurantNameIs+"\n"+"Price : $ "+String(cv)
                
            } else {
                
                cell.lblPrice.text = "Restaurant Name : "+self.restaurantNameIs+"\n"+"Price : $ "+String(tempString)
                
            }
            
            // self.changesBookingIdis = String(tempString)
            
            
        }
        
        
        
        
        
        
        
        cell.lblQuantity.text = (item!["quantity"] as! String)
        
        cell.imgFoodProfile.sd_setImage(with: URL(string: (item!["image_1"] as! String)), placeholderImage: UIImage(named: "food1"))
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}

extension PDRestaurantDetailsOrders: UITableViewDelegate {
    
}

