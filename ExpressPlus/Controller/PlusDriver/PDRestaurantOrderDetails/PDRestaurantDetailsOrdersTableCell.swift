//
//  PDRestaurantDetailsOrdersTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 02/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class PDRestaurantDetailsOrdersTableCell: UITableViewCell {

    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    
    @IBOutlet weak var imgFoodProfile:UIImageView! {
        didSet {
            imgFoodProfile.backgroundColor = .clear
            // imgFoodProfile.layer.cornerRadius = 40
            // imgFoodProfile.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblQuantity:UILabel! {
        didSet {
            lblQuantity.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            lblQuantity.layer.cornerRadius = 4
            lblQuantity.clipsToBounds = true
            lblQuantity.textColor = .white
        }
    }
    
    @IBOutlet weak var lblRestaurantName:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
