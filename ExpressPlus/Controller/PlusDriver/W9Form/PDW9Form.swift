//
//  PDW9Form.swift
//  ExpressPlus
//
//  Created by Apple on 24/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import WebKit

class PDW9Form: UIViewController, UIWebViewDelegate {

    // MARK:- CUSTOM NAVIGATION BAR -
       @IBOutlet weak var navigationBar:UIView! {
           didSet {
               navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
           }
       }

       // MARK:- CUSTOM NAVIGATION TITLE
       @IBOutlet weak var lblNavigationTitle:UILabel! {
           didSet {
               lblNavigationTitle.text = "W-9 FORM"
           }
       }
       @IBOutlet weak var btnBack:UIButton! {
           didSet {
               // btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
           }
       }
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(PDPayFiveDollar.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    
        self.sideBarMenuClick()
        
        // let url = URL(string: K9_FORM_URL)
        // webView.load(URLRequest(url: url!))
        
        if let url = URL(string: K9_FORM_URL) {
            UIApplication.shared.open(url)
        }
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func sideBarMenuClick() {
        
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
           }
        
    }
}
