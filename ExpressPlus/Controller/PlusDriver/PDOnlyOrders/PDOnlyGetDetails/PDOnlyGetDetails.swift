//
//  PDOnlyGetDetails.swift
//  ExpressPlus
//
//  Created by Apple on 11/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire
class PDOnlyGetDetails: UIViewController {

    var dictGetDriveGetDetails:NSDictionary!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var lblProductName:UILabel!
    @IBOutlet weak var lblEstimateprice:UILabel!
    @IBOutlet weak var lblTipAmount:UILabel!
    @IBOutlet weak var lblDeliveryFee:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblStoreName:UILabel!
    
    @IBOutlet weak var btnAccept:UIButton! {
        didSet {
            btnAccept.layer.cornerRadius = 8
            btnAccept.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnDecline:UIButton! {
        didSet {
            btnDecline.layer.cornerRadius = 8
            btnDecline.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnBack.addTarget(self, action: #selector(backClickMthod), for: .touchUpInside)
          print(dictGetDriveGetDetails as Any)
        
        /*
         EPrice = 100;
         StoreCity = Noida;
         TIP = 5;
         address = "Ram Prastha Ghaziabad";
         created = "2020-09-14 15:21:00";
         deliveryFee = 6;
         driverId = 279;
         togetDriverRequestId = 834;
         togetRequestId = 113;
         totalAmount = 111;
         userId = 295;
         userImage = "";
         userName = purnima;
         wahtUwant = Test;
         */
        
        self.lblProductName.text        = (dictGetDriveGetDetails["wahtUwant"] as! String)
        // self.lblEstimateprice.text      = "$ "+(dictGetDriveGetDetails["EPrice"] as! String)
        // self.lblTipAmount.text          = "$ "+(dictGetDriveGetDetails["TIP"] as! String)
        // self.lblDeliveryFee.text        = "$ "+(dictGetDriveGetDetails["deliveryFee"] as! String)
        self.lblAddress.text            = (dictGetDriveGetDetails["address"] as! String)
        self.lblStoreName.text = (dictGetDriveGetDetails["StoreCity"] as! String)
        
        self.btnAccept.addTarget(self, action: #selector(acceptServiceClickMethod), for: .touchUpInside)
        self.btnDecline.addTarget(self, action: #selector(declineServiceClickMethod), for: .touchUpInside)
    }
    @objc func backClickMthod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func acceptServiceClickMethod() {
        self.acceptOrDecline(strStatus: "2")
    }
    @objc func declineServiceClickMethod() {
        self.acceptOrDecline(strStatus: "3")
    }
    
    // MARK:- WEBSERVICE ( ACCEPT OR DECLINE ) -
    @objc func acceptOrDecline(strStatus:String) {
        
        if strStatus == "2" {
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "accepting...")
        } else {
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "rejecting...")
        }
        
    /*
     [action] => foodrequestaccept
     [userId] => 123
     [foodrequestId] => 295
     [foodorderId] => 56
     [status] => 3 // reject
     */
    
    /*
    [action] => togetrequestaccept
    [driverId] => 123
    [togetDriverRequestId] => 340
    [togetRequestId] => 59
    [status] => 2 // accept
    */
        
        let urlString = BASE_URL_EXPRESS_PLUS
    
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            // let x : Int = dictGetDriveGetDetails["bookingId"] as! Int
            // let myStringBookingId = String(x)
            
            let x2 : Int = person["userId"] as! Int
            let driverId = String(x2)
            
            let x3 : Int = dictGetDriveGetDetails["togetRequestId"] as! Int
            let foodRequestId = String(x3)
            
            // togetDriverRequestId
            let x4 : Int = dictGetDriveGetDetails["togetDriverRequestId"] as! Int
            let driverRequestId = String(x4)
            
            if strStatus == "3" {
                parameters = [
                    "action"        : "foodrequestaccept", // done
                    "userId"        : String(driverId), // done
                    "foodrequestId" : String(driverRequestId),
                    "foodorderId"   : String(foodRequestId),
                    "status"        : String(strStatus) // done
                ]
            } else {
                parameters = [
                    "action"                : "togetrequestaccept",
                    "driverId"              : String(driverId),
                    "togetDriverRequestId"  : String(driverRequestId),
                    "togetRequestId"        : String(foodRequestId),
                    "status"                : String(strStatus)
                ]
            }
            
                   
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                ERProgressHud.sharedInstance.hide()
                                let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                    // self.dismiss(animated: true, completion: nil)
                                    self.backClickMthod()
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                               }
                               else if strSuccess == String("Fails") {
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                ERProgressHud.sharedInstance.hide()
                                let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                    // self.dismiss(animated: true, completion: nil)
                                    self.backClickMthod()
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
}
