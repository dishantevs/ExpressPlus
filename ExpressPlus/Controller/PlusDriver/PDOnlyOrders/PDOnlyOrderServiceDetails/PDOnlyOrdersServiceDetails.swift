//
//  PDOnlyOrdersServiceDetails.swift
//  ExpressPlus
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire
class PDOnlyOrdersServiceDetails: UIViewController {

    var dictGetDriveServiceDetails:NSDictionary!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblService:UILabel!
    @IBOutlet weak var lblCreated:UILabel!
    
    @IBOutlet weak var lblBookingDate:UILabel!
    @IBOutlet weak var lblWorkLocation:UILabel!
    
    @IBOutlet weak var lblServiceFee:UILabel!
    @IBOutlet weak var lblTipAmount:UILabel!
    
    @IBOutlet weak var lblTotalAmount:UILabel!
    
    @IBOutlet weak var btnAccept:UIButton! {
        didSet {
            btnAccept.layer.cornerRadius = 8
            btnAccept.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnDecline:UIButton! {
        didSet {
            btnDecline.layer.cornerRadius = 8
            btnDecline.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnBack.addTarget(self, action: #selector(backClickMthod), for: .touchUpInside)
         print(dictGetDriveServiceDetails as Any)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
        }
        
        /*
         AVGRating = "";
         TIP = 0;
         address = "Ram Prastha Ghaziabad";
         bookingDate = "16th Sep";
         bookingId = 314;
         contactNumber = 2525365623;
         created = "2020-09-12 20:58:00";
         email = "cust1@gmail.com";
         fullName = cust;
         groupId = 128;
         image = "";
         serviceAmount = 35;
         serviceName = "Yard Work";
         servicePrice = 35;
         slote = "7:57 PM-8:57 PM";
         status = 0;
         totalAmount = 35;
         userAddress = "";
         userId = 291;
         zipCode = "";
         */
        
        
        
        self.lblService.text        = (dictGetDriveServiceDetails["serviceName"] as! String)
        self.lblCreated.text        = (dictGetDriveServiceDetails["slote"] as! String)
        self.lblWorkLocation.text   = (dictGetDriveServiceDetails["address"] as! String)
        self.lblBookingDate.text    = (dictGetDriveServiceDetails["created"] as! String)
        
        // self.lblServiceFee.text     = "$ "+(dictGetDriveServiceDetails["serviceAmount"] as! String)
        // self.lblTipAmount.text      = "$ "+(dictGetDriveServiceDetails["TIP"] as! String)
        // self.lblTotalAmount.text    = "$ "+(dictGetDriveServiceDetails["totalAmount"] as! String)
        
        self.btnAccept.addTarget(self, action: #selector(acceptServiceClickMethod), for: .touchUpInside)
        self.btnDecline.addTarget(self, action: #selector(declineServiceClickMethod), for: .touchUpInside)
    }
    
    @objc func backClickMthod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func acceptServiceClickMethod() {
        
        let alert = UIAlertController(title: String("Message!"), message: String("Are you sure you want to accept ?"), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Accept", style: UIAlertAction.Style.default, handler: { action in
             
            self.acceptOrDecline(strStatus: "1")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: { action in
             
 
        }))
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    @objc func declineServiceClickMethod() {
        
        let alert = UIAlertController(title: String("Message!"), message: String("Are you sure you want to Skip ?"), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Skip", style: UIAlertAction.Style.destructive, handler: { action in
             
            self.acceptOrDecline(strStatus: "3")
            
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: { action in
             
            
            
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    // MARK:- WEBSERVICE ( ACCEPT OR DECLINE ) -
    @objc func acceptOrDecline(strStatus:String) {
        
        if strStatus == "1" {
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "accepting...")
        } else {
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "rejecting...")
        }
        
    /*
     [action] => changebookingstatus
     [driverId] => 123
     [bookingId] => 254
     [status] => 3 // reject
     */
    
    /*
    [action] => changebookingstatus
    [driverId] => 123
    [bookingId] => 254
    [status] => 1 // accept
    */
        let urlString = BASE_URL_EXPRESS_PLUS
    
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            
            let x : Int = dictGetDriveServiceDetails["bookingId"] as! Int
            let myStringBookingId = String(x)
            
            let x2 : Int = person["userId"] as! Int
            let driverId = String(x2)
            
            if strStatus == "1" {
                parameters = [
                    "action"        : "changebookingstatus",
                    "driverId"      : String(driverId),
                    "bookingId"     : String(myStringBookingId),
                    "status"        : String(strStatus)
                ]
            } else {
                parameters = [
                    "action"        : "changebookingstatus",
                    "driverId"      : String(driverId),
                    "bookingId"     : String(myStringBookingId),
                    "status"        : String(strStatus)
                ]
            }
            
                   
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                ERProgressHud.sharedInstance.hide()
                                let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                    // self.dismiss(animated: true, completion: nil)
                                    self.backClickMthod()
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
}
