//
//  PDOnlyOrdersTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class PDOnlyOrdersTableCell: UITableViewCell {

    @IBOutlet weak var imgProfileImage:UIImageView! {
        didSet {
            imgProfileImage.layer.cornerRadius = 40
            imgProfileImage.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblService:UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
