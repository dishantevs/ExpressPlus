//
//  PDOnlyOrders.swift
//  ExpressPlus
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class PDOnlyOrders: UIViewController {

    let cellReuseIdentifier = "pDOnlyOrdersTableCell"
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
    
    var page : Int! = 1
    var loadMore : Int! = 1;
    
    var strString:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "ORDERS"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnService:UIButton! {
        didSet {
            btnService.setTitle("Services", for: .normal)
            btnService.setTitleColor(.white, for: .normal)
            btnService.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var btnFoodDelivery:UIButton! {
        didSet {
            btnFoodDelivery.setTitle("Food Delivery", for: .normal)
            btnFoodDelivery.setTitleColor(.white, for: .normal)
            btnFoodDelivery.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var btnToGet:UIButton! {
        didSet {
            btnToGet.setTitle("To Get", for: .normal)
            btnToGet.setTitleColor(.white, for: .normal)
            btnToGet.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var lblDownOne:UILabel!
    @IBOutlet weak var lblDownTwo:UILabel!
    @IBOutlet weak var lblDownThree:UILabel!
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.separatorStyle = UITableViewCell.SeparatorStyle.none
        }
    }
    
    @IBOutlet weak var segMentControll:UISegmentedControl! {
        didSet {
            segMentControll.backgroundColor = .orange
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        
        // self.lblDownOne.isHidden = false
        // self.lblDownTwo.isHidden = true
        // self.lblDownThree.isHidden = true
        
        self.sideBarMenuClick()
        
        // btnFoodDelivery.addTarget(self, action: #selector(foodOrderListWB), for: .touchUpInside)
        // btnService.addTarget(self, action: #selector(serviceWB), for: .touchUpInside)
        // btnToGet.addTarget(self, action: #selector(toGetServiceWB), for: .touchUpInside)
        
        // self.serviceWB()
        
        
        self.segMentControll.isHidden = true
        self.segMentControll.selectedSegmentIndex = 0
        self.segMentControll.addTarget(self, action: #selector(segmentControlClick), for: .valueChanged)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.foodOrderListWB()
    }
    
    @objc func segmentControlClick() {
        
        if segMentControll.selectedSegmentIndex == 0 {
            self.serviceWB()
        } else if segMentControll.selectedSegmentIndex == 1 {
            self.foodOrderListWB()
        } else if segMentControll.selectedSegmentIndex == 2 {
            self.toGetServiceWB()
        } else if segMentControll.selectedSegmentIndex == 3 {
             self.specialRequestList()
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("call")
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    // MARK:- WEBSERVICE ( SERVICE JOB REQUEST ) -
    @objc func serviceWB() {
        
        // self.lblDownOne.isHidden = false
        // self.lblDownTwo.isHidden = true
        // self.lblDownThree.isHidden = true
        
        self.strString = "1"
     self.arrListOfAllMyOrders.removeAllObjects()
     
      ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => bookinglist
          [userId] => 116
          [userType] => Driver
          [status] =>
          [pageNo] => 2
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
                   parameters = [
                       "action"              : "requestbookinglist",
                        "userId"              : person["userId"] as Any,
                       // "userType"            : String("Driver"),
                       // "status"              : String(""),
                       // "pageNo"              : String("0")
             ]
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                  print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                ERProgressHud.sharedInstance.hide()
                                 
                                 var ar : NSArray!
                                 ar = (JSON["data"] as! Array<Any>) as NSArray
                                 self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                 
                                 self.tbleView.delegate = self
                                 self.tbleView.dataSource = self
                                 self.tbleView.reloadData()
                                 self.loadMore = 1;
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    
    // MARK:- WEBSERVICE ( SERVICE JOB REQUEST ) -
    @objc func foodOrderListWB() {
        
        // self.lblDownOne.isHidden = true
        // self.lblDownTwo.isHidden = false
        // self.lblDownThree.isHidden = true
        
        self.strString = "2"
     self.arrListOfAllMyOrders.removeAllObjects()
     
      ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => foodorderlist
      [userId] => 116
      [userType] => Driver
      [status] =>
      [pageNo] => 2
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
                   parameters = [
                       "action"              : "foodorderrequestlist",
                       // "userType"            : String("Driver"),
                        "driverId"              : person["userId"] as Any
             ]
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                ERProgressHud.sharedInstance.hide()
                                 
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                 
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                self.loadMore = 1
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    //
    // MARK:- WEBSERVICE ( SERVICE JOB REQUEST ) -
    @objc func toGetServiceWB() {
        
        // self.lblDownOne.isHidden = true
        // self.lblDownTwo.isHidden = true
        // self.lblDownThree.isHidden = false
        
        self.strString = "3"
     self.arrListOfAllMyOrders.removeAllObjects()
     
      ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => foodorderlist
      [userId] => 116
      [userType] => Driver
      [status] =>
      [pageNo] => 2
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
                   parameters = [
                       "action"              : "togetdriverrequestlist",
                        "driverId"              : person["userId"] as Any,
                       // "userType"            : String("Driver"),
                       // "status"              : String(""),
                       // "pageNo"              : String("1")
             ]
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                  print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                ERProgressHud.sharedInstance.hide()
                                 
                                 var ar : NSArray!
                                 ar = (JSON["data"] as! Array<Any>) as NSArray
                                 self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                 
                                 self.tbleView.delegate = self
                                 self.tbleView.dataSource = self
                                 self.tbleView.reloadData()
                                 self.loadMore = 1;
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    
    
    
    // MARK:- (SPECIAL ORDER ) -
    @objc func specialRequestList() {
        /*
         [action] => specialrequestlist
            [userId] => 267
            [userType] => Member
            [pageNo] => 2
         */
        
        
        

            // self.lblDownOne.isHidden = true
            // self.lblDownTwo.isHidden = true
            // self.lblDownThree.isHidden = false
            
            self.strString = "4"
            self.arrListOfAllMyOrders.removeAllObjects();
         
          ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
         let urlString = BASE_URL_EXPRESS_PLUS
        
         /*
          [action] => foodorderlist
          [userId] => 116
          [userType] => Driver
          [status] =>
          [pageNo] => 2
          */
         
            var parameters:Dictionary<AnyHashable, Any>!
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                // let str:String = person["role"] as! String
                parameters = [
                    "action"              : "specialrequestlist",
                    "userId"              : person["userId"] as Any,
                    "userType"            : String(person["role"] as! String),
                    "status"              : String(""),
                    "pageNo"              : String("1")
                ]
            }
            print("parameters-------\(String(describing: parameters))")
                       
            Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                {
                    response in
                   
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value {

                            let JSON = data as! NSDictionary
                            print(JSON as Any)
                                   
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                                   
                                     // var strSuccessAlert : String!
                                     // strSuccessAlert = JSON["msg"]as Any as? String
                                   
                            if strSuccess == String("success") {
                                print("yes")
                                     
                                ERProgressHud.sharedInstance.hide()
                                     
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                     
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                self.loadMore = 1;
                                     
                            }
                            else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                    
                            }
                        }

                               case .failure(_):
                                   print("Error message:\(String(describing: response.result.error))")
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   
                                   let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                   
                                   let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                           UIAlertAction in
                                           NSLog("OK Pressed")
                                       }
                                   
                                   alertController.addAction(okAction)
                                   
                                   self.present(alertController, animated: true, completion: nil)
                                   
                                   break
                                }
                           }
        
        
        
        
    }
    
    
    
    
    
}

extension PDOnlyOrders: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
        
        /*
        var numOfSection: NSInteger = 0

        if arrListOfAllMyOrders.count > 0 {
            
            self.tbleView.backgroundView = nil
            numOfSection = 1
            
         }
         else {
            
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: self.tbleView.bounds.size.width, height: self.tbleView.bounds.size.height))
            noDataLabel.text          = "No Data Found"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            self.tbleView.backgroundView  = noDataLabel
            
        }
        return numOfSection
        */
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PDOnlyOrdersTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! PDOnlyOrdersTableCell
        
        /*
         AVGRating = "";
         TIP = 10;
         address = "8, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
         bookingDate = "26th May";
         bookingId = 230;
         contactNumber = 2352639000;
         created = "2020-05-25 11:36:00";
         email = "tt@gmail.com";
         fullName = purnima;
         groupId = 46;
         image = "";
         serviceAmount = 100;
         serviceName = "Yard Work";
         servicePrice = 100;
         slote = "9:0-6:0";
         status = 2;
         totalAmount = 110;
         userAddress = "";
         userId = 117;
         zipCode = "";
         */
        
        /* // food delivery
        TIP = 5;
        cardNo = 4242424242424242;
        couponCode = "";
        created = "2020-05-25 11:18:00";
        discount = 0;
        driverName = "new driver 2.1";
        foodDetails =             (
                            {
                id = 49;
                name = babbSs;
                price = "20.00";
                quantity = 1;
                resturentId = 104;
            }
        );
        foodId = "";
        foodorderId = 89;
        noContact = "";
        resturentAddress = "Gwalior, Madhya Pradesh, India";
        resturentId = 104;
        resturentImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1588663958IMG20191105144302.jpg";
        resturentName = satish;
        specialNote = "";
        status = 2;
        totalAmount = 25;
        userAddress = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
        userId = 114;
        userImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1589889715IMG-20200519-WA0003.jpg";
        userName = kiwi;
        whatYouWant = "";
        workPlace = Home;
        */
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        
        if strString == "1" {
            
            cell.lblName.text = (item!["fullName"] as! String)
            
            cell.lblPrice.text = " - $"+(item!["totalAmount"] as! String)
            cell.lblDate.text = (item!["created"] as! String)
            cell.lblService.text = (item!["serviceName"] as! String)
            
            cell.imgProfileImage.sd_setImage(with: URL(string: (item!["image"] as! String)), placeholderImage: UIImage(named: "profile"))
            
        } else if strString == "2" {
            
            cell.lblName.text = (item!["resturentName"] as! String)
            
            
            
            cell.lblPrice.text = " - $"+(item!["totalAmount"] as! String)
            cell.lblDate.text = (item!["created"] as! String)
            cell.lblService.text = (item!["resturentddress"] as! String)
            
            cell.imgProfileImage.sd_setImage(with: URL(string: (item!["resturentImage"] as! String)), placeholderImage: UIImage(named: "food1"))
            
        } else if strString == "3" {
            
            cell.lblName.text = (item!["userName"] as! String)
            cell.lblPrice.text = " - $"+(item!["totalAmount"] as! String)
            cell.lblDate.text = (item!["created"] as! String)
            cell.lblService.text = (item!["address"] as! String)
            
            cell.imgProfileImage.sd_setImage(with: URL(string: (item!["userImage"] as! String)), placeholderImage: UIImage(named: "profile"))
            
        } else if strString == "4" {
            
            cell.lblName.text = (item!["whatYouWant"] as! String)
            
            if item!["totalAmount"] is String {
                                
                print("Yes, it's a String")
                
                cell.lblPrice.text = "$ "+(item!["totalAmount"] as! String)

            } else if item!["totalAmount"] is Int {
                
                print("It is Integer")
                
                let x2 : Int = (item!["totalAmount"] as! Int)
                let myString2 = String(x2)
                cell.lblPrice.text = "$ "+myString2
                
            } else {
              //some other check
                print("i am ")
                
                let temp:NSNumber = item!["totalAmount"] as! NSNumber
                let tempString = temp.stringValue
                cell.lblPrice.text = "$ "+tempString
                
            }
            
            cell.lblDate.text = (item!["created"] as! String)
            cell.lblService.text = (item!["address"] as! String)
            
            cell.imgProfileImage.sd_setImage(with: URL(string: (item!["userImage"] as! String)), placeholderImage: UIImage(named: "profile"))
            
        }
        
        cell.backgroundColor = .clear
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        
        return cell
    }

func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView .deselectRow(at: indexPath, animated: true)
    // dictGetAllServiceDetails
    print(strString as Any)
    
    let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
    
    let defaults = UserDefaults.standard

    let x2 : Int = (item!["foodorderId"] as! Int)
    let foodOrderId = String(x2)
    
    let x21 : Int = (item!["foodrequestId"] as! Int)
    let foodRequestId = String(x21)
    
    defaults.set(String(foodOrderId), forKey: "keySaveFoodOrderIdHere")
    defaults.set(String(foodRequestId), forKey: "keySaveFoodRequestIdHere")
    
    let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDRestaurantDetailsOrdersId") as? PDRestaurantDetailsOrders
    settingsVCId!.dictGetFoodDetails = item as NSDictionary?
    settingsVCId!.strDeliveryDetailsFromUserProfile = "yes"
    settingsVCId!.deliveryDetails = "nice"
    self.navigationController?.pushViewController(settingsVCId!, animated: true)
    
    /*if strString == "1" {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDOnlyOrdersServiceDetailsId") as? PDOnlyOrdersServiceDetails
         settingsVCId!.dictGetDriveServiceDetails = item as NSDictionary?
        // settingsVCId!.fromDriverScreen2 = "yesFromDriverPortal"
         self.navigationController?.pushViewController(settingsVCId!, animated: true)
//        self.present(settingsVCId!, animated: true, completion: nil)
        
    } else if strString == "2" {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDRestaurantDetailsOrdersId") as? PDRestaurantDetailsOrders
        settingsVCId!.dictGetFoodDetails = item as NSDictionary?
        settingsVCId!.strDeliveryDetailsFromUserProfile = "yes"
        settingsVCId!.deliveryDetails = "nice"
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    } else if strString == "3" {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDOnlyGetDetailsId") as? PDOnlyGetDetails
         settingsVCId!.dictGetDriveGetDetails = item as NSDictionary?
        // settingsVCId!.fromDriver = "iAmFromDriverScreen"
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    } else if strString == "4" {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpecialOrderDetailsCustomerId") as? SpecialOrderDetailsCustomer
        settingsVCId!.dictGetSpecialOrderData = item as NSDictionary?
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }*/
     
}
    
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
    
}
    
}


extension PDOnlyOrders: UITableViewDelegate {
    
}
