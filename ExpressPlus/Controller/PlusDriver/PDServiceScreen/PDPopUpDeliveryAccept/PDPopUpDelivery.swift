//
//  PDPopUpDelivery.swift
//  ExpressPlus
//
//  Created by Apple on 11/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

// MARK:- LOCATION -
import CoreLocation

class PDPopUpDelivery: UIViewController, CLLocationManagerDelegate {

    let cellReuseIdentifier = "popupDeliveryTableCell"
    
    var dictGetFoodDeliveryDetailsForAccept:NSDictionary!
    
    var dictGetFoodDeliveryDetailsForAcceptNew:NSDictionary!
    
    let locationManager = CLLocationManager()
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    var foodOrderSavedid:String!
    var foodrequestSavedid:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblDeliveryInMiles:UILabel!
    @IBOutlet weak var lblDeliveryTime:UILabel!
    @IBOutlet weak var lblTipAmount:UILabel!
    @IBOutlet weak var lblTotalAmount:UILabel!
    @IBOutlet weak var lblPickUpLocation:UILabel!
    @IBOutlet weak var lblDropLocation:UILabel!
    
    
    @IBOutlet weak var btnGoogleMaps:UIButton!
    
    @IBOutlet weak var btnAccept:UIButton! {
        didSet {
            btnAccept.layer.cornerRadius = 8
            btnAccept.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnDecline:UIButton! {
        didSet {
            btnDecline.layer.cornerRadius = 8
            btnDecline.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
            tbleView.tableFooterView = UIView.init()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        
        // print(dictGetFoodDeliveryDetailsForAccept as Any)
        print(dictGetFoodDeliveryDetailsForAcceptNew as Any)
        
        self.strSaveLatitude = "0"
        self.strSaveLongitude = "0"
        
        /*
         Optional({
             EstTime = "15 Minutes";
             Mile = "4 Mile";
             TIP = 10;
             address = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
             assignDriver = "";
             cardNo = 4242424242424242;
             city = Ghaziabad;
             couponCode = "";
             created = "2020-06-01 18:47:00";
             deliveryFee = "";
             deliveryLat = "28.6634675";
             deliveryLong = "77.3240365";
             discount = 0;
             driverAVG = 0;
             driverContactNumber = "";
             driverId = "";
             driverImage = "";
             driverName = "";
             foodDetails =     (
                         {
                     id = 44;
                     name = pizza;
                     price = "230.00";
                     quantity = 1;
                     resturentId = 98;
                 }
             );
             foodDetailsNew =     (
                         {
                     categoryId = 66;
                     description = "pizza for all";
                     foodName = pizza;
                     foodTag = "";
                     foodType = "";
                     "image_1" = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/foods/1587617132IMG-20200319-WA0009.jpeg";
                     "image_2" = "";
                     "image_3" = "";
                     "image_4" = "";
                     "image_5" = "";
                     menuId = 44;
                     price = 100;
                     quantity = 1;
                     resturentId = 98;
                     specialPrice = 230;
                 }
             );
             foodId = "";
             foodorderId = 99;
             landmark = "purnimafit@gmail.com";
             name = "Purnima Pandey";
             noContact = "";
             phone = 8956231425;
             resturentAddress = "Ramprastha Greens, Vaishali, Ghaziabad, Uttar Pradesh, India";
             resturentId = 98;
             resturentLatitude = "28.6493401";
             resturentLongitude = "77.3475174";
             resturentName = "purnima rest new";
             salesTax = 0;
             specialNote = test;
             state = "Uttar Pradesh";
             status = 10;
             storecity = "";
             totalAmount = 240;
             userAVG = 0;
             userId = 124;
             userName = customers;
             whatYouWant = "";
             workPlace = Home;
             zipcode = 201011;
         })
         (lldb)
         */
        
        self.tbleView.delegate = self
        self.tbleView.dataSource = self
        self.tbleView.reloadData()
        
        self.btnAccept.addTarget(self, action: #selector(acceptServiceClickMethod), for: .touchUpInside)
        self.btnDecline.addTarget(self, action: #selector(declineServiceClickMethod), for: .touchUpInside)
        
        self.btnGoogleMaps.addTarget(self, action: #selector(openGoogleMaps), for: .touchUpInside)
        
        self.iAmHereForLocationPermission()
    }
    
    @objc func openGoogleMaps() {
        
        let alert = UIAlertController(title: String("Express Plus"), message: String("Please Google to the closest restaurant to you based on this order."), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
            
            
            if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                UIApplication.shared.openURL(NSURL(string:
                                                    "comgooglemaps://?saddr=&daddr=\(self.strSaveLatitude!),\(self.strSaveLongitude!)&directionsmode=driving")! as URL)

            } else {
                NSLog("Can't use comgooglemaps://")
                
                let alert = UIAlertController(title: String("Error"), message: String("Either Google Maps is not installed in your Device or your Device does not support Google Maps"), preferredStyle: UIAlertController.Style.alert)
               
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                     
         
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            }
         
            
        }))
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    // MARK:- GET CUSTOMER LOCATION -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! PDBagPurchaseTableCell
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            //print(self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            print("locality ==> "+locality as Any) // sector 10 dwarka
            
            print(self.strSaveCountryName as Any) // india
            print(self.strSaveStateName as Any) // new delhi
            print(self.strSaveZipcodeName as Any) // 110075
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            
            
            // self.findMyStateTaxWB()
        }
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // foodorderdetails()
    }
    
    @objc func acceptServiceClickMethod() {
        self.acceptOrDecline(strStatus: "2")
    }
    
    @objc func declineServiceClickMethod() {
        
        let alert = UIAlertController(title: String("Alert!"), message: String("Are you sure you want to skip ?"), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes, Skip", style: UIAlertAction.Style.destructive, handler: { action in
             self.acceptOrDecline(strStatus: "3")
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: { action in
             
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    // MARK:- WEBSERVICE ( ACCEPT OR DECLINE ) -
    @objc func acceptOrDecline(strStatus:String) {
        
        if strStatus == "2" {
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "accepting...")
        } else {
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "skipping...")
        }
        
    /*
     /*
     [action] => foodrequestaccept
     [userId] => 123
     [foodrequestId] => 920
     [foodorderId] => 102
     [status] => 3 // reject
     */
     */
    
    /*
    [action] => foodrequestaccept
    [userId] => 123
    [foodrequestId] => 943
    [foodorderId] => 104
    [status] => 2 // accept
    */
        
        let urlString = BASE_URL_EXPRESS_PLUS
    
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            // let x : Int = dictGetDriveGetDetails["bookingId"] as! Int
            // let myStringBookingId = String(x)
            
            let x2 : Int = person["userId"] as! Int
            let driverId = String(x2)
            
            /*let x3 : Int = dictGetFoodDeliveryDetailsForAccept["foodrequestId"] as! Int
            let foodRequestId = String(x3)
            
            // togetDriverRequestId
            let x4 : Int = dictGetFoodDeliveryDetailsForAccept["foodorderId"] as! Int
            let foodOrderId = String(x4)
             
             var foodOrderSavedid:String!
             var foodrequestSavedid:String!
             */
            
            let defaults = UserDefaults.standard
            if let myString = defaults.string(forKey: "keySaveFoodOrderIdHere") {
                print("defaults savedString: \(myString)")
                
                self.foodOrderSavedid = String(myString)
            }
            
            if let myString = defaults.string(forKey: "keySaveFoodRequestIdHere") {
                print("defaults savedString: \(myString)")
                
                self.foodrequestSavedid = String(myString)
            }
            
            
            if strStatus == "3" {
                parameters = [
                    "action"        : "foodrequestaccept", // done
                    "userId"        : String(driverId), // done
                    "foodrequestId" : String(foodrequestSavedid),
                    "foodorderId"   : String(foodOrderSavedid),
                    "status"        : String(strStatus) // done
                ]
            } else {
                parameters = [
                    "action"        : "foodrequestaccept", // done
                    "userId"        : String(driverId), // done
                    "foodrequestId" : String(foodrequestSavedid),
                    "foodorderId"   : String(foodOrderSavedid),
                    "status"        : String(strStatus) // done
                ]
                
                
            }
            
                   
     }
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
        {
            response in
               
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                               
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                    if strSuccess == String("success") {
                        print("yes")
                                 
                        var strSuccess2 : String!
                        strSuccess2 = JSON["msg"]as Any as? String
                                
                        ERProgressHud.sharedInstance.hide()
                        let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                    // self.dismiss(animated: true, completion: nil)
                                    // self.sideBarMenuClick()
                                    
                            if strStatus == "2" {
                                
                                self.navigationController?.popViewController(animated: true)
                                
                            } else {
                                
                                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDOnlyOrdersId") as? PDOnlyOrders
                                self.navigationController?.pushViewController(settingsVCId!, animated: true)
                                
                            }
                            
                            
                                    
                                    /*if strStatus == "3" {
                                        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDOnlyOrdersId") as? PDOnlyOrders
                                        self.navigationController?.pushViewController(settingsVCId!, animated: true)
                                    } else {
                                        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDOnlyOrdersId") as? PDOnlyOrders
                                        self.navigationController?.pushViewController(settingsVCId!, animated: true)
                                       
                                        
                                    }*/
                                    
                        }))
                        self.present(alert, animated: true, completion: nil)
                                
                    }
                    else if strSuccess == String("Fails") {
                        var strSuccess2 : String!
                        strSuccess2 = JSON["msg"]as Any as? String
                                
                        ERProgressHud.sharedInstance.hide()
                        let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                    // self.dismiss(animated: true, completion: nil)
                                    
                            self.sideBarMenuClick()
                                     
                        }))
                        self.present(alert, animated: true, completion: nil)
                                
                                
                    }
                }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    
}


//MARK:- TABLE VIEW
extension PDPopUpDelivery: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 // self.arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PopupDeliveryTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! PopupDeliveryTableCell
        
        cell.backgroundColor = .white
       
        if indexPath.row == 0 {
            
            cell.lblTitle.text = "Restaurant Name : "
            cell.lblSubTitle.text = (dictGetFoodDeliveryDetailsForAccept["resturentName"] as! String)
            
        } else if indexPath.row == 1 {
            
            cell.lblTitle.text = "Drop Location : "
            cell.lblSubTitle.text = (dictGetFoodDeliveryDetailsForAccept["resturentAddress"] as! String)
            
        }
        
        /*if indexPath.row == 0 {
            
            cell.lblTitle.text = "Mile"
            cell.lblSubTitle.text = (dictGetFoodDeliveryDetailsForAccept["Mile"] as! String)
            
        } else if indexPath.row == 1 {
            
            cell.lblTitle.text = "Estimate Delivery Time"
            cell.lblSubTitle.text = (dictGetFoodDeliveryDetailsForAccept["EstTime"] as! String)
            
        } else if indexPath.row == 2 {
            
            cell.lblTitle.text = "Total Amount"
            cell.lblSubTitle.text = "$ "+(dictGetFoodDeliveryDetailsForAccept["totalAmount"] as! String)
            
        } else if indexPath.row == 3 {
            
            cell.lblTitle.text = "Pickup Location"
            cell.lblSubTitle.text = (dictGetFoodDeliveryDetailsForAccept["resturentAddress"] as! String)
            
        } else {
            
            cell.lblTitle.text = "Drop Location"
            cell.lblSubTitle.text = (dictGetFoodDeliveryDetailsForAccept["address"] as! String)
            
        }*/
        
//        cell.lblDeliveryInMiles.text    = (dictGetFoodDeliveryDetailsForAccept["Mile"] as! String)
//        cell.lblDeliveryTime.text       = (dictGetFoodDeliveryDetailsForAccept["EstTime"] as! String)
//        cell.lblTotalAmount.text        = "$ "+(dictGetFoodDeliveryDetailsForAccept["totalAmount"] as! String)
//        cell.lblPickUpLocation.text     = (dictGetFoodDeliveryDetailsForAccept["resturentAddress"] as! String)
//        cell.lblDropLocation.text       = (dictGetFoodDeliveryDetailsForAccept["address"] as! String)
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0  {
             return UITableView.automaticDimension
        } else if indexPath.row == 1  {
             return UITableView.automaticDimension
        } else if indexPath.row == 2  {
            return 0
        } else {
            return UITableView.automaticDimension
        }
        
    }
    
}

extension PDPopUpDelivery: UITableViewDelegate {
    
}

