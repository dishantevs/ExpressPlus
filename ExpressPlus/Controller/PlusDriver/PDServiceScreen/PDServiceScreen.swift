//
//  PDServiceScreen.swift
//  ExpressPlus
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire

class PDServiceScreen: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    var dictGetDeliveryDetail:NSDictionary!
    
    var driverNiceTwo:String!
    
    var dict2Try:NSDictionary!
    
    var dict202: Dictionary<AnyHashable, Any> = [:]
    
    var locationManager:CLLocationManager!
    // var mapView:MKMapView!
    var myDouble12:Double!
    var  myDouble22 : Double!
    @IBOutlet weak var mapView:MKMapView!
    @IBOutlet weak var mapView2:MKMapView!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "Delivery Details"
        }
    }
    
    @IBOutlet weak var viewOneUpper:UIView! {
        didSet {
            viewOneUpper.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
            viewOneUpper.layer.cornerRadius = 6
            viewOneUpper.clipsToBounds = true
        }
    }
    @IBOutlet weak var viewOneSecond:UIView! {
        didSet {
            viewOneSecond.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var viewTwoUpper:UIView! {
        didSet {
            viewTwoUpper.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
            viewTwoUpper.layer.cornerRadius = 6
            viewTwoUpper.clipsToBounds = true
        }
    }
    @IBOutlet weak var viewTwoSecond:UIView! {
        didSet {
            viewTwoSecond.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblRestaurantName:UILabel!
    @IBOutlet weak var lblTotalPrice:UILabel!
    
    @IBOutlet weak var lblRestaurantTotalPrice:UILabel!
    
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblRestaurantAddress:UILabel!
    
    
    
    @IBOutlet weak var btnDeliveryStatus:UIButton! {
        didSet {
            btnDeliveryStatus.layer.cornerRadius = 4
            btnDeliveryStatus.clipsToBounds = true
            btnDeliveryStatus.setTitleColor(.white, for: .normal)
            btnDeliveryStatus.backgroundColor = .systemGreen
        }
    }
    
    @IBOutlet weak var btnConfirmPickUp:UIButton! {
        didSet {
            btnConfirmPickUp.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
          // print(dictGetDeliveryDetail as Any)
          // print(dict2Try as Any)
        
        /*
         Optional({
             EstTime = "15 Minutes";
             Mile = "4 Mile";
             TIP = 5;
             address = "SW 344 St & SW 2 Av (E/F), Florida City, FL 33034, USA";
             assignDriver = 123;
             cardNo = 4242424242424242;
             city = "Florida City";
             couponCode = "";

             created = "2020-06-01 18:57:00";
             deliveryFee = "";
             deliveryLat = "25.4478898";
             deliveryLong = "-80.47922369999999";
             discount = 0;
             driverAVG = "2.67";
             driverContactNumber = 9632356000;
             driverId = 123;
             driverImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1590998442IMG-20200601-WA0012.jpg";
             driverName = Drix;
             foodDetails =     (
                         {
                     id = 44;
                     name = pizza;
                     price = "230.00";
                     quantity = 1;
                     resturentId = 98;
                 }
             );
             foodDetailsNew =     (
                         {
                     categoryId = 66;
                     description = "pizza for all";
                     foodName = pizza;
                     foodTag = "";
                     foodType = "";
                     "image_1" = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/foods/1587617132IMG-20200319-WA0009.jpeg";
                     "image_2" = "";
                     "image_3" = "";
                     "image_4" = "";
                     "image_5" = "";
                     menuId = 44;
                     price = 100;
                     quantity = 1;
                     resturentId = 98;
                     specialPrice = 230;
                 }
             );
             foodId = "";
             foodorderId = 101;
             landmark = "purnimafit@gmail.com";
             name = "Purnima Pandey";
             noContact = "";
             phone = 8956231425;
             resturentAddress = "Ramprastha Greens, Vaishali, Ghaziabad, Uttar Pradesh, India";
             resturentId = 98;
             resturentLatitude = "28.6493401";
             resturentLongitude = "77.3475174";
             resturentName = "purnima rest new";
             salesTax = 0;
             specialNote = "";
             state = Florida;
             status = 4;
             storecity = "";
             totalAmount = 235;
             userAVG = 0;
             userId = 124;
             userName = customers;
             whatYouWant = "";
             workPlace = Home;
             zipcode = 33034;
         })
         (lldb)
         */
        // status = 4; delivered
        // status = 2; on the way
        
        // btnDeliveryStatus
        
        // print(driverNiceTwo as Any)
        
        /*
        let x : Int = dictGetDeliveryDetail["status"] as! Int
        let myString = String(x)
        if String(myString) == "2" {
            self.btnDeliveryStatus.setTitle("On the way", for: .normal)
        } else if String(myString) == "4" {
            self.btnDeliveryStatus.setTitle("Delivered", for: .normal)
        }
        */
        
        /*
         2020-06-02 18:53:45.950771+0530 ExpressPlus[6425:147775] Metal API Validation Enabled
         Optional({
             EstTime = "15 Minutes";
             Mile = "4 Mile";
             TIP = 5;
             address = "117, Karkar Duma, Ram Vihar, Anand Vihar, Delhi, 110092, India";
             assignDriver = 116;
             cardNo = 4242424242424242;
             city = Delhi;
             couponCode = "";
             created = "2020-05-25 11:18:00";
             deliveryFee = "";
             deliveryLat = "28.650218";
             deliveryLong = "77.30270589999999";
             discount = 0;
             driverAVG = "2.5";
             driverContactNumber = 5623895236;
             driverId = 116;
             driverImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1590385622IMG-20200520-WA0005.jpg";
             driverName = "new driver 2.1";
             foodDetails =     (
                         {
                     id = 49;
                     name = babbSs;
                     price = "20.00";
                     quantity = 1;
                     resturentId = 104;
                 }
             );
             foodDetailsNew =     (
                         {
                     categoryId = 67;
                     description = vzhhss;
                     foodName = babbSs;
                     foodTag = "";
                     foodType = "";
                     "image_1" = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/foods/1588664112IMG20200226201604.jpg";
                     "image_2" = "";
                     "image_3" = "";
                     "image_4" = "";
                     "image_5" = "";
                     menuId = 49;
                     price = 25;
                     quantity = 1;
                     resturentId = 104;
                     specialPrice = 20;
                 }
             );
             foodId = "";
             foodorderId = 89;
             landmark = test;
             name = purnima;
             noContact = "";
             phone = 5623852369;
             resturentAddress = "Gwalior, Madhya Pradesh, India";
             resturentId = 104;
             resturentLatitude = "26.2182871";
             resturentLongitude = "";
             resturentName = satish;
             salesTax = 0;
             specialNote = "";
             state = Delhi;
             status = 2;
             storecity = "";
             totalAmount = 25;
             userAVG = 0;
             userId = 114;
             userName = kiwi;
             whatYouWant = "";
             workPlace = Home;
             zipcode = 110092;
         transactionFee = "1.53";
         })
         (lldb)
         */
        
        let addressLandmarkAndNotes = (dictGetDeliveryDetail["address"] as! String)+"\nLandmark: "+(dictGetDeliveryDetail["landmark"] as! String)+"\nSpecial notes : "+(dictGetDeliveryDetail["specialNote"] as! String)
        
        self.lblName.text = (dictGetDeliveryDetail["name"] as! String)
        self.lblRestaurantName.text = (dictGetDeliveryDetail["resturentName"] as! String)
        
        let addtransactionFeeWithTotalAmount = Double((dictGetDeliveryDetail["totalAmount"] as! String))!+Double((dictGetDeliveryDetail["transactionFee"] as! String))!
        
        self.lblTotalPrice.text = "$ "+String(addtransactionFeeWithTotalAmount)
        self.lblRestaurantTotalPrice.text = "$ "+String(addtransactionFeeWithTotalAmount)
        
        self.lblAddress.text = String(addressLandmarkAndNotes)
        self.lblRestaurantAddress.text = (dictGetDeliveryDetail["resturentAddress"] as! String)
    }
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func confirmPickUpClickMethod() {
        print(dictGetDeliveryDetail as Any)
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDInProgressId") as? PDInProgress
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.foodorderdetails()
        // Create and Add MapView to our main view
        
    }
    @objc func foodorderdetails() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        let urlString = BASE_URL_EXPRESS_PLUS
    
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            // let x : Int = dictGetDriveGetDetails["bookingId"] as! Int
            // let myStringBookingId = String(x)
            
            let x2 : Int = person["userId"] as! Int
            let driverId = String(x2)
            
            // togetDriverRequestId
            let x4 : Int = dict2Try["foodorderId"] as! Int
            let foodOrderId = String(x4)
            
                parameters = [
                    "action"                : "foodorderdetails",
                    "foodorderId"              : String(foodOrderId),
                    
                ]
            
            
                   
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                                self.dict202 = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                
                                
                                if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                                       if (person["role"] as! String) == "Driver" {
                                        
                                        
                                        // let x : Int = self.dict202["status"] as! Int
                                        // let myString = String(x)
                                        
                                        if self.dict202["status"] as! String == "10" {
                                            
                                            self.btnDeliveryStatus.setTitle("Accept", for: .normal)
                                            self.btnDeliveryStatus.addTarget(self, action: #selector(self.openPopupClick), for: .touchUpInside)
                                            
                                        } else {
                                           
                                            if self.dict202["PaymentStatus"] is String {
                                                              
                                                print("Yes, it's a String")
                                                
                                                
                                                if self.dict202["PaymentStatus"] as! String == "1" {
                                                    self.btnDeliveryStatus.setTitle(" Payment Not Done ", for: .normal)
                                                    
                                                    self.btnDeliveryStatus.addTarget(self, action: #selector(self.paymentNotDone), for: .touchUpInside)
                                                    
                                                } else {
                                                    self.btnDeliveryStatus.setTitle("Confirm", for: .normal)
                                                    
                                                    self.btnDeliveryStatus.addTarget(self, action: #selector(self.paymentNotDone1), for: .touchUpInside)
                                                    
                                                    
                                                    
                                                }
                                                

                                            } else if self.dict202["PaymentStatus"] is Int {
                                              
                                                print("It is Integer")
                                              
                                                let x2 : Int = (self.dict202["PaymentStatus"] as! Int)
                                                let myString2 = String(x2)
                                                
                                                
                                                if myString2 == "1" {
                                                    self.btnDeliveryStatus.setTitle(" Payment Not Done ", for: .normal)
                                                    
                                                    self.btnDeliveryStatus.addTarget(self, action: #selector(self.paymentNotDone), for: .touchUpInside)
                                                    
                                                    
                                                } else {
                                                    self.btnDeliveryStatus.setTitle("Confirm", for: .normal)
                                                    
                                                    self.btnDeliveryStatus.addTarget(self, action: #selector(self.paymentNotDone1), for: .touchUpInside)
                                                    
                                                    
                                                    
                                                }
                                                
                                                
                                                
                                                // self.btnDeliveryStatus.setTitle(myString2, for: .normal)
                                              
                                            } else {
                                            //some other check
                                              print("i am ")
                                              
                                                let temp:NSNumber = self.dict202["PaymentStatus"] as! NSNumber
                                                let tempString = temp.stringValue
                                                
                                                
                                                if tempString == "1" {
                                                    self.btnDeliveryStatus.setTitle(" Payment Not Done ", for: .normal)
                                                    
                                                    self.btnDeliveryStatus.addTarget(self, action: #selector(self.paymentNotDone), for: .touchUpInside)
                                                    
                                                } else {
                                                    self.btnDeliveryStatus.setTitle("Confirm", for: .normal)
                                                    
                                                    self.btnDeliveryStatus.addTarget(self, action: #selector(self.paymentNotDone1), for: .touchUpInside)
                                                    
                                                    
                                                }
                                                
                                                
                                                
                                                // self.btnDeliveryStatus.setTitle(tempString, for: .normal)
                                              
                                            }
                                            
                                        }
                                        
                                        
                                        
                                        
                                        
                                        /*
                                        // let x : Int = dict["PaymentStatus"] as! Int
                                        // let myString = String(x)
                                        
                                        if myString == "1" {
                                            self.btnDeliveryStatus.setTitle("Payment Not Done", for: .normal)
                                        } else if myString == "2" {
                                            self.btnDeliveryStatus.setTitle("Confirm", for: .normal)
                                        } else {
                                            self.btnDeliveryStatus.isHidden = true
                                        }
                                        */
                                       } else {
                                        
                                        let x : Int = self.dict202["status"] as! Int
                                        let myString = String(x)
                                        
                                        if myString == "10" {
                                            self.btnDeliveryStatus.setTitle("Accept", for: .normal)
                                            
                                        } else if myString == "1" {
                                            self.btnDeliveryStatus.setTitle("Confirm", for: .normal)
                                            // self.btnDeliveryStatus.addTarget(self, action: #selector(self.confirmPickUpClickMethod), for: .touchUpInside)
                                        }
                                        
                                        self.btnDeliveryStatus.addTarget(self, action: #selector(self.openPopupClick), for: .touchUpInside)
                                        
                                       }
                                    
                                }
                                
                                
                                
                                self.createMapView()
                               }
                               else if strSuccess == String("Fails") {
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                ERProgressHud.sharedInstance.hide()
                                let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                    // self.dismiss(animated: true, completion: nil)
                                    // self.sideBarMenuClick()
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    @objc func paymentNotDone() {
        let alert = UIAlertController(title: String("Pending Payment"), message: String("Payment still pending from user side."), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
             
 
        }))
        
         
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func paymentNotDone1() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDInProgressId") as? PDInProgress
        
        settingsVCId!.dictOne = self.dict202 as NSDictionary//self.dictGetDeliveryDetail
        settingsVCId!.dictTwo = self.dict2Try
        
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        determineCurrentLocation()
    }
    @objc func openPopupClick() {
        
        // print(self.btnDeliveryStatus.titleLabel?.text as Any)
        if self.btnDeliveryStatus.titleLabel?.text == "Confirm" {
            
            // print(dictGetDeliveryDetail as Any)
            // print(dict2Try as Any)
            
            self.confirmFoodOrderWB()
            
            
        } else if self.btnDeliveryStatus.titleLabel?.text == "Accept"  {
            
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDPopUpDeliveryId") as? PDPopUpDelivery
            settingsVCId!.dictGetFoodDeliveryDetailsForAccept = dictGetDeliveryDetail
            settingsVCId!.dictGetFoodDeliveryDetailsForAcceptNew = dict2Try
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
            
        } else if self.btnDeliveryStatus.titleLabel?.text == "Delivered"  {
            
            
            
        } else {
            
            print("third party")
            print(dictGetDeliveryDetail as Any)
            print(dict2Try as Any)
            
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDInProgressId") as? PDInProgress
            
            settingsVCId!.dictOne = self.dictGetDeliveryDetail
            settingsVCId!.dictTwo = self.dict2Try
            
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
            
        }
        
    }
    
    // MARK:- CONFIRM FOOD -
    @objc func confirmFoodOrderWB() {
        
        /*
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDInProgressId") as? PDInProgress
        
        // settingsVCId!.dictOne = self.dictGetDeliveryDetail
        // settingsVCId!.dictTwo = self.dict2Try
        
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        */
        /*
         [action] => updatefoodorder
         [foodorderId] => 214
         [driverId] => 279
         [status] => 2
         */
        
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
            
        let urlString = BASE_URL_EXPRESS_PLUS
        
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
               
            let x2 : Int = person["userId"] as! Int
            let driverId = String(x2)
                
            let x4 : Int = dict2Try["foodorderId"] as! Int
            let foodOrderId = String(x4)
                
            parameters = [
                "action"        : "updatefoodorder",
                "foodorderId"   : String(foodOrderId),
                "driverId"      : String(driverId),
                "status"        : String("2")
            ]
        }
        print("parameters-------\(String(describing: parameters))")
                       
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                        {
                            response in
                   
                            switch(response.result) {
                            case .success(_):
                                if let data = response.result.value {

                                    let JSON = data as! NSDictionary
                                    print(JSON as Any)
                                   
                                    var strSuccess : String!
                                    strSuccess = JSON["status"]as Any as? String
                                   
                                    var strSuccessAlert : String!
                                    strSuccessAlert = JSON["msg"]as Any as? String
                                   
                                    if strSuccess == String("success") {
                                        print("yes")
                                        ERProgressHud.sharedInstance.hide()
                                    
                                        let alert = UIAlertController(title: String(strSuccess), message: String(strSuccessAlert), preferredStyle: UIAlertController.Style.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                        
                                            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDInProgressId") as? PDInProgress
                                            
                                            settingsVCId!.dictOne = self.dictGetDeliveryDetail
                                            settingsVCId!.dictTwo = self.dict2Try
                                            
                                            self.navigationController?.pushViewController(settingsVCId!, animated: true)
                                        
                                        }))
                                        self.present(alert, animated: true, completion: nil)
                                    
                                    }
                                    else if strSuccess == String("Fails") {
                                        var strSuccess2 : String!
                                        strSuccess2 = JSON["msg"]as Any as? String
                                    
                                        ERProgressHud.sharedInstance.hide()
                                        
                                        let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                        
                                        }))
                                        self.present(alert, animated: true, completion: nil)
                                    
                                    }
                                }

                            case .failure(_):
                                print("Error message:\(String(describing: response.result.error))")
                                   
                                ERProgressHud.sharedInstance.hide()
                                   
                                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                   
                                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                    UIAlertAction in
                                    NSLog("OK Pressed")
                                }
                                   
                                alertController.addAction(okAction)
                                   
                                self.present(alertController, animated: true, completion: nil)
                                   
                                break
                            }
        }
        
    }
    
    func createMapView() {
        // mapView = MKMapView()
        
        // let leftMargin:CGFloat = 10
        // let topMargin:CGFloat = 60
        // let mapWidth:CGFloat = view.frame.size.width-20
        // let mapHeight:CGFloat = 300
        
        // mapView.frame = CGRectMake(leftMargin, topMargin, mapWidth, mapHeight)
        // mapView.frame = CGRect(x: leftMargin, y: topMargin, width: mapWidth, height: mapHeight)
        
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        // mapView.center = view.center
        // view.addSubview(mapView)
        
        
        mapView2.mapType = MKMapType.standard
        mapView2.isZoomEnabled = true
        mapView2.isScrollEnabled = true
        // mapView2.center = view.center
        // view.addSubview(mapView2)
        
    }
    func determineCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            //locationManager.startUpdatingHeading()
            locationManager.startUpdatingLocation()
        }
    }
    // private func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        // let editedText1 = (dictGetDeliveryDetail["deliveryLat"] as! String).replacingOccurrences(of: "-", with: "")
        // let editedText2 = (dictGetDeliveryDetail["deliveryLong"] as! String).replacingOccurrences(of: "-", with: "")
        
        // upper map
        let myDouble1 = Double(dictGetDeliveryDetail["deliveryLat"] as! String)
        let myDouble2 = Double(dictGetDeliveryDetail["deliveryLong"] as! String)
        
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(myDouble1!, myDouble2!)
        myAnnotation.title = (dictGetDeliveryDetail["name"] as! String)
        mapView.addAnnotation(myAnnotation)
        
        
        // lower map
        let myDouble11 = Double(dictGetDeliveryDetail["resturentLatitude"] as! String)
        let myDouble22 = Double(dictGetDeliveryDetail["resturentLongitude"] as! String)
        
        let myAnnotation12: MKPointAnnotation = MKPointAnnotation()
        myAnnotation12.coordinate = CLLocationCoordinate2DMake(myDouble11!, myDouble22!)
        myAnnotation12.title = (dictGetDeliveryDetail["resturentName"] as! String)
        mapView2.addAnnotation(myAnnotation12)
        
        /*
        let userLocation:CLLocation = locations[0] as CLLocation
        
        let editedText1 = (dictGetDeliveryDetail["deliveryLat"] as! String).replacingOccurrences(of: "-", with: "")
        let editedText2 = (dictGetDeliveryDetail["deliveryLong"] as! String).replacingOccurrences(of: "-", with: "")
        
        let myDouble1 = Double(editedText1)
         
        let myDouble2 = Double(editedText2)
        
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: myDouble1!, longitudeDelta: myDouble2!))
        
        mapView.setRegion(region, animated: true)
        
        // Drop a pin at user's Current Location
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
        myAnnotation.title = (dictGetDeliveryDetail["name"] as! String)
        mapView.addAnnotation(myAnnotation)
        
        
        print(Double(dictGetDeliveryDetail["resturentLongitude"] as! String) as Any )
        
        // #2
         
        let userLocation2:CLLocation = locations[0] as CLLocation
        
        
        
        
        
        print(dictGetDeliveryDetail as Any)
        
        
        
        
        // restaurant
        let trimmedString = (dictGetDeliveryDetail["resturentLatitude"] as! String).trimmingCharacters(in: .whitespaces)
        print(trimmedString as Any)
        
        myDouble12 = Double(trimmedString)
        print(myDouble12 as Any)
        
        
         myDouble12 = Double(dictGetDeliveryDetail["resturentLatitude"] as! String)
        if myDouble12 == nil {
            myDouble12 = Double("28.5849")
        } else {
            myDouble12 = Double(dictGetDeliveryDetail["resturentLatitude"] as! String)
        }
        
        
        
        
        // print(myDouble12 as Any)
        // print(myDouble22 as Any)
        
        myDouble22 = Double(dictGetDeliveryDetail["resturentLongitude"] as! String)
        if myDouble22 == nil {
            myDouble22 = Double("77.0583")
        } else {
            myDouble22 = Double(dictGetDeliveryDetail["resturentLongitude"] as! String)
        }
        
        let center2 = CLLocationCoordinate2D(latitude: userLocation2.coordinate.latitude, longitude: userLocation2.coordinate.longitude)
        let region2 = MKCoordinateRegion(center: center2, span: MKCoordinateSpan(latitudeDelta: myDouble12!, longitudeDelta: myDouble22!))
        
        mapView2.setRegion(region2, animated: true)
        
        // Drop a pin at user's Current Location
        let myAnnotation2: MKPointAnnotation = MKPointAnnotation()
        myAnnotation2.coordinate = CLLocationCoordinate2DMake(userLocation2.coordinate.latitude, userLocation2.coordinate.longitude);
        myAnnotation2.title = (dictGetDeliveryDetail["name"] as! String)
        mapView2.addAnnotation(myAnnotation2)
        */
    }
    
    private func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error \(error)")
    }
}
