//
//  PDPaymentInformation.swift
//  ExpressPlus
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class PDPaymentInformation: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var editDriverProfileIsYesOrNo:String!
    
    let cellReuseIdentifier = "pDpaymentInformationTableCell"
    
    var BankType = ["Current", "Saving"]
    var itemSelected = ""
    
    var fromRestaurantPage:String!
    
    var pickerView: UIPickerView?
    var pickerViewSection: UIPickerView?
    
    var getImgDataOne:Data!
    var getImgDataTwo:Data!
    var getImgDataThree:Data!
    
    var getAddress:String!
    var getLat:String!
    var getLong:String!
    var getCountry:String!
    var getFoodTag:String!
    
    var strYesDriverPortalForEdit:String!
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.separatorStyle = UITableViewCell.SeparatorStyle.none
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBarStyle()
        self.pickerViewMwthod()
        
        // print(editDriverProfileIsYesOrNo as Any)
        print(strYesDriverPortalForEdit as Any)
        
        if fromRestaurantPage == "yesRestaurant" {
             if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             // let x : Int = (person["userId"] as! Int)
             // let myString = String(x)
                
                // print(person as Any)
                /*
                 ["ssnImage": , "wallet": 0, "latitude": 28.5870717, "logitude": 77.0605306, "RoutingNo": vstvush6sg6, "AccountHolderName": jjsjs, "role": Restaurant, "fullName": restaurent 2d, "longitude": 77.0605306, "foodTag": Veg, "state": , "email": restaurent@gmail.com, "middleName": , "AutoInsurance": , "accountType": Saving, "companyBackground": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/15907377771590737761302.jpg, "firebaseId": , "AccountNo": 8505858545884555, "country": India, "zipCode": , "BankName": djdgsgs, "socialType": , "contactNumber": 9494645541, "address": Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India, "lastName": , "socialId": , "device": iOS, "image": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/15907377771590737743718.jpg, "drivlingImage": , "gender": , "userId": 99, "dob": , "deviceToken": ]

                 */
                
                let indexPath = IndexPath.init(row: 0, section: 0)
                let cell = self.tbleView.cellForRow(at: indexPath) as! PDpaymentInformationTableCell
                
                cell.txtYourName.text = (person["AccountHolderName"] as! String)
                cell.txtSelectBank.text = (person["BankName"] as! String)
                cell.txtBankAccountNumber.text = (person["AccountNo"] as! String)
                cell.txtRoutingNumber.text = (person["RoutingNo"] as! String)
                cell.txtAccountType.text = (person["accountType"] as! String)
                
                
            }
        } else if fromRestaurantPage == "afterRegistrationOfRestaurant" {
            print("after registration")
        } else {
            
        }
        
        if editDriverProfileIsYesOrNo == "yesFromDriverEdit" {
            print("i am in")
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                if (person["role"] as! String) == "Driver" {
                    let indexPath = IndexPath.init(row: 0, section: 0)
                        let cell = self.tbleView.cellForRow(at: indexPath) as! PDpaymentInformationTableCell
                    
                        cell.txtYourName.text = (person["AccountHolderName"] as! String)
                        cell.txtSelectBank.text = (person["BankName"] as! String)
                        cell.txtBankAccountNumber.text = (person["AccountNo"] as! String)
                        cell.txtRoutingNumber.text = (person["RoutingNo"] as! String)
                        cell.txtAccountType.text = (person["accountType"] as! String)
                }
            }
        }
        
        
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
    }
    
    @objc func pickerViewMwthod() {
        //UIPICKER
        self.pickerView = UIPickerView()
        self.pickerView?.delegate = self //#1
        self.pickerView?.dataSource = self //#1
        
        self.pickerViewSection = UIPickerView()
        self.pickerViewSection?.delegate = self //#2
        self.pickerViewSection?.dataSource = self //#2
               
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDpaymentInformationTableCell
        
        cell.txtAccountType.inputView = self.pickerView
        cell.txtAccountType.inputView = self.pickerViewSection
    }
    
    @objc func navigationBarStyle() {
        self.title = "PAYMENT INFORMATION"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.barTintColor = NAVIGATION_BACKGROUND_COLOR
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.backgroundColor = .orange
    }
    
    //    picker delegates
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDpaymentInformationTableCell
        
        if cell.txtAccountType.isFirstResponder {
            return BankType.count
        } // else
        
        return 0
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDpaymentInformationTableCell
        
        if cell.txtAccountType.isFirstResponder {
            return  BankType[row]
        }
        // else
        
        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDpaymentInformationTableCell
        
        if cell.txtAccountType.isFirstResponder {
            let itemselected = BankType[row]
            cell.txtAccountType.text = itemselected
            
            cell.txtAccountType.isUserInteractionEnabled = true
            
            
        } // else
        
    }
    //
    
    // MARK:- PUSH TO DASHBOARD -
    @objc func pushToDashboardPD() {
        if fromRestaurantPage == "yesRestaurant" {
            self.navigationController?.popViewController(animated: true)
        } else if fromRestaurantPage == "afterRegistrationOfRestaurant" {
            print("after registration")
            
            let indexPath = IndexPath.init(row: 0, section: 0)
            let cell = self.tbleView.cellForRow(at: indexPath) as! PDpaymentInformationTableCell
            
            if cell.txtYourName.text == "" {
                let alert = UIAlertController(title: String("Error!"), message: String("Name Should not be Empty."), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                
                }))
                self.present(alert, animated: true, completion: nil)
            } else if cell.txtSelectBank.text == "" {
                let alert = UIAlertController(title: String("Error!"), message: String("Bank Should not be Empty."), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                
                }))
                self.present(alert, animated: true, completion: nil)
            } else if cell.txtBankAccountNumber.text == "" {
                let alert = UIAlertController(title: String("Error!"), message: String("Account Number Should not be Empty."), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                
                }))
                self.present(alert, animated: true, completion: nil)
            } else if cell.txtBranch.text == "" {
                let alert = UIAlertController(title: String("Error!"), message: String("Branch Should not be Empty."), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                
                }))
                self.present(alert, animated: true, completion: nil)
            } else if cell.txtRoutingNumber.text == "" {
                let alert = UIAlertController(title: String("Error!"), message: String("Routing Number Should not be Empty."), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                
                }))
                self.present(alert, animated: true, completion: nil)
            } else if cell.txtAccountType.text == "" {
                let alert = UIAlertController(title: String("Error!"), message: String("Account Type Should not be Empty."), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                // call webservice
                
                // print(getImgDataOne as Any)
                // print(getImgDataTwo as Any)
                
                self.restaurantRegistrationAfterPaymentWB()
            }
            
        } else {
            // PDPayFiveDollarId
            
            if editDriverProfileIsYesOrNo == "yesFromDriverEdit" {
                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                self.navigationController?.pushViewController(settingsVCId!, animated: true)
            } else {
                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDPayFiveDollarId") as? PDPayFiveDollar
                self.navigationController?.pushViewController(settingsVCId!, animated: true)
            }
            
            
//            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
//            self.navigationController?.pushViewController(settingsVCId!, animated: true)
        }
         
    }
    
    @objc func driverDashbaordIs() {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    @objc func restaurantRegistrationAfterPaymentWB() {
        self.view.endEditing(true)
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Updating...")
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDpaymentInformationTableCell
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
          
            // var parameters:Dictionary<AnyHashable, Any>!
                 let parameters = [
                    "action"          : "editprofile",
                    "userId"              : String(myString),
                    "address"             : String(getAddress),
                    "country"             : String(getCountry),
                    "AccountHolderName"   : String(cell.txtYourName.text!),
                    "BankName"            : String(cell.txtSelectBank.text!),
                    "AccountNo"           : String(cell.txtBankAccountNumber.text!),
                    "RoutingNo"           : String(cell.txtRoutingNumber.text!),
                    
                 ]
                /*
             "userId"              : String(myString),
             "address"             : String(getAddress),
             "country"             : String(getCountry),
             "AccountHolderName"   : String(cell.txtYourName.text!),
             "BankName"            : String(cell.txtSelectBank.text!),
             "AccountNo"           : String(cell.txtBankAccountNumber.text!),
             "RoutingNo"           : String(cell.txtRoutingNumber.text!),
              "accountType"         : String(cell.txtAccountType.text!)
             */
            print(parameters as Any)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.getImgDataOne, withName: "image",fileName: "expressPlusEditProfilePhoto.jpg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        
                        // let paramsData:Data = NSKeyedArchiver.archivedData(withRootObject: value)
                        
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                },
                to:BASE_URL_EXPRESS_PLUS)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):

                        upload.uploadProgress(closure: { (progress) in
                            //print("Upload Progress: \(progress.fractionCompleted)")
                            
                            /*
                            let alertController = UIAlertController(title: "Uploading image", message: "Please wait......", preferredStyle: .alert)

                            let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)

                            progressDownload.setProgress(Float((progress.fractionCompleted)/1.0), animated: true)
                            progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)

                            alertController.view.addSubview(progressDownload)
                            self.present(alertController, animated: true, completion: nil)
                            */
                        })

                        upload.responseJSON { response in
                            //print(response.result.value as Any)
                            if let data = response.result.value {
                                let JSON = data as! NSDictionary
                                print(JSON)
                                // ERProgressHud.sharedInstance.hide()
                                 
                                
                                self.dismiss(animated: true, completion: nil)
                                
                                self.secondWB()
                                // self.navigationController?.popViewController(animated: true)
                            }
                            else {
                                ERProgressHud.sharedInstance.hide()
                                self.secondWB()
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        ERProgressHud.sharedInstance.hide()
                        self.dismiss(animated: true, completion: nil)
                    }}}
        
    }
    
    /*
    // MARK:-  WEBSERVICE (RESTAURANT REGISTRATION AFTER PAYMENT ) -
    @objc func restaurantRegistrationAfterPaymentWB() {
        
        
        let userDefaults = UserDefaults.standard

        // Write/Set Value
        userDefaults.set("menuReg", forKey: "forMenuAfterReg")
        
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            let x : Int = (person["userId"] as! Int)
            let myString = String(x)
            
          let indexPath = IndexPath.init(row: 0, section: 0)
          let cell = self.tbleView.cellForRow(at: indexPath) as! PDpaymentInformationTableCell
            
            let parameters = [
                "action"          : "editprofile",
                "userId"              : String(myString),
                "address"             : String(getAddress),
                "country"             : String(getCountry),
                "AccountHolderName"   : String(cell.txtYourName.text!),
                "BankName"            : String(cell.txtSelectBank.text!),
                "AccountNo"           : String(cell.txtBankAccountNumber.text!),
                "RoutingNo"           : String(cell.txtRoutingNumber.text!),
                 "accountType"         : String(cell.txtAccountType.text!)
            ]
            
            let parametersw = [
                "action"          : "editprofile",
                "userId"              : String(myString),
                "address"             : String(getAddress),
                "country"             : String(getCountry),
                "AccountHolderName"   : String(cell.txtYourName.text!),
                "BankName"            : String(cell.txtSelectBank.text!),
                "AccountNo"           : String(cell.txtBankAccountNumber.text!),
                "RoutingNo"           : String(cell.txtRoutingNumber.text!),
                 "accountType"         : String(cell.txtAccountType.text!),
                  // "Branch"              : String(cell.txtBranch.text!),
                // "latitude"            : String(getLat),
                // "longitude"           : String(getLong),
                // "foodTag"             : String(getFoodTag)
            ]
      
                
            print(parameters as Any)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.getImgDataOne, withName: "image",fileName: "expressPlusRestaurant.jpg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        
                        // let paramsData:Data = NSKeyedArchiver.archivedData(withRootObject: value)
                        
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                },
                to:BASE_URL_EXPRESS_PLUS)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):

                        upload.uploadProgress(closure: { (progress) in
                            //print("Upload Progress: \(progress.fractionCompleted)")
                            /*
                            let alertController = UIAlertController(title: "Uploading image", message: "Please wait......", preferredStyle: .alert)

                            let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)

                            progressDownload.setProgress(Float((progress.fractionCompleted)/1.0), animated: true)
                            progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)

                            alertController.view.addSubview(progressDownload)
                            self.present(alertController, animated: true, completion: nil)
                             */
                        })

                        upload.responseJSON { response in
                            //print(response.result.value as Any)
                            if let data = response.result.value {
                                let JSON = data as! NSDictionary
                                print(JSON)
                                // ERProgressHud.sharedInstance.hide()
                                self.dismiss(animated: true, completion: nil)
                                
                                self.secondWB()
                                // self.strPassImgString = "0"
                                
                                // var dict: Dictionary<AnyHashable, Any>
                                // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                // let defaults = UserDefaults.standard
                                // defaults.setValue(dict, forKey: "keyLoginFullData")
                                
                                // self.navigationController?.popViewController(animated: true)
                            }
                            else {
                                ERProgressHud.sharedInstance.hide()
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        ERProgressHud.sharedInstance.hide()
                        self.dismiss(animated: true, completion: nil)
                    }}}
        
        
        
    }
    */
    @objc func secondWB() {
        
        // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
          let indexPath = IndexPath.init(row: 0, section: 0)
          let cell = self.tbleView.cellForRow(at: indexPath) as! PDpaymentInformationTableCell
            
            let parameters = [
                "action"          : "editprofile",
                "userId"              : String(myString),
                   "Branch"              : String(cell.txtBranch.text!),
                 "latitude"            : String(getLat),
                 "longitude"           : String(getLong),
                 "foodTag"             : String(getFoodTag)
            ]
            
            /*
            // var parameters:Dictionary<AnyHashable, Any>!
                 let parameters = [ "action"              : "editprofile",
                     "userId"              : String(myString),
                     "address"             : String(getAddress),
                     "country"             : String(getCountry),
                     "AccountHolderName"   : String(cell.txtBankAccountNumber.text!),
                     "BankName"            : String(cell.txtSelectBank.text!),
                     "AccountNo"           : String(cell.txtBankAccountNumber.text!),
                     "RoutingNo"           : String(cell.txtRoutingNumber.text!),
                     "accountType"         : String(cell.txtAccountType.text!),
                     "Branch"              : String(cell.txtBranch.text!),
                     "latitude"            : String(getLat),
                     "longitude"           : String(getLong),
                     "foodTag"             : String(getFoodTag)
                 ]
            */
                
            print(parameters as Any)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.getImgDataTwo, withName: "drivlingImage",fileName: "expressPlusRestaurantLogo.jpg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        
                        // let paramsData:Data = NSKeyedArchiver.archivedData(withRootObject: value)
                        
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                },
                to:BASE_URL_EXPRESS_PLUS)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):

                        upload.uploadProgress(closure: { (progress) in
                            //print("Upload Progress: \(progress.fractionCompleted)")
                            
                            /*
                            let alertController = UIAlertController(title: "Uploading image", message: "Please wait......", preferredStyle: .alert)

                            let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)

                            progressDownload.setProgress(Float((progress.fractionCompleted)/1.0), animated: true)
                            progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)

                            alertController.view.addSubview(progressDownload)
                            self.present(alertController, animated: true, completion: nil)
                            */
                        })

                        upload.responseJSON { response in
                            //print(response.result.value as Any)
                            if let data = response.result.value {
                                let JSON = data as! NSDictionary
                                print(JSON)
                                 ERProgressHud.sharedInstance.hide()
                                self.dismiss(animated: true, completion: nil)
                                
                                // self.strPassImgString = "0"
                                
                                var dict: Dictionary<AnyHashable, Any>
                                dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue(dict, forKey: "keyLoginFullData")
                                  
                                if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                                 print(person as Any)
                                /*
                                 ["fullName": ios2020, "AccountHolderName": , "state": , "email": ios2020@gmail.com, "socialType": , "middleName": , "longitude": , "companyBackground": , "BankName": , "lastName": , "latitude": , "AccountNo": , "socialId": , "accountType": , "gender": , "ssnImage": , "role": Member, "country": , "address": Wadala West Mumbai, "deviceToken": , "firebaseId": , "contactNumber": 8287632340, "drivlingImage": , "device": iOS, "dob": , "logitude": , "AutoInsurance": , "userId": 127, "wallet": 0, "image": , "zipCode": , "RoutingNo": , "foodTag": ]
                                 */
                                
                                if person["role"] as! String == "Driver" {
                                    
                                    if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                                     print(person as Any)
                                    /*
                                     ["fullName": ios2020, "AccountHolderName": , "state": , "email": ios2020@gmail.com, "socialType": , "middleName": , "longitude": , "companyBackground": , "BankName": , "lastName": , "latitude": , "AccountNo": , "socialId": , "accountType": , "gender": , "ssnImage": , "role": Member, "country": , "address": Wadala West Mumbai, "deviceToken": , "firebaseId": , "contactNumber": 8287632340, "drivlingImage": , "device": iOS, "dob": , "logitude": , "AutoInsurance": , "userId": 127, "wallet": 0, "image": , "zipCode": , "RoutingNo": , "foodTag": ]
                                     */
                                    
                                        if self.editDriverProfileIsYesOrNo == "yesFromDriverEdit" {
                                            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                                            self.navigationController?.pushViewController(settingsVCId!, animated: true)
                                            
                                        } else {
                                            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDStateCountryCityId") as? PDStateCountryCity
                                            self.navigationController?.pushViewController(push!, animated: false)
                                        }
                                    
                                    
                                    }
                                    
                                    
                                    
                                    
                                    
                                } else if person["role"] as! String == "Restaurant" {
                                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RHomeId") as? RHome
                                    self.navigationController?.pushViewController(push!, animated: false)
                                } else {
                                    
                                    }
                                }
                                
                                
                                
                                // self.navigationController?.popViewController(animated: true)
                            }
                            else {
                                ERProgressHud.sharedInstance.hide()
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        ERProgressHud.sharedInstance.hide()
                        self.dismiss(animated: true, completion: nil)
                    }}}
        
    }
    
    @objc func thirdTimeEditForOnlyDriver() {
        
        
        
        
        
        
        
        // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Updating...")
        
        /*if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
          // let indexPath = IndexPath.init(row: 0, section: 0)
          // let cell = self.tbleView.cellForRow(at: indexPath) as! PDpaymentInformationTableCell
            
            let parameters = [
                "action"        : "editprofile",
                "userId"        : String(myString)
                   
            ]
       
            print(parameters as Any)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.getImgDataThree, withName: "ssnImage",fileName: "expressPlusDriver.jpg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        
                        // let paramsData:Data = NSKeyedArchiver.archivedData(withRootObject: value)
                        
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                },
                to:BASE_URL_EXPRESS_PLUS)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):

                        upload.uploadProgress(closure: { (progress) in
                            //print("Upload Progress: \(progress.fractionCompleted)")
                            
                            /*
                            let alertController = UIAlertController(title: "Uploading image", message: "Please wait......", preferredStyle: .alert)

                            let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)

                            progressDownload.setProgress(Float((progress.fractionCompleted)/1.0), animated: true)
                            progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)

                            alertController.view.addSubview(progressDownload)
                            self.present(alertController, animated: true, completion: nil)
                            
                            */
                        })

                        upload.responseJSON { response in
                            //print(response.result.value as Any)
                            if let data = response.result.value {
                                let JSON = data as! NSDictionary
                                print(JSON)
                                ERProgressHud.sharedInstance.hide()
                                self.dismiss(animated: true, completion: nil)
                                
                                // self.strPassImgString = "0"
                                
                                var dict: Dictionary<AnyHashable, Any>
                                dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue(dict, forKey: "keyLoginFullData")
                                  
                                if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                                 print(person as Any)
                                /*
                                 ["fullName": ios2020, "AccountHolderName": , "state": , "email": ios2020@gmail.com, "socialType": , "middleName": , "longitude": , "companyBackground": , "BankName": , "lastName": , "latitude": , "AccountNo": , "socialId": , "accountType": , "gender": , "ssnImage": , "role": Member, "country": , "address": Wadala West Mumbai, "deviceToken": , "firebaseId": , "contactNumber": 8287632340, "drivlingImage": , "device": iOS, "dob": , "logitude": , "AutoInsurance": , "userId": 127, "wallet": 0, "image": , "zipCode": , "RoutingNo": , "foodTag": ]
                                 */
                                
                                    if self.editDriverProfileIsYesOrNo == "yesFromDriverEdit" {
                                        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                                        self.navigationController?.pushViewController(settingsVCId!, animated: true)
                                        
                                    } else {
                                        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDStateCountryCityId") as? PDStateCountryCity
                                        self.navigationController?.pushViewController(push!, animated: false)
                                    }
                                
                                
                                }
                                
                                
                                
                                
                                
                                // self.navigationController?.popViewController(animated: true)
                            }
                            else {
                                ERProgressHud.sharedInstance.hide()
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        ERProgressHud.sharedInstance.hide()
                        self.dismiss(animated: true, completion: nil)
                    }}}*/
        
    }
}


extension PDPaymentInformation: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PDpaymentInformationTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! PDpaymentInformationTableCell
        
        cell.backgroundColor = .clear
       
//        if editDriverProfileIsYesOrNo == "yesFromDriverEdit" {
//            cell.btnSaveandContinue.addTarget(self, action: #selector(driverDashbaordIs), for: .touchUpInside)
//        } else {
            cell.btnSaveandContinue.addTarget(self, action: #selector(pushToDashboardPD), for: .touchUpInside)
//        }
        
        
        return cell
    }

func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView .deselectRow(at: indexPath, animated: true)
}
    
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 900
    
}
    
}


extension PDPaymentInformation: UITableViewDelegate
{
    
}
