//
//  PDpaymentInformationTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class PDpaymentInformationTableCell: UITableViewCell {

    @IBOutlet weak var txtYourName:UITextField! {
        didSet {
            txtYourName.layer.cornerRadius = 6
            txtYourName.clipsToBounds = true
            txtYourName.backgroundColor = .white
            txtYourName.layer.borderColor = UIColor.lightGray.cgColor
            txtYourName.layer.borderWidth = 0.8
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            txtYourName.leftView = paddingView
            txtYourName.leftViewMode = .always
            
            txtYourName.attributedPlaceholder = NSAttributedString(string: "Your name",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
    }
    
    @IBOutlet weak var txtSelectBank:UITextField! {
        didSet {
            txtSelectBank.layer.cornerRadius = 6
            txtSelectBank.clipsToBounds = true
            txtSelectBank.backgroundColor = .white
            txtSelectBank.layer.borderColor = UIColor.lightGray.cgColor
            txtSelectBank.layer.borderWidth = 0.8
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            txtSelectBank.leftView = paddingView
            txtSelectBank.leftViewMode = .always
            
            txtSelectBank.attributedPlaceholder = NSAttributedString(string: "Select Bank",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
    }
    
    @IBOutlet weak var txtBankAccountNumber:UITextField! {
        didSet {
            txtBankAccountNumber.layer.cornerRadius = 6
            txtBankAccountNumber.clipsToBounds = true
            txtBankAccountNumber.backgroundColor = .white
            txtBankAccountNumber.layer.borderColor = UIColor.lightGray.cgColor
            txtBankAccountNumber.layer.borderWidth = 0.8
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            txtBankAccountNumber.leftView = paddingView
            txtBankAccountNumber.leftViewMode = .always
            
            txtBankAccountNumber.attributedPlaceholder = NSAttributedString(string: "Bank account number",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
    }
    
    @IBOutlet weak var txtBranch:UITextField! {
        didSet {
            txtBranch.layer.cornerRadius = 6
            txtBranch.clipsToBounds = true
            txtBranch.backgroundColor = .white
            txtBranch.layer.borderColor = UIColor.lightGray.cgColor
            txtBranch.layer.borderWidth = 0.8
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            txtBranch.leftView = paddingView
            txtBranch.leftViewMode = .always
            
            txtBranch.attributedPlaceholder = NSAttributedString(string: "Branch",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
    }
    
    @IBOutlet weak var txtRoutingNumber:UITextField! {
        didSet {
            txtRoutingNumber.layer.cornerRadius = 6
            txtRoutingNumber.clipsToBounds = true
            txtRoutingNumber.backgroundColor = .white
            txtRoutingNumber.layer.borderColor = UIColor.lightGray.cgColor
            txtRoutingNumber.layer.borderWidth = 0.8
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            txtRoutingNumber.leftView = paddingView
            txtRoutingNumber.leftViewMode = .always
            
            txtRoutingNumber.attributedPlaceholder = NSAttributedString(string: "Routing Number",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
    }
    
    @IBOutlet weak var txtAccountType:UITextField! {
        didSet {
            txtAccountType.layer.cornerRadius = 6
            txtAccountType.clipsToBounds = true
            txtAccountType.backgroundColor = .white
            txtAccountType.layer.borderColor = UIColor.lightGray.cgColor
            txtAccountType.layer.borderWidth = 0.8
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            txtAccountType.leftView = paddingView
            txtAccountType.leftViewMode = .always
            
            txtAccountType.attributedPlaceholder = NSAttributedString(string: "Account Type",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
    }
    
    @IBOutlet weak var btnSaveandContinue:UIButton! {
        didSet {
            btnSaveandContinue.setTitleColor(.white, for: .normal)
            btnSaveandContinue.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnSaveandContinue.layer.cornerRadius = 4
            btnSaveandContinue.clipsToBounds = true
            btnSaveandContinue.setTitle("Save and Continue", for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
