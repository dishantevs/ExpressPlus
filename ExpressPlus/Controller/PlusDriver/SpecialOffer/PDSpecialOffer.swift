//
//  PDSpecialOffer.swift
//  ExpressPlus
//
//  Created by Apple on 02/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire
class PDSpecialOffer: UIViewController {

    var dictGetDetailsForSpecialOffer:NSDictionary!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "Special Offer"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var imgUserImage:UIImageView!
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblProductName:UILabel!
    @IBOutlet weak var lblEstimatedPrice:UILabel!
    @IBOutlet weak var lblTipAmount:UILabel!
    @IBOutlet weak var lblDeliveryFee:UILabel!
    @IBOutlet weak var lblStateTax:UILabel!
    
    @IBOutlet weak var btnTotalPrice:UIButton! {
        didSet {
            btnTotalPrice.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnTotalPrice.setTitleColor(.white, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        print(dictGetDetailsForSpecialOffer as Any)
        
        /*
        Optional({
             TIP = 5;
             cardNo = "08/25";
             couponCode = "";
             created = "2020-06-01 13:54:00";
             discount = 0;
             driverName = Drix;
             foodDetails =     (
             );
             foodId = "";
             foodorderId = 97;
             noContact = "";
             resturentAddress = "";
             resturentId = "";
             resturentImage = "";
             resturentName = "";
             specialNote = "";
             status = 4;
             totalAmount = 42;
             userAddress = "";
             userId = 124;
             userImage = "";
             userName = customers;
             whatYouWant = "test new food order";
             workPlace = Home;
         })
         (lldb)
         */
        
        
         self.imgUserImage.sd_setImage(with: URL(string: (dictGetDetailsForSpecialOffer!["resturentImage"] as! String)), placeholderImage: UIImage(named: "user"))
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            self.lblAddress.text = (person["address"] as! String)
        }
        self.lblTitle.text = (dictGetDetailsForSpecialOffer!["userName"] as! String)
        
        self.lblEstimatedPrice.text = "$ "+(dictGetDetailsForSpecialOffer!["totalAmount"] as! String)
        self.lblTipAmount.text = "$ "+(dictGetDetailsForSpecialOffer!["TIP"] as! String)
        self.lblProductName.text = (dictGetDetailsForSpecialOffer!["whatYouWant"] as! String)
        self.lblDeliveryFee.text = "$ "+"6"
        // self.lblStateTax.text = "$ "+"0"
        
        
        
        if dictGetDetailsForSpecialOffer["salesTax"] is String {
                          
            print("Yes, it's a String")
          
            self.lblStateTax.text = "$ "+(dictGetDetailsForSpecialOffer["salesTax"] as! String)

        } else if dictGetDetailsForSpecialOffer["salesTax"] is Int {
          
            print("It is Integer")
          
            let x2 : Int = (dictGetDetailsForSpecialOffer["salesTax"] as! Int)
            let myString2 = String(x2)
            self.lblStateTax.text = "$ "+myString2
          
        } else {
        //some other check
          print("i am ")
          
            let temp:NSNumber = dictGetDetailsForSpecialOffer["salesTax"] as! NSNumber
            let tempString = temp.stringValue
            self.lblStateTax.text = "$ "+tempString
          
        }
        
        
        
        
        
        let myString1 = (dictGetDetailsForSpecialOffer["totalAmount"] as! String)
        let myInt1 = Int(myString1)
        
        let myString2 = (dictGetDetailsForSpecialOffer["TIP"] as! String)
        let myInt2 = Int(myString2)
        
        let addPrice = myInt1
        let addTip = myInt2
        // let addDeliveryFee = (dictGetDetailsForSpecialOffer["6"] as! Int)
        
        var ans2 = Int();
        ans2 = addPrice!+addTip!+6;
        
        let x2 : Int = ans2
        let myString3 = String(x2)
   
        let x : Int = dictGetDetailsForSpecialOffer["status"] as! Int
        let myString = String(x)
        if myString == "4" {
            self.btnTotalPrice.setTitle("Delivered $ "+String(myString3), for: .normal)
            self.btnTotalPrice.isUserInteractionEnabled = false
        } else {
            self.btnTotalPrice.setTitle("Mark as Delivered $ "+String(myString3), for: .normal)
            self.btnTotalPrice.isUserInteractionEnabled = true
            self.btnTotalPrice.addTarget(self, action: #selector(markAsDelivered), for: .touchUpInside)
        }
    }
     
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func markAsDelivered() {
     
          ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
         let urlString = BASE_URL_EXPRESS_PLUS
        
         /*
          [action] => updatefoodorder
          [foodorderId] => 97
          [driverId] => 123
          [status] => 4
          */
         // foodorderId
        
        let x : Int = dictGetDetailsForSpecialOffer["foodorderId"] as! Int
        let myString = String(x)
        
            var parameters:Dictionary<AnyHashable, Any>!
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                // let str:String = person["role"] as! String
                       parameters = [
                        "action"        : "updatefoodorder",
                        "driverId"      : person["userId"] as Any,
                        "foodorderId"   : String(myString),
                        "status"        : String("4")
                ]
         }
                       print("parameters-------\(String(describing: parameters))")
                       
                       Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                           {
                               response in
                   
                               switch(response.result) {
                               case .success(_):
                                  if let data = response.result.value {

                                   let JSON = data as! NSDictionary
                                      print(JSON as Any)
                                   
                                   var strSuccess : String!
                                   strSuccess = JSON["status"]as Any as? String
                                   
                                     // var strSuccessAlert : String!
                                     // strSuccessAlert = JSON["msg"]as Any as? String
                                   
                                   if strSuccess == String("success") {
                                    print("yes")
                                   var strSuccess2 : String!
                                   strSuccess2 = JSON["msg"]as Any as? String
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                   alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                       // self.dismiss(animated: true, completion: nil)
                                    self.navigationController?.popViewController(animated: true)
                                       
                                   }))
                                   self.present(alert, animated: true, completion: nil)
                                   }
                                   else {
                                    print("no")
                                     ERProgressHud.sharedInstance.hide()
                                    
                                    
                                   }
                               }

                               case .failure(_):
                                   print("Error message:\(String(describing: response.result.error))")
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   
                                   let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                   
                                   let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                           UIAlertAction in
                                           NSLog("OK Pressed")
                                       }
                                   
                                   alertController.addAction(okAction)
                                   
                                   self.present(alertController, animated: true, completion: nil)
                                   
                                   break
                                }
                           }
        
           }
        
    }

