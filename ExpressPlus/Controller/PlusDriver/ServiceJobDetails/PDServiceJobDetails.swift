//
//  PDServiceJobDetails.swift
//  ExpressPlus
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class PDServiceJobDetails: UIViewController {
    
    let cellReuseIdentifier = "pDServiceJobDetailsTableCell"
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "SERVICE JOB DETAILS"
        }
    }
    @IBOutlet weak var BelowNavigationBar:UIView! {
        didSet {
            BelowNavigationBar.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var BelowBelowNavigationBar:UIView! {
        didSet {
            BelowBelowNavigationBar.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var imgProfileImage:UIImageView! {
        didSet {
            imgProfileImage.layer.cornerRadius = 35
            imgProfileImage.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var upperMapNavigationBar:UIView! {
        didSet {
            upperMapNavigationBar.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var addressView:UIView! {
        didSet {
            addressView.backgroundColor = .white // NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.separatorStyle = UITableViewCell.SeparatorStyle.none
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var lblUsername:UILabel!
    @IBOutlet weak var lblUsernamePhoneNumber:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblCreatedAt:UILabel!
    
    @IBOutlet weak var lblServiceTitle:UILabel!
    @IBOutlet weak var lblServiceAmount:UILabel!
    @IBOutlet weak var lblTotalPrice:UILabel!
    @IBOutlet weak var lblTipAmount:UILabel!
    
    @IBOutlet weak var lblSalesTax:UILabel!
    
    var dictGetAllServiceDetails:NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
         print(dictGetAllServiceDetails as Any)
        /*
         AVGRating = "";
         TIP = 10;
         address = "8, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
         bookingDate = "26th May";
         bookingId = 230;
         contactNumber = 2352639000;
         created = "2020-05-25 11:36:00";
         email = "tt@gmail.com";
         fullName = purnima;
         groupId = 46;
         image = "";
         serviceAmount = 100;
         serviceName = "Yard Work";
         servicePrice = 100;
         slote = "9:0-6:0";
         status = 2;
         totalAmount = 110;
         userAddress = "";
         userId = 117;
         zipCode = "";
         */
        
        /*
         @IBOutlet weak var lblServiceTitle:UILabel!
         @IBOutlet weak var lblTotalPrice:UILabel!
         @IBOutlet weak var lblTipAmount:UILabel!
         */
        
        /*
         
         */
        self.lblUsername.text               = (dictGetAllServiceDetails["fullName"] as! String)
        self.lblUsernamePhoneNumber.text    = (dictGetAllServiceDetails["contactNumber"] as! String)
        self.lblAddress.textColor           = .black
        self.lblAddress.text                = (dictGetAllServiceDetails["address"] as! String)
        self.lblCreatedAt.text              = (dictGetAllServiceDetails["created"] as! String)
        
        self.lblServiceTitle.text       = (dictGetAllServiceDetails["serviceName"] as! String)
        self.lblServiceAmount.text      = "$ "+(dictGetAllServiceDetails["serviceAmount"] as! String)
        self.lblTipAmount.text          = "$ "+(dictGetAllServiceDetails["TIP"] as! String)
       
        self.lblTotalPrice.text = "$ "+(dictGetAllServiceDetails["totalAmount"] as! String)
        self.lblTotalPrice.textColor = .white
        
        self.foodOrderListWB()
    }
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK:- WEBSERVICE ( SERVICE JOB REQUEST ) -
    @objc func foodOrderListWB() {
        
      ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => bookingdetail
      [bookingId] => 233
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
                   parameters = [
                       "action"              : "bookingdetail",
                       "bookingId"              : dictGetAllServiceDetails["bookingId"] as Any
             ]
     // }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                  print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                ERProgressHud.sharedInstance.hide()
                                
                                var dict: Dictionary<AnyHashable, Any>
                                dict = JSON["historyList"] as! Dictionary<AnyHashable, Any>
                                
                                self.lblSalesTax.text = ((dict["salesTax"] as Any) as! String)
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    
}


extension PDServiceJobDetails: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PDServiceJobDetailsTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! PDServiceJobDetailsTableCell
        
        cell.backgroundColor = .clear
        
        return cell
    }

func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView .deselectRow(at: indexPath, animated: true)
    
    if indexPath.row == 1 {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDInProgressId") as? PDInProgress
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    } else {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDServiceScreenId") as? PDServiceScreen
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
}
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
}


extension PDServiceJobDetails: UITableViewDelegate {
    
}
