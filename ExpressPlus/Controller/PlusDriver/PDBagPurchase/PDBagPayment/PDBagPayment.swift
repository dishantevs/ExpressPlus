//
//  PDBagPayment.swift
//  ExpressPlus
//
//  Created by Apple on 25/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import  Stripe

class PDBagPayment: UIViewController, UITextFieldDelegate {

    let cellReuseIdentifier = "pDBagPaymentTableCell"
    
    var strWhatCardIam:String!
    
    var strGetName:String!
    var strGetPhoneNumber:String!
    var strGetAddress:String!
    var strGetZipcode:String!
    var strGetState:String!
    var strGetCountry:String!
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "PAYMENT"
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            // btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
    }
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
        }
    }
    
    @IBOutlet weak var lblCardNumberHeading:UILabel!
    @IBOutlet weak var lblEXPDate:UILabel!
    
    @IBOutlet weak var lblPayableAmount:UILabel!
    
    @IBOutlet weak var btnmakePayment:UIButton! {
        didSet {
            btnmakePayment.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnmakePayment.setTitle("MAKE PAYMENT", for: .normal)
            btnmakePayment.setTitleColor(.white, for: .normal)
        }
    }
    @IBOutlet weak var viewCard:UIView! {
        didSet {
            viewCard.backgroundColor = NAVIGATION_BACKGROUND_COLOR // UIColor.init(red: 34.0/255.0, green: 72.0/255.0, blue: 104.0/255.0, alpha: 1)
            viewCard.layer.cornerRadius = 6
            viewCard.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var imgCardImage:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(PDBagPayment.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        self.btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        
        self.btnmakePayment.addTarget(self, action: #selector(firstCheckValidation), for: .touchUpInside)
        
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func sideBarMenuClick() {
         self.navigationController?.popViewController(animated: true)
    }
    
    @objc func firstCheckValidation() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDBagPaymentTableCell
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        if cell.txtCardNumber.text == "" {
            let alert = UIAlertController(title: "Card Number", message: "Card number should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtExpDate.text == "" {
            let alert = UIAlertController(title: "Exp Month", message: "Expiry Month should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtCVV.text == "" {
            let alert = UIAlertController(title: "Security Code", message: "Security Code should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else {
                let cardParams = STPCardParams()
                  
                  let fullNameArr = cell.txtExpDate.text!.components(separatedBy: "/")

                  // print(fullNameArr as Any)
                  let name    = fullNameArr[0]
                  let surname = fullNameArr[1]
                  
                  cardParams.number       = String(cell.txtCardNumber.text!)
                  cardParams.expMonth     = UInt(name)!
                  cardParams.expYear      = UInt(surname)!
                  cardParams.cvc          = String(cell.txtCVV.text!)
                  
            STPAPIClient.shared.createToken(withCard: cardParams) { token, error in
                      guard let token = token else {
                          // Handle the error
                          // print(error as Any)
                          // print(error?.localizedDescription as Any)
                          ERProgressHud.sharedInstance.hide()
                          
                          let alert = UIAlertController(title: "Error", message: String(error!.localizedDescription), preferredStyle: UIAlertController.Style.alert)
                          alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                              
                          }))
                          
                          self.present(alert, animated: true, completion: nil)
                          
                          
                          
                          return
                      }
                      
                      let tokenID = token.tokenId
                      print(tokenID)
                      
                      self.paymentForBag(stripeTokenId: tokenID)
                  }
        }
    }
    
    @objc func paymentForBag(stripeTokenId:String) {
    
      let urlString = BASE_URL_EXPRESS_PLUS
    
        /*
         action: purcheseupdate
         userId:
         transactionId:
         shippingAddress:
         shippingCity:
         shippingState:
         shippingCountry:
         shippingZipcode:
         */
        
        /*
         var strGetName:String!
         var strGetPhoneNumber:String!
         var strGetAddress:String!
         var strGetZipcode:String!
         var strGetState:String!
         var strGetCountry:String!
         */
        
         var parameters:Dictionary<AnyHashable, Any>!
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             // let str:String = person["role"] as! String
            
            let x : Int = person["userId"] as! Int
                      let myString = String(x)
            
                    parameters = [
                        "action"            : "purcheseupdate",
                         "userId"           : String(myString),
                         "transactionId"    : String(stripeTokenId),
                         "shippingAddress"  : String(strGetAddress),
                         "shippingCity"     : String("city"),
                         "shippingState"    : String(strGetState),
                         "shippingCountry"  : String(strGetCountry),
                         "shippingZipcode"  : String(strGetZipcode),
                         "shippingName"     : String(strGetName)
              ]
      }
                    print("parameters-------\(String(describing: parameters))")
                    
                    Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                        {
                            response in
                
                            switch(response.result) {
                            case .success(_):
                               if let data = response.result.value {

                                let JSON = data as! NSDictionary
                                print(JSON as Any)
                                
                                var strSuccess : String!
                                strSuccess = JSON["status"]as Any as? String
                                
                                  // var strSuccessAlert : String!
                                  // strSuccessAlert = JSON["msg"]as Any as? String
                                
                                if strSuccess == String("success") {
                                    print("yes")
                                    var strSuccess2 : String!
                                    strSuccess2 = JSON["msg"]as Any as? String
                                  
                                    ERProgressHud.sharedInstance.hide()
                                    
                                  let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                  alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                      // self.dismiss(animated: true, completion: nil)
                                    
                                       let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                                       
                                       self.navigationController?.pushViewController(push!, animated: true)
                                      
                                  }))
                                  self.present(alert, animated: true, completion: nil)
                                    
                                 // self.callAfterFullAnFinalPayment(transactionIdIs: JSON["transactionID"] as! String)
                                 
                                }
                                else {
                                 print("no")
                                  ERProgressHud.sharedInstance.hide()
                                 
                                 
                                }
                            }

                            case .failure(_):
                                print("Error message:\(String(describing: response.result.error))")
                                
                                ERProgressHud.sharedInstance.hide()
                                
                                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                
                                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                        UIAlertAction in
                                        NSLog("OK Pressed")
                                    }
                                
                                alertController.addAction(okAction)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                break
                             }
                        }
        }
}


//MARK:- TABLE VIEW -
extension PDBagPayment: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PDBagPaymentTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! PDBagPaymentTableCell
        //
        cell.backgroundColor = .white
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.txtCardNumber.delegate = self
        cell.txtExpDate.delegate = self
        cell.txtCVV.delegate = self
        
        cell.txtCardNumber.addTarget(self, action: #selector(PDBagPayment.textFieldDidChange(_:)), for: .editingChanged)
        cell.txtExpDate.addTarget(self, action: #selector(PDBagPayment.textFieldDidChange2(_:)), for: .editingChanged)
        
        return cell
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDBagPaymentTableCell
            
         self.lblCardNumberHeading.text! = cell.txtCardNumber.text!
    }
    
    @objc func textFieldDidChange2(_ textField: UITextField) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDBagPaymentTableCell
            
         self.lblEXPDate.text! = cell.txtExpDate.text!
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDBagPaymentTableCell
        
        if textField == cell.txtCardNumber {
            
            let first2 = String(self.lblCardNumberHeading.text!.prefix(2))
            
            if first2.count == 2 {
                // print("yes")
                
                let first3 = String(self.lblCardNumberHeading.text!.prefix(2))
                // print(first3 as Any)
                
                if first3 == "34" { // amex
                    self.imgCardImage.image = UIImage(named: "amex")
                    self.strWhatCardIam = "amex"
                } else if first3 == "37" { // amex
                    self.imgCardImage.image = UIImage(named: "amex")
                    self.strWhatCardIam = "amex"
                } else if first3 == "51" { // master
                    self.imgCardImage.image = UIImage(named: "mastercard")
                    self.strWhatCardIam = "master"
                } else if first3 == "55" { // master
                    self.imgCardImage.image = UIImage(named: "mastercard")
                    self.strWhatCardIam = "master"
                }  else if first3 == "42" { // visa
                    self.imgCardImage.image = UIImage(named: "visa")
                    self.strWhatCardIam = "visa"
                } else if first3 == "65" { // discover
                    self.imgCardImage.image = UIImage(named: "discover")
                    self.strWhatCardIam = "discover"
                } else {
                    self.imgCardImage.image = UIImage(named: "ccCard")
                    self.strWhatCardIam = "none"
                }
                
            } else {
                // print("no")
                self.imgCardImage.image = UIImage(named: "ccCard")
            }
            
            /*
            if self.lblCardNumberHeading.text!.count+1 == 2 {
                // print("check card")
                
            } else {
                // print("do not check card")
            }
            */
            
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            // print(self.strWhatCardIam as Any)
            if self.strWhatCardIam == "amex" {
                return count <= 15
            } else {
                return count <= 16
            }
            
            
        }
        
        if textField == cell.txtExpDate {
            if string == "" {
                return true
            }

            
            let currentText = textField.text! as NSString
            let updatedText = currentText.replacingCharacters(in: range, with: string)

            textField.text = updatedText
            let numberOfCharacters = updatedText.count
            
            if numberOfCharacters == 2 {
                textField.text?.append("/")
            }
            self.lblEXPDate.text! = cell.txtExpDate.text!
        }
        
       if textField == cell.txtCVV {
           
           guard let textFieldText = textField.text,
               let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                   return false
           }
           let substringToReplace = textFieldText[rangeOfTextToReplace]
           let count = textFieldText.count - substringToReplace.count + string.count
        
        if self.strWhatCardIam == "amex" {
            return count <= 4
        } else {
            return count <= 3
        }
           
       }
        
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
    
}

extension PDBagPayment: UITableViewDelegate {
    
}


