//
//  ForgotPassword.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class ForgotPassword: UIViewController {

    @IBOutlet weak var imglogo:UIImageView! {
        didSet {
            imglogo.layer.cornerRadius = 60
            imglogo.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "FORGOT PASSWORD"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var txtNewPassword:UITextField! {
        didSet {
            txtNewPassword.backgroundColor = .white
            txtNewPassword.layer.borderColor = UIColor.lightGray.cgColor
            txtNewPassword.layer.borderWidth = 0.8
            txtNewPassword.layer.cornerRadius = 6
            txtNewPassword.clipsToBounds = true
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 20))
            txtNewPassword.leftView = paddingView
            txtNewPassword.leftViewMode = .always
            
            txtNewPassword.attributedPlaceholder = NSAttributedString(string: "Enter Email Address",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            
            txtNewPassword.keyboardType = .emailAddress
        }
    }
    
    @IBOutlet weak var btnUpdatePassword:UIButton! {
        didSet {
            btnUpdatePassword.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnUpdatePassword.layer.cornerRadius = 6
            btnUpdatePassword.clipsToBounds = true
            btnUpdatePassword.setTitleColor(.white, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        self.btnUpdatePassword.addTarget(self, action: #selector(forgotSubmitClickMethod), for: .touchUpInside)
        // self.sideBarMenuClick()
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    // MARK:- WEBSERVICE ( FORGOT PASSWORD ) -
    @objc func forgotSubmitClickMethod() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

        var parameters:Dictionary<AnyHashable, Any>!
                    
        parameters = ["action"      : "forgotpassword",
                      "email"       : String(self.txtNewPassword.text!)
        ]
        
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String // match your success status here
                           
                    var strMessage : String!
                    strMessage = JSON["msg"]as Any as? String
                    
                    if strSuccess == String("success") {
                        ERProgressHud.sharedInstance.hide()
                        
                        let alert = UIAlertController(title: String(strSuccess), message: String(strMessage), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                             self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                        
                        let alert = UIAlertController(title: String(strSuccess), message: String(strMessage), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                             
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
    }
}
