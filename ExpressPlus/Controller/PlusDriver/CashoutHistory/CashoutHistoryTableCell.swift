//
//  CashoutHistoryTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CashoutHistoryTableCell: UITableViewCell {

    @IBOutlet weak var lblPaid:UILabel! {
        didSet {
            lblPaid.backgroundColor = .systemGreen
            lblPaid.layer.cornerRadius = 12
            lblPaid.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
