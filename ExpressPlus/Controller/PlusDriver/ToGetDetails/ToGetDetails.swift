//
//  ToGetDetails.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ToGetDetails: UIViewController {

    let cellReuseIdentifier = "toGetDetailsTableCell"
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "TO GET DETAILS"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnMarkAsDelivery:UIButton! {
        didSet {
            btnMarkAsDelivery.setTitle("MARK AS DELIVERED", for: .normal)
            btnMarkAsDelivery.setTitleColor(.white, for: .normal)
            btnMarkAsDelivery.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    @IBOutlet weak var btnRequestMoney:UIButton! {
        didSet {
            btnRequestMoney.setTitle("REQUEST MONEY", for: .normal)
            btnRequestMoney.setTitleColor(.white, for: .normal)
            btnRequestMoney.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var btnReminder:UIButton! {
        didSet {
            btnReminder.setTitle("Reminder : No contact on the delivery", for: .normal)
            btnReminder.setTitleColor(.white, for: .normal)
            btnReminder.backgroundColor = UIColor.init(red: 223.0/255.0, green: 54.0/255.0, blue: 100.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var btnTotalAmount:UIButton! {
        didSet {
            btnTotalAmount.layer.cornerRadius = 4
            btnTotalAmount.clipsToBounds = true
            btnTotalAmount.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
            btnTotalAmount.setTitle("Total Amount : $ 250", for: .normal)
            btnTotalAmount.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var BelowNavigationBar:UIView! {
        didSet {
            BelowNavigationBar.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var imgProfileImage:UIImageView! {
        didSet {
            imgProfileImage.layer.cornerRadius = 35
            imgProfileImage.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.separatorStyle = UITableViewCell.SeparatorStyle.none
        }
    }
    
    @IBOutlet weak var lblUsername:UILabel!
    @IBOutlet weak var lblUsernamePhoneNumber:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        btnTotalAmount.addTarget(self, action: #selector(uploadDeliverediMAGEClickMethod), for: .touchUpInside)
        
    }
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func uploadDeliverediMAGEClickMethod() {
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDUploadDelivereditemImageId") as? PDUploadDelivereditemImage
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
}


extension ToGetDetails: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ToGetDetailsTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! ToGetDetailsTableCell
        
        cell.backgroundColor = .clear
        
        return cell
    }

func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView .deselectRow(at: indexPath, animated: true)
    
    
}
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}


extension ToGetDetails: UITableViewDelegate {
    
}
