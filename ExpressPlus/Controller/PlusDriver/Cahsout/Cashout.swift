//
//  Cashout.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire
class Cashout: UIViewController {

    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "CASHOUT"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var btnSubmitRequest:UIButton! {
        didSet {
            btnSubmitRequest.layer.cornerRadius = 6
            btnSubmitRequest.clipsToBounds = true
            btnSubmitRequest.setTitleColor(.white, for: .normal)
            btnSubmitRequest.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var txtAmountField:UITextField! {
        didSet {
            txtAmountField.backgroundColor = UIColor.init(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1)
            txtAmountField.layer.cornerRadius = 6
            txtAmountField.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnSubmitRequest.addTarget(self, action: #selector(setUpValidation), for: .touchUpInside)
        
        self.sideBarMenuClick()
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    @objc func setUpValidation() {
        if String(txtAmountField.text!) == "" {
            let alert = UIAlertController(title: String("Alert"), message: String("Please enter some Amount"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
                // self.backClickMthod()
                
                self.txtAmountField.text = ""
                
            }))
            self.present(alert, animated: true, completion: nil)
        } else if String(txtAmountField.text!) == "0" {
            let alert = UIAlertController(title: String("Alert"), message: String("Amount should be greate than zero."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
                // self.backClickMthod()
                
                self.txtAmountField.text = ""
                
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            self.cashoutMoneyFromDriverScreen()
        }
    }
    @objc func cashoutMoneyFromDriverScreen() {
     
      ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => cashoutrequest
      [userId] => 123
      [requestAmount] => 1
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
                   parameters = [
                       "action"              : "cashoutrequest",
                        "userId"              : person["userId"] as Any,
                        "requestAmount"      : String(txtAmountField.text!),
                       // "status"              : String(""),
                       // "pageNo"              : String("0")
             ]
     }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                 print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                ERProgressHud.sharedInstance.hide()
                                 
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                ERProgressHud.sharedInstance.hide()
                                let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                    // self.dismiss(animated: true, completion: nil)
                                    // self.backClickMthod()
                                    
                                    self.txtAmountField.text = ""
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                                
                                
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
}
