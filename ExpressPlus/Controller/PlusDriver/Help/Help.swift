//
//  Help.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import MessageUI

import Alamofire

class Help: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var imglogo:UIImageView! {
        didSet {
            imglogo.layer.cornerRadius = 60
            imglogo.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "HELP"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var btnPhone:UIButton!
    @IBOutlet weak var btnEmil:UIButton!
    
    @IBOutlet weak var btnTermsAndConditions:UIButton!
    
    var strPhone:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        self.view.backgroundColor = .white
        
        self.btnTermsAndConditions.addTarget(self, action: #selector(openTermsAndPrivacyPopup), for: .touchUpInside)
        
        self.sideBarMenuClick()
        
        self.helpWB()
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }

    @objc func openTermsAndPrivacyPopup() {
        let alertController = UIAlertController(title: nil, message: "Help", preferredStyle: .actionSheet)
        let okAction = UIAlertAction(title: "Terms", style: UIAlertAction.Style.default) {
                UIAlertAction in
                NSLog("OK Pressed")
            
            if let url = URL(string: "https://www.expressplusnow.com/terms-and-conditions/") {
                UIApplication.shared.open(url)
            }
            
        }
        let okAction3 = UIAlertAction(title: "Privacy  Policy", style: UIAlertAction.Style.default) {
                UIAlertAction in
                NSLog("OK Pressed")
            
            if let url = URL(string: "https://www.expressplusnow.com/privacy-policy/") {
                UIApplication.shared.open(url)
            }
            
        }
        let okAction2 = UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        
        alertController.addAction(okAction)
        alertController.addAction(okAction2)
        alertController.addAction(okAction3)
        
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func helpWB() {
       ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        let urlString = BASE_URL_EXPRESS_PLUS
        var parameters:Dictionary<AnyHashable, Any>!
           
        /*
          [action] => help
         */
                   parameters = [
                       "action"     : "help"
            ]
        
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON)
                                /*
                                 data =     {
                                     eamil = "support@foodexpress.com";
                                     phone = "180-234-5678";
                                 };
                                 */
                               
                                var strSuccess : String!
                                strSuccess = JSON["status"]as Any as? String
                               
                               // var strSuccessAlert : String!
                               // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == "success" {
                                ERProgressHud.sharedInstance.hide()
                              
                                var dict: Dictionary<AnyHashable, Any>
                                 dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                // (dict["phone"] as! String)
                                self.btnPhone.setTitle((dict["phone"] as! String), for: .normal)
                                self.btnEmil.setTitle((dict["eamil"] as! String), for: .normal)
                                
                                self.strPhone = (dict["eamil"] as! String)
                                
                                self.btnPhone.addTarget(self, action: #selector(self.callMethodClick), for: .touchUpInside)
                                self.btnEmil.addTarget(self, action: #selector(self.mailMethodClick), for: .touchUpInside)
                                
                               }
                               else {
                                 ERProgressHud.sharedInstance.hide()
                               }
                               
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                              ERProgressHud.sharedInstance.hide()
                               let alertController = UIAlertController(title: nil, message: SERVER_ISSUE_MESSAGE_ONE+"\n"+SERVER_ISSUE_MESSAGE_TWO, preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    @objc func callMethodClick() {
        if let url = URL(string: "tel://\(self.strPhone ?? "")"),
        UIApplication.shared.canOpenURL(url) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @objc func mailMethodClick() {
           let mailComposeViewController = configuredMailComposeViewController()
           if MFMailComposeViewController.canSendMail() {
               self.present(mailComposeViewController, animated: true, completion: nil)
           } else {
               self.showSendMailErrorAlert()
           }
       }
    
       func configuredMailComposeViewController() -> MFMailComposeViewController {
           let mailComposerVC = MFMailComposeViewController()
           mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
    
            mailComposerVC.setToRecipients([self.strPhone])
            mailComposerVC.setSubject("I AM SUBJECT")
            mailComposerVC.setMessageBody("I AM MESSAGE BODY", isHTML: false)
    
            return mailComposerVC
        }
    
        func showSendMailErrorAlert() {
            let alert = UIAlertController(title: "Could Not Send Email", message: "You can always access your content by signing back in",         preferredStyle: UIAlertController.Style.alert)

            alert.addAction(UIAlertAction(title: "Ok",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            //Sign out action
            }))
            self.present(alert, animated: true, completion: nil)

        }
    
        // MARK: MFMailComposeViewControllerDelegate Method
        func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
            controller.dismiss(animated: true, completion: nil)
        }
    
}
