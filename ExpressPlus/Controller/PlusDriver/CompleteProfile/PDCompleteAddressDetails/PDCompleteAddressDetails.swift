//
//  PDCompleteAddressDetails.swift
//  ExpressPlus
//
//  Created by Apple on 12/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
// MARK:- LOCATION -
import CoreLocation

import Alamofire

class PDCompleteAddressDetails: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {

    let cellReuseIdentifier = "pDCompleteAddressDetailsTableCell"
    
    let locationManager = CLLocationManager()
         
    let regularFont = UIFont.systemFont(ofSize: 16)
    let boldFont = UIFont.boldSystemFont(ofSize: 16)
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var arrGetCategoryName:NSMutableArray! = []
    var arrGetCategoryId:NSMutableArray! = []
    var saveCategoryIdForWebserviceParameter:String!
    
    var arrListOfAllMyCity:NSMutableArray! = []
    
          // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    var fullAddress:String!
    
    
    var finalStateId:String!
    
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "COMPLETE PROFILE"
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            // btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.backgroundColor = .clear
            tbleView.delegate = self
            tbleView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(PDPayFiveDollar.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        self.sideBarMenuClick()
        
        self.statelist()
        
        self.iAmHereForLocationPermission()
    }
    
    @objc func sideBarMenuClick() {
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    // MARK:- GET CUSTOMER LOCATION -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDCompleteAddressDetailsTableCell
        
        
        
        
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            //print(self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            print("locality ==> "+locality as Any) // sector 10 dwarka
            
            print(self.strSaveCountryName as Any) // india
            print(self.strSaveStateName as Any) // new delhi
            print(self.strSaveZipcodeName as Any) // 110075
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            cell.txtAddress.text = String(self.strSaveLocality) //+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
            // cell.txtCountry.text = String(self.strSaveCountryName)
            cell.txtState.text = String(self.strSaveStateName)
            cell.txtZipcode.text = String(self.strSaveZipcodeName)
            // cell.txtCity.text = String(locality)
            
            // self.findMyStateTaxWB()
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func statelist() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

        // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        var parameters:Dictionary<AnyHashable, Any>!
                    
            // let x : Int = person["userId"] as! Int
            // let myString = String(x)
            
        parameters = ["action" : "statelist"
        ]
            
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                            
                        strSuccess = JSON["status"]as Any as? String // match your success status here
                                                                                    
                    if strSuccess == String("success") {
                        ERProgressHud.sharedInstance.hide()
                        
                        var ar : NSArray!
                        ar = (JSON["data"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                        
                        // self.tbl
                        
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
    // }
    }
    
    
    
    @objc func pleaseSelectState() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDCompleteAddressDetailsTableCell
        
        // self.arrGetCategoryName.removeAllObjects()
        // self.arrGetCategoryId.removeAllObjects()
        
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "Please Select State",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : NAVIGATION_BACKGROUND_COLOR,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "Search State",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : NAVIGATION_BACKGROUND_COLOR,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        for check in 0...self.arrListOfAllMyOrders.count-1 {
             print(check as Any)
            
            let item = arrListOfAllMyOrders[check] as? [String:Any]
            // print(item as Any)
            
            let strName:String!
            let strId:String!
            strName = (item!["name"] as! String)
            
            // let x : Int = item!["stateCode"] as! Int
            // let myString = String(x)
            strId = (item!["stateCode"] as! String)
            
            self.arrGetCategoryName.add(strName as Any)
            
            self.arrGetCategoryId.add(strId as Any)
            
        }
        
        /*
         bankId = 2;
         bankName = "BANK OF AMERICA";
         created = "Oct 30th, 2019, 6:54 am";
         image = "http://demo2.evirtualservices.com/kam-cash/site/img/uploads/bank/BOA.jpg";
         */
        
        
        let array: [String] = self.arrGetCategoryName.copy() as! [String]
        
        let arrGender = array
        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
           onCompletion: { (selectedIndexes, selectedValues) in
            if let selectedValue = selectedValues.first{
                if selectedValue == arrGender.last!{
                    // cell.txtCategory.text = ""
                    
                    cell.txtCountry.text = "\(selectedValue)"
                      
                    let index = self.arrGetCategoryName.index(of: "\(selectedValue)")
                    
                    let item2 = self.arrListOfAllMyOrders[index] as? [String:Any]
                    
                    let x : Int = item2!["id"] as! Int
                    let myString = String(x)
                    print(myString as Any)
                    self.finalStateId = myString
                    
                    self.allCityList(strCityIdIs: self.finalStateId)
                    
                    self.saveCategoryIdForWebserviceParameter = (item2!["stateCode"] as! String)
                    
                } else {
                    
                    cell.txtCountry.text = "\(selectedValue)"
                    
                    let index = self.arrGetCategoryName.index(of: "\(selectedValue)");
                  
                    let item2 = self.arrListOfAllMyOrders[index] as? [String:Any]
                    
                    let x : Int = item2!["stateCode"] as! Int
                    let myString = String(x)
                    print(myString as Any)
                    self.finalStateId = myString
                    
                    self.allCityList(strCityIdIs: self.finalStateId)
                    
                    self.saveCategoryIdForWebserviceParameter = (item2!["stateCode"] as! String)
                    
                }
                
            } else {
                // self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                cell.txtCountry.text = "Please select type"
            }
        },
           onCancel: {
            print("Cancelled")
        }
        )
        
        picker.show(withAnimation: .FromBottom)
        
    }
    
    
    @objc func allCityList(strCityIdIs:String) {
    
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "fetchin cities name...")
        
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

        // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        var parameters:Dictionary<AnyHashable, Any>!
                    
            // let x : Int = person["userId"] as! Int
            // let myString = String(x)
            
        parameters = [
            "action" : "citylist",
            "stateId" : String(strCityIdIs)
        ]
            
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                            
                        strSuccess = JSON["status"]as Any as? String // match your success status here
                                                                                    
                    if strSuccess == String("success") {
                        ERProgressHud.sharedInstance.hide()
                        
                        var ar : NSArray!
                        ar = (JSON["data"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                        
                        // self.tbl
                        
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
        
    }
    
    @objc func allCitiesName() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDCompleteAddressDetailsTableCell
        
        // self.arrGetCategoryName.removeAllObjects()
        // self.arrGetCategoryId.removeAllObjects()
        
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "Please Select City",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : NAVIGATION_BACKGROUND_COLOR,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "Search City",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : NAVIGATION_BACKGROUND_COLOR,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        for check in 0...self.arrListOfAllMyOrders.count-1 {
             print(check as Any)
            
            let item = arrListOfAllMyCity[check] as? [String:Any]
             print(item as Any)
            
            let strName:String!
            let strId:String!
            strName = (item!["name"] as! String)
            
            // let x : Int = item!["stateCode"] as! Int
            // let myString = String(x)
            strId = (item!["stateCode"] as! String)
            
            // self.arrGetCategoryName.add(strName as Any)
            
            // self.arrGetCategoryId.add(strId as Any)
            
        }
        
        /*
         bankId = 2;
         bankName = "BANK OF AMERICA";
         created = "Oct 30th, 2019, 6:54 am";
         image = "http://demo2.evirtualservices.com/kam-cash/site/img/uploads/bank/BOA.jpg";
         */
        
        
        let array: [String] = self.arrGetCategoryName.copy() as! [String]
        
        let arrGender = array
        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
           onCompletion: { (selectedIndexes, selectedValues) in
            if let selectedValue = selectedValues.first{
                if selectedValue == arrGender.last!{
                    // cell.txtCategory.text = ""
                    
                    cell.txtCountry.text = "\(selectedValue)"
                      
                    let index = self.arrGetCategoryName.index(of: "\(selectedValue)")
                    
                    let item2 = self.arrListOfAllMyOrders[index] as? [String:Any]
                    
                    let x : Int = item2!["id"] as! Int
                    let myString = String(x)
                    print(myString as Any)
                    self.finalStateId = myString
                    
                    self.allCityList(strCityIdIs: self.finalStateId)
                    
                    self.saveCategoryIdForWebserviceParameter = (item2!["stateCode"] as! String)
                    
                } else {
                    
                    cell.txtCountry.text = "\(selectedValue)"
                    
                    let index = self.arrGetCategoryName.index(of: "\(selectedValue)");
                  
                    let item2 = self.arrListOfAllMyOrders[index] as? [String:Any]
                    
                    let x : Int = item2!["stateCode"] as! Int
                    let myString = String(x)
                    print(myString as Any)
                    self.finalStateId = myString
                    
                    self.allCityList(strCityIdIs: self.finalStateId)
                    
                    self.saveCategoryIdForWebserviceParameter = (item2!["stateCode"] as! String)
                    
                }
                
            } else {
                // self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                cell.txtCountry.text = "Please select type"
            }
        },
           onCancel: {
            print("Cancelled")
        }
        )
        
        picker.show(withAnimation: .FromBottom)
        
    }
    
    // MARK:- BAG PURCHASE PAYMENT -
    @objc func bagPuchaseBW() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDCompleteAddressDetailsTableCell
        
        
        if cell.txtName.text == "" {
            
            let alert = UIAlertController(title: String("Error Message"), message: String("Name should not be empty"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtPhoneNumber.text == "" {
            let alert = UIAlertController(title: String("Error Message"), message: String("Phone number should not be empty"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
                
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtAddress.text == "" {
            let alert = UIAlertController(title: String("Error Message"), message: String("Address should not be empty"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
                
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtZipcode.text == "" {
            let alert = UIAlertController(title: String("Error Message"), message: String("Zipcode should not be empty"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
                
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtState.text == "" {
            let alert = UIAlertController(title: String("Error Message"), message: String("State should not be empty"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
                
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtCountry.text == "" {
            let alert = UIAlertController(title: String("Error Message"), message: String("Country should not be empty"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                // self.dismiss(animated: true, completion: nil)
                
            }))
            self.present(alert, animated: true, completion: nil)
        }  else {
            
            /*
             var strGetName:String!
             var strGetPhoneNumber:String!
             var strGetAddress:String!
             var strGetZipcode:String!
             var strGetState:String!
             var strGetCountry:String!
             */
            
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDBagPaymentId") as? PDBagPayment
            
            push!.strGetName        = String(cell.txtName.text!)
            push!.strGetPhoneNumber = String(cell.txtPhoneNumber.text!)
            push!.strGetAddress     = String(cell.txtAddress.text!)
            push!.strGetZipcode     = String(cell.txtZipcode.text!)
            push!.strGetState       = String(cell.txtState.text!)
            push!.strGetCountry     = String(cell.txtCountry.text!)
            
            self.navigationController?.pushViewController(push!, animated: true)
        }
    }
}



//MARK:- TABLE VIEW -
extension PDCompleteAddressDetails: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PDCompleteAddressDetailsTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! PDCompleteAddressDetailsTableCell
        
        cell.backgroundColor = .clear
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.btnAllCities.addTarget(self, action: #selector(allCitiesName), for: .touchUpInside)
        cell.btnAllState.addTarget(self, action: #selector(pleaseSelectState), for: .touchUpInside)
        cell.btnmakePayment.addTarget(self, action: #selector(bagPuchaseBW), for: .touchUpInside)
        
        return cell
    }
    
    @objc func makeFinalPayment() {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1000
    }
    
}

extension PDCompleteAddressDetails: UITableViewDelegate {
    
}


