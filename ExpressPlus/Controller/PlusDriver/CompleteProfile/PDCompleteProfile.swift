//
//  PDCompleteProfile.swift
//  ExpressPlus
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
// MARK:- LOCATION -
import CoreLocation

class PDCompleteProfile: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    var areYouFromDriverEdit:String!
    
    let regularFont = UIFont.systemFont(ofSize: 16)
    let boldFont = UIFont.boldSystemFont(ofSize: 16)
    
    var dictGetRegistrationData:NSDictionary!
    
    let locationManager = CLLocationManager()
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    // MARK:- ADDRESS STRING -
    var strSaveAddress:String!
    
    var imageStrOne:String!
    var imageStrTwo:String!
    var imageStrThree:String!
    
    var strPassImgString1:String!
    var imageStr1:String!
    var imgData1:Data!
    
    var strPassImgString2:String!
    var imageStr2:String!
    var imgData2:Data!
    
    var strPassImgString3:String!
    var imageStr3:String!
    var imgData3:Data!
    
    @IBOutlet weak var viewOne:UIView! {
        didSet {
            viewOne.backgroundColor = .white
            viewOne.layer.cornerRadius = 6
            viewOne.clipsToBounds = true
        }
    }
    @IBOutlet weak var viewTwo:UIView! {
        didSet {
            viewTwo.backgroundColor = .white
            viewTwo.layer.cornerRadius = 6
            viewTwo.clipsToBounds = true
        }
    }
    @IBOutlet weak var viewThree:UIView! {
        didSet {
            viewThree.backgroundColor = .white
            viewThree.layer.cornerRadius = 6
            viewThree.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var imgOne:UIImageView!
    @IBOutlet weak var imgTwo:UIImageView!
    @IBOutlet weak var imgThree:UIImageView!
    
    @IBOutlet weak var btnSaveandContinue:UIButton! {
        didSet {
            btnSaveandContinue.setTitleColor(.white, for: .normal)
            btnSaveandContinue.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnSaveandContinue.layer.cornerRadius = 4
            btnSaveandContinue.clipsToBounds = true
            btnSaveandContinue.setTitle("Save and Continue", for: .normal)
        }
    }
    
    @IBOutlet weak var txtAddress:UITextField! {
        didSet {
            txtAddress.layer.cornerRadius = 6
            txtAddress.clipsToBounds = true
            txtAddress.backgroundColor = .white
            txtAddress.layer.borderColor = UIColor.lightGray.cgColor
            txtAddress.layer.borderWidth = 0.8
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            txtAddress.leftView = paddingView
            txtAddress.leftViewMode = .always
            
            txtAddress.attributedPlaceholder = NSAttributedString(string: "Address",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            txtAddress.isUserInteractionEnabled = false
        }
    }
    
    @IBOutlet weak var txtCountry:UITextField! {
        didSet {
            txtCountry.layer.cornerRadius = 6
            txtCountry.clipsToBounds = true
            txtCountry.backgroundColor = .white
            txtCountry.layer.borderColor = UIColor.lightGray.cgColor
            txtCountry.layer.borderWidth = 0.8
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            txtCountry.leftView = paddingView
            txtCountry.leftViewMode = .always
            
            txtCountry.attributedPlaceholder = NSAttributedString(string: "Country",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            txtAddress.isUserInteractionEnabled = false
        }
    }
    
    @IBOutlet weak var lblCountryName:UILabel!
    @IBOutlet weak var imgOneShow:UIImageView! {
        didSet {
            imgOneShow.isHidden = true
        }
    }
    @IBOutlet weak var imgTwoShow:UIImageView! {
        didSet {
            imgTwoShow.isHidden = true
        }
    }
    @IBOutlet weak var imgThreeShow:UIImageView! {
        didSet {
            imgThreeShow.isHidden = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarStyle()
        
        self.txtAddress.delegate = self
        self.txtCountry.delegate = self
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(PDCompleteProfile.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        
        // self.btnSaveandContinue.addTarget(self, action: #selector(pushToPaymentInformation), for: .touchUpInside)
        self.btnSaveandContinue.addTarget(self, action: #selector(saveAndContinueClickMethod), for: .touchUpInside)
        
        self.imageStrOne = "0"
        self.imageStrTwo = "0"
        self.imageStrThree = "0"
        
        /*self.imgData1 = NSData(0) as Data?
        self.imgData1 = 0
        self.imgData1 = 0*/
        
        // print(dictGetRegistrationData as Any)
        
        /*
         AccountHolderName = "";
         AccountNo = "";
         AutoInsurance = "";
         BankName = "";
         RoutingNo = "";
         accountType = "";
         address = "";
         companyBackground = "";
         contactNumber = 1298764526282;
         country = "";
         device = "";
         deviceToken = "";
         dob = "";
         drivlingImage = "";
         email = "driverios2@gmail.com";
         firebaseId = "";
         foodTag = "";
         fullName = "driver 2";
         gender = "";
         image = "";
         lastName = "";
         latitude = "";
         logitude = "";
         longitude = "";
         middleName = "";
         role = Driver;
         socialId = "";
         socialType = "";
         ssnImage = "";
         state = "";
         userId = 145;
         wallet = 0;
         zipCode = "";
         */
        
        // image one
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(PDCompleteProfile.cellTappedMethod1(_:)))

        imgOne.isUserInteractionEnabled = true
        // imgProfile.tag = indexPath.row
        imgOne.addGestureRecognizer(tapGestureRecognizer1)
        
        // image one
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(PDCompleteProfile.cellTappedMethod2(_:)))

        imgTwo.isUserInteractionEnabled = true
        // imgProfile.tag = indexPath.row
        imgTwo.addGestureRecognizer(tapGestureRecognizer2)
        
        /*// image one
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(PDCompleteProfile.cellTappedMethod3(_:)))

        imgThree.isUserInteractionEnabled = true
        // imgProfile.tag = indexPath.row
        imgThree.addGestureRecognizer(tapGestureRecognizer3)*/
 
        
        
        if areYouFromDriverEdit == "yesFromDriverEdit" {
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                print(person as Any)
                /*
                 ["dob": , "ssnImage": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1591957824expressPlusDriver.jpg, "state": , "userId": 181, "logitude": 19.0176147, "image": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1591957821expressPlusRestaurant.jpg, "foodTag": none, "drivlingImage": , "AccountHolderName": m, "gender": , "companyBackground": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1591957822expressPlusRestaurantLogo.jpg, "email": m2@gmail.com, "RoutingNo": m5, "role": Driver, "AccountNo": m2, "accountType": Saving, "lastName": , "longitude": 19.0176147, "wallet": 0, "latitude": 19.0176147, "socialId": , "fullName": m, "contactNumber": 23456789078, "address": Wadala West Mumbai, "device": , "AutoInsurance": , "zipCode": , "socialType": , "country": India, "deviceToken": , "firebaseId": , "middleName": , "BankName": m1]
                 (lldb)
                 */
                
                self.imgOneShow.isHidden = false
                self.imgTwoShow.isHidden = false
                self.imgThreeShow.isHidden = false
                
                if imgOneShow == nil {
                    self.imgOneShow.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "camera11"))
                } else {
                    self.imgOneShow.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "camera11"))
                    
                    // guard let image = UIImage(named: "someImage") else { return }
                    // guard let data = self.imgOneShow.image?.jpegData(compressionQuality: 1.0) else { return }
                    
                    guard let data = self.imgOneShow.image?.pngData() else { return }
                    // print(data as Any)
                    
                    imgData1 = data
                }
                
                if imgTwoShow == nil {
                 self.imgTwoShow.sd_setImage(with: URL(string: (person["companyBackground"] as! String)), placeholderImage: UIImage(named: "camera11"))
                } else {
                   self.imgTwoShow.sd_setImage(with: URL(string: (person["companyBackground"] as! String)), placeholderImage: UIImage(named: "camera11"))
                    
                    guard let data = self.imgTwoShow.image?.pngData() else { return }
                    // print(data as Any)
                    imgData2 = data
                }
                
                /*if imgThreeShow == nil {
                    self.imgThreeShow.sd_setImage(with: URL(string: (person["ssnImage"] as! String)), placeholderImage: UIImage(named: "camera11"))
                } else {
                    self.imgThreeShow.sd_setImage(with: URL(string: (person["ssnImage"] as! String)), placeholderImage: UIImage(named: "camera11"))
                    
                    guard let data = self.imgThreeShow.image?.pngData() else { return }
                    // print(data as Any)
                    imgData3 = data
                }*/
                
                /*
                 print(imgData1 as Any)
                 print(imgData2 as Any)
                 print(imgData3 as Any)
                 */
                
                
                
            }
        } else {
            
        }
        
        
        
        
        self.strSaveLatitude = "0"
        self.strSaveLongitude = "0"
        self.strSaveLocality = "0"
        self.strSaveLocalAddress = "0"
        
        
        self.iAmHereForLocationPermission()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    // MARK:- DETEMINE LOCATION -
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    // MARK:- GET CUSTOMER LOCATION
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            // print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            // print("locality ==> "+locality as Any) // sector 10 dwarka
            
            // print(self.strSaveCountryName as Any) // india
            // print(self.strSaveStateName as Any) // new delhi
            // print(self.strSaveZipcodeName as Any) // 110075
            
            let addressIs:String = String(self.strSaveLocality)+" "+String(self.self.strSaveLocalAddress)
            self.txtAddress.text = String(addressIs)
             self.lblCountryName.text = self.strSaveCountryName
            self.txtCountry.text = self.strSaveCountryName
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            // self.findMyStateTaxWB()
        }
    }
    
    @objc func navigationBarStyle() {
        self.title = "COMPLETE PROFILE"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.barTintColor = NAVIGATION_BACKGROUND_COLOR
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.backgroundColor = .orange
    }
    
    // MARK:- PUSH TO PAYMENT INFORMATION -
       @objc func pushToPaymentInformation() {
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDPaymentInformationId") as? PDPaymentInformation
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
       }
    
    @objc func cellTappedMethod1(_ sender:AnyObject) {
         print("you tap image number: \(sender.view.tag)")
        
        let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera1()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery1()
        }))

        alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    @objc func cellTappedMethod2(_ sender:AnyObject){
         print("you tap image number: \(sender.view.tag)")
        
        let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera2()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery2()
        }))

        alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    @objc func cellTappedMethod3(_ sender:AnyObject){
         print("you tap image number: \(sender.view.tag)")
        
        let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera3()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery3()
        }))

        alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    @objc func openCamera1() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        self.imageStrTwo = "0"
        self.imageStrOne = "1"
        self.imageStrThree = "0"
    }
    
    @objc func openGallery1() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        self.imageStrTwo = "0"
        self.imageStrOne = "1"
        self.imageStrThree = "0"
    }
    
    @objc func openCamera2() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        self.imageStrTwo = "1"
        self.imageStrOne = "0"
        self.imageStrThree = "0"
        
    }
    
    @objc func openGallery2() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        self.imageStrTwo = "1"
        self.imageStrOne = "0"
        self.imageStrThree = "0"
    }
    
    @objc func openCamera3() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        self.imageStrTwo = "0"
        self.imageStrOne = "0"
        self.imageStrThree = "1"
        
    }
    
    @objc func openGallery3() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        self.imageStrTwo = "0"
        self.imageStrOne = "0"
        self.imageStrThree = "1"
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
           // let indexPath = IndexPath.init(row: 0, section: 0)
           // let cell = self.tbleView.cellForRow(at: indexPath) as! RAddMenuTableCell
           
           // cell.imgPhoto.isHidden = false
           
           if imageStrOne == "1" {
               imgOneShow.isHidden = false
               let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
               imgOneShow.image = image_data // show image on profile
               let imageData:Data = image_data!.pngData()!
               imageStr1 = imageData.base64EncodedString()
               self.dismiss(animated: true, completion: nil)
               imgData1 = image_data!.jpegData(compressionQuality: 0.2)!
               //print(type(of: imgData)) // data
               
               // self.strPassImgString1 = "1"
           } else if imageStrTwo == "1" {
               imgTwoShow.isHidden = false
               let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
               imgTwoShow.image = image_data // show image on profile
               let imageData:Data = image_data!.pngData()!
               imageStr2 = imageData.base64EncodedString()
               self.dismiss(animated: true, completion: nil)
               imgData2 = image_data!.jpegData(compressionQuality: 0.2)!
               //print(type(of: imgData)) // data
               
               // self.strPassImgString1 = "1"
           } /*else if imageStrThree == "1" {
               imgThreeShow.isHidden = false
               let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
               imgThreeShow.image = image_data // show image on profile
               let imageData:Data = image_data!.pngData()!
               imageStr3 = imageData.base64EncodedString()
               self.dismiss(animated: true, completion: nil)
               imgData3 = image_data!.jpegData(compressionQuality: 0.2)!
               //print(type(of: imgData)) // data
               
               // self.strPassImgString1 = "1"
           }*/
           
           
           // self.uploadWithImage()
       }
    
    @objc func saveAndContinueClickMethod() {
        if imgOneShow.isHidden == true {
            
            let alert = UIAlertController(title: String("Error!"), message: String("Please upload your profile Picture."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if imgTwoShow.isHidden == true {
            
            let alert = UIAlertController(title: String("Error!"), message: String("Please upload your driving license."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            // push
            // PDPaymentInformation
            
//            if areYouFromDriverEdit == "yesFromDriverEdit" {
//                // strYesDriverPortalForEdit
//            } else {
//
//            }
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDPaymentInformationId") as? PDPaymentInformation
            push!.fromRestaurantPage = "afterRegistrationOfRestaurant"
            push!.getImgDataOne = imgData1
            push!.getImgDataTwo = imgData2
            // push!.getImgDataThree = imgData3
            push!.editDriverProfileIsYesOrNo = areYouFromDriverEdit
            
            print(imgData1 as Any)
            print(imgData2 as Any)
            // print(imgData3 as Any)
            
            push!.strYesDriverPortalForEdit = "yesDriverEditPortalCont"
            
            push!.getCountry    = self.strSaveCountryName
            push!.getLat        = String(self.strSaveLatitude)
            push!.getLong       = String(self.strSaveLongitude)
            push!.getAddress    = String(self.strSaveLocality)+" "+String(self.self.strSaveLocalAddress)
            push!.getFoodTag    = String("none")
            
            
            self.navigationController?.pushViewController(push!, animated: true)
            
            //
            /*
            var getAddress:String!
            var getLat:String!
            var getLong:String!
            var getCountry:String!
            */
            
            /*
             self.strSaveCountryName     = country
             self.strSaveStateName       = city
             self.strSaveZipcodeName     = zipcode
             
             self.strSaveLocalAddress     = localAddress
             self.strSaveLocality         = locality
             self.strSaveLocalAddressMini = localAddressMini
             
             let doubleLat = locValue.latitude
             let doubleStringLat = String(doubleLat)
             
             let doubleLong = locValue.latitude
             let doubleStringLong = String(doubleLong)
             
             self.strSaveLatitude = String(doubleStringLat)
             self.strSaveLongitude = String(doubleStringLong)
             */
            
            // let addressIs:String = String(self.strSaveLocality)+" "+String(self.self.strSaveLocalAddress)
            
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}
