//
//  PDUploadDelivereditemImage.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class PDUploadDelivereditemImage: UIViewController {

    
    
    @IBOutlet weak var navigationBar:UIView! {
           didSet {
               navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
           }
       }
       
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "UPLOAD IMAGES"
        }
    }
       
    @IBOutlet weak var btnDoneThankyou:UIButton! {
        didSet {
            btnDoneThankyou.backgroundColor = .systemGreen
            btnDoneThankyou.setTitleColor(.white, for: .normal)
        }
    }
    
    // 81, 158, 238
    @IBOutlet weak var btnCamera:UIButton! {
        didSet {
            btnCamera.setTitleColor(.white, for: .normal)
            btnCamera.layer.cornerRadius = 4
            btnCamera.clipsToBounds = true
            btnCamera.backgroundColor = UIColor.init(red: 81.0/255.0, green: 158.0/255.0, blue: 238.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var btnGalery:UIButton! {
        didSet {
            btnGalery.setTitleColor(.white, for: .normal)
            btnGalery.layer.cornerRadius = 4
            btnGalery.clipsToBounds = true
            btnGalery.backgroundColor = UIColor.init(red: 223.0/255.0, green: 54.0/255.0, blue: 100.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}
