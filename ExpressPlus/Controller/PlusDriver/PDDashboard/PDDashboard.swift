//
//  PDDashboard.swift
//  ExpressPlus
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

import GoogleMaps

class PDDashboard: UIViewController {

    var dictOfNotificationPopup:NSDictionary!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "DASHBOARD"
        }
    }
    
    @IBOutlet weak var btnMenu:UIButton!
    
    @IBOutlet weak var btnStopSchedule:UIButton! {
        didSet {
            btnStopSchedule.layer.cornerRadius = 4
            btnStopSchedule.clipsToBounds = true
            btnStopSchedule.backgroundColor = UIColor.init(red: 240.0/255.0, green: 210.0/255.0, blue: 70.0/255.0, alpha: 1) // 240, 210, 70
        }
    }
    
    @IBOutlet weak var lblTitle:UILabel!
    // @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var btnAddress:UIButton! {
        didSet {
            btnAddress.tintColor = .systemYellow
        }
    }
    
    @IBOutlet weak var btnEdit:UIButton!
    @IBOutlet weak var btnGroup:UIButton!
    
    @IBOutlet weak var imgVieww:UIImageView!
    
    @IBOutlet weak var switchh:UISwitch!
    
    @IBOutlet weak var bottomVieww:UIView! {
        didSet {
            bottomVieww.backgroundColor = .clear
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.sideBarMenuClick()
        
        self.btnGroup.addTarget(self, action: #selector(jobHistoryClickMethod), for: .touchUpInside)
        self.btnEdit.addTarget(self, action: #selector(editClickMethod), for: .touchUpInside)
        
        /*
        let defaults = UserDefaults.standard
        defaults.setValue("", forKey: "keyLoginFullData")
        defaults.setValue(nil, forKey: "keyLoginFullData")
        */
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
            
            /*
            ["email": dxz@mailinator.com, "gender": , "AccountNo": 2356898523, "role": Driver, "socialId": , "RoutingNo": 2222, "dob": , "drivlingImage": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1590385622Screenshot_20200521-194736_GetSum.jpg, "firebaseId": , "image": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1590385622IMG-20200520-WA0005.jpg, "companyBackground": , "state": , "AccountHolderName": new dri, "BankName": bank t, "middleName": , "fullName": new driver, "accountType": Saving, "foodTag": , "deviceToken": fSAr6xyiStSytYjO7P0H9H:APA91bEg-FiTbWk2ZIqtR-ChOZXraiVE3cfr3HunqDgpcT5zOs0oENHc97x7gMa4znab0AUdQ0B4oICEmzHx3K5K-pghwO6CDuMcVVQTRplbyRqCU6uN5XlWdkPEw7b-G2ruVW51KBS5, "zipCode": , "longitude": 77.3240493, "userId": 116, "latitude": 28.587159, "device": Android, "lastName": , "ssnImage": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1590385622IMG-20200520-WA0001.jpg, "address": 99, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India, "contactNumber": 5623895236, "socialType": , "wallet": 272, "country": India, "AutoInsurance": , "logitude": 77.3240493]
             */
            
            if person["StripeStatus"] as! String == "0" {
                
                /*let alert = UIAlertController(title: String("Strip Account Setup"), message: String("Please connect your account with Stripe."), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Yes, Connect", style: UIAlertAction.Style.default, handler: { action in
                    
                     self.connectAccountWithStripe(strAccountNumber: person["stripeAccountNo"] as! String)
                }))
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                    // self.dismiss(animated: true, completion: nil)
                    
                    
                }))
                
                self.present(alert, animated: true, completion: nil)*/
                
                self.connectAccountWithStripe(strAccountNumber: person["stripeAccountNo"] as! String)
                
            } else {
                ERProgressHud.sharedInstance.hide()
                /*guard let url = URL(string: strURL) else { return }
                UIApplication.shared.open(url)*/
            }
            
            
            
            self.lblTitle.text      = (person["fullName"] as! String)
            // self.lblAddress.text    = (person["address"] as! String)
            
            self.btnAddress.contentHorizontalAlignment = .center
            UIView.animate(withDuration: 0.3, animations: {
              self.view.layoutIfNeeded()
                
                self.btnAddress.titleLabel?.numberOfLines = 0
                self.btnAddress.setTitle(String(" ")+(person["address"] as! String), for: .normal)
                
            })

            self.imgVieww.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "avatar"))
            
        } else {
            // self.plusDriverLogin()
        }
        
        self.switchh.addTarget(self, action: #selector(switchhClickMethod), for: .valueChanged)
        // self.btnStopSchedule.addTarget(self, action: #selector(pushToDashboardPD), for: .touchUpInside)
        
        
        
        print(dictOfNotificationPopup as Any)
        
        
        
        
        
        
        
        /*var input = GInput()
        input.keyword = "Restaurants"
        input.radius = 20000
        var location = GLocation()
        location.latitude = 26.273178
        location.longitude = 73.009545
        input.destinationCoordinate = location
        GoogleApi.shared.callApi(.nearBy, input: input) { (response) in
            if let data = response.data as? [GApiResponse.NearBy], response.isValidFor(.nearBy){
                // all nearby places
            }
        }*/
        
         // self.openGoogleMap()
        
        
        
        
        
    }
    
    @objc func openGoogleMap() {
        
        /*
         https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=restaurant&keyword=cruise&key=YOUR_API_KEY
         */
        
        
            UIApplication.shared.openURL(NSURL(string:"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=restaurant&keyword=cruise&key=AIzaSyDEYP9nenPV-zJjlCSbPuyf9eYSOfBdKlk")! as URL)

        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        print("calling ji")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            print("we are being popped")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        print("hello hello hello")
    }
    
    @objc func connectAccountWithStripe(strAccountNumber:String) {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

        var parameters:Dictionary<AnyHashable, Any>!
                  
        /*
         action: checkstripe
         userId:
         Account:  //stripeAccountNo
         */
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // let str:String = person["role"] as! String
           
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
            parameters = ["action"      : "checkstripe",
                          "userId"      : String(myString),
                          "Account"     : String(strAccountNumber)
            ]
        }
        
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String // match your success status here
                                              
                    var strMessage : String!
                    strMessage = JSON["msg"]as Any as? String
                    
                    var strMessage22 : String!
                    strMessage22 = JSON["StripeStatus"]as Any as? String
                    
                    if strSuccess == String("success") {
                        ERProgressHud.sharedInstance.hide()
                        
                         var strURL : String!
                         strURL = JSON["url"]as Any as? String
                        
//                         var dict: Dictionary<AnyHashable, Any>
//                         dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                        if strMessage22 == "1" {
                            
                            // self.pushScreenWB()
                            
                        } else {
                            
                            // self.pushScreenWB()
                            
                            /*guard let url = URL(string: strURL) else { return }
                            UIApplication.shared.open(url)*/
                            
                            let alert = UIAlertController(title: String("Strip Account Setup"), message: String("Please connect your account with Stripe."), preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Yes, Connect", style: UIAlertAction.Style.default, handler: { action in
                                
                                 // self.connectAccountWithStripe(strAccountNumber: person["stripeAccountNo"] as! String)
                                
                                guard let url = URL(string: strURL) else { return }
                                UIApplication.shared.open(url)
                                
                            }))
                            
                            alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                                // self.dismiss(animated: true, completion: nil)
                                
                                
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                            
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                        
                        /*let alert = UIAlertController(title: String(strSuccess), message: String(strMessage), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                             
                        }))
                        self.present(alert, animated: true, completion: nil)*/
                        
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
    }
    
    // MARK:- JOB HISTORY -
    @objc func jobHistoryClickMethod() {

        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDJobRequestId") as? PDJobRequest
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    
    // MARK:- EDIT -
    @objc func editClickMethod() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "REditProfileId") as? REditProfile
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    
    @objc func switchhClickMethod() {
        
        if switchh.isOn {
            print("on")
            self.availaibilityOnOrOff(strOnOff: "1")
        } else {
            print("off")
            self.availaibilityOnOrOff(strOnOff: "0")
        }
        
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
        btnMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    // MARK:- PUSH TO SPOT SCHEDULING -
    @objc func pushToDashboardPD() {
         let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDSpotSchedulingId") as? PDSpotScheduling
         self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    // MARK:- WEBSERVICE ( PLUS DRIVER LOGIN ) -
    @objc func availaibilityOnOrOff(strOnOff:String) {
           
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
                  
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        /*
         [action] => onoff
         [userId] => 123
         [availableStatus] => 0
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            let x2 : Int = person["userId"] as! Int
            let driverId = String(x2)
            
            if strOnOff == "0" {
                parameters = [
                          "action"              : "onoff",
                          "userId"              : String(driverId),
                          "availableStatus"     : String("0")
                ]
            } else {
                parameters = [
                          "action"              : "onoff",
                          "userId"              : String(driverId),
                          "availableStatus"     : String("1")
                ]
            }
        }
        print("parameters-------\(String(describing: parameters))")
                      
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                        response in
                  
            switch(response.result) {
                              case .success(_):
                                if let data = response.result.value {

                                    let JSON = data as! NSDictionary
                                   print(JSON as Any)
                                  
                                  var strSuccess : String!
                                  strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                                  if strSuccess == String("success") {
                                   print("yes")
                                    
                                     ERProgressHud.sharedInstance.hide()
                                    
                                    var strSuccess2 : String!
                                    strSuccess2 = JSON["msg"]as Any as? String
                                    
                                    let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                        // self.dismiss(animated: true, completion: nil)
                                        
                                        
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                        }
        }
    }
    
}
