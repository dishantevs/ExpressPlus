//
//  PDEarningTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class PDEarningTableCell: UITableViewCell {

    @IBOutlet weak var lblUsersName:UILabel!
    @IBOutlet weak var lblUsersTime:UILabel!
    @IBOutlet weak var imgUsersProfileImage:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
