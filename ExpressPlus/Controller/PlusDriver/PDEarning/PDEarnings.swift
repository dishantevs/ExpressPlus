//
//  PDEarnings.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class PDEarnings: UIViewController {

    let cellReuseIdentifier = "pDEarningTableCell"
    
    var arrStaticTextOfCollection = ["My Earnings","Spend Time","Completed Trip"]
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "TOTAL EARNING"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var sgmentControl:UISegmentedControl! {
        didSet {
            sgmentControl.selectedSegmentIndex = 0
            sgmentControl.addTarget(self, action: #selector(segmentControlClickMethod), for: .valueChanged)
        }
    }
    
    @IBOutlet weak var tbleView:UITableView! {
            didSet {
                tbleView.delegate = self
                tbleView.dataSource = self
            }
            
        }
        
        @IBOutlet weak var clView:UICollectionView! {
            didSet {
                clView.delegate = self
                clView.dataSource = self
            }
        }
        
        var collectionView: UICollectionView?
        var screenSize: CGRect!
        var screenWidth: CGFloat!
        var screenHeight: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.sideBarMenuClick()
    }

    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func segmentControlClickMethod() {
        if sgmentControl.selectedSegmentIndex == 0 {
 
          }
        else
        if sgmentControl.selectedSegmentIndex == 1 {
 
         }
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
}

//MARK:- COLLECTION VIEW
extension PDEarningCollectionCell: UICollectionViewDelegate {
    //Write Delegate Code Here
    
}

extension PDEarnings: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pDEarningCollectionCell", for: indexPath as IndexPath) as! PDEarningCollectionCell
           cell.backgroundColor = UIColor.white
           cell.layer.borderWidth = 0.5
           cell.layer.borderColor = UIColor.black.cgColor
           cell.layer.borderWidth = 0.70
           
           cell.contentView.layer.cornerRadius = 2.0
           cell.contentView.layer.borderWidth = 1.0
           cell.contentView.layer.borderColor = UIColor.clear.cgColor
           cell.contentView.layer.masksToBounds = true

           cell.layer.shadowColor = UIColor.darkGray.cgColor
           cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
           cell.layer.shadowRadius = 2.0
           cell.layer.shadowOpacity = 0.5
           cell.layer.masksToBounds = false
           cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
           
        
           return cell
    }
    
    //Write DataSource Code Here
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
  
}


extension PDEarnings: UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        var sizes: CGSize
        
        let result = UIScreen.main.bounds.size
            
//        NSLog("%f",result.height)
     
        if result.height == 480
        {
            //Load 3.5 inch xib
            sizes = CGSize(width: 170.0, height: 190.0)
        }
        else if result.height == 568
        {
            //Load 4 inch xib
            sizes = CGSize(width: 100.0, height: 80.0)
        }
        else if result.height == 667.000000
        {
            //Load 4.7 inch xib , 8
            sizes = CGSize(width: 100.0, height: 80.0) // done
        }
        else if result.height == 736.000000
        {
            // iphone 6s Plus and 7 Plus
            sizes = CGSize(width: 100.0, height: 80.0) // done
        }
        else if result.height == 812.000000
        {
            // iphone X , 11 pro
            sizes = CGSize(width: 100.0, height: 80.0)
        }
        else if result.height == 896.000000
        {
            // iphone Xr ,11, 11 pro max
            sizes = CGSize(width: 120.0, height: 100.0) // done
        }
        else
        {
            sizes = CGSize(width: 114.0, height: 114.0)
        }
     
        return sizes
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        let result = UIScreen.main.bounds.size
                             
       if result.height == 1024
       {
        return 60.0
       }
        else
       {
        return 0.0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        let result = UIScreen.main.bounds.size
                       
        if result.height == 1024
        {
            return UIEdgeInsets(top: 10, left: 150, bottom: 10, right: 40)
        }
        else
        {
            return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        }
        
    
    }
    
}


//MARK:- TABLE VIEW
extension PDEarnings: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:PDEarningTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! PDEarningTableCell
        
        cell.backgroundColor = .white
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
}

extension PDEarnings: UITableViewDelegate
{
    
}
