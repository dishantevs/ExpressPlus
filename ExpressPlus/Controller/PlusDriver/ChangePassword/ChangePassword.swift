//
//  ChangePassword.swift
//  ExpressPlus
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class ChangePassword: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var viewDown:UIView!{
        didSet {
            viewDown.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "CHANGE PASSWORD"
        }
    }
    
    @IBOutlet weak var lblHelloWelcome:UILabel! {
        didSet {
            lblHelloWelcome.text = "CHANGE PASSWORD"
        }
    }
    
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var txtOldPassword:UITextField! {
        didSet {
            txtOldPassword.backgroundColor = .white
            txtOldPassword.layer.borderColor = UIColor.lightGray.cgColor
            txtOldPassword.layer.borderWidth = 0.8
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 20))
            txtOldPassword.leftView = paddingView
            txtOldPassword.leftViewMode = .always
            
            txtOldPassword.attributedPlaceholder = NSAttributedString(string: "Old password",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
    }
    
    @IBOutlet weak var txtNewPassword:UITextField! {
        didSet {
            txtNewPassword.backgroundColor = .white
            txtNewPassword.layer.borderColor = UIColor.lightGray.cgColor
            txtNewPassword.layer.borderWidth = 0.8
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 20))
            txtNewPassword.leftView = paddingView
            txtNewPassword.leftViewMode = .always
            
            txtNewPassword.attributedPlaceholder = NSAttributedString(string: "New password",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
    }
    
    @IBOutlet weak var txtConfirmPassword:UITextField! {
        didSet {
            txtConfirmPassword.backgroundColor = .white
            txtConfirmPassword.layer.borderColor = UIColor.lightGray.cgColor
            txtConfirmPassword.layer.borderWidth = 0.8
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 20))
            txtConfirmPassword.leftView = paddingView
            txtConfirmPassword.leftViewMode = .always
            
            txtConfirmPassword.attributedPlaceholder = NSAttributedString(string: "Confirm password",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
    }
    
    @IBOutlet weak var btnUpdatePassword:UIButton! {
        didSet {
            btnUpdatePassword.setTitleColor(.white, for: .normal)
            btnUpdatePassword.layer.cornerRadius = 4
            btnUpdatePassword.clipsToBounds = true
            btnUpdatePassword.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnUpdatePassword.setTitle("UPDATE PASSWORD", for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        
        self.txtOldPassword.delegate = self
        self.txtNewPassword.delegate = self
        self.txtConfirmPassword.delegate = self
        
        self.btnUpdatePassword.addTarget(self, action: #selector(changePasswordWB), for: .touchUpInside)
        
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            self.lblHelloWelcome.text = "Hello "+(person["fullName"] as! String)+" welcome!"
            
        } else {
            
        }
        
        self.sideBarMenuClick()
        
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }

    // MARK:- WEBSERVICE ( CHANGE PASSWORD ) -
    @objc func changePasswordWB() {
       ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS
        var parameters:Dictionary<AnyHashable, Any>!
           
        /*
         [action] => changePassword
         [newPassword] => 123456
         [oldPassword] => 123456
         [userId] => 99
         */
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            let x : Int = (person["userId"] as! Int)
            let myString = String(x)
                   parameters = [
                       "action"     : "changePassword",
                       "userId"     : String(myString),
                       "newPassword": String(txtNewPassword.text!),
                       "oldPassword": String(txtOldPassword.text!)
            ]
        }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                    print(JSON)
                                /*
                                 status = success;
                                 wallet = 0;
                                 */
                               
                                var strSuccess : String!
                                strSuccess = JSON["status"]as Any as? String
                               
                                var strSuccessAlert : String!
                                strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == "success" {
                                ERProgressHud.sharedInstance.hide()
                                
                                self.txtNewPassword.text = ""
                                self.txtOldPassword.text = ""
                                self.txtConfirmPassword.text = ""
                                
                                let alert = UIAlertController(title: String(strSuccess), message: String(strSuccessAlert), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                     
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                               }
                               else {
                                 ERProgressHud.sharedInstance.hide()
                                
                                let alert = UIAlertController(title: String(strSuccess), message: String(strSuccessAlert), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                     
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                               }
                               
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                              ERProgressHud.sharedInstance.hide()
                               let alertController = UIAlertController(title: nil, message: SERVER_ISSUE_MESSAGE_ONE+"\n"+SERVER_ISSUE_MESSAGE_TWO, preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
}
