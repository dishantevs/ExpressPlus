//
//  PDPayFiveDollar.swift
//  ExpressPlus
//
//  Created by Apple on 24/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

import Stripe

// MARK:- LOCATION -
import CoreLocation

class PDPayFiveDollar: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {

    let cellReuseIdentifier = "pDPayFiveDollarTableCell"
    
    var strWhatCardIam:String!
    
    let locationManager = CLLocationManager()
    
    // MARK:- SAVE LOCATION STRING -
    var strCountryCodeIs:String!
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    // MARK:- FETCHED FROM PREVIOUS SCREEEN -
    var fetchedAddress:String!
    var fetchedState:String!
    var fetchedCity:String!
    var fetchedZipcode:String!
    var fetchedPhoneNumber:String!
    var fetchedStateCode:String!
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "PAYMENT"
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            // btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
    }
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
        }
    }
    
    @IBOutlet weak var lblCardNumberHeading:UILabel!
    @IBOutlet weak var lblEXPDate:UILabel!
    
    @IBOutlet weak var lblPayableAmount:UILabel!
    
    @IBOutlet weak var btnmakePayment:UIButton! {
        didSet {
            btnmakePayment.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnmakePayment.setTitle("MAKE PAYMENT", for: .normal)
            btnmakePayment.setTitleColor(.white, for: .normal)
        }
    }
    @IBOutlet weak var viewCard:UIView! {
        didSet {
            viewCard.backgroundColor = NAVIGATION_BACKGROUND_COLOR // UIColor.init(red: 34.0/255.0, green: 72.0/255.0, blue: 104.0/255.0, alpha: 1)
            viewCard.layer.cornerRadius = 6
            viewCard.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var imgCardImage:UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.strCountryCodeIs = "0"
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(PDPayFiveDollar.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        self.btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        
        self.btnmakePayment.addTarget(self, action: #selector(firstCheckValidation), for: .touchUpInside)
        
        // self.iAmHereForLocationPermission()
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    
    // MARK:- GET CUSTOMER LOCATION
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! PDPayFiveDollarTableCell
        
        
        /*
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.countryCode { countryCodeIs, error in
            guard let countryCodeIs = countryCodeIs, error == nil else { return }
            
            print(countryCodeIs as Any)
            self.strCountryCodeIs = countryCodeIs
            
          */
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fullAddressFull { city, country, zipcode,localAddress,localAddressMini,locality,countryCodeIs, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality,let countryCodeIs = countryCodeIs, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            self.strCountryCodeIs = countryCodeIs
            
            print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            print("locality ==> "+locality as Any) // city // sector 10 dwarka
            
            // print("administ are"+subAdministrativeArea as Any)// state

            print(self.strSaveLocalAddress as Any)// state
            print(self.strSaveCountryName as Any) // india
            print(self.strSaveStateName as Any) // new delhi
            print(self.strSaveZipcodeName as Any) // 110075
            print(self.strCountryCodeIs as Any) // 110075
            
            
            
            
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func sideBarMenuClick() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func firstCheckValidation() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDPayFiveDollarTableCell
        
        if cell.txtCardNumber.text == "" {
            let alert = UIAlertController(title: "Card Number", message: "Card number should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtExpDate.text == "" {
            let alert = UIAlertController(title: "Exp Month", message: "Expiry Month should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtCVV.text == "" {
            let alert = UIAlertController(title: "Security Code", message: "Security Code should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            self.getTokenFromStripe()
        }
    }
    
    
    
    @objc func getTokenFromStripe() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDPayFiveDollarTableCell
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        let cardParams = STPCardParams()
        
        let fullNameArr = cell.txtExpDate.text!.components(separatedBy: "/")

        // print(fullNameArr as Any)
        let name    = fullNameArr[0]
        let surname = fullNameArr[1]
        
        cardParams.number       = String(cell.txtCardNumber.text!)
        cardParams.expMonth     = UInt(name)!
        cardParams.expYear      = UInt(surname)!
        cardParams.cvc          = String(cell.txtCVV.text!)
        
        STPAPIClient.shared.createToken(withCard: cardParams) { token, error in
            guard let token = token else {
                // Handle the error
                // print(error as Any)
                // print(error?.localizedDescription as Any)
                ERProgressHud.sharedInstance.hide()
                
                let alert = UIAlertController(title: "Error", message: String(error!.localizedDescription), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
                
                
                
                return
            }
            
            let tokenID = token.tokenId
            print(tokenID)
            
            self.finalPaymentIsAfterStripe(strTokenIdIs: tokenID)
        }
    }
    
    // MARK:- FINAL PAYMENT -
    @objc func finalPaymentIsAfterStripe(strTokenIdIs:String) {
        
        /*
        action:chargeramount
        userId:25
        amount:100
        tokenID:tok_1GxrrzIudZlr53ucmrz4pyyq
        */
    
        let urlString = CHARGE_PROCESS
        
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // let str:String = person["role"] as! String
                   parameters = [
                    "action"    : "chargeramount",
                    "userId"    : person["userId"] as Any,
                    "amount"    : String("500"),
                    "tokenID"   : String(strTokenIdIs)
            ]
     }
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
               
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                               
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                    if strSuccess == String("success") {
                        print("yes")
                                 
                                // ERProgressHud.sharedInstance.hide()
                                
                                
                        self.callAfterFullAnFinalPayment(transactionIdIs: JSON["transactionID"] as! String)
                                
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                                
                                
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                               
                ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
  
    @objc func callAfterFullAnFinalPayment(transactionIdIs:String) {
        
         /*
         action:cardholder
         userId:4
         name:Raushan Suri
         phoneNumber:17428171872
         email:raushan.kumar@evirtualservices.com
         address:1234 Main Street
         city:San Francisco

         state:CA
         country:US
         zipcode:94111
         transactionId:TRN784514
         */
       
     
        let urlString = CARD_PROCESS
        
         var parameters:Dictionary<AnyHashable, Any>!
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             // let str:String = person["role"] as! String
            
            /*
             var fetchedAddress:String!
             var fetchedState:String!
             var fetchedCity:String!
             var fetchedZipcode:String!
             var fetchedPhoneNumber:String!
             var fetchedStateCode:String!
             */
            
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
                    parameters = [
                        "action"            : "cardholder",
                         "userId"           : String(myString),
                         "name"             : person["fullName"] as! String,
                         "phoneNumber"      : String(self.fetchedPhoneNumber),
                         "email"            : person["email"] as! String,
                         "address"          : String(self.fetchedAddress),
                         "city"             : String(self.fetchedCity),
                         "state"            : String(self.fetchedStateCode),
                         "country"          : String("US"),
                         "zipcode"          : String(self.fetchedZipcode),
                         "transactionId"    : String(transactionIdIs)
              ]
      }
        print("parameters-------\(String(describing: parameters))")
                    
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
            {
                response in
                
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value {

                        let JSON = data as! NSDictionary
                        print(JSON as Any)
                                
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                                
                                  // var strSuccessAlert : String!
                                  // strSuccessAlert = JSON["msg"]as Any as? String
                                
                        if strSuccess == String("success") {
                            print("yes")
                                  
                                 
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                                
                            ERProgressHud.sharedInstance.hide()
                            let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                    // self.dismiss(animated: true, completion: nil)
                                    
                                self.finalLoginWBB()
                                    
                            }))
                            self.present(alert, animated: true, completion: nil)
                                    
                        }
                        else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                                 
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                                 
                            let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                                                       // self.dismiss(animated: true, completion: nil)
                                                                       
                                                                       // self.finalLoginWBB()
                                                                       
                                                                       
                                    }))
                            self.present(alert, animated: true, completion: nil)
                                                                   
                        }
                    }

                case .failure(_):
                    print("Error message:\(String(describing: response.result.error))")
                                
                    ERProgressHud.sharedInstance.hide()
                                
                    let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                        UIAlertAction in
                                        NSLog("OK Pressed")
                    }
                                
                    alertController.addAction(okAction)
                                
                    self.present(alertController, animated: true, completion: nil)
                                
                    break
                }
        }
     
    }
    
    
    
    @objc func finalLoginWBB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        var parameters:Dictionary<AnyHashable, Any>!
                    
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
        parameters = ["action" : "profile",
                      "userId" : String(myString)
        ]
            
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                            
                    strSuccess = JSON["status"]as Any as? String // match your success status here
                                                                                    
                    if strSuccess == String("success") {
                        ERProgressHud.sharedInstance.hide()
                        
                        var dict: Dictionary<AnyHashable, Any>
                        dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                        let defaults = UserDefaults.standard
                        defaults.setValue(dict, forKey: "keyLoginFullData")
                        
                        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginAllId")
                        self.navigationController?.pushViewController(settingsVCId, animated: false)
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
    }
    }
}


//MARK:- TABLE VIEW -
extension PDPayFiveDollar: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PDPayFiveDollarTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! PDPayFiveDollarTableCell
        //
        cell.backgroundColor = .white
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.txtCardNumber.delegate = self
        cell.txtExpDate.delegate = self
        cell.txtCVV.delegate = self
        
        cell.txtCardNumber.addTarget(self, action: #selector(PDPayFiveDollar.textFieldDidChange(_:)), for: .editingChanged)
        cell.txtExpDate.addTarget(self, action: #selector(PDPayFiveDollar.textFieldDidChange2(_:)), for: .editingChanged)
        
        return cell
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDPayFiveDollarTableCell
            
         self.lblCardNumberHeading.text! = cell.txtCardNumber.text!
        
    }
    @objc func textFieldDidChange2(_ textField: UITextField) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDPayFiveDollarTableCell
            
         self.lblEXPDate.text! = cell.txtExpDate.text!
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDPayFiveDollarTableCell
        
        if textField == cell.txtCardNumber {
            
            let first2 = String(self.lblCardNumberHeading.text!.prefix(2))
            
            if first2.count == 2 {
                // print("yes")
                
                let first3 = String(self.lblCardNumberHeading.text!.prefix(2))
                // print(first3 as Any)
                
                if first3 == "34" { // amex
                    self.imgCardImage.image = UIImage(named: "amex")
                    self.strWhatCardIam = "amex"
                } else if first3 == "37" { // amex
                    self.imgCardImage.image = UIImage(named: "amex")
                    self.strWhatCardIam = "amex"
                } else if first3 == "51" { // master
                    self.imgCardImage.image = UIImage(named: "mastercard")
                    self.strWhatCardIam = "master"
                } else if first3 == "55" { // master
                    self.imgCardImage.image = UIImage(named: "mastercard")
                    self.strWhatCardIam = "master"
                }  else if first3 == "42" { // visa
                    self.imgCardImage.image = UIImage(named: "visa")
                    self.strWhatCardIam = "visa"
                } else if first3 == "65" { // discover
                    self.imgCardImage.image = UIImage(named: "discover")
                    self.strWhatCardIam = "discover"
                } else {
                    self.imgCardImage.image = UIImage(named: "ccCard")
                    self.strWhatCardIam = "none"
                }
                
            } else {
                // print("no")
                self.imgCardImage.image = UIImage(named: "ccCard")
            }
            
            /*
            if self.lblCardNumberHeading.text!.count+1 == 2 {
                // print("check card")
                
            } else {
                // print("do not check card")
            }
            */
            
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            // print(self.strWhatCardIam as Any)
            if self.strWhatCardIam == "amex" {
                return count <= 15
            } else {
                return count <= 16
            }
            
            
        }
        
        if textField == cell.txtExpDate {
            if string == "" {
                return true
            }

            
            let currentText = textField.text! as NSString
            let updatedText = currentText.replacingCharacters(in: range, with: string)

            textField.text = updatedText
            let numberOfCharacters = updatedText.count
            
            if numberOfCharacters == 2 {
                textField.text?.append("/")
            }
            self.lblEXPDate.text! = cell.txtExpDate.text!
        }
        
       if textField == cell.txtCVV {
           
           guard let textFieldText = textField.text,
               let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                   return false
           }
           let substringToReplace = textFieldText[rangeOfTextToReplace]
           let count = textFieldText.count - substringToReplace.count + string.count
        if self.strWhatCardIam == "amex" {
            return count <= 4
        } else {
            return count <= 3
        }
       }
        
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
    
}

extension PDPayFiveDollar: UITableViewDelegate {
    
}


