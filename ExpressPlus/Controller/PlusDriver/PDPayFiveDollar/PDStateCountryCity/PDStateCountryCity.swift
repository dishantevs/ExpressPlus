//
//  PDStateCountryCity.swift
//  ExpressPlus
//
//  Created by Apple on 26/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

// MARK:- LOCATION -
import CoreLocation

class PDStateCountryCity: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {

    let cellReuseIdentifier = "pDStateCountryCityTableCell"
    
    let regularFont = UIFont.systemFont(ofSize: 16)
    let boldFont = UIFont.boldSystemFont(ofSize: 16)
    
    var arrGetCategoryName:NSMutableArray! = []
    var arrGetCategoryId:NSMutableArray! = []
    var saveCategoryIdForWebserviceParameter:String!
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var finalStateId:String!
    var arrListOfAllMyCity:NSMutableArray! = []
    var arrGetCategoryNameCity:NSMutableArray! = []
    var arrGetCategoryIdCity:NSMutableArray! = []
    var saveCategoryIdForWebserviceParameterCity:String!
    
    
    
    let locationManager = CLLocationManager()
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    var fullAddress:String!
    
    
    
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "ADDRESS"
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            // btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
    }
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(PDStateCountryCity.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
     
        self.btnBack.addTarget(self, action: #selector(sideBarMenuClickMethod), for: .touchUpInside)
        
        let alert = UIAlertController(title: String("Message"), message: String("Only US States is listed."), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
            // self.fetchingAllStateWB()
        }))
        self.present(alert, animated: true, completion: nil)
        
        self.iAmHereForLocationPermission()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // self.fetchingAllStateWB()
         
    }
    @objc func sideBarMenuClickMethod() {
        // self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        let defaults = UserDefaults.standard
        defaults.setValue("", forKey: "keyLoginFullData")
        defaults.setValue(nil, forKey: "keyLoginFullData")
               
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers

        for aViewController in viewControllers {
            if(aViewController is Welcome) {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
        
        
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    // MARK:- GET CUSTOMER LOCATION -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            //print(self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            print("locality ==> "+locality as Any) // sector 10 dwarka
            
            print(self.strSaveCountryName as Any) // india
            print(self.strSaveStateName as Any) // new delhi
            print(self.strSaveZipcodeName as Any) // 110075
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.reloadData()
            
            let indexPath = IndexPath.init(row: 0, section: 0)
            let cell = self.tbleView.cellForRow(at: indexPath) as! PDStateCountryCityTableCell
            
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                // print(person as Any)
                cell.txtAddress.text = (person["address"] as! String)
            }
            
            
            // String(self.strSaveLocality) //+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
            // cell.txtCountry.text = String(self.strSaveCountryName)
            // cell.txtState.text = String(self.strSaveStateName)
            cell.txtZipcode.text = String(self.strSaveZipcodeName)
            // cell.txtCity.text = String(locality)
            
            // self.findMyStateTaxWB()
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func pushToPaymentCheck() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDStateCountryCityTableCell
        
        if cell.txtAddress.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Address should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtState.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("State should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtCity.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("City should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtZipcode.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Zipcode should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if cell.txtPhoneNumber.text == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Phone number should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            self.makeFinalPayment()
        }
    }
    
    @objc func openAllDynamicBanklist() {
        
        self.fetchingAllStateWB()
    }
    
    @objc func fetchingAllStateWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

        // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        var parameters:Dictionary<AnyHashable, Any>!
                    
            // let x : Int = person["userId"] as! Int
            // let myString = String(x)
            
        parameters = [
            "action" : "statelist"
        ]
            
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                            
                        strSuccess = JSON["status"]as Any as? String // match your success status here
                                                                                    
                    if strSuccess == String("success") {
                        ERProgressHud.sharedInstance.hide()
                        
                        var ar : NSArray!
                        ar = (JSON["data"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                        
                        // self.tbl
                        
                        
                        
                        let indexPath = IndexPath.init(row: 0, section: 0)
                        let cell = self.tbleView.cellForRow(at: indexPath) as! PDStateCountryCityTableCell
                        
                        self.arrGetCategoryName.removeAllObjects()
                        self.arrGetCategoryId.removeAllObjects()
                        // self.arrListOfAllMyOrders.removeAllObjects()
                        
                        let redAppearance = YBTextPickerAppearanceManager.init(
                            pickerTitle         : "Please Select State",
                            titleFont           : self.boldFont,
                            titleTextColor      : .white,
                            titleBackground     : NAVIGATION_BACKGROUND_COLOR,
                            searchBarFont       : self.regularFont,
                            searchBarPlaceholder: "Search State",
                            closeButtonTitle    : "Cancel",
                            closeButtonColor    : .darkGray,
                            closeButtonFont     : self.regularFont,
                            doneButtonTitle     : "Okay",
                            doneButtonColor     : NAVIGATION_BACKGROUND_COLOR,
                            doneButtonFont      : self.boldFont,
                            checkMarkPosition   : .Right,
                            itemCheckedImage    : UIImage(named:"red_ic_checked"),
                            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
                            itemColor           : .black,
                            itemFont            : self.regularFont
                        )
                        
                        for check in 0...self.arrListOfAllMyOrders.count-1 {
                             print(check as Any)
                            
                            let item = self.arrListOfAllMyOrders[check] as? [String:Any]
                            // print(item as Any)
                            
                            let strName:String!
                            let strId:String!
                            strName = (item!["name"] as! String)
                            
                            
                            // let x : Int = item!["stateCode"] as! Int
                            // let myString = String(x)
                            strId = (item!["stateCode"] as! String)
                            
                            
                            self.arrGetCategoryName.add(strName as Any)
                            
                            self.arrGetCategoryId.add(strId as Any)
                            
                        }
                        /*
                         bankId = 2;
                         bankName = "BANK OF AMERICA";
                         created = "Oct 30th, 2019, 6:54 am";
                         image = "http://demo2.evirtualservices.com/kam-cash/site/img/uploads/bank/BOA.jpg";
                         */
                        
                        
                        let array: [String] = self.arrGetCategoryName.copy() as! [String]
                        
                        let arrGender = array
                        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
                           onCompletion: { (selectedIndexes, selectedValues) in
                            if let selectedValue = selectedValues.first{
                                if selectedValue == arrGender.last!{
                                    // cell.txtCategory.text = ""
                                    
                                    cell.txtState.text = "\(selectedValue)"
                                      
                                      let index = self.arrGetCategoryName.index(of: "\(selectedValue)");
                                    
                                      let item2 = self.arrListOfAllMyOrders[index] as? [String:Any]
                                      
                                    let x : Int = item2!["id"] as! Int
                                    let myString = String(x)
                                    print(myString as Any)
                                    self.finalStateId = myString
                                    
                                    self.allCityList(strCityIdIs: self.finalStateId)
                                    
                                      self.saveCategoryIdForWebserviceParameter = (item2!["stateCode"] as! String)
                                      // print(self.saveCategoryIdForWebserviceParameter as Any)
                                    
                                    
                                } else{
                                    cell.txtState.text = "\(selectedValue)"
                                    
                                    let index = self.arrGetCategoryName.index(of: "\(selectedValue)");
                                  
                                    let item2 = self.arrListOfAllMyOrders[index] as? [String:Any]
                                    
                                    let x : Int = item2!["id"] as! Int
                                    let myString = String(x)
                                    print(myString as Any)
                                    self.finalStateId = myString
                                    
                                    self.allCityList(strCityIdIs: self.finalStateId)
                                    
                                    self.saveCategoryIdForWebserviceParameter = (item2!["stateCode"] as! String)
                                    // print(self.saveCategoryIdForWebserviceParameter as Any)
                                    
                                }
                            } else {
                                // self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                                cell.txtState.text = "Please select type"
                            }
                        },
                           onCancel: {
                            print("Cancelled")
                        }
                        )
                        
                        picker.show(withAnimation: .FromBottom)
                        
                        
                        
                        
                        
                        
                        
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
    // }
    }
    
    
    
    
    @objc func allCityList(strCityIdIs:String) {
    
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "fetchin cities name...")
        
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

        // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        var parameters:Dictionary<AnyHashable, Any>!
                    
            // let x : Int = person["userId"] as! Int
            // let myString = String(x)
            
        parameters = [
            "action" : "citylist",
            "stateId" : String(strCityIdIs)
        ]
            
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                            
                        strSuccess = JSON["status"]as Any as? String // match your success status here
                                                                                    
                    if strSuccess == String("success") {
                        ERProgressHud.sharedInstance.hide()
                        
                        var ar : NSArray!
                        ar = (JSON["data"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyCity.addObjects(from: ar as! [Any])
                        
                        // self.tbl
                        
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
        
    }
    
    // MARK:- LIST OF ALL CITIES -
    @objc func listOfAllCities() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDStateCountryCityTableCell
        
        if cell.txtState.text == "" {
            
            let alert = UIAlertController(title: String("Error"), message: String("Please select State first."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            self.arrGetCategoryName.removeAllObjects()
            self.arrGetCategoryId.removeAllObjects()
            self.arrGetCategoryNameCity.removeAllObjects()
            
            // print(self.arrListOfAllMyCity as Any)
            
            if self.arrListOfAllMyCity.count == 0 {
                cell.btnAllCities.isHidden = true
                
                self.saveCategoryIdForWebserviceParameterCity = String(cell.txtCity.text!)
                
            } else {
                cell.btnAllCities.isHidden = false
                let redAppearance = YBTextPickerAppearanceManager.init(
                    pickerTitle         : "Please Select City",
                    titleFont           : boldFont,
                    titleTextColor      : .white,
                    titleBackground     : NAVIGATION_BACKGROUND_COLOR,
                    searchBarFont       : regularFont,
                    searchBarPlaceholder: "Search City",
                    closeButtonTitle    : "Cancel",
                    closeButtonColor    : .darkGray,
                    closeButtonFont     : regularFont,
                    doneButtonTitle     : "Okay",
                    doneButtonColor     : NAVIGATION_BACKGROUND_COLOR,
                    doneButtonFont      : boldFont,
                    checkMarkPosition   : .Right,
                    itemCheckedImage    : UIImage(named:"red_ic_checked"),
                    itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
                    itemColor           : .black,
                    itemFont            : regularFont
                )
                
                for check in 0...self.arrListOfAllMyCity.count-1 {
                     print(check as Any)
                    
                    let item = arrListOfAllMyCity[check] as? [String:Any]
                     print(item as Any)
                    
                    let strName:String!
                    let strId:String!
                    strName = (item!["name"] as! String)
                    
                    /*
                     id = 43383;
                     name = "Air Force Academy";
                     */
                    
                    let x : Int = item!["id"] as! Int
                    let myString = String(x)
                    strId = myString
                    
                    
                    self.arrGetCategoryNameCity.add(strName as Any)
                    
                    self.arrGetCategoryIdCity.add(strId as Any)
                    
                }
                /*
                 bankId = 2;
                 bankName = "BANK OF AMERICA";
                 created = "Oct 30th, 2019, 6:54 am";
                 image = "http://demo2.evirtualservices.com/kam-cash/site/img/uploads/bank/BOA.jpg";
                 */
                
                
                let array: [String] = self.arrGetCategoryNameCity.copy() as! [String]
                
                let arrGender = array
                let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
                   onCompletion: { (selectedIndexes, selectedValues) in
                    if let selectedValue = selectedValues.first{
                        if selectedValue == arrGender.last!{
                            // cell.txtCategory.text = ""
                            
                            cell.txtCity.text = "\(selectedValue)"
                              
                              let index = self.arrGetCategoryNameCity.index(of: "\(selectedValue)");
                            
                              let item2 = self.arrListOfAllMyCity[index] as? [String:Any]
                              
                            
                            
                            let x : Int = item2!["id"] as! Int
                            let myString = String(x)
                            self.saveCategoryIdForWebserviceParameterCity = String(myString)
                              
                            
                            
                        } else {
                            cell.txtCity.text = "\(selectedValue)"
                            
                            let index = self.arrGetCategoryNameCity.index(of: "\(selectedValue)");
                          
                            let item2 = self.arrListOfAllMyCity[index] as? [String:Any]
                            
                            
                            let x : Int = item2!["id"] as! Int
                            let myString = String(x)
                            self.saveCategoryIdForWebserviceParameterCity = String(myString)
                            
                            
                        }
                    } else {
                        // self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                        cell.txtCity.text = "Please select type"
                    }
                },
                   onCancel: {
                    print("Cancelled")
                }
                )
                
                picker.show(withAnimation: .FromBottom)
                
            }
            
            }
            
        
    }
    
    
    
    
}


//MARK:- TABLE VIEW -
extension PDStateCountryCity: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PDStateCountryCityTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! PDStateCountryCityTableCell
        
        cell.backgroundColor = .clear
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
            cell.txtPhoneNumber.text = (person["contactNumber"] as! String)
        }
        
        cell.btnAllCities.addTarget(self, action: #selector(listOfAllCities), for: .touchUpInside)
        cell.btnStateList.addTarget(self, action: #selector(openAllDynamicBanklist), for: .touchUpInside)
        cell.btnmakePayment.addTarget(self, action: #selector(pushToPaymentCheck), for: .touchUpInside)
        
        return cell
    }
    
    @objc func makeFinalPayment() {
        
        /*
         var fetchedAddress:String!
         var fetchedState:String!
         var fetchedCity:String!
         var fetchedZipcode:String!
         var fetchedPhoneNumber:String!
         */
        
        // print(saveCategoryIdForWebserviceParameterCity as Any)
        
        /*let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDStateCountryCityTableCell
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDPayFiveDollarId") as? PDPayFiveDollar
        
        push!.fetchedAddress        = String(cell.txtAddress.text!)
        push!.fetchedState          = String(cell.txtState.text!)
        push!.fetchedCity           = String(self.saveCategoryIdForWebserviceParameterCity)
        push!.fetchedZipcode        = String(cell.txtZipcode.text!)
        push!.fetchedPhoneNumber    = String(cell.txtPhoneNumber.text!)
        push!.fetchedStateCode      = String(self.saveCategoryIdForWebserviceParameter)
        
        self.navigationController?.pushViewController(push!, animated: true)
         
         "action"            : "cardholder",
          "userId"           : String(myString),
          "name"             : person["fullName"] as! String,
          "phoneNumber"      : String(self.fetchedPhoneNumber),
          "email"            : person["email"] as! String,
          "address"          : String(self.fetchedAddress),
          "city"             : String(self.fetchedCity),
          "state"            : String(self.fetchedStateCode),
          "country"          : String("US"),
          "zipcode"          : String(self.fetchedZipcode),
          "transactionId"    : String(transactionIdIs)
         
        */
        
        self.saveBankDetails()
    }
    
    @objc func saveBankDetails() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PDStateCountryCityTableCell
        
        self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        var parameters:Dictionary<AnyHashable, Any>!
                    
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
        parameters = [
            "action"            : "editprofile",
            "userId"           : String(myString),
            "address"          : String(cell.txtAddress.text!),
            "city"             : String(cell.txtCity.text!),
            "state"            : String(cell.txtState.text!),
            "country"          : String("US"),
            "zipcode"          : String(cell.txtZipcode.text!),
        ]
            
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String // match your success status here
                                                                                    
                    if strSuccess == String("success") {
                        
                        self.finalLoginWBB()
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
     }
    }
    
    @objc func finalLoginWBB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        var parameters:Dictionary<AnyHashable, Any>!
                    
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
        parameters = ["action" : "profile",
                      "userId" : String(myString)
        ]
            
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                            
                    strSuccess = JSON["status"]as Any as? String // match your success status here
                                                                                    
                    if strSuccess == String("success") {
                        ERProgressHud.sharedInstance.hide()
                        
                        var dict: Dictionary<AnyHashable, Any>
                        dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                        let defaults = UserDefaults.standard
                        defaults.setValue(dict, forKey: "keyLoginFullData")
                        
                        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginAllId")
                        self.navigationController?.pushViewController(settingsVCId, animated: false)
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
    }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1000
    }
    
}

extension PDStateCountryCity: UITableViewDelegate {
    
}


