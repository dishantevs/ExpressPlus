//
//  PDStateCountryCityTableCell.swift
//  ExpressPlus
//
//  Created by Apple on 26/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class PDStateCountryCityTableCell: UITableViewCell {

    @IBOutlet weak var txtAddress:UITextField! {
        didSet {
            txtAddress.layer.cornerRadius = 6
            txtAddress.clipsToBounds = true
            txtAddress.layer.borderColor = UIColor.lightGray.cgColor
            txtAddress.layer.borderWidth = 0.8
            txtAddress.backgroundColor = .white
            txtAddress.placeholder = "Address"
            txtAddress.setLeftPaddingPoints(20)
        }
    }
    @IBOutlet weak var txtZipcode:UITextField! {
        didSet {
            txtZipcode.layer.cornerRadius = 6
            txtZipcode.clipsToBounds = true
            txtZipcode.layer.borderColor = UIColor.lightGray.cgColor
            txtZipcode.layer.borderWidth = 0.8
            txtZipcode.backgroundColor = .white
            txtZipcode.placeholder = "Zipcode"
            txtZipcode.setLeftPaddingPoints(20)
        }
    }
    @IBOutlet weak var txtState:UITextField! {
        didSet {
            txtState.layer.cornerRadius = 6
            txtState.clipsToBounds = true
            txtState.layer.borderColor = UIColor.lightGray.cgColor
            txtState.layer.borderWidth = 0.8
            txtState.backgroundColor = .white
            txtState.placeholder = "State"
            txtState.setLeftPaddingPoints(20)
        }
    }
    
    @IBOutlet var btnStateList:UIButton!
    @IBOutlet weak var btnAllCities:UIButton!
    
    @IBOutlet weak var txtCity:UITextField! {
        didSet {
            txtCity.layer.cornerRadius = 6
            txtCity.clipsToBounds = true
            txtCity.layer.borderColor = UIColor.lightGray.cgColor
            txtCity.layer.borderWidth = 0.8
            txtCity.backgroundColor = .white
            txtCity.placeholder = "City"
            txtCity.setLeftPaddingPoints(20)
        }
    }
    @IBOutlet weak var txtPhoneNumber:UITextField! {
        didSet {
            txtPhoneNumber.layer.cornerRadius = 6
            txtPhoneNumber.clipsToBounds = true
            txtPhoneNumber.layer.borderColor = UIColor.lightGray.cgColor
            txtPhoneNumber.layer.borderWidth = 0.8
            txtPhoneNumber.backgroundColor = .white
            txtPhoneNumber.placeholder = "Phone Number"
            txtPhoneNumber.setLeftPaddingPoints(20)
            txtPhoneNumber.keyboardType = .phonePad
        }
    }
    @IBOutlet weak var btnmakePayment:UIButton! {
        didSet {
            btnmakePayment.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnmakePayment.setTitle("Continue & Payment", for: .normal)
            btnmakePayment.setTitleColor(.white, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
