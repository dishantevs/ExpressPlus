//
//  PDToGetDetailsOnly.swift
//  ExpressPlus
//
//  Created by Apple on 11/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire
class PDToGetDetailsOnly: UIViewController {

    var dictGetAllValueOfGetDetails:NSDictionary!
    
    var addRequestMoney:String!
    
    var dict: Dictionary<AnyHashable, Any> = [:]
    
    var andTotalProiceIs:String!
    
    var strStateTaxValueIs:String!
    var strTransactionFeeValueIs:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "TO GET DETAILS"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var lblProductNmae:UILabel!
    @IBOutlet weak var lblWhatStore:UILabel!
    @IBOutlet weak var lblEstimatedPrice:UILabel!
    @IBOutlet weak var lblTipAmount:UILabel!
    @IBOutlet weak var lblDeliveryFee:UILabel!
    @IBOutlet weak var lblSpecialNotes:UILabel!
    @IBOutlet weak var lblStateTax:UILabel!
    
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lblUserAddress:UILabel!
    
    @IBOutlet weak var lblRequestMoney:UILabel!
    
    @IBOutlet weak var btnTotalPrice:UIButton! {
        didSet {
            btnTotalPrice.backgroundColor = .systemOrange
            btnTotalPrice.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var imgProfile:UIImageView! {
        didSet {
            imgProfile.layer.cornerRadius = 30
            imgProfile.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnMarkAsDelivery:UIButton! {
        didSet {
            btnMarkAsDelivery.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnMarkAsDelivery.setTitleColor(.white, for: .normal)
            btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
        }
    }
    @IBOutlet weak var btnRequestMoreMoney:UIButton!{
        didSet {
            btnRequestMoreMoney.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnRequestMoreMoney.setTitleColor(.white, for: .normal)
            btnRequestMoreMoney.setTitle("Request more Money", for: .normal)
        }
    }
    
    @IBOutlet weak var btnRequestPayMoney:UIButton!
    var saveTransactionValueForAdd:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnRequestPayMoney.isHidden = true
        
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        
        btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        print(dictGetAllValueOfGetDetails as Any)
        
        /*
         EPrice = 1200;
         PaymentDate = "";
         RequestDate = "";
         RequestStatus = 0;
         StoreCity = "Ok ok";
         TIP = 58;
         address = "Sector 10 Dwarka New Delhi";
         created = "2020-09-14 12:00:00";
         deliveryFee = 6;
         latidude = "";
         longitude = "28.587316593949264";
         noContact = "";
         notes = Tu;
         requestMoney = 0;
         salesTax = 0;
         status = 1;
         togetrequestId = 101;
         userId = 267;
         userImage = "";
         userName = "I am customer";
         wahtUwant = Ok;
         */
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        if (person["role"] as! String) == "Driver" {
            
            imgProfile.sd_setImage(with: URL(string: (dictGetAllValueOfGetDetails["userImage"] as! String)), placeholderImage: UIImage(named: "profile"))
             
            self.lblUserName.text    = (dictGetAllValueOfGetDetails["userName"] as! String)
            self.lblUserAddress.text    = (dictGetAllValueOfGetDetails["address"] as! String)
             
            self.lblProductNmae.text    = (dictGetAllValueOfGetDetails["wahtUwant"] as! String)
            self.lblWhatStore.text      = (dictGetAllValueOfGetDetails["StoreCity"] as! String)
            self.lblEstimatedPrice.text = "$ "+(dictGetAllValueOfGetDetails["EPrice"] as! String)
            self.lblTipAmount.text      = "$ "+(dictGetAllValueOfGetDetails["TIP"] as! String)
            
            
            
             // let x : Int = (dictGetAllValueOfGetDetails["deliveryFee"] as! Int)
             // let myString = String(x)
             // self.lblDeliveryFee.text    = "$ "+String(myString)
            
            if dictGetAllValueOfGetDetails["deliveryFee"] is String {
                           
                print("Yes, it's a String")
           
                self.lblDeliveryFee.text = "$ "+(dictGetAllValueOfGetDetails["deliveryFee"] as! String)

            } else if dictGetAllValueOfGetDetails["deliveryFee"] is Int {
           
                print("It is Integer")
           
                let x2 : Int = (dictGetAllValueOfGetDetails["deliveryFee"] as! Int)
                let myString2 = String(x2)
                self.lblDeliveryFee.text = "$ "+myString2
           
            } else {
         //some other check
                print("i am ")
           
                let temp:NSNumber = dictGetAllValueOfGetDetails["deliveryFee"] as! NSNumber
                let tempString = temp.stringValue
                self.lblDeliveryFee.text = "$ "+tempString
           
            }
            
            
            
            
            
            
            
            
            
            
             self.lblSpecialNotes.text   = (dictGetAllValueOfGetDetails["notes"] as! String)
             // self.lblStateTax.text       = "$ "+(dictGetAllValueOfGetDetails["salesTax"] as! String)
             
            /*if dictGetAllValueOfGetDetails["salesTax"] is String {
                           
               print("Yes, it's a String")
               
               self.lblStateTax.text = "$ "+(dictGetAllValueOfGetDetails["salesTax"] as! String)

           } else if dictGetAllValueOfGetDetails["salesTax"] is Int {
               
               print("It is Integer")
               
               let x2 : Int = (dictGetAllValueOfGetDetails["salesTax"] as! Int)
               let myString2 = String(x2)
               self.lblStateTax.text = "$ "+myString2
               
           } else {
             //some other check
               print("i am ")
               
               let temp:NSNumber = dictGetAllValueOfGetDetails["salesTax"] as! NSNumber
               let tempString = temp.stringValue
               self.lblStateTax.text = "$ "+tempString
               
           }*/
            
            
            if dictGetAllValueOfGetDetails["salesTax"] is String {
                           
               print("Yes, it's a String")
               
               // self.lblStateTax.text = "$ "+(dictGetAllValueOfGetDetails["salesTax"] as! String)
                self.strStateTaxValueIs = (dictGetAllValueOfGetDetails["salesTax"] as! String)
           } else if dictGetAllValueOfGetDetails["salesTax"] is Int {
               
               print("It is Integer")
               
            let x2 : Int = (dictGetAllValueOfGetDetails["salesTax"] as! Int)
            let myString2 = String(x2)
               // self.lblStateTax.text = "$ "+myString2
            self.strStateTaxValueIs = myString2
           } else {
             //some other check
               print("i am ")
               
               let temp:NSNumber = dictGetAllValueOfGetDetails["salesTax"] as! NSNumber
               let tempString = temp.stringValue
               // self.lblStateTax.text = "$ "+tempString
            self.strStateTaxValueIs = tempString
           }
            
            // tf
            if dictGetAllValueOfGetDetails["transactionFee"] is String {
                           
                print("Yes, it's a String")
               
               // self.lblStateTax.text = "$ "+(dictGetAllValueOfGetDetails["transactionFee"] as! String)
                self.strTransactionFeeValueIs = (dictGetAllValueOfGetDetails["transactionFee"] as! String)
                self.saveTransactionValueForAdd = (dictGetAllValueOfGetDetails["transactionFee"] as! String)
                
           } else if dictGetAllValueOfGetDetails["transactionFee"] is Int {
               
               print("It is Integer")
               
            let x2 : Int = (dictGetAllValueOfGetDetails["transactionFee"] as! Int)
            let myString2 = String(x2)
               // self.strTransactionFeeValueIs.text = "$ "+myString2
            
            self.strStateTaxValueIs = myString2
            self.saveTransactionValueForAdd = myString2
            
           } else {
             //some other check
               print("i am ")
               
            let temp:NSNumber = dictGetAllValueOfGetDetails["transactionFee"] as! NSNumber
            let tempString = temp.stringValue
               // self.lblStateTax.text = "$ "+tempString
            self.strTransactionFeeValueIs = tempString
            self.saveTransactionValueForAdd = tempString
            
           }
            
            self.lblStateTax.text = "STATE TAX : $ "+strStateTaxValueIs+" || "+" TRANSACTION FEE : $ "+strTransactionFeeValueIs
            
            
            
             let myString1 = (dictGetAllValueOfGetDetails["EPrice"] as! String)
             let myInt1 = Int(myString1)
             
             let myString2 = (dictGetAllValueOfGetDetails["TIP"] as! String)
             let myInt2 = Int(myString2)
             
              let addPrice = myInt1
              let addTip = myInt2
             // let addDeliveryFee = (dictGetAllValueOfGetDetails["deliveryFee"] as! Int)
             
            // let addRequestMoney = (dictGetAllValueOfGetDetails["requestMoney"] as! Int)
            
            
            
            // request Money
            if dictGetAllValueOfGetDetails["requestMoney"] is String {
                                
                print("Yes, it's a String")
                
                 self.lblRequestMoney.text = "$ "+(dictGetAllValueOfGetDetails["requestMoney"] as! String)

                // let addDeliveryFee = (dictGetAllValueOfGetDetails["deliveryFee"] as! String)
                // ans2 = addPrice!+addTip!+addDeliveryFee;
                
            } else if dictGetAllValueOfGetDetails["requestMoney"] is Int {
                
                print("It is Integer")
                
                let x2 : Int = (dictGetAllValueOfGetDetails["requestMoney"] as! Int)
                let myString2 = String(x2)
                 self.lblRequestMoney.text = "$ "+myString2
                self.addRequestMoney = myString2
                
            } else {
              //some other check
                print("i am ")
                
                let temp:NSNumber = dictGetAllValueOfGetDetails["requestMoney"] as! NSNumber
                let tempString = temp.stringValue
                 self.lblRequestMoney.text = "$ "+tempString
                
                self.addRequestMoney = tempString
            }
            
            
            
            
            
            
            
            
            
            
            var ans2 = Int();
            
            if dictGetAllValueOfGetDetails["deliveryFee"] is String {
                                
                print("Yes, it's a String")
                
                self.lblDeliveryFee.text = "$ "+(dictGetAllValueOfGetDetails["deliveryFee"] as! String)

                // let addDeliveryFee = (dictGetAllValueOfGetDetails["deliveryFee"] as! String)
                // ans2 = addPrice!+addTip!+addDeliveryFee;
                
            } else if dictGetAllValueOfGetDetails["deliveryFee"] is Int {
                
                print("It is Integer")
                
                let x2 : Int = (dictGetAllValueOfGetDetails["deliveryFee"] as! Int)
                let myString2 = String(x2)
                self.lblDeliveryFee.text = "$ "+myString2
                
                let addDeliveryFee = (dictGetAllValueOfGetDetails["deliveryFee"] as! Int)
                ans2 = addPrice!+addTip!+addDeliveryFee;
            } else {
              //some other check
                print("i am ")
                
                let temp:NSNumber = dictGetAllValueOfGetDetails["deliveryFee"] as! NSNumber
                let tempString = temp.stringValue
                self.lblDeliveryFee.text = "$ "+tempString
                
                let addDeliveryFee = (dictGetAllValueOfGetDetails["deliveryFee"] as! NSNumber)
                ans2 = addPrice!+addTip!+Int(truncating: addDeliveryFee);
            }
            
            
             let x2 : Int = ans2
             let myString3 = String(x2)
             
            print(myString3 as Any)
            
            let x12 : Int = dictGetAllValueOfGetDetails["status"] as! Int
            let myString12 = String(x12)
            
            let totalPriceIs : Int = dictGetAllValueOfGetDetails["totalAmount"] as! Int
            let totalPriceInString = String(totalPriceIs)
            
            self.andTotalProiceIs = totalPriceInString
            
             if myString12 == "2" {
                 // self.btnTotalPrice.setTitle("Delivered $ "+String(myString3)+" "+totalPriceInString, for: .normal)
                
                self.btnTotalPrice.setTitle("Delivered $ "+totalPriceInString, for: .normal)
                
                 self.btnMarkAsDelivery.isHidden = true
                 self.btnRequestMoreMoney.isHidden = true
             } else {
                 // self.btnTotalPrice.setTitle("$ "+String(myString3), for: .normal)
                
                self.btnTotalPrice.setTitle("$ "+totalPriceInString, for: .normal)
                
                 self.btnMarkAsDelivery.isHidden = false
                 self.btnRequestMoreMoney.isHidden = false
                 
                 self.btnMarkAsDelivery.addTarget(self, action: #selector(markAsDelivered), for: .touchUpInside)
                 self.btnRequestMoreMoney.addTarget(self, action: #selector(requestmoreMoenyPopup), for: .touchUpInside)
             }
            
        } else {
            
            imgProfile.sd_setImage(with: URL(string: (dictGetAllValueOfGetDetails["userImage"] as! String)), placeholderImage: UIImage(named: "profile"))
             
             self.lblUserName.text    = (dictGetAllValueOfGetDetails["userName"] as! String)
             self.lblUserAddress.text    = (dictGetAllValueOfGetDetails["address"] as! String)
             
             self.lblProductNmae.text    = (dictGetAllValueOfGetDetails["wahtUwant"] as! String)
             self.lblWhatStore.text      = (dictGetAllValueOfGetDetails["StoreCity"] as! String)
             self.lblEstimatedPrice.text = "$ "+(dictGetAllValueOfGetDetails["EPrice"] as! String)
             self.lblTipAmount.text      = "$ "+(dictGetAllValueOfGetDetails["TIP"] as! String)
            
            
            
            
            if dictGetAllValueOfGetDetails["deliveryFee"] is String {
                
                print("Yes, it's a String")
                
                self.lblDeliveryFee.text = "$ "+(dictGetAllValueOfGetDetails["deliveryFee"] as! String)
 
            } else if dictGetAllValueOfGetDetails["deliveryFee"] is Int {
                
                print("It is Integer")
                
                let x2 : Int = (dictGetAllValueOfGetDetails["deliveryFee"] as! Int)
                let myString2 = String(x2)
                self.lblDeliveryFee.text = "$ "+myString2
                
            } else {
              //some other check
                print("i am ")
                
                let temp:NSNumber = dictGetAllValueOfGetDetails["deliveryFee"] as! NSNumber
                let tempString = temp.stringValue
                self.lblDeliveryFee.text = "$ "+tempString
                
            }
            
            
            
            
            
             // let x : Int = (dictGetAllValueOfGetDetails["deliveryFee"] as! Int)
             // let myString = String(x)
             // self.lblDeliveryFee.text    = "$ "+String(myString)
            
             self.lblSpecialNotes.text   = (dictGetAllValueOfGetDetails["notes"] as! String)
            
            
            
            
            /*
             var strStateTaxValueIs:String!
             var strTransactionFeeValueIs:String!
             */
            if dictGetAllValueOfGetDetails["salesTax"] is String {
                           
               print("Yes, it's a String")
               
               // self.lblStateTax.text = "$ "+(dictGetAllValueOfGetDetails["salesTax"] as! String)
                self.strStateTaxValueIs = (dictGetAllValueOfGetDetails["salesTax"] as! String)
           } else if dictGetAllValueOfGetDetails["salesTax"] is Int {
               
               print("It is Integer")
               
            let x2 : Int = (dictGetAllValueOfGetDetails["salesTax"] as! Int)
            let myString2 = String(x2)
               // self.lblStateTax.text = "$ "+myString2
            self.strStateTaxValueIs = myString2
           } else {
             //some other check
               print("i am ")
               
               let temp:NSNumber = dictGetAllValueOfGetDetails["salesTax"] as! NSNumber
               let tempString = temp.stringValue
               // self.lblStateTax.text = "$ "+tempString
            self.strStateTaxValueIs = tempString
           }
            
            // tf
            if dictGetAllValueOfGetDetails["transactionFee"] is String {
                           
                print("Yes, it's a String")
               
               // self.lblStateTax.text = "$ "+(dictGetAllValueOfGetDetails["transactionFee"] as! String)
                self.strTransactionFeeValueIs = (dictGetAllValueOfGetDetails["transactionFee"] as! String)
                self.saveTransactionValueForAdd = (dictGetAllValueOfGetDetails["transactionFee"] as! String)
                
           } else if dictGetAllValueOfGetDetails["transactionFee"] is Int {
               
               print("It is Integer")
               
            let x2 : Int = (dictGetAllValueOfGetDetails["transactionFee"] as! Int)
            let myString2 = String(x2)
               // self.strTransactionFeeValueIs.text = "$ "+myString2
            self.strStateTaxValueIs = myString2
            self.saveTransactionValueForAdd = myString2
           } else {
             //some other check
               print("i am ")
               
            let temp:NSNumber = dictGetAllValueOfGetDetails["transactionFee"] as! NSNumber
            let tempString = temp.stringValue
               // self.lblStateTax.text = "$ "+tempString
            self.strTransactionFeeValueIs = tempString
            self.saveTransactionValueForAdd = tempString
           }
            
            self.lblStateTax.text = "STATE TAX : $ "+strStateTaxValueIs+" || "+" TRANSACTION FEE : $ "+strTransactionFeeValueIs
            
            
            
            
            
            // request Money
            if dictGetAllValueOfGetDetails["requestMoney"] is String {
                                
                print("Yes, it's a String")
                
                 self.lblRequestMoney.text = "$ "+(dictGetAllValueOfGetDetails["requestMoney"] as! String)

                // let addDeliveryFee = (dictGetAllValueOfGetDetails["deliveryFee"] as! String)
                // ans2 = addPrice!+addTip!+addDeliveryFee;
                
            } else if dictGetAllValueOfGetDetails["requestMoney"] is Int {
                
                print("It is Integer")
                
                let x2 : Int = (dictGetAllValueOfGetDetails["requestMoney"] as! Int)
                let myString2 = String(x2)
                 self.lblRequestMoney.text = "$ "+myString2
                self.addRequestMoney = myString2
                
            } else {
              //some other check
                print("i am ")
                
                let temp:NSNumber = dictGetAllValueOfGetDetails["requestMoney"] as! NSNumber
                let tempString = temp.stringValue
                 self.lblRequestMoney.text = "$ "+tempString
                
                self.addRequestMoney = tempString
            }
            
            
            
            
            
            
            
            
            
            
            
             // self.lblStateTax.text       = "$ "+(dictGetAllValueOfGetDetails["salesTax"] as! String)
             
             let myString1 = (dictGetAllValueOfGetDetails["EPrice"] as! String)
             let myInt1 = Int(myString1)
             
             let myString2 = (dictGetAllValueOfGetDetails["TIP"] as! String)
             let myInt2 = Int(myString2)
             
            
            
            
            
            
             // let addPrice = myInt1
             // let addTip = myInt2
             // let addDeliveryFee = (dictGetAllValueOfGetDetails["deliveryFee"] as! Int)
             
            
            
            
            /*if dictGetAllValueOfGetDetails["salesTax"] is String {
                            
                print("Yes, it's a String")
                
                self.lblStateTax.text = "$ "+(dictGetAllValueOfGetDetails["salesTax"] as! String)

            } else if dictGetAllValueOfGetDetails["salesTax"] is Int {
                
                print("It is Integer")
                
                let x2 : Int = (dictGetAllValueOfGetDetails["salesTax"] as! Int)
                let myString2 = String(x2)
                self.lblStateTax.text = "$ "+myString2
                
            } else {
              //some other check
                print("i am ")
                
                let temp:NSNumber = dictGetAllValueOfGetDetails["salesTax"] as! NSNumber
                let tempString = temp.stringValue
                self.lblStateTax.text = "$ "+tempString
                
            }*/
            
            
            // convert delivery fee from string to int
            
            let addPrice = myInt1
            let addTip = myInt2
            
            let myString1111 = (dictGetAllValueOfGetDetails["deliveryFee"] as! String)
            let myInt1111 = Int(myString1111)
            
            
            
             var ans2 = Int();
            ans2 = addPrice!+addTip!+myInt1111!
             
             let x2 : Int = ans2
             let myString3 = String(x2)
             
            let x12 : Int = dictGetAllValueOfGetDetails["status"] as! Int
            let myString12 = String(x12)
            
            
            
            
            let addTransactionWithTotal = Double(self.saveTransactionValueForAdd)!+Double(myString3)!
            
            
            self.andTotalProiceIs = String(addTransactionWithTotal) // myString3
            print(self.andTotalProiceIs as Any)
            
            
            
            
            
            if myString12 == "2" {
                 self.btnTotalPrice.setTitle("Delivered $ "+String(myString3), for: .normal)
                 self.btnMarkAsDelivery.isHidden = true
                 self.btnRequestMoreMoney.isHidden = true
                
             } else {
                 self.btnTotalPrice.setTitle("Total Amount $ "+String(myString3), for: .normal)
                
                 self.btnMarkAsDelivery.isHidden = true
                 self.btnRequestMoreMoney.isHidden = true
                 
                 self.btnMarkAsDelivery.addTarget(self, action: #selector(markAsDelivered), for: .touchUpInside)
                 self.btnRequestMoreMoney.addTarget(self, action: #selector(requestmoreMoenyPopup), for: .touchUpInside)
             }
            
            
            
            
            }
            
        }
        self.toGetDetailsWB()
        
    
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     
     */
    @objc func markAsDelivered() {
    
        print(self.btnMarkAsDelivery.titleLabel?.text as Any)
        
        if self.btnMarkAsDelivery.titleLabel?.text == "Pay Request Money" {
            print("pay screen")
            self.payRequestMoeny()
            
        } else {
            
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
          
           let urlString = BASE_URL_EXPRESS_PLUS
          
           /*
            [action] => togetstatusupdate
            [togetRequestId] => 61
            [driverId] => 123
            [status] => 2
            */
           // foodorderId
          
          let x : Int = dictGetAllValueOfGetDetails["togetrequestId"] as! Int
          let myString = String(x)
          
              var parameters:Dictionary<AnyHashable, Any>!
              if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                  // let str:String = person["role"] as! String
                         parameters = [
                             "action"          : "togetstatusupdate",
                              "driverId"       : person["userId"] as Any,
                             "togetRequestId"  : String(myString),
                              "status"         : String("2")
                  ]
           }
                         print("parameters-------\(String(describing: parameters))")
                         
                         Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                             {
                                 response in
                     
                                 switch(response.result) {
                                 case .success(_):
                                    if let data = response.result.value {

                                     let JSON = data as! NSDictionary
                                        print(JSON as Any)
                                     
                                     var strSuccess : String!
                                     strSuccess = JSON["status"]as Any as? String
                                     
                                       // var strSuccessAlert : String!
                                       // strSuccessAlert = JSON["msg"]as Any as? String
                                     
                                     if strSuccess == String("success") {
                                      print("yes")
                                     var strSuccess2 : String!
                                     strSuccess2 = JSON["msg"]as Any as? String
                                     
                                     ERProgressHud.sharedInstance.hide()
                                     let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                     alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                         // self.dismiss(animated: true, completion: nil)
                                      self.navigationController?.popViewController(animated: true)
                                         
                                     }))
                                     self.present(alert, animated: true, completion: nil)
                                     }
                                     else {
                                      print("no")
                                       ERProgressHud.sharedInstance.hide()
                                      
                                      
                                     }
                                 }

                                 case .failure(_):
                                     print("Error message:\(String(describing: response.result.error))")
                                     
                                     ERProgressHud.sharedInstance.hide()
                                     
                                     let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                     
                                     let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                             UIAlertAction in
                                             NSLog("OK Pressed")
                                         }
                                     
                                     alertController.addAction(okAction)
                                     
                                     self.present(alertController, animated: true, completion: nil)
                                     
                                     break
                                  }
                             }
            
        }
        
        
        
         
       
          }
    
    
    
    @objc func requestmoreMoenyPopup() {
        let alertController = UIAlertController(title: "Request More Money", message: "Enter your amout", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "enter amount"
        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            self.requestMoreMoneyWB(strNewRequestMoney: firstTextField.text!)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        

        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func requestMoreMoneyWB(strNewRequestMoney:String) {
    
         ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
       
        /*
         [action] => sendmoremomeyrequest
         [togetRequestId] => 59
         [driverId] => 123
         [requestMoney] => 25
         */
        // foodorderId
       
       let x : Int = dictGetAllValueOfGetDetails["togetrequestId"] as! Int
       let myString = String(x)
       
           var parameters:Dictionary<AnyHashable, Any>!
           if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
               // let str:String = person["role"] as! String
            if strNewRequestMoney == "" {
                parameters = [
                           "action"          : "sendmoremomeyrequest",
                            "driverId"       : person["userId"] as Any,
                           "togetRequestId"  : String(myString),
                            "requestMoney"         : String("0")
                ]
            } else {
                parameters = [
                           "action"          : "sendmoremomeyrequest",
                            "driverId"       : person["userId"] as Any,
                           "togetRequestId"  : String(myString),
                            "requestMoney"         : String(strNewRequestMoney)
                ]
            }
                      
        }
                      print("parameters-------\(String(describing: parameters))")
                      
                      Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                          {
                              response in
                  
                              switch(response.result) {
                              case .success(_):
                                 if let data = response.result.value {

                                  let JSON = data as! NSDictionary
                                     print(JSON as Any)
                                  
                                  var strSuccess : String!
                                  strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                                  if strSuccess == String("success") {
                                   print("yes")
                                  var strSuccess2 : String!
                                  strSuccess2 = JSON["msg"]as Any as? String
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                  alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                      // self.dismiss(animated: true, completion: nil)
                                   self.navigationController?.popViewController(animated: true)
                                      
                                  }))
                                  self.present(alert, animated: true, completion: nil)
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                               }
                          }
       
          }
    
    
    
    
    @objc func toGetDetailsWB() {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
      
       let urlString = BASE_URL_EXPRESS_PLUS
      
       /*
         [action] => togetdetils
             [togetrequestId] => 252
        */
       // foodorderId
      
      let x : Int = dictGetAllValueOfGetDetails["togetrequestId"] as! Int
      let myString = String(x)
      
        var parameters:Dictionary<AnyHashable, Any>!
        // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
              // let str:String = person["role"] as! String
           
            parameters = [
                          "action"          : "togetdetils",
                          "togetrequestId"  : String(myString),
               ]
           
                     
       
        print("parameters-------\(String(describing: parameters))")
                     
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
        {
            response in
                 
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                 
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                                 
                                   // var strSuccessAlert : String!
                                   // strSuccessAlert = JSON["msg"]as Any as? String
                                 
                    if strSuccess == String("success") {
                        print("yes")
                        var strSuccess2 : String!
                        strSuccess2 = JSON["msg"]as Any as? String
                                 
                        ERProgressHud.sharedInstance.hide()
                                
                        
                        
                        
                        self.dict = JSON["historyList"] as! Dictionary<AnyHashable, Any>
                        
                        
                        // member
/* ////////////////////////////////////////////////////////////////////////////////////////////////  */
           
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        if person["role"] as! String == "Member" {
                            
        let x : Int = self.dict["status"] as! Int
        let myString = String(x)

                        
           // pending status
           if self.dict["paymentStatus"] is String {
                                                       
               print("Yes, it's a String")
               
               // self.btnPaymentAccess.setTitle((self.dict["paymentStatus"] as! String), for: .normal)
               
               if (self.dict["paymentStatus"] as! String) == "0" {
                   
                self.btnTotalPrice.isHidden = false
                self.btnTotalPrice.setTitle("Driver not Accept yet", for: .normal)
                // self.btnReview.isHidden = true
                
               } else if (self.dict["paymentStatus"] as! String) == "1" {
                   
                self.btnTotalPrice.isHidden = false
                self.btnTotalPrice.setTitle("Confirm & Pay $ "+self.andTotalProiceIs, for: .normal)
                self.btnTotalPrice.backgroundColor = .systemGreen
                     self.btnTotalPrice.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                // self.btnReview.isHidden = true
                
               } else if (self.dict["paymentStatus"] as! String) == "2" {
                   
                self.btnTotalPrice.isHidden = false
                self.btnTotalPrice.setTitle("Payment Done ( Order not Delivered yet )", for: .normal)
                self.btnTotalPrice.setTitleColor(.white, for: .normal)
                self.btnTotalPrice.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                
                if String(myString) == "2" {
                    
                    // self.btnReview.isHidden = false
                    self.btnTotalPrice.setTitle("Delivered", for: .normal)
                    
                } else {
                    
                    // self.btnReview.isHidden = true
                    
                    
                    let x : Int = self.dict["RequestStatus"] as! Int
                    let myString = String(x)
                    
                    if myString == "1" {
                        print("request more money")
                    } else {
                        print("request more money")
                    }
                    
                    
                }
                
                
                
                
               }
               
               
               // 2 = complete
               
               
               
               
               
               
               
               
               
               
               
               
               
           } else if self.dict["paymentStatus"] is Int {
               
               print("It is Integer")
               let x2 : Int = (self.dict["paymentStatus"] as! Int)
               let myString2 = String(x2)
               // self.btnPaymentAccess.setTitle((myString2), for: .normal)
               
               
               
               if myString2 == "0" {
                   
                   self.btnTotalPrice.isHidden = false
                   self.btnTotalPrice.setTitle("Driver not Accept yet", for: .normal)
                   
               } else if myString2 == "1" {
                   
                self.btnTotalPrice.isHidden = false
                self.btnTotalPrice.setTitle("Confirm & Pay $"+self.andTotalProiceIs, for: .normal)
                 self.btnTotalPrice.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                self.btnTotalPrice.backgroundColor = .systemGreen
                
                // self.btnReview.isHidden = true
                
               } else if myString2 == "2" {
                   
                self.btnTotalPrice.isHidden = false
                self.btnTotalPrice.setTitle("Payment Done ( Order not Delivered yet )", for: .normal)
                self.btnTotalPrice.setTitleColor(.white, for: .normal)
                self.btnTotalPrice.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                
                
                if String(myString) == "2" {
                    
                     self.btnTotalPrice.setTitle("Delivered", for: .normal)
                    
                } else {
                    
                    // self.btnReview.isHidden = true
                    
                    // let x : Int = self.dict["RequestStatus"] as! Int
                    // let myString = String(x)
                    
                    
                    if self.dict["RequestStatus"] is String {
                                      
                        print("Yes, it's a String")
                      
                        // self.lblDeliveryFee.text = "$ "+(dictGetSpecialOrderData["deliveryFee"] as! String)

                        if (self.dict["RequestStatus"] as! String) == "1" {
                            
                            // btnRequestPayMoney
                            print("request more money")
                            
                            
                            self.btnRequestPayMoney.isHidden = false
                            self.btnRequestPayMoney.backgroundColor = .systemOrange
                            self.btnRequestPayMoney.setTitle("Pay Request Money", for: .normal)
                            self.btnRequestPayMoney.addTarget(self, action: #selector(self.payRequestMoeny), for: .touchUpInside)
                            
                            // self.btnMarkAsDelivery.isHidden = false
                            // self.btnMarkAsDelivery.backgroundColor = .systemGreen
                            // self.btnMarkAsDelivery.setTitle("Pay Request Money", for: .normal)
                            
                            // self.btnRequestMoreMoney.isHidden = false
                            // self.btnRequestMoreMoney.setTitle("", for: .normal)
                            // self.btnRequestMoreMoney.backgroundColor = .lightGray
                            // self.btnRequestMoreMoney.isUserInteractionEnabled = false
                            
                        } else {
                            
                            self.btnMarkAsDelivery.isHidden = true
                            self.btnRequestMoreMoney.isHidden = true
                            self.btnRequestMoreMoney.isUserInteractionEnabled = false
                            
                            print("request no more money")
                            
                        }
                        
                        
                    } else if self.dict["RequestStatus"] is Int {
                      
                        print("It is Integer")
                      
                         let x2 : Int = (self.dict["RequestStatus"] as! Int)
                         let myString2 = String(x2)
                        // self.lblDeliveryFee.text = "$ "+myString2
                      
                        if myString2 == "1" {
                            
                            self.btnRequestPayMoney.isHidden = false
                            self.btnRequestPayMoney.backgroundColor = .systemOrange
                            self.btnRequestPayMoney.setTitle("Pay Request Money", for: .normal)
                            self.btnRequestPayMoney.addTarget(self, action: #selector(self.payRequestMoeny), for: .touchUpInside)
                            
                        } else {
                            
                            self.btnMarkAsDelivery.isHidden = true
                            self.btnRequestMoreMoney.isHidden = true
                            self.btnRequestMoreMoney.isUserInteractionEnabled = false
                            
                            print("request no more money")
                            
                        }
                        
                    } else {
                    
                      print("i am ")
                      
                         let temp:NSNumber = self.dict["RequestStatus"] as! NSNumber
                         let tempString = temp.stringValue
                        // self.lblDeliveryFee.text = "$ "+tempString
                      
                        
                        if tempString == "1" {
                            
                            self.btnRequestPayMoney.isHidden = false
                            self.btnRequestPayMoney.backgroundColor = .systemOrange
                            self.btnRequestPayMoney.setTitle("Pay Request Money", for: .normal)
                            self.btnRequestPayMoney.addTarget(self, action: #selector(self.payRequestMoeny), for: .touchUpInside)
                            
                        } else {
                            
                            self.btnMarkAsDelivery.isHidden = true
                            self.btnRequestMoreMoney.isHidden = true
                            self.btnRequestMoreMoney.isUserInteractionEnabled = false
                            
                            print("request no more money")
                            
                        }
                        
                    }
                    
                }
                
               }
               
           } else {
               
               print("i am ")
               let temp:NSNumber = self.dict["paymentStatus"] as! NSNumber
               let tempString = temp.stringValue
               // self.btnPaymentAccess.setTitle((tempString), for: .normal)
               
               
               if tempString == "0" {
                   
                   self.btnTotalPrice.isHidden = false
                   self.btnTotalPrice.setTitle("Driver not Accept yet", for: .normal)
                   
               } else if tempString == "1" {
                   
                   self.btnTotalPrice.isHidden = false
                   self.btnTotalPrice.setTitle("Confirm & Pay $ "+self.andTotalProiceIs, for: .normal)
                     self.btnTotalPrice.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                self.btnTotalPrice.backgroundColor = .systemGreen
                
               } else if tempString == "2" {
                   
                self.btnTotalPrice.isHidden = false
                self.btnTotalPrice.setTitle("Payment Done ( Order not Delivered yet )", for: .normal)
                self.btnTotalPrice.setTitleColor(.white, for: .normal)
                self.btnTotalPrice.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                
                
                
                
                
                
                
                
                if String(myString) == "2" {
                    
                     self.btnTotalPrice.setTitle("Delivered", for: .normal)
                    
                } else {
                    
                    let x : Int = self.dict["RequestStatus"] as! Int
                    let myString = String(x)
                    
                    if myString == "1" {
                        print("request more money")
                    } else {
                        print("request more money")
                    }
                    
                    
                    // self.btnReview.isHidden = true
                    /*
                     [action] => moremoneypaymentupdate
                         [userId] => 367
                         [togetRequestId] => 262
                         [transactionID] => tok_1Hf3FEIudZlr53ucyrv6CkBF
                     */
                }
                
                
                
                
                
                
                
                
                
                
                
                
                
                
               }
               
               
           }
           
           
                        } // member ends here
        }
           
           
/* ////////////////////////////////////////////////////////////////////////////////////////////////  */

                        
        
                        
                        
                        
                        
                        
                        // driver
/* ////////////////////////////////////////////////////////////////////////////////////////////////  */
           
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        if person["role"] as! String == "Driver" {
                            
        let x : Int = self.dict["status"] as! Int
        let myString = String(x)

                        
           // pending status
           if self.dict["paymentStatus"] is String {
                                                       
               print("Yes, it's a String")
               
               // self.btnPaymentAccess.setTitle((self.dict["paymentStatus"] as! String), for: .normal)
               
               if (self.dict["paymentStatus"] as! String) == "0" {
                   
                self.btnTotalPrice.isHidden = false
                self.btnTotalPrice.setTitle("Order Not Accept", for: .normal)
                // self.btnReview.isHidden = true
                
               } else if (self.dict["paymentStatus"] as! String) == "1" {
                   
                self.btnTotalPrice.isHidden = false
                self.btnTotalPrice.setTitle("Payment not Done $ "+self.andTotalProiceIs, for: .normal)
                self.btnTotalPrice.backgroundColor = .systemRed
                     // self.btnTotalPrice.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                // self.btnReview.isHidden = true
                
                self.btnMarkAsDelivery.isHidden = true
                self.btnRequestMoreMoney.isHidden = true
                
               } else if (self.dict["paymentStatus"] as! String) == "2" {
                   
                self.btnTotalPrice.isHidden = false
                self.btnTotalPrice.setTitle("Payment Done ( Order not Delivered yet )", for: .normal)
                self.btnTotalPrice.setTitleColor(.white, for: .normal)
                self.btnTotalPrice.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                self.btnTotalPrice.backgroundColor = .systemOrange
                
                self.btnMarkAsDelivery.isHidden = false
                self.btnRequestMoreMoney.isHidden = false
                
                if String(myString) == "2" {
                    
                    // self.btnReview.isHidden = false
                    self.btnTotalPrice.setTitle("Delivered", for: .normal)
                    self.btnMarkAsDelivery.isHidden = true
                    self.btnRequestMoreMoney.isHidden = true
                    
                } else {
                    
                    // self.btnReview.isHidden = true
                    
                    
                    
                    // 2 = already requested
                    if self.dict["RequestStatus"] is String {
                                      
                        print("Yes, it's a String")
                      
                        // self.lblDeliveryFee.text = "$ "+(dictGetSpecialOrderData["deliveryFee"] as! String)

                        if (self.dict["RequestStatus"] as! String) == "2" {
                            
                            // btnRequestPayMoney
                            print("Already requested")
                            
                            self.btnRequestMoreMoney.setTitle("Already Requested", for: .normal)
                            self.btnRequestMoreMoney.backgroundColor = .systemGray
                            self.btnRequestMoreMoney.isUserInteractionEnabled = false
                        }
                        
                        
                    } else if self.dict["RequestStatus"] is Int {
                      
                        print("It is Integer")
                      
                         let x2 : Int = (self.dict["RequestStatus"] as! Int)
                         let myString2 = String(x2)
                        // self.lblDeliveryFee.text = "$ "+myString2
                      
                        if myString2 == "2" {
                            
                            self.btnRequestMoreMoney.setTitle("Already Requested", for: .normal)
                            self.btnRequestMoreMoney.backgroundColor = .systemGray
                            self.btnRequestMoreMoney.isUserInteractionEnabled = false
                        }
                        
                    } else {
                    
                      print("i am ")
                      
                         let temp:NSNumber = self.dict["RequestStatus"] as! NSNumber
                         let tempString = temp.stringValue
                        // self.lblDeliveryFee.text = "$ "+tempString
                      
                        
                        if tempString == "2" {
                            
                            self.btnRequestMoreMoney.setTitle("Already Requested", for: .normal)
                            self.btnRequestMoreMoney.backgroundColor = .systemGray
                            self.btnRequestMoreMoney.isUserInteractionEnabled = false
                        }
                        
                    }
                }
                
                
               }
               
               
               // 2 = complete
               
               
               
               
               
               
               
               
               
               
               
               
               
           } else if self.dict["paymentStatus"] is Int {
               
               print("It is Integer")
               let x2 : Int = (self.dict["paymentStatus"] as! Int)
               let myString2 = String(x2)
               // self.btnPaymentAccess.setTitle((myString2), for: .normal)
               
               
               
               if myString2 == "0" {
                   
                   self.btnTotalPrice.isHidden = false
                   self.btnTotalPrice.setTitle("Order Not Accept", for: .normal)
                   
               } else if myString2 == "1" {
                   
                self.btnTotalPrice.isHidden = false
                self.btnTotalPrice.setTitle("Payment Not Done $"+self.andTotalProiceIs, for: .normal)
                 // self.btnTotalPrice.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                self.btnTotalPrice.backgroundColor = .systemRed
                
                // self.btnReview.isHidden = true
                
                self.btnMarkAsDelivery.isHidden = true
                self.btnRequestMoreMoney.isHidden = true
                
               } else if myString2 == "2" {
                   
                self.btnTotalPrice.isHidden = false
                self.btnTotalPrice.setTitle("Payment Done ( Order not Delivered yet )", for: .normal)
                self.btnTotalPrice.setTitleColor(.white, for: .normal)
                self.btnTotalPrice.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                self.btnTotalPrice.backgroundColor = .systemOrange
                
                self.btnMarkAsDelivery.isHidden = false
                self.btnRequestMoreMoney.isHidden = false
                
                if String(myString) == "2" {
                    
                     self.btnTotalPrice.setTitle("Delivered", for: .normal)
                    self.btnMarkAsDelivery.isHidden = true
                    self.btnRequestMoreMoney.isHidden = true
                    
                } else {
                    
                    // self.btnReview.isHidden = true
                    if self.dict["RequestStatus"] is String {
                                      
                        print("Yes, it's a String")
                      
                        // self.lblDeliveryFee.text = "$ "+(dictGetSpecialOrderData["deliveryFee"] as! String)

                        if (self.dict["RequestStatus"] as! String) == "2" {
                            
                            // btnRequestPayMoney
                            print("Already requested")
                            
                            self.btnRequestMoreMoney.setTitle("Already Requested", for: .normal)
                            self.btnRequestMoreMoney.backgroundColor = .systemGray
                            self.btnRequestMoreMoney.isUserInteractionEnabled = false
                        }
                        
                        
                    } else if self.dict["RequestStatus"] is Int {
                      
                        print("It is Integer")
                      
                         let x2 : Int = (self.dict["RequestStatus"] as! Int)
                         let myString2 = String(x2)
                        // self.lblDeliveryFee.text = "$ "+myString2
                      
                        if myString2 == "2" {
                            
                            self.btnRequestMoreMoney.setTitle("Already Requested", for: .normal)
                            self.btnRequestMoreMoney.backgroundColor = .systemGray
                            self.btnRequestMoreMoney.isUserInteractionEnabled = false
                        }
                        
                    } else {
                    
                      print("i am ")
                      
                         let temp:NSNumber = self.dict["RequestStatus"] as! NSNumber
                         let tempString = temp.stringValue
                        // self.lblDeliveryFee.text = "$ "+tempString
                      
                        
                        if tempString == "2" {
                            
                            self.btnRequestMoreMoney.setTitle("Already Requested", for: .normal)
                            self.btnRequestMoreMoney.backgroundColor = .systemGray
                            self.btnRequestMoreMoney.isUserInteractionEnabled = false
                        }
                        
                    }
                }
                
                
                
                
               }
               
           } else {
               
               print("i am ")
               let temp:NSNumber = self.dict["paymentStatus"] as! NSNumber
               let tempString = temp.stringValue
               // self.btnPaymentAccess.setTitle((tempString), for: .normal)
               
               
               if tempString == "0" {
                   
                   self.btnTotalPrice.isHidden = false
                   self.btnTotalPrice.setTitle("Order Not Accept", for: .normal)
                   
               } else if tempString == "1" {
                   
                   self.btnTotalPrice.isHidden = false
                   self.btnTotalPrice.setTitle("Payment Not Done $ "+self.andTotalProiceIs, for: .normal)
                     // self.btnTotalPrice.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
                self.btnTotalPrice.backgroundColor = .systemRed
                
                
                self.btnMarkAsDelivery.isHidden = true
                self.btnRequestMoreMoney.isHidden = true
                
               } else if tempString == "2" {
                   
                self.btnTotalPrice.isHidden = false
                self.btnTotalPrice.setTitle("Payment Done ( Order not Delivered yet )", for: .normal)
                self.btnTotalPrice.setTitleColor(.white, for: .normal)
                self.btnTotalPrice.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
                self.btnTotalPrice.backgroundColor = .systemOrange
                
                self.btnMarkAsDelivery.isHidden = false
                self.btnRequestMoreMoney.isHidden = false
                
                if String(myString) == "2" {
                    
                     self.btnTotalPrice.setTitle("Delivered", for: .normal)
                    self.btnMarkAsDelivery.isHidden = true
                    self.btnRequestMoreMoney.isHidden = true
                    
                } else {
                    
                    // self.btnReview.isHidden = true
                    if self.dict["RequestStatus"] is String {
                                      
                        print("Yes, it's a String")
                      
                        // self.lblDeliveryFee.text = "$ "+(dictGetSpecialOrderData["deliveryFee"] as! String)

                        if (self.dict["RequestStatus"] as! String) == "2" {
                            
                            // btnRequestPayMoney
                            print("Already requested")
                            
                            self.btnRequestMoreMoney.setTitle("Already Requested", for: .normal)
                            self.btnRequestMoreMoney.backgroundColor = .systemGray
                            self.btnRequestMoreMoney.isUserInteractionEnabled = false
                        }
                        
                        
                    } else if self.dict["RequestStatus"] is Int {
                      
                        print("It is Integer")
                      
                         let x2 : Int = (self.dict["RequestStatus"] as! Int)
                         let myString2 = String(x2)
                        // self.lblDeliveryFee.text = "$ "+myString2
                      
                        if myString2 == "2" {
                            
                            self.btnRequestMoreMoney.setTitle("Already Requested", for: .normal)
                            self.btnRequestMoreMoney.backgroundColor = .systemGray
                            self.btnRequestMoreMoney.isUserInteractionEnabled = false
                        }
                        
                    } else {
                    
                      print("i am ")
                      
                         let temp:NSNumber = self.dict["RequestStatus"] as! NSNumber
                         let tempString = temp.stringValue
                        // self.lblDeliveryFee.text = "$ "+tempString
                      
                        
                        if tempString == "2" {
                            
                            self.btnRequestMoreMoney.setTitle("Already Requested", for: .normal)
                            self.btnRequestMoreMoney.backgroundColor = .systemGray
                            self.btnRequestMoreMoney.isUserInteractionEnabled = false
                        }
                        
                    }
                }
                
                
                
               }
               
               
           }
           
           
                        } // driver ends here
        }
           
           
/* ////////////////////////////////////////////////////////////////////////////////////////////////  */

                        
                        
                        
                        
                        
                        
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                                 
                ERProgressHud.sharedInstance.hide()
                                 
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                 
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                         UIAlertAction in
                    NSLog("OK Pressed")
                }
                                 
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion: nil)
                                 
                break
            }
        }
    }
         // }
    
    
    
    @objc func payRequestMoeny() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPPayemtnScreenForFoodSectionId") as? UPPayemtnScreenForFoodSection
        
        // TO GET REQUEST ID
        if self.dict["togetRequestId"] is String {
                            
            print("Yes, it's a String")
            push!.toGetRequestId = (self.dict["togetRequestId"] as! String)
            
        } else if self.dict["togetRequestId"] is Int {
            
            print("It is Integer")
            let x2 : Int = (self.dict["togetRequestId"] as! Int)
            let myString2 = String(x2)
            push!.toGetRequestId = String(myString2)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = self.dict["togetRequestId"] as! NSNumber
            let tempString = temp.stringValue
            push!.toGetRequestId = String(tempString)
            
        }
        
        // REQUEST MONEY
        if self.dict["requestMoney"] is String {
                            
            print("Yes, it's a String")
            push!.requestMoney = (self.dict["requestMoney"] as! String)
            
        } else if self.dict["requestMoney"] is Int {
            
            print("It is Integer")
            let x2 : Int = (self.dict["requestMoney"] as! Int)
            let myString2 = String(x2)
            push!.requestMoney = String(myString2)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = self.dict["requestMoney"] as! NSNumber
            let tempString = temp.stringValue
            push!.requestMoney = String(tempString)
            
        }
        
        
        
        
        
        push!.paymentFoodOrService = "iAmFromPayRequestMoney"
        push!.enableBackOrNot = "yes"
        
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    
    @objc func confirmAndPay() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPPayemtnScreenForFoodSectionId") as? UPPayemtnScreenForFoodSection
        
        // strGetAdminAmount
        if self.dict["AdminAmount"] is String {
                            
            print("Yes, it's a String")
            // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
            // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
            push!.strGetAdminAmount = (self.dict["AdminAmount"] as! String)
            
        } else if self.dict["AdminAmount"] is Int {
            
            print("It is Integer")
            let x2 : Int = (self.dict["AdminAmount"] as! Int)
            let myString2 = String(x2)
            // self.changesBookingIdis = String(myString2)
            // push!.getTotalPriceOfFood = String(myString2)
            push!.strGetAdminAmount = String(myString2)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = self.dict["AdminAmount"] as! NSNumber
            let tempString = temp.stringValue
            // self.changesBookingIdis = String(tempString)
            // push!.getTotalPriceOfFood = String(tempString)
            push!.strGetAdminAmount = String(tempString)
            
        }
        
       // TO GET REQUEST ID
       if self.dict["togetRequestId"] is String {
                           
           print("Yes, it's a String")
           push!.toGetRequestId = (self.dict["togetRequestId"] as! String)
           
       } else if self.dict["togetRequestId"] is Int {
           
           print("It is Integer")
           let x2 : Int = (self.dict["togetRequestId"] as! Int)
           let myString2 = String(x2)
           push!.toGetRequestId = String(myString2)
           
       } else {
           
           print("i am ")
           let temp:NSNumber = self.dict["togetRequestId"] as! NSNumber
           let tempString = temp.stringValue
           push!.toGetRequestId = String(tempString)
           
       }
       
       
       
       
       // TOTAL AMOUNT
       if self.dict["TIP"] is String {
                           
           print("Yes, it's a String")
           push!.toGetTipAmount = (self.dict["TIP"] as! String)
           
       } else if self.dict["TIP"] is Int {
           
           print("It is Integer")
           let x2 : Int = (self.dict["TIP"] as! Int)
           let myString2 = String(x2)
           push!.toGetTipAmount = String(myString2)
           
       } else {
           
           print("i am ")
           let temp:NSNumber = self.dict["TIP"] as! NSNumber
           let tempString = temp.stringValue
           push!.toGetTipAmount = String(tempString)
           
       }
       
        push!.toGetTotalAmount = String(self.andTotalProiceIs)
        
        push!.paymentFoodOrService = "iAmFromToGet"
        push!.enableBackOrNot = "yes"
        
        push!.strAccountNumberIs = (self.dict["driverStripeAccount"] as! String)
        
       self.navigationController?.pushViewController(push!, animated: true)
       
        
    }
}
