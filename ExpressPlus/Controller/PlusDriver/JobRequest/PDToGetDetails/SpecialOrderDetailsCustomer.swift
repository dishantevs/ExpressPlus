//
//  SpecialOrderDetailsCustomer.swift
//  ExpressPlus
//
//  Created by Apple on 18/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class SpecialOrderDetailsCustomer: UIViewController {

    var dictGetSpecialOrderData:NSDictionary!
    
    @IBOutlet weak var lblProductNmae:UILabel!
    @IBOutlet weak var lblEstimatedPrice:UILabel!
    @IBOutlet weak var lblTipAmount:UILabel!
    @IBOutlet weak var lblDeliveryFee:UILabel!
    @IBOutlet weak var lblStateTax:UILabel!
    
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lblUserAddress:UILabel!
    
    var myString12 : String!
    var totalPriceInString : String!
    
    var andTotalProiceIs:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "SPECIAL ORDER"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    
    @IBOutlet weak var imgProfile:UIImageView! {
        didSet {
            imgProfile.layer.cornerRadius = 30
            imgProfile.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnMarkAsDelivery:UIButton! {
        didSet {
            
            btnMarkAsDelivery.layer.cornerRadius = 8
            btnMarkAsDelivery.clipsToBounds = true
            
            btnMarkAsDelivery.backgroundColor = .systemGreen
            btnMarkAsDelivery.setTitleColor(.white, for: .normal)
            btnMarkAsDelivery.setTitle("PENDING", for: .normal)
        }
    }
    @IBOutlet weak var btnRequestMoreMoney:UIButton!{
        didSet {
            
            btnRequestMoreMoney.layer.cornerRadius = 8
            btnRequestMoreMoney.clipsToBounds = true
            
            btnRequestMoreMoney.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnRequestMoreMoney.setTitleColor(.white, for: .normal)
            btnRequestMoreMoney.setTitle("Request more Money", for: .normal)
        }
    }
    
    
    @IBOutlet weak var btnTotalPrice:UIButton! {
        didSet {
            btnTotalPrice.backgroundColor = .systemOrange
            btnTotalPrice.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var lblTransactionFee:UILabel!
    
    var strFinalTransactionFeeIsForAdd:String!
    var strFinalTotalAmountIsForAdd:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(dictGetSpecialOrderData as Any)
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        /*
         TIP = 10;
         address = "Sector 10 Dwarka South West Delhi";
         created = "2020-10-22 11:56:00";
         deliveryFee = 6;
         driverId = 372;
         driverImage = "https://www.expressplusnow.com/appbackend/img/uploads/users/1603183585123269927912595.png";
         driverName = ddriver;
         driverPhone = 5689562352;
         latitude = "28.587283865546446";
         longitude = "77.06087227247546";
         paymentStatus = 1;
         price = 800;
         salesTax = 0;
         specialrequestId = 60;
         status = 1;
         totalAmount = 816;
         transaction = "";
         userId = 367;
         userImage = "https://www.expressplusnow.com/appbackend/img/uploads/users/1603183526expressPlusEditProfilePhoto.jpg";
         userName = user;
         userPhone = 9696986532;
         whatYouWant = Hdhd;
         transactionFee = "1.93";
         */
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
               if (person["role"] as! String) == "Driver" {
                   
                if dictGetSpecialOrderData["transactionFee"] is String {
                                   
                    print("Yes, it's a String")
                       
                    self.lblTransactionFee.text = "$ "+(dictGetSpecialOrderData["transactionFee"] as! String)
                    self.strFinalTransactionFeeIsForAdd = (dictGetSpecialOrderData["transactionFee"] as! String)
                    
                } else if dictGetSpecialOrderData["transactionFee"] is Int {
                       
                    print("It is Integer")
                       
                    let x2 : Int = (dictGetSpecialOrderData["transactionFee"] as! Int)
                    let myString2 = String(x2)
                    self.lblTransactionFee.text = "$ "+myString2
                    self.strFinalTransactionFeeIsForAdd = myString2
                    // self.lblTransactionFee.text = "$ "+myString2
                    
                } else {
                     //some other check
                    print("i am ")
                       
                    let temp:NSNumber = dictGetSpecialOrderData["transactionFee"] as! NSNumber
                    let tempString = temp.stringValue
                    self.lblTransactionFee.text = "$ "+tempString
                    self.strFinalTransactionFeeIsForAdd = tempString
                    
                }
                
                imgProfile.sd_setImage(with: URL(string: (dictGetSpecialOrderData["userImage"] as! String)), placeholderImage: UIImage(named: "profile"))
                    
                self.lblUserName.text    = (dictGetSpecialOrderData["userName"] as! String)
                self.lblUserAddress.text    = (dictGetSpecialOrderData["address"] as! String)
                    
                self.lblProductNmae.text    = (dictGetSpecialOrderData["whatYouWant"] as! String)
                self.lblEstimatedPrice.text = "$ "+(dictGetSpecialOrderData["price"] as! String)
                self.lblTipAmount.text      = "$ "+(dictGetSpecialOrderData["TIP"] as! String)
                   
                   
                   
                    // let x : Int = (dictGetSpecialOrderData["deliveryFee"] as! Int)
                    // let myString = String(x)
                    // self.lblDeliveryFee.text    = "$ "+String(myString)
                   
                if dictGetSpecialOrderData["deliveryFee"] is String {
                                  
                    print("Yes, it's a String")
                  
                    self.lblDeliveryFee.text = "$ "+(dictGetSpecialOrderData["deliveryFee"] as! String)

                } else if dictGetSpecialOrderData["deliveryFee"] is Int {
                  
                    print("It is Integer")
                  
                    let x2 : Int = (dictGetSpecialOrderData["deliveryFee"] as! Int)
                    let myString2 = String(x2)
                    self.lblDeliveryFee.text = "$ "+myString2
                  
                } else {
                //some other check
                  print("i am ")
                  
                    let temp:NSNumber = dictGetSpecialOrderData["deliveryFee"] as! NSNumber
                    let tempString = temp.stringValue
                    self.lblDeliveryFee.text = "$ "+tempString
                  
                }
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                self.lblStateTax.text       = "$ "+(dictGetSpecialOrderData["salesTax"] as! String)
                    
                let myString1 = (dictGetSpecialOrderData["price"] as! String)
                let myInt1 = Int(myString1)
                    
                let myString2 = (dictGetSpecialOrderData["TIP"] as! String)
                let myInt2 = Int(myString2)
                    
                let addPrice = myInt1
                let addTip = myInt2
                    // let addDeliveryFee = (dictGetSpecialOrderData["deliveryFee"] as! Int)
                    
                   
                   
                var ans2 = Int()
                   
                if dictGetSpecialOrderData["deliveryFee"] is String {
                                       
                    print("Yes, it's a String")
                       
                    self.lblDeliveryFee.text = "$ "+(dictGetSpecialOrderData["deliveryFee"] as! String)

                       // let addDeliveryFee = (dictGetSpecialOrderData["deliveryFee"] as! String)
                       // ans2 = addPrice!+addTip!+addDeliveryFee;
                       
                } else if dictGetSpecialOrderData["deliveryFee"] is Int {
                       
                    print("It is Integer")
                       
                    let x2 : Int = (dictGetSpecialOrderData["deliveryFee"] as! Int)
                    let myString2 = String(x2)
                    self.lblDeliveryFee.text = "$ "+myString2
                       
                    let addDeliveryFee = (dictGetSpecialOrderData["deliveryFee"] as! Int)
                    ans2 = addPrice!+addTip!+addDeliveryFee;
                } else {
                     //some other check
                    print("i am ")
                       
                       let temp:NSNumber = dictGetSpecialOrderData["deliveryFee"] as! NSNumber
                       let tempString = temp.stringValue
                       self.lblDeliveryFee.text = "$ "+tempString
                       
                       let addDeliveryFee = (dictGetSpecialOrderData["deliveryFee"] as! NSNumber)
                       ans2 = addPrice!+addTip!+Int(truncating: addDeliveryFee);
                   }
                   
                   
                    let x2 : Int = ans2
                    let myString3 = String(x2)
                    
                   print(myString3 as Any)
                   
                if dictGetSpecialOrderData["status"] is String {
                                    
                    print("Yes, it's a String")
                    // self.changesBookingIdis = (dictGetSpecialOrderData["bookingId"] as! String);
                    myString12 = (dictGetSpecialOrderData["status"] as! String)
                } else if dictGetSpecialOrderData["status"] is Int {
                    
                    print("It is Integer")
                    let x2 : Int = (dictGetSpecialOrderData["status"] as! Int)
                    let myString2 = String(x2)
                    // self.dictGetSpecialOrderData = String(myString2)
                    myString12 = String(myString2)
                } else {
                    
                    print("i am ")
                    let temp:NSNumber = dictGetSpecialOrderData["status"] as! NSNumber
                    let tempString = temp.stringValue
                    // self.changesBookingIdis = String(tempString)
                    myString12 = String(tempString)
                }
                
                
                
                
                
                
                if dictGetSpecialOrderData["totalAmount"] is String {
                                    
                    print("Yes, it's a String")
                    // self.changesBookingIdis = (dictGetSpecialOrderData["bookingId"] as! String);
                    totalPriceInString = (dictGetSpecialOrderData["totalAmount"] as! String)
                } else if dictGetSpecialOrderData["totalAmount"] is Int {
                    
                    print("It is Integer")
                    let x2 : Int = (dictGetSpecialOrderData["totalAmount"] as! Int)
                    let myString2 = String(x2)
                    // self.dictGetSpecialOrderData = String(myString2)
                    totalPriceInString = String(myString2)
                } else {
                    
                    print("i am ")
                    let temp:NSNumber = dictGetSpecialOrderData["totalAmount"] as! NSNumber
                    let tempString = temp.stringValue
                    // self.changesBookingIdis = String(tempString)
                    totalPriceInString = String(tempString)
                }
                
                
                let andFinalPriceAfterAddTransactionfeeAndTotalAmount = Double(strFinalTransactionFeeIsForAdd)!+Double(totalPriceInString)!
                
                
                self.andTotalProiceIs = String(andFinalPriceAfterAddTransactionfeeAndTotalAmount)
                
                
                
                
                
                
                self.strFinalTotalAmountIsForAdd = totalPriceInString
                
                    if myString12 == "2" {
                        // self.btnTotalPrice.setTitle("Delivered $ "+String(myString3)+" "+totalPriceInString, for: .normal)
                       
                       self.btnTotalPrice.setTitle("Total Amount : $ "+totalPriceInString+" ( COMPLETED )", for: .normal)
                       
                        self.btnMarkAsDelivery.isHidden = true
                        self.btnRequestMoreMoney.isHidden = true
                    } else {
                        // self.btnTotalPrice.setTitle("$ "+String(myString3), for: .normal)
                       
                       self.btnTotalPrice.setTitle("Total Amount : $ "+totalPriceInString, for: .normal)
                       
                        self.btnMarkAsDelivery.isHidden = false
                        self.btnRequestMoreMoney.isHidden = true
                        
                         self.btnMarkAsDelivery.addTarget(self, action: #selector(markAsDelivered), for: .touchUpInside)
                         self.btnRequestMoreMoney.addTarget(self, action: #selector(requestmoreMoenyPopup), for: .touchUpInside)
                    }
                  
                
                
                
                
                // driver
/* ////////////////////////////////////////////////////////////////////////////////////////////////  */
   
  
                    
let x : Int = self.dictGetSpecialOrderData["status"] as! Int
let myString = String(x)

                
   // pending status
   if self.dictGetSpecialOrderData["paymentStatus"] is String {
                                               
       print("Yes, it's a String")
       
       // self.btnPaymentAccess.setTitle((self.dict["paymentStatus"] as! String), for: .normal)
       
       if (self.dictGetSpecialOrderData["paymentStatus"] as! String) == "0" {
           
        self.btnTotalPrice.isHidden = false
        self.btnTotalPrice.setTitle("Order Not Accept", for: .normal)
        // self.btnReview.isHidden = true
        
       } else if (self.dictGetSpecialOrderData["paymentStatus"] as! String) == "1" {
           
        self.btnTotalPrice.isHidden = false
        self.btnTotalPrice.setTitle("Payment not Done $ "+self.andTotalProiceIs, for: .normal)
        self.btnTotalPrice.backgroundColor = .systemRed
             // self.btnTotalPrice.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
        // self.btnReview.isHidden = true
        
        self.btnMarkAsDelivery.isHidden = true
        self.btnRequestMoreMoney.isHidden = true
        
       } else if (self.dictGetSpecialOrderData["paymentStatus"] as! String) == "2" {
           
        self.btnTotalPrice.isHidden = false
        self.btnTotalPrice.setTitle("Payment Done ( Order not Delivered yet )", for: .normal)
        self.btnTotalPrice.setTitleColor(.white, for: .normal)
        self.btnTotalPrice.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        self.btnTotalPrice.backgroundColor = .systemOrange
        
        self.btnMarkAsDelivery.isHidden = false
        self.btnRequestMoreMoney.isHidden = true
        
        if String(myString) == "2" {
            
            // self.btnReview.isHidden = false
            self.btnTotalPrice.setTitle("Delivered", for: .normal)
            self.btnMarkAsDelivery.isHidden = true
            self.btnRequestMoreMoney.isHidden = true
            
        } else {
            
            // self.btnReview.isHidden = true
            
        }
        
        
       }
       
       
       // 2 = complete
       
       
       
       
       
       
       
       
       
       
       
       
       
   } else if self.dictGetSpecialOrderData["paymentStatus"] is Int {
       
       print("It is Integer")
       let x2 : Int = (self.dictGetSpecialOrderData["paymentStatus"] as! Int)
       let myString2 = String(x2)
       // self.btnPaymentAccess.setTitle((myString2), for: .normal)
       
       
       
       if myString2 == "0" {
           
           self.btnTotalPrice.isHidden = false
           self.btnTotalPrice.setTitle("Order Not Accept", for: .normal)
           
       } else if myString2 == "1" {
           
        self.btnTotalPrice.isHidden = false
        self.btnTotalPrice.setTitle("Payment Not Done $"+self.andTotalProiceIs, for: .normal)
         // self.btnTotalPrice.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
        self.btnTotalPrice.backgroundColor = .systemRed
        
        // self.btnReview.isHidden = true
        
        self.btnMarkAsDelivery.isHidden = true
        self.btnRequestMoreMoney.isHidden = true
        
       } else if myString2 == "2" {
           
        self.btnTotalPrice.isHidden = false
        self.btnTotalPrice.setTitle("Payment Done ( Order not Delivered yet )", for: .normal)
        self.btnTotalPrice.setTitleColor(.white, for: .normal)
        self.btnTotalPrice.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        self.btnTotalPrice.backgroundColor = .systemOrange
        
        self.btnMarkAsDelivery.isHidden = false
        self.btnRequestMoreMoney.isHidden = true
        
        if String(myString) == "2" {
            
             self.btnTotalPrice.setTitle("Delivered", for: .normal)
            self.btnMarkAsDelivery.isHidden = true
            self.btnRequestMoreMoney.isHidden = true
            
        } else {
            
            // self.btnReview.isHidden = true
            
        }
        
        
        
        
       }
       
   } else {
       
       print("i am ")
       let temp:NSNumber = self.dictGetSpecialOrderData["paymentStatus"] as! NSNumber
       let tempString = temp.stringValue
       // self.btnPaymentAccess.setTitle((tempString), for: .normal)
       
       
       if tempString == "0" {
           
           self.btnTotalPrice.isHidden = false
           self.btnTotalPrice.setTitle("Order Not Accept", for: .normal)
           
       } else if tempString == "1" {
           
           self.btnTotalPrice.isHidden = false
           self.btnTotalPrice.setTitle("Payment Not Done $ "+self.andTotalProiceIs, for: .normal)
             // self.btnTotalPrice.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
        self.btnTotalPrice.backgroundColor = .systemRed
        
        
        self.btnMarkAsDelivery.isHidden = true
        self.btnRequestMoreMoney.isHidden = true
        
       } else if tempString == "2" {
           
        self.btnTotalPrice.isHidden = false
        self.btnTotalPrice.setTitle("Payment Done ( Order not Delivered yet )", for: .normal)
        self.btnTotalPrice.setTitleColor(.white, for: .normal)
        self.btnTotalPrice.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        self.btnTotalPrice.backgroundColor = .systemOrange
        
        self.btnMarkAsDelivery.isHidden = false
        self.btnRequestMoreMoney.isHidden = true
        
        if String(myString) == "2" {
            
             self.btnTotalPrice.setTitle("Delivered", for: .normal)
            self.btnMarkAsDelivery.isHidden = true
            self.btnRequestMoreMoney.isHidden = true
            
        } else {
            
            // self.btnReview.isHidden = true
            
        }
        
        
        
       }
       
       
   
   
   
                } // driver ends here

   
   
/* ////////////////////////////////////////////////////////////////////////////////////////////////  */

                
                
                
                
               } else {
                   
                if dictGetSpecialOrderData["transactionFee"] is String {
                                   
                    print("Yes, it's a String")
                       
                    self.lblTransactionFee.text = "$ "+(dictGetSpecialOrderData["transactionFee"] as! String)
                    self.strFinalTransactionFeeIsForAdd = (dictGetSpecialOrderData["transactionFee"] as! String)
                    
                } else if dictGetSpecialOrderData["transactionFee"] is Int {
                       
                    print("It is Integer")
                       
                    let x2 : Int = (dictGetSpecialOrderData["transactionFee"] as! Int)
                    let myString2 = String(x2)
                    self.lblTransactionFee.text = "$ "+myString2
                    self.strFinalTransactionFeeIsForAdd = myString2
                } else {
                     //some other check
                    print("i am ")
                       
                    let temp:NSNumber = dictGetSpecialOrderData["transactionFee"] as! NSNumber
                    let tempString = temp.stringValue
                    self.lblTransactionFee.text = "$ "+tempString
                    self.strFinalTransactionFeeIsForAdd = tempString
                }
                
                
                
                // self.specialOrdersList()
                
                   imgProfile.sd_setImage(with: URL(string: (dictGetSpecialOrderData["userImage"] as! String)), placeholderImage: UIImage(named: "profile"))
                    
                    self.lblUserName.text    = (dictGetSpecialOrderData["userName"] as! String)
                    self.lblUserAddress.text    = (dictGetSpecialOrderData["address"] as! String)
                    
                    self.lblProductNmae.text    = (dictGetSpecialOrderData["whatYouWant"] as! String)
                
                if dictGetSpecialOrderData["deliveryFee"] is String {
                               
                               print("Yes, it's a String")
                               
                               self.lblDeliveryFee.text = "$ "+(dictGetSpecialOrderData["deliveryFee"] as! String)
                
                           } else if dictGetSpecialOrderData["deliveryFee"] is Int {
                               
                               print("It is Integer")
                               
                               let x2 : Int = (dictGetSpecialOrderData["deliveryFee"] as! Int)
                               let myString2 = String(x2)
                               self.lblDeliveryFee.text = "$ "+myString2
                               
                           } else {
                             //some other check
                               print("i am ")
                               
                               let temp:NSNumber = dictGetSpecialOrderData["deliveryFee"] as! NSNumber
                               let tempString = temp.stringValue
                               self.lblDeliveryFee.text = "$ "+tempString
                               
                           }
                
                
                    self.lblEstimatedPrice.text = "$ "+(dictGetSpecialOrderData["price"] as! String)
                    self.lblTipAmount.text      = "$ "+(dictGetSpecialOrderData["TIP"] as! String)
                   
                   
                   
                   
                   
                   if dictGetSpecialOrderData["deliveryFee"] is String {
                       
                       print("Yes, it's a String")
                       
                       self.lblDeliveryFee.text = "$ "+(dictGetSpecialOrderData["deliveryFee"] as! String)
        
                   } else if dictGetSpecialOrderData["deliveryFee"] is Int {
                       
                       print("It is Integer")
                       
                       let x2 : Int = (dictGetSpecialOrderData["deliveryFee"] as! Int)
                       let myString2 = String(x2)
                       self.lblDeliveryFee.text = "$ "+myString2
                       
                   } else {
                     //some other check
                       print("i am ")
                       
                       let temp:NSNumber = dictGetSpecialOrderData["deliveryFee"] as! NSNumber
                       let tempString = temp.stringValue
                       self.lblDeliveryFee.text = "$ "+tempString
                       
                   }
                   
                   if dictGetSpecialOrderData["salesTax"] is String {
                                  
                      print("Yes, it's a String")
                      
                      self.lblStateTax.text = "$ "+(dictGetSpecialOrderData["salesTax"] as! String)

                  } else if dictGetSpecialOrderData["salesTax"] is Int {
                      
                      print("It is Integer")
                      
                      let x2 : Int = (dictGetSpecialOrderData["salesTax"] as! Int)
                      let myString2 = String(x2)
                      self.lblStateTax.text = "$ "+myString2
                      
                  } else {
                    //some other check
                      print("i am ")
                      
                      let temp:NSNumber = dictGetSpecialOrderData["salesTax"] as! NSNumber
                      let tempString = temp.stringValue
                      self.lblStateTax.text = "$ "+tempString
                      
                  }
                   
                 
                let myString1 = (dictGetSpecialOrderData["price"] as! String)
                let myInt1 = Int(myString1)
                    
                let myString2 = (dictGetSpecialOrderData["TIP"] as! String)
                let myInt2 = Int(myString2)
                    
                if dictGetSpecialOrderData["salesTax"] is String {
                                   
                    print("Yes, it's a String")
                       
                    self.lblStateTax.text = "$ "+(dictGetSpecialOrderData["salesTax"] as! String)

                } else if dictGetSpecialOrderData["salesTax"] is Int {
                       
                    print("It is Integer")
                       
                    let x2 : Int = (dictGetSpecialOrderData["salesTax"] as! Int)
                    let myString2 = String(x2)
                    self.lblStateTax.text = "$ "+myString2
                       
                } else {
                     //some other check
                    print("i am ")
                       
                    let temp:NSNumber = dictGetSpecialOrderData["salesTax"] as! NSNumber
                    let tempString = temp.stringValue
                    self.lblStateTax.text = "$ "+tempString
                       
                }
                   
                   
                   // convert delivery fee from string to int
                   
                let addPrice = myInt1
                let addTip = myInt2
                   
                let myString1111 = (dictGetSpecialOrderData["deliveryFee"] as! String)
                let myInt1111 = Int(myString1111)
                   
                   
                   
                var ans2 = Int()
                ans2 = addPrice!+addTip!+myInt1111!
                    
                let x2 : Int = ans2
                let myString3 = String(x2)
                
                
                
                let andFinalPriceAfterAddTransactionfeeAndTotalAmount = Double(strFinalTransactionFeeIsForAdd)!+Double(myString3)!
                
                
                self.andTotalProiceIs = String(andFinalPriceAfterAddTransactionfeeAndTotalAmount)
                
                
                
                // self.andTotalProiceIs = myString3
                
                   let x12 : Int = dictGetSpecialOrderData["status"] as! Int
                   let myString12 = String(x12)
                   
                   if myString12 == "2" {
                        self.btnTotalPrice.setTitle("Delivered $ "+String(myString3), for: .normal)
                        self.btnMarkAsDelivery.isHidden = true
                        self.btnRequestMoreMoney.isHidden = true
                       
                    } else {
                        self.btnTotalPrice.setTitle("Total Amount : $ "+String(myString3)+" ( PENDING )", for: .normal)
                       
                        self.btnMarkAsDelivery.isHidden = true
                        self.btnRequestMoreMoney.isHidden = true
                        
                        // self.btnMarkAsDelivery.addTarget(self, action: #selector(markAsDelivered), for: .touchUpInside)
                        // self.btnRequestMoreMoney.addTarget(self, action: #selector(requestmoreMoenyPopup), for: .touchUpInside)
                }
                
                
                // member
/* ////////////////////////////////////////////////////////////////////////////////////////////////  */
   
                // 1 = driver not accept
                // 2 = delivered
                
                
                
                
                   
let x : Int = self.dictGetSpecialOrderData["status"] as! Int
let myString = String(x)

                
   // pending status
   if self.dictGetSpecialOrderData["paymentStatus"] is String {
                                               
       print("Yes, it's a String")
       
       // self.btnPaymentAccess.setTitle((self.dict["paymentStatus"] as! String), for: .normal)
       
       if (self.dictGetSpecialOrderData["paymentStatus"] as! String) == "0" {
           
        self.btnTotalPrice.isHidden = false
        self.btnTotalPrice.setTitle("Driver not Accept yet", for: .normal)
        // self.btnReview.isHidden = true
        
       } else if (self.dictGetSpecialOrderData["paymentStatus"] as! String) == "1" {
           
        self.btnTotalPrice.isHidden = false
        self.btnTotalPrice.setTitle("Confirm & Pay $ "+self.andTotalProiceIs, for: .normal)
        self.btnTotalPrice.backgroundColor = .systemGreen
              self.btnTotalPrice.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
        // self.btnReview.isHidden = true
        
       } else if (self.dictGetSpecialOrderData["paymentStatus"] as! String) == "2" {
           
        self.btnTotalPrice.isHidden = false
        self.btnTotalPrice.setTitle("Payment Done ( Order not Delivered yet )", for: .normal)
        self.btnTotalPrice.setTitleColor(.white, for: .normal)
        self.btnTotalPrice.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        
        if String(myString) == "2" {
            
            // self.btnReview.isHidden = false
            self.btnTotalPrice.setTitle("Delivered", for: .normal)
            
        } else {
            
            // self.btnReview.isHidden = true
            
        }
        
        
        
        
       }
       
       
       // 2 = complete
       
       
       
       
       
       
       
       
       
       
       
       
       
   } else if self.dictGetSpecialOrderData["paymentStatus"] is Int {
       
       print("It is Integer")
       let x2 : Int = (self.dictGetSpecialOrderData["paymentStatus"] as! Int)
       let myString2 = String(x2)
       // self.btnPaymentAccess.setTitle((myString2), for: .normal)
       
       
       
       if myString2 == "0" {
           
           self.btnTotalPrice.isHidden = false
           self.btnTotalPrice.setTitle("Driver not Accept yet", for: .normal)
           
       } else if myString2 == "1" {
           
        self.btnTotalPrice.isHidden = false
        self.btnTotalPrice.setTitle("Confirm & Pay $"+self.andTotalProiceIs, for: .normal)
           self.btnTotalPrice.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
        self.btnTotalPrice.backgroundColor = .systemGreen
        
        // self.btnReview.isHidden = true
        
       } else if myString2 == "2" {
           
        self.btnTotalPrice.isHidden = false
        self.btnTotalPrice.setTitle("Payment Done ( Order not Delivered yet )", for: .normal)
        self.btnTotalPrice.setTitleColor(.white, for: .normal)
        self.btnTotalPrice.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        
        
        if String(myString) == "2" {
            
             self.btnTotalPrice.setTitle("Delivered", for: .normal)
            
        } else {
            
            // self.btnReview.isHidden = true
            
        }
        
        
        
        
       }
       
   } else {
       
       print("i am ")
       let temp:NSNumber = self.dictGetSpecialOrderData["paymentStatus"] as! NSNumber
       let tempString = temp.stringValue
       // self.btnPaymentAccess.setTitle((tempString), for: .normal)
       
       
       if tempString == "0" {
           
           self.btnTotalPrice.isHidden = false
           self.btnTotalPrice.setTitle("Driver not Accept yet", for: .normal)
           
       } else if tempString == "1" {
           
           self.btnTotalPrice.isHidden = false
           self.btnTotalPrice.setTitle("Confirm & Pay $ "+self.andTotalProiceIs, for: .normal)
              self.btnTotalPrice.addTarget(self, action: #selector(self.confirmAndPay), for: .touchUpInside)
        self.btnTotalPrice.backgroundColor = .systemGreen
        
       } else if tempString == "2" {
           
        self.btnTotalPrice.isHidden = false
        self.btnTotalPrice.setTitle("Payment Done ( Order not Delivered yet )", for: .normal)
        self.btnTotalPrice.setTitleColor(.white, for: .normal)
        self.btnTotalPrice.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        
        if String(myString) == "2" {
            
             self.btnTotalPrice.setTitle("Delivered", for: .normal)
            
        } else {
            
            // self.btnReview.isHidden = true
            
        }
        
        
        
       }
       
       
   }
   
   
/* ////////////////////////////////////////////////////////////////////////////////////////////////  */

            
                
                
                
                

            }
        }
    }
    
    @objc func confirmAndPay() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPPayemtnScreenForFoodSectionId") as? UPPayemtnScreenForFoodSection
        
        /*
         var specialFoodSpecialId:String!
         var specialFoodTotalAmount:String!
         var specialFoodTip:String!
         */
        
        // dictGetSpecialOrderData,specialrequestId
        
        // service id
        if self.dictGetSpecialOrderData["specialrequestId"] is String {
                            
            print("Yes, it's a String")
            // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
            // push!.getTotalPriceOfFood = (dictOfNotificationPopup["totalAmount"] as! String)
            push!.specialFoodSpecialId = (self.dictGetSpecialOrderData["specialrequestId"] as! String)
            
        } else if self.dictGetSpecialOrderData["specialrequestId"] is Int {
            
            print("It is Integer")
            let x2 : Int = (self.dictGetSpecialOrderData["specialrequestId"] as! Int)
            let myString2 = String(x2)
            // self.changesBookingIdis = String(myString2)
            // push!.getTotalPriceOfFood = String(myString2)
            push!.specialFoodSpecialId = String(myString2)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = self.dictGetSpecialOrderData["specialrequestId"] as! NSNumber
            let tempString = temp.stringValue
            // self.changesBookingIdis = String(tempString)
            // push!.getTotalPriceOfFood = String(tempString)
            push!.specialFoodSpecialId = String(tempString)
            
        }
        
        // TOTAL AMOUNT
        if self.dictGetSpecialOrderData["totalAmount"] is String {
                            
            print("Yes, it's a String")
            // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
            
            let addTransactionFeeeesWithTotal = Double((self.dictGetSpecialOrderData["transactionFee"] as! String))!+Double((self.dictGetSpecialOrderData["totalAmount"] as! String))!
            
             push!.specialFoodTotalAmount = String(addTransactionFeeeesWithTotal)
            
        } else if self.dictGetSpecialOrderData["totalAmount"] is Int {
            
            print("It is Integer")
            let x2 : Int = (self.dictGetSpecialOrderData["totalAmount"] as! Int)
            let myString2 = String(x2)
            // self.changesBookingIdis = String(myString2)
            // push!.specialFoodTotalAmount = String(myString2)
            
            let addTransactionFeeeesWithTotal = Double((self.dictGetSpecialOrderData["transactionFee"] as! String))!+Double(myString2)!
            
             push!.specialFoodTotalAmount = String(addTransactionFeeeesWithTotal)
            
        } else {
            
            print("i am ")
            let temp:NSNumber = self.dictGetSpecialOrderData["totalAmount"] as! NSNumber
            let tempString = temp.stringValue
            // self.changesBookingIdis = String(tempString)
            // push!.specialFoodTotalAmount = String(tempString)
            
            let addTransactionFeeeesWithTotal = Double((self.dictGetSpecialOrderData["transactionFee"] as! String))!+Double(tempString)!
            
             push!.specialFoodTotalAmount = String(addTransactionFeeeesWithTotal)
            
        }
        
        
        // TIP
        if self.dictGetSpecialOrderData["TIP"] is String {
                            
            print("Yes, it's a String")
            // self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
            push!.specialFoodTip = (self.dictGetSpecialOrderData["TIP"] as! String)
            
        } else if self.dictGetSpecialOrderData["TIP"] is Int {
            
            print("It is Integer")
            let x2 : Int = (self.dictGetSpecialOrderData["TIP"] as! Int)
            let myString2 = String(x2)
            // self.changesBookingIdis = String(myString2)
            push!.specialFoodTip = String(myString2)
        } else {
            
            print("i am ")
            let temp:NSNumber = self.dictGetSpecialOrderData["TIP"] as! NSNumber
            let tempString = temp.stringValue
            // self.changesBookingIdis = String(tempString)
            push!.specialFoodTip = String(tempString)
        }
        
        push!.paymentFoodOrService = "iAmFromSpecialOrder"
        push!.enableBackOrNot = "yes"
        
       self.navigationController?.pushViewController(push!, animated: true)
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     let myString = "fromNotification"
     UserDefaults.standard.set(myString, forKey: "keyFromNotification")
     */
    
    
    
    @objc func markAsDelivered() {
    
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
       
        /*
         [action] => togetstatusupdate
         [togetRequestId] => 61
         [driverId] => 123
         [status] => 2
         */
        // foodorderId
       
       let x : Int = dictGetSpecialOrderData["specialrequestId"] as! Int
       let myString = String(x)
       
        /*
         action: specialchangestatus
         specialrequestId:
         status:
         driverId:
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
               // let str:String = person["role"] as! String
            parameters = [
                "action"            : "specialchangestatus",
                "driverId"          : person["userId"] as Any,
                "specialrequestId"  : String(myString),
                "status"            : String("2")
            ]
        }
        
        print("parameters-------\(String(describing: parameters))")
                      
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
            {
                response in
                  
                switch(response.result) {
                              case .success(_):
                                 if let data = response.result.value {

                                  let JSON = data as! NSDictionary
                                     print(JSON as Any)
                                  
                                  var strSuccess : String!
                                  strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                                  if strSuccess == String("success") {
                                   print("yes")
                                  var strSuccess2 : String!
                                  strSuccess2 = JSON["msg"]as Any as? String
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                  alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                      // self.dismiss(animated: true, completion: nil)
                                   self.navigationController?.popViewController(animated: true)
                                      
                                  }))
                                  self.present(alert, animated: true, completion: nil)
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                               }
                          }
       
          }
    
    
    
    @objc func specialOrdersList() {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
       
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
               
            let x : Int = person["userId"] as! Int
            let myString = String(x)

            parameters = [
                "action"            : "specialrequestlist",
                "userType"          : person["role"] as! String,
                "userId"            : String(myString),
                "pageNo"            : String("")
            ]
        }
        
        print("parameters-------\(String(describing: parameters))")
                      
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
            {
                response in
                  
                switch(response.result) {
                              case .success(_):
                                 if let data = response.result.value {

                                  let JSON = data as! NSDictionary
                                     print(JSON as Any)
                                  
                                  var strSuccess : String!
                                  strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                                  if strSuccess == String("success") {
                                   print("yes")
                                  // var strSuccess2 : String!
                                  // strSuccess2 = JSON["msg"]as Any as? String
                                  
                                  ERProgressHud.sharedInstance.hide()
                                    /*
                                  let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                  alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                      // self.dismiss(animated: true, completion: nil)
                                   self.navigationController?.popViewController(animated: true)
                                     
                                  }))
                                  self.present(alert, animated: true, completion: nil)
                                     */
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                               }
                          }
       
          }
    
    
    
    
    @objc func requestmoreMoenyPopup() {
        let alertController = UIAlertController(title: "Request More Money", message: "Enter your amout", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "enter amount"
        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            self.requestMoreMoneyWB(strNewRequestMoney: firstTextField.text!)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        

        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    
    
    @objc func requestMoreMoneyWB(strNewRequestMoney:String) {
    
         ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
       
        /*
         [action] => sendmoremomeyrequest
         [togetRequestId] => 59
         [driverId] => 123
         [requestMoney] => 25
         */
        // foodorderId
       
       let x : Int = dictGetSpecialOrderData["specialrequestId"] as! Int
       let myString = String(x)
       
           var parameters:Dictionary<AnyHashable, Any>!
           if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
               // let str:String = person["role"] as! String
            if strNewRequestMoney == "" {
                parameters = [
                           "action"          : "sendmoremomeyrequest",
                            "driverId"       : person["userId"] as Any,
                           "togetRequestId"  : String(myString),
                            "requestMoney"         : String("0")
                ]
            } else {
                parameters = [
                           "action"          : "sendmoremomeyrequest",
                            "driverId"       : person["userId"] as Any,
                           "togetRequestId"  : String(myString),
                            "requestMoney"         : String(strNewRequestMoney)
                ]
            }
                      
        }
                      print("parameters-------\(String(describing: parameters))")
                      
                      Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                          {
                              response in
                  
                              switch(response.result) {
                              case .success(_):
                                 if let data = response.result.value {

                                  let JSON = data as! NSDictionary
                                     print(JSON as Any)
                                  
                                  var strSuccess : String!
                                  strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                                  if strSuccess == String("success") {
                                   print("yes")
                                  var strSuccess2 : String!
                                  strSuccess2 = JSON["msg"]as Any as? String
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                  alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                      // self.dismiss(animated: true, completion: nil)
                                   self.navigationController?.popViewController(animated: true)
                                      
                                  }))
                                  self.present(alert, animated: true, completion: nil)
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                               }
                          }
       
          }
    
}
