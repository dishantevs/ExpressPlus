//
//  PDJobRequest.swift
//  ExpressPlus
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

import CoreLocation

class PDJobRequest: UIViewController, CLLocationManagerDelegate {

    let cellReuseIdentifier = "pDJobRequestTableCell"
    
    var whoAreYou:String!
    
    var dictOfNotificationPopup:NSDictionary!
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
    
    var page : Int! = 1
    var loadMore : Int! = 1;
    
    var strCheckOption:String!
    
    @IBOutlet weak var segMentControll:UISegmentedControl! {
        didSet {
            segMentControll.backgroundColor = .orange
        }
    }
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            
        }
    }
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnService:UIButton! {
        didSet {
            btnService.setTitle("Services", for: .normal)
            btnService.setTitleColor(.white, for: .normal)
            btnService.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var btnFoodDelivery:UIButton! {
        didSet {
            btnFoodDelivery.setTitle("Food Delivery", for: .normal)
            btnFoodDelivery.setTitleColor(.white, for: .normal)
            btnFoodDelivery.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var btnToGet:UIButton! {
        didSet {
            btnToGet.setTitle("To Get", for: .normal)
            btnToGet.setTitleColor(.white, for: .normal)
            btnToGet.backgroundColor = UIColor.init(red: 213.0/255.0, green: 85.0/255.0, blue: 39.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var lblDownOne:UILabel!
    @IBOutlet weak var lblDownTwo:UILabel!
    @IBOutlet weak var lblDownThree:UILabel!
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
            self.tbleView.separatorStyle = UITableViewCell.SeparatorStyle.none
        }
    }
    
    @IBOutlet weak var btnMarkAsDelivery:UIButton! {
        didSet {
            btnMarkAsDelivery.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnMarkAsDelivery.setTitleColor(.white, for: .normal)
            btnMarkAsDelivery.setTitle("Mark as Delivered", for: .normal)
        }
    }
    @IBOutlet weak var btnRequestMoreMoney:UIButton!{
        didSet {
            btnRequestMoreMoney.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnRequestMoreMoney.setTitleColor(.white, for: .normal)
            btnRequestMoreMoney.setTitle("Request more Money", for: .normal)
        }
    }
    
    var strString:String!
    
    var locationManager:CLLocationManager!
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = UIColor.init(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1)
        
        // self.lblDownOne.isHidden = false
        // self.lblDownTwo.isHidden = true
        // self.lblDownThree.isHidden = true
        
        print(self.dictOfNotificationPopup as Any)
        
        // self.iAmHereForLocationPermission()
        
        if self.dictOfNotificationPopup == nil {
            
            self.sideBarMenuClick()
            
            // btnFoodDelivery.addTarget(self, action: #selector(foodOrderListWB), for: .touchUpInside)
            // btnService.addTarget(self, action: #selector(serviceWB), for: .touchUpInside)
            // btnToGet.addTarget(self, action: #selector(toGetServiceWB), for: .touchUpInside)
            
            
              
            /*if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // ,
                if String(person["role"] as! String) == "Member" {
                    lblNavigationTitle.text = "ORDERS"
                } else {
                    lblNavigationTitle.text = "JOB HISTORY"
                }
                
            }*/
            
            self.lblNavigationTitle.text = "JOB HISTORY"
            
            
            self.segMentControll.selectedSegmentIndex = 0
            self.segMentControll.addTarget(self, action: #selector(segmentControlClick), for: .valueChanged)
            
            // self.serviceWB()
            
            self.segMentControll.isHidden = true
            // self.foodOrderListWB()
            
        } else {
            
            self.sideBarMenuClick()
            
            // btnFoodDelivery.addTarget(self, action: #selector(foodOrderListWB), for: .touchUpInside)
            // btnService.addTarget(self, action: #selector(serviceWB), for: .touchUpInside)
            // btnToGet.addTarget(self, action: #selector(toGetServiceWB), for: .touchUpInside)
            
            // self.specialRequestList()
              
            /*if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // ,
                if String(person["role"] as! String) == "Member" {
                    lblNavigationTitle.text = "ORDERS"
                } else {
                    lblNavigationTitle.text = "JOB HISTORY"
                }
                
            }*/
            
            self.lblNavigationTitle.text = "JOB HISTORY"
            
            self.segMentControll.isHidden = true
            self.segMentControll.selectedSegmentIndex = 3
            self.segMentControll.addTarget(self, action: #selector(segmentControlClick), for: .valueChanged)
            
            self.tbleView.separatorColor = .black
            
            
        }
        
        
    }
    
    @objc func segmentControlClick() {
        
        if segMentControll.selectedSegmentIndex == 0 {
            self.serviceWB()
        } else if segMentControll.selectedSegmentIndex == 1 {
            self.foodOrderListWB()
        } else if segMentControll.selectedSegmentIndex == 2 {
            self.toGetServiceWB()
        } else if segMentControll.selectedSegmentIndex == 3 {
            self.specialRequestList()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.foodOrderListWB()
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    // MARK:- GET CUSTOMER LOCATION -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! PDBagPurchaseTableCell
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { [self] city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            //print(self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            print("locality ==> "+locality as Any) // sector 10 dwarka
            
            print(self.strSaveCountryName as Any) // india
            print(self.strSaveStateName as Any) // new delhi
            print(self.strSaveZipcodeName as Any) // 110075
            
            
            
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            
            
            // self.findMyStateTaxWB()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
       //  addTopAndBottomBorders()
    }
    
    func addTopAndBottomBorders() {
       let thickness: CGFloat = 2.0
       let bottomBorder = CALayer()
       bottomBorder.frame = CGRect(x:0, y: self.btnService.frame.size.height - thickness, width: self.btnService.frame.size.width, height:thickness)
       bottomBorder.backgroundColor = UIColor.yellow.cgColor
       btnService.layer.addSublayer(bottomBorder)
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    
    
    
    // MARK:- WEBSERVICE ( SERVICE JOB REQUEST ) -
    @objc func serviceWB() {
        
        // self.lblDownOne.isHidden = false
        // self.lblDownTwo.isHidden = true
        // self.lblDownThree.isHidden = true
        
        self.strString = "1"
        self.arrListOfAllMyOrders.removeAllObjects()
     
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
        let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => bookinglist
          [userId] => 116
          [userType] => Driver
          [status] =>
          [pageNo] => 2
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
     
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
            parameters = [
                "action"              : "bookinglist",
                "userId"              : String(myString),
                "userType"            : String(person["role"] as! String),
                "status"              : String(""),
                "pageNo"              : String("1")
            ]
        }
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
            {
                response in
               
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value {

                        let JSON = data as! NSDictionary
                        print(JSON as Any)
                               
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                        if strSuccess == String("success") {
                            print("yes")
                                 
                            ERProgressHud.sharedInstance.hide()
                                 
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                 
                            self.tbleView.delegate = self
                            self.tbleView.dataSource = self
                            self.tbleView.reloadData()
                            self.loadMore = 1
                                 
                        }
                        else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                                
                                
                        }
                    }

                case .failure(_):
                    print("Error message:\(String(describing: response.result.error))")
                               
                    ERProgressHud.sharedInstance.hide()
                               
                    let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                        NSLog("OK Pressed")
                    }
                               
                    alertController.addAction(okAction)
                               
                    self.present(alertController, animated: true, completion: nil)
                               
                    break
                }
        }
    
    }
    
    
    // MARK:- WEBSERVICE ( SERVICE JOB REQUEST ) -
    @objc func foodOrderListWB() {
        
        // self.lblDownOne.isHidden = true
        // self.lblDownTwo.isHidden = false
        // self.lblDownThree.isHidden = true
        
        self.strString = "2"
        self.arrListOfAllMyOrders.removeAllObjects()
     
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
        let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => foodorderlist
      [userId] => 116
      [userType] => Driver
      [status] =>
      [pageNo] => 2
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            parameters = [
                "action"              : "foodorderlist",
                "userType"            : String(person["role"] as! String),
                "userId"              : person["userId"] as Any
            ]
            
        }
        
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
            {
                response in
               
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value {

                        let JSON = data as! NSDictionary
                        print(JSON as Any)
                               
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                        if strSuccess == String("success") {
                            print("yes")
                                 
                            ERProgressHud.sharedInstance.hide()
                                 
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                 
                            self.tbleView.delegate = self
                            self.tbleView.dataSource = self
                            self.tbleView.reloadData()
                            self.loadMore = 1
                               
                            
                        }
                        else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                        }
                    }

                case .failure(_):
                    print("Error message:\(String(describing: response.result.error))")
                               
                    ERProgressHud.sharedInstance.hide()
                               
                    let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                    }
                               
                    alertController.addAction(okAction)
                               
                    self.present(alertController, animated: true, completion: nil)
                               
                    break
                }
        }
    }
    
    // MARK:- WEBSERVICE ( SERVICE JOB REQUEST ) -
    @objc func toGetServiceWB() {
        
        // self.lblDownOne.isHidden = true
        // self.lblDownTwo.isHidden = true
        // self.lblDownThree.isHidden = false
        
        self.strString = "3"
        self.arrListOfAllMyOrders.removeAllObjects();
     
      ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
    
     let urlString = BASE_URL_EXPRESS_PLUS
    
     /*
      [action] => foodorderlist
      [userId] => 116
      [userType] => Driver
      [status] =>
      [pageNo] => 2
      */
     
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // let str:String = person["role"] as! String
            parameters = [
                "action"              : "gogetlist",
                "userId"              : person["userId"] as Any,
                "userType"            : String(person["role"] as! String),
                "status"              : String(""),
                "pageNo"              : String("1")
            ]
        }
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
            {
                response in
               
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value {

                        let JSON = data as! NSDictionary
                        print(JSON as Any)
                               
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                        if strSuccess == String("success") {
                            print("yes")
                                 
                            ERProgressHud.sharedInstance.hide()
                                 
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                 
                            self.tbleView.delegate = self
                            self.tbleView.dataSource = self
                            self.tbleView.reloadData()
                            self.loadMore = 1;
                                 
                        }
                        else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                                
                        }
                    }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
    
       }
    
    
    // MARK:- (SPECIAL ORDER ) -
    @objc func specialRequestList() {
        /*
         [action] => specialrequestlist
            [userId] => 267
            [userType] => Member
            [pageNo] => 2
         */
        
        
        

            // self.lblDownOne.isHidden = true
            // self.lblDownTwo.isHidden = true
            // self.lblDownThree.isHidden = false
            
            self.strString = "4"
            self.arrListOfAllMyOrders.removeAllObjects();
         
          ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
         let urlString = BASE_URL_EXPRESS_PLUS
        
         /*
          [action] => foodorderlist
          [userId] => 116
          [userType] => Driver
          [status] =>
          [pageNo] => 2
          */
         
            var parameters:Dictionary<AnyHashable, Any>!
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                // let str:String = person["role"] as! String
                parameters = [
                    "action"              : "specialrequestlist",
                    "userId"              : person["userId"] as Any,
                    "userType"            : String(person["role"] as! String),
                    "status"              : String(""),
                    "pageNo"              : String("1")
                ]
            }
            print("parameters-------\(String(describing: parameters))")
                       
            Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                {
                    response in
                   
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value {

                            let JSON = data as! NSDictionary
                            print(JSON as Any)
                                   
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                                   
                                     // var strSuccessAlert : String!
                                     // strSuccessAlert = JSON["msg"]as Any as? String
                                   
                            if strSuccess == String("success") {
                                print("yes")
                                     
                                ERProgressHud.sharedInstance.hide()
                                     
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                     
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                self.loadMore = 1;
                                     
                            }
                            else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                    
                            }
                        }

                               case .failure(_):
                                   print("Error message:\(String(describing: response.result.error))")
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   
                                   let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                   
                                   let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                           UIAlertAction in
                                           NSLog("OK Pressed")
                                       }
                                   
                                   alertController.addAction(okAction)
                                   
                                   self.present(alertController, animated: true, completion: nil)
                                   
                                   break
                                }
                           }
        
    }

}

extension PDJobRequest: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PDJobRequestTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! PDJobRequestTableCell
        
        /*
         AVGRating = "";
         TIP = 10;
         address = "8, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
         bookingDate = "26th May";
         bookingId = 230;
         contactNumber = 2352639000;
         created = "2020-05-25 11:36:00";
         email = "tt@gmail.com";
         fullName = purnima;
         groupId = 46;
         image = "";
         serviceAmount = 100;
         serviceName = "Yard Work";
         servicePrice = 100;
         slote = "9:0-6:0";
         status = 2;
         totalAmount = 110;
         userAddress = "";
         userId = 117;
         zipCode = "";
         */
        
        /* // food delivery
        TIP = 5;
        cardNo = 4242424242424242;
        couponCode = "";
        created = "2020-05-25 11:18:00";
        discount = 0;
        driverName = "new driver 2.1";
        foodDetails =             (
                            {
                id = 49;
                name = babbSs;
                price = "20.00";
                quantity = 1;
                resturentId = 104;
            }
        );
        foodId = "";
        foodorderId = 89;
        noContact = "";
        resturentAddress = "Gwalior, Madhya Pradesh, India";
        resturentId = 104;
        resturentImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1588663958IMG20191105144302.jpg";
        resturentName = satish;
        specialNote = "";
        status = 2;
        totalAmount = 25;
        userAddress = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
        userId = 114;
        userImage = "http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1589889715IMG-20200519-WA0003.jpg";
        userName = kiwi;
        whatYouWant = "";
        workPlace = Home;
        */
        
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        print(item as Any)
        
        if strString == "1" {
            cell.viewCellBG.backgroundColor = .clear
            
            cell.lblName.text = (item!["fullName"] as! String)
            
            let totalAmount = Double((item!["totalAmount"] as! String))!+Double((item!["transactionFee"] as! String))!
            
            let aStr = String(format: "%.2f", totalAmount)
            cell.lblPrice.text = " - $"+String(aStr)
            
            cell.lblDate.text = (item!["created"] as! String)
            cell.lblService.text = (item!["serviceName"] as! String)
            
            cell.imgProfileImage.sd_setImage(with: URL(string: (item!["image"] as! String)), placeholderImage: UIImage(named: "profile"))
            
        } else if strString == "2" {
            cell.viewCellBG.backgroundColor = .clear
            // transactionFee
            cell.lblName.text = (item!["userName"] as! String)
            
            let totalAmount = Double((item!["totalAmount"] as! String))!+Double((item!["transactionFee"] as! String))!
            
            let aStr = String(format: "%.2f", totalAmount)
            cell.lblPrice.text = " - $"+String(aStr)
            
            // cell.lblPrice.text = " - $"+String(totalAmount)
            cell.lblDate.text = (item!["created"] as! String)
            cell.lblService.text = ""
            cell.lblService.text = (item!["resturentAddress"] as! String)
            
            cell.imgProfileImage.sd_setImage(with: URL(string: (item!["userImage"] as! String)), placeholderImage: UIImage(named: "profile"))
            
        } else if strString == "3" {
            
            
            
            if (item!["requestMoney"] as! String) == "20" {
                cell.viewCellBG.backgroundColor = .systemRed
            } else {
                cell.backgroundColor = .white
            }
            
            cell.lblName.text = (item!["userName"] as! String)
            
            
            // let totalAmount = Double((item!["totalAmount"] as! String))!+Double((item!["transactionFee"] as! String))!
            
            if item!["totalAmount"] is String {
                                
                print("Yes, it's a String")
                
                // cell.lblPrice.text = "$ "+(item!["totalAmount"] as! String)

                let totalAmount = Double((item!["totalAmount"] as! String))!+Double((item!["transactionFee"] as! String))!
                cell.lblPrice.text = "$ "+String(totalAmount)
                
                
                
            } else if item!["totalAmount"] is Int {
                
                print("It is Integer")
                
                let x2 : Int = (item!["totalAmount"] as! Int)
                let myString2 = String(x2)
                // cell.lblPrice.text = "$ "+myString2
                
                let totalAmount = Double(myString2)!+Double((item!["transactionFee"] as! String))!
                cell.lblPrice.text = "$ "+String(totalAmount)
                
            } else {
              //some other check
                print("i am ")
                
                let temp:NSNumber = item!["totalAmount"] as! NSNumber
                let tempString = temp.stringValue
                // cell.lblPrice.text = "$ "+tempString
                
                let totalAmount = Double(tempString)!+Double((item!["transactionFee"] as! String))!
                cell.lblPrice.text = "$ "+String(totalAmount)
                
            }
            
            
           //  cell.lblPrice.text = " - $"+(item!["totalAmount"] as! String)
            
            
            
            cell.lblDate.text = (item!["created"] as! String)
            cell.lblService.text = (item!["address"] as! String)
            
            cell.imgProfileImage.sd_setImage(with: URL(string: (item!["userImage"] as! String)), placeholderImage: UIImage(named: "profile"))
            
        } else if strString == "4" {
            cell.viewCellBG.backgroundColor = .clear
            /*
             TIP = 5;
             address = "Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India";
             created = "2020-09-18 17:15:00";
             deliveryFee = 6;
             driverId = "";
             driverImage = "";
             driverName = "";
             driverPhone = "";
             latitude = "28.5871232";
             longitude = "77.06061";
             price = 21;
             salesTax = 0;
             specialrequestId = 16;
             status = 1;
             totalAmount = 32;
             userId = 267;
             userImage = "";
             userName = "I am customer";
             userPhone = 8787654567;
             whatYouWant = bh;
             */
//            if (item!["requestMoney"] as! String) == "" {
//                cell.backgroundColor = .systemRed
//            } else {
//                cell.backgroundColor = .white
//            }
            cell.lblName.text = (item!["whatYouWant"] as! String)
            
            if item!["totalAmount"] is String {
                                
                print("Yes, it's a String")
                
                // cell.lblPrice.text = "$ "+(item!["totalAmount"] as! String)
                let totalAmount = Double((item!["totalAmount"] as! String))!+Double((item!["transactionFee"] as! String))!
                cell.lblPrice.text = "$ "+String(totalAmount)
                
            } else if item!["totalAmount"] is Int {
                
                print("It is Integer")
                
                let x2 : Int = (item!["totalAmount"] as! Int)
                let myString2 = String(x2)
                // cell.lblPrice.text = "$ "+myString2
                
                let totalAmount = Double(myString2)!+Double((item!["transactionFee"] as! String))!
                cell.lblPrice.text = "$ "+String(totalAmount)
                
            } else {
              //some other check
                print("i am ")
                
                let temp:NSNumber = item!["totalAmount"] as! NSNumber
                let tempString = temp.stringValue
                // cell.lblPrice.text = "$ "+tempString
                
                let totalAmount = Double(tempString)!+Double((item!["transactionFee"] as! String))!
                cell.lblPrice.text = "$ "+String(totalAmount)
                
            }
            
            cell.lblDate.text = (item!["created"] as! String)
            cell.lblService.text = (item!["address"] as! String)
            
            cell.imgProfileImage.sd_setImage(with: URL(string: (item!["userImage"] as! String)), placeholderImage: UIImage(named: "profile"))
            
        }
        
        cell.backgroundColor = .clear
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
    // dictGetAllServiceDetails
        print(strString as Any)
    
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        print(item as Any)
    
        /*if strString == "1" {
        
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPServiceJobDetailsId") as? UPServiceJobDetails
            settingsVCId!.dictGetAllServiceDetails = item as NSDictionary?
            settingsVCId!.fromDriverScreen2 = "yesFromDriverPortal"
        // settingsVCId!.hideReviewString = "yes"
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
        } else if strString == "2" {
        
        
            var ar : NSArray!
            ar = (item!["foodDetails"] as! Array<Any>) as NSArray
            if ar.count == 0 {
            
                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDSpecialOfferId") as? PDSpecialOffer
                settingsVCId!.dictGetDetailsForSpecialOffer = item as NSDictionary?
                self.navigationController?.pushViewController(settingsVCId!, animated: true)
            
            } else {
            
                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDRestaurantDetailsOrdersId") as? PDRestaurantDetailsOrders
                settingsVCId!.dictGetFoodDetails = item as NSDictionary?
                settingsVCId!.strDeliveryDetailsFromUserProfile = "yes"
                settingsVCId!.deliveryDetails = "nice"
                self.navigationController?.pushViewController(settingsVCId!, animated: true)
            
            }
        // self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
        
        
        } else if strString == "3" {
        
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDToGetDetailsOnlyId") as? PDToGetDetailsOnly
            settingsVCId!.dictGetAllValueOfGetDetails = item as NSDictionary?
        // settingsVCId!.fromDriver = "iAmFromDriverScreen"
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
        } else if strString == "4" {
            
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpecialOrderDetailsCustomerId") as? SpecialOrderDetailsCustomer
                settingsVCId!.dictGetSpecialOrderData = item as NSDictionary?
                self.navigationController?.pushViewController(settingsVCId!, animated: true)
            
        }*/
        
        
        var ar : NSArray!
        ar = (item!["foodDetails"] as! Array<Any>) as NSArray
        if ar.count == 0 {
        
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDSpecialOfferId") as? PDSpecialOffer
            settingsVCId!.dictGetDetailsForSpecialOffer = item as NSDictionary?
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
        } else {
        
            
        
            
            
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDRestaurantDetailsOrdersId") as? PDRestaurantDetailsOrders
            settingsVCId!.dictGetFoodDetails = item as NSDictionary?
            settingsVCId!.strDeliveryDetailsFromUserProfile = "yes"
            settingsVCId!.deliveryDetails = "nice"
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
        }
        
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100 // UITableView.automaticDimension // 101
    }
    
}

extension PDJobRequest: UITableViewDelegate {
    
}
