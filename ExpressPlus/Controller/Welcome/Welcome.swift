//
//  Welcome.swift
//  ExpressPlus
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
// MARK:- LOCATION -
import CoreLocation

class Welcome: UIViewController, CLLocationManagerDelegate {

     let locationManager = CLLocationManager()
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var btnCustomer:UIButton! {
        didSet {
            btnCustomer.setTitleColor(.white, for: .normal)
            btnCustomer.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnCustomer.layer.cornerRadius = 4
            btnCustomer.clipsToBounds = true
        }
    }
    @IBOutlet weak var btnPlusDriver:UIButton! {
        didSet {
            btnPlusDriver.setTitleColor(.white, for: .normal)
            btnPlusDriver.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnPlusDriver.layer.cornerRadius = 4
            btnPlusDriver.clipsToBounds = true
        }
    }
    @IBOutlet weak var btnRestaurant:UIButton! {
        didSet {
            btnRestaurant.setTitleColor(.white, for: .normal)
            btnRestaurant.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnRestaurant.layer.cornerRadius = 4
            btnRestaurant.clipsToBounds = true
            btnRestaurant.setTitle("Restaurant", for: .normal)
            btnRestaurant.isHidden = true
        }
    }
    
    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.backgroundColor = .white
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // navigationBarStyle()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        sideBarMenuClick()
        
        self.btnPlusDriver.addTarget(self, action: #selector(pushToPlusDriverScreen), for: .touchUpInside)
        self.btnCustomer.addTarget(self, action: #selector(pushToCustomerScreen), for: .touchUpInside)
        self.btnRestaurant.addTarget(self, action: #selector(restaurantToCustomerScreen), for: .touchUpInside)
        
        let a:Int? = Int("12.24")
        print(a as Any)
        
        let decimalToInt = NSDecimalNumber(decimal: 12.24).intValue
        print(decimalToInt as Any)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             print(person as Any)
            /*
             ["fullName": ios2020, "AccountHolderName": , "state": , "email": ios2020@gmail.com, "socialType": , "middleName": , "longitude": , "companyBackground": , "BankName": , "lastName": , "latitude": , "AccountNo": , "socialId": , "accountType": , "gender": , "ssnImage": , "role": Member, "country": , "address": Wadala West Mumbai, "deviceToken": , "firebaseId": , "contactNumber": 8287632340, "drivlingImage": , "device": iOS, "dob": , "logitude": , "AutoInsurance": , "userId": 127, "wallet": 0, "image": , "zipCode": , "RoutingNo": , "foodTag": ]
             */
            
            if person["role"] as! String == "Member" {
                
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPFoodId")
                self.navigationController?.pushViewController(push, animated: false)
                
            } else if person["role"] as! String == "Driver" {
                print(person as Any)
                
                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                self.navigationController?.pushViewController(settingsVCId!, animated: false)
                
                // if person["ssnImage"] as! String == "" {
                    // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDCompleteProfileId") as? PDCompleteProfile
                    // settingsVCId!.dictGetRegistrationData = person as NSDictionary
                    // self.navigationController?.pushViewController(settingsVCId!, animated: false)
                // } else
                /*if person["AccountHolderName"] as! String == "" {
                      let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDStateCountryCityId") as? PDStateCountryCity
                     self.navigationController?.pushViewController(settingsVCId!, animated: false)
                } else {
                    
                    // if person["StripeStatus"] as! String == "0" {
                        
                    // } else {
                        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                        self.navigationController?.pushViewController(settingsVCId!, animated: false)
                    // }
                     
                }*/
                
            } else if person["role"] as! String == "Restaurant" {
                if person["AccountHolderName"] as! String == "" {
                    let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RAfterRegistrationId") as? RAfterRegistration
                    // settingsVCId!.dictGetRegistrationData = person as NSDictionary
                    self.navigationController?.pushViewController(settingsVCId!, animated: false)
                } else {
                    let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RHomeId") as? RHome
                    self.navigationController?.pushViewController(settingsVCId!, animated: false)
                }
                
                
            } else {
                
            }
        }
        
        self.iAmHereForLocationPermission()
    }

    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                // self.strSaveLatitude = "0"
                // self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
                // self.iAmHereForLocationPermission()
                
            @unknown default:
                break
            }
        }
    }
    
    @objc func navigationBarStyle() {
        self.title = "WELCOME"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.barTintColor = NAVIGATION_BACKGROUND_COLOR
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.backgroundColor = .orange
    }
    
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    // MARK:- PUSH TO RESTAURANT SCREEN -
    @objc func restaurantToCustomerScreen() {
        let myString = "RestaurantScreen"
        UserDefaults.standard.set(myString, forKey: "keyWhichScreenItIs")
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginAllId") as? LoginAll
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
//         let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RHomeId") as? RHome
//         self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    // MARK:- PUSH TO PLUS DRIVER SCREEN -
    @objc func pushToPlusDriverScreen() {
        let myString = "PlusDriverScreen"
        UserDefaults.standard.set(myString, forKey: "keyWhichScreenItIs")
        
         let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginAllId") as? LoginAll
         self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    // MARK:- PUSH TO CUSTOMER DASHBOARD SCREEN -
    @objc func pushToCustomerScreen() {
        let myString = "CustomerScreen"
        UserDefaults.standard.set(myString, forKey: "keyWhichScreenItIs")
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginAllId") as? LoginAll
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
         // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPDashboardId") as? UPDashboard
         // self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
        
         
        
        
    }
}
