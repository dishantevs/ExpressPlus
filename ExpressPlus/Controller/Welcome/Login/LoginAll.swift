//
//  LoginAll.swift
//  ExpressPlus
//
//  Created by Apple on 05/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class LoginAll: UIViewController, UITextFieldDelegate {

     var myDeviceTokenIs:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "LOGIN"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var txtEmail:UITextField! {
        didSet {
            txtEmail.layer.cornerRadius = 6
            txtEmail.clipsToBounds = true
            txtEmail.setLeftPaddingPoints(40)
            txtEmail.layer.borderColor = UIColor.clear.cgColor
            txtEmail.layer.borderWidth = 0.8
        }
    }
    
    @IBOutlet weak var txtPassword:UITextField! {
        didSet {
            txtPassword.layer.cornerRadius = 6
            txtPassword.clipsToBounds = true
            txtPassword.setLeftPaddingPoints(40)
            txtPassword.layer.borderColor = UIColor.clear.cgColor
            txtPassword.layer.borderWidth = 0.8
        }
    }
    
    @IBOutlet weak var btnSignUp:UIButton!
    @IBOutlet weak var btnForgotPassword:UIButton!
    
    @IBOutlet weak var btnSignInSubmit:UIButton! {
        didSet {
            btnSignInSubmit.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnSignInSubmit.layer.cornerRadius = 6
            btnSignInSubmit.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        btnSignUp.addTarget(self, action: #selector(signUpClickMethod), for: .touchUpInside)
        btnSignInSubmit.addTarget(self, action: #selector(signInSubmitClickMethod), for: .touchUpInside)
        btnForgotPassword.addTarget(self, action: #selector(forgotPasswordClickMethod), for: .touchUpInside)
        
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        // MARK:- VIEW UP WHEN CLICK ON TEXT FIELD -
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
         // self.pushToCompleteprofile()
    }
    
    
    
    @objc func pushToCompleteprofile() {
    
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDStateCountryCityId") as? PDStateCountryCity
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    // MARK:- SIGN UP CLICK -
    @objc func signUpClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegistrationAllId") as? RegistrationAll
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    // MARK:- KEYBOARD WILL SHOW -
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    // MARK:- KEYBOARD WILL HIDE -
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    // MARK:- FORGOT PASSWORD -
    @objc func forgotPasswordClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ForgotPasswordId") as? ForgotPassword
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    // MARK:- CHECK VALIDATION BEFORE LOGIN -
    @objc func signInValidationCheck() {
        
        if self.txtEmail.text! == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Email should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if self.txtPassword.text! == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Password should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            // MARK:- CALL WEBSERVICE OF SIGN IN WHEN TEXT FIELDS IS NOT EMPTY -
            self.signInSubmitClickMethod()
        }
        
    }
    
    // MARK:- WEBSERVICE ( SIGN IN ) -
    @objc func signInSubmitClickMethod() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

        var parameters:Dictionary<AnyHashable, Any>!
                  
        // Create UserDefaults
        let defaults = UserDefaults.standard
        if let myString = defaults.string(forKey: "deviceFirebaseToken") {
            myDeviceTokenIs = myString

        }
        else {
            myDeviceTokenIs = "111111111111111111111"
        }
        
        parameters = ["action"          : "login",
                      "email"           : String(self.txtEmail.text!),
                      "password"        : String(self.txtPassword.text!),
                      "device"          : "iOS",
                      "deviceToken"     : String(myDeviceTokenIs),
        ]
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String // match your success status here
                                              
                    var strMessage : String!
                    strMessage = JSON["msg"]as Any as? String
                    
                    if strSuccess == String("success") {
                        
                        
                        var dict: Dictionary<AnyHashable, Any>
                        dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                        let defaults = UserDefaults.standard
                        defaults.setValue(dict, forKey: "keyLoginFullData")
                        
                        print(dict as Any)
                        
                        
                        
                        self.pushScreenWB()
                        
                        // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPDashboardId") as? UPDashboard
                        // self.navigationController?.pushViewController(settingsVCId!, animated: true)
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                        
                        let alert = UIAlertController(title: String(strSuccess), message: String(strMessage), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                             
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
    }
    
    /*@objc func ifUserConnectTheirAccount(strAccountNumber:String) {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

        var parameters:Dictionary<AnyHashable, Any>!
                  
        /*
         action: checkstripe
         userId:
         Account:  //stripeAccountNo
         */
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // let str:String = person["role"] as! String
           
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
            parameters = ["action"      : "checkstripe",
                          "userId"      : String(myString),
                          "Account"     : String(strAccountNumber)
            ]
        }
        
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String // match your success status here
                                              
                    var strMessage : String!
                    strMessage = JSON["msg"]as Any as? String
                    
                    var strMessage22 : String!
                    strMessage22 = JSON["StripeStatus"]as Any as? String
                    
                    if strSuccess == String("success") {
                        ERProgressHud.sharedInstance.hide()
                        
                       //  var strURL : String!
                        // strURL = JSON["url"]as Any as? String
                        
                        // var dict: Dictionary<AnyHashable, Any>
                        // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                        if strMessage22 == "1" {
                            
                            self.pushScreenWB()
                            
                        } else {
                            
                            self.pushScreenWB()
                            
                            /*guard let url = URL(string: strURL) else { return }
                            UIApplication.shared.open(url)*/
                        }
                            
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                        
                        let alert = UIAlertController(title: String(strSuccess), message: String(strMessage), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                             
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
    }*/
    
    @objc func pushScreenWB() {
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
             print(person as Any)
            
            /*
             ["fullName": ios2020, "AccountHolderName": , "state": , "email": ios2020@gmail.com, "socialType": , "middleName": , "longitude": , "companyBackground": , "BankName": , "lastName": , "latitude": , "AccountNo": , "socialId": , "accountType": , "gender": , "ssnImage": , "role": Member, "country": , "address": Wadala West Mumbai, "deviceToken": , "firebaseId": , "contactNumber": 8287632340, "drivlingImage": , "device": iOS, "dob": , "logitude": , "AutoInsurance": , "userId": 127, "wallet": 0, "image": , "zipCode": , "RoutingNo": , "foodTag": ]
             */
            
            if person["role"] as! String == "Member" {
                
                ERProgressHud.sharedInstance.hide()
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPFoodId")
                self.navigationController?.pushViewController(push, animated: false)
                
            } else if person["role"] as! String == "Driver" {
                
                /*if person["AccountHolderName"] as! String == "" {
                    let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDCompleteProfileId") as? PDCompleteProfile
                    // settingsVCId!.dictGetRegistrationData = person as NSDictionary
                    self.navigationController?.pushViewController(settingsVCId!, animated: false)
                } else
                 */
                
                ERProgressHud.sharedInstance.hide()
                
                ERProgressHud.sharedInstance.hide()
                
                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                self.navigationController?.pushViewController(settingsVCId!, animated: false)
                
                /*if person["AccountHolderName"] as! String == "" {
                    
                    let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDStateCountryCityId") as? PDStateCountryCity
                    self.navigationController?.pushViewController(settingsVCId!, animated: false)
                        
                } else {
                    
                    ERProgressHud.sharedInstance.hide()
                    
                    let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                    self.navigationController?.pushViewController(settingsVCId!, animated: false)
                    
                }*/
                
                // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                // self.navigationController?.pushViewController(settingsVCId!, animated: true)
                
            } else if person["role"] as! String == "Restaurant" {
                
                ERProgressHud.sharedInstance.hide()
                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RHomeId") as? RHome
                self.navigationController?.pushViewController(settingsVCId!, animated: true)
                
            } else {
                
                ERProgressHud.sharedInstance.hide()
                
            }
        }
    }
    
}
