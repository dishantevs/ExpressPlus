//
//  NotificationPopupForServiceRequest.swift
//  ExpressPlus
//
//  Created by Apple on 19/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import Alamofire

class NotificationPopupForServiceRequest: UIViewController {

    var dictOfNotificationPopup:NSDictionary!
    
    @IBOutlet weak var lblService:UILabel!
    @IBOutlet weak var lblCreated:UILabel!
    
    @IBOutlet weak var lblBookingDate:UILabel!
    @IBOutlet weak var lblWorkLocation:UILabel!
    
    @IBOutlet weak var lblServiceFee:UILabel!
    @IBOutlet weak var lblTipAmount:UILabel!
    
    @IBOutlet weak var lblTotalAmount:UILabel!
    
    var changesBookingIdis:String!
    
    @IBOutlet weak var btnAccept:UIButton! {
        didSet {
            btnAccept.layer.cornerRadius = 8
            btnAccept.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnDecline:UIButton! {
        didSet {
            btnDecline.layer.cornerRadius = 8
            btnDecline.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var notificationTitle:UILabel!
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var dismissFullPopup:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
         
        print(dictOfNotificationPopup as Any)
        
        self.notificationTitle.text = "Service Request"
        
        self.dismissFullPopup.addTarget(self, action: #selector(dismisspOPupToDashbaord), for: .touchUpInside)
        
        /*
         address = "Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India";
         aps =     {
             alert = "New booking request to confirm or decline.";
         };
         bookingDate = "2020-09-19";
         bookingId = 410;
         contactNumber = 2385245222;
         "gcm.message_id" = 1600515077120959;
         "google.c.a.e" = 1;
         "google.c.sender.id" = 27735899901;
         groupId = 222;
         message = "New booking request to confirm or decline.";
         serviceName = "Pet Walking";
         slote = "22:00-23:00";
         type = ServiceRequest;
         userName = driverw;
         */
        
        self.lblService.text        = (dictOfNotificationPopup["serviceName"] as! String)
        self.lblCreated.text        = (dictOfNotificationPopup["slote"] as! String)
        self.lblWorkLocation.text   = (dictOfNotificationPopup["address"] as! String)
        self.lblBookingDate.text    = (dictOfNotificationPopup["bookingDate"] as! String)
        
        self.btnAccept.addTarget(self, action: #selector(acceptServiceClickMethod), for: .touchUpInside)
        self.btnDecline.addTarget(self, action: #selector(declineServiceClickMethod), for: .touchUpInside)
    }
    

     @objc func dismisspOPupToDashbaord() {
         
         let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
         self.navigationController?.pushViewController(settingsVCId!, animated: false)
         
     }

    @objc func acceptServiceClickMethod() {
            
            let alert = UIAlertController(title: String("Message!"), message: String("Are you sure you want to accept ?"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Accept", style: UIAlertAction.Style.default, handler: { action in
                 
                self.acceptOrDecline(strStatus: "1")
                
            }))
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: { action in
                 
     
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            
        }
        
        @objc func declineServiceClickMethod() {
            
            let alert = UIAlertController(title: String("Message!"), message: String("Are you sure you want to Skip ?"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Skip", style: UIAlertAction.Style.destructive, handler: { action in
                 
                self.acceptOrDecline(strStatus: "3")
                
            }))
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: { action in
                 
                
                
            }))
            self.present(alert, animated: true, completion: nil)
            
            
        }
        
        // MARK:- WEBSERVICE ( ACCEPT OR DECLINE ) -
        @objc func acceptOrDecline(strStatus:String) {
            
            if strStatus == "1" {
                ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "accepting...")
            } else {
                ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "rejecting...")
            }
            
        /*
         [action] => changebookingstatus
         [driverId] => 123
         [bookingId] => 254
         [status] => 3 // reject
         */
        
        /*
        [action] => changebookingstatus
        [driverId] => 123
        [bookingId] => 254
        [status] => 1 // accept
        */
            let urlString = BASE_URL_EXPRESS_PLUS
        
            var parameters:Dictionary<AnyHashable, Any>!
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                
                
                // let x : Int = dictOfNotificationPopup["bookingId"] as! Int
                // let myStringBookingId = String(x)
                
                
                if dictOfNotificationPopup["bookingId"] is String {
                                    
                    print("Yes, it's a String")
                    self.changesBookingIdis = (dictOfNotificationPopup["bookingId"] as! String);
                    
                } else if dictOfNotificationPopup["bookingId"] is Int {
                    
                    print("It is Integer")
                    let x2 : Int = (dictOfNotificationPopup["bookingId"] as! Int)
                    let myString2 = String(x2)
                    self.changesBookingIdis = String(myString2)
                    
                } else {
                    
                    print("i am ")
                    let temp:NSNumber = dictOfNotificationPopup["bookingId"] as! NSNumber
                    let tempString = temp.stringValue
                    self.changesBookingIdis = String(tempString)
                    
                }
                
                
                
                
                let x2 : Int = person["userId"] as! Int
                let driverId = String(x2)
                
                if strStatus == "1" {
                    parameters = [
                        "action"        : "changebookingstatus",
                        "driverId"      : String(driverId),
                        "bookingId"     : String(self.changesBookingIdis),
                        "status"        : String(strStatus)
                    ]
                } else {
                    parameters = [
                        "action"        : "changebookingstatus",
                        "driverId"      : String(driverId),
                        "bookingId"     : String(self.changesBookingIdis),
                        "status"        : String(strStatus)
                    ]
                }
                
                       
         }
                       print("parameters-------\(String(describing: parameters))")
                       
                       Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                           {
                               response in
                   
                               switch(response.result) {
                               case .success(_):
                                  if let data = response.result.value {

                                   let JSON = data as! NSDictionary
                                    print(JSON as Any)
                                   
                                   var strSuccess : String!
                                   strSuccess = JSON["status"]as Any as? String
                                   
                                     // var strSuccessAlert : String!
                                     // strSuccessAlert = JSON["msg"]as Any as? String
                                   
                                   if strSuccess == String("success") {
                                    print("yes")
                                     
                                    var strSuccess2 : String!
                                    strSuccess2 = JSON["msg"]as Any as? String
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                        
                                        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                                        self.navigationController?.pushViewController(settingsVCId!, animated: false)
                                        
                                        
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                   }
                                   else {
                                    print("no")
                                     ERProgressHud.sharedInstance.hide()
                                    
                                    
                                   }
                               }

                               case .failure(_):
                                   print("Error message:\(String(describing: response.result.error))")
                                   
                                   ERProgressHud.sharedInstance.hide()
                                   
                                   let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                   
                                   let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                           UIAlertAction in
                                           NSLog("OK Pressed")
                                       }
                                   
                                   alertController.addAction(okAction)
                                   
                                   self.present(alertController, animated: true, completion: nil)
                                   
                                   break
                                }
                           }
        
           }
    }

