//
//  NotificationForSpecialOrder.swift
//  ExpressPlus
//
//  Created by Apple on 19/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class NotificationForSpecialOrder: UIViewController {

    var dictOfNotificationPopup:NSDictionary!
    
    @IBOutlet weak var lblService:UILabel!
    @IBOutlet weak var lblCreated:UILabel!
    
    @IBOutlet weak var lblBookingDate:UILabel!
    @IBOutlet weak var lblWorkLocation:UILabel!
    
    @IBOutlet weak var lblServiceFee:UILabel!
    @IBOutlet weak var lblTipAmount:UILabel!
    
    @IBOutlet weak var lblTotalAmount:UILabel!
    
    var changesBookingIdis:String!
    
    @IBOutlet weak var btnAccept:UIButton! {
        didSet {
            btnAccept.layer.cornerRadius = 8
            btnAccept.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnDecline:UIButton! {
        didSet {
            btnDecline.layer.cornerRadius = 8
            btnDecline.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var notificationTitle:UILabel!
    
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var dismissFullPopup:UIButton!
    
     
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
         
        print(dictOfNotificationPopup as Any)
        
        self.notificationTitle.text = "Special Order"
        
        self.dismissFullPopup.addTarget(self, action: #selector(dismisspOPupToDashbaord), for: .touchUpInside)
        
        /*
       TIP = 253;
         address = "Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India";
         aps =     {
             alert = "New special request";
         };
         bookingDate = "2020-09-19";
         deliveryFee = 6;
         driverImage = "/home/expressplusnow/public_html/appbackend/webroot/img/uploads/users/160033481021-06-12-images.jpg";
         driverName = driverw;
         driverPhone = 2385245222;
         "gcm.message_id" = 1600517983348885;
         "google.c.a.e" = 1;
         "google.c.sender.id" = 27735899901;
         longitude = "77.0606093";
         message = "New special request";
         price = 517;
         salesTax = 0;
         specialrequestId = 47;
         status = 1;
         type = specialRequest;
         userId = 343;
         userImage = "/home/expressplusnow/public_html/appbackend/webroot/img/uploads/users/";
         userName = customer;
         userPhone = 2525360000;
         whatYouWant = g;
         */
        
        // self.lblService.text        = (dictOfNotificationPopup["serviceName"] as! String)
        // self.lblCreated.text        = (dictOfNotificationPopup["slote"] as! String)
        self.lblWorkLocation.text   = (dictOfNotificationPopup["address"] as! String)
        self.lblBookingDate.text    = (dictOfNotificationPopup["whatYouWant"] as! String)
        
         self.btnAccept.addTarget(self, action: #selector(acceptServiceClickMethod), for: .touchUpInside)
        // self.btnDecline.addTarget(self, action: #selector(declineServiceClickMethod), for: .touchUpInside)
    }
    

    @objc func dismisspOPupToDashbaord() {
         
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
        self.navigationController?.pushViewController(settingsVCId!, animated: false)
         
    }
    
    @objc func acceptServiceClickMethod() {
        
         // let myString = "fromNotification"
         // UserDefaults.standard.set(myString, forKey: "keyFromNotification")
        
        
        /*
         let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpecialOrderScreenFromnotificaitonId") as? SpecialOrderScreenFromnotificaiton
         settingsVCId!.dictGetSpecialOrderData = dictOfNotificationPopup
         self.navigationController?.pushViewController(settingsVCId!, animated: false)
         */
        
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDJobRequestId") as? PDJobRequest
         settingsVCId!.dictOfNotificationPopup = dictOfNotificationPopup
        self.navigationController?.pushViewController(settingsVCId!, animated: false)
        
        
        
        
        //
    }
        
        
}

