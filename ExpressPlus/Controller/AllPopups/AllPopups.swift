//
//  AllPopups.swift
//  ExpressPlus
//
//  Created by Apple on 19/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class AllPopups: UIViewController {

    let cellReuseIdentifier = "allPopupsTableCell"
    
    var dictOfNotificationPopup:NSDictionary!
    
    // to get
    var toGetRequestIdInString:String!
    var toGetDriverRequestIdInString:String!
    
    // food request
    var foodRequestIdInString:String!
    var foodDriverRequestIdInString:String!
    
    @IBOutlet weak var notificationTitle:UILabel!
    
    var whichRequest:String!
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
            tbleView.tableFooterView = UIView.init()
        }
    }
    
    @IBOutlet weak var dismissFullPopup:UIButton!
    
    @IBOutlet weak var btnGooglemaps:UIButton!
    
    @IBOutlet weak var btnAccept:UIButton! {
        didSet {
            btnAccept.layer.cornerRadius = 8
            btnAccept.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnDecline:UIButton! {
        didSet {
            btnDecline.layer.cornerRadius = 8
            btnDecline.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        print(dictOfNotificationPopup as Any)
        
        self.dismissFullPopup.addTarget(self, action: #selector(dismisspOPupToDashbaord), for: .touchUpInside)
        
        // to get
        /*
        StoreCity = vgg;
           address = "Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India";
           aps =     {
               alert = "New Toget request for confirm or decline.";
           };
           created = "2020-09-19 15:41:41";
           driverId = 334;
           "gcm.message_id" = 1600510301546273;
           "google.c.a.e" = 1;
           "google.c.sender.id" = 27735899901;
           message = "New Toget request for confirm or decline.";
           notes = dhdudi;
           phone = 2525360000;
           togetDriverRequestId = 1209;
           togetRequestId = 190;
           type = ToGetRequest;
           userId = 343;
           userName = customer;
           wahtUwant = gigig;
        */
        
        // food
        
        /*
         address = "Unnamed Road, Sector 6, Sector 10 Dwarka, Dwarka, Delhi, 110075, India";
         aps =     {
             alert = "New booking request to confirm or decline.";
         };
         city = Delhi;
         deliveryLat = "28.5871315";
         deliveryLong = "77.0606113";
         foodOrderId = 287;
         foodrequestId = 3056;
         "gcm.message_id" = 1600512939608879;
         "google.c.a.e" = 1;
         "google.c.sender.id" = 27735899901;
         landmark = "dishantrajput38@gmail.com";
         message = "New booking request to confirm or decline.";
         name = hdhdh;
         phone = 6598656532;
         resturentAddress = "89, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, Uttar Pradesh 201011, India";
         resturentLatitude = "33.985805";
         resturentLongitude = "77.3240623";
         resturentcontactNumber = 5552808525;
         state = Delhi;
         totalDistance = 50;
         totalTime = "40 Min";
         type = FoodRequest;
         zipcode = 110075;
         */
        
        self.btnGooglemaps.addTarget(self, action: #selector(googleMapsOpen), for: .touchUpInside)
        
        if dictOfNotificationPopup["type"] as! String == "ToGetRequest" {
            self.whichRequest = "toGetRequest"
            
            self.notificationTitle.text = "To Get Request"
            
            self.btnAccept.addTarget(self, action: #selector(acceptServiceClickMethod), for: .touchUpInside)
            self.btnDecline.addTarget(self, action: #selector(declineServiceClickMethod), for: .touchUpInside)
            
        } else if dictOfNotificationPopup["type"] as! String == "FoodRequest" {
            self.whichRequest = "foodRequest"
            
            self.notificationTitle.text = "Food Request"
            
            self.btnAccept.addTarget(self, action: #selector(acceptServiceClickMethod), for: .touchUpInside)
            self.btnDecline.addTarget(self, action: #selector(declineServiceClickMethod), for: .touchUpInside)
        }
        
    }

    @objc func googleMapsOpen() {
    
        let alert = UIAlertController(title: String("Express Plus"), message: String("Please Google to the closest restaurant to you based on this order."), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
            
            
            if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                UIApplication.shared.openURL(NSURL(string:
                                                    "comgooglemaps://?saddr=&daddr=\(self.dictOfNotificationPopup["deliveryLat"] as! String),\(self.dictOfNotificationPopup["deliveryLong"] as! String)&directionsmode=driving")! as URL)

            } else {
                NSLog("Can't use comgooglemaps://")
                
                let alert = UIAlertController(title: String("Error"), message: String("Either Google Maps is not installed in your Device or your Device does not support Google Maps"), preferredStyle: UIAlertController.Style.alert)
               
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                     
         
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            }
         
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func dismisspOPupToDashbaord() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
        self.navigationController?.pushViewController(settingsVCId!, animated: false)
        
    }
    
    @objc func acceptServiceClickMethod() {
        
        if self.whichRequest == "toGetRequest" {
            self.acceptOrDecline(strStatus: "2")
        } else if self.whichRequest == "foodRequest" {
            self.acceptOrDecline(strStatus: "2")
        }
        
    }
    @objc func declineServiceClickMethod() {
        
        if self.whichRequest == "toGetRequest" {
            self.acceptOrDecline(strStatus: "3")
        } else if self.whichRequest == "foodRequest" {
            self.acceptOrDecline(strStatus: "3")
        }
        
    }
    
    // MARK:- WEBSERVICE ( ACCEPT OR DECLINE ) -
    @objc func acceptOrDecline(strStatus:String) {
        
        if strStatus == "2" {
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "accepting...")
        } else {
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "skipping...")
        }
      
        
        let urlString = BASE_URL_EXPRESS_PLUS
    
        var parameters:Dictionary<AnyHashable, Any>!
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            // let x : Int = dictGetDriveGetDetails["bookingId"] as! Int
            // let myStringBookingId = String(x)
            
            let x2 : Int = person["userId"] as! Int
            let driverId = String(x2)
           
            if self.whichRequest == "toGetRequest" {
                // driver request id
                if dictOfNotificationPopup["togetDriverRequestId"] is String {
                                    
                    print("Yes, it's a String")
                    self.toGetDriverRequestIdInString = (dictOfNotificationPopup["togetDriverRequestId"] as! String);
                    
                } else if dictOfNotificationPopup["togetDriverRequestId"] is Int {
                    
                    print("It is Integer")
                    let x2 : Int = (dictOfNotificationPopup["togetDriverRequestId"] as! Int)
                    let myString2 = String(x2)
                    self.toGetDriverRequestIdInString = String(myString2)
                    
                } else {
                    
                    print("i am ")
                    let temp:NSNumber = dictOfNotificationPopup["togetDriverRequestId"] as! NSNumber
                    let tempString = temp.stringValue
                    self.toGetDriverRequestIdInString = String(tempString)
                    
                }
                
                // request id
                if dictOfNotificationPopup["togetRequestId"] is String {
                                    
                    print("Yes, it's a String")
                    self.toGetRequestIdInString = (dictOfNotificationPopup["togetRequestId"] as! String);
                    
                } else if dictOfNotificationPopup["togetRequestId"] is Int {
                    
                    print("It is Integer")
                    let x2 : Int = (dictOfNotificationPopup["togetRequestId"] as! Int)
                    let myString2 = String(x2)
                    self.toGetRequestIdInString = String(myString2)
                    
                } else {
                    
                    print("i am ")
                    let temp:NSNumber = dictOfNotificationPopup["togetRequestId"] as! NSNumber
                    let tempString = temp.stringValue
                    self.toGetRequestIdInString = String(tempString)
                    
                }
            
            } else if self.whichRequest == "foodRequest" {
                
                /*
                 var foodRequestIdInString:String!
                 var foodDriverRequestIdInString:String!
                 */
                
                
                // driver request id
                if dictOfNotificationPopup["foodrequestId"] is String {
                                    
                    print("Yes, it's a String")
                    self.foodRequestIdInString = (dictOfNotificationPopup["foodrequestId"] as! String);
                    
                } else if dictOfNotificationPopup["foodrequestId"] is Int {
                    
                    print("It is Integer")
                    let x2 : Int = (dictOfNotificationPopup["foodrequestId"] as! Int)
                    let myString2 = String(x2)
                    self.foodRequestIdInString = String(myString2)
                    
                } else {
                    
                    print("i am ")
                    let temp:NSNumber = dictOfNotificationPopup["foodrequestId"] as! NSNumber
                    let tempString = temp.stringValue
                    self.foodRequestIdInString = String(tempString)
                    
                }
                
                // request id
                if dictOfNotificationPopup["foodOrderId"] is String {
                                    
                    print("Yes, it's a String")
                    self.foodDriverRequestIdInString = (dictOfNotificationPopup["foodOrderId"] as! String);
                    
                } else if dictOfNotificationPopup["foodOrderId"] is Int {
                    
                    print("It is Integer")
                    let x2 : Int = (dictOfNotificationPopup["foodOrderId"] as! Int)
                    let myString2 = String(x2)
                    self.foodDriverRequestIdInString = String(myString2)
                    
                } else {
                    /*
                     foodOrderId = 287;
                     foodrequestId = 3056;
                     */
                    print("i am ")
                    let temp:NSNumber = dictOfNotificationPopup["foodOrderId"] as! NSNumber
                    let tempString = temp.stringValue
                    self.foodDriverRequestIdInString = String(tempString)
                    
                }
                
                
            }
            
            
            
            if self.whichRequest == "toGetRequest" {
                
                parameters = [
                    "action"                : "togetrequestaccept",
                    "driverId"              : String(driverId),
                    "togetDriverRequestId"  : String(self.toGetDriverRequestIdInString),
                    "togetRequestId"        : String(self.toGetRequestIdInString),
                    "status"                : String(strStatus)
                ]
                
            } else if self.whichRequest == "foodRequest" {
                
                parameters = [
                    "action"        : "foodrequestaccept", // done
                    "userId"        : String(driverId), // done
                    "foodrequestId" : String(self.foodRequestIdInString),
                    "foodorderId"   : String(self.foodDriverRequestIdInString),
                    "status"        : String(strStatus) // done
                ]
                
            }
                   
     }
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
               
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                               
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                    if strSuccess == String("success") {
                        print("yes")
                                 
                        var strSuccess2 : String!
                        strSuccess2 = JSON["msg"]as Any as? String
                                
                        ERProgressHud.sharedInstance.hide()
                        let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                    
                            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                            self.navigationController?.pushViewController(settingsVCId!, animated: false)
                                    
                        }))
                        self.present(alert, animated: true, completion: nil)
                                
                    }
                    else if strSuccess == String("Fails") {
                        var strSuccess2 : String!
                        strSuccess2 = JSON["msg"]as Any as? String
                                
                        ERProgressHud.sharedInstance.hide()
                        let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                    
                                    
                            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                            self.navigationController?.pushViewController(settingsVCId!, animated: false)
                                    
                                    
                                    
                        }))
                        self.present(alert, animated: true, completion: nil)
                                
                                
                    }
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                               
                ERProgressHud.sharedInstance.hide()
                               
                let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                    NSLog("OK Pressed")
                }
                               
                alertController.addAction(okAction)
                               
                self.present(alertController, animated: true, completion: nil)
                               
                break
            }
        }
    }
}

//MARK:- TABLE VIEW -
extension AllPopups: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.whichRequest == "toGetRequest" {
            return 3
        } else if self.whichRequest == "foodRequest" {
            return 2
        } else {
            return 10
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AllPopupsTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! AllPopupsTableCell
        
        cell.backgroundColor = .white
        
        if self.whichRequest == "toGetRequest" {
            
            if indexPath.row == 0 {
                
                cell.lblTitle.text = "Product Name"
                cell.lblSubTitle.text   = (dictOfNotificationPopup["wahtUwant"] as! String)
                
            } else if indexPath.row == 1 {
                
                cell.lblTitle.text = "Store Name"
                cell.lblSubTitle.text   = (dictOfNotificationPopup["StoreCity"] as! String)
                
            } else if indexPath.row == 2 {
                
                cell.lblTitle.text = "Address"
                cell.lblSubTitle.text   = (dictOfNotificationPopup["address"] as! String)
                
            }
            
            
            // food restaurant request
        } else if self.whichRequest == "foodRequest" {
             
            if indexPath.row == 0 {
                
                cell.lblTitle.text = "Restaurant Name"
                cell.lblSubTitle.text = (dictOfNotificationPopup["resturentName"] as! String)
                
            } else {
                
                cell.lblTitle.text = "Drop Location"
                cell.lblSubTitle.text = (dictOfNotificationPopup["address"] as! String)
                
            }
            
        }
        
        
        
        
       //  let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        // print(item as Any)
       
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    
}

extension AllPopups: UITableViewDelegate {
    
}

