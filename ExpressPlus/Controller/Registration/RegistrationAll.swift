//
//  RegistrationAll.swift
//  ExpressPlus
//
//  Created by Apple on 05/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

// MARK:- LOCATION -
import CoreLocation

class RegistrationAll: UIViewController, CLLocationManagerDelegate {

    var strRole:String!
    
    let locationManager = CLLocationManager()
    
    var myDeviceTokenIs:String!
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "REGISTRATION"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    @IBOutlet weak var txtName:UITextField! {
        didSet {
            txtName.layer.cornerRadius = 6
            txtName.clipsToBounds = true
            txtName.setLeftPaddingPoints(40)
            txtName.layer.borderColor = UIColor.clear.cgColor
            txtName.layer.borderWidth = 0.8
        }
    }
    @IBOutlet weak var txtEmail:UITextField! {
        didSet {
            txtEmail.layer.cornerRadius = 6
            txtEmail.clipsToBounds = true
            txtEmail.setLeftPaddingPoints(40)
            txtEmail.layer.borderColor = UIColor.clear.cgColor
            txtEmail.layer.borderWidth = 0.8
            txtEmail.keyboardType = .emailAddress
        }
    }
    @IBOutlet weak var txtPhone:UITextField! {
        didSet {
            txtPhone.layer.cornerRadius = 6
            txtPhone.clipsToBounds = true
            txtPhone.setLeftPaddingPoints(40)
            txtPhone.layer.borderColor = UIColor.clear.cgColor
            txtPhone.layer.borderWidth = 0.8
            txtPhone.keyboardType = .phonePad
        }
    }
    @IBOutlet weak var txtPassword:UITextField! {
        didSet {
            txtPassword.layer.cornerRadius = 6
            txtPassword.clipsToBounds = true
            txtPassword.setLeftPaddingPoints(40)
            txtPassword.layer.borderColor = UIColor.clear.cgColor
            txtPassword.layer.borderWidth = 0.8
        }
    }
    
    @IBOutlet weak var btnSignIn:UIButton!
    @IBOutlet weak var btnSignUpSubmit:UIButton! {
        didSet {
            btnSignUpSubmit.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnSignUpSubmit.layer.cornerRadius = 6
            btnSignUpSubmit.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // MARK:- HIDE NAVIGATION BAR CONTROLLER -
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // MARK:- SET BUTTON ACTION -
        btnBack.addTarget(self, action: #selector(backMenuClick), for: .touchUpInside)
        btnSignIn.addTarget(self, action: #selector(backMenuClick), for: .touchUpInside)
        
        btnSignUpSubmit.addTarget(self, action: #selector(signUpValidationCheck), for: .touchUpInside)
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        // MARK:- VIEW UP WHEN CLICK ON TEXT FIELD -
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        self.strSaveLatitude = "0"
        self.strSaveLongitude = "0"
        
        self.iAmHereForLocationPermission()
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    // MARK:- GET CUSTOMER LOCATION -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! PDBagPurchaseTableCell
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            //print(self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            print("locality ==> "+locality as Any) // sector 10 dwarka
            
            print(self.strSaveCountryName as Any) // india
            print(self.strSaveStateName as Any) // new delhi
            print(self.strSaveZipcodeName as Any) // 110075
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            
            
            // self.findMyStateTaxWB()
        }
    }
    
    // MARK:- SIGN UP CLICK -
    @objc func signUpClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegistrationAllId") as? RegistrationAll
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    // MARK:- KEYBOARD WILL SHOW -
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    // MARK:- KEYBOARD WILL HIDE -
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func backMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    // MARK:- CHECK VALIDATION BEFORE REGISTRATION -
    @objc func signUpValidationCheck() {
        if self.txtName.text! == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Name should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if self.txtEmail.text! == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Email should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if self.txtPassword.text! == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Password should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if self.txtPhone.text! == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Phone should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            // MARK:- CALL WEBSERVICE OF SIGN UP WHEN TEXT FIELDS IS NOT EMPTY
            self.signUpSubmitClickMethod()
        }
    }
    // MARK:- WEBSERVICE ( SIGN UP )
    @objc func signUpSubmitClickMethod() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        self.view.endEditing(true)
        
        let urlString = BASE_URL_EXPRESS_PLUS

        var parameters:Dictionary<AnyHashable, Any>!
        
        if let myLoadedString = UserDefaults.standard.string(forKey: "keyWhichScreenItIs") {
            if myLoadedString == "CustomerScreen" {
                self.strRole = "Member"
            } else if myLoadedString == "PlusDriverScreen" { // driver
                self.strRole = "Driver"
            } else if myLoadedString == "RestaurantScreen" { // restaurant
                self.strRole = "Restaurant"
            }
        }
        
        /*
         self.strSaveLatitude = String(doubleStringLat)
         self.strSaveLongitude = String(doubleStringLong)
         */
        
        // Create UserDefaults
        let defaults = UserDefaults.standard
        if let myString = defaults.string(forKey: "deviceFirebaseToken") {
            myDeviceTokenIs = myString
        }
        else {
            myDeviceTokenIs = "111111111111111111111"
        }
        
        parameters = ["action"          : "registration",
                      "contactNumber"   : String(self.txtPhone.text!),
                      "email"           : String(self.txtEmail.text!),
                      "fullName"        : String(self.txtName.text!),
                      "password"        : String(self.txtPassword.text!),
                      "role"            : String(self.strRole),
                      "latitude"        : String(self.strSaveLatitude),
                      "longitude"       : String(self.strSaveLongitude),
                      "device"          : String("iOS"),
                      "deviceToken"     : String(myDeviceTokenIs),
        ]
        
        print("parameters-------\(String(describing: parameters))")

        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
            switch(response.result) {
            case .success(_):

                if let data = response.result.value {
                    let JSON = data as! NSDictionary
                    print(JSON as Any)
                                                  
                    var strSuccess : String!
                            
                    strSuccess = JSON["status"]as Any as? String // match your success status here
                                 
                    var strMessage : String!
                    strMessage = JSON["msg"]as Any as? String
                    
                    if strSuccess == String("success") {
                        ERProgressHud.sharedInstance.hide()
                        
                        var dict: Dictionary<AnyHashable, Any>
                        dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                        let defaults = UserDefaults.standard
                        defaults.setValue(dict, forKey: "keyLoginFullData")
                        
                        // print(dict as Any)
                        
                        if dict["role"] as! String == "Restaurant" {
                            
                            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RAfterRegistrationId") as? RAfterRegistration
                            settingsVCId!.dictGetRegistrationData = dict as NSDictionary
                            self.navigationController?.pushViewController(settingsVCId!, animated: true)
                            
                        } else if dict["role"] as! String == "Driver" {
                            
                            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDCompleteProfileId") as? PDCompleteProfile
                             settingsVCId!.dictGetRegistrationData = dict as NSDictionary
                            self.navigationController?.pushViewController(settingsVCId!, animated: true)
                            
                        } else {
                            
                            let alert = UIAlertController(title: String(strSuccess), message: String(strMessage), preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                 
                                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPFoodId")
                                self.navigationController?.pushViewController(settingsVCId, animated: true)
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            // self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                        
                        let alert = UIAlertController(title: String(strSuccess), message: String(strMessage), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                            // self.dismiss(animated: true, completion: nil)
                         
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                ERProgressHud.sharedInstance.hide()
                
                break
            }
        }
    }
}
